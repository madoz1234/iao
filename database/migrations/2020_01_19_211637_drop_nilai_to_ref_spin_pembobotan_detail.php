<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropNilaiToRefSpinPembobotanDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_spin_pembobotan_detail', function (Blueprint $table) {
            $table->dropColumn('nilai');
            // $table->dropColumn('pof_id');
        });

        Schema::table('log_ref_spin_pembobotan_detail', function (Blueprint $table) {
            $table->dropColumn('nilai');
            // $table->dropColumn('pof_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_spin_pembobotan_detail', function (Blueprint $table) {
            $table->decimal('nilai', 20, 2)->default(0);
            // $table->integer('pof_id');
        });

        Schema::table('log_ref_spin_pembobotan_detail', function (Blueprint $table) {
            $table->decimal('nilai', 20, 2)->default(0);
            // $table->integer('pof_id');
        });
    }
}
