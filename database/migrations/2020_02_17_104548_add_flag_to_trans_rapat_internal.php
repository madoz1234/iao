<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFlagToTransRapatInternal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_rapat_internal', function (Blueprint $table) {
            $table->integer('flag')->default(0)->comment('0:Draft, 1:Simpan')->after('status');
        });

        Schema::table('log_trans_rapat_internal', function (Blueprint $table) {
            $table->integer('flag')->default(0)->comment('0:Draft, 1:Simpan')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_rapat_internal', function (Blueprint $table) {
            $table->dropColumn('flag');
        });
        
        Schema::table('log_trans_rapat_internal', function (Blueprint $table) {
            $table->dropColumn('flag');
        });
    }
}
