<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransKegiatanAuditAddStatusRevisi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_rencana_audit', function (Blueprint $table) {
            $table->integer('status_revisi')->default(0)->comment('1:Historis')->after('ket_dirut');
        });

        Schema::table('log_trans_rencana_audit', function (Blueprint $table) {
            $table->integer('status_revisi')->default(0)->comment('1:Historis')->after('ket_dirut');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('log_trans_rencana_audit', function (Blueprint $table) {
            $table->dropColumn('status_revisi');
        });

        Schema::table('trans_rencana_audit', function (Blueprint $table) {
            $table->dropColumn('status_revisi');
        });
    }
}
