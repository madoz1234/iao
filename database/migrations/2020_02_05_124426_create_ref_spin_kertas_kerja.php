<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefSpinKertasKerja extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_spin_kertas_kerja', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('spin_id')->unsigned();
            $table->tinyInteger('kategori')->comment('0:BU,1:CO');
            $table->integer('kategori_id')->unsigned();
            $table->text('deskripsi')->nullable();
            $table->integer('status')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
            $table->foreign('spin_id')->references('id')->on('ref_spin_pembobotan')->onDelete('cascade');
        });

        Schema::create('ref_spin_kertas_kerja_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kertas_kerja_id')->unsigned();
            $table->integer('pembobotan_detail_id')->unsigned();
            $table->integer('status')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
            $table->foreign('kertas_kerja_id')->references('id')->on('ref_spin_kertas_kerja')->onDelete('cascade');
        });

        Schema::create('ref_spin_kertas_kerja_detail_nilai', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('detail_id')->unsigned();
            $table->integer('detail_nilai_id')->unsigned();
            $table->integer('status')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
            $table->foreign('detail_id')->references('id')->on('ref_spin_kertas_kerja_detail')->onDelete('cascade');
        });

        Schema::create('log_ref_spin_kertas_kerja', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('spin_id')->unsigned();
            $table->tinyInteger('kategori')->comment('0:BU,1:CO');
            $table->integer('kategori_id')->unsigned();
            $table->text('deskripsi')->nullable();
            $table->integer('status')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_spin_kertas_kerja_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('kertas_kerja_id')->unsigned();
            $table->integer('pembobotan_detail_id')->unsigned();
            $table->integer('status')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_spin_kertas_kerja_detail_nilai', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('detail_id')->unsigned();
            $table->integer('detail_nilai_id')->unsigned();
            $table->integer('status')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_ref_spin_kertas_kerja_detail_nilai');
        Schema::dropIfExists('log_ref_spin_kertas_kerja_detail');
        Schema::dropIfExists('log_ref_spin_kertas_kerja');

        Schema::dropIfExists('ref_spin_kertas_kerja_detail_nilai');
        Schema::dropIfExists('ref_spin_kertas_kerja_detail');
        Schema::dropIfExists('ref_spin_kertas_kerja');
    }
}
