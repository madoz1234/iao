<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefSurveiKepuasan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_survei', function (Blueprint $table) {
            $table->increments('id');
            $table->string('version');
            $table->integer('status')->default(0)->comment('0:Tidak Aktif,1:Aktif');
            $table->text('description')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });


        Schema::create('ref_survei_pertanyaan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('survei_id')->unsigned();
            $table->text('pertanyaan');
            $table->integer('jenis')->default(1)->comment('1:Rating,2:Setuju/Tidak, 3:Essai,4:PG');
            $table->integer('sum')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('survei_id')->references('id')->on('ref_survei')->onDelete('cascade');
        });

        Schema::create('ref_survei_pilgan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tanya_id')->unsigned();
            $table->string('pilgan', 10);
            $table->text('isi');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('tanya_id')->references('id')->on('ref_survei_pertanyaan')->onDelete('cascade');
        });

        Schema::create('trans_survei_jawab', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tanya_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->text('jawaban');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('tanya_id')->references('id')->on('ref_survei_pertanyaan')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('sys_users');
        });


        Schema::create('log_ref_survei', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->string('version');
            $table->integer('status')->default(0)->comment('0:Tidak Aktif,1:Aktif');
            $table->text('description')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });


        Schema::create('log_ref_survei_pertanyaan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('survei_id')->unsigned();
            $table->text('pertanyaan');
            $table->integer('jenis')->default(1)->comment('1:Rating,2:Setuju/Tidak, 3:Essai,4:PG');
            $table->integer('sum')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_survei_pilgan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('tanya_id')->unsigned();
            $table->string('pilgan', 10);
            $table->text('isi');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_survei_jawab', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('tanya_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->text('jawaban');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_survei_jawab');
        Schema::dropIfExists('log_ref_survei_pilgan');
        Schema::dropIfExists('log_ref_survei_pertanyaan');
        Schema::dropIfExists('log_ref_survei');
        Schema::dropIfExists('trans_survei_jawab');
        Schema::dropIfExists('ref_survei_pilgan');
        Schema::dropIfExists('ref_survei_pertanyaan');
        Schema::dropIfExists('ref_survei');
    }
}
