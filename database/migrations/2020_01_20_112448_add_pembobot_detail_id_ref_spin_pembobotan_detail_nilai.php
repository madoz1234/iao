<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPembobotDetailIdRefSpinPembobotanDetailNilai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_spin_pembobotan_detail_nilai', function (Blueprint $table) {
            $table->BigInteger('pembobot_detail_id')->after('pembobot_id')->unsigned();
            $table->foreign('pembobot_detail_id')->references('id')->on('ref_spin_pembobotan_detail')->onDelete('cascade');

        });

        Schema::table('log_ref_spin_pembobotan_detail_nilai', function (Blueprint $table) {
            $table->integer('pembobot_detail_id')->after('pembobot_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_spin_pembobotan_detail_nilai', function (Blueprint $table) {
        	$table->dropForeign(['pembobot_detail_id']);
            $table->dropColumn('pembobot_detail_id');
        });

        Schema::table('log_ref_spin_pembobotan_detail_nilai', function (Blueprint $table) {
            $table->dropColumn('pembobot_detail_id');
        });
    }
}
