<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransSurveiJawabDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_survei_jawab_detail', function (Blueprint $table) {
            $table->integer('tanya_id')->unsigned()->nullable()->change();
            $table->text('jawaban')->nullable()->change();
        });

        Schema::table('log_trans_survei_jawab_detail', function (Blueprint $table) {
            $table->integer('tanya_id')->unsigned()->nullable()->change();
            $table->text('jawaban')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_survei_jawab_detail', function (Blueprint $table) {
            $table->integer('tanya_id')->unsigned()->notNullable()->change();
            $table->text('jawaban')->notNullable()->change();
        });

        Schema::table('log_trans_survei_jawab_detail', function (Blueprint $table) {
            $table->integer('tanya_id')->unsigned()->notNullable()->change();
            $table->text('jawaban')->notNullable()->change();
        });
    }
}
