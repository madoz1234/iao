<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateFixTransSurveiKepuasanAudit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_survei_jawab', function (Blueprint $table) {
            $table->dropForeign(['lha_id']);
            $table->foreign('lha_id')->references('id')->on('trans_lha')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_survei_jawab', function (Blueprint $table) {
             $table->dropForeign(['lha_id']);
             $table->foreign('lha_id')->references('id')->on('trans_penugasan_audit')->onDelete('cascade');
        });
    }
}
