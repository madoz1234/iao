<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateRefKategoriTemuanChangeStatusToNama extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_kategori_temuan', function (Blueprint $table) {
           $table->dropColumn('status');
           $table->text('nama')->nullable()->after('id');
        });

        Schema::table('log_ref_kategori_temuan', function (Blueprint $table) {
           $table->dropColumn('status');
           $table->text('nama')->nullable()->after('ref_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('log_ref_kategori_temuan', function (Blueprint $table) {
            $table->dropColumn('nama');
            $table->integer('status')->default(1)->comment('1:Major, 2:Minor, 3:Observasi Negatif, 4:Observasi Positif')->after('ref_id');
        });

        Schema::table('ref_kategori_temuan', function (Blueprint $table) {
            $table->dropColumn('nama');
            $table->integer('status')->default(1)->comment('1:Major, 2:Minor, 3:Observasi Negatif, 4:Observasi Positif')->after('id');
        });
    }
}
