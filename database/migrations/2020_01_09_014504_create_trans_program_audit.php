<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransProgramAudit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_program_audit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tinjauan_id')->unsigned();
            $table->integer('status')->default(0)->comment('0:Draft,1:Approve SVP, 2:Final, 3:Historis');
            $table->integer('user_id')->unsigned();
            $table->text('ruang_lingkup')->nullable();
            $table->text('sasaran')->nullable();
            $table->text('ket_svp')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('tinjauan_id')->references('id')->on('trans_tinjauan_dokumen');
            $table->foreign('user_id')->references('id')->on('sys_users');
        });

        Schema::create('trans_program_audit_detail_anggota', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('program_id')->unsigned();
            $table->integer('fungsi')->default(0)->comment('1:Operasional,2:Keuangan, 3:Sistem');
            $table->integer('user_id')->unsigned();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('program_id')->references('id')->on('trans_program_audit')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('sys_users');
        });

        Schema::create('trans_program_audit_detail_jadwal', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('program_id')->unsigned();
            $table->text('keterangan')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('program_id')->references('id')->on('trans_program_audit')->onDelete('cascade');
        });

        Schema::create('trans_program_audit_detail_pel', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('program_id')->unsigned();
            $table->string('tgl', 200);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('program_id')->references('id')->on('trans_program_audit')->onDelete('cascade');
        });

        Schema::create('trans_program_audit_detail_pel_d', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pelaksanaan_detail_id')->unsigned();
            $table->string('mulai', 200);
            $table->string('selesai', 200);
             $table->text('keterangan')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('pelaksanaan_detail_id')->references('id')->on('trans_program_audit_detail_pel')->onDelete('cascade');
        });

        Schema::create('trans_program_audit_detail_selesai', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('program_id')->unsigned();
            $table->string('tgl_mulai', 200);
            $table->string('tgl_selesai', 200);
            $table->text('keterangan')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('program_id')->references('id')->on('trans_program_audit')->onDelete('cascade');
        });

        Schema::create('trans_program_audit_detail_kerja', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('program_id')->unsigned();
            $table->integer('fokus_audit_id')->unsigned();
            $table->integer('bidang')->default(1)->comment('1:Operasional,2:Keuangan, 3:Sistem');
            $table->string('rencana', 200);
            $table->string('realisasi', 200);
            $table->text('keterangan')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('program_id')->references('id')->on('trans_program_audit')->onDelete('cascade');
            $table->foreign('fokus_audit_id')->references('id')->on('ref_fokus_audit');
        });

        //LOG
        Schema::create('log_trans_program_audit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('tinjauan_id')->unsigned();
            $table->integer('status')->default(0)->comment('0:Draft,1:Approve SVP, 2:Final, 3:Historis');
            $table->integer('user_id')->unsigned();
            $table->text('ruang_lingkup')->nullable();
            $table->text('sasaran')->nullable();
            $table->text('ket_svp')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_program_audit_detail_anggota', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('program_id')->unsigned();
            $table->integer('fungsi')->default(0)->comment('1:Operasional,2:Keuangan, 3:Sistem');
            $table->integer('user_id')->unsigned();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_program_audit_detail_jadwal', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('program_id')->unsigned();
            $table->text('keterangan')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_program_audit_detail_pel', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('program_id')->unsigned();
            $table->string('tgl', 200);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_program_audit_detail_pel_d', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('pelaksanaan_detail_id')->unsigned();
            $table->string('mulai', 200);
            $table->string('selesai', 200);
            $table->text('keterangan')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_program_audit_detail_selesai', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('program_id')->unsigned();
            $table->string('tgl_mulai', 200);
            $table->string('tgl_selesai', 200);
            $table->text('keterangan')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_program_audit_detail_kerja', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('program_id')->unsigned();
            $table->integer('fokus_audit_id')->unsigned();
            $table->integer('bidang')->default(1)->comment('1:Operasional,2:Keuangan, 3:Sistem');
            $table->string('rencana', 200);
            $table->string('realisasi', 200);
            $table->text('keterangan')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_program_audit_detail_kerja');
        Schema::dropIfExists('log_trans_program_audit_detail_selesai');
        Schema::dropIfExists('log_trans_program_audit_detail_pel_d');
        Schema::dropIfExists('log_trans_program_audit_detail_pel');
        Schema::dropIfExists('log_trans_program_audit_detail_jadwal');
        Schema::dropIfExists('log_trans_program_audit_detail_anggota');
        Schema::dropIfExists('log_trans_program_audit');

        Schema::dropIfExists('trans_program_audit_detail_kerja');
        Schema::dropIfExists('trans_program_audit_detail_selesai');
        Schema::dropIfExists('trans_program_audit_detail_pel_d');
        Schema::dropIfExists('trans_program_audit_detail_pel');
        Schema::dropIfExists('trans_program_audit_detail_jadwal');
        Schema::dropIfExists('trans_program_audit_detail_anggota');
        Schema::dropIfExists('trans_program_audit');
    }
}
