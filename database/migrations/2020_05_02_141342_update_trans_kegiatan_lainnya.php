<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransKegiatanLainnya extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_kegiatan_lainnya', function (Blueprint $table) {
            $table->string('judul_audit', 255)->nullable()->after('ket_svp');
            $table->timestamp('tanggal_mulai')->nullable()->after('judul_audit');
            $table->timestamp('tanggal_selesai')->nullable()->after('tanggal_mulai');
        });

        Schema::table('log_trans_kegiatan_lainnya', function (Blueprint $table) {
            $table->string('judul_audit', 255)->nullable()->after('ket_svp');
            $table->timestamp('tanggal_mulai')->nullable()->after('judul_audit');
            $table->timestamp('tanggal_selesai')->nullable()->after('tanggal_mulai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_kegiatan_lainnya', function (Blueprint $table) {
            $table->dropColumn('judul_audit');
            $table->dropColumn('tanggal_mulai');
            $table->dropColumn('tanggal_selesai');
        });
        
        Schema::table('trans_kegiatan_lainnya', function (Blueprint $table) {
            $table->dropColumn('judul_audit');
            $table->dropColumn('tanggal_mulai');
            $table->dropColumn('tanggal_selesai');
        });
    }
}
