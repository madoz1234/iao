<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransProgramAuditPenyeseuaianParalelProses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_program_audit', function (Blueprint $table) {
            $table->integer('status_kka')->default(0)->comment('0:On Progress, 1:Close')->after('user_id');
        });

        Schema::table('log_trans_program_audit', function (Blueprint $table) {
            $table->integer('status_kka')->default(0)->comment('0:On Progress, 1:Close')->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {	
    	Schema::table('log_trans_program_audit', function (Blueprint $table) {
            $table->dropColumn('status_kka');
        });

        Schema::table('trans_program_audit', function (Blueprint $table) {
            $table->dropColumn('status_kka');
        });
    }
}
