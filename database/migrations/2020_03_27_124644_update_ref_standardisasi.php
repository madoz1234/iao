<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateRefStandardisasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_standarisasi_temuan', function (Blueprint $table) {
            $table->integer('bidang')->default(1)->comment('1:Operasional,2:Keuangan, 3:Sistem')->after('id');
        });

        Schema::table('log_ref_standarisasi_temuan', function (Blueprint $table) {
            $table->integer('bidang')->default(1)->comment('1:Operasional,2:Keuangan, 3:Sistem')->after('id');
        });

        Schema::table('ref_kategori_temuan', function (Blueprint $table) {
            $table->string('deskripsi', 50)->change()->nullable();
        });

        Schema::table('log_ref_kategori_temuan', function (Blueprint $table) {
            $table->string('deskripsi', 50)->change()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_ref_kategori_temuan', function (Blueprint $table) {
            $table->string('deskripsi', 50)->change();
        });

    	Schema::table('ref_kategori_temuan', function (Blueprint $table) {
            $table->string('deskripsi', 50)->change();
        });

    	Schema::table('log_ref_standarisasi_temuan', function (Blueprint $table) {
            $table->dropColumn('bidang');
        });

        Schema::table('ref_standarisasi_temuan', function (Blueprint $table) {
            $table->dropColumn('bidang');
        });
    }
}
