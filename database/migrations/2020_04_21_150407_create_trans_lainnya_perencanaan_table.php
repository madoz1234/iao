<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransLainnyaPerencanaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_lainnya_perencanaan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rencana_detail_id')->unsigned();
            $table->string('jenis_rapat', 30)->default('internal')->comment('internal||eksternal')->nullable();
            $table->integer('pimpinan_rapat')->unsigned()->nullable();
            $table->integer('notulen_rapat')->unsigned()->nullable();
            $table->timestamp('tanggal_rapat')->nullable();
            $table->tinyInteger('status')->comment('0:Baru, 1:Draft, 2:Waiting Approval SVP, 3: Completed');
            $table->text('ket_svp')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('rencana_detail_id')->references('id')->on('trans_rencana_audit_detail')->onDelete('cascade');
            $table->foreign('pimpinan_rapat')->references('id')->on('sys_users')->onDelete('cascade');
            $table->foreign('notulen_rapat')->references('id')->on('sys_users')->onDelete('cascade');
        });
        
        Schema::create('log_trans_lainnya_perencanaan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('rencana_detail_id')->unsigned();
            $table->string('jenis_rapat', 30)->default('internal')->comment('internal||eksternal')->nullable();
            $table->integer('pimpinan_rapat')->unsigned()->nullable();
            $table->integer('notulen_rapat')->unsigned()->nullable();
            $table->timestamp('tanggal_rapat')->nullable();
            $table->tinyInteger('status')->default(0)->comment('0:Baru, 1:Draft, 2:Waiting Approval SVP, 3: Completed');
            $table->text('ket_svp')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_lainnya_perencanaan');
        Schema::dropIfExists('trans_lainnya_perencanaan');
    }
}
