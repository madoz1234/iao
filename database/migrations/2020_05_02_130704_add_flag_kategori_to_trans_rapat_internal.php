<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFlagKategoriToTransRapatInternal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_rapat_internal', function (Blueprint $table) {
            $table->tinyInteger('status_kategori')->default(0)->after('status_rapat')->comment('1: Umum, 2:Konsultasi');
        });

        Schema::table('log_trans_rapat_internal', function (Blueprint $table) {
            $table->tinyInteger('status_kategori')->default(0)->after('status_rapat')->comment('1: Umum, 2:Konsultasi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_rapat_internal', function (Blueprint $table) {
            $table->dropColumn('status_kategori');
        });
        
        Schema::table('log_trans_rapat_internal', function (Blueprint $table) {
            $table->dropColumn('status_kategori');
        });
    }
}