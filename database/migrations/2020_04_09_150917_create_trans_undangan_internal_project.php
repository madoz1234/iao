<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransUndanganInternalProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_rapat_internal', function (Blueprint $table) {
            $table->integer('status_rapat')->default(0)->comment('0:Belum Selesai, 1:Selesai')->after('flag');
        });

        Schema::table('log_trans_rapat_internal', function (Blueprint $table) {
            $table->integer('status_rapat')->default(0)->comment('0:Belum Selesai, 1:Selesai')->after('flag');
        });

        Schema::table('trans_rapat_internal_undangan_peserta', function (Blueprint $table) {
            $table->dropColumn('project_id');
        });

        Schema::table('log_trans_rapat_internal_undangan_peserta', function (Blueprint $table) {
            $table->dropColumn('project_id');
        });

        Schema::create('trans_undangan_internal_project', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('rapat_id')->unsigned();
            $table->integer('project_id')->default(0)->unsigned();

            $table->foreign('rapat_id')->references('id')->on('trans_rapat_internal')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_rapat_internal', function (Blueprint $table) {
            $table->dropColumn('status_rapat');
        });

        Schema::table('log_trans_rapat_internal', function (Blueprint $table) {
            $table->dropColumn('status_rapat');
        });

        Schema::table('trans_rapat_internal_undangan_peserta', function (Blueprint $table) {
            $table->integer('project_id')->default(0)->unsigned();
        });

        Schema::table('log_trans_rapat_internal_undangan_peserta', function (Blueprint $table) {
            $table->integer('project_id')->default(0)->unsigned();
        });

        Schema::dropIfExists('trans_undangan_internal_project');
    }
}