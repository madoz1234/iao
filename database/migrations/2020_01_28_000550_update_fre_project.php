<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateFreProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_project', function (Blueprint $table) {
            $table->string('project_id', 200)->nullable()->after('project_ab');
        });

        Schema::table('log_ref_project', function (Blueprint $table) {
            $table->string('project_id', 200)->nullable()->after('project_ab');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_project', function (Blueprint $table) {
            $table->dropColumn('project_id');
        });

        Schema::table('log_ref_project', function (Blueprint $table) {
            $table->dropColumn('project_id');
        });
    }
}
