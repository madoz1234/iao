<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UbahTransDraftKkaDetailAddLampiran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_draft_kka_detail', function (Blueprint $table) {
            $table->dropForeign(['temuan_id']);
            $table->dropColumn('temuan_id');
            $table->dropColumn('kriteria');

            $table->integer('kriteria_id')->default(1)->unsigned()->after('standarisasi_id');
        });

        Schema::table('log_trans_draft_kka_detail', function (Blueprint $table) {
            $table->dropColumn('temuan_id');
            $table->dropColumn('kriteria');
            $table->integer('kriteria_id')->default(1)->unsigned()->after('standarisasi_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {	
    	Schema::table('log_trans_draft_kka_detail', function (Blueprint $table) {
            $table->dropColumn('kriteria_id');
            $table->integer('temuan_id')->unsigned()->default(1)->after('standarisasi_id');
            $table->text('kriteria')->nullable()->after('catatan_kondisi');
        });

        Schema::table('trans_draft_kka_detail', function (Blueprint $table) {
            $table->dropColumn('kriteria_id');
            $table->integer('temuan_id')->unsigned()->default(1)->after('standarisasi_id');
            $table->text('kriteria')->nullable()->after('catatan_kondisi');
            $table->foreign('temuan_id')->references('id')->on('ref_temuan_detail')->onDelete('cascade');
        });
    }
}
