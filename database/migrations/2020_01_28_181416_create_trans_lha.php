<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransLha extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_lha', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('draft_id')->unsigned();
            $table->string('tanggal', 255)->nullable();
            $table->string('memo', 255)->nullable();
            $table->tinyInteger('status')->default(0);
            $table->text('ket_svp')->nullable();
            $table->text('ket_dirut')->nullable();
            $table->string('bukti_surat', 255)->nullable();
            $table->string('filename_surat', 255)->nullable();
            $table->string('bukti_petunjuk', 255)->nullable();
            $table->string('filename_petunjuk', 255)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
            $table->foreign('draft_id')->references('id')->on('trans_draft_kka')->onDelete('cascade');
        });

        Schema::create('log_trans_lha', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('draft_id')->unsigned();
            $table->string('tanggal', 255)->nullable();
            $table->string('memo', 255)->nullable();
            $table->tinyInteger('status')->default(0);
            $table->text('ket_svp')->nullable();
            $table->text('ket_dirut')->nullable();
            $table->string('bukti_surat', 255)->nullable();
            $table->string('filename_surat', 255)->nullable();
            $table->string('bukti_petunjuk', 255)->nullable();
            $table->string('filename_petunjuk', 255)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_lha');
        Schema::dropIfExists('trans_lha');
    }
}
