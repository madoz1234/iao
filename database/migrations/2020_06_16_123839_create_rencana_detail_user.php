<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRencanaDetailUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('log_trans_rencana_audit_detail', function (Blueprint $table) {
            $table->dropColumn('user_operasional');
            $table->dropColumn('user_keuangan');
            $table->dropColumn('user_sistem');
        });

        Schema::table('trans_rencana_audit_detail', function (Blueprint $table) {
        	$table->dropForeign(['user_operasional']);
        	$table->dropForeign(['user_keuangan']);
        	$table->dropForeign(['user_sistem']);
        	
            $table->dropColumn('user_operasional');
            $table->dropColumn('user_keuangan');
            $table->dropColumn('user_sistem');
        });

        Schema::create('trans_rencana_audit_detail_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('detail_id')->unsigned();
            $table->integer('user_id')->unsigned()->default(1);
            $table->integer('tipe')->default(0)->comment('0:Operasional, 1:Keuangan, 2:Sistem');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('user_id')->references('id')->on('sys_users')->onDelete('cascade');
        });

        Schema::create('log_trans_rencana_audit_detail_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('detail_id')->unsigned();
            $table->integer('user_id')->unsigned()->default(1);
            $table->integer('tipe')->default(0)->comment('0:Operasional, 1:Keuangan, 2:Sistem');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('trans_rencana_audit_detail', function (Blueprint $table) {
        	$table->integer('user_operasional')->unsigned()->default(1)->after('status_lain');
        	$table->integer('user_keuangan')->unsigned()->default(1)->after('user_operasional');
        	$table->integer('user_sistem')->unsigned()->default(1)->after('user_keuangan');

        	$table->foreign('user_operasional')->references('id')->on('sys_users');
        	$table->foreign('user_keuangan')->references('id')->on('sys_users');
        	$table->foreign('user_sistem')->references('id')->on('sys_users');
        });

        Schema::table('log_trans_rencana_audit_detail', function (Blueprint $table) {
        	$table->integer('user_operasional')->unsigned()->default(1)->after('status_lain');
        	$table->integer('user_keuangan')->unsigned()->default(1)->after('user_operasional');
        	$table->integer('user_sistem')->unsigned()->default(1)->after('user_keuangan');
        });

        Schema::dropIfExists('log_trans_rencana_audit_detail_user');
        Schema::dropIfExists('trans_rencana_audit_detail_user');
    }
}
