<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUndanganId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_undangan_internal_project', function (Blueprint $table) {
            $table->dropForeign(['rapat_id']);
            $table->dropColumn('rapat_id');

            $table->BigInteger('undangan_id')->unsigned()->nullable()->after('id');
            $table->foreign('undangan_id')->references('id')->on('trans_rapat_internal_undangan_peserta')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_undangan_internal_project', function (Blueprint $table) {
            $table->integer('rapat_id')->unsigned();
            $table->foreign('rapat_id')->references('id')->on('trans_rapat_internal')->onDelete('cascade');

            $table->dropForeign(['undangan_id']);
            $table->dropColumn('undangan_id');
        });
    }
}