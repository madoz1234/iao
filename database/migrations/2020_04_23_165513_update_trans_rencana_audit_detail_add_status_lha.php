<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransRencanaAuditDetailAddStatusLha extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_rencana_audit_detail', function (Blueprint $table) {
            $table->integer('status_lha')->default(0)->comment('0:On Progress, 1:Close')->after('user_sistem');
        });

        Schema::table('log_trans_rencana_audit_detail', function (Blueprint $table) {
            $table->integer('status_lha')->default(0)->comment('0:On Progress, 1:Close')->after('user_sistem');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_rencana_audit_detail', function (Blueprint $table) {
            $table->dropColumn('status_lha');
        });

        Schema::table('trans_rencana_audit_detail', function (Blueprint $table) {
            $table->dropColumn('status_lha');
        });
    }
}
