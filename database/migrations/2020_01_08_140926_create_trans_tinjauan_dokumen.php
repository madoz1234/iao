<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransTinjauanDokumen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_tinjauan_dokumen', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('penugasan_id')->unsigned();
            $table->string('nomor', 255)->nullable();
            $table->string('tempat', 255)->nullable();
            $table->string('tanggal', 255)->nullable();
            $table->string('judul', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('dateline', 255)->nullable();
            $table->integer('status')->default(0)->comment('0:Draft,1:Approve SVP, 2:Final, 3:Historis');
            $table->string('bukti', 255)->nullable();
            $table->string('filename', 255)->nullable();
            $table->text('ket_svp')->nullable();
            $table->integer('status_program')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
            $table->foreign('penugasan_id')->references('id')->on('trans_penugasan_audit')->onDelete('cascade');
        });

        Schema::create('trans_tinjauan_dokumen_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tinjauan_id')->unsigned();
            $table->integer('dokumen_id')->unsigned();
            $table->tinyInteger('status')->default(0)->comment('0:Tidak Penting,1:Penting');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
            $table->foreign('tinjauan_id')->references('id')->on('trans_tinjauan_dokumen')->onDelete('cascade');
            $table->foreign('dokumen_id')->references('id')->on('ref_dokumen');
        });

        Schema::create('trans_tinjauan_dokumen_penerima', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tinjauan_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('nama', 255)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
            $table->foreign('tinjauan_id')->references('id')->on('trans_tinjauan_dokumen')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('sys_users');
        });

        Schema::create('trans_tinjauan_dokumen_tembusan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tinjauan_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->text('tembusan')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
            $table->foreign('tinjauan_id')->references('id')->on('trans_tinjauan_dokumen')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('sys_users');
        });


        //LOG
        Schema::create('log_trans_tinjauan_dokumen', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('penugasan_id')->unsigned();
            $table->string('nomor', 255)->nullable();
            $table->string('tempat', 255)->nullable();
            $table->string('tanggal', 255)->nullable();
            $table->string('judul', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('dateline', 255)->nullable();
            $table->integer('status')->default(0)->comment('0:Draft,1:Approve SVP, 2:Final, 3:Historis');
            $table->string('bukti', 255)->nullable();
            $table->string('filename', 255)->nullable();
            $table->text('ket_svp')->nullable();
            $table->integer('status_program')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_tinjauan_dokumen_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('tinjauan_id')->unsigned();
            $table->integer('dokumen_id')->unsigned();
            $table->tinyInteger('status')->default(0)->comment('0:Tidak Penting,1:Penting');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_tinjauan_dokumen_penerima', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('tinjauan_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('nama', 255)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_tinjauan_dokumen_tembusan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('tinjauan_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->text('tembusan')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::table('trans_penugasan_audit', function (Blueprint $table) {
            $table->integer('status_tinjauan')->default(0)->after('group');
        });

        Schema::table('log_trans_penugasan_audit', function (Blueprint $table) {
            $table->integer('status_tinjauan')->default(0)->after('group');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('log_trans_penugasan_audit', function (Blueprint $table) {
    		$table->dropColumn('status_tinjauan');
        });
        Schema::table('trans_penugasan_audit', function (Blueprint $table) {
    		$table->dropColumn('status_tinjauan');
        }); 
        
        Schema::dropIfExists('log_trans_tinjauan_dokumen_tembusan');
        Schema::dropIfExists('log_trans_tinjauan_dokumen_penerima');
        Schema::dropIfExists('log_trans_tinjauan_dokumen_detail');
        Schema::dropIfExists('log_trans_tinjauan_dokumen');

        Schema::dropIfExists('trans_tinjauan_dokumen_tembusan');
        Schema::dropIfExists('trans_tinjauan_dokumen_penerima');
        Schema::dropIfExists('trans_tinjauan_dokumen_detail');
        Schema::dropIfExists('trans_tinjauan_dokumen');
    }
}
