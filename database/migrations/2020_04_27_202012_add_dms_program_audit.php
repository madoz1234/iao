<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDmsProgramAudit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_program_audit', function (Blueprint $table) {
            $table->integer('dms')->default(0);
            $table->string('dms_parent_id')->nullable();
            $table->integer('dms_type')->default(144);
            $table->dateTime('dms_uploaded_date')->nullable();
            $table->string('dms_file_id')->nullable();
            $table->integer('dms_update')->default(0);
            $table->integer('dms_multiple')->default(0);
            $table->integer('dms_object_id')->default(0);
            $table->string('dms_object_type')->nullable();
        });

        Schema::table('log_trans_program_audit', function (Blueprint $table) {
            $table->integer('dms')->default(0);
            $table->string('dms_parent_id')->nullable();
            $table->integer('dms_type')->default(144);
            $table->dateTime('dms_uploaded_date')->nullable();
            $table->string('dms_file_id')->nullable();
            $table->integer('dms_update')->default(0);
            $table->integer('dms_multiple')->default(0);
            $table->integer('dms_object_id')->default(0);
            $table->string('dms_object_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_program_audit', function (Blueprint $table) {
            $table->dropColumn(['dms', 'dms_parent_id', 'dms_type', 'dms_uploaded_date', 'dms_file_id', 'dms_update', 'dms_multiple', 'dms_object_id', 'dms_object_type']);
        });

        Schema::table('trans_program_audit', function (Blueprint $table) {
            $table->dropColumn(['dms', 'dms_parent_id', 'dms_type', 'dms_uploaded_date', 'dms_file_id', 'dms_update', 'dms_multiple', 'dms_object_id', 'dms_object_type']);
        });
    }
}
