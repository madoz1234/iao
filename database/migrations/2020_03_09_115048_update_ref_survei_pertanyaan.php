<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateRefSurveiPertanyaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_survei_pertanyaan', function (Blueprint $table) {
            $table->dropColumn(['jenis', 'sum']);
        });

        Schema::table('log_ref_survei_pertanyaan', function (Blueprint $table) {
            $table->dropColumn(['jenis', 'sum']);
        });

        Schema::dropIfExists('ref_survei_pilgan');
        Schema::dropIfExists('log_ref_survei_pilgan');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_survei_pertanyaan', function (Blueprint $table) {
            $table->integer('jenis')->default(1)->comment('1:Rating,2:Setuju/Tidak, 3:Essai,4:PG');
            $table->integer('sum')->default(0);
        });
        
        Schema::table('log_ref_survei_pertanyaan', function (Blueprint $table) {
            $table->integer('jenis')->default(1)->comment('1:Rating,2:Setuju/Tidak, 3:Essai,4:PG');
            $table->integer('sum')->default(0);
        });

        Schema::create('ref_survei_pilgan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tanya_id')->unsigned();
            $table->string('pilgan', 10);
            $table->text('isi');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('tanya_id')->references('id')->on('ref_survei_pertanyaan')->onDelete('cascade');
        });

        Schema::create('log_ref_survei_pilgan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('tanya_id')->unsigned();
            $table->string('pilgan', 10);
            $table->text('isi');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

    }
}
