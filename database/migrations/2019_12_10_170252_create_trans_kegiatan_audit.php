<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransKegiatanAudit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_rencana_audit', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tahun', 200);
            $table->integer('revisi')->default(0);
            $table->integer('tipe')->default(0)->comment('0:PKIA,1:Khusus');
            $table->integer('status')->default(0)->comment('0:Draft,1:Approve SVP, 2:Approve Dirut, 3:Final, 4:Historis');
            $table->string('bukti', 255)->nullable();
            $table->string('filename', 255)->nullable();
            $table->text('ket_svp')->nullable();
            $table->text('ket_dirut')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('trans_rencana_audit_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rencana_id')->unsigned();
            $table->integer('tipe_object')->default(0)->comment('0:Business Unit (BU),1:Corporate Office (CO), 2:Project, 3:Anak Perusahaan');
            $table->integer('object_id')->default(0);
            $table->string('rencana', 200);
            $table->integer('keterangan')->default(0)->comment('0:Tidak Dipilih,1:Dipilih');
            $table->string('tgl_mulai', 200)->nullable();
            $table->string('tgl_selesai', 200)->nullable();
            $table->integer('jenis')->default(0)->comment('0:Kontrak Baru,1:Sisa Nilai Kontrak');
            $table->decimal('nilai_kontrak', 20, 2)->default(0);
            $table->decimal('snk', 20, 2)->default(0);
            $table->decimal('progress', 20, 2)->default(0);
            $table->decimal('progress_ra', 20, 2)->default(0);
            $table->decimal('progress_ri', 20, 2)->default(0);
            $table->decimal('deviasi', 20, 2)->default(0);
            $table->integer('risk_impact')->default(0)->comment('0:Sangat Ringan,1:Ringan, 2:Sedang, 3:Berat, 4:Sangat Berat');
            $table->decimal('mapp', 20, 2)->default(0);
            $table->decimal('bkpu', 20, 2)->default(0);
            $table->decimal('deviasi_bkpu', 20, 2)->default(0);
            $table->integer('bkpu_ri')->default(0)->comment('0:Sangat Ringan,1:Ringan, 2:Sedang, 3:Berat, 4:Sangat Berat');
            $table->integer('status_penugasan')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('rencana_id')->references('id')->on('trans_rencana_audit')->onDelete('cascade');
        });

        Schema::create('log_trans_rencana_audit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->string('tahun', 200);
            $table->integer('revisi')->default(0);
            $table->integer('tipe')->default(0)->comment('0:PKIA,1:Khusus');
            $table->integer('status')->default(0)->comment('0:Draft,1:Approve SVP, 2:Approve Dirut, 3:Final, 4:Historis');
            $table->string('bukti', 255)->nullable();
            $table->string('filename', 255)->nullable();
            $table->text('ket_svp')->nullable();
            $table->text('ket_dirut')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_rencana_audit_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('rencana_id')->unsigned();
            $table->integer('tipe_object')->default(0)->comment('0:Business Unit (BU),1:Corporate Office (CO), 2:Project, 3:Anak Perusahaan');
            $table->integer('object_id')->default(0);
            $table->string('rencana', 200);
            $table->integer('keterangan')->default(0)->comment('0:Tidak Dipilih,1:Dipilih');
            $table->string('tgl_mulai', 200)->nullable();
            $table->string('tgl_selesai', 200)->nullable();
            $table->integer('jenis')->default(0)->comment('0:Kontrak Baru,1:Sisa Nilai Kontrak');
            $table->decimal('nilai_kontrak', 20, 2)->default(0);
            $table->decimal('snk', 20, 2)->default(0);
            $table->decimal('progress', 20, 2)->default(0);
            $table->decimal('progress_ra', 20, 2)->default(0);
            $table->decimal('progress_ri', 20, 2)->default(0);
            $table->decimal('deviasi', 20, 2)->default(0);
            $table->integer('risk_impact')->default(0)->comment('0:Sangat Ringan,1:Ringan, 2:Sedang, 3:Berat, 4:Sangat Berat');
            $table->decimal('mapp', 20, 2)->default(0);
            $table->decimal('bkpu', 20, 2)->default(0);
            $table->decimal('deviasi_bkpu', 20, 2)->default(0);
            $table->integer('bkpu_ri')->default(0)->comment('0:Sangat Ringan,1:Ringan, 2:Sedang, 3:Berat, 4:Sangat Berat');
            $table->integer('status_penugasan')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_rencana_audit_detail');
        Schema::dropIfExists('log_trans_rencana_audit');
        Schema::dropIfExists('trans_rencana_audit_detail');
        Schema::dropIfExists('trans_rencana_audit');
    }
}
