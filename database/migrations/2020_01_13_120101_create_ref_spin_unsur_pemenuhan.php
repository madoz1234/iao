<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefSpinUnsurPemenuhan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_spin_unsur_pemenuhan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pof_id')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('pof_id')->references('id')->on('ref_spin_pof')->onDelete('cascade');
        });

        Schema::create('log_ref_spin_unsur_pemenuhan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pof_id');
            $table->integer('ref_id')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_spin_unsur_pemenuhan');
        Schema::dropIfExists('log_ref_spin_unsur_pemenuhan');
    }
}