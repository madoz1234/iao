<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransLhaAddLingkupDanAdendum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_lha', function (Blueprint $table) {
            $table->text('pemilik')->nullable()->after('memo');
            $table->string('sumber')->nullable()->after('pemilik');
            $table->string('sifat')->nullable()->after('sumber');
            $table->string('nomor_spmk')->nullable()->after('sifat');
            $table->string('nilai_kontrak')->nullable()->after('nomor_spmk');
            $table->string('tanggal_kontrak')->nullable()->after('nilai_kontrak');
            $table->string('tanggal_akhir_kontrak')->nullable()->after('tanggal_kontrak');
            $table->string('waktu_pelaksanaaan')->nullable()->after('tanggal_akhir_kontrak');
            $table->string('waktu_pemeliharaan')->nullable()->after('waktu_pelaksanaaan');
            $table->string('pembayaran')->nullable()->after('waktu_pemeliharaan');
            $table->string('konsultan_perencana')->nullable()->after('pembayaran');
            $table->string('konsultan_pengawas')->nullable()->after('konsultan_perencana');
            $table->text('bkpu')->nullable()->after('konsultan_pengawas');
            $table->text('progress')->nullable()->after('bkpu');
            $table->text('pu')->nullable()->after('progress');
            $table->text('cash')->nullable()->after('pu');
            $table->text('piutang')->nullable()->after('cash');
            $table->text('tagihan')->nullable()->after('piutang');
        });

        Schema::table('log_trans_lha', function (Blueprint $table) {
            $table->text('pemilik')->nullable()->after('memo');
            $table->string('sumber')->nullable()->after('pemilik');
            $table->string('sifat')->nullable()->after('sumber');
            $table->string('nomor_spmk')->nullable()->after('sifat');
            $table->string('nilai_kontrak')->nullable()->after('nomor_spmk');
            $table->string('tanggal_kontrak')->nullable()->after('nilai_kontrak');
            $table->string('tanggal_akhir_kontrak')->nullable()->after('tanggal_kontrak');
            $table->string('waktu_pelaksanaaan')->nullable()->after('tanggal_akhir_kontrak');
            $table->string('waktu_pemeliharaan')->nullable()->after('waktu_pelaksanaaan');
            $table->string('pembayaran')->nullable()->after('waktu_pemeliharaan');
            $table->string('konsultan_perencana')->nullable()->after('pembayaran');
            $table->string('konsultan_pengawas')->nullable()->after('konsultan_perencana');
            $table->text('bkpu')->nullable()->after('konsultan_pengawas');
            $table->text('progress')->nullable()->after('bkpu');
            $table->text('pu')->nullable()->after('progress');
            $table->text('cash')->nullable()->after('pu');
            $table->text('piutang')->nullable()->after('cash');
            $table->text('tagihan')->nullable()->after('piutang');
        });

        Schema::create('trans_lha_adendum', function (Blueprint $table) {
        	$table->increments('id');
            $table->integer('lha_id')->unsigned();
            $table->string('no_kontrak')->nullable();
            $table->string('nilai')->nullable();
            $table->string('tgl_awal')->nullable();
            $table->string('tgl_akhir')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('lha_id')->references('id')->on('trans_lha')->onDelete('cascade');
        });

        Schema::create('trans_lha_lingkup', function (Blueprint $table) {
        	$table->increments('id');
            $table->integer('lha_id')->unsigned();
            $table->string('uraian')->nullable();
            $table->string('kontrak')->nullable();
            $table->string('rencana')->nullable();
            $table->string('realisasi')->nullable();

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('lha_id')->references('id')->on('trans_lha')->onDelete('cascade');
        });

        Schema::create('log_trans_lha_adendum', function (Blueprint $table) {
        	$table->increments('id');
        	$table->integer('ref_id')->unsigned();
            $table->integer('lha_id')->unsigned();
            $table->string('no_kontrak')->nullable();
            $table->string('nilai')->nullable();
            $table->string('tgl_awal')->nullable();
            $table->string('tgl_akhir')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_lha_lingkup', function (Blueprint $table) {
        	$table->increments('id');
        	$table->integer('ref_id')->unsigned();
            $table->integer('lha_id')->unsigned();
            $table->string('uraian')->nullable();
            $table->string('kontrak')->nullable();
            $table->string('rencana')->nullable();
            $table->string('realisasi')->nullable();
            
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('log_trans_lha', function (Blueprint $table) {
            $table->dropColumn('pemilik');
            $table->dropColumn('sumber');
            $table->dropColumn('sifat');
            $table->dropColumn('nomor_spmk');
            $table->dropColumn('nilai_kontrak');
            $table->dropColumn('tanggal_kontrak');
            $table->dropColumn('tanggal_akhir_kontrak');
            $table->dropColumn('waktu_pelaksanaaan');
            $table->dropColumn('waktu_pemeliharaan');
            $table->dropColumn('pembayaran');
            $table->dropColumn('konsultan_perencana');
            $table->dropColumn('konsultan_pengawas');
            $table->dropColumn('bkpu');
            $table->dropColumn('progress');
            $table->dropColumn('pu');
            $table->dropColumn('cash');
            $table->dropColumn('piutang');
            $table->dropColumn('tagihan');
        });

        Schema::table('trans_lha', function (Blueprint $table) {
            $table->dropColumn('pemilik');
            $table->dropColumn('sumber');
            $table->dropColumn('sifat');
            $table->dropColumn('nomor_spmk');
            $table->dropColumn('nilai_kontrak');
            $table->dropColumn('tanggal_kontrak');
            $table->dropColumn('tanggal_akhir_kontrak');
            $table->dropColumn('waktu_pelaksanaaan');
            $table->dropColumn('waktu_pemeliharaan');
            $table->dropColumn('pembayaran');
            $table->dropColumn('konsultan_perencana');
            $table->dropColumn('konsultan_pengawas');
            $table->dropColumn('bkpu');
            $table->dropColumn('progress');
            $table->dropColumn('pu');
            $table->dropColumn('cash');
            $table->dropColumn('piutang');
            $table->dropColumn('tagihan');
        });

        Schema::dropIfExists('log_trans_lha_lingkup');
        Schema::dropIfExists('log_trans_lha_adendum');

        Schema::dropIfExists('trans_lha_lingkup');
        Schema::dropIfExists('trans_lha_adendum');
    }
}
