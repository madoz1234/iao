<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransLaporanTlDanUpdateDetilKka extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_draft_kka_detail', function (Blueprint $table) {
            $table->text('tindak_lanjut')->nullable()->after('tgl');
            $table->integer('status_tl')->default(1)->after('tindak_lanjut')->comment('1:Sudah, 2:Belum, 3:Dalam Proses');
        });

        Schema::table('log_trans_draft_kka_detail', function (Blueprint $table) {
            $table->text('tindak_lanjut')->nullable()->after('tgl');
            $table->integer('status_tl')->default(1)->after('tindak_lanjut')->comment('1:Sudah, 2:Belum, 3:Dalam Proses');
        });

        Schema::create('trans_laporan_tl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('surat_id')->unsigned();
            $table->integer('status')->default(0);
            $table->text('ket_svp')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();

            $table->foreign('surat_id')->references('id')->on('trans_surat_perintah_tl')->onDelete('cascade');
        });

        Schema::create('log_trans_laporan_tl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('surat_id')->unsigned();
            $table->integer('status')->default(0);
            $table->text('ket_svp')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_draft_kka_detail', function (Blueprint $table) {
            $table->dropColumn('status_tl');
            $table->dropColumn('tindak_lanjut');
        });

        Schema::table('trans_draft_kka_detail', function (Blueprint $table) {
            $table->dropColumn('status_tl');
            $table->dropColumn('tindak_lanjut');
        });

        Schema::dropIfExists('log_trans_laporan_tl');
        Schema::dropIfExists('trans_laporan_tl');
    }
}
