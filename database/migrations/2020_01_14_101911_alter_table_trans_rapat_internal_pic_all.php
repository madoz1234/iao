<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableTransRapatInternalPicAll extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_rapat_internal_risalah', function (Blueprint $table) {
            $table->integer('pic_1_id')->unsigned()->nullable();
            $table->integer('pic_2_id')->unsigned()->nullable();
            $table->integer('pic_3_id')->unsigned()->nullable();

            $table->foreign('pic_1_id')->references('id')->on('sys_users');
            $table->foreign('pic_2_id')->references('id')->on('sys_users');
            $table->foreign('pic_3_id')->references('id')->on('sys_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_rapat_internal_risalah', function (Blueprint $table) {
            $table->dropForeign(['pic_1_id']);
            $table->dropForeign(['pic_2_id']);
            $table->dropForeign(['pic_3_id']);
            
            $table->dropColumn('pic_1_id');
            $table->dropColumn('pic_2_id');
            $table->dropColumn('pic_3_id');
        });
    }
}
