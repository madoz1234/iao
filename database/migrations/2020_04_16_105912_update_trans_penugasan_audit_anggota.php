<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransPenugasanAuditAnggota extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_penugasan_audit_detail_anggota', function (Blueprint $table) {
            $table->integer('data_id')->default(1)->after('fungsi');
        });

        Schema::table('log_trans_penugasan_audit_detail_anggota', function (Blueprint $table) {
            $table->integer('data_id')->default(1)->after('fungsi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('log_trans_penugasan_audit_detail_anggota', function (Blueprint $table) {
            $table->dropColumn('data_id');
        });

        Schema::table('trans_penugasan_audit_detail_anggota', function (Blueprint $table) {
            $table->dropColumn('data_id');
        });
    }
}
