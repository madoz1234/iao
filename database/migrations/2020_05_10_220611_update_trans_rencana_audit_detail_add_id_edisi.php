<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransRencanaAuditDetailAddIdEdisi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_rencana_audit_detail', function (Blueprint $table) {
            $table->integer('edisi_id')->unsigned()->default(1)->after('lain_id');
        });

        Schema::table('log_trans_rencana_audit_detail', function (Blueprint $table) {
            $table->integer('edisi_id')->unsigned()->default(1)->after('lain_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('log_trans_rencana_audit_detail', function (Blueprint $table) {
            $table->dropColumn('edisi_id');
        });

        Schema::table('trans_rencana_audit_detail', function (Blueprint $table) {
            $table->dropColumn('edisi_id');
        });
    }
}
