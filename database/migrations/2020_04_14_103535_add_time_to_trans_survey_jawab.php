<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTimeToTransSurveyJawab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_survei_jawab', function (Blueprint $table) {
            $table->time('timer')->default('00:20:00')->after('status');
        });
        Schema::table('log_trans_survei_jawab', function (Blueprint $table) {
            $table->time('timer')->default('00:20:00')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_survei_jawab', function (Blueprint $table) {
            $table->dropColumn('timer');
        });
        Schema::table('log_trans_survei_jawab', function (Blueprint $table) {
            $table->dropColumn('timer');
        });
    }
}
