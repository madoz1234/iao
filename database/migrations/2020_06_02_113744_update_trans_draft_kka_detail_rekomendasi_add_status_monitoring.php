<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransDraftKkaDetailRekomendasiAddStatusMonitoring extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_draft_kka_detail_rekomendasi', function (Blueprint $table) {
            $table->integer('status_monitoring')->default(0)->comment('0:On Progress, 1:Monitoring')->after('rekomendasi');
        });

        Schema::table('trans_register_tl_detail', function (Blueprint $table) {
            $table->integer('status_monitoring')->default(0)->comment('0:On Progress, 1:Monitoring')->after('stage');
        });

        Schema::table('log_trans_draft_kka_detail_rekomendasi', function (Blueprint $table) {
            $table->integer('status_monitoring')->default(0)->comment('0:On Progress, 1:Monitoring')->after('rekomendasi');
        });

        Schema::table('log_trans_register_tl_detail', function (Blueprint $table) {
            $table->integer('status_monitoring')->default(0)->comment('0:On Progress, 1:Monitoring')->after('stage');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('log_trans_draft_kka_detail_rekomendasi', function (Blueprint $table) {
            $table->dropColumn('status_monitoring');
        });

        Schema::table('log_trans_register_tl_detail', function (Blueprint $table) {
            $table->dropColumn('status_monitoring');
        });

        Schema::table('trans_draft_kka_detail_rekomendasi', function (Blueprint $table) {
            $table->dropColumn('status_monitoring');
        });

        Schema::table('trans_register_tl_detail', function (Blueprint $table) {
            $table->dropColumn('status_monitoring');
        });
    }
}
