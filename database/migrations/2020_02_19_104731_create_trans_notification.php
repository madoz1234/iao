<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_notification', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('parent_id')->unsigned();
            $table->text('url');
            $table->text('modul');
            $table->integer('status')->default(1)->comment('1:Aktif, 2:Tidak Aktif');
            $table->integer('stage')->default(1);
            $table->text('keterangan');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('sys_users')->onDelete('cascade');
        });

        Schema::create('log_trans_notification', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('parent_id')->unsigned();
            $table->text('url');
            $table->text('modul');
            $table->integer('status')->default(1)->comment('1:Aktif, 2:Tidak Aktif');
            $table->integer('stage')->default(1);
            $table->text('keterangan');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_notification');
        Schema::dropIfExists('trans_notification');
    }
}
