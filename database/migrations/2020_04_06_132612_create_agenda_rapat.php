<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgendaRapat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_rapat_internal', function (Blueprint $table) {
            $table->dropColumn('materi');
            $table->dropColumn('waktu');
            $table->text('keterangan')->nullable()->change();
            $table->string('jam_mulai', 255)->after('tanggal')->nullable();
            $table->string('jam_selesai', 255)->after('jam_mulai')->nullable();
        });

        Schema::table('log_trans_rapat_internal', function (Blueprint $table) {
            $table->dropColumn('materi');
            $table->dropColumn('waktu');
            $table->text('keterangan')->nullable()->change();
            $table->string('jam_mulai', 255)->after('tanggal')->nullable();
            $table->string('jam_selesai', 255)->after('jam_mulai')->nullable();
        });

        Schema::create('trans_rapat_internal_agenda', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('rapat_id')->unsigned();
            $table->text('agenda')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('rapat_id')->references('id')->on('trans_rapat_internal')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_rapat_internal', function (Blueprint $table) {
            $table->string('materi', 255)->nullable();
            $table->string('waktu', 255)->nullable();
            $table->text('keterangan')->change();
            $table->dropColumn('jam_mulai');
            $table->dropColumn('jam_selesai');
        });

        Schema::table('log_trans_rapat_internal', function (Blueprint $table) {
            $table->string('materi', 255)->nullable();
            $table->string('waktu', 255)->nullable();
            $table->text('keterangan')->change();
            $table->dropColumn('jam_mulai');
            $table->dropColumn('jam_selesai');
        });

        Schema::dropIfExists('trans_rapat_internal_agenda');
    }
}