<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefEdisi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_edisi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('edisi', 200)->nullable();
            $table->string('revisi', 200);
            $table->integer('status')->default(1)->comment('1:Aktif,2:Nonaktif');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_edisi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->string('edisi', 200)->nullable();
            $table->string('revisi', 200);
            $table->integer('status')->default(1)->comment('1:Aktif,2:Nonaktif');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_ref_edisi');
        Schema::dropIfExists('ref_edisi');
    }
}
