<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefElemen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_elemen', function (Blueprint $table) {
            $table->increments('id');
            $table->text('elemen')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('ref_kpa', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('elemen_id')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
            $table->foreign('elemen_id')->references('id')->on('ref_elemen')->onDelete('cascade');
        });

        Schema::create('ref_kpa_level', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kpa_id')->unsigned();
            $table->integer('level')->default(1)->comment('1 Dst');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
            $table->foreign('kpa_id')->references('id')->on('ref_kpa')->onDelete('cascade');
        });

        Schema::create('ref_kpa_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('level_id')->unsigned();
            $table->text('kpa')->nullable();
            $table->text('deskripsi')->nullable();
            $table->text('uraian')->nullable();
            $table->text('penjelasan')->nullable();
            $table->text('output')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
            $table->foreign('level_id')->references('id')->on('ref_kpa_level')->onDelete('cascade');
        });

        Schema::create('log_ref_elemen', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->text('elemen')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_kpa', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('elemen_id')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_kpa_level', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('kpa_id')->unsigned();
            $table->integer('level')->default(1)->comment('1 Dst');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_kpa_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('level_id')->unsigned();
            $table->text('kpa')->nullable();
            $table->text('deskripsi')->nullable();
            $table->text('uraian')->nullable();
            $table->text('penjelasan')->nullable();
            $table->text('output')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_ref_kpa_detail');
        Schema::dropIfExists('log_ref_kpa_level');
        Schema::dropIfExists('log_ref_kpa');
        Schema::dropIfExists('log_ref_elemen');

        Schema::dropIfExists('ref_kpa_detail');
        Schema::dropIfExists('ref_kpa_level');
        Schema::dropIfExists('ref_kpa');
        Schema::dropIfExists('ref_elemen');
    }
}
