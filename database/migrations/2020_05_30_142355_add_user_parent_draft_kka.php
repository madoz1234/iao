<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserParentDraftKka extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_draft_kka', function (Blueprint $table) {
            $table->integer('parent_svp')->unsigned()->default(0)->after('program_id');
        });

        Schema::table('log_trans_draft_kka', function (Blueprint $table) {
            $table->integer('parent_svp')->unsigned()->default(0)->after('program_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_draft_kka', function (Blueprint $table) {
            $table->dropColumn('parent_svp');
        });

        Schema::table('trans_draft_kka', function (Blueprint $table) {
            $table->dropColumn('parent_svp');
        });
    }
}
