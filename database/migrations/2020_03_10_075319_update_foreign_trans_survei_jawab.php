<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateForeignTransSurveiJawab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_survei_jawab', function (Blueprint $table) {
            $table->integer('status')->default(0)->after('user_id')->comment('0:Baru, 1:Draft, 2:Completed');
            $table->dropForeign(['lha_id']);
            $table->foreign('lha_id')->references('id')->on('trans_lha')->onDelete('cascade');
        });

        Schema::table('log_trans_survei_jawab', function (Blueprint $table) {
            $table->integer('status')->default(0)->after('user_id')->comment('0:Baru, 1:Draft, 2:Completed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_survei_jawab', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropForeign(['lha_id']);
            $table->foreign('lha_id')->references('id')->on('trans_penugasan_audit')->onDelete('cascade');
        });

        Schema::table('log_trans_survei_jawab', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
