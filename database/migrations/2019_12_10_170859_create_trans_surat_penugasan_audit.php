<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransSuratPenugasanAudit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_penugasan_audit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rencana_id')->unsigned();
            $table->string('no_surat', 255)->nullable();
            $table->string('tgl_surat', 255)->nullable();
            $table->integer('status')->default(0)->comment('0:Draft,1:Approve SVP, 2:Approve Dirut, 3:Final, 4:Historis');
            $table->string('bukti', 255)->nullable();
            $table->string('filename', 255)->nullable();
            $table->text('ket_svp')->nullable();
            $table->integer('user_id')->unsigned()->default(1);
            $table->integer('group')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('rencana_id')->references('id')->on('trans_rencana_audit_detail')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('sys_users');
        });

        Schema::create('trans_penugasan_audit_detail_anggota', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('penugasan_id')->unsigned();
            $table->integer('fungsi')->default(0)->comment('1:Operasional,2:Keuangan, 3:Sistem');
            $table->integer('user_id')->unsigned();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('penugasan_id')->references('id')->on('trans_penugasan_audit')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('sys_users');
        });

        Schema::create('trans_penugasan_audit_detail_jadwal', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('penugasan_id')->unsigned();
            $table->string('tgl', 200);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('penugasan_id')->references('id')->on('trans_penugasan_audit')->onDelete('cascade');
        });

        Schema::create('trans_penugasan_audit_detail_jadwal_d', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jadwal_detail_id')->unsigned();
            $table->string('mulai', 200);
            $table->string('selesai', 200);
            $table->text('keterangan')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('jadwal_detail_id')->references('id')->on('trans_penugasan_audit_detail_jadwal')->onDelete('cascade');
        });

        Schema::create('log_trans_penugasan_audit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('rencana_id')->unsigned();
            $table->string('no_surat', 255)->nullable();
            $table->string('tgl_surat', 255)->nullable();
            $table->integer('status')->default(0)->comment('0:Draft,1:Approve SVP, 2:Approve Dirut, 3:Final, 4:Historis');
            $table->string('bukti', 255)->nullable();
            $table->string('filename', 255)->nullable();
            $table->text('ket_svp')->nullable();
            $table->integer('user_id')->unsigned()->default(1);
            $table->integer('group')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_penugasan_audit_detail_anggota', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('penugasan_id')->unsigned();
            $table->integer('fungsi')->default(0)->comment('1:Operasional,2:Keuangan, 3:Sistem');
            $table->integer('user_id')->unsigned();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_penugasan_audit_detail_jadwal', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('penugasan_id')->unsigned();
            $table->string('tgl', 200);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_penugasan_audit_detail_jadwal_d', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('jadwal_detail_id')->unsigned();
            $table->string('mulai', 200);
            $table->string('selesai', 200);
            $table->text('keterangan')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_penugasan_audit_detail_jadwal_d');
        Schema::dropIfExists('log_trans_penugasan_audit_detail_jadwal');
        Schema::dropIfExists('log_trans_penugasan_audit_detail_anggota');
        Schema::dropIfExists('log_trans_penugasan_audit');
        Schema::dropIfExists('trans_penugasan_audit_detail_jadwal_d');
        Schema::dropIfExists('trans_penugasan_audit_detail_jadwal');
        Schema::dropIfExists('trans_penugasan_audit_detail_anggota');
        Schema::dropIfExists('trans_penugasan_audit');
    }
}
