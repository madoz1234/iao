<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransRencanaAuditDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_rencana_audit_detail', function (Blueprint $table) {
            $table->integer('flag')->default(1)->comment('1:Aktif, 2:Tidak Aktif')->after('rencana_id');
        });

        Schema::table('log_trans_rencana_audit_detail', function (Blueprint $table) {
            $table->integer('flag')->default(1)->comment('1:Aktif, 2:Tidak Aktif')->after('rencana_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_rencana_audit_detail', function (Blueprint $table) {
            $table->dropColumn('flag');
        });

        Schema::table('trans_rencana_audit_detail', function (Blueprint $table) {
            $table->dropColumn('flag');
        });
    }
}
