<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UbahTransLhaAddFreeText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_lha', function (Blueprint $table) {
            $table->string('tgl_bkpu', 200)->nullable()->after('bkpu');
            $table->string('tgl_progress', 200)->nullable()->after('progress');
            $table->string('tgl_pu', 200)->nullable()->after('pu');
            $table->string('tgl_cash', 200)->nullable()->after('cash');
            $table->string('tgl_piutang', 200)->nullable()->after('piutang');
            $table->string('tgl_tagihan', 200)->nullable()->after('tagihan');
        });

        Schema::table('log_trans_lha', function (Blueprint $table) {
            $table->string('tgl_bkpu', 200)->nullable()->after('bkpu');
            $table->string('tgl_progress', 200)->nullable()->after('progress');
            $table->string('tgl_pu', 200)->nullable()->after('pu');
            $table->string('tgl_cash', 200)->nullable()->after('cash');
            $table->string('tgl_piutang', 200)->nullable()->after('piutang');
            $table->string('tgl_tagihan', 200)->nullable()->after('tagihan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_lha', function (Blueprint $table) {
            $table->dropColumn('tgl_bkpu');
            $table->dropColumn('tgl_progress');
            $table->dropColumn('tgl_pu');
            $table->dropColumn('tgl_cash');
            $table->dropColumn('tgl_piutang');
            $table->dropColumn('tgl_tagihan');
        });
        
        Schema::table('trans_lha', function (Blueprint $table) {
            $table->dropColumn('tgl_bkpu');
            $table->dropColumn('tgl_progress');
            $table->dropColumn('tgl_pu');
            $table->dropColumn('tgl_cash');
            $table->dropColumn('tgl_piutang');
            $table->dropColumn('tgl_tagihan');
        });
    }
}
