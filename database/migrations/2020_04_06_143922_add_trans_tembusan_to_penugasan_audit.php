<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransTembusanToPenugasanAudit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_penugasan_audit_detail_tembusan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('penugasan_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('penugasan_id')->references('id')->on('trans_penugasan_audit')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('sys_users');
        });

        Schema::create('log_trans_penugasan_audit_detail_tembusan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('penugasan_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_penugasan_audit_detail_tembusan');
        Schema::dropIfExists('trans_penugasan_audit_detail_tembusan');
    }
}
