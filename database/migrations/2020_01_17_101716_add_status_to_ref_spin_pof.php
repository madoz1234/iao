<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToRefSpinPof extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_spin_pof', function (Blueprint $table) {
            $table->integer('status')->default(0)->after('deskripsi')->comment('0:Belum Aktif, 1:Aktif');
        });

        Schema::table('log_ref_spin_pof', function (Blueprint $table) {
            $table->integer('status')->default(0)->after('deskripsi')->comment('0:Belum Aktif, 1:Aktif');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_spin_pof', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::table('log_ref_spin_pof', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}