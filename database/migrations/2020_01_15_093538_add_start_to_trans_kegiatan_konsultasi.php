<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStartToTransKegiatanKonsultasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_kegiatan_konsultasi', function (Blueprint $table) {
            $table->dropColumn('waktu');
            $table->time('mulai')->after('lokasi');
            $table->time('selesai')->after('mulai');

        });

        Schema::table('log_trans_kegiatan_konsultasi', function (Blueprint $table) {
            $table->dropColumn('waktu');
            $table->time('mulai')->after('lokasi');
            $table->time('selesai')->after('mulai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_kegiatan_konsultasi', function (Blueprint $table) {
            $table->time('waktu');
            $table->dropColumn('mulai');
            $table->dropColumn('selesai');
        });

        Schema::table('log_trans_kegiatan_konsultasi', function (Blueprint $table) {
            $table->time('waktu');
            $table->dropColumn('mulai');
            $table->dropColumn('selesai');
        });
    }
}
