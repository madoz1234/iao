<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_bu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 200)->nullable();
            $table->string('nama', 200);
            $table->text('alamat')->nullable();
            $table->string('no_tlp', 150)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('ref_bu_pic', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bu_id')->unsigned();
            $table->integer('pic')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('bu_id')->references('id')->on('ref_bu')->onDelete('cascade');
            $table->foreign('pic')->references('id')->on('sys_users');
        });

        Schema::create('ref_co', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 200)->nullable();
            $table->string('nama', 200);
            $table->text('alamat')->nullable();
            $table->string('no_tlp', 150)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('ref_co_pic', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('co_id')->unsigned();
            $table->integer('pic')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('co_id')->references('id')->on('ref_co')->onDelete('cascade');
            $table->foreign('pic')->references('id')->on('sys_users');
        });

        Schema::create('ref_project', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bu_id')->unsigned();
            $table->string('project_ab', 200)->nullable();
            $table->string('nama', 200);
            $table->text('alamat')->nullable();
            $table->string('no_tlp', 150)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('bu_id')->references('id')->on('ref_bu')->onDelete('cascade');
        });

        Schema::create('ref_project_pic', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->integer('pic')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('project_id')->references('id')->on('ref_project')->onDelete('cascade');
            $table->foreign('pic')->references('id')->on('sys_users');
        });

        Schema::create('ref_anak_perusahaan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 200)->nullable();
            $table->string('nama', 200);
            $table->text('alamat')->nullable();
            $table->string('no_tlp', 150)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('ref_anak_perusahaan_pic', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ap_id')->unsigned();
            $table->integer('pic')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('ap_id')->references('id')->on('ref_anak_perusahaan')->onDelete('cascade');
            $table->foreign('pic')->references('id')->on('sys_users');
        });

        Schema::create('ref_vendor', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 200)->nullable();
            $table->string('nama', 200);
            $table->text('alamat')->nullable();
            $table->string('no_tlp', 150)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('ref_vendor_pic', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id')->unsigned();
            $table->integer('pic')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('vendor_id')->references('id')->on('ref_vendor')->onDelete('cascade');
            $table->foreign('pic')->references('id')->on('sys_users');
        });

        Schema::create('ref_dokumen', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul', 200);
            $table->text('deskripsi')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('ref_fokus_audit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bidang')->default(1)->comment('1:Operasional,2:Keuangan, 3:Sistem');
            $table->text('audit')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('ref_langkah_kerja', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bidang')->default(1)->comment('1:Operasional,2:Keuangan, 3:Sistem');
            $table->integer('fokus_audit_id')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('fokus_audit_id')->references('id')->on('ref_fokus_audit')->onDelete('cascade');
        });

        Schema::create('ref_langkah_kerja_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('langkah_kerja_id')->unsigned();
            $table->text('deskripsi')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('langkah_kerja_id')->references('id')->on('ref_langkah_kerja')->onDelete('cascade');
        });

        Schema::create('ref_konsultasi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 200);
            // $table->text('deskripsi')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('ref_kegiatan_lain', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 200);
            // $table->text('deskripsi')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('ref_cae', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 200);
            $table->text('alamat')->nullable();
            $table->string('no_tlp', 150)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('ref_kategori_temuan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status')->default(1)->comment('1:Major, 2:Minor, 3:Observasi Negatif, 4:Observasi Positif');
            $table->string('deskripsi', 50);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('ref_standarisasi_temuan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 200);
            $table->text('deskripsi')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('ref_standarisasi_temuan_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('standarisasi_id')->unsigned();
            $table->text('deskripsi')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('standarisasi_id')->references('id')->on('ref_standarisasi_temuan')->onDelete('cascade');
        });

        Schema::create('ref_temuan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('standarisasi_id')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('standarisasi_id')->references('id')->on('ref_standarisasi_temuan')->onDelete('cascade');
        });

        Schema::create('ref_temuan_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('temuan_id')->unsigned();
            $table->string('kode', 200);
            $table->text('deskripsi')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('temuan_id')->references('id')->on('ref_temuan')->onDelete('cascade');
        });

        Schema::create('log_ref_bu', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->string('kode', 200)->nullable();
            $table->string('nama', 200);
            $table->text('alamat')->nullable();
            $table->string('no_tlp', 150)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_co', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->string('kode', 200)->nullable();
            $table->string('nama', 200);
            $table->text('alamat')->nullable();
            $table->string('no_tlp', 150)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_project', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('bu_id')->unsigned();
            $table->string('project_ab', 200)->nullable();
            $table->string('nama', 200);
            $table->text('alamat')->nullable();
            $table->string('no_tlp', 150)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_anak_perusahaan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->string('kode', 200)->nullable();
            $table->string('nama', 200);
            $table->text('alamat')->nullable();
            $table->string('no_tlp', 150)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_vendor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->string('kode', 200)->nullable();
            $table->string('nama', 200);
            $table->text('alamat')->nullable();
            $table->string('no_tlp', 150)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_dokumen', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->string('judul', 200);
            $table->text('deskripsi')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_fokus_audit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('bidang')->default(1)->comment('1:Operasional,2:Keuangan, 3:Sistem');
            $table->text('audit')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_bu_pic', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('bu_id')->unsigned();
            $table->integer('pic')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_co_pic', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('co_id')->unsigned();
            $table->integer('pic')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_project_pic', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->integer('pic')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_anak_perusahaan_pic', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('ap_id')->unsigned();
            $table->integer('pic')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_vendor_pic', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('vendor_id')->unsigned();
            $table->integer('pic')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_langkah_kerja', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('bidang')->default(1)->comment('1:Operasional,2:Keuangan, 3:Sistem');
            $table->integer('fokus_audit_id')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_langkah_kerja_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('langkah_kerja_id')->unsigned();
            $table->text('deskripsi')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_konsultasi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->string('nama', 200);
            // $table->text('deskripsi')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_kegiatan_lain', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->string('nama', 200);
            // $table->text('deskripsi')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_cae', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->string('nama', 200);
            $table->text('alamat')->nullable();
            $table->string('no_tlp', 150)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_kategori_temuan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('status')->default(1)->comment('1:Major, 2:Minor, 3:Observasi Negatif, 4:Observasi Positif');
            $table->string('deskripsi', 50);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_standarisasi_temuan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->string('kode', 200);
            $table->text('deskripsi')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_standarisasi_temuan_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('standarisasi_id')->unsigned();
            $table->text('deskripsi')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_temuan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('standarisasi_id')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_temuan_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('temuan_id')->unsigned();
            $table->string('kode', 200);
            $table->text('deskripsi')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {	
    	Schema::dropIfExists('log_ref_temuan_detail');
    	Schema::dropIfExists('log_ref_temuan');
    	Schema::dropIfExists('log_ref_standarisasi_temuan_detail');
    	Schema::dropIfExists('log_ref_standarisasi_temuan');
    	Schema::dropIfExists('log_ref_kategori_temuan');
    	Schema::dropIfExists('log_ref_cae');
    	Schema::dropIfExists('log_ref_kegiatan_lain');
    	Schema::dropIfExists('log_ref_konsultasi');
    	Schema::dropIfExists('log_ref_langkah_kerja_detail');
        Schema::dropIfExists('log_ref_langkah_kerja');
    	Schema::dropIfExists('log_ref_vendor_pic');
    	Schema::dropIfExists('log_ref_anak_perusahaan_pic');
        Schema::dropIfExists('log_ref_project_pic');
        Schema::dropIfExists('log_ref_co_pic');
        Schema::dropIfExists('log_ref_bu_pic');
    	Schema::dropIfExists('log_ref_fokus_audit');
    	Schema::dropIfExists('log_ref_vendor');
    	Schema::dropIfExists('log_ref_anak_perusahaan');
        Schema::dropIfExists('log_ref_dokumen');
        Schema::dropIfExists('log_ref_project');
        Schema::dropIfExists('log_ref_co');
        Schema::dropIfExists('log_ref_bu');
        Schema::dropIfExists('ref_temuan_detail');
    	Schema::dropIfExists('ref_temuan');
    	Schema::dropIfExists('ref_standarisasi_temuan_detail');
    	Schema::dropIfExists('ref_standarisasi_temuan');
    	Schema::dropIfExists('ref_kategori_temuan');
        Schema::dropIfExists('ref_cae');
    	Schema::dropIfExists('ref_kegiatan_lain');
    	Schema::dropIfExists('ref_konsultasi');
        Schema::dropIfExists('ref_langkah_kerja_detail');
        Schema::dropIfExists('ref_langkah_kerja');
        Schema::dropIfExists('ref_fokus_audit');
        Schema::dropIfExists('ref_vendor_pic');
        Schema::dropIfExists('ref_anak_perusahaan_pic');
        Schema::dropIfExists('ref_project_pic');
        Schema::dropIfExists('ref_co_pic');
        Schema::dropIfExists('ref_bu_pic');
        Schema::dropIfExists('ref_vendor');
        Schema::dropIfExists('ref_anak_perusahaan');
        Schema::dropIfExists('ref_dokumen');
        Schema::dropIfExists('ref_project');
        Schema::dropIfExists('ref_co');
        Schema::dropIfExists('ref_bu');
    }
}
