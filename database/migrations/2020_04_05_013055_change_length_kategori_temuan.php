<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeLengthKategoriTemuan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_kategori_temuan', function (Blueprint $table) {
            $table->text('deskripsi')->nullable()->change();
        });

        Schema::table('log_ref_kategori_temuan', function (Blueprint $table) {
            $table->text('deskripsi')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ref_kategori_temuan', function (Blueprint $table) {
            $table->string('deskripsi', 50)->change();
        });

        Schema::table('log_ref_kategori_temuan', function (Blueprint $table) {
            $table->string('deskripsi', 50)->change();
        });
    }
}
