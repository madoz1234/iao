<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SetNullTempDivision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('temp_division', function (Blueprint $table) {
            $table->string('parent_id')->nullable()->change();
            $table->string('obj_level')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('temp_division', function (Blueprint $table) {
            $table->string('parent_id')->nullable()->change();
            $table->string('obj_level')->nullable()->change();
        });
    }
}
