<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransKkaMenyesuaikanRoleAuditor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_draft_kka', function (Blueprint $table) {
            $table->tinyInteger('status_operasional')->nullable()->default(0)->comment('0: Baru, 1: Save As Draft, 2:Submit')->after('status');
            $table->tinyInteger('status_keuangan')->nullable()->default(0)->comment('0: Baru, 1: Save As Draft, 2:Submit')->after('status_operasional');
            $table->tinyInteger('status_sistem')->nullable()->default(0)->comment('0: Baru, 1: Save As Draft, 2:Submit')->after('status_keuangan');
        });

        Schema::table('log_trans_draft_kka', function (Blueprint $table) {
            $table->tinyInteger('status_operasional')->nullable()->default(0)->comment('0: Baru, 1: Save As Draft, 2:Submit')->after('status');
            $table->tinyInteger('status_keuangan')->nullable()->default(0)->comment('0: Baru, 1: Save As Draft, 2:Submit')->after('status_operasional');
            $table->tinyInteger('status_sistem')->nullable()->default(0)->comment('0: Baru, 1: Save As Draft, 2:Submit')->after('status_keuangan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_draft_kka', function (Blueprint $table) {
            $table->dropColumn('status_operasional');
            $table->dropColumn('status_keuangan');
            $table->dropColumn('status_sistem');
        });

        Schema::table('trans_draft_kka', function (Blueprint $table) {
            $table->dropColumn('status_operasional');
            $table->dropColumn('status_keuangan');
            $table->dropColumn('status_sistem');
        });
    }
}
