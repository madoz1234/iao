<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddWestToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sys_users', function (Blueprint $table) {
            $table->string('no_ktp')->nullable();
            $table->string('kode_divisi')->nullable();
            $table->string('nama_divisi')->nullable();
            $table->string('posisi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sys_users', function (Blueprint $table) {
            $table->dropColumn(['no_ktp', 'kode_divisi', 'nama_divisi', 'posisi']);
        });
    }
}
