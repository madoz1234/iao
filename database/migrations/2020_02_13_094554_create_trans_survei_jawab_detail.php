<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransSurveiJawabDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_survei_jawab_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('survei_jawab_id')->unsigned();
            $table->integer('tanya_id')->unsigned();
            $table->integer('jenis');
            $table->text('jawaban');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();

            $table->foreign('survei_jawab_id')->references('id')->on('trans_survei_jawab')->onDelete('cascade');
            $table->foreign('tanya_id')->references('id')->on('ref_survei_pertanyaan')->onDelete('cascade');
        });

        Schema::create('log_trans_survei_jawab_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ref_id');
            $table->unsignedInteger('survei_jawab_id');
            $table->unsignedInteger('tanya_id');
            $table->integer('jenis');
            $table->text('jawaban');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_survei_jawab_detail');
        Schema::dropIfExists('log_trans_survei_jawab_detail');
    }
}
