<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateRefKertasKerja extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_spin_kertas_kerja', function (Blueprint $table) {
            $table->tinyInteger('version')->after('status');
        });

        Schema::table('log_ref_spin_kertas_kerja', function (Blueprint $table) {
            $table->tinyInteger('version')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('log_ref_spin_kertas_kerja', function (Blueprint $table) {
            $table->dropColumn('version');
        });

        Schema::table('ref_spin_kertas_kerja', function (Blueprint $table) {
            $table->dropColumn('version');
        });
    }
}
