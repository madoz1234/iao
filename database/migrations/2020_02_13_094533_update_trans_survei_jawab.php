<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransSurveiJawab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_survei_jawab', function (Blueprint $table) {
            $table->dropForeign(['tanya_id']);
            $table->dropColumn('tanya_id');
            $table->dropColumn('jawaban');
            $table->integer('lha_id')->unsigned()->after('id');
            $table->integer('survei_id')->unsigned()->after('lha_id');

            $table->foreign('lha_id')->references('id')->on('trans_lha')->onDelete('cascade');
            $table->foreign('survei_id')->references('id')->on('ref_survei')->onDelete('cascade');
        });

        Schema::table('log_trans_survei_jawab', function (Blueprint $table) {
            $table->dropColumn('tanya_id');
            $table->dropColumn('jawaban');
            $table->integer('lha_id')->unsigned()->after('ref_id');
            $table->integer('survei_id')->unsigned()->after('lha_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_survei_jawab', function (Blueprint $table) {
            $table->dropForeign(['lha_id']);
            $table->dropForeign(['survei_id']);
            $table->dropColumn('lha_id');
            $table->dropColumn('survei_id');
            $table->integer('tanya_id')->unsigned()->after('id');
            $table->text('jawaban')->after('tanya_id');

            $table->foreign('tanya_id')->references('id')->on('ref_survei_pertanyaan')->onDelete('cascade');
        });

        Schema::table('log_trans_survei_jawab', function (Blueprint $table) {
            $table->dropColumn('lha_id');
            $table->dropColumn('survei_id');
            $table->integer('tanya_id')->unsigned()->after('ref_id');
            $table->text('jawaban')->after('tanya_id');
        });
    }
}
