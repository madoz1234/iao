<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransDraftKka extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_draft_kka', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('program_id')->unsigned();
            $table->string('lampiran', 255)->nullable();
            $table->string('lampiranname', 255)->nullable();
            $table->tinyInteger('status')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('program_id')->references('id')->on('trans_program_audit')->onDelete('cascade');
        });

        Schema::create('trans_draft_kka_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('draft_id')->unsigned();
            $table->integer('standarisasi_id')->unsigned()->default(1);
            $table->integer('temuan_id')->unsigned()->default(1);
            $table->tinyInteger('tipe')->comment('0:Operasional,1:Keuangan,2:Sistem');
            $table->text('catatan_kondisi');
            $table->text('kriteria');
            $table->text('sebab');
            $table->text('risiko');
            $table->text('rekomendasi');
            $table->integer('user_id')->unsigned();
            $table->integer('kategori_id')->unsigned()->default(1);
            $table->tinyInteger('tanggapan')->comment('0:Menyetujui,1:Tidak Menyetujui');
            $table->text('catatan')->nullable();
            $table->string('tgl', 255)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('draft_id')->references('id')->on('trans_draft_kka')->onDelete('cascade');
            $table->foreign('standarisasi_id')->references('id')->on('ref_standarisasi_temuan')->onDelete('cascade');
            $table->foreign('temuan_id')->references('id')->on('ref_temuan_detail')->onDelete('cascade');
            $table->foreign('kategori_id')->references('id')->on('ref_kategori_temuan')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('sys_users');
        });

        Schema::create('log_trans_draft_kka', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('program_id')->unsigned();
            $table->string('lampiran', 255)->nullable();
            $table->string('lampiranname', 255)->nullable();
            $table->tinyInteger('status')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_draft_kka_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('draft_id')->unsigned();
            $table->integer('standarisasi_id')->unsigned()->default(1);
            $table->integer('temuan_id')->unsigned()->default(1);
            $table->tinyInteger('tipe')->comment('0:Operasional,1:Keuangan,2:Sistem');
            $table->text('catatan_kondisi');
            $table->text('kriteria');
            $table->text('sebab');
            $table->text('risiko');
            $table->text('rekomendasi');
            $table->integer('user_id')->unsigned();
            $table->integer('kategori_id')->unsigned()->default(1);
            $table->tinyInteger('tanggapan')->comment('0:Menyetujui,1:Tidak Menyetujui');
            $table->text('catatan')->nullable();
            $table->string('tgl', 255)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_draft_kka_detail');
        Schema::dropIfExists('log_trans_draft_kka');
        Schema::dropIfExists('trans_draft_kka_detail');
        Schema::dropIfExists('trans_draft_kka');
    }
}
