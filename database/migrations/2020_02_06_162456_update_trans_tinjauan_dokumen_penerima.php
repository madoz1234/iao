<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransTinjauanDokumenPenerima extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('trans_tinjauan_dokumen_penerima', function (Blueprint $table) {
            $table->tinyInteger('tipe')->nullable()->after('nama');
        });

        Schema::table('log_trans_tinjauan_dokumen_penerima', function (Blueprint $table) {
            $table->tinyInteger('tipe')->nullable()->after('nama');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('log_trans_tinjauan_dokumen_penerima', function (Blueprint $table) {
            $table->dropColumn('tipe')->nullable();
        });

        Schema::table('trans_tinjauan_dokumen_penerima', function (Blueprint $table) {
            $table->dropColumn('tipe')->nullable();
        });
    }
}
