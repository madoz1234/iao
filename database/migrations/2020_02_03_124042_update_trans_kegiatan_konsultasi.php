<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransKegiatanKonsultasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_kkr', function (Blueprint $table) {
            $table->tinyInteger('status_ditolak')->comment('0:Draft,1:Ditolak')->after('status');
            $table->text('ket_svp')->nullable()->after('status_ditolak');
        });

        Schema::table('log_trans_kkr', function (Blueprint $table) {
            $table->tinyInteger('status_ditolak')->comment('0:Draft,1:Ditolak')->after('status');
            $table->text('ket_svp')->nullable()->after('status_ditolak');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('log_trans_kkr', function (Blueprint $table) {
            $table->dropColumn('status_ditolak');
            $table->dropColumn('ket_svp');
        });

        Schema::table('trans_kkr', function (Blueprint $table) {
            $table->dropColumn('status_ditolak');
            $table->dropColumn('ket_svp');
        });
    }
}
