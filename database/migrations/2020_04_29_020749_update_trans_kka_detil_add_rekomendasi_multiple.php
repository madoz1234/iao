<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransKkaDetilAddRekomendasiMultiple extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_draft_kka_detail', function (Blueprint $table) {
            $table->dropColumn('rekomendasi');
        });

        Schema::table('log_trans_draft_kka_detail', function (Blueprint $table) {
            $table->dropColumn('rekomendasi');
        });

        Schema::create('trans_draft_kka_detail_rekomendasi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kka_detail_id')->unsigned();
            $table->text('rekomendasi')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
            $table->foreign('kka_detail_id')->references('id')->on('trans_draft_kka_detail')->onDelete('cascade');
        });

        Schema::create('log_trans_draft_kka_detail_rekomendasi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('kka_detail_id')->unsigned();
            $table->text('rekomendasi')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_draft_kka_detail', function (Blueprint $table) {
            $table->text('rekomendasi')->nullable()->after('risiko');
        });

        Schema::table('log_trans_draft_kka_detail', function (Blueprint $table) {
            $table->text('rekomendasi')->nullable()->after('risiko');
        });

        Schema::dropIfExists('log_trans_draft_kka_detail_rekomendasi');
        Schema::dropIfExists('trans_draft_kka_detail_rekomendasi');
    }
}
