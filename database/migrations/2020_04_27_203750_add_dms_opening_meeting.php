<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDmsOpeningMeeting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_opening_meeting', function (Blueprint $table) {
            $table->integer('dms')->default(0);
            $table->string('dms_parent_id')->nullable();
            $table->integer('dms_type')->default(144);
            $table->dateTime('dms_uploaded_date')->nullable();
            $table->string('dms_file_id')->nullable();
            $table->integer('dms_update')->default(0);
            $table->integer('dms_multiple')->default(0);
            $table->integer('dms_object_id')->default(0);
            $table->string('dms_object_type')->nullable();
            $table->integer('dms_hadir')->default(0);
            $table->string('dms_hadir_parent_id')->nullable();
            $table->integer('dms_hadir_type')->default(144);
            $table->dateTime('dms_hadir_uploaded_date')->nullable();
            $table->string('dms_hadir_file_id')->nullable();
            $table->integer('dms_hadir_update')->default(0);
            $table->integer('dms_hadir_multiple')->default(0);
            $table->integer('dms_hadir_object_id')->default(0);
            $table->string('dms_hadir_object_type')->nullable();
        });
        Schema::table('log_trans_opening_meeting', function (Blueprint $table) {
            $table->integer('dms')->default(0);
            $table->string('dms_parent_id')->nullable();
            $table->integer('dms_type')->default(144);
            $table->dateTime('dms_uploaded_date')->nullable();
            $table->string('dms_file_id')->nullable();
            $table->integer('dms_update')->default(0);
            $table->integer('dms_multiple')->default(0);
            $table->integer('dms_object_id')->default(0);
            $table->string('dms_object_type')->nullable();
            $table->integer('dms_hadir')->default(0);
            $table->string('dms_hadir_parent_id')->nullable();
            $table->integer('dms_hadir_type')->default(144);
            $table->dateTime('dms_hadir_uploaded_date')->nullable();
            $table->string('dms_hadir_file_id')->nullable();
            $table->integer('dms_hadir_update')->default(0);
            $table->integer('dms_hadir_multiple')->default(0);
            $table->integer('dms_hadir_object_id')->default(0);
            $table->string('dms_hadir_object_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_opening_meeting', function (Blueprint $table) {
            $table->dropColumn(['dms', 'dms_parent_id', 'dms_type', 'dms_uploaded_date', 'dms_file_id', 'dms_update', 'dms_multiple', 'dms_object_id', 'dms_object_type',
          'dms_hadir', 'dms_hadir_parent_id', 'dms_hadir_type', 'dms_hadir_uploaded_date', 'dms_hadir_file_id', 'dms_hadir_update', 'dms_hadir_multiple', 'dms_hadir_object_id', 'dms_hadir_object_type']);
        });
        Schema::table('log_trans_opening_meeting', function (Blueprint $table) {
            $table->dropColumn(['dms', 'dms_parent_id', 'dms_type', 'dms_uploaded_date', 'dms_file_id', 'dms_update', 'dms_multiple', 'dms_object_id', 'dms_object_type',
          'dms_hadir', 'dms_hadir_parent_id', 'dms_hadir_type', 'dms_hadir_uploaded_date', 'dms_hadir_file_id', 'dms_hadir_update', 'dms_hadir_multiple', 'dms_hadir_object_id', 'dms_hadir_object_type']);
        });
    }
}
