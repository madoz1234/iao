<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UbahProgramAuditChangeRelasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_program_audit', function (Blueprint $table) {
            $table->dropForeign(['tinjauan_id']);
            $table->dropColumn('tinjauan_id');

            $table->integer('penugasan_id')->unsigned()->after('id');
            $table->foreign('penugasan_id')->references('id')->on('trans_penugasan_audit');
        });

        Schema::table('log_trans_program_audit', function (Blueprint $table) {
            $table->dropColumn('tinjauan_id');
            $table->integer('penugasan_id')->unsigned()->after('ref_id');
        });

        Schema::table('trans_penugasan_audit', function (Blueprint $table) {
        	$table->integer('status_audit')->default(0)->after('status_tinjauan');
        });

        Schema::table('log_trans_penugasan_audit', function (Blueprint $table) {
        	$table->integer('status_audit')->default(0)->after('status_tinjauan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_program_audit', function (Blueprint $table) {
            $table->dropColumn('penugasan_id');
            $table->integer('tinjauan_id')->unsigned()->after('ref_id');
        });

        Schema::table('trans_program_audit', function (Blueprint $table) {
            $table->dropForeign(['penugasan_id']);
            $table->dropColumn('penugasan_id');

            $table->integer('tinjauan_id')->unsigned()->after('id');
            $table->foreign('tinjauan_id')->references('id')->on('trans_tinjauan_dokumen');
        });

        Schema::table('log_trans_penugasan_audit', function (Blueprint $table) {
        	$table->dropColumn('status_audit');
        });

        Schema::table('trans_penugasan_audit', function (Blueprint $table) {
        	$table->dropColumn('status_audit');
        });
    }
}
