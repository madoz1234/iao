<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransRkiaDetilAddUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_rencana_audit_detail', function (Blueprint $table) {
        	$table->integer('status_konsultasi')->default(0)->after('status_penugasan');
        	$table->integer('status_lain')->default(0)->after('status_konsultasi');
        	$table->integer('user_operasional')->unsigned()->default(1)->after('status_lain');
        	$table->integer('user_keuangan')->unsigned()->default(1)->after('user_operasional');
        	$table->integer('user_sistem')->unsigned()->default(1)->after('user_keuangan');

        	$table->foreign('user_operasional')->references('id')->on('sys_users');
        	$table->foreign('user_keuangan')->references('id')->on('sys_users');
        	$table->foreign('user_sistem')->references('id')->on('sys_users');
        });

        Schema::table('log_trans_rencana_audit_detail', function (Blueprint $table) {
        	$table->integer('status_konsultasi')->default(0)->after('status_penugasan');
        	$table->integer('status_lain')->default(0)->after('status_konsultasi');
        	$table->integer('user_operasional')->unsigned()->default(1)->after('status_lain');
        	$table->integer('user_keuangan')->unsigned()->default(1)->after('user_operasional');
        	$table->integer('user_sistem')->unsigned()->default(1)->after('user_keuangan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_rencana_audit_detail', function (Blueprint $table) {
            $table->dropColumn('status_konsultasi');
            $table->dropColumn('status_lain');
            $table->dropColumn('user_operasional');
            $table->dropColumn('user_keuangan');
            $table->dropColumn('user_sistem');
        });

        Schema::table('trans_rencana_audit_detail', function (Blueprint $table) {
        	$table->dropForeign(['user_operasional']);
        	$table->dropForeign(['user_keuangan']);
        	$table->dropForeign(['user_sistem']);
            $table->dropColumn('status_konsultasi');
            $table->dropColumn('status_lain');
            $table->dropColumn('user_operasional');
            $table->dropColumn('user_keuangan');
            $table->dropColumn('user_sistem');
        });
    }
}
