<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransKegiatanAudit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_rencana_audit', function (Blueprint $table) {
            $table->tinyInteger('status_ditolak')->comment('0:Draft,1:Ditolak')->after('status_revisi');
        });

        Schema::table('log_trans_rencana_audit', function (Blueprint $table) {
            $table->tinyInteger('status_ditolak')->comment('0:Draft,1:Ditolak')->after('status_revisi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_rencana_audit', function (Blueprint $table) {
            $table->dropColumn('status_ditolak');
        });

        Schema::table('trans_rencana_audit', function (Blueprint $table) {
            $table->dropColumn('status_ditolak');
        });
    }
}
