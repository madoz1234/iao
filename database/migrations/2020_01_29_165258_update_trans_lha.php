<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransLha extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('log_trans_lha', function (Blueprint $table) {
            $table->dropColumn('bukti_surat');
            $table->dropColumn('filename_surat');
            $table->dropColumn('bukti_petunjuk');
            $table->dropColumn('filename_petunjuk');
            $table->text('kesimpulan')->nullable()->after('ket_dirut');
        });

        Schema::table('trans_lha', function (Blueprint $table) {
            $table->dropColumn('bukti_surat');
            $table->dropColumn('filename_surat');
            $table->dropColumn('bukti_petunjuk');
            $table->dropColumn('filename_petunjuk');
            $table->text('kesimpulan')->nullable()->after('ket_dirut');
        });

        Schema::create('trans_lha_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lha_id')->unsigned();
            $table->tinyInteger('tipe')->comment('0:Operasional,1:Keuangan,2:Sistem');
            $table->text('keterangan');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('lha_id')->references('id')->on('trans_lha')->onDelete('cascade');
        });

        Schema::create('log_trans_lha_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('lha_id')->unsigned();
            $table->tinyInteger('tipe')->comment('0:Operasional,1:Keuangan,2:Sistem');
            $table->text('keterangan');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_lha', function (Blueprint $table) {
            $table->string('bukti_surat', 255)->nullable()->after('ket_dirut');
            $table->string('filename_surat', 255)->nullable()->after('bukti_surat');
            $table->string('bukti_petunjuk', 255)->nullable()->after('filename_surat');
            $table->string('filename_petunjuk', 255)->nullable()->after('bukti_petunjuk');
            $table->dropColumn('kesimpulan');
        });

        Schema::table('trans_lha', function (Blueprint $table) {
            $table->string('bukti_surat', 255)->nullable()->after('ket_dirut');
            $table->string('filename_surat', 255)->nullable()->after('bukti_surat');
            $table->string('bukti_petunjuk', 255)->nullable()->after('filename_surat');
            $table->string('filename_petunjuk', 255)->nullable()->after('bukti_petunjuk');
            $table->dropColumn('kesimpulan');
        });

        Schema::dropIfExists('log_trans_lha_detail');
        Schema::dropIfExists('trans_lha_detail');
    }
}
