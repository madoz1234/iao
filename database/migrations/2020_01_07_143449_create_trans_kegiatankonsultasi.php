<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransKegiatankonsultasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_kegiatan_konsultasi', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tanggal');
            $table->time('waktu');
            $table->text('lokasi');
            $table->integer('kategori')->default(0)->comment('1:Business Unit, 2:Business Office, 3:Project, 4:Anak Perusahaan, 5:Vendor');
            $table->integer('status')->default(0)->comment('0:Baru, 1:On Progress, 2:Historis');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('trans_kegiatan_konsultasi_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('konsultasi_id')->unsigned();
            $table->text('materi');
            $table->integer('penanggung_jawab_materi');
            $table->text('realisasi');
            $table->integer('penanggung_jawab_realisasi');
            $table->text('rencana');
            $table->integer('penanggung_jawab_rencana');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('konsultasi_id')->references('id')->on('trans_kegiatan_konsultasi')->onDelete('cascade');
        });

        Schema::create('trans_kegiatan_konsultasi_detail_peserta', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('konsultasi_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('konsultasi_id')->references('id')->on('trans_kegiatan_konsultasi')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('sys_users');
        });

        //logs
        Schema::create('log_trans_kegiatan_konsultasi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->date('tanggal');
            $table->time('waktu');
            $table->text('lokasi');
            $table->integer('kategori')->default(0)->comment('1:Business Unit, 2:Business Office, 3:Project, 4:Anak Perusahaan, 5:Vendor');
            $table->integer('status')->default(0)->comment('0:Baru, 1:On Progress, 2:Historis');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
        Schema::create('log_trans_kegiatan_konsultasi_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('konsultasi_id')->unsigned();
            $table->text('materi');
            $table->integer('penanggung_jawab_materi');
            $table->text('realisasi');
            $table->integer('penanggung_jawab_realisasi');
            $table->text('rencana');
            $table->integer('penanggung_jawab_rencana');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
        Schema::create('log_trans_kegiatan_konsultasi_detail_peserta', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('konsultasi_id')->unsigned();
            $table->integer('user_id');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_kegiatan_konsultasi_detail_peserta');
        Schema::dropIfExists('log_trans_kegiatan_konsultasi_detail');
        Schema::dropIfExists('log_trans_kegiatan_konsultasi');
        
        Schema::dropIfExists('trans_kegiatan_konsultasi_detail_peserta');
        Schema::dropIfExists('trans_kegiatan_konsultasi_detail');
        Schema::dropIfExists('trans_kegiatan_konsultasi');

    }
}
