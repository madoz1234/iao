<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransTinjauanDokumenEmail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_tinjauan_dokumen_email', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tinjauan_id')->unsigned();
            $table->string('email', 255)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
            $table->foreign('tinjauan_id')->references('id')->on('trans_tinjauan_dokumen')->onDelete('cascade');
        });

        Schema::create('log_trans_tinjauan_dokumen_email', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('tinjauan_id')->unsigned();
            $table->string('email', 255)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_tinjauan_dokumen_email');
        Schema::dropIfExists('trans_tinjauan_dokumen_email');
    }
}
