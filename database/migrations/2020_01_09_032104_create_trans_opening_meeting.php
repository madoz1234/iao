<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransOpeningMeeting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_opening_meeting', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('program_id')->unsigned();
            $table->string('tanggal', 255)->nullable();
            $table->string('mulai', 255)->nullable();
            $table->string('selesai', 255)->nullable();
            $table->text('tempat');
            $table->tinyInteger('status')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('program_id')->references('id')->on('trans_program_audit')->onDelete('cascade');
        });

        Schema::create('trans_opening_meeting_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('opening_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('opening_id')->references('id')->on('trans_opening_meeting')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('sys_users');
        });

        Schema::create('log_trans_opening_meeting', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('program_id')->unsigned();
            $table->string('tanggal', 255)->nullable();
            $table->string('mulai', 255)->nullable();
            $table->string('selesai', 255)->nullable();
            $table->text('tempat');
            $table->tinyInteger('status')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_opening_meeting_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('opening_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_opening_meeting_detail');
        Schema::dropIfExists('log_trans_opening_meeting');
        Schema::dropIfExists('trans_opening_meeting_detail');
        Schema::dropIfExists('trans_opening_meeting');
    }
}
