<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableTransRapatInternalDaftarHadir extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_rapat_internal_daftar_hadir', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->nullable();

            $table->string('email')->nullable();
            $table->string('nomor')->nullable();
            $table->string('perusahaan')->nullable();

            $table->foreign('user_id')->references('id')->on('sys_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_rapat_internal_daftar_hadir', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');
            $table->dropColumn('email');
            $table->dropColumn('nomor');
            $table->dropColumn('perusahaan');
        });
    }
}
