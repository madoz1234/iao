<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefSpinPembobotanDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_spin_pembobotan_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('pof_id')->unsigned();
            $table->integer('spin_pembobot_id')->unsigned();
            $table->decimal('nilai', 20, 2)->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            
            $table->foreign('spin_pembobot_id')->references('id')->on('ref_spin_pembobotan')->onDelete('cascade');
            $table->foreign('pof_id')->references('id')->on('ref_spin_pof')->onDelete('cascade');
        });
        Schema::create('log_ref_spin_pembobotan_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('pof_id');
            $table->integer('spin_pembobot_id');
            $table->decimal('nilai', 20, 2)->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_spin_pembobotan_detail');
        Schema::dropIfExists('log_ref_spin_pembobotan_detail');
    }
}
