<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefSpinPembobotanDetailNilai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_spin_pembobotan_detail_nilai', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('pembobot_id')->unsigned();
            $table->integer('pemenuhan_detail_id')->unsigned();
            $table->decimal('nilai', 10, 3)->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('pembobot_id')->references('id')->on('ref_spin_pembobotan')->onDelete('cascade');
            $table->foreign('pemenuhan_detail_id')->references('id')->on('ref_spin_unsur_pemenuhan_detail')->onDelete('cascade');
        });

        Schema::create('log_ref_spin_pembobotan_detail_nilai', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('pembobot_id');
            $table->integer('pemenuhan_detail_id');
            $table->decimal('nilai', 10, 3)->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_spin_pembobotan_detail_nilai');
        Schema::dropIfExists('log_ref_spin_pembobotan_detail_nilai');
    }
}
