<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefKpaDetailUraian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_kpa_detail_uraian', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kpa_detail_id')->unsigned();
            $table->text('uraian')->nullable();
            $table->text('penjelasan')->nullable();
            $table->text('output')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('kpa_detail_id')->references('id')->on('ref_kpa_detail')->onDelete('cascade');
        });

        Schema::create('log_ref_kpa_detail_uraian', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('kpa_detail_id')->unsigned();
            $table->text('uraian')->nullable();
            $table->text('penjelasan')->nullable();
            $table->text('output')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_ref_kpa_detail_uraian');
        Schema::dropIfExists('ref_kpa_detail_uraian');
    }
}
