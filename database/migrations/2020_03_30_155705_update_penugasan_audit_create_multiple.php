<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePenugasanAuditCreateMultiple extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::table('trans_penugasan_audit_detail_jadwal', function (Blueprint $table) {
            $table->integer('tgl_id')->default(1)->after('penugasan_id');
        });

    	Schema::table('log_trans_penugasan_audit_detail_jadwal', function (Blueprint $table) {
            $table->integer('tgl_id')->default(1)->after('penugasan_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    	Schema::table('log_trans_penugasan_audit_detail_jadwal', function (Blueprint $table) {
            $table->dropColumn('tgl_id');
        });

        Schema::table('trans_penugasan_audit_detail_jadwal', function (Blueprint $table) {
            $table->dropColumn('tgl_id');
        });
    }
}
