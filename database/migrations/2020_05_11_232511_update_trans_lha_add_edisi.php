<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransLhaAddEdisi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_lha', function (Blueprint $table) {
            $table->integer('edisi_id')->unsigned()->default(1)->after('draft_id');
        });

        Schema::table('log_trans_lha', function (Blueprint $table) {
            $table->integer('edisi_id')->unsigned()->default(1)->after('draft_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {	
    	Schema::table('log_trans_lha', function (Blueprint $table) {
            $table->dropColumn('edisi_id');
        });

        Schema::table('trans_lha', function (Blueprint $table) {
            $table->dropColumn('edisi_id');
        });
    }
}
