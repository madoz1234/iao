<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddObjIdCoBuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_bu', function (Blueprint $table) {
            $table->string('obj_id')->nullable();
        });
        Schema::table('log_ref_bu', function (Blueprint $table) {
            $table->string('obj_id')->nullable();
        });
        Schema::table('ref_co', function (Blueprint $table) {
            $table->string('obj_id')->nullable();
        });
        Schema::table('log_ref_co', function (Blueprint $table) {
            $table->string('obj_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('ref_bu', function (Blueprint $table) {
          $table->dropColumn('obj_id');
      });
      Schema::table('log_ref_bu', function (Blueprint $table) {
          $table->dropColumn('obj_id');
      });
      Schema::table('ref_co', function (Blueprint $table) {
          $table->dropColumn('obj_id');
      });
      Schema::table('log_ref_co', function (Blueprint $table) {
          $table->dropColumn('obj_id');
      });
    }
}
