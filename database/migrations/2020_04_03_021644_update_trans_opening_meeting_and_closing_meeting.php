<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransOpeningMeetingAndClosingMeeting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_opening_meeting_detail', function (Blueprint $table) {
           $table->text('nama')->nullable()->after('user_id');
        });

        Schema::table('log_trans_opening_meeting_detail', function (Blueprint $table) {
           $table->text('nama')->nullable()->after('user_id');
        });

        Schema::table('trans_closing_meeting_detail', function (Blueprint $table) {
           $table->text('nama')->nullable()->after('user_id');
        });

        Schema::table('log_trans_closing_meeting_detail', function (Blueprint $table) {
           $table->text('nama')->nullable()->after('user_id');
        });

        Schema::table('trans_draft_kka_detail', function (Blueprint $table) {
           $table->text('catatan_kondisi')->nullable()->change();
        });

        Schema::table('log_trans_draft_kka_detail', function (Blueprint $table) {
           $table->text('catatan_kondisi')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('log_trans_closing_meeting_detail', function (Blueprint $table) {
            $table->dropColumn('nama');
        });

        Schema::table('trans_closing_meeting_detail', function (Blueprint $table) {
            $table->dropColumn('nama');
        });

    	Schema::table('log_trans_opening_meeting_detail', function (Blueprint $table) {
            $table->dropColumn('nama');
        });

        Schema::table('trans_opening_meeting_detail', function (Blueprint $table) {
            $table->dropColumn('nama');
        });

        Schema::table('log_trans_draft_kka_detail', function (Blueprint $table) {
           $table->text('catatan_kondisi')->change();
        });

        Schema::table('trans_draft_kka_detail', function (Blueprint $table) {
           $table->text('catatan_kondisi')->change();
        });

    }
}
