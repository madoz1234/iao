<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefSpinPembobotan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_spin_pembobotan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('version', 20);
            $table->text('deskripsi')->nullable();
            $table->integer('status')->default(0)->comment('0:Belum Aktif, 1:Aktif, 2:Non-Aktif');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });


        Schema::create('log_ref_spin_pembobotan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->string('version', 20);
            $table->text('deskripsi')->nullable();
            $table->integer('status')->default(0)->comment('0:Belum Aktif, 1:Aktif, 2:Non-Aktif');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_spin_pembobotan');
        Schema::dropIfExists('log_ref_spin_pembobotan');
    }
}
