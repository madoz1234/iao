<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransRapatEksternalUndanganPeserta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_rapat_eksternal_undangan_peserta', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('rapat_id')->unsigned();
            $table->text('email')->nullable();
            $table->text('perusahaan')->nullable();
            $table->integer('status')->default(0)->comment('1:Undangan Terkirim, 2:Hadir, 3:Tidak Hadir');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('rapat_id')->references('id')->on('trans_rapat_internal')->onDelete('cascade');
        });

        Schema::create('log_trans_rapat_eksternal_undangan_peserta', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('rapat_id');
            $table->text('email')->nullable();
            $table->text('perusahaan')->nullable();
            $table->integer('status')->default(0)->comment('1:Undangan Terkirim, 2:Hadir, 3:Tidak Hadir');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_rapat_eksternal_undangan_peserta');
        Schema::dropIfExists('log_trans_rapat_eksternal_undangan_peserta');
    }
}