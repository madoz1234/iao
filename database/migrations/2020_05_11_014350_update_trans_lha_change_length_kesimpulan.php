<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransLhaChangeLengthKesimpulan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_lha_kesimpulan', function (Blueprint $table) {
            $table->text('kesimpulan')->nullable()->change();
        });

        Schema::table('log_trans_lha_kesimpulan', function (Blueprint $table) {
            $table->text('kesimpulan')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {	
    	Schema::table('log_trans_lha_kesimpulan', function (Blueprint $table) {
            $table->string('kesimpulan')->nullable()->change();
        });

        Schema::table('trans_lha_kesimpulan', function (Blueprint $table) {
            $table->string('kesimpulan')->nullable()->change();
        });
    }
}
