<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransKkaDetailRemoveKriteria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_draft_kka_detail', function (Blueprint $table) {
            $table->dropColumn('kriteria_id');
        });

        Schema::table('log_trans_draft_kka_detail', function (Blueprint $table) {
            $table->dropColumn('kriteria_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trans_draft_kka_detail', function (Blueprint $table) {
            $table->integer('kriteria_id')->default(1)->unsigned()->after('standarisasi_id');
        });

        Schema::table('log_trans_draft_kka_detail', function (Blueprint $table) {
            $table->integer('kriteria_id')->default(1)->unsigned()->after('standarisasi_id');
        });
    }
}
