<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateSysUserAddInisialDanKategori extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sys_users', function (Blueprint $table) {
            $table->string('nip', 200)->nullable()->after('name');
            $table->string('inisial', 200)->nullable()->after('nip');
            $table->integer('kategori')->default(1)->comment('1:Business Unit (BU),2:Corporate Office (CO)')->after('inisial');
            $table->integer('kategori_id')->default(1)->after('kategori');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sys_users', function (Blueprint $table) {
            $table->dropColumn('nip');
            $table->dropColumn('inisial');
            $table->dropColumn('kategori');
            $table->dropColumn('kategori_id');
        });
    }
}
