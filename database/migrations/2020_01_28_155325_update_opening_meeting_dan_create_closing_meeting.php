<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOpeningMeetingDanCreateClosingMeeting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_opening_meeting', function (Blueprint $table) {
            $table->string('lampiran', 255)->nullable()->after('status');
            $table->string('lampiranname', 255)->nullable()->after('lampiran');
            $table->string('hadir', 255)->nullable()->after('lampiranname');
            $table->string('hadirname', 255)->nullable()->after('hadir');
        });

        Schema::table('log_trans_opening_meeting', function (Blueprint $table) {
            $table->string('lampiran', 255)->nullable()->after('status');
            $table->string('lampiranname', 255)->nullable()->after('lampiran');
            $table->string('hadir', 255)->nullable()->after('lampiranname');
            $table->string('hadirname', 255)->nullable()->after('hadir');
        });

        Schema::create('trans_closing_meeting', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('opening_id')->unsigned();
            $table->string('tanggal', 255)->nullable();
            $table->string('mulai', 255)->nullable();
            $table->string('selesai', 255)->nullable();
            $table->text('tempat');
            $table->tinyInteger('status')->default(0);
            $table->string('lampiran', 255)->nullable();
            $table->string('lampiranname', 255)->nullable();
            $table->string('hadir', 255)->nullable();
            $table->string('hadirname', 255)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('opening_id')->references('id')->on('trans_opening_meeting')->onDelete('cascade');
        });

        Schema::create('trans_closing_meeting_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('closing_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('closing_id')->references('id')->on('trans_closing_meeting')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('sys_users');
        });

        Schema::create('log_trans_closing_meeting', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('opening_id')->unsigned();
            $table->string('tanggal', 255)->nullable();
            $table->string('mulai', 255)->nullable();
            $table->string('selesai', 255)->nullable();
            $table->text('tempat');
            $table->tinyInteger('status')->default(0);
            $table->string('lampiran', 255)->nullable();
            $table->string('lampiranname', 255)->nullable();
            $table->string('hadir', 255)->nullable();
            $table->string('hadirname', 255)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_closing_meeting_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('closing_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('log_trans_opening_meeting', function (Blueprint $table) {
             $table->dropColumn('lampiran');
             $table->dropColumn('lampiranname');
             $table->dropColumn('hadir');
             $table->dropColumn('hadirname');
        });

        Schema::table('trans_opening_meeting', function (Blueprint $table) {
             $table->dropColumn('lampiran');
             $table->dropColumn('lampiranname');
             $table->dropColumn('hadir');
             $table->dropColumn('hadirname');
        });

        Schema::dropIfExists('log_trans_closing_meeting_detail');
        Schema::dropIfExists('log_trans_closing_meeting');
        Schema::dropIfExists('trans_closing_meeting_detail');
        Schema::dropIfExists('trans_closing_meeting');
    }
}
