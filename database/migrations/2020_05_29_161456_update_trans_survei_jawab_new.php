<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransSurveiJawabNew extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_survei_jawab', function (Blueprint $table) {
            $table->integer('edisi_id')->unsigned()->default(1)->after('user_id');
            $table->tinyInteger('is_countdown')->default(1)->comment('0: countup, 1: countdown')->after('timer');

            $table->foreign('edisi_id')->references('id')->on('ref_edisi')->onDelete('cascade');
        });

        Schema::table('log_trans_survei_jawab', function (Blueprint $table) {
            $table->integer('edisi_id')->unsigned()->default(1)->after('user_id');
            $table->tinyInteger('is_countdown')->default(1)->comment('0: countup, 1: countdown')->after('timer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_survei_jawab', function (Blueprint $table) {
            $table->dropForeign('edisi_id');
            
            $table->dropColumn('edisi_id');
            $table->dropColumn('is_countdown');
        });

        Schema::table('trans_survei_jawab', function (Blueprint $table) {
            $table->dropColumn('edisi_id');
            $table->dropColumn('is_countdown');
        });
    }
}
