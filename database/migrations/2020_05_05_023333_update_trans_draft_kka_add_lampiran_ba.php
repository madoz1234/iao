<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransDraftKkaAddLampiranBa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_draft_kka_detail', function (Blueprint $table) {
            $table->string('ba', 255)->nullable()->after('tipe');
            $table->string('baname', 255)->nullable()->after('ba');
        });

        Schema::table('log_trans_draft_kka_detail', function (Blueprint $table) {
            $table->string('ba', 255)->nullable()->after('tipe');
            $table->string('baname', 255)->nullable()->after('ba');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('log_trans_draft_kka_detail', function (Blueprint $table) {
            $table->dropColumn('baname');
            $table->dropColumn('ba');
        });

        Schema::table('trans_draft_kka_detail', function (Blueprint $table) {
            $table->dropColumn('baname');
            $table->dropColumn('ba');
        });
    }
}
