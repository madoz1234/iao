<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTotalTransKegiatanKonsultasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // // Delete semua tabel kegiatan konsultasi
        // Schema::dropIfExists('log_trans_kegiatan_konsultasi_detail_peserta');
        // Schema::dropIfExists('log_trans_kegiatan_konsultasi_detail');
        // Schema::dropIfExists('log_trans_kegiatan_konsultasi');
        // Schema::dropIfExists('trans_kegiatan_konsultasi_detail_peserta');
        // Schema::dropIfExists('trans_kegiatan_konsultasi_detail');
        // Schema::dropIfExists('trans_kegiatan_konsultasi');

        // // Buat baru tabel kegiatan konsultasi
        // Schema::create('trans_kegiatan_konsultasi', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->integer('rencana_detail_id')->unsigned();
        //     $table->integer('pic_konsultasi')->unsigned();
        //     $table->tinyInteger('tipe_rapat')->default(0)->comment('0:Internal, 1:External');
        //     $table->tinyInteger('status')->default(0)->comment('0:Baru, 1:Draft, 2:Waiting Approval SVP, 3: Completed');
        //     $table->integer('created_by')->nullable();
        //     $table->integer('updated_by')->nullable();
        //     $table->nullableTimestamps();

        //     $table->foreign('rencana_detail_id')->references('id')->on('trans_rencana_audit_detail')->onDelete('cascade');
        //     $table->foreign('pic_konsultasi')->references('id')->on('sys_users')->onDelete('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // //Jalankan migration 2020_01_07_143449_create_trans_kegiatankonsultasi.php
        // Schema::create('trans_kegiatan_konsultasi', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->date('tanggal');
        //     $table->time('waktu');
        //     $table->text('lokasi');
        //     $table->integer('kategori')->default(0)->comment('1:Business Unit, 2:Business Office, 3:Project, 4:Anak Perusahaan, 5:Vendor');
        //     $table->integer('status')->default(0)->comment('0:Baru, 1:On Progress, 2:Historis');
        //     $table->integer('created_by')->nullable();
        //     $table->integer('updated_by')->nullable();
        //     $table->nullableTimestamps();
        // });

        // Schema::create('trans_kegiatan_konsultasi_detail', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->integer('konsultasi_id')->unsigned();
        //     $table->text('materi');
        //     $table->integer('penanggung_jawab_materi');
        //     $table->text('realisasi');
        //     $table->integer('penanggung_jawab_realisasi');
        //     $table->text('rencana');
        //     $table->integer('penanggung_jawab_rencana');
        //     $table->integer('created_by')->nullable();
        //     $table->integer('updated_by')->nullable();
        //     $table->nullableTimestamps();

        //     $table->foreign('konsultasi_id')->references('id')->on('trans_kegiatan_konsultasi')->onDelete('cascade');
        // });

        // Schema::create('trans_kegiatan_konsultasi_detail_peserta', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->integer('konsultasi_id')->unsigned();
        //     $table->integer('user_id')->unsigned();
        //     $table->integer('created_by')->nullable();
        //     $table->integer('updated_by')->nullable();
        //     $table->nullableTimestamps();

        //     $table->foreign('konsultasi_id')->references('id')->on('trans_kegiatan_konsultasi')->onDelete('cascade');
        //     $table->foreign('user_id')->references('id')->on('sys_users');
        // });

        // //logs
        // Schema::create('log_trans_kegiatan_konsultasi', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->integer('ref_id')->unsigned();
        //     $table->date('tanggal');
        //     $table->time('waktu');
        //     $table->text('lokasi');
        //     $table->integer('kategori')->default(0)->comment('1:Business Unit, 2:Business Office, 3:Project, 4:Anak Perusahaan, 5:Vendor');
        //     $table->integer('status')->default(0)->comment('0:Baru, 1:On Progress, 2:Historis');
        //     $table->integer('created_by')->nullable();
        //     $table->integer('updated_by')->nullable();
        //     $table->nullableTimestamps();
        // });
        // Schema::create('log_trans_kegiatan_konsultasi_detail', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->integer('ref_id')->unsigned();
        //     $table->integer('konsultasi_id')->unsigned();
        //     $table->text('materi');
        //     $table->integer('penanggung_jawab_materi');
        //     $table->text('realisasi');
        //     $table->integer('penanggung_jawab_realisasi');
        //     $table->text('rencana');
        //     $table->integer('penanggung_jawab_rencana');
        //     $table->integer('created_by')->nullable();
        //     $table->integer('updated_by')->nullable();
        //     $table->nullableTimestamps();
        // });
        // Schema::create('log_trans_kegiatan_konsultasi_detail_peserta', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->integer('ref_id')->unsigned();
        //     $table->integer('konsultasi_id')->unsigned();
        //     $table->integer('user_id');
        //     $table->integer('created_by')->nullable();
        //     $table->integer('updated_by')->nullable();
        //     $table->nullableTimestamps();
        // });

        // // Jalankan migration 2020_01_15_093538_add_start_to_trans_kegiatan_konsultasi.php
        // Schema::table('trans_kegiatan_konsultasi', function (Blueprint $table) {
        //     $table->dropColumn('waktu');
        //     $table->time('mulai')->after('lokasi');
        //     $table->time('selesai')->after('mulai');

        // });

        // Schema::table('log_trans_kegiatan_konsultasi', function (Blueprint $table) {
        //     $table->dropColumn('waktu');
        //     $table->time('mulai')->after('lokasi');
        //     $table->time('selesai')->after('mulai');
        // });

        // // Jalankan migration 2020_02_03_124042_update_trans_kegiatan_konsultasi.php
        // Schema::table('trans_kkr', function (Blueprint $table) {
        //     $table->tinyInteger('status_ditolak')->comment('0:Draft,1:Ditolak')->after('status');
        //     $table->text('ket_svp')->nullable()->after('status_ditolak');
        // });

        // Schema::table('log_trans_kkr', function (Blueprint $table) {
        //     $table->tinyInteger('status_ditolak')->comment('0:Draft,1:Ditolak')->after('status');
        //     $table->text('ket_svp')->nullable()->after('status_ditolak');
        // });
    }
}
