<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefSpinUnsurPemenuhanDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_spin_unsur_pemenuhan_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('spin_pemenuhan_id')->unsigned();
            $table->text('deskripsi')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('spin_pemenuhan_id')->references('id')->on('ref_spin_unsur_pemenuhan')->onDelete('cascade');
        });

        Schema::create('log_ref_spin_unsur_pemenuhan_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('spin_pemenuhan_id');
            $table->text('deskripsi')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_spin_unsur_pemenuhan_detail');
        Schema::dropIfExists('log_ref_spin_unsur_pemenuhan_detail');
    }
}
