<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransKesimpulanLha extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_lha_kesimpulan', function (Blueprint $table) {
        	$table->increments('id');
            $table->integer('lha_id')->unsigned();
            $table->string('kesimpulan')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('lha_id')->references('id')->on('trans_lha')->onDelete('cascade');
        });

        Schema::create('log_trans_lha_kesimpulan', function (Blueprint $table) {
        	$table->increments('id');
        	$table->integer('ref_id')->unsigned();
            $table->integer('lha_id')->unsigned();
            $table->string('kesimpulan')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_lha_kesimpulan');
        Schema::dropIfExists('trans_lha_kesimpulan');
    }
}
