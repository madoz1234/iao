<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransSuratPerintahTl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_surat_perintah_tl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lha_id')->unsigned();
            $table->string('tanggal', 255)->nullable();
            $table->integer('status')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('lha_id')->references('id')->on('trans_lha')->onDelete('cascade');
        });

        Schema::create('log_trans_surat_perintah_tl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('lha_id')->unsigned();
            $table->string('tanggal', 255)->nullable();
            $table->integer('status')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_trans_surat_perintah_tl');
        Schema::dropIfExists('trans_surat_perintah_tl');
    }
}
