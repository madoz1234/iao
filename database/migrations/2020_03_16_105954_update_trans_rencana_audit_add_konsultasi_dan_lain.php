<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransRencanaAuditAddKonsultasiDanLain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_rencana_audit_detail', function (Blueprint $table) {
        	$table->integer('tipe')->default(0)->comment('0: Audit, 1:Konsultasi, 3:Lain-Lain')->after('rencana_id');
            $table->integer('konsultasi_id')->default(0)->after('tipe');
            $table->integer('lain_id')->default(0)->after('konsultasi_id');
        });

        Schema::table('log_trans_rencana_audit_detail', function (Blueprint $table) {
        	$table->integer('tipe')->default(0)->comment('0: Audit, 1:Konsultasi, 3:Lain-Lain')->after('rencana_id');
            $table->integer('konsultasi_id')->default(0)->after('tipe');
            $table->integer('lain_id')->default(0)->after('konsultasi_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('log_trans_rencana_audit_detail', function (Blueprint $table) {
            $table->dropColumn('tipe');
            $table->dropColumn('konsultasi_id');
            $table->dropColumn('lain_id');
        });

        Schema::table('trans_rencana_audit_detail', function (Blueprint $table) {
            $table->dropColumn('tipe');
            $table->dropColumn('konsultasi_id');
            $table->dropColumn('lain_id');
        });
    }
}
