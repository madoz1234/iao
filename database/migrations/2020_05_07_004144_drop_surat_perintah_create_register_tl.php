<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropSuratPerintahCreateRegisterTl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('log_trans_laporan_tl');
        Schema::dropIfExists('trans_laporan_tl');
        Schema::dropIfExists('log_trans_surat_perintah_tl');
        Schema::dropIfExists('trans_surat_perintah_tl');

        Schema::create('trans_register_tl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('draft_id')->unsigned();
            $table->integer('status')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('draft_id')->references('id')->on('trans_draft_kka')->onDelete('cascade');
        });

        Schema::create('trans_register_tl_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('register_id')->unsigned();
            $table->integer('kka_detail_id')->unsigned();
            $table->integer('rekomendasi_id')->unsigned();
            $table->integer('fungsi')->default(0);
            $table->text('tl')->nullable();
            $table->integer('status')->default(0);
            $table->integer('stage')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('register_id')->references('id')->on('trans_register_tl')->onDelete('cascade');
            $table->foreign('kka_detail_id')->references('id')->on('trans_draft_kka_detail')->onDelete('cascade');
            $table->foreign('rekomendasi_id')->references('id')->on('trans_draft_kka_detail_rekomendasi')->onDelete('cascade');
        });

        Schema::create('trans_register_tl_detail_lampiran', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('detail_id')->unsigned();
            $table->string('filename', 100)->nullable();
            $table->string('url', 255)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('detail_id')->references('id')->on('trans_register_tl_detail')->onDelete('cascade');
        });


        Schema::create('log_trans_register_tl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('draft_id')->unsigned();
            $table->integer('status')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_register_tl_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('register_id')->unsigned();
            $table->integer('kka_detail_id')->unsigned();
            $table->integer('rekomendasi_id')->unsigned();
            $table->integer('fungsi')->default(0);
            $table->text('tl')->nullable();
            $table->integer('status')->default(0);
            $table->integer('stage')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_trans_register_tl_detail_lampiran', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('detail_id')->unsigned();
            $table->string('filename', 100)->nullable();
            $table->string('url', 255)->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('trans_surat_perintah_tl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lha_id')->unsigned();
            $table->string('tanggal', 255)->nullable();
            $table->integer('status')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('lha_id')->references('id')->on('trans_lha')->onDelete('cascade');
        });

        Schema::create('log_trans_surat_perintah_tl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('lha_id')->unsigned();
            $table->string('tanggal', 255)->nullable();
            $table->integer('status')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('trans_laporan_tl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('surat_id')->unsigned();
            $table->integer('status')->default(0);
            $table->text('ket_svp')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();

            $table->foreign('surat_id')->references('id')->on('trans_surat_perintah_tl')->onDelete('cascade');
        });

        Schema::create('log_trans_laporan_tl', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->integer('surat_id')->unsigned();
            $table->integer('status')->default(0);
            $table->text('ket_svp')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });

        Schema::dropIfExists('log_trans_register_tl_detail_lampiran');
        Schema::dropIfExists('log_trans_register_tl_detail');
        Schema::dropIfExists('log_trans_register_tl');

        Schema::dropIfExists('trans_register_tl_detail_lampiran');
        Schema::dropIfExists('trans_register_tl_detail');
        Schema::dropIfExists('trans_register_tl');
    }
}
