<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateDraftKkaAddKetSvp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_draft_kka', function (Blueprint $table) {
            $table->text('ket_ketua')->nullable()->after('status');
        });

        Schema::table('log_trans_draft_kka', function (Blueprint $table) {
            $table->text('ket_ketua')->nullable()->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('log_trans_draft_kka', function (Blueprint $table) {
            $table->dropColumn('ket_ketua');
        });

        Schema::table('trans_draft_kka', function (Blueprint $table) {
            $table->dropColumn('ket_ketua');
        });
    }
}
