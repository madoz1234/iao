<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateProgramAuditChangeRencanaNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_program_audit_detail_kerja', function (Blueprint $table) {
            $table->string('rencana', 200)->nullable()->change();
        });

        Schema::table('log_trans_program_audit_detail_kerja', function (Blueprint $table) {
            $table->string('rencana', 200)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_trans_program_audit_detail_kerja', function (Blueprint $table) {
            $table->string('rencana', 200)->change();
        });

        Schema::table('trans_program_audit_detail_kerja', function (Blueprint $table) {
            $table->string('rencana', 200)->change();
        });
    }
}
