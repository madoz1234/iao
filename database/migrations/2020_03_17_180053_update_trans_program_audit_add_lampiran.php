<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransProgramAuditAddLampiran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_program_audit', function (Blueprint $table) {
            $table->string('lampiran', 255)->nullable()->after('user_id');
            $table->string('lampiranname', 255)->nullable()->after('lampiran');
        });

        Schema::table('log_trans_program_audit', function (Blueprint $table) {
            $table->string('lampiran', 255)->nullable()->after('user_id');
            $table->string('lampiranname', 255)->nullable()->after('lampiran');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('log_trans_program_audit', function (Blueprint $table) {
            $table->dropColumn('lampiranname');
            $table->dropColumn('lampiran');
        });

        Schema::table('trans_program_audit', function (Blueprint $table) {
            $table->dropColumn('lampiranname');
            $table->dropColumn('lampiran');
        });
    }
}
