<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRapatInternal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_rapat_internal', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nomor', 255)->nullable();
            $table->string('materi', 255)->nullable();
            $table->string('tanggal', 255)->nullable();
            $table->string('waktu', 255)->nullable();
            $table->string('tempat', 255)->nullable();
            $table->string('jumlah_peserta', 255)->nullable();
            $table->text('keterangan');

            $table->tinyInteger('status')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
        Schema::create('log_trans_rapat_internal', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trans_id')->unsigned();

            $table->string('nomor', 255)->nullable();
            $table->string('materi', 255)->nullable();
            $table->string('tanggal', 255)->nullable();
            $table->string('waktu', 255)->nullable();
            $table->string('tempat', 255)->nullable();
            $table->string('jumlah_peserta', 255)->nullable();
            $table->text('keterangan');

            $table->tinyInteger('status')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });
        Schema::table('sys_files', function (Blueprint $table) {
            $table->string('flag')->nullable();
            $table->integer('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sys_files', function (Blueprint $table) {
            $table->dropColumn(['flag']);
            $table->dropColumn(['status']);
        });
        Schema::dropIfExists('log_trans_rapat_internal');
        Schema::dropIfExists('trans_rapat_internal');
    }
}
