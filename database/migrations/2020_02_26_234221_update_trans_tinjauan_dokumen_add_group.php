<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransTinjauanDokumenAddGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trans_tinjauan_dokumen', function (Blueprint $table) {
            $table->integer('group')->default(0)->after('status_program');
        });

         Schema::table('log_trans_tinjauan_dokumen', function (Blueprint $table) {
            $table->integer('group')->default(0)->after('status_program');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('log_trans_tinjauan_dokumen', function (Blueprint $table) {
             $table->dropColumn('group');
        });

        Schema::table('trans_tinjauan_dokumen', function (Blueprint $table) {
             $table->dropColumn('group');
        });
    }
}
