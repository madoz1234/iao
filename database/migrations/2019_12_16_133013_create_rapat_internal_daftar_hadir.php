<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRapatInternalDaftarHadir extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_rapat_internal_daftar_hadir', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rapat_id')->unsigned();

            $table->string('nama', 255)->nullable();
            $table->string('jabatan', 255)->nullable();

            $table->tinyInteger('status')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->foreign('rapat_id')->references('id')->on('trans_rapat_internal');
            $table->nullableTimestamps();
        });
        Schema::create('trans_rapat_internal_risalah', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rapat_id')->unsigned();

            $table->text('risalah')->nullable();
            $table->string('pen_1')->nullable();
            $table->text('realisasi')->nullable();
            $table->string('pen_2')->nullable();
            $table->text('rencana')->nullable();
            $table->string('pen_3')->nullable();

            $table->tinyInteger('status')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->foreign('rapat_id')->references('id')->on('trans_rapat_internal');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_rapat_internal_daftar_hadir');
        Schema::dropIfExists('trans_rapat_internal_risalah');
    }
}
