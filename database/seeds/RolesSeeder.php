<?php

use App\Models\Auths\Role;
use App\Models\Auths\User;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin 		         	= Role::create(['name' => 'admin']);
        $dirut 		 		 	= Role::create(['name' => 'dirut']);
        $boc 		 		 	= Role::create(['name' => 'boc']);
        $bod 		 		 	= Role::create(['name' => 'bod']);
        $sekre_dirut 		 	= Role::create(['name' => 'sekre-dirut']);
        $corsec 		     	= Role::create(['name' => 'corsec']);
        $bpk 		         	= Role::create(['name' => 'bpk']);
        $svp_audit 		 		= Role::create(['name' => 'svp-audit']);
        $leader_team 		 	= Role::create(['name' => 'leader-team']);
        $auditor 		 		= Role::create(['name' => 'auditor']);
        $sekre_ia 		 		= Role::create(['name' => 'sekre-ia']);
        $kapro 		 			= Role::create(['name' => 'kapro']);
        $kanit 		 			= Role::create(['name' => 'kanit']);
        $svp 		 			= Role::create(['name' => 'svp']);
        $vendor 		 		= Role::create(['name' => 'vendor']);

        $admin->users()->save(new User([
            'name'  => 'Super Admin',
            'email' => 'admin@email.com',
            'phone' => '+62 999 9991',
            'password' => bcrypt('password'),
        ]));

        // $dirut->users()->save(new User([
        //     'name'  => 'Direktur Utama',
        //     'email' => 'dirut@email.com',
        //     'phone' => '+62 999 9992',
        //     'password' => bcrypt('password'),
        // ]));

        // $boc->users()->save(new User([
        //     'name'  => 'Board of Commissioners',
        //     'email' => 'boc@email.com',
        //     'phone' => '+62 999 9993',
        //     'password' => bcrypt('password'),
        // ]));

        // $bod->users()->save(new User([
        //     'name'  => 'Board of Directors',
        //     'email' => 'bod@email.com',
        //     'phone' => '+62 999 9994',
        //     'password' => bcrypt('password'),
        // ]));

        // $sekre_dirut->users()->save(new User([
        //     'name'  => 'Sekretaris Dirut',
        //     'email' => 'sekre_dirut@email.com',
        //     'phone' => '+62 999 9995',
        //     'password' => bcrypt('password'),
        // ]));

        // $corsec->users()->save(new User([
        //     'name'  => 'Corporate Secretary',
        //     'email' => 'corsec@email.com',
        //     'phone' => '+62 999 9996',
        //     'password' => bcrypt('password'),
        // ]));

        // $bpk->users()->save(new User([
        //     'name'  => 'BPK',
        //     'email' => 'bpk@email.com',
        //     'phone' => '+62 999 9997',
        //     'password' => bcrypt('password'),
        // ]));

        // $svp_audit->users()->save(new User([
        //     'name'  => 'SVP Audit',
        //     'email' => 'svp_audit_01@email.com',
        //     'phone' => '+62 999 9998',
        //     'password' => bcrypt('password'),
        // ]));

        // $leader_team->users()->save(new User([
        //     'name'  => 'Leader Team',
        //     'email' => 'leader_team@email.com',
        //     'phone' => '+62 999 9999',
        //     'password' => bcrypt('password'),
        // ]));

        // $auditor->users()->save(new User([
        //     'name'  => 'Auditor',
        //     'email' => 'auditor@email.com',
        //     'phone' => '+62 999 99911',
        //     'password' => bcrypt('password'),
        //     'fungsi' => '1',
        // ]));

        // $sekre_ia->users()->save(new User([
        //     'name'  => 'Sekretaris IA',
        //     'email' => 'sekre_ia@email.com',
        //     'phone' => '+62 999 99912',
        //     'password' => bcrypt('password'),
        // ]));

        // $kapro->users()->save(new User([
        //     'name'  => 'Kepala Project',
        //     'email' => 'kapro@email.com',
        //     'phone' => '+62 999 99913',
        //     'password' => bcrypt('password'),
        // ]));

        // $kanit->users()->save(new User([
        //     'name'  => 'Kepala Unit',
        //     'email' => 'kanit@email.com',
        //     'phone' => '+62 999 99914',
        //     'password' => bcrypt('password'),
        // ]));

        // $svp->users()->save(new User([
        //     'name'  => 'SVP 01',
        //     'email' => 'svp_01@email.com',
        //     'phone' => '+62 999 99915',
        //     'password' => bcrypt('password'),
        // ]));

        // $vendor->users()->save(new User([
        //     'name'  => 'Vendor',
        //     'email' => 'vendor@email.com',
        //     'phone' => '+62 999 99916',
        //     'password' => bcrypt('password'),
        // ]));
    }
}
