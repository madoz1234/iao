<?php

use App\Models\Master\Survey;
use Illuminate\Database\Seeder;

class SurveiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $survei = [
            'version'  => '1.0',
            'status' => 2,
            'description' => 'Survei Kepuasan Audit Versi 1.0',
        ];

        $pertanyaan = [
            [
                'pertanyaan' => 'Kertas Kerja Audit (KKA) mudah dipahami',
            ],
            [
                'pertanyaan' => 'Kertas Kerja Audit (KKA) didukung dokumentasi bukti audit yang akurat',
            ],
            [
                'pertanyaan' => 'Auditor memiliki pengetahuan yang memadai atas substansi sasaran pemeriksaan',
            ],
            [
                'pertanyaan' => 'Pelaksanaan audit dilaksanakan dengan jadwal yang jelas',
            ],
            [
                'pertanyaan' => 'Auditor menunjukkan kesatuan pendapat antara sesama anggota tim selama penugasan',
            ],
            [
                'pertanyaan' => 'Auditor memahami kebutuhan organisasi (Proyek/Unis Bisnis/Unit Kerja)',
            ],
            [
                'pertanyaan' => 'Auditor memberikan saran yang konstruktif',
            ],
            [
                'pertanyaan' => 'Temuan Auditor membantu Auditi meningkatkan kepatuhan terhadap kebijakan & prosedur',
            ],
            [
                'pertanyaan' => 'Temuan Auditor membantu Auditi meningkatkan efisiensi operasional organisasi',
            ],
            [
                'pertanyaan' => 'Temuan Auditor membantu Auditi meningkatkan efektivitas operasional organisasi',
            ],
            [
                'pertanyaan' => 'Auditor mampu memberikan solusi atas permasalahan Auditi dengan tepat',
            ],
            [
                'pertanyaan' => 'Auditor menunjukkan kesediaan untuk membantu Auditi',
            ],
            [
                'pertanyaan' => 'Auditor memberikan rekomendasi/saran yang dapat ditindaklanjuti',
            ],
            [
                'pertanyaan' => 'Auditor menunjukkan perilaku yang baik',
            ],
            [
                'pertanyaan' => 'Auditor dapat menjaga kerahasiaan data informasi Auditi',
            ],
            [
                'pertanyaan' => 'Penyajian temuan dilakukan secara objektif',
            ],
            [
                'pertanyaan' => 'Auditor peduli terhadap keluahan Auditi',
            ],
            [
                'pertanyaan' => 'Auditor memberikan perhatian terhadap Auditi',
            ],
            [
                'pertanyaan' => 'Auditor dapat menciptakan suasana yang nyaman',
            ],
        ];

        $record = new Survey;
        $record->version     = $survei['version'];
        $record->status      = $survei['status'];
        $record->description = $survei['description'];
        $record->save();
        $record->pertanyaan()->createMany($pertanyaan);
    }
}
