<?php

use Illuminate\Database\Seeder;

use App\Models\Auths\User;
use Spatie\Permission\Models\Role;


class UserSeeder extends Seeder
{
    public function run()
    {

    	// create user
		$user = [
			[
				'name'  => 'Super Admin',
				'email' => 'admin@email.com',
				'phone' => '+62 999 9991',
				'password' => bcrypt('password'),
				'perms' => 'admin',
				'fungsi' => '0',
			],
			// [
			// 	'name'  => 'Direktur Utama',
			// 	'email' => 'dirut@email.com',
			// 	'phone' => '+62 999 9992',
			// 	'password' => bcrypt('password'),
			// 	'perms' => 'dirut',
			// 	'fungsi' => '0',
			// ],
			// [
			// 	'name'  => 'Board of Commissioners',
			// 	'email' => 'boc@email.com',
			// 	'phone' => '+62 999 9993',
			// 	'password' => bcrypt('password'),
			// 	'perms' => 'boc',
			// 	'fungsi' => '0',
			// ],
			// [
			// 	'name'  => 'Board of Directors',
			// 	'email' => 'bod@email.com',
			// 	'phone' => '+62 999 9994',
			// 	'password' => bcrypt('password'),
			// 	'perms' => 'bod',
			// 	'fungsi' => '0',
			// ],
			// [
			// 	'name'  => 'Sekretaris Dirut',
			// 	'email' => 'sekre_dirut@email.com',
			// 	'phone' => '+62 999 9995',
			// 	'password' => bcrypt('password'),
			// 	'perms' => 'sekre-dirut',
			// 	'fungsi' => '0',
			// ],
			// [
			// 	'name'  => 'Corporate Secretary',
			// 	'email' => 'corsec@email.com',
			// 	'phone' => '+62 999 9996',
			// 	'password' => bcrypt('password'),
			// 	'perms' => 'corsec',
			// 	'fungsi' => '0',
			// ],
			// [
			// 	'name'  => 'BPK',
			// 	'email' => 'bpk@email.com',
			// 	'phone' => '+62 999 9997',
			// 	'password' => bcrypt('password'),
			// 	'perms' => 'bpk',
			// 	'fungsi' => '0',
			// ],
			// [
			// 	'name'  => 'SVP Audit',
			// 	'email' => 'svp_audit_01@email.com',
			// 	'phone' => '+62 999 9998',
			// 	'password' => bcrypt('password'),
			// 	'perms' => 'svp-audit',
			// 	'fungsi' => '0',
			// ],
			// [
			// 	'name'  => 'Leader Team',
			// 	'email' => 'leader_team@email.com',
			// 	'phone' => '+62 999 9999',
			// 	'password' => bcrypt('password'),
			// 	'perms' => 'leader-team',
			// 	'fungsi' => '0',
			// ],
			// [	
			// 	'name'  => 'Auditor',
			// 	'email' => 'auditor@email.com',
			// 	'phone' => '+62 999 99911',
			// 	'password' => bcrypt('password'),
			// 	'perms' => 'auditor',
			// 	'fungsi' => '1',
			// ],
			// [
			// 	'name'  => 'Sekretaris IA',
			// 	'email' => 'sekre_ia@email.com',
			// 	'phone' => '+62 999 99912',
			// 	'password' => bcrypt('password'),
			// 	'perms' => 'sekre-ia',
			// 	'fungsi' => '0',
			// ],
			// [
			// 	'name'  => 'Kepala Project',
			// 	'email' => 'kapro@email.com',
			// 	'phone' => '+62 999 99913',
			// 	'password' => bcrypt('password'),
			// 	'perms' => 'kapro',
			// 	'fungsi' => '0',
			// ],
			// [
			// 	'name'  => 'Kepala Unit',
			// 	'email' => 'kanit@email.com',
			// 	'phone' => '+62 999 99914',
			// 	'password' => bcrypt('password'),
			// 	'perms' => 'kanit',
			// 	'fungsi' => '0',
			// ],
			// [
			// 	'name'  => 'SVP 01',
			// 	'email' => 'svp_01@email.com',
			// 	'phone' => '+62 999 99915',
			// 	'password' => bcrypt('password'),
			// 	'perms' => 'svp',
			// 	'fungsi' => '0',
			// ],
			// [
			// 	'name'  => 'Vendor',
			// 	'email' => 'vendor@email.com',
			// 	'phone' => '+62 999 99916',
			// 	'password' => bcrypt('password'),
			// 	'perms' => 'vendor',
			// 	'fungsi' => '0',
			// ]
		];
		
		foreach($user as $data){
			$admin = new User();
			$admin->name   = $data['name'];
			$admin->password   = $data['password'];
			$admin->email   = $data['email'];
			$admin->phone = $data['phone'];
			$admin->fungsi = $data['fungsi'];
			$admin->save();
			$role_admin = Role::findByName($data['perms']);
			$admin->roles()->sync([$role_admin->id]);
		}
    }
}
