<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// Reset cached roles and permissions
		app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
		
    	// create permissions
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('sys_role_has_permissions')->truncate();
        DB::table('sys_role_has_permissions')->truncate();
        DB::table('sys_model_has_roles')->truncate();
        DB::table('sys_permissions')->truncate();
        DB::table('sys_roles')->truncate();
        DB::table('sys_users')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    	
    	$permissions = [
    		// ------------- DASHBOARD ---------------
    		[
				'name'         => 'dashboard',
				// 'guard_name' => 'Dashboard',
				'action'       => ['view'],
    		],

            // ------------- RENCANA AUDIT ---------------
            [
                'name'         => 'rkia',
                // 'guard_name' => 'Rencana Audit PKAT',
                'action'       => ['view', 'create', 'edit', 'delete'],
            ],
            // ------------- PERSIAPAN AUDIT ---------------
            [
                'name'         => 'persiapan-audit-penugasan',
                // 'guard_name' => 'Persiapan Audit Program',
                'action'       => ['view', 'create', 'edit', 'delete'],
            ],
            [
                'name'         => 'persiapan-audit-tinjauan',
                // 'guard_name' => 'Persiapan Audit Penugasan',
                'action'       => ['view', 'create', 'edit', 'delete'],
            ],
            [
                'name'         => 'persiapan-audit-program',
                // 'guard_name' => 'Persiapan Audit Tinjauan',
                'action'       => ['view', 'create', 'edit', 'delete'],
            ],
            // ------------- PELAKSANAAN ---------------
            [
                'name'         => 'pelaksanaan-opening',
                // 'guard_name' => 'Pelaksanaan Opening Meeting',
                'action'       => ['view', 'create', 'edit', 'delete'],
            ],
            [
                'name'         => 'pelaksanaan-kka',
                // 'guard_name' => 'Pelaksanaan Draft KKA',
                'action'       => ['view', 'create', 'edit', 'delete'],
            ],
            [
                'name'         => 'pelaksanaan-closing',
                // 'guard_name' => 'Pelaksanaan Closing Meeting',
                'action'       => ['view', 'create', 'edit', 'delete'],
            ],
            [
                'name'         => 'pelaksanaan-lha',
                // 'guard_name' => 'Pelaksanaan LHA',
                'action'       => ['view', 'create', 'edit', 'delete'],
            ],
            [
                'name'         => 'pelaksanaan-surat-perintah',
                // 'guard_name' => 'Pelaksanaan Surat Perintah Tindak Lanjut',
                'action'       => ['view', 'create', 'edit', 'delete'],
            ],
            [
                'name'         => 'pelaksanaan-laporan-tl',
                // 'guard_name' => 'Pelaksanaan Laporan Tindak Lanjut',
                'action'       => ['view', 'create', 'edit', 'delete'],
            ],
            // ------------- REGISTER TINDAK LANJUT ---------------
            [
                'name'         => 'monitoring-tl',
                // 'guard_name' => 'Register Tindak Lanjut',
                'action'       => ['view', 'create', 'edit', 'delete'],
            ],
            // ------------- REGISTER TINDAK LANJUT ---------------
            [
                'name'         => 'survei',
                // 'guard_name' => 'Survei Kepuasan',
                'action'       => ['view', 'create', 'edit', 'delete'],
            ],
            // ------------- MASTER ---------------
            [
                'name'         => 'master-bu',
                // 'guard_name' => 'Master Business Unit (BU)',
                'action'       => ['view', 'create', 'edit', 'delete'],
            ],
            [
                'name'         => 'master-co',
                // 'guard_name' => 'Master Corporate Office (CO)',
                'action'       => ['view', 'create', 'edit', 'delete'],
            ],
            [
                'name'         => 'master-project',
                // 'guard_name' => 'Master Project',
                'action'       => ['view', 'create', 'edit', 'delete'],
            ],
            [
                'name'         => 'master-anak-perusahaan',
                // 'guard_name' => 'Master Anak Perusahaan',
                'action'       => ['view', 'create', 'edit', 'delete'],
            ],
            [
                'name'         => 'master-dokumen',
                // 'guard_name' => 'Master Dokumen Pendahuluan',
                'action'       => ['view', 'create', 'edit', 'delete'],
            ],
            [
                'name'         => 'master-dateline',
                // 'guard_name' => 'Master Dateline',
                'action'       => ['view', 'create', 'edit', 'delete'],
            ],
            [
                'name'         => 'master-survei',
                // 'guard_name' => 'Master Survei',
                'action'       => ['view', 'create', 'edit', 'delete'],
            ],
            [
                'name'         => 'master-fokus-audit',
                // 'guard_name' => 'Master Fokus Audit',
                'action'       => ['view', 'create', 'edit', 'delete'],
            ],
    	];
    	foreach ($permissions as $row) {
    		foreach ($row['action'] as $key => $val) {
    			$temp = [
					'name'         => $row['name'].'-'.$val,
    			];	
				Permission::create($temp);
    		}
		}
		
		$role1 = Role::create(['name' => 'admin']);
        $role1->givePermissionTo(Permission::all());
        
		$role2 = Role::create(['name' => 'dirut']);
        $role2->givePermissionTo(Permission::all());
        
		$role3 = Role::create(['name' => 'boc']);
        $role3->givePermissionTo(Permission::all());
        
		$role4 = Role::create(['name' => 'bod']);
        $role4->givePermissionTo(Permission::all());
        
		$role5 = Role::create(['name' => 'sekre-dirut']);
        $role5->givePermissionTo(Permission::all());
        
		$role6 = Role::create(['name' => 'corsec']);
        $role6->givePermissionTo(Permission::all());
        
		$role7 = Role::create(['name' => 'bpk']);
        $role7->givePermissionTo(Permission::all());
        
		$role8 = Role::create(['name' => 'svp-audit']);
        $role8->givePermissionTo(Permission::all());
        
		$role9 = Role::create(['name' => 'leader-team']);
        $role9->givePermissionTo(Permission::all());
        
		$role10 = Role::create(['name' => 'auditor']);
        $role10->givePermissionTo(Permission::all());
        
		$role11 = Role::create(['name' => 'sekre-ia']);
        $role11->givePermissionTo(Permission::all());
        
		$role12 = Role::create(['name' => 'kapro']);
        $role12->givePermissionTo(Permission::all());
        
        $role12 = Role::create(['name' => 'kanit']);
        $role12->givePermissionTo(Permission::all());

        $role13 = Role::create(['name' => 'svp']);
        $role13->givePermissionTo(Permission::all());

        $role14 = Role::create(['name' => 'sendor']);
        $role14->givePermissionTo(Permission::all());
    }
}
