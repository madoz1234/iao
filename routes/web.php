<?php

Route::get('/', function () {
    return redirect()->route('idproo.login');
});

Route::get('login', 'Auth\LoginController@loginIdProo');
Route::get('auth', 'Auth\LoginController@callback')->name('idproo.auth');
Route::get('login-check', 'Auth\LoginController@loginIdProo')->name('idproo.login');

Route::get('absensi', 'Auth\AbsensiController@showLoginForm')->name('absensi.form');
Route::post('absensi', 'Auth\AbsensiController@login')->name('absensi');
Route::post('absensi-auth', 'Auth\AbsensiController@callback')->name('absensi.auth');
Route::get('absensi-check', 'Auth\AbsensiController@loginIdProo')->name('absensi.check');

Route::middleware('absensi')->group(function() {
	Route::get('in/{id}/{id_real}', 'DaftarHadirInternalController@showing')->name('in.showing');
	Route::get('in/store', 'DaftarHadirInternalController@store')->name('in.store');
});

Route::get('mail', function(){
    $details['email'] = 'wahid.pragma@gmail.com';
    dispatch(new App\Jobs\SendEmail($details));
    return "Email telah dikirim";
});

Route::get('download-template', 'DownloadController@va');

Route::get('ex/{id}/{id_real}', 'DaftarHadirController@showing')->name('ex.showing');
Route::post('ex/store', 'DaftarHadirController@store')->name('ex.store');

Route::name('e.')->prefix('e')->group( function() {
	Route::resource('/', 'DaftarHadirController');
});

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home', function(){
  return redirect()->route('dashboard.home.index');
});

Route::middleware('auth')->group(function() {
	Route::name('dashboard.')->prefix('dashboard')->namespace('Dashboard')->group( function() {
		Route::get('/', function(){
			return redirect()->route('dashboard.home.index');
		});
		Route::post('home/gridTahap', 'HomeController@gridTahap')->name('home.gridTahap');
		Route::post('home/gridProgress', 'HomeController@gridProgress')->name('home.gridProgress');
		Route::post('home/gridKegiatan', 'HomeController@gridKegiatan')->name('home.gridKegiatan');
		Route::post('home/gridTemuan', 'HomeController@gridTemuan')->name('home.gridTemuan');
		Route::post('home/gridTemuans', 'HomeController@gridTemuans')->name('home.gridTemuans');
		Route::post('home/gridTl', 'HomeController@gridTl')->name('home.gridTl');
		Route::post('home/gridTls', 'HomeController@gridTls')->name('home.gridTls');
		Route::resource('home', 'HomeController');
	});

	// Route::get('in/{id}/{id_real}', 'DaftarHadirInternalController@showing')->name('in.showing');
	// Route::post('in/store', 'DaftarHadirInternalController@store')->name('in.store');

	Route::group(['prefix' => 'ajax/option', 'namespace' => 'Ajax'], function(){
	    Route::post('fokus-audit', 'ChainOptionController@getFokusAudit');
	    Route::post('get-kategori', 'ChainOptionController@getKategori');
	    Route::post('get-kategoris', 'ChainOptionController@getKategoris');
	    Route::post('get-kategoriz', 'ChainOptionController@getKategoriz');
	    Route::post('get-langkah', 'ChainOptionController@getLangkah');
	    Route::post('get-pof', 'ChainOptionController@getPof');
	    Route::post('get-bobot', 'ChainOptionController@getBobot');
	    Route::post('get-lokasi', 'ChainOptionController@getLokasi');
	    Route::post('get-noAb', 'ChainOptionController@getNoAb');
	    Route::post('get-noId', 'ChainOptionController@getNoId');
	    Route::post('get-kriteria-temuan', 'ChainOptionController@getKriteriaTemuan');
	    Route::post('get-kriteria-temuans', 'ChainOptionController@getKriteriaTemuans');
	    Route::post('get-detil-standarisasi', 'ChainOptionController@getStandarisasi');
	    Route::post('get-standarisasis', 'ChainOptionController@getStandarisasis');
	    Route::post('get-standarisasiss', 'ChainOptionController@getStandarisasiss');
	    Route::post('get-kode-temuan', 'ChainOptionController@getKodeTemuan');
	    Route::post('get-user', 'ChainOptionController@getUser');
	    Route::post('get-project', 'ChainOptionController@getProject');
	    Route::post('get-roles', 'ChainOptionController@getRoles');
	    Route::post('get-file', 'ChainOptionController@getFile');
	    Route::post('get-kka', 'ChainOptionController@getFileKka');
	    Route::post('get-tl', 'ChainOptionController@getFileTL');
	    Route::post('get-ba', 'ChainOptionController@getFileBA');
	    Route::post('get-alldatadashboard', 'ChainOptionController@getAllDataDashboard');
	    Route::post('cek-data', 'ChainOptionController@getData');
	    Route::post('cek-datas', 'ChainOptionController@getDatas');
	    // Route::post('get-alldatatahap', 'ChainOptionController@getAllDataTahap');
	});

	Route::name('setting.')->prefix('setting')->namespace('Setting')->group( function() {
		Route::get('/', function(){
			return redirect()->route('setting.users.index');
		});

		Route::post('users/grid', 'UserController@grid')->name('users.grid');
		Route::get('users/clear','UserController@clear')->name('users.clear');
		Route::resource('users', 'UserController');

		Route::post('log/grid', 'LogController@grid')->name('log.grid');
		Route::resource('log', 'LogController');

		Route::post('roles/grid', 'RoleController@grid')->name('roles.grid');
		Route::patch('roles/saveData/{id}','RoleController@saveData')->name('roles.saveData');
		Route::resource('roles', 'RoleController');
	});

	Route::name('rkia.')->prefix('rkia')->namespace('RKIA')->group( function() {
	    Route::post('rkia/grid', 'RKIAController@grid')->name('rkia.grid');
		Route::post('rkia/onProgress', 'RKIAController@onProgress')->name('rkia.onProgress');
		Route::post('rkia/historis', 'RKIAController@historis')->name('rkia.historis');
		Route::post('rkia/{id}/approval/{tipe}','RKIAController@approval')->name('rkia.approval');
		Route::get('rkia/{id}/detil','RKIAController@detil')->name('rkia.detil');
		Route::get('rkia/{id}/updateObject','RKIAController@updateObject')->name('rkia.updateObject');
		Route::get('rkia/{id}/revisi','RKIAController@revisi')->name('rkia.revisi');
		Route::patch('rkia/{id}/saveObject','RKIAController@saveObject')->name('rkia.saveObject');
		Route::patch('rkia/{id}/saveReject','RKIAController@saveReject')->name('rkia.saveReject');
		Route::patch('rkia/{id}/saveReject2','RKIAController@saveReject2')->name('rkia.saveReject2');
		Route::patch('rkia/{id}/saveUpload','RKIAController@saveUpload')->name('rkia.saveUpload');
		Route::patch('rkia/{id}/saveRevisi','RKIAController@saveRevisi')->name('rkia.saveRevisi');
		Route::get('rkia/{id}/reject','RKIAController@reject')->name('rkia.reject');
		Route::get('rkia/{id}/reject2','RKIAController@reject2')->name('rkia.reject2');
		Route::get('rkia/{id}/final','RKIAController@uploadFinal')->name('rkia.final');
		Route::get('rkia/{id}/preview','RKIAController@preview')->name('rkia.preview');
		Route::get('rkia/{id}/cetak','RKIAController@cetak')->name('rkia.cetak');
		Route::get('rkia/{id}/downloadAudit','RKIAController@downloadAudit')->name('rkia.downloadAudit');
		// Route::resource('rkia', 'RKIAController')->parameters([
		//     'rkia' => 'rkia'
		// ]);
		Route::resource('rkia', 'RKIAController');
	});

	Route::name('monitoring.')->prefix('monitoring')->namespace('Monitoring')->group( function() {
	    Route::post('monitoring/grid', 'MonitoringController@grid')->name('monitoring.grid');
		Route::resource('monitoring', 'MonitoringController');
	});

	Route::name('kegiatan-audit.')->prefix('kegiatan-audit')->namespace('KegiatanAudit')->group( function() {
		Route::name('persiapan.')->prefix('persiapan')->namespace('Persiapan')->group( function() {
			//Surat Penugasan
			Route::post('penugasan/grid', 'PenugasanAuditController@grid')->name('penugasan.grid');
			Route::post('penugasan/onProgress', 'PenugasanAuditController@onProgress')->name('penugasan.onProgress');
			Route::post('penugasan/historis', 'PenugasanAuditController@historis')->name('penugasan.historis');
			Route::get('penugasan/{id}/buat','PenugasanAuditController@buat')->name('penugasan.buat');
			Route::get('penugasan/{id}/reject','PenugasanAuditController@reject')->name('penugasan.reject');
			Route::get('penugasan/{id}/rejectDirut','PenugasanAuditController@rejectDirut')->name('penugasan.rejectDirut');
			Route::patch('penugasan/{id}/saveReject','PenugasanAuditController@saveReject')->name('penugasan.saveReject');
			Route::patch('penugasan/{id}/saveRejectDirut','PenugasanAuditController@saveRejectDirut')->name('penugasan.saveRejectDirut');
			Route::get('penugasan/{id}/edit','PenugasanAuditController@edit')->name('penugasan.edit');
			Route::get('penugasan/{id}/detilProject','PenugasanAuditController@detilProject')->name('penugasan.detilProject');
			Route::get('penugasan/{id}/detilPenugasan','PenugasanAuditController@detilPenugasan')->name('penugasan.detilPenugasan');
			Route::get('penugasan/{id}/detil','PenugasanAuditController@detil')->name('penugasan.detil');
			Route::get('penugasan/{id}/surat','PenugasanAuditController@surat')->name('penugasan.surat');
			Route::get('penugasan/{id}/preview','PenugasanAuditController@preview')->name('penugasan.preview');
			Route::get('penugasan/{id}/final','PenugasanAuditController@uploadFinal')->name('penugasan.final');
			Route::get('penugasan/{id}/approve','PenugasanAuditController@approve')->name('penugasan.approve');
			Route::get('penugasan/{id}/approveSvpDetail','PenugasanAuditController@approveSvpDetail')->name('penugasan.approveSvpDetail');
			Route::get('penugasan/{id}/approveDirutDetail','PenugasanAuditController@approveDirutDetail')->name('penugasan.approveDirutDetail');
			Route::patch('penugasan/{id}/approveDirut','PenugasanAuditController@approveDirut')->name('penugasan.approveDirut');
			Route::patch('penugasan/{id}/saveApproval','PenugasanAuditController@saveApproval')->name('penugasan.saveApproval');
			Route::patch('penugasan/{id}/approveSvp','PenugasanAuditController@approveSvp')->name('penugasan.approveSvp');
			Route::patch('penugasan/{id}/updateProgram','PenugasanAuditController@updateProgram')->name('penugasan.updateProgram');
			Route::patch('penugasan/{id}/updateSurat','PenugasanAuditController@updateSurat')->name('penugasan.updateSurat');
			Route::patch('penugasan/{id}/saveFinal','PenugasanAuditController@saveFinal')->name('penugasan.saveFinal');
			Route::get('penugasan/{id}/cetak','PenugasanAuditController@cetak')->name('penugasan.cetak');
			Route::get('penugasan/{id}/cetakAudit','PenugasanAuditController@cetakAudit')->name('penugasan.cetakAudit');
			Route::get('penugasan/{id}/cetakExcelPenugasan','PenugasanAuditController@cetakExcelPenugasan')->name('penugasan.cetakExcelPenugasan');
			Route::get('penugasan/{id}/downloadAudit','PenugasanAuditController@downloadAudit')->name('penugasan.downloadAudit');
			Route::get('penugasan/{id}/cetakExcelPenugasan','PenugasanAuditController@cetakExcelPenugasan')->name('penugasan.cetakExcelPenugasan');
			Route::resource('penugasan', 'PenugasanAuditController');

			//Tinjauan Dokumen
			Route::post('tinjauan-dokumen/grid', 'TinjauanDokumenController@grid')->name('tinjauan-dokumen.grid');
			Route::post('tinjauan-dokumen/onProgress', 'TinjauanDokumenController@onProgress')->name('tinjauan-dokumen.onProgress');
			Route::post('tinjauan-dokumen/historis', 'TinjauanDokumenController@historis')->name('tinjauan-dokumen.historis');
			Route::get('tinjauan-dokumen/{id}/detil','TinjauanDokumenController@detil')->name('tinjauan-dokumen.detil');
			Route::get('tinjauan-dokumen/{id}/buat','TinjauanDokumenController@buat')->name('tinjauan-dokumen.buat');
			Route::post('tinjauan-dokumen/{id}/approve-svp','TinjauanDokumenController@approveSvp')->name('tinjauan-dokumen.approveSvp');
			Route::get('tinjauan-dokumen/{id}/approval','TinjauanDokumenController@approval')->name('tinjauan-dokumen.approval');
			Route::get('tinjauan-dokumen/{id}/kirim','TinjauanDokumenController@kirim')->name('tinjauan-dokumen.kirim');
			Route::get('tinjauan-dokumen/{id}/upload-dokumen','TinjauanDokumenController@uploadDokumen')->name('tinjauan-dokumen.uploadDokumen');
			Route::patch('tinjauan-dokumen/{id}/simpanDokumen','TinjauanDokumenController@simpanDokumen')->name('tinjauan-dokumen.simpanDokumen');
			Route::patch('tinjauan-dokumen/simpanData/{id}','TinjauanDokumenController@simpanData')->name('tinjauan-dokumen.simpanData');
			Route::patch('tinjauan-dokumen/{id}/saveApproval','TinjauanDokumenController@saveApproval')->name('tinjauan-dokumen.saveApproval');
			Route::get('tinjauan-dokumen/{id}/ubahData','TinjauanDokumenController@ubahData')->name('tinjauan-dokumen.ubahData');
			Route::get('tinjauan-dokumen/{id}/reject','TinjauanDokumenController@reject')->name('tinjauan-dokumen.reject');
			Route::patch('tinjauan-dokumen/{id}/saveReject','TinjauanDokumenController@saveReject')->name('tinjauan-dokumen.saveReject');
			Route::get('tinjauan-dokumen/{id}/get-file','TinjauanDokumenController@getFile')->name('tinjauan-dokumen.getFile');
			Route::get('tinjauan-dokumen/{id}/draftSurat','TinjauanDokumenController@draftSurat')->name('tinjauan-dokumen.draftSurat');
			Route::get('tinjauan-dokumen/{id}/draftFinal','TinjauanDokumenController@draftFinal')->name('tinjauan-dokumen.draftFinal');
			Route::get('tinjauan-dokumen/{id}/detilProject','TinjauanDokumenController@detilProject')->name('tinjauan-dokumen.detilProject');
			Route::get('tinjauan-dokumen/{id}/detilProgram','TinjauanDokumenController@detilProgram')->name('tinjauan-dokumen.detilProgram');
			Route::get('tinjauan-dokumen/{id}/detilTinjauan','TinjauanDokumenController@detilTinjauan')->name('tinjauan-dokumen.detilTinjauan');
			Route::get('tinjauan-dokumen/{id}/dokumenPenugasan','TinjauanDokumenController@dokumenPenugasan')->name('tinjauan-dokumen.dokumenPenugasan');
			Route::get('tinjauan-dokumen/{id}/cetak','TinjauanDokumenController@cetak')->name('tinjauan-dokumen.cetak');
			Route::get('tinjauan-dokumen/{id}/cetakAudit','TinjauanDokumenController@cetakAudit')->name('tinjauan-dokumen.cetakAudit');
			Route::get('tinjauan-dokumen/{id}/cetakPenugasan','TinjauanDokumenController@cetakPenugasan')->name('tinjauan-dokumen.cetakPenugasan');
			Route::get('tinjauan-dokumen/{id}/downloadAudit','TinjauanDokumenController@downloadAudit')->name('tinjauan-dokumen.downloadAudit');
			Route::get('tinjauan-dokumen/{id}/downloadPenugasan','TinjauanDokumenController@downloadPenugasan')->name('tinjauan-dokumen.downloadPenugasan');
			Route::resource('tinjauan-dokumen', 'TinjauanDokumenController');

			//Program Audit
			Route::post('program/grid', 'ProgramAuditController@grid')->name('program.grid');
			Route::post('program/onProgress', 'ProgramAuditController@onProgress')->name('program.onProgress');
			Route::post('program/historis', 'ProgramAuditController@historis')->name('program.historis');
			Route::get('program/{id}/buat','ProgramAuditController@buat')->name('program.buat');
			Route::get('program/{id}/buatFokus','ProgramAuditController@buatFokus')->name('program.buatFokus');
			Route::get('program/{id}/ubah','ProgramAuditController@ubah')->name('program.ubah');
			Route::get('program/{id}/ubahFokus','ProgramAuditController@ubahFokus')->name('program.ubahFokus');
			Route::get('program/{id}/detil','ProgramAuditController@detil')->name('program.detil');
			Route::patch('program/{id}/updateProgram','ProgramAuditController@updateProgram')->name('program.updateProgram');
			Route::post('program/{id}/approveSvp','ProgramAuditController@approveSvp')->name('program.approveSvp');
			Route::get('program/{id}/detilProject','ProgramAuditController@detilProject')->name('program.detilProject');
			Route::get('program/{id}/detilPenugasan','ProgramAuditController@detilPenugasan')->name('program.detilPenugasan');
			Route::get('program/{id}/detilTinjauan','ProgramAuditController@detilTinjauan')->name('program.detilTinjauan');
			Route::get('program/{id}/detilProgram','ProgramAuditController@detilProgram')->name('program.detilProgram');
			Route::get('program/{id}/cetakAudit','ProgramAuditController@cetakAudit')->name('program.cetakAudit');
			Route::get('program/{id}/cetakPenugasan','ProgramAuditController@cetakPenugasan')->name('program.cetakPenugasan');
			Route::get('program/{id}/cetak','ProgramAuditController@cetak')->name('program.cetak');
			Route::get('program/{id}/jadwalAudit','ProgramAuditController@jadwalAudit')->name('program.jadwalAudit');
			Route::get('program/{id}/downloadAudit','ProgramAuditController@downloadAudit')->name('program.downloadAudit');
			Route::get('program/{id}/downloadPenugasan','ProgramAuditController@downloadPenugasan')->name('program.downloadPenugasan');
			Route::get('program/{id}/upload','ProgramAuditController@upload')->name('program.upload');
			Route::get('program/{id}/ubahTim','ProgramAuditController@ubahTim')->name('program.ubahTim');
			Route::get('program/{id}/ubahRealisasi','ProgramAuditController@ubahRealisasi')->name('program.ubahRealisasi');
			Route::patch('program/{id}/saveUpload','ProgramAuditController@saveUpload')->name('program.saveUpload');
			Route::patch('program/{id}/saveUbahTim','ProgramAuditController@saveUbahTim')->name('program.saveUbahTim');
			Route::patch('program/{id}/saveUbahRealisasi','ProgramAuditController@saveUbahRealisasi')->name('program.saveUbahRealisasi');
			Route::patch('program/{id}/updateFokus','ProgramAuditController@updateFokus')->name('program.updateFokus');
			Route::get('program/{id}/reject','ProgramAuditController@reject')->name('program.reject');
			Route::patch('program/{id}/saveReject','ProgramAuditController@saveReject')->name('program.saveReject');
			Route::resource('program', 'ProgramAuditController');
		});

		Route::name('pelaksanaan.')->prefix('pelaksanaan')->namespace('Pelaksanaan')->group( function() {
			//Opening Meeting
			Route::post('opening-meeting/grid', 'OpeningMeetingController@grid')->name('opening-meeting.grid');
			Route::post('opening-meeting/onProgress', 'OpeningMeetingController@onProgress')->name('opening-meeting.onProgress');
			Route::post('opening-meeting/historis', 'OpeningMeetingController@historis')->name('opening-meeting.historis');
			Route::get('opening-meeting/{id}/detil','OpeningMeetingController@detil')->name('opening-meeting.detil');
			Route::get('opening-meeting/{id}/buat','OpeningMeetingController@buat')->name('opening-meeting.buat');
			Route::get('opening-meeting/{id}/ubah','OpeningMeetingController@ubah')->name('opening-meeting.ubah');
			Route::get('opening-meeting/{id}/detilOpening','OpeningMeetingController@detilOpening')->name('opening-meeting.detilOpening');
			Route::get('opening-meeting/{id}/detilProject','OpeningMeetingController@detilProject')->name('opening-meeting.detilProject');
			Route::get('opening-meeting/{id}/detilPenugasan','OpeningMeetingController@detilPenugasan')->name('opening-meeting.detilPenugasan');
			Route::get('opening-meeting/{id}/detilTinjauan','OpeningMeetingController@detilTinjauan')->name('opening-meeting.detilTinjauan');
			Route::get('opening-meeting/{id}/detilProgram','OpeningMeetingController@detilProgram')->name('opening-meeting.detilProgram');
			Route::get('opening-meeting/{id}/cetakAudit','OpeningMeetingController@cetakAudit')->name('opening-meeting.cetakAudit');
			Route::get('opening-meeting/{id}/cetakPenugasan','OpeningMeetingController@cetakPenugasan')->name('opening-meeting.cetakPenugasan');
			Route::get('opening-meeting/{id}/downloadAudit','OpeningMeetingController@downloadAudit')->name('opening-meeting.downloadAudit');
			Route::get('opening-meeting/{id}/downloadPenugasan','OpeningMeetingController@downloadPenugasan')->name('opening-meeting.downloadPenugasan');
			Route::get('opening-meeting/{id}/downloadProgramAudit','OpeningMeetingController@downloadProgramAudit')->name('opening-meeting.downloadProgramAudit');
			Route::resource('opening-meeting', 'OpeningMeetingController');

			//Draft Kka
			Route::post('draft-kka/grid', 'DraftKkaController@grid')->name('draft-kka.grid');
			Route::post('draft-kka/onProgress', 'DraftKkaController@onProgress')->name('draft-kka.onProgress');
			Route::post('draft-kka/historis', 'DraftKkaController@historis')->name('draft-kka.historis');
			Route::get('draft-kka/{id}/detil','DraftKkaController@detil')->name('draft-kka.detil');
			Route::get('draft-kka/{id}/buat','DraftKkaController@buat')->name('draft-kka.buat');
			Route::get('draft-kka/{id}/ubah','DraftKkaController@ubah')->name('draft-kka.ubah');
			Route::get('draft-kka/{id}/ubahPersetujuan','DraftKkaController@ubahPersetujuan')->name('draft-kka.ubahPersetujuan');
			Route::get('draft-kka/{id}/ubahKKA','DraftKkaController@ubahKKA')->name('draft-kka.ubahKKA');
			Route::get('draft-kka/{id}/cetak','DraftKkaController@cetak')->name('draft-kka.cetak');
			Route::get('draft-kka/{id}/detilProject','DraftKkaController@detilProject')->name('draft-kka.detilProject');
			Route::get('draft-kka/{id}/detilPenugasan','DraftKkaController@detilPenugasan')->name('draft-kka.detilPenugasan');
			Route::get('draft-kka/{id}/detilTinjauan','DraftKkaController@detilTinjauan')->name('draft-kka.detilTinjauan');
			Route::get('draft-kka/{id}/detilProgram','DraftKkaController@detilProgram')->name('draft-kka.detilProgram');
			Route::get('draft-kka/{id}/detilDraft','DraftKkaController@detilDraft')->name('draft-kka.detilDraft');
			Route::post('draft-kka/{id}/submit','DraftKkaController@submit')->name('draft-kka.submit');
			Route::post('draft-kka/{id}/approval','DraftKkaController@approval')->name('draft-kka.approval');
			Route::get('draft-kka/{id}/cetakAudit','DraftKkaController@cetakAudit')->name('draft-kka.cetakAudit');
			Route::get('draft-kka/{id}/cetakPenugasan','DraftKkaController@cetakPenugasan')->name('draft-kka.cetakPenugasan');
			Route::get('draft-kka/{id}/cetakDraft','DraftKkaController@cetakDraft')->name('draft-kka.cetakDraft');
			Route::get('draft-kka/{id}/downloadAudit','DraftKkaController@downloadAudit')->name('draft-kka.downloadAudit');
			Route::get('draft-kka/{id}/downloadPenugasan','DraftKkaController@downloadPenugasan')->name('draft-kka.downloadPenugasan');
			Route::get('draft-kka/{id}/downloadProgramAudit','DraftKkaController@downloadProgramAudit')->name('draft-kka.downloadProgramAudit');
			Route::get('draft-kka/{id}/upload','DraftKkaController@upload')->name('draft-kka.upload');
			Route::patch('draft-kka/{id}/saveUpload','DraftKkaController@saveUpload')->name('draft-kka.saveUpload');
			Route::patch('draft-kka/{id}/saveUbah','DraftKkaController@saveUbah')->name('draft-kka.saveUbah');
			Route::patch('draft-kka/{id}/saveUbahKKA','DraftKkaController@saveUbahKKA')->name('draft-kka.saveUbahKKA');
			Route::get('draft-kka/{id}/reject','DraftKkaController@reject')->name('draft-kka.reject');
			Route::patch('draft-kka/{id}/saveReject','DraftKkaController@saveReject')->name('draft-kka.saveReject');
			Route::resource('draft-kka', 'DraftKkaController');

			Route::post('closing-meeting/grid', 'ClosingMeetingController@grid')->name('closing-meeting.grid');
			Route::post('closing-meeting/onProgress', 'ClosingMeetingController@onProgress')->name('closing-meeting.onProgress');
			Route::post('closing-meeting/historis', 'ClosingMeetingController@historis')->name('closing-meeting.historis');
			Route::get('closing-meeting/{id}/ubah','ClosingMeetingController@ubah')->name('closing-meeting.ubah');
			Route::get('closing-meeting/{id}/detilProject','ClosingMeetingController@detilProject')->name('closing-meeting.detilProject');
			Route::get('closing-meeting/{id}/detilPenugasan','ClosingMeetingController@detilPenugasan')->name('closing-meeting.detilPenugasan');
			Route::get('closing-meeting/{id}/detilTinjauan','ClosingMeetingController@detilTinjauan')->name('closing-meeting.detilTinjauan');
			Route::get('closing-meeting/{id}/detilProgram','ClosingMeetingController@detilProgram')->name('closing-meeting.detilProgram');
			Route::get('closing-meeting/{id}/detilDraft','ClosingMeetingController@detilDraft')->name('closing-meeting.detilDraft');
			Route::get('closing-meeting/{id}/detilClosing','ClosingMeetingController@detilClosing')->name('opening-meeting.detilClosing');
			// Route::post('closing-meeting/{id}/closing','ClosingMeetingController@closing')->name('closing-meeting.closing');
			Route::get('closing-meeting/{id}/buat','ClosingMeetingController@buat')->name('closing-meeting.buat');
			Route::get('closing-meeting/{id}/cetakAudit','ClosingMeetingController@cetakAudit')->name('closing-meeting.cetakAudit');
			Route::get('closing-meeting/{id}/cetakPenugasan','ClosingMeetingController@cetakPenugasan')->name('closing-meeting.cetakPenugasan');
			Route::get('closing-meeting/{id}/downloadAudit','ClosingMeetingController@downloadAudit')->name('closing-meeting.downloadAudit');
			Route::get('closing-meeting/{id}/downloadPenugasan','ClosingMeetingController@downloadPenugasan')->name('closing-meeting.downloadPenugasan');
			Route::get('closing-meeting/{id}/downloadProgramAudit','ClosingMeetingController@downloadProgramAudit')->name('closing-meeting.downloadProgramAudit');
			Route::resource('closing-meeting', 'ClosingMeetingController');

			Route::post('final-kka/grid', 'FinalKkaController@grid')->name('final-kka.grid');
			Route::get('final-kka/{id}/detilProject','FinalKkaController@detilProject')->name('final-kka.detilProject');
			Route::get('final-kka/{id}/detilPenugasan','FinalKkaController@detilPenugasan')->name('final-kka.detilPenugasan');
			Route::get('final-kka/{id}/detilTinjauan','FinalKkaController@detilTinjauan')->name('final-kka.detilTinjauan');
			Route::get('final-kka/{id}/detilProgram','FinalKkaController@detilProgram')->name('final-kka.detilProgram');
			Route::get('final-kka/{id}/detilDraft','FinalKkaController@detilDraft')->name('final-kka.detilDraft');
			Route::post('final-kka/{id}/approve','FinalKkaController@approve')->name('final-kka.approve');
			Route::get('final-kka/{id}/cetakAudit','FinalKkaController@cetakAudit')->name('final-kka.cetakAudit');
			Route::get('final-kka/{id}/cetakPenugasan','FinalKkaController@cetakPenugasan')->name('final-kka.cetakPenugasan');
			Route::resource('final-kka', 'FinalKkaController');
		});

		Route::name('pelaporan.')->prefix('pelaporan')->namespace('Pelaporan')->group( function() {
			//Draft Kka
			Route::post('lha/grid', 'LHAController@grid')->name('lha.grid');
			Route::post('lha/onProgress', 'LHAController@onProgress')->name('lha.onProgress');
			Route::post('lha/historis', 'LHAController@historis')->name('lha.historis');
			Route::get('lha/{id}/detil','LHAController@detil')->name('lha.detil');
			Route::get('lha/{id}/detilLha','LHAController@detilLha')->name('lha.detilLha');
			Route::get('lha/{id}/detilResume','LHAController@detilResume')->name('lha.detilResume');
			Route::get('lha/{id}/buat','LHAController@buat')->name('lha.buat');
			Route::get('lha/{id}/resume','LHAController@resume')->name('lha.resume');
			Route::get('lha/{id}/ubah','LHAController@ubah')->name('lha.ubah');
			Route::get('lha/{id}/detilProject','LHAController@detilProject')->name('lha.detilProject');
			Route::get('lha/{id}/detilPenugasan','LHAController@detilPenugasan')->name('lha.detilPenugasan');
			Route::get('lha/{id}/detilTinjauan','LHAController@detilTinjauan')->name('lha.detilTinjauan');
			Route::get('lha/{id}/detilProgram','LHAController@detilProgram')->name('lha.detilProgram');
			Route::get('lha/{id}/detilDraft','LHAController@detilDraft')->name('lha.detilDraft');
			Route::post('lha/{id}/approveTim','LHAController@approveTim')->name('lha.approveTim');
			Route::post('lha/{id}/approveSvp2','LHAController@approveSvp2')->name('lha.approveSvp2');
			Route::get('lha/{id}/reject','LHAController@reject')->name('lha.reject');
			Route::get('lha/{id}/reject2','LHAController@reject2')->name('lha.reject2');
			Route::patch('lha/{id}/saveReject','LHAController@saveReject')->name('lha.saveReject');
			Route::patch('lha/{id}/saveReject2','LHAController@saveReject2')->name('lha.saveReject2');
			Route::get('lha/{id}/upload-dokumen','LHAController@uploadDokumen')->name('lha.uploadDokumen');
			Route::patch('lha/{id}/simpanDokumen','LHAController@simpanDokumen')->name('lha.simpanDokumen');
			Route::patch('lha/{id}/simpanResume','LHAController@simpanResume')->name('lha.simpanResume');
			Route::get('lha/{id}/rejectdirut','LHAController@rejectdirut')->name('lha.rejectdirut');
			Route::post('lha/{id}/approveDirut','LHAController@approveDirut')->name('lha.approveDirut');
			Route::patch('lha/{id}/saveRejectDirut','LHAController@saveRejectDirut')->name('lha.saveRejectDirut');
			Route::get('lha/{id}/cetakAudit','LHAController@cetakAudit')->name('lha.cetakAudit');
			Route::get('lha/{id}/cetakPenugasan','LHAController@cetakPenugasan')->name('lha.cetakPenugasan');
			Route::get('lha/{id}/cetakLHA','LHAController@cetakLHA')->name('lha.cetakLHA');
			Route::get('lha/{id}/cetakSuratDisposisi','LHAController@cetakSuratDisposisi')->name('lha.cetakSuratDisposisi');
			Route::get('lha/{id}/cetakSurat','LHAController@cetakSurat')->name('lha.cetakSurat');
			Route::get('lha/{id}/cetakResume','LHAController@cetakResume')->name('lha.cetakResume');
			Route::get('lha/{id}/downloadAudit','LHAController@downloadAudit')->name('lha.downloadAudit');
			Route::get('lha/{id}/downloadPenugasan','LHAController@downloadPenugasan')->name('lha.downloadPenugasan');
			Route::get('lha/{id}/downloadProgramAudit','LHAController@downloadProgramAudit')->name('lha.downloadProgramAudit');
			Route::resource('lha', 'LHAController');
		});

		Route::name('tindak-lanjut.')->prefix('tindak-lanjut')->namespace('TindakLanjut')->group( function() {
			Route::post('register/grid', 'RegisterController@grid')->name('register.grid');
			Route::get('register/{id}/buat','RegisterController@buat')->name('register.buat');
			Route::get('register/{id}/ubah','RegisterController@ubah')->name('register.ubah');
			Route::get('register/{id}/udata','RegisterController@udata')->name('register.udata');
			Route::get('register/{id}/udataSvp','RegisterController@udataSvp')->name('register.udataSvp');
			Route::get('register/{id}/detil','RegisterController@detil')->name('register.detil');
			Route::get('register/{id}/cetakKka','RegisterController@cetakKka')->name('register.cetakKka');
			Route::get('register/{id}/cetakSuratDisposisi','RegisterController@cetakSuratDisposisi')->name('register.cetakSuratDisposisi');
			Route::patch('register/{id}/ubahData','RegisterController@ubahData')->name('register.ubahData');
			Route::patch('register/{id}/ubahDataApprove','RegisterController@ubahDataApprove')->name('register.ubahDataApprove');
			Route::resource('register', 'RegisterController');

			Route::post('monitoring-tl/grid', 'MonitoringController@grid')->name('monitoring-tl.grid');
			Route::get('monitoring-tl/{id}/buat','MonitoringController@buat')->name('monitoring-tl.buat');
			Route::get('monitoring-tl/{id}/ubah','MonitoringController@ubah')->name('monitoring-tl.ubah');
			Route::get('monitoring-tl/{id}/udata','MonitoringController@udata')->name('monitoring-tl.udata');
			Route::get('monitoring-tl/{id}/udataSvp','MonitoringController@udataSvp')->name('monitoring-tl.udataSvp');
			Route::get('monitoring-tl/{id}/detil','MonitoringController@detil')->name('monitoring-tl.detil');
			Route::get('monitoring-tl/{id}/cetakKka','MonitoringController@cetakKka')->name('monitoring-tl.cetakKka');
			Route::get('monitoring-tl/{id}/cetakSuratDisposisi','MonitoringController@cetakSuratDisposisi')->name('monitoring-tl.cetakSuratDisposisi');
			Route::patch('monitoring-tl/{id}/ubahData','MonitoringController@ubahData')->name('monitoring-tl.ubahData');
			Route::patch('monitoring-tl/{id}/ubahDataApprove','MonitoringController@ubahDataApprove')->name('monitoring-tl.ubahDataApprove');
			Route::get('monitoring-tl/monitoring','MonitoringController@monitoring')->name('monitoring-tl.monitoring');
			Route::resource('monitoring-tl', 'MonitoringController');
		});
	});

	// kegiatan konsultasi
	Route::name('kegiatan-konsultasi.')->prefix('kegiatan-konsultasi')->namespace('KegiatanKonsultasi')->group( function() {
		Route::post('perencanaan/grid', 'KonsultasiPerencanaanController@grid')->name('perencanaan.grid');
		Route::post('perencanaan/onProgress', 'KonsultasiPerencanaanController@onProgress')->name('perencanaan.onProgress');
		Route::post('perencanaan/historis', 'KonsultasiPerencanaanController@historis')->name('perencanaan.historis');
		Route::get('perencanaan/{rkia}/buat', 'KonsultasiPerencanaanController@create')->name('perencanaan.create');
		Route::post('perencanaan/{perencanaan}/approve', 'KonsultasiPerencanaanController@approve')->name('perencanaan.approve');
		Route::get('perencanaan/{perencanaan}/reject', 'KonsultasiPerencanaanController@formReject')->name('perencanaan.formReject');
		Route::post('perencanaan/{perencanaan}/reject', 'KonsultasiPerencanaanController@reject')->name('perencanaan.reject');
		Route::get('perencanaan/{perencanaan}/cetak', 'KonsultasiPerencanaanController@cetak')->name('perencanaan.cetak');
		Route::resource('perencanaan', 'KonsultasiPerencanaanController')->except(['create', 'destroy']);

		Route::post('pelaksanaan/grid', 'KonsultasiPelaksanaanController@grid')->name('pelaksanaan.grid');
		Route::post('pelaksanaan/onProgress', 'KonsultasiPelaksanaanController@onProgress')->name('pelaksanaan.onProgress');
		Route::post('pelaksanaan/historis', 'KonsultasiPelaksanaanController@historis')->name('pelaksanaan.historis');
		Route::get('pelaksanaan/{pelaksanaan}/buat', 'KonsultasiPelaksanaanController@create')->name('pelaksanaan.create');
		Route::post('pelaksanaan/{pelaksanaan}/store', 'KonsultasiPelaksanaanController@store')->name('pelaksanaan.store');
		Route::get('pelaksanaan/{pelaksanaan}/cetak', 'KonsultasiPelaksanaanController@cetak')->name('pelaksanaan.cetak');
		Route::resource('pelaksanaan', 'KonsultasiPelaksanaanController')->except(['create', 'store', 'destroy']);

		Route::post('laporan/grid', 'KonsultasiLaporanController@grid')->name('laporan.grid');
		Route::post('laporan/onProgress', 'KonsultasiLaporanController@onProgress')->name('laporan.onProgress');
		Route::post('laporan/historis', 'KonsultasiLaporanController@historis')->name('laporan.historis');
		Route::post('laporan/{id}/grid-risalah', 'KonsultasiLaporanController@gridRisalah')->name('laporan.gridRisalah');
		Route::get('laporan/{laporan}/risalah', 'KonsultasiLaporanController@risalah')->name('laporan.risalah');
		Route::post('laporan/{laporan}/update-risalah', 'KonsultasiLaporanController@updateRisalah')->name('laporan.updateRisalah');
		Route::get('laporan/{laporan}/cetak', 'KonsultasiLaporanController@cetak')->name('laporan.cetak');
		Route::get('laporan/{laporan}/{risalah}/uploadFile', 'KonsultasiLaporanController@formUploadFile')->name('laporan.formUploadFile');
		Route::post('laporan/{laporan}/uploadFile', 'KonsultasiLaporanController@uploadFile')->name('laporan.uploadFile');
		Route::get('laporan/{laporan}/{risalah}/downloadFile', 'KonsultasiLaporanController@downloadFile')->name('laporan.downloadFile');
		Route::resource('laporan', 'KonsultasiLaporanController')->except(['create', 'store', 'destroy']);
	});

	// kegiatan lainnya
	Route::name('kegiatan-lainnya.')->prefix('kegiatan-lainnya')->namespace('KegiatanLainnya')->group( function() {
		Route::post('perencanaan/grid', 'LainnyaPerencanaanController@grid')->name('perencanaan.grid');
		Route::post('perencanaan/onProgress', 'LainnyaPerencanaanController@onProgress')->name('perencanaan.onProgress');
		Route::post('perencanaan/historis', 'LainnyaPerencanaanController@historis')->name('perencanaan.historis');
		Route::get('perencanaan/{rkia}/buat', 'LainnyaPerencanaanController@create')->name('perencanaan.create');
		Route::post('perencanaan/{perencanaan}/approve', 'LainnyaPerencanaanController@approve')->name('perencanaan.approve');
		Route::get('perencanaan/{perencanaan}/reject', 'LainnyaPerencanaanController@formReject')->name('perencanaan.formReject');
		Route::post('perencanaan/{perencanaan}/reject', 'LainnyaPerencanaanController@reject')->name('perencanaan.reject');
		Route::get('perencanaan/{perencanaan}/cetak', 'LainnyaPerencanaanController@cetak')->name('perencanaan.cetak');
		Route::resource('perencanaan', 'LainnyaPerencanaanController')->except(['create', 'destroy']);

		Route::post('pelaksanaan/grid', 'LainnyaPelaksanaanController@grid')->name('pelaksanaan.grid');
		Route::post('pelaksanaan/onProgress', 'LainnyaPelaksanaanController@onProgress')->name('pelaksanaan.onProgress');
		Route::post('pelaksanaan/historis', 'LainnyaPelaksanaanController@historis')->name('pelaksanaan.historis');
		Route::get('pelaksanaan/{pelaksanaan}/buat', 'LainnyaPelaksanaanController@create')->name('pelaksanaan.create');
		Route::post('pelaksanaan/{pelaksanaan}/store', 'LainnyaPelaksanaanController@store')->name('pelaksanaan.store');
		Route::get('pelaksanaan/{pelaksanaan}/cetak', 'LainnyaPelaksanaanController@cetak')->name('pelaksanaan.cetak');
		Route::resource('pelaksanaan', 'LainnyaPelaksanaanController')->except(['create', 'store', 'destroy']);

		Route::post('laporan/grid', 'LainnyaLaporanController@grid')->name('laporan.grid');
		Route::post('laporan/onProgress', 'LainnyaLaporanController@onProgress')->name('laporan.onProgress');
		Route::post('laporan/historis', 'LainnyaLaporanController@historis')->name('laporan.historis');
		Route::post('laporan/{id}/grid-risalah', 'LainnyaLaporanController@gridRisalah')->name('laporan.gridRisalah');
		Route::get('laporan/{laporan}/risalah', 'LainnyaLaporanController@risalah')->name('laporan.risalah');
		Route::post('laporan/{laporan}/update-risalah', 'LainnyaLaporanController@updateRisalah')->name('laporan.updateRisalah');
		Route::get('laporan/{laporan}/cetak', 'LainnyaLaporanController@cetak')->name('laporan.cetak');
		Route::get('laporan/{laporan}/{risalah}/uploadFile', 'LainnyaLaporanController@formUploadFile')->name('laporan.formUploadFile');
		Route::post('laporan/{laporan}/uploadFile', 'LainnyaLaporanController@uploadFile')->name('laporan.uploadFile');
		Route::get('laporan/{laporan}/{risalah}/downloadFile', 'LainnyaLaporanController@downloadFile')->name('laporan.downloadFile');
		Route::resource('laporan', 'LainnyaLaporanController')->except(['create', 'store', 'destroy']);
	});

	Route::name('master.')->prefix('master')->namespace('Master')->group( function() {
        Route::post('bu/grid', 'BUController@grid')->name('bu.grid');
		Route::resource('bu', 'BUController');

		Route::post('co/grid', 'COController@grid')->name('co.grid');
		Route::resource('co', 'COController');

		Route::post('vendor/grid', 'VendorController@grid')->name('vendor.grid');
		Route::resource('vendor', 'VendorController');

        Route::post('project/importing', 'ProjectController@importing')->name('project.importing');
        Route::get('project/import', 'ProjectController@import')->name('project.import');
		Route::post('project/grid', 'ProjectController@grid')->name('project.grid');
		Route::resource('project', 'ProjectController');

		Route::post('anak-perusahaan/grid', 'AnakPerusahaanController@grid')->name('anak-perusahaan.grid');
		Route::resource('anak-perusahaan', 'AnakPerusahaanController');

		Route::post('dokumen/grid', 'DokumenController@grid')->name('dokumen.grid');
		Route::resource('dokumen', 'DokumenController');

		Route::post('survey/simpanData/{id}','SurveyController@simpanData')->name('survey.simpanData');
		Route::get('survey/{id}/buat-survey','SurveyController@buatSurvey')->name('survey.buatSurvey');
		Route::get('survey/{id}/lihat-survey','SurveyController@lihatSurvey')->name('survey.lihatSurvey');
		Route::post('survey/grid', 'SurveyController@grid')->name('survey.grid');
		Route::resource('survey', 'SurveyController');

		Route::post('fokus-audit/grid', 'FokusAuditController@grid')->name('fokus-audit.grid');
		Route::resource('fokus-audit', 'FokusAuditController');

		Route::post('langkah-kerja/grid', 'LangkahKerjaController@grid')->name('langkah-kerja.grid');
		Route::resource('langkah-kerja', 'LangkahKerjaController');

		Route::post('kegiatan-konsultasi/grid', 'KonsultasiController@grid')->name('kegiatan-konsultasi.grid');
		Route::resource('kegiatan-konsultasi', 'KonsultasiController');

		Route::post('kegiatan-lain/grid', 'LainController@grid')->name('kegiatan-lain.grid');
		Route::resource('kegiatan-lain', 'LainController');

		Route::post('cae/grid', 'CaeController@grid')->name('cae.grid');
		Route::resource('cae', 'CaeController');

		Route::post('standarisasi-temuan/grid', 'StandarisasiTemuanController@grid')->name('standarisasi-temuan.grid');
		Route::resource('standarisasi-temuan', 'StandarisasiTemuanController');

		Route::post('kategori-temuan/grid', 'KategoriTemuanController@grid')->name('kategori-temuan.grid');
		Route::resource('kategori-temuan', 'KategoriTemuanController');

		Route::post('temuan/grid', 'TemuanController@grid')->name('temuan.grid');
		Route::resource('temuan', 'TemuanController');

		Route::post('spin-pof/grid', 'SpinPofController@grid')->name('spin-pof.grid');
		Route::resource('spin-pof', 'SpinPofController');

		Route::post('spin-pemenuhan/grid', 'SpinPemenuhanController@grid')->name('spin-pemenuhan.grid');
		Route::resource('spin-pemenuhan', 'SpinPemenuhanController');

		Route::post('spin-kerja/grid', 'SpinKerjaController@grid')->name('spin-kerja.grid');
		Route::get('spin-kerja/{id}/buat','SpinKerjaController@buat')->name('spin-kerja.buat');
		Route::patch('spin-kerja/{id}/simpanData','SpinKerjaController@simpanData')->name('spin-kerja.simpanData');
		Route::resource('spin-kerja', 'SpinKerjaController');

		Route::post('spin-pembobotan/simpanData/{id}','SpinPembobotanController@simpanData')->name('spin-pembobotan.simpanData');
		Route::get('spin-pembobotan/{id}/buat-pembobotan','SpinPembobotanController@buatPembobotan')->name('spin-pembobotan.buatPembobotan');
		Route::get('spin-pembobotan/{id}/lihat-pembobotan','SpinPembobotanController@lihatPembobotan')->name('spin-pembobotan.lihatPembobotan');
		Route::post('spin-pembobotan/grid', 'SpinPembobotanController@grid')->name('spin-pembobotan.grid');
		Route::resource('spin-pembobotan', 'SpinPembobotanController');

		Route::name('iacm.')->prefix('iacm')->namespace('Iacm')->group( function() {
			Route::post('elemen/grid', 'ElemenController@grid')->name('elemen.grid');
			Route::resource('elemen', 'ElemenController');

			Route::post('kpa/grid', 'KPAController@grid')->name('kpa.grid');
			Route::resource('kpa', 'KPAController');

			Route::post('uraian/grid', 'UraianController@grid')->name('uraian.grid');
			Route::resource('uraian', 'UraianController');
		});

		Route::post('edisi/grid', 'EdisiController@grid')->name('edisi.grid');
		Route::resource('edisi', 'EdisiController');
	});

	Route::name('rapat.')->prefix('rapat')->namespace('Rapat')->group( function() {
		Route::post('internal/grid', 'RapatInternalController@grid')->name('internal.grid');
		Route::get('internal/{id}/cetak','RapatInternalController@cetak')->name('internal.cetak');
    	Route::get('internal/email/hadir/{id}', 'RapatInternalController@hadir')->name('internal.hadir');
    	Route::get('internal/email/tidakhadir/{id}', 'RapatInternalController@tidakHadir')->name('internal.tidakHadir');
		Route::patch('internal/re-upload/{id}', 'RapatInternalController@postUpload')->name('internal.postUpload');
		Route::get('internal/re-upload/{id}', 'RapatInternalController@reUpload')->name('internal.re-upload');
		Route::get('internal/download/{id}', 'RapatInternalController@download')->name('internal.download');
		Route::get('internal/code/{id}', 'RapatInternalController@showCode')->name('internal.code');
		Route::resource('internal', 'RapatInternalController');

		Route::post('eksternal-advisor/grid', 'RapatAdvisorController@grid')->name('eksternal-advisor.grid');
		Route::get('eksternal-advisor/{id}/cetak','RapatAdvisorController@cetak')->name('eksternal-advisor.cetak');
    	Route::get('eksternal-advisor/email/{id}', 'RapatAdvisorController@email')->name('eksternal-advisor.email');
    	Route::get('eksternal-advisor/email/hadir/{id}', 'RapatAdvisorController@hadir')->name('eksternal-advisor.hadir');
    	Route::get('eksternal-advisor/email/tidakhadir/{id}', 'RapatAdvisorController@tidakHadir')->name('eksternal-advisor.tidakHadir');
		Route::patch('eksternal-advisor/re-upload/{id}', 'RapatAdvisorController@postUpload')->name('eksternal-advisor.re-upload');
		Route::get('eksternal-advisor/re-upload/{id}', 'RapatAdvisorController@reUpload')->name('eksternal-advisor.re-upload');
		Route::get('eksternal-advisor/download/{id}', 'RapatAdvisorController@download')->name('eksternal-advisor.download');
		Route::get('eksternal-advisor/code/{id}', 'RapatAdvisorController@showCode')->name('eksternal-advisor.code');
		Route::resource('eksternal-advisor', 'RapatAdvisorController');

		Route::post('eksternal-konsultasi/grid', 'RapatKonsultasiController@grid')->name('eksternal-konsultasi.grid');
		Route::get('eksternal-konsultasi/{id}/cetak','RapatKonsultasiController@cetak')->name('eksternal-konsultasi.cetak');
    	Route::get('eksternal-konsultasi/email/hadir/{id}', 'RapatKonsultasiController@hadir')->name('eksternal-konsultasi.hadir');
    	Route::get('eksternal-konsultasi/email/tidakhadir/{id}', 'RapatKonsultasiController@tidakHadir')->name('eksternal-konsultasi.tidakHadir');
		Route::patch('eksternal-konsultasi/re-upload/{id}', 'RapatKonsultasiController@postUpload')->name('eksternal-konsultasi.re-upload');
		Route::get('eksternal-konsultasi/re-upload/{id}', 'RapatKonsultasiController@reUpload')->name('eksternal-konsultasi.re-upload');
		Route::get('eksternal-konsultasi/download/{id}', 'RapatKonsultasiController@download')->name('eksternal-konsultasi.download');
		Route::get('eksternal-konsultasi/code/{id}', 'RapatKonsultasiController@showCode')->name('eksternal-konsultasi.code');
		Route::resource('eksternal-konsultasi', 'RapatKonsultasiController');

		Route::post('eksternal-real/grid', 'RapatEksternalController@grid')->name('eksternal-real.grid');
		Route::get('eksternal-real/{id}/cetak','RapatEksternalController@cetak')->name('eksternal-real.cetak');
    	Route::get('eksternal-real/email/hadir/{id}', 'RapatEksternalController@hadir')->name('eksternal-real.hadir');
    	Route::get('eksternal-real/email/tidakhadir/{id}', 'RapatEksternalController@tidakHadir')->name('eksternal-real.tidakHadir');
		Route::patch('eksternal-real/re-upload/{id}', 'RapatEksternalController@postUpload')->name('eksternal-real.re-upload');
		Route::get('eksternal-real/re-upload/{id}', 'RapatEksternalController@reUpload')->name('eksternal-real.re-upload');
		Route::get('eksternal-real/download/{id}', 'RapatEksternalController@download')->name('eksternal-real.download');
		Route::get('eksternal-real/code/{id}', 'RapatEksternalController@showCode')->name('eksternal-real.code');
		Route::resource('eksternal-real', 'RapatEksternalController');
	});

	//Survey Kepuasan
	Route::name('survey-kepuasan-audit.')->prefix('survey-kepuasan-audit')->namespace('SurveyKepuasan')->group( function() {
		Route::name('survey.')->prefix('survey')->group( function() {
			Route::post('grid', 'SurveyKepuasanAuditController@grid')->name('grid');
			Route::post('onProgress', 'SurveyKepuasanAuditController@onProgress')->name('onProgress');
			Route::post('historis', 'SurveyKepuasanAuditController@historis')->name('historis');
			Route::get('{id}/buat','SurveyKepuasanAuditController@buat')->name('buat');
			Route::get('{id}/downloadLHA','SurveyKepuasanAuditController@downloadLHA')->name('downloadLHA');
			Route::get('{id}/cetak','SurveyKepuasanAuditController@cetak')->name('cetak');
			Route::get('{id}/downloadExcel','SurveyKepuasanAuditController@downloadExcel')->name('downloadExcel');
			Route::resource('/', 'SurveyKepuasanAuditController');
		});

		Route::name('hasil-survey.')->prefix('hasil-survey')->group( function() {
			Route::post('grid', 'HasilSurveyKepuasanAuditController@grid')->name('grid');
			Route::post('item', 'HasilSurveyKepuasanAuditController@item')->name('item');
			Route::get('{id}/detil-survey','HasilSurveyKepuasanAuditController@detilSurvey')->name('detil-survey');
			Route::get('{survey}/{tahun}/detil-item','HasilSurveyKepuasanAuditController@detilItem')->name('detil-item');
			Route::get('{survey}/{tahun}/downloadExcel','HasilSurveyKepuasanAuditController@downloadExcel')->name('downloadExcel');
			Route::get('{id}/cetak','SurveyKepuasanAuditController@cetak')->name('cetak');
			Route::resource('/', 'HasilSurveyKepuasanAuditController');
		});

	});

});

Auth::routes();
