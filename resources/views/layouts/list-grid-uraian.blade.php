@extends('layouts.base')

@include('libs.datatable')
@include('libs.actions')
@section('side-header')

@endsection

@section('body')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-8" style="left: 17px;">
                    <form id="dataFilters" class="form-inline" role="form">
                        @yield('filters')
                    </form>
                </div>
                <div class="col-sm-4 text-right">
                    @section('buttons')
                    {{-- <button class="btn m-b-xs btn-add btn-addon add button" style="margin-right: 15px;"><i class="fa fa-plus" style="margin-right: -2px;"></i>Tambah Data</button> --}}
                    @show
                </div>
            </div>
        </div>
        <div class="panel-body" style="padding-top: 0px;padding-bottom: 0px;">
	        <div class="table-responsive">
	            @if(isset($tableStruct))
	            <table id="dataTable" class="table table-bordered m-t-none" style="width: 100%">
	                <thead>
	                    <tr>
	                        @foreach ($tableStruct as $struct)
	                            <th class="text-center v-middle">{{ $struct['label'] }}</th>
	                        @endforeach
	                    </tr>
	                </thead>
	                <tbody>
	                    @yield('tableBody')
	                </tbody>
	            </table>
	            @endif
	        </div>
	    </div>
    </div>
@endsection

@push('scripts')
    <script>
        var modal = '#mediumModal';
        var onShow = function(){};

        $(document).on('click', '.add.button', function(e){
        	var url = "{!! route($routes.'.create') !!}";
            window.location = url;
        });

        $(document).on('click', '.edit.button', function(e){
            var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/edit';
        	window.location = url;
            
        });
        
        $(document).on('click', '.detil.button', function(e){
            var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx;
        	window.location = url;
            
        });

        $(document).on('click', '.save.button', function(e){
            saveData('formData', function(resp){
                $(modal).modal('hide');
                dt.draw(false);
            });
        });

        $(document).on('click', '.delete.button', function(e){
            var idx = $(this).data('id');

            deleteData('{!! route($routes.'.index') !!}/' + idx, function(resp){
                dt.draw(false);
            });
        });

        $(document).on('submit', '#formData', function(e){
            e.preventDefault();

            $('.save.button').trigger('click');
        });
    </script>

    @yield('js-extra')
@endpush