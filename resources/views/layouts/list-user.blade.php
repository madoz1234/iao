@extends('layouts.base')

@include('libs.datatable')
@include('libs.actions')
@section('side-header')
	
@endsection
@push('styles')
    <style>
        .spin{
            animation: spin-animation 0.6s infinite;
  			display: inline-block;
        }
	    @keyframes spin-animation {
		  0% {
		    transform: rotate(0deg);
		  }
		  100% {
		    transform: rotate(359deg);
		  }
		}
    </style>
@endpush

@section('body')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-8" style="left: 17px;">
                    <form id="dataFilters" class="form-inline" role="form">
                        @yield('filters')
                    </form>
                </div>
                <div class="col-sm-4 text-right">
                    @section('buttons')
                    @php 
                		$user = auth()->user();
                	@endphp
                    @if($user->hasRole(['admin']))
                     	<button class="btn m-b-xs btn-info btn-clear button" style="margin-right: 10px;"><i id="clear-session" class="fa fa-refresh"></i>&nbsp;&nbsp;&nbsp;Clear Session</button>
	                    {{-- <button class="btn m-b-xs btn-add btn-addon add button btn-user" style="margin-right: 15px;"><i class="fa fa-plus" style="margin-right: -2px;"></i>Tambah Data</button> --}}
                     @endif
                    @show
                </div>
            </div>
        </div>
        <div class="panel-body" style="padding-top: 0px;padding-bottom: 0px;">
        	<div class="table-responsive">
	            @if(isset($tableStruct))
		            <table id="dataTable" class="table table-bordered m-t-none">
		                <thead>
		                    <tr>
		                        @foreach ($tableStruct as $struct)
		                            <th class="text-center v-middle">{{ $struct['label'] }}</th>
		                        @endforeach
		                    </tr>
		                </thead>
		                <tbody>
		                    @yield('tableBody')
		                </tbody>
		            </table>
	            @endif
	        </div>
	    </div>
    </div>
@endsection

@push('scripts')
    <script>
        var modal = '#mediumModal';
        var onShow = function(){
           var tipe = $('select[name=tipe]').val();
           var kategori = $('select[name=kategori]').val();

           if(tipe == 1){
	           var a = $('select[name=roles]').val();
	           $('#non-pegawai').hide();
               $('#pegawai').show();
           }else{
           	   var a = $('select[name=role]').val();
           	   $('#non-pegawai').show();
               $('#pegawai').hide();
               $('.isi').hide();
           }

           if(kategori == 1){
           	  $('.isi-bu').show();
    		  $('.isi-co').hide();
           }else{
           	  $('.isi-bu').hide();
    		  $('.isi-co').show();
           }

           if(a == 10){
           	 $('.isi').show();
           }

            $('.cari').on('change', function(){
                if($(this).find('option:selected').text() === 'auditor'){
                    $('.isi').show();
                }else{
                    $('.isi').hide();
                }
            });

            $('.tipe').on('change', function(){
                if($(this).find('option:selected').text() === 'Pegawai'){
                	var aa = $('select[name=roles]').find('option:selected').text();
                	if(aa === 'auditor'){
                		$('.isi').show();
                	}else{
                		$('.isi').hide();
                	}
                    $('#non-pegawai').hide();
                    $('#pegawai').show();
                }else{
                    $('#non-pegawai').show();
                    $('#pegawai').hide();
                    $('.isi').hide();
                }
            });

    		$('.kategori').on('change', function(){
    			if($(this).val() == 1){
    				$('.isi-bu').show();
    				$('.isi-co').hide();
    			}else{
					$('.isi-bu').hide();
    				$('.isi-co').show();
    			}
            });

            $('.tipe').each(function(){
                $.ajax({
                    url: '{{ url('ajax/option/get-roles') }}',
                    type: 'POST',
                    data: {
                        _token: "{{ csrf_token() }}",
                        tipe: this.value,
                    },
                }).done(function(response) {
                    console.log(response);
                }).fail(function() {
                    console.log("error");
                });
            });
        };
        
        $(document).on('click', '.add.button', function(e){
            loadModal({
                url: "{!! route($routes.'.create') !!}",
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                $('.selectpicker').selectpicker();
                onShow();
            });
        });

        $(document).on('click', '.edit.button', function(e){
            var idx = $(this).data('id');

            loadModal({
                url: "{!! route($routes.'.index') !!}/" + idx + '/edit',
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                $('.selectpicker').selectpicker();
                onShow();
            });
        });

        $(document).on('click', '.save.button', function(e){
            saveData('formData', function(resp){
                $(modal).modal('hide');
                dt.draw(false);
            });
        });

        $(document).on('click', '.delete.button', function(e){
            var idx = $(this).data('id');

            deleteData('{!! route($routes.'.index') !!}/' + idx, function(resp){
                dt.draw(false);
            });
        });

        $(document).on('submit', '#formData', function(e){
            e.preventDefault();

            $('.save.button').trigger('click');
        });

        $(document).on('click', '.btn-clear.button', function(e){
        	$("#clear-session").addClass('spin');
			$.ajax({
				url: "{!! route($routes.'.index') !!}/clear",
				type: 'GET',
				success: function(resp){
					swal(
						'Berhasil!',
						'Session berhasil di hapus.',
						'success'
						).then(function(e){
							$("#clear-session").removeClass('spin');
							$('#dataFilters').trigger("reset");
							dt.draw();
						});
				},
				error : function(resp){
					swal(
						'Gagal!',
						'Session gagal di hapus',
						'error'
						).then(function(e){
							$("#clear-session").removeClass('spin');
							$('#dataFilters').trigger("reset");
						});
				}
			});
        });
    </script>

    @yield('js-extra')
@endpush