@extends('layouts.base')

@include('libs.datatable-multigrids')
@include('libs.actions')
@section('side-header')
	
@endsection

@section('body')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-8" style="left: 17px;">
                    <form id="dataFilters" class="form-inline" role="form">
                        @yield('filters')
                    </form>
                </div>
                <div class="col-sm-4 text-right" style="top: 31px;">
                    @section('buttons')
                    @php 
                		$user = auth()->user();
                	@endphp
                	@if($user->hasRole(['auditor']))
                    	<button class="btn m-b-xs btn-add btn-addon buat-tinjauan button" style="margin-right: 15px;"><i class="fa fa-plus" style="margin-right: -2px;"></i>Buat Surat Permintaan Dokumen</button>
                    	<button class="btn m-b-xs btn-add btn-info buat-approval button" style="margin-right: 15px;display: none;"><i class="fa fa-plus" style="margin-right: -2px;"></i> &nbsp;&nbsp;Approval</button>
                    @endif
                    @show
                </div>
            </div>
        </div>
        <div class="panel-body" style="padding-top: 0px;padding-bottom: 0px;">
	        	@if($mockup == true)
                    @yield('tables')
                @else
		        	<div class="table-responsive">
			            @if(isset($tableStruct))
				            <table id="dataTable" class="table table-bordered m-t-none">
				                <thead>
				                    <tr>
				                        @foreach ($tableStruct as $struct)
				                            <th class="text-center v-middle">{{ $struct['label'] }}</th>
				                        @endforeach
				                    </tr>
				                </thead>
				                <tbody>
				                    @yield('tableBody')
				                </tbody>
				            </table>
			            @endif
			        </div>
		        @endif
	    </div>
    </div>
@endsection

@push('scripts')
    <script>
        var modal = '#mediumModal';
        var onShow = function(){
          $('.cari').on('change', function(){
			  var aa = $('select[name="approval"]').val();
			  if(aa == 2){
			  	$('.alasan').show();
			  }else{
			  	$('.alasan').hide();
			  }
		  });
    	  $('.tahun').datepicker({
                format: "yyyy",
			    viewMode: "years", 
			    minViewMode: "years",
                orientation: "auto",
                autoclose:true
           });

    	   $('.bulan-tahun').datepicker({
                format: "mm-yyyy",
			    viewMode: "months", 
			    minViewMode: "months",
                orientation: "auto",
                autoclose:true
           });

           $(document).on('click', '.save2.button', function(e){
	            saveData('formData2', function(resp){
	                var url = "{!! route($routes.'.index') !!}";
		            window.location = url;
					return true;
	            });
	        });
        };

        $(document).on('click', '.approve-svp.button', function(e){
        	var idx = $(this).data('id');
        	swal({
					title: "Apakah Anda yakin?",
					text: "Data yang sudah disetujui, tidak dapat diubah!",
					icon: "warning",
					buttons: ['Reject', 'Approve'],
					reverseButtons: true
			}).then((result) => {
				if (result) {
		        	var url = "{!! route('kegiatan-audit.persiapan.tinjauan-dokumen.index') !!}/" + idx + '/approve-svp';
					$.ajax({
						url: url,
						type: 'POST',
						data: {
							id : idx,
							tipe : 1,
							'_token' : '{{ csrf_token() }}'
						}
					})
					.done(function(response) {
						swal({
							icon: 'success',
							title:'Berhasil!',
							text:'Data berhasil disetujui!',
							button: false,
							timer: 1000,
					    }).then((res) => {
					    	dt2 = $('#dataTable2').DataTable();
				    		dt2.draw();
					    })
					})
					.fail(function(response) {
						console.log(response);
						swal('Data gagal disetujui!', {
							icon: 'error',
					    }).then((res) => {
					    	dt2 = $('#dataTable2').DataTable();
				    		dt2.draw();
					    })
					})
				}else{
					var url = "{!! route($routes.'.index') !!}/" + idx + '/reject';
		            loadModal({
		                url: url,
		                modal: modal,
		            }, function(resp){
		            	$(modal).find('.loading.dimmer').hide();
		                onShow();
		            });
				}
			})
        });

        $(document).on('click', '.reject-svp.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route('kegiatan-audit.persiapan.tinjauan-dokumen.index') !!}/" + idx + '/approve-svp/'+2;
        	swal({
					title: "Apakah Anda yakin?",
					text: "Data yang sudah ditolak, tidak dapat diubah!",
					icon: "warning",
					buttons: true,
					reverseButtons: true
			}).then((result) => {
				if (result) {
					$.ajax({
						url: url,
						type: 'POST',
						data: {
							id : idx,
							tipe : 1,
							'_token' : '{{ csrf_token() }}'
						}
					})
					.done(function(response) {
						swal({
							icon: 'success',
							title:'Berhasil!',
							text:'Data berhasil ditolak!',
							button: false,
							timer: 1000,
					    }).then((res) => {
					    	dt2 = $('#dataTable2').DataTable();
				    		dt2.draw();
					    })
					})
					.fail(function(response) {
						console.log(response);
						swal('Data gagal ditolak!', {
							icon: 'error',
					    }).then((res) => {
					    	dt2 = $('#dataTable2').DataTable();
				    		dt2.draw();
					    })
					})

				}
			})
        });

        $(document).on('click', '.approve-ka.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route('kegiatan-audit.persiapan.tinjauan-dokumen.index') !!}/" + idx + '/approve-ka/'+1;
        	swal({
					title: "Apakah Anda yakin?",
					text: "Data yang sudah disetujui, tidak dapat diubah!",
					icon: "warning",
					buttons: true,
					reverseButtons: true
			}).then((result) => {
				if (result) {
					$.ajax({
						url: url,
						type: 'POST',
						data: {
							id : idx,
							tipe : 1,
							'_token' : '{{ csrf_token() }}'
						}
					})
					.done(function(response) {
						swal({
							icon: 'success',
							title:'Berhasil!',
							text:'Data berhasil disetujui!',
							button: false,
							timer: 1000,
					    }).then((res) => {
					    	dt2 = $('#dataTable2').DataTable();
				    		dt2.draw();
					    })
					})
					.fail(function(response) {
						console.log(response);
						swal('Data gagal disetujui!', {
							icon: 'error',
					    }).then((res) => {
					    	dt2 = $('#dataTable2').DataTable();
				    		dt2.draw();
					    })
					})

				}
			})
        });

        $(document).on('click', '.reject-ka.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route('kegiatan-audit.persiapan.tinjauan-dokumen.index') !!}/" + idx + '/approve-ka/'+2;
        	swal({
					title: "Apakah Anda yakin?",
					text: "Data yang sudah ditolak, tidak dapat diubah!",
					icon: "warning",
					buttons: true,
					reverseButtons: true
			}).then((result) => {
				if (result) {
					$.ajax({
						url: url,
						type: 'POST',
						data: {
							id : idx,
							tipe : 1,
							'_token' : '{{ csrf_token() }}'
						}
					})
					.done(function(response) {
						swal({
							icon: 'success',
							title:'Berhasil!',
							text:'Data berhasil ditolak!',
							button: false,
							timer: 1000,
					    }).then((res) => {
					    	dt2 = $('#dataTable2').DataTable();
				    		dt2.draw();
					    })
					})
					.fail(function(response) {
						console.log(response);
						swal('Data gagal ditolak!', {
							icon: 'error',
					    }).then((res) => {
					    	dt2 = $('#dataTable2').DataTable();
				    		dt2.draw();
					    })
					})

				}
			})
        });

        $(document).on('click', '.kirim.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route('kegiatan-audit.persiapan.tinjauan-dokumen.index') !!}/" + idx + '/kirim/';
        	swal({
					title: "Apakah Anda yakin?",
					text: "Data yang sudah dikirim, tidak dapat diubah!",
					icon: "warning",
					buttons: true,
					reverseButtons: true
			}).then((result) => {
				if (result) {
					$.ajax({
						url: url,
						type: 'GET',
						data: {
							id : idx,
							tipe : 1,
							'_token' : '{{ csrf_token() }}'
						}
					})
					.done(function(response) {
						swal({
							icon: 'success',
							title:'Berhasil!',
							text:'Data berhasil dikirim!',
							button: false,
							timer: 1000,
					    }).then((res) => {
					    	dt2 = $('#dataTable2').DataTable();
				    		dt2.draw();
					    })
					})
					.fail(function(response) {
						console.log(response);
						swal('Data gagal dikirim!', {
							icon: 'error',
					    }).then((res) => {
					    	dt2 = $('#dataTable2').DataTable();
				    		dt2.draw();
					    })
					})

				}
			})
        });

        $(document).on('click', '.upload-dokumen.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route('kegiatan-audit.persiapan.tinjauan-dokumen.index') !!}/" + idx + '/upload-dokumen';
            window.location = url;
        });

        $(document).on('click', '.detil.tinjauan.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route('kegiatan-audit.persiapan.tinjauan-dokumen.index') !!}/" + idx + '/detil';
            window.location = url;
        });

        // $(document).on('click', '.buat.button', function(e){
        // 	var idx = $(this).data('id');
        // 	var url = "{!! route('kegiatan-audit.persiapan.tinjauan-dokumen.index') !!}/" + idx + '/buat';
        //     window.location = url;
        // });

        $(document).on('click', '.ubah.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/ubahData';
            window.location = url;
        });

        $(document).on('click', '.buat-tinjauan.button', function(e){
        	var chkArray = [];
			dt1 = $('#dataTable1').DataTable();
        	var checkedcollection = dt1.$(".check:checked", { "page": "all" });
        	checkedcollection.each(function() {
				chkArray.push($(this).val());
			});
            if(chkArray.length == 0){
            	swal({
            		type:'error',
					icon: "warning",
					title:'Oopss!',
					text:'Checklist data terlebih dahulua.',
					button: false,
					timer: 1000,
				}).then((result) => { 
				})
            }else{
            	$.ajax({
					url: '{{ url('ajax/option/cek-datas') }}',
					type: 'POST',
					data: {
						_token: "{{ csrf_token() }}",
						id: chkArray
					},
				})
				.done(function(response) {
					if(response == 1){
						swal({
		            		type:'error',
							icon: "warning",
							title:'Oopss!',
							text:'Mohon pilih data dengan tahun audit yang sama.',
							buttons: 'OK',
						}).then((result) => { 
						})
					}else{
			        	var url = "{!! route('kegiatan-audit.persiapan.tinjauan-dokumen.index') !!}/" + chkArray + '/buat';
			        	window.location = url;
					}
				})
				.fail(function() {
					console.log("error");
				});
            }
        });

        $(document).on('click', '.buat-approval.button', function(e){
        	var chkArray = [];
			$(".checks:checked").each(function() {
				chkArray.push($(this).val());
			});
            if(chkArray.length == 0){
            	swal({
            		type:'error',
					icon: "warning",
					title:'Oopss!',
					text:'Checklist data terlebih dahulu.',
					button: false,
					timer: 1000,
				}).then((result) => { 
				})
            }else{
	            loadModal({
	                url: "{!! route($routes.'.index') !!}/" + chkArray + '/approval',
	                modal: modal,
	            }, function(resp){
	                $(modal).find('.loading.dimmer').hide();
	                $('.selectpicker').selectpicker();
	                onShow();
	            });
            }
        });

        $(document).on('click', '.add.button', function(e){
            loadModal({
                url: "{!! route($routes.'.create') !!}",
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                $('.selectpicker').selectpicker();
                onShow();
            });
        });

        $(document).on('click', '.draft.final.penugasan.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route('kegiatan-audit.persiapan.penugasan.index') !!}/" + idx + '/preview';
            loadModalPreview({
                url: url,
                modal: modal,
            }, function(resp){
                $('#largeModal').find('.loading.dimmer').hide();
                onShow();
            });
        });

        // $(document).on('click', '.edit.button', function(e){
        //     var idx = $(this).data('id');
        //     loadModal({
        //         url: "{!! route($routes.'.index') !!}/" + idx + '/edit',
        //         modal: modal,
        //     }, function(resp){
        //         $(modal).find('.loading.dimmer').hide();
        //         $('.selectpicker').selectpicker();
        //         onShow();
        //     });
        // });

        $(document).on('click', '.save.button', function(e){
            saveData('formData', function(resp){
                $(modal).modal('hide');
                dt.draw(false);
                var cek = dt.settings()[0].json.recordsTotal;
                if(cek == 0){
                	$('.for-isi1').show();
	                $('.for-isi1').text(cek + 1);
                }else{
                	$('.for-isi1').text(cek + 1);
                }
            });
        });

        $(document).on('click', '.delete.button', function(e){
            var idx = $(this).data('id');

            deleteData('{!! route($routes.'.index') !!}/' + idx, function(resp){
                dt.draw(false);
                var cek = dt.settings()[0].json.recordsTotal - 1;
                if(cek == 0){
					$('.for-isi1').hide();
                }else{
					$('.for-isi1').text(cek);
                }
            });
        });

        $(document).on('submit', '#formData', function(e){
            e.preventDefault();

            $('.save.button').trigger('click');
        });
    </script>

    @yield('js-extra')
@endpush