@extends('layouts.base')
@include('libs.datatable-multigrid')
@include('libs.actions')
@section('side-header')

   <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color: transparent !important;">
        <?php $i=1; $last=count($breadcrumb);?>
         @foreach ($breadcrumb as $name => $link)
            @if($i++ != $last)
                <li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
         @endforeach
      </ol>
    </nav>
@endsection

@section('body')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-8" style="left: 17px;">
                    <form id="dataFilters" class="form-inline" role="form">
                        @yield('filters')
                    </form>
                </div>
                <?php $user = auth()->user() ?>
                @if ($user->hasRole(['auditor']))
                    <div class="col-sm-4 text-right">
                        @section('buttons')
                            <button class="btn m-b-xs btn-add btn-addon add button" style="margin-right: 15px;"><i class="fa fa-plus" style="margin-right: -2px;"></i>Tambah Data</button>
                        @show
                    </div>
                @endif
            </div>
        </div>
        <div class="panel-body" style="padding-top: 0px;padding-bottom: 0px;">
            <ul class="nav nav-tabs" id="myTab" role="tablist" style="margin-bottom: 7px;">
                <li class="nav-item tab-a">
                    <a class="nav-link active" id="a-tab" data-toggle="tab" href="#a" role="tab" aria-selected="true">Baru</a>
                </li>
                <li class="nav-item tab-b">
                    <a class="nav-link" id="b-tab" data-toggle="tab" href="#b" role="tab" aria-selected="false">On Progress</a>
                </li>
                <li class="nav-item tab-c">
                    <a class="nav-link" id="c-tab" data-toggle="tab" href="#c" role="tab" aria-selected="false">Historis</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="a" role="tabpanel" aria-labelledby="a-tab">
                    <div class="table-responsive">
                        @if(isset($structs['listStruct']))
                        <table id="dataTable1" class="table table-bordered m-t-none" style="width: 100%">
                            <thead>
                                <tr>
                                    @foreach ($structs['listStruct'] as $struct)
                                    <th class="text-center v-middle">{{ $struct['label'] }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @yield('tableBody')
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
                <div class="tab-pane" id="b" role="tabpanel" aria-labelledby="b-tab">
                    <div class="table-responsive">
                        @if(isset($structs['listStruct2']))
                        <table id="dataTable2" class="table table-bordered m-t-none" style="width: 100%">
                            <thead>
                                <tr>
                                    @foreach ($structs['listStruct2'] as $struct)
                                    <th class="text-center v-middle">{{ $struct['label'] }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @yield('tableBody')
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
                <div class="tab-pane" id="c" role="tabpanel" aria-labelledby="c-tab">
                    <div class="table-responsive">
                        @if(isset($structs['listStruct3']))
                        <table id="dataTable3" class="table table-bordered m-t-none" style="width: 100%">
                            <thead>
                                <tr>
                                    @foreach ($structs['listStruct3'] as $struct)
                                    <th class="text-center v-middle">{{ $struct['label'] }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @yield('tableBody')
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
    	var modal = '#mediumModal';
    	var onShow = function(){
        };
        $(document).ready(function(){
            $('.tanggal').datepicker({
                        format: 'yyyy-mm-dd',
                        startDate: '-0d',
                        orientation: "auto",
                        autoclose:true,
                    });
            var modal = '#mediumModal';
            var onShow = function(){};
        });

        $(document).on('click', '.add.button', function(e){
            var url = "{!! route($routes.'.create') !!}";
            window.location = url;
        });

        $(document).on('click', '.edit.button', function(e){
            var idx = $(this).data('id');
            var url = "{!! route($routes.'.index') !!}/" + idx + '/edit';
            window.location = url;
            
        });
        
        $(document).on('click', '.detil.button', function(e){
            var idx = $(this).data('id');
            var url = "{!! route($routes.'.index') !!}/" + idx;
            window.location = url;
            
        });

        $(document).on('click', '.save.button', function(e){
            saveData('formData', function(resp){
                $(modal).modal('hide');
                dt.draw(false);
            });
        });

        $(document).on('click', '.delete.button', function(e){
            var idx = $(this).data('id');

            deleteData('{!! route($routes.'.index') !!}/' + idx, function(resp){
                dt.draw(false);
            });
        });

        $(document).on('submit', '#formData', function(e){
            e.preventDefault();

            $('.save.button').trigger('click');
        });

        $(document).on('click', '.save.as.page', function(e){
            $('#formData').find('input[name="status"]').val("1");
            var formDom = "formData";
            if($(this).data("form") !== undefined){
                formDom = $(this).data('form');
            }
            swal({
                    title: "Apakah Anda yakin?",
                    text: "Data yang sudah dikirim, tidak dapat diubah!",
                    icon: "warning",
                    buttons: ['Batal', 'OK'],
                    reverseButtons: true
            }).then((result) => {
                if (result) {
                    saveForm(formDom);
                }
            })
        });

        // $(document).on('click', '.approve.button', function(e){
        //     var idx = $(this).data('id');
        //     var url = "{!! route($routes.'.index') !!}/" + idx + '/approve/'+1;
        //     swal({
        //             title: "Apakah anda yakin?",
        //             text: "Data yang sudah disetujui, tidak dapat diubah!",
        //             icon: "warning",
        //             buttons: true,
        //             reverseButtons: true
        //     }).then((result) => {
        //         if (result) {
        //             $.ajax({
        //                 url: url,
        //                 type: 'POST',
        //                 data: {
        //                     id : idx,
        //                     tipe : 1,
        //                     '_token' : '{{ csrf_token() }}'
        //                 }
        //             })
        //             .done(function(response) {
        //                 swal({
        //                     icon: 'success',
        //                     title:'Berhasil!',
        //                     text:'Data berhasil disetujui!',
        //                     button: false,
        //                     timer: 1000,
        //                 }).then((res) => {
        //                     dt2 = $('#dataTable2').DataTable();
        //                     dt2.draw();
        //                 })
        //             })
        //             .fail(function(response) {
        //                 console.log(response);
        //                 swal('Data gagal disetujui!', {
        //                     icon: 'error',
        //                 }).then((res) => {
        //                     dt2 = $('#dataTable2').DataTable();
        //                     dt2.draw();
        //                 })
        //             })

        //         }
        //     })
        // });

        // $(document).on('click', '.reject.button', function(e){
        //     var idx = $(this).data('id');
        //     var url = "{!! route($routes.'.index') !!}/" + idx + '/approve/'+2;
        //     swal({
        //             title: "Apakah anda yakin?",
        //             text: "Data yang sudah ditolak, tidak dapat diubah!",
        //             icon: "warning",
        //             buttons: true,
        //             reverseButtons: true
        //     }).then((result) => {
        //         if (result) {
        //             $.ajax({
        //                 url: url,
        //                 type: 'POST',
        //                 data: {
        //                     id : idx,
        //                     tipe : 1,
        //                     '_token' : '{{ csrf_token() }}'
        //                 }
        //             })
        //             .done(function(response) {
        //                 swal({
        //                     icon: 'success',
        //                     title:'Berhasil!',
        //                     text:'Data berhasil ditolak!',
        //                     button: false,
        //                     timer: 1000,
        //                 }).then((res) => {
        //                     dt2 = $('#dataTable2').DataTable();
        //                     dt2.draw();
        //                 })
        //             })
        //             .fail(function(response) {
        //                 console.log(response);
        //                 swal('Data gagal ditolak!', {
        //                     icon: 'error',
        //                 }).then((res) => {
        //                     dt2 = $('#dataTable2').DataTable();
        //                     dt2.draw();
        //                 })
        //             })

        //         }
        //     })
        // });

        // // rencana
        // $(document).on('click', '.approve.button', function(e){
        //     var idx = $(this).data('id');
        //     var url = "{!! route($routes.'.index') !!}/" + idx + '/approve/'+1;
        //     swal({
        //             title: "Apakah anda yakin?",
        //             text: "Data yang sudah disetujui, tidak dapat diubah!",
        //             icon: "warning",
        //             buttons: true,
        //             reverseButtons: true
        //     }).then((result) => {
        //         if (result) {
        //             $.ajax({
        //                 url: url,
        //                 type: 'POST',
        //                 data: {
        //                     id : idx,
        //                     tipe : 1,
        //                     '_token' : '{{ csrf_token() }}'
        //                 }
        //             })
        //             .done(function(response) {
        //                 swal({
        //                     icon: 'success',
        //                     title:'Berhasil!',
        //                     text:'Data berhasil disetujui!',
        //                     button: false,
        //                     timer: 1000,
        //                 }).then((res) => {
        //                     dt2 = $('#dataTable2').DataTable();
        //                     dt2.draw();
        //                 })
        //             })
        //             .fail(function(response) {
        //                 console.log(response);
        //                 swal('Data gagal disetujui!', {
        //                     icon: 'error',
        //                 }).then((res) => {
        //                     dt2 = $('#dataTable2').DataTable();
        //                     dt2.draw();
        //                 })
        //             })

        //         }
        //     })
        // });

        // $(document).on('click', '.reject.button', function(e){
        //     var idx = $(this).data('id');
        //     var url = "{!! route($routes.'.index') !!}/" + idx + '/approve/'+2;
        //     swal({
        //             title: "Apakah anda yakin?",
        //             text: "Data yang sudah ditolak, tidak dapat diubah!",
        //             icon: "warning",
        //             buttons: true,
        //             reverseButtons: true
        //     }).then((result) => {
        //         if (result) {
        //             $.ajax({
        //                 url: url,
        //                 type: 'POST',
        //                 data: {
        //                     id : idx,
        //                     tipe : 1,
        //                     '_token' : '{{ csrf_token() }}'
        //                 }
        //             })
        //             .done(function(response) {
        //                 swal({
        //                     icon: 'success',
        //                     title:'Berhasil!',
        //                     text:'Data berhasil ditolak!',
        //                     button: false,
        //                     timer: 1000,
        //                 }).then((res) => {
        //                     dt2 = $('#dataTable2').DataTable();
        //                     dt2.draw();
        //                 })
        //             })
        //             .fail(function(response) {
        //                 console.log(response);
        //                 swal('Data gagal ditolak!', {
        //                     icon: 'error',
        //                 }).then((res) => {
        //                     dt2 = $('#dataTable2').DataTable();
        //                     dt2.draw();
        //                 })
        //             })

        //         }
        //     })
        // });
    </script>

    @yield('js-extra')
@endpush