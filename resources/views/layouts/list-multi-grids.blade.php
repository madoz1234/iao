@extends('layouts.base')

@include('libs.datatable-multigrid')
@include('libs.actions')
@section('side-header')
	
@endsection

@section('body')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-8" style="left: 17px;">
                    <form id="dataFilters" class="form-inline" role="form">
                        @yield('filters')
                    </form>
                </div>
                <div class="col-sm-4 text-right" style="top: 40px;">
                    @section('buttons')
                    	<button class="btn m-b-xs btn-success button" style="margin-right: 3px;"><i class="fa fa-upload" style="margin-right: -2px;"></i>&nbsp;&nbsp;&nbsp;&nbsp; Upload Template</button>
                    	<button class="btn m-b-xs btn-info button" style="margin-right: 15px;"><i class="fa fa-download" style="margin-right: -2px;"></i>&nbsp;&nbsp;&nbsp;&nbsp; Download Template</button>
                    @show
                </div>
            </div>
        </div>
        <div class="panel-body" style="padding-top: 0px;padding-bottom: 0px;">
	        	@if($mockup == true)
                    @yield('tables')
                @else
		        	<div class="table-responsive">
			            @if(isset($tableStruct))
				            <table id="dataTable" class="table table-bordered m-t-none">
				                <thead>
				                    <tr>
				                        @foreach ($tableStruct as $struct)
				                            <th class="text-center v-middle">{{ $struct['label'] }}</th>
				                        @endforeach
				                    </tr>
				                </thead>
				                <tbody>
				                    @yield('tableBody')
				                </tbody>
				            </table>
			            @endif
			        </div>
		        @endif
	    </div>
    </div>
@endsection

@push('scripts')
    <script>
        var modal = '#mediumModal';
        var onShow = function(){
    	  $('.tahun').datepicker({
                format: "yyyy",
			    viewMode: "years", 
			    minViewMode: "years",
                orientation: "auto",
                autoclose:true
           });

    	   $('.bulan-tahun').datepicker({
                format: "mm-yyyy",
			    viewMode: "months", 
			    minViewMode: "months",
                orientation: "auto",
                autoclose:true
           });

           $('.tanggal').datepicker({
				format: 'dd-mm-yyyy',
				startDate: '-0d',
				orientation: "auto",
				autoclose:true,
			});

           	$('.waktu').clockpicker({
	        	autoclose:true,
	        	'default': 'now',
	        });
        };


        $(document).on('click', '.add.button', function(e){
            loadModal({
                url: "{!! route($routes.'.create') !!}",
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                $('.selectpicker').selectpicker();
                onShow();
            });
        });

        $(document).on('click', '.add.penugasan.button', function(e){
        	var idx = $(this).data('id');
            loadModal({
                url: "{!! route($routes.'.create') !!}",
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                $('.selectpicker').selectpicker();
                $('input[name="rencana_id"]').val(idx);
                $("#pdf").fileinput({
				    language: 'es',
				    uploadUrl: "/file-upload-batch/2",
				    previewFileType: "pdf",
				    showUpload: false,
				    previewFileIcon: '<i class="fas fa-file"></i>',
				    previewFileIconSettings: {
				        'docx': '<i class="fas fa-file-word text-primary"></i>',
				        'xlsx': '<i class="fas fa-file-excel text-success"></i>',
				        'pptx': '<i class="fas fa-file-powerpoint text-danger"></i>',
				        'pdf': '<i class="fas fa-file-pdf text-danger"></i>',
				        'zip': '<i class="fas fa-file-archive text-muted"></i>',
				    },
				    fileActionSettings: {
					    showRemove: true,
					    showUpload: false, //This remove the upload button
					    showZoom: true,
					    showDrag: true
					},
				    initialPreviewAsData: true,
				    purifyHtml: true,
				    allowedFileTypes: ['pdf'],
			        allowedFileExtensions: ['pdf'],
			        // allowed image size up to 5 MB
			        maxFileSize: 5000,
				    previewFileExtSettings: {
				    	'doc': function(ext) {
				    		return ext.match(/(doc|docx)$/i);
				    	},
				    	'xls': function(ext) {
				    		return ext.match(/(xls|xlsx)$/i);
				    	},
				    	'ppt': function(ext) {
				    		return ext.match(/(ppt|pptx)$/i);
				    	}
				    }
				});
                onShow();
            });
        });

        $(document).on('click', '.add.final.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route('kegiatan-audit.persiapan.penugasan.index') !!}/" + idx + '/final';
            loadModal({
                url: url,
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                $('input[name="penugasan_id"]').val(idx);
                $("#pdf").fileinput({
				    language: 'es',
				    uploadUrl: "/file-upload-batch/2",
				    previewFileType: "pdf",
				    showUpload: false,
				    previewFileIcon: '<i class="fas fa-file"></i>',
				    previewFileIconSettings: {
				        'docx': '<i class="fas fa-file-word text-primary"></i>',
				        'xlsx': '<i class="fas fa-file-excel text-success"></i>',
				        'pptx': '<i class="fas fa-file-powerpoint text-danger"></i>',
				        'pdf': '<i class="fas fa-file-pdf text-danger"></i>',
				        'zip': '<i class="fas fa-file-archive text-muted"></i>',
				    },
				    fileActionSettings: {
					    showRemove: true,
					    showUpload: false, //This remove the upload button
					    showZoom: true,
					    showDrag: true
					},
				    initialPreviewAsData: true,
				    purifyHtml: true,
				    allowedFileTypes: ['pdf'],
			        allowedFileExtensions: ['pdf'],
			        // allowed image size up to 5 MB
			        maxFileSize: 5000,
				    previewFileExtSettings: {
				    	'doc': function(ext) {
				    		return ext.match(/(doc|docx)$/i);
				    	},
				    	'xls': function(ext) {
				    		return ext.match(/(xls|xlsx)$/i);
				    	},
				    	'ppt': function(ext) {
				    		return ext.match(/(ppt|pptx)$/i);
				    	}
				    }
				});
                onShow();
            });
        });

        $(document).on('click', '.edit.button', function(e){
            var idx = $(this).data('id');

            loadModal({
                url: "{!! route($routes.'.index') !!}/" + idx + '/edit',
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                $('.selectpicker').selectpicker();
                onShow();
            });
        });

        $(document).on('click', '.save.button', function(e){
            saveData('formData', function(resp){
                $(modal).modal('hide');
                dt.draw(false);
                var cek = dt.settings()[0].json.recordsTotal;
                if(cek == 0){
                	$('.for-isi1').show();
	                $('.for-isi1').text(cek + 1);
                }else{
                	$('.for-isi1').text(cek + 1);
                }
            });
        });

        $(document).on('click', '.delete.button', function(e){
            var idx = $(this).data('id');
            deleteData('{!! route($routes.'.index') !!}/' + idx, function(resp){
                dt.draw(false);
                var cek = dt.settings()[0].json.recordsTotal - 1;
                if(cek == 0){
					$('.for-isi1').hide();
                }else{
					$('.for-isi1').text(cek);
                }
            });
        });

        $(document).on('submit', '#formData', function(e){
            e.preventDefault();
            $('.save.button').trigger('click');
        });
    </script>

    @yield('js-extra')
@endpush