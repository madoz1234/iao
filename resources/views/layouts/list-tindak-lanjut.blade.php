@extends('layouts.base')

@include('libs.datatable')
@include('libs.actions')
@section('side-header')
	
@endsection

@section('body')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-8" style="left: 17px;">
                    <form id="dataFilters" class="form-inline" role="form">
                        @yield('filters')
                    </form>
                </div>
                <div class="col-sm-4 text-right">
                    @section('buttons')
                    {{-- <button class="btn m-b-xs btn-add btn-addon add button" style="margin-right: 15px;"><i class="fa fa-plus" style="margin-right: -2px;"></i>Tambah Data</button> --}}
                    @show
                </div>
            </div>
        </div>
        <div class="panel-body" style="padding-top: 0px;padding-bottom: 0px;">
            <div class="table-responsive">
                @if(isset($tableStruct))
                    <table id="dataTable" class="table table-bordered m-t-none">
                        <thead>
                            <tr>
                                @foreach ($tableStruct as $struct)
                                    <th class="text-center v-middle">{{ $struct['label'] }}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @yield('tableBody')
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        var modal = '#mediumModal';
        var onShow = function(){
    	  $('.tahun').datepicker({
                format: "yyyy",
			    viewMode: "years", 
			    minViewMode: "years",
                orientation: "auto",
                autoclose:true
           });

    	   $('.bulan-tahun').datepicker({
                format: "mm-yyyy",
			    viewMode: "months", 
			    minViewMode: "months",
                orientation: "auto",
                autoclose:true
           });

            $(".tlp").on("keyup", function(e){
				setTimeout(jQuery.proxy(function() {
			        this.val(this.val().replace(/[^0-9]/g, ''));
			    }, $(this)), 0);
			});
			$("[data-toggle='toggle']").bootstrapToggle('destroy')                 
		    $("[data-toggle='toggle']").bootstrapToggle();

		    $('.kategori').on('change', function(){
				$.ajax({
					url: '{{ url('ajax/option/get-kategoriz') }}',
					type: 'POST',
					data: {
						_token: "{{ csrf_token() }}",
						kategori: this.value
					},
				})
				.done(function(response) {
					$('select[name="kategori_id"]').html(response);
					$('select[name="kategori_id"]').selectpicker("refresh");
				})
				.fail(function() {
					console.log("error");
				});
			});

		    var object = $('input[name=object]').val();
			$.ajax({
				url: '{{ url('ajax/option/get-kategoriz') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					kategori: $('select[name=kategori]').val()
				},
			})
			.done(function(response) {
				$('select[name="kategori_id"]').html(response);
				$('select[name="kategori_id"]').selectpicker("refresh");
				$('select[name="kategori_id"]').children('option[value="'+object+'"]').prop('selected',true);
				$('select[name="kategori_id"]').selectpicker("refresh");
			})
			.fail(function() {
				console.log("error");
			});

        };
        $(document).on('click', '.add.button', function(e){
            loadModal({
                url: "{!! route($routes.'.create') !!}",
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                $('.selectpicker').selectpicker();
                onShow();
            });
        });

        $(document).on('click', '.edit.button', function(e){
            var idx = $(this).data('id');

            loadModal({
                url: "{!! route($routes.'.index') !!}/" + idx + '/edit',
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                $('.selectpicker').selectpicker();
                onShow();
            });
        });

        $(document).on('click', '.save.button', function(e){
            saveData('formData', function(resp){
                $(modal).modal('hide');
                dt.draw(false);
            });
        });

        $(document).on('click', '.delete.button', function(e){
            var idx = $(this).data('id');

            deleteData('{!! route($routes.'.index') !!}/' + idx, function(resp){
                dt.draw(false);
            });
        });

        $(document).on('submit', '#formData', function(e){
            e.preventDefault();

            $('.save.button').trigger('click');
        });

        $(document).on('click', '.detil.button', function(e){
            var idx = $(this).data('id');
            var url = "{!! route($routes.'.index') !!}/" + idx;
            window.location = url;
            
        });
    </script>

    @yield('js-extra')
@endpush