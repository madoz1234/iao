@extends('layouts.base')
@include('libs.datatable-multigrid')
@include('libs.actions')
@section('side-header')

@endsection

@section('body')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-8" style="left: 17px;">
                    <form id="dataFilters" class="form-inline" role="form">
                        @yield('filters')
                    </form>
                </div>
                <div class="col-sm-4 text-right" style="top: 35px;">
                    @section('buttons')
                    	@php 
                    		$user = auth()->user();
                    	@endphp
                    	@if($user->hasJabatan(['auditor']))
                    		<button class="btn m-b-xs btn-add btn-addon add pkat button" style="margin-right: 15px;"><i class="fa fa-plus" style="margin-right: -2px;"></i>Tambah Data</button>
                    	@endif
                    @show
                </div>
            </div>
        </div>
        <div class="panel-body" style="padding-top: 0px;padding-bottom: 0px;">
        	@if($mockup == true)
                @yield('tables')
            @else
	        	<div class="table-responsive">
		            @if(isset($tableStruct))
			            <table id="dataTable" class="table table-bordered">
			                <thead>
			                    <tr>
			                        @foreach ($tableStruct as $struct)
			                            <th class="text-center v-middle">{{ $struct['label'] }}</th>
			                        @endforeach
			                    </tr>
			                </thead>
			                <tbody>
			                    @yield('tableBody')
			                </tbody>
			            </table>
		            @endif
		        </div>
		    @endif
	    </div>
    </div>
@endsection
@push('styles')
    <style>
        .swal-text {
		  padding: 17px;
		  display: block;
		  margin: 22px;
		  text-align: center;
		  color: #61534e;
		}
    </style>
@endpush

@push('scripts')
    <script>
        var modal = '#mediumModal';
        var onShow = function(){
    	  $('.tahun').datepicker({
                format: "yyyy",
			    viewMode: "years", 
			    minViewMode: "years",
                orientation: "auto",
                autoclose:true
           });

    	   $('.bulan-tahun').datepicker({
                format: "mm-yyyy",
			    viewMode: "months", 
			    minViewMode: "months",
                orientation: "auto",
                autoclose:true
           });
        };

        $(document).on('click', '.add.pkat.button', function(e){
            loadModal({
                url: "{!! route($routes.'.create') !!}",
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                $('.selectpicker').selectpicker();
                onShow();
            });
        });

        $(document).on('click', '.add.khusus.button', function(e){
            loadModal({
                url: "{!! route($routes.'.create') !!}",
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                $('.selectpicker').selectpicker();
                onShow();
            });
        });


        $(document).on('click', '.objectaudit.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/updateObject';
            window.location = url;
        });

        $(document).on('click', '.objectaudit-khusus.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/updateObject';
            window.location = url;
        });

        $(document).on('click', '.ubah.khusus.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/edit';
            window.location = url;
        });

        $(document).on('click', '.edit.button', function(e){
            var idx = $(this).data('id');
            loadModal({
                url: "{!! route($routes.'.index') !!}/" + idx + '/edit',
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                $('.selectpicker').selectpicker();
                onShow();
            });
        });

        $(document).on('click', '.save.button', function(e){
            saveData('formData', function(resp){
                $(modal).modal('hide');
                dt.draw(false);
                // var cek = dt.settings()[0].json.recordsTotal;
                // if(cek == 0){
                // 	$('.for-isi1').show();
	               //  $('.for-isi1').text(cek + 1);
                // }else{
                // 	$('.for-isi1').text(cek + 1);
                // }
            });
        });

        $(document).on('click', '.delete.button', function(e){
            var idx = $(this).data('id');

            deleteData('{!! route($routes.'.index') !!}/' + idx, function(resp){
                dt.draw(false);
                var cek = dt.settings()[0].json.recordsTotal - 1;
                if(cek == 0){
					$('.for-isi1').hide();
                }else{
					$('.for-isi1').text(cek);
                }
            });
        });

        $(document).on('submit', '#formData', function(e){
            e.preventDefault();
            $('.save.button').trigger('click');
        });
    </script>

    @yield('js-extra')
@endpush