@extends('layouts.list-log')
@section('title', 'Session Log')
@section('side-header')
    <div class="btn-group" style="left: -15px;">
        <a href="{{ route('setting.users.index') }}" type="button" class="btn btn-grey ada" style="border-radius: 20px;">User</a>
        <a href="{{ route('setting.roles.index') }}" type="button" class="btn btn-grey ada" style="border-radius: 20px;">Role</a>
        <a href="{{ route('setting.log.index') }}" type="button" class="btn btn-grey hilang active" style="border-radius: 20px;">Log</a>
    </div>
@endsection
@section('filters')
    <div class="form-group">
        <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
        <label class="control-label sr-only" for="filter-modul">Modul</label>
	    <select class="select selectpicker filter-control" name="filter[modul]" data-post="modul" data-style="btn-default" data-size="5" data-live-search="true"> 
	           <option style="font-size: 12px;" value="0">Modul</option>
	           <option style="font-size: 12px;" value="Login">Login</option>
	           <option style="font-size: 12px;" value="Logout">Logout</option>
	           <option style="font-size: 12px;" value="Kegiatan Audit Rencana">Kegiatan Audit Rencana</option>
	           <option style="font-size: 12px;" value="Kegiatan Audit Persiapan">Kegiatan Audit Persiapan</option>
	           <option style="font-size: 12px;" value="Kegiatan Audit Pelaksanaan">Kegiatan Audit Pelaksanaan</option>
	           <option style="font-size: 12px;" value="Kegiatan Audit Pelaporan">Kegiatan Audit Pelaporan</option>
	           <option style="font-size: 12px;" value="Kegiatan Audit Tindak Lanjut">Kegiatan Audit Tindak Lanjut</option>
	           <option style="font-size: 12px;" value="Kegiatan Konsultasi">Kegiatan Konsultasi</option>
	           <option style="font-size: 12px;" value="Kegiatan Lainnya">Kegiatan Lainnya</option>
	           <option style="font-size: 12px;" value="Evaluasi Mutu Audit">Evaluasi Mutu Audit</option>
	           <option style="font-size: 12px;" value="Master">Master</option>
	           <option style="font-size: 12px;" value="Manajemen Pengguna">Manajemen Pengguna</option>
	    </select>
        <label class="control-label sr-only" for="filter-sub">Sub Modul</label>
	    <select class="select selectpicker filter-control" name="filter[sub]" data-post="sub" data-style="btn-default" data-size="5" data-live-search="true"> 
	           <option style="font-size: 12px;" value="0">Sub Modul</option>
	           <option style="font-size: 12px;" value="Audit Reguler">Audit Reguler</option>
	           <option style="font-size: 12px;" value="Audit Khusus">Audit Khusus</option>
	           <option style="font-size: 12px;" value="Surat Penugasan">Surat Penugasan</option>
	           <option style="font-size: 12px;" value="Permintaan Dokumen">Permintaan Dokumen</option>
	           <option style="font-size: 12px;" value="Program Audit">Program Audit</option>
	           <option style="font-size: 12px;" value="Opening Meeting">Opening Meeting</option>
	           <option style="font-size: 12px;" value="Closing Meeting">Closing Meeting</option>
	           <option style="font-size: 12px;" value="KKA">KKA</option>
	           <option style="font-size: 12px;" value="LHA">LHA</option>
	           <option style="font-size: 12px;" value="Konsultasi">Konsultasi</option>
	           <option style="font-size: 12px;" value="Surat Perintah TL">Surat Perintah TL</option>
	           <option style="font-size: 12px;" value="Laporan TL">Laporan TL</option>
	           <option style="font-size: 12px;" value="Register TL">Register TL</option>
	           <option style="font-size: 12px;" value="Perencanaan">Perencanaan</option>
	           <option style="font-size: 12px;" value="Pelaksanaan">Pelaksanaan</option>
	           <option style="font-size: 12px;" value="Pelaporan">Pelaporan</option>
	           <option style="font-size: 12px;" value="Counterpart Auditor Eksternal">Counterpart Auditor Eksternal</option>
	           <option style="font-size: 12px;" value="Evaluasi SPIN">Evaluasi SPIN</option>
	           <option style="font-size: 12px;" value="IACM">IACM</option>
	           <option style="font-size: 12px;" value="Realisasi Vs Rencana">Realisasi Vs Rencana</option>
	           <option style="font-size: 12px;" value="Rekomendasi">Rekomendasi</option>
	           <option style="font-size: 12px;" value="Survey Kepuasan Auditi">Survey Kepuasan Auditi</option>
	           <option style="font-size: 12px;" value="Internal Audit Capability Model">Internal Audit Capability Model</option>
	           <option style="font-size: 12px;" value="Cost Saving & Potential Cost Saving">Cost Saving & Potential Cost Saving</option>
	           <option style="font-size: 12px;" value="Temuan">Temuan</option>
	           <option style="font-size: 12px;" value="Beban Umum & Administrasi IA">Beban Umum & Administrasi IA</option>
	           <option style="font-size: 12px;" value="Pengembangan / Pelatihan">Pengembangan / Pelatihan</option>
	           <option style="font-size: 12px;" value="Rapat Internal">Rapat Internal</option>
	           <option style="font-size: 12px;" value="Rapat Eksternal">Rapat Eksternal</option>
	           <option style="font-size: 12px;" value="Project">Project</option>
	           <option style="font-size: 12px;" value="Business Unit (BU)">Business Unit (BU)</option>
	           <option style="font-size: 12px;" value="Corporate Office (CO)">Corporate Office (CO)</option>
	           <option style="font-size: 12px;" value="Anak Perusahaan">Anak Perusahaan</option>
	           <option style="font-size: 12px;" value="Vendor">Vendor</option>
	           <option style="font-size: 12px;" value="Dokumen Pendahuluan">Dokumen Pendahuluan</option>
	           <option style="font-size: 12px;" value="Fokus Audit">Fokus Audit</option>
	           <option style="font-size: 12px;" value="Langkah Kerja">Langkah Kerja</option>
	           <option style="font-size: 12px;" value="Kategori Temuan">Kategori Temuan</option>
	           <option style="font-size: 12px;" value="Kriteria Temuan">Kriteria Temuan</option>
	           <option style="font-size: 12px;" value="SPIN PoF">SPIN PoF</option>
	           <option style="font-size: 12px;" value="SPIN Unsur Pemenuhan">SPIN Unsur Pemenuhan</option>
	           <option style="font-size: 12px;" value="SPIN Pembobotan">SPIN Pembobotan</option>
	           <option style="font-size: 12px;" value="IACM Elemen">IACM Elemen</option>
	           <option style="font-size: 12px;" value="IACM Key Process Area">IACM Key Process Area</option>
	           <option style="font-size: 12px;" value="Survei">Survei</option>
	           <option style="font-size: 12px;" value="User">User</option>
	           <option style="font-size: 12px;" value="Role">Role</option>
	    </select>
        <label class="control-label sr-only" for="filter-user">User</label>
        <select class="select selectpicker filter-control" name="filter[user]" data-style="btn-default" data-post="user" data-live-search="true" data-size="5" data-live-search="true">
        	    <option style="font-size: 12px;" value="0">User</option>
                @foreach(App\Models\Auths\User::all() as $user)
                    <option value="{{ $user->name }}">{{ $user->name }}</option>
                @endforeach
       </select>       
    </div>
@endsection
@push('scripts')
    <script> 
        $('.btn-user').hide();
    </script>
@endpush