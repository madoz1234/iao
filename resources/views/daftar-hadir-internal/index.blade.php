@extends('layouts.extend')

@section('content')
<div class="image-container set-full-height" style="background-image: url('../../gate/img/waskita.jpg')">
    <!--   Creative Tim Branding   -->
    <a href="#">
         <div class="logo-container">
            <div class="logo">
                <img src="{{  asset('src/img/logo-mini.png') }}">
            </div>
            <div class="brand">
                <h4>PT. Waskita Karya (Persero)</h4>
            </div>
        </div>
    </a>

    <!--  Made With Material Kit  -->
    <a href="#" class="made-with-mk">
        <div class="brand">IAO</div>
        <div class="made-with"><strong>Internal Audit Online</strong></div>
    </a>

    <!--   Big container   -->
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <!--      Wizard container        -->
                <div class="wizard-container">
                    <div class="card wizard-card" data-color="red" id="wizard">
                        <div class="wizard-header">
                            <h3 class="wizard-title">
                                Daftar Hadir Rapat <br>{{ str_replace(',','',$record->tanggal_convert) }}
                            </h3>
                            <div class="alert alert-danger showerror" style="display: none; margin-bottom: none" role="alert">
                                <span class="fa fa-exclamation-triangle " aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                                Lengkapi data yang kosong
                            </div>
                        </div>
                        <div class="content">
                            <div class="row show-sukses">
                                <div class="col-sm-12 text-center">
                                    <div class="choice active" data-toggle="wizard-radio" rel="tooltip" title="Telah Terdaftar.">
                                        <input type="radio" readonly name="job" value="Design">
                                        <div class="icon">
                                            <i class="material-icons">done</i>
                                        </div>
                                        <h6>Terimakasih telah mengisi daftar hadir</h6>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center">
                                    <a href="javascript:void(0)" class="logout"><button class="btn btn-mini btn-primary btn-login" type="button">{{ __('Logout') }}</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div> <!-- row -->
    </div> <!--  big container -->

    <div class="footer">
        <div class="container text-center">
            &copy; 2019 PT. Waskita Karya (Persero) TBK.
        </div>
    </div>
</div>
@endsection
