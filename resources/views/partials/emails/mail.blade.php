<!DOCTYPE html>
<html lang="it"><head><meta http-equiv="content-type" content="text/html; charset=UTF-8"><title>Action Item Emails :</title><!--


COLORE INTENSE  #9C010F
COLORE LIGHT #EDE8DA

TESTO LIGHT #3F3D33
TESTO INTENSE #ffffff


--><meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<style type="text/css">
  * {
    margin: 0;
    padding: 0;
  }
  body {
    font: 14px/1.5 sans-serif;
    background: #ebe6e6;
    color: #555;
  }
  a {
    color: #297ca0;
    text-decoration: none;
  }
  .container {
    padding: 2em;
  }
  header {
    text-align: center;
    background: #297ca0;
    color: #fff;
    position: relative;
  }
  header img {
    vertical-align: middle;
    position: absolute;
    top: 50%;
    left: 2em;
    transform: translateY(-50%);
  }
  header h1 {
    font-size: 1.5em;
    margin-top: 0;
  }
  header small {
    display: block;
    font-size: 12px;
    font-weight: normal;
  }
  .btn {
    display: inline-block;
    padding: 10px 25px;
    font-size: 1.2em;
    background: #297ca0;
    color: #fff;
    text-decoration: none;
    margin-bottom: 20px;
  }
  p {
    margin-bottom: 20px;
  }
  footer {
    background: #999;
    color: #fff;
    display: flex;
  }
  footer .container {
    width: 100%;
  }
  footer .one-two {
    display: block;
    width: 50%;
    float: left;
  }
  footer:after {
    content: " ";
    display: block;
    clear: both;
  }

  .btn.hadir.a {
    display: inline-block;
    padding: 10px 25px;
    font-size: 1.2em;
    background: #21bf73;
    color: #fff;
    text-decoration: none;
    margin-bottom: 20px;
  }
</style>
</head>
<body>
  <header>
    <div class="container">
      <img src="https://www.waskita.co.id/img/logo/wk.png" alt="logo" width="150px" height="100px">
      <h1>PT. Waskita Karya (Persero) Tbk.
        <small>Konfirmasi Undangan Kehadiran</small>
      </h1>
    </div>
  </header>
  <section>
    <div class="container">
      <table width="100%">
        <tbody>
          <tr>
            <td width="200px"><b>Nomor Rapat</b></td>
            <td> : {{ $record->nomor }}</td>
            <td width="200px"><b>Tanggal Rapat</b></td>
            <td> : {{ Carbon::parse($record->tanggal)->format('d-m-Y') }}</td>
          </tr>
          <tr>
            <td width="200px"><b>Agenda</b></td>
            <td> : <table id="example-materi" style="width: 100%;font-size: 12px;">
                        <tbody class="container-materi">
                            {{-- {{ dd($record->agendaInternal) }} --}}
                            @foreach($record->agendaInternal as $key => $data)
                                <tr>
                                    <td scope="row">
                                        -  {{ $data->agenda }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
            </td>
            <td width="200px"><b>Pukul</b></td>
            <td> : {{ $record->jam_mulai }} - {{ $record->jam_selesai }}</td>
          </tr>
          <tr>
            <td width="200px"><b>Lokasi Rapat</b></td>
            <td> : {{ $record->tempat }}</td>
          </tr>
          <tr>
            <td width="200px"><b>Keterangan Rapat</b></td>
            <td> : {{ $record->keterangan }}</td>
          </tr>
        </tbody>
      </table><br><br>
      {{-- <div style="text-align: center;">
        @if ($record->status == 0)
          <a class="btn hadir" href="http://iao-waskita.pragmadev.us/rapat/internal/email/hadir/{{ $user->id }}" target="_blank" style="color: #ffffff; background-color: #21bf73;">Hadir</a>
          <a class="btn hadir" href="{{ url('rapat/internal/email/hadir/', $user->id) }}" target="_blank" style="color: #ffffff; background-color: #21bf73;">Hadir</a>
          <a class="btn" href="http://iao-waskita.pragmadev.us/rapat/internal/email/tidakhadir/{{ $user->id }}" target="_blank" style="color: #ffffff; background-color: #fd5e53;">Tidak Hadir</a>
        @elseif ($record->status == 1)
          <a class="btn hadir" href="http://iao-waskita.pragmadev.us/rapat/eksternal-advisor/email/hadir/{{ $user->id }}" target="_blank" style="color: #ffffff; background-color: #21bf73;">Hadir</a>
          <a class="btn" href="http://iao-waskita.pragmadev.us/rapat/eksternal-advisor/email/tidakhadir/{{ $user->id }}" target="_blank" style="color: #ffffff; background-color: #fd5e53;">Tidak Hadir</a>
        @elseif ($record->status == 2)
          <a class="btn hadir" href="http://iao-waskita.pragmadev.us/rapat/eksternal-real/email/hadir/{{ $user->id }}" target="_blank" style="color: #ffffff; background-color: #21bf73;">Hadir</a>
          <a class="btn" href="http://iao-waskita.pragmadev.us/rapat/eksternal-real/email/tidakhadir/{{ $user->id }}" target="_blank" style="color: #ffffff; background-color: #fd5e53;">Tidak Hadir</a>
        @elseif ($record->status == 3)
          <a class="btn hadir" href="http://iao-waskita.pragmadev.us/rapat/eksternal-konsultasi/email/hadir/{{ $user->id }}" target="_blank" style="color: #ffffff; background-color: #21bf73;">Hadir</a>
          <a class="btn" href="http://iao-waskita.pragmadev.us/rapat/eksternal-konsultasi/email/tidakhadir/{{ $user->id }}" target="_blank" style="color: #ffffff; background-color: #fd5e53;">Tidak Hadir</a>
        @endif
      </div> --}}
    </div>
  </section>
  <footer>
    <div class="container">
      <div class="one-two" style="text-align: left">
        PT. Waskita Karya (Persero) Tbk.
      </div>
      <div class="one-two" style="text-align: right">
        <span>Waskita Building  MT Haryono Kav. No 10,<br>&emsp;&emsp;&emsp; Cawang - Jakarta 13340</span><br>
        <span>(021) 850-8510</span><br>
        <span>waskita@waskita.co.id</span>
      </div>
    </div>
  </footer>
</body>
</html>






