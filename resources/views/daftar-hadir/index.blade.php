@extends('layouts.extend')

@section('content')
<div class="image-container set-full-height" style="background-image: url('../../gate/img/waskita.jpg')">
    <!--   Creative Tim Branding   -->
    <a href="#">
         <div class="logo-container">
            <div class="logo">
                <img src="{{  asset('src/img/logo-mini.png') }}">
            </div>
            <div class="brand">
                <h4>PT. Waskita Karya (Persero)</h4>
            </div>
        </div>
    </a>

    <!--  Made With Material Kit  -->
    <a href="#" class="made-with-mk">
        <div class="brand">IAO</div>
        <div class="made-with"><strong>Internal Audit Online</strong></div>
    </a>

    <!--   Big container   -->
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <!--      Wizard container        -->
                <div class="wizard-container">
                    <div class="card wizard-card" data-color="red" id="wizard">
                        <div class="wizard-header">
                            <h3 class="wizard-title">
                                Daftar Hadir Rapat <br>{{ str_replace(',','',$record->tanggal_convert) }}
                            </h3>
                            <h5>Silahkan isi daftar hadir anda.</h5>
                            <div class="alert alert-danger showerror" style="display: none; margin-bottom: none" role="alert">
                                <span class="fa fa-exclamation-triangle " aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                                Lengkapi data yang kosong
                            </div>
                        </div>
                        <div class="content">
                            <div class="row form-input">
                                <form action="{{ route($routes.'.store') }}" method="POST" id="saveData" autocomplete="off">
                                    @csrf
                                    <input type="hidden" name="rapat_id" value="{{$record->id}}">
                                    <div class="col-sm-12">
                                        <div class="input-group" style="margin-right: 20px">
                                            <span class="input-group-addon">
                                                <i class="material-icons">assignment_ind</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Nama Lengkap</label>
                                                <input name="nama" type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="input-group" style="margin-right: 20px">
                                            <span class="input-group-addon">
                                                <i class="material-icons">email</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Email Anda</label>
                                                <input name="email" type="email" class="form-control">
                                            </div>
                                        </div>
                                        <div class="input-group" style="margin-right: 20px">
                                            <span class="input-group-addon">
                                                <i class="material-icons">call</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Nomor Handphone</label>
                                                <input name="nomor" type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" class="form-control">
                                            </div>
                                        </div>
                                        <div class="input-group" style="margin-right: 20px">
                                            <span class="input-group-addon">
                                                <i class="material-icons">business</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Perusahaan</label>
                                                <input name="perusahaan" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 text-center">
                                        <button class="btn btn-mini btn-primary btn-login save-data" type="button">{{ __('Submit') }}</button>
                                    </div>
                                </form>
                            </div>
                            <div class="row show-sukses" style="display: none">
                                <div class="col-sm-12 text-center">
                                    <div class="choice active" data-toggle="wizard-radio" rel="tooltip" title="Telah Terdaftar.">
                                        <input type="radio" readonly name="job" value="Design">
                                        <div class="icon">
                                            <i class="material-icons">done</i>
                                        </div>
                                        <h6>Terimakasih telah mengisi daftar hadir</h6>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center">
                                    <button class="btn btn-mini btn-primary btn-login kembali" type="button">{{ __('Kembali') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div> <!-- row -->
    </div> <!--  big container -->

    <div class="footer">
        <div class="container text-center">
            &copy; 2019 PT. Waskita Karya (Persero) TBK.
        </div>
    </div>
</div>
@endsection
