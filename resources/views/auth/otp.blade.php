@extends('layouts.auth')

@section('content')
    <div class="mb-3">
        <span class="text-muted">{{ $user->name }}</span>
    </div>
    <p>Account Status:
        @if($user->verified)
            Verified
        @else
            Not Verified
        @endif
    </p>
    @if(!$user->verified)
        <p>
          <a href="{{ route('user-verify') }}">Verify your account now</a>
        </p>
    @endif
@endsection