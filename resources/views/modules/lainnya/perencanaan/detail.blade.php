<div class="modal-header">
    <h5 class="modal-title" style="font-weight: bold;">Detail Data Kegiatan Lainnya</h5>
</div>
<div class="modal-body">
    <div class="row">
        @if($record->ket_svp)
            <div class="col-md-12">
                <table class="table hr-border">
                    <tbody>
                        <tr>
                            <td class="border-none text-red" colspan="3">
                                Status <b>Ditolak</b> SVP pada {{DateToStringWday($record->updated_at)}}
                            </td>
                        </tr>
                        <tr>
                            <td class="border-none wd-label"><b>Keterangan </b></td>
                            <td class="border-none wd-5 px-0">:</td>
                            <td class="border-none" style="text-align: justify;text-justify: inter-word;">{{ $record->ket_svp }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        @endif
        <div class="col-md-12">
            <table class="table">
                <tbody>
                    <tr>
                        <td class="border-none wd-label">Tahun</td>
                        <td class="border-none wd-5 px-0">:</td>
                        <td class="border-none">{{ $record->rkia->rencanaaudit->tahun }}</td>
                    </tr>
                    <tr>
                        <td class="border-none wd-label">Rencana Pelaksanaan</td>
                        <td class="border-none wd-5 px-0">:</td>
                        <td class="border-none">{{ $record->rkia->rencana }}</td>
                    </tr>
                    <tr>
                        <td class="border-none wd-label">Tipe</td>
                        <td class="border-none wd-5 px-0">:</td>
                        <td class="border-none"><span class="label label-success">Kegiatan Lainnya</span></td>
                    </tr>
                    <tr>
                        <td class="border-none wd-label">Kategori</td>
                        <td class="border-none wd-5 px-0">:</td>
                        <td class="border-none">{{ $record->rkia->kategori }}</td>
                    </tr>
                    <tr class="form-group">
                        <td class="border-none wd-label control-label">Jenis</td>
                        <td class="border-none wd-5 px-0 control-label">:</td>
                        <td class="border-none field">
                            @if($record->jenis)
                                {{ $record->jenis }}
                            @else
                                {!! '<p style="color: red;">*Belum dipilih</p>' !!}
                            @endif
                        </td>
                    </tr>
                    <tr class="form-group">
                        <td class="border-none wd-label control-label">Pimpinan Rapat</td>
                        <td class="border-none wd-5 px-0 control-label">:</td>
                        <td class="border-none field">
                            @if($record->pimpinan)
                                {{ $record->pimpinan->name }}
                            @else
                                {!! '<p style="color: red;">*Belum dipilih</p>' !!}
                            @endif
                        </td>
                    </tr>
                    <tr class="form-group">
                        <td class="border-none wd-label control-label">Notulen</td>
                        <td class="border-none wd-5 px-0 control-label">:</td>
                        <td class="border-none field">
                            @if($record->notulen)
                                {{ $record->notulen->name }}
                            @else
                                {!! '<p style="color: red;">*Belum dipilih</p>' !!}
                            @endif
                        </td>
                    </tr>
                    <tr class="form-group">
                        <td class="border-none wd-label control-label">Tanggal Pelaksanaan</td>
                        <td class="border-none wd-5 px-0 control-label">:</td>
                        <td class="border-none field">
                            @if($record->tanggal_rapat)
                                {{ $record->tanggal }}
                            @else
                                {!! '<p style="color: red;">*Belum dipilih</p>' !!}
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
</div>
<div class="loading dimmer padder-v">
    <div class="loader"></div>
</div>