@extends('layouts.form')
@section('title', 'Detil')
@section('side-header')
   <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color: transparent !important;">
        <?php $i=1; $last=count($breadcrumb);?>
         @foreach ($breadcrumb as $name => $link)
            @if($i++ != $last)
                <li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
         @endforeach
      </ol>
    </nav>
@endsection
@section('body')
<ul id="myTabs" class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Data Rapat</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Daftar Hadir</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Risalah Rapat</a></li>
</ul>
<form action="{{ route($routes.'.update', $record->rapat->id) }}" method="POST" id="formData">
@method('PATCH')
@csrf
    <div id="myTabContent" class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">
            <div class="panel panel-default">
                <div class="panel-body" style="padding-bottom: 0px;">
                    <input type="hidden" name="id" value="{{$record->rapat->id}}">
                    <input type="hidden" name="id" value="{{$record->rapat->id}}">
                    <div class="form-row">
                        {{-- kiri --}}
                        <div class="form-group col-xs-6" style="padding-right: 14px;">
                            <div class="form-group col-xs-12 field" style="padding-right: 14px;">
                                <label for="inputEmail4">Tahun</label>
                                <input type="text" class="form-control" value="{{ $record->rkia->rencanaaudit->tahun }}" readonly>
                            </div>
                            <div class="form-group col-xs-12 field" style="padding-right: 14px;">
                                <label for="inputEmail4">Rencana Pelaksanaan</label>
                                <input type="text" class="form-control" value="{{ $record->rkia->rencana }}" readonly>
                            </div>
                            <div class="col-xs-12" style="padding-right: 14px;">
                                <div class="form-row">
                                    <div class="form-group col-xs-6 field" style="padding-right: 14px;">
                                        <label for="inputEmail4">Tipe</label>
                                        <input type="text" class="form-control" value="Kegiatan Lainnya" readonly>
                                    </div>
                                    <div class="form-group col-xs-6 field" style="padding-left: 14px;">
                                        <label for="inputEmail4">Jenis</label>
                                        <input type="text" class="form-control" value="{{ $record->jenis }}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 field" style="padding-right: 14px;">
                                <label for="inputEmail4">Nomor Rapat</label>
                                <input type="text" name="nomor" class="form-control" placeholder="Nomor Rapat" value="{{$record->rapat->nomor}}" disabled="">
                                <input type="hidden" class="form-control" name="jumlah_peserta" value="{{$record->rapat->daftarHadir->count()}}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" placeholder="Jumlah Peserta">
                            </div>
                            <div class="form-group col-xs-12 field" style="padding-right: 14px;">
                                <label for="inputEmail4">Tempat</label>
                                <input type="text" name="tempat" class="form-control" placeholder="Tempat" value="{{$record->rapat->tempat}}" disabled="">
                            </div>
                            <div class="col-xs-12" style="padding-right: 14px;">
                                <div class="form-row">
                                    <div class="form-group col-xs-6 field" style="padding-right: 14px;">
                                        <label for="inputEmail4">Hari</label>
                                        <input type="text" name="hari" class="form-control" placeholder="Hari" disabled="" value={{ App\Libraries\CoreSn::Day($record->rapat->tanggal) }}>
                                    </div>
                                    <div class="form-group col-xs-6 field" style="padding-left: 14px;">
                                        <label for="inputEmail4">Tanggal</label>
                                        <div class="input-group" data-placement="left" data-align="top" data-autoclose="true">
                                            <input type="text" class="form-control tanggal" name="tanggal" placeholder="Tanggal Pelaksanaan" value="{{ Carbon::parse($record->rapat->tanggal)->format('d/m/Y') }}" disabled="">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12" style="padding-right: 14px;">
                                <div class="form-row">
                                    <div class="form-group col-xs-6 field" style="padding-right: 14px;">
                                        <label for="inputEmail4">Jam</label>
                                        <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                            <input type="text" class="form-control" name="jam_mulai" placeholder="Mulai" value="{{$record->rapat->jam_mulai}}" disabled="">
                                            <span class="input-group-addon">
                                                <span class="fa fa-clock-o"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-6 field" style="padding-left: 14px;">
                                        <label for="inputEmail4">&nbsp</label>
                                        <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                            <input type="text" class="form-control" name="jam_selesai" placeholder="Selesai" value="{{$record->rapat->jam_selesai}}" disabled="">
                                            <span class="input-group-addon">
                                                <span class="fa fa-clock-o"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- kanan --}}
                        <div class="form-group col-xs-6" style="padding-left: 14px;">
                            <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                                <label for="inputEmail4">Kategori</label>
                                <input type="text" class="form-control" value="{{ $record->rkia->lain->nama }}" readonly>
                            </div>
                            <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                                <label for="inputEmail4">Pimpinan Rapat</label>
                                <input type="text" class="form-control" value="{{ $record->pimpinan->name }}" readonly>
                            </div>
                            <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                                <label for="inputEmail4">Notulen</label>
                                <input type="text" class="form-control" value="{{ $record->notulen->name }}" readonly>
                            </div>
                            <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                                <table id="example-materi" style="width: 100%;font-size: 12px;">
                                    <tbody class="container-materi">
                                        {{-- {{ dd($record->rapat->agendaInternal) }} --}}
                                        @foreach($record->rapat->agendaInternal as $key => $data)
                                            <tr class="data-container-{{$key+1}}" data-id="{{$key+1}}">
                                                @if($key == 0)
                                                <input type="hidden" name="data[{{ $key }}][id]" value="{{ $data->id }}">
                                                <input type="hidden" name="exists[]" value="{{$data->id}}">
                                                <td scope="row">
                                                    <div class="field">
                                                        <label for="inputEmail4">Agenda</label>
                                                        <input type="text" name="data[{{$key}}][agenda]" data-id="{{$key}}" class="form-control" placeholder="Agenda" value="{{ $data->agenda }}" disabled="">
                                                    </div>
                                                </td>
                                                @else
                                                <input type="hidden" name="data[{{ $key }}][id]" value="{{ $data->id }}">
                                                <input type="hidden" name="exists[]" value="{{$data->id}}">
                                                <td scope="row">
                                                    <div class="field">
                                                        <label for="inputEmail4">&nbsp;</label>
                                                        <input type="text" name="data[{{$key}}][agenda]" data-id="{{$key}}" class="form-control" placeholder="Agenda" value="{{ $data->agenda }}" disabled="">
                                                    </div>
                                                </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                                <label for="inputEmail4">Keterangan Rapat</label>
                                <textarea name="keterangan" rows="3" class="form-control" placeholder="Keterangan Rapat" disabled="">{{$record->rapat->keterangan}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        @if ($record->rapat->files()->where('flag','hadir')->get()->count() > 0)
                            <div class="form-group col-md-12 field">
                                <label for="hadir" class="control-label">Daftar Hadir</label>
                                <div class="file-loading">
                                    <input id="hadir" name="daftar_hadir" type="file" readonly class="form-control" 
                                    data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf">
                                </div>
                            </div>
                        @endif
                        @if ($record->rapat->files()->where('flag','other')->get()->count() > 0)
                            <div class="form-group col-md-12 field">
                                <label for="other" class="control-label">Surat Undangan & Dokumentasi</label>
                                <div class="file-loading">
                                    <input id="other" name="other[]" multiple type="file" readonly class="form-control" 
                                    data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf,image/x-png,image/gif,image/jpeg">
                                </div>
                            </div>
                        @endif
                    </div>


                    {{-- daftar peserta --}}
                    <div class="col-sm-12">
                        <h4 class="m-t-lg m-b" style="font-weight: bold;">Peserta Undangan</h4>
                        <table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
                            <thead style="background-color: #f9fafb;">
                                <tr>
                                    {{-- <th scope="col" style="text-align: center; width: 5%;">#</th>
                                    <th scope="col" style="text-align: center; width: 20%;">Nama</th>
                                    <th scope="col" style="text-align: center; width: 20%;">Email</th>
                                    <th scope="col" style="text-align: center; width: 10%;">BU/CO</th>
                                    <th scope="col" style="text-align: center; width: 35%;">Project</th>
                                    <th scope="col" style="text-align: center; width: 10%;">Status</th> --}}
                                    <th scope="col" style="text-align: center; width: 5%;">#</th>
                                    <th scope="col" style="text-align: center; width: 20%;">Nama</th>
                                    <th scope="col" style="text-align: center; width: 15%;">Email</th>
                                    <th scope="col" style="text-align: center; width: 15%;">No Telp/HP</th>
                                    <th scope="col" style="text-align: center; width: 15%;">Jabatan</th>
                                    <th scope="col" style="text-align: center; width: 15%;">BU/CO</th>
                                    <th scope="col" style="text-align: center; width: 40%;">Project</th>
                                    <th scope="col" style="text-align: center; width: 5%;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="container">
                                @foreach($record->rapat->undanganInternal as $key => $data)
                                {{-- {{ dd($data) }} --}}
                                {{-- {{ dd($data->user->bu_pic->bu->kode) }} --}}
                                <tr class="data-container-{{$key+1}}" data-id="{{$key+1}}">
                                    <input type="hidden" name="detail[{{$key}}][id]" value="{{ $data->id }}">
                                    <input type="hidden" name="exists[]" value="{{$data->id}}">
                                    <td scope="row" style="text-align: center;">
                                        <label style="margin-top: 6px;" class="numboor-{{$key+1}}">{{ $key+1 }}</label>
                                    </td>
                                    <td scope="row">
                                        <div class="field">
                                            <select class="selectpicker form-control user" 
                                            name="detail[{{$key}}][user_id]"
                                            data-style="btn-default" 
                                            data-live-search="true" 
                                            title="(Nama)"
                                            data-size="10"
                                            data-id="{{$key}}"
                                            disabled="" 
                                            >
                                            {!! \App\Models\Auths\User::options('name', 'id', ['selected' => $data->user_id] ,'') !!}
                                            </select>
                                        </div>
                                    </td>
                                    <td scope="row">
                                        <input type="text" name="detail[{{$key}}][email]" id="email" data-id="{{$key}}" class="form-control email-{{$key}}" placeholder="Email" disabled>
                                    </td>
                                    <td scope="row">
                                        <input type="text" name="detail[{{$key}}][hp]" id="hp" data-id="{{$key}}" class="form-control hp-{{$key}}" placeholder="No HP" disabled>
                                    </td>
                                    <td scope="row">
                                        <input type="text" name="detail[{{$key}}][jabatan]" id="jabatan" data-id="{{$key}}" class="form-control jabatan-{{$key}}" placeholder="Jabatan" disabled>
                                    </td>
                                    <td scope="row">
                                        <input type="text" name="detail[{{$key}}][bu_co]" id="bu_co" data-id="{{$key}}" class="form-control bu_co-{{$key}}" placeholder="BU/CO" disabled>
                                    </td>
                                    <td scope="row">
                                        <input type="hidden" name="val_{{$key}}" value="{{ $data->project_id }}">
                                        <select class="selectpicker form-control project data-project-" 
                                            name="detail[{{$key}}][project_id][]"
                                            data-style="btn-default" 
                                            data-live-search="true" 
                                            title="(Project)"
                                            data-size="10"
                                            id="project"
                                            data-id="{{$key}}"
                                            disabled=""
                                            data-width="500px"
                                            >
                                            <option value="0" @if($data->undanganInternalProject[0]['project_id'] == 0)selected @endif>Tidak ada pilihan</option>
                                            @foreach(App\Models\Master\Project::get() as $p)
                                                <option value="{{ $p->id }}" @if(in_array($p->id, $data->undanganInternalProject->pluck('project_id')->toArray(), TRUE)) selected @endif>{{ $p->nama }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td style="text-align: center;">
                                        @if (auth()->user()->id == $record->rapat->created_by)
                                            @if ($data->status == 2)
                                                <span class="btn btn-success">Hadir</span>
                                            @else
                                                <span class="btn btn-warning">Undangan Terkirim</span>
                                            @endif
                                        @else
                                            -
                                        @endif
                                        {{-- @if($data->status == 1)
                                            <span class="btn btn-warning">Undangan Terkirim</span>
                                        @elseif($data->status == 2)
                                            <span class="btn btn-success">Hadir</span>
                                        @elseif($data->status == 3)
                                            <span class="btn btn-danger">Tidak Hadir</span>
                                        @else
                                            <span class="btn btn-default">Belum Dikirim</span>
                                        @endif --}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-xs-12">
                            <br>
                            <div class="text-right">
                                <button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
                                <button type="button" class="btn btn-primary next-tab">Selanjutnya</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="profile">
            <div class="panel panel-default">
                <div class="panel-body" style="padding-bottom: 0px;">
                    <div class="form-row">
                        <table class="table table-bordered" style="font-size: 12px;">
                            <thead>
                                <tr>
                                    <th class="text-center">Nama</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Nomor Handphone</th>
                                    @if ($record->rapat->status == 0)
                                        <th class="text-center">Jabatan</th>
                                    @else
                                        <th class="text-center">Perusahaan</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody class="show-daftar">
                                @if ($record->rapat->daftarHadir->count() > 0)
                                    @if ($record->rapat->status == 0)
                                        @foreach ($record->rapat->daftarHadir as $key => $item)
                                            <tr>
                                                @if (isset($item->user_id))
                                                    <td class="form-row">
                                                        {{$item->user->name}}
                                                    </td>
                                                    <td class="form-row">
                                                        {{$item->user->email}}
                                                    </td>
                                                    <td class="form-row">
                                                        {{$item->user->phone}}
                                                    </td>
                                                    <td class="form-row">
                                                        {{$item->user->roles->first()->name}}
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    @else
                                        @foreach ($record->rapat->daftarHadir as $key => $item)
                                            <tr>
                                                <td class="form-row">
                                                    {{$item->nama}}
                                                </td>
                                                <td class="form-row">
                                                    {{$item->email}}
                                                </td>
                                                <td class="form-row">
                                                    {{$item->nomor}}
                                                </td>
                                                <td class="form-row">
                                                    {{$item->perusahaan}}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @else
                                    <tr>
                                        <td colspan="4" class="text-center">
                                            Data tidak ditemukan
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-xs-12">
                            <br>
                            <div class="text-right">
                                <button type="button" class="btn btn-cancel previous-tab">Sebelumnya</button>
                                <button type="button" class="btn btn-primary next-tab">Selanjutnya</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="messages">
            <div class="panel panel-default">
                <div class="panel-body" style="padding-bottom: 0px;">
                    <div class="form-row">
                        <table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
                            <thead style="background-color: #f9fafb;">
                                <tr>
                                    <th scope="col" style="text-align: center; width: 5%;">#</th>
                                    <th scope="col" style="text-align: center; width: 15%;">Risalah</th>
                                    <th scope="col" style="text-align: center; width: 15%;">Pen. Jawab</th>
                                    <th scope="col" style="text-align: center; width: 15%;">Realisasi</th>
                                    <th scope="col" style="text-align: center; width: 15%;">Pen. Jawab</th>
                                    <th scope="col" style="text-align: center; width: 15%;">Rencana yang akan datang</th>
                                    <th scope="col" style="text-align: center; width: 15%;">Pen. Jawab</th>
                                </tr>
                            </thead>
                            {{-- <tbody class="container"> --}}
                            <tbody>
                                @if (count($record->rapat->risalah) > 0)
                                    @foreach($record->rapat->risalah as $key => $data)
                                    <tr class="data-container-{{$key+1}}" data-id="{{$key+1}}">
                                        <input type="hidden" name="detail[{{$key}}][id]" value="{{ $data->id }}">
                                        <input type="hidden" name="exists[]" value="{{$data->id}}">
                                        <td scope="row" style="text-align: center;">
                                            <label class="numboor-{{$key+1}}">{{ $key+1 }}</label>
                                        </td>
                                        <td scope="row">
                                            <textarea class="form-control" name="detail[{{$key}}][risalah]" id="exampleFormControlTextarea1" placeholder="Risalah" rows="1" style="height: 32px;" disabled="">{{$data->risalah}}</textarea>
                                        </td>
                                        <td scope="row">
                                            <div class="field">
                                                <select class="selectpicker form-control user" 
                                                name="detail[{{$key}}][pic_1_id]"
                                                data-style="btn-default" 
                                                data-live-search="true" 
                                                title="(Pen. Jawab)"
                                                data-size="10"
                                                disabled="" 
                                                >
                                                    @foreach(App\Models\Auths\User::where('id','<>', 1)->where('tipe', 1)->get() as $user)
                                                    <option value="{{ $user->id }}" {{ ($user->id == $data->pic_1_id) ? 'selected' : '' }}>{{ $user->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </td>
                                        <td scope="row">
                                            <textarea class="form-control" name="detail[{{$key}}][realisasi]" id="exampleFormControlTextarea1" placeholder="Realisasi" rows="1" style="height: 32px;" disabled="">{{$data->realisasi}}</textarea>
                                        </td>
                                        <td scope="row">
                                            <div class="field">
                                                <select class="selectpicker form-control user" 
                                                name="detail[{{$key}}][pic_2_id]"
                                                data-style="btn-default" 
                                                data-live-search="true" 
                                                title="(Pen. Jawab)"
                                                data-size="10"
                                                disabled="" 
                                                >
                                                    @foreach(App\Models\Auths\User::where('id','<>', 1)->where('tipe', 1)->get() as $user)
                                                    <option value="{{ $user->id }}" {{ ($user->id == $data->pic_2_id) ? 'selected' : '' }}>{{ $user->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </td>
                                        <td scope="row">
                                            <textarea class="form-control" name="detail[{{$key}}][rencana]" id="exampleFormControlTextarea1" placeholder="Rencana yang akan datang" rows="1" style="height: 32px;" disabled="">{{$data->rencana}}</textarea>
                                        </td>
                                        <td scope="row">
                                            <div class="field">
                                                <select class="selectpicker form-control user" 
                                                name="detail[{{$key}}][pic_3_id]"
                                                data-style="btn-default" 
                                                data-live-search="true" 
                                                title="(Pen. Jawab)"
                                                data-size="10"
                                                disabled="" 
                                                >
                                                    @foreach(App\Models\Auths\User::where('id','<>', 1)->where('tipe', 1)->get() as $user)
                                                    <option value="{{ $user->id }}" {{ ($user->id == $data->pic_3_id) ? 'selected' : '' }}>{{ $user->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                <tr class="data-container-1" data-id="1">
                                    <td scope="row" style="text-align: center;" colspan="7">
                                        Data tidak ditemukan
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-xs-12">
                            <br>
                            <div class="text-right">
                                <button type="button" class="btn btn-cancel previous-tab">Sebelumnya</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
@push('js')
    <script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
    <script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
        .input-group.file-caption-main{
            display: none;
        }
        td{
            font-size: 12px;
        }
    </style>
    <style>
        div.dropdown-menu.open{
          max-width: 500px !important;
          overflow: hidden;
        }
        ul.dropdown-menu.inner{
          max-width: 500px !important;
          overflow-y: auto;
        }
    </style>
@endpush

@push('scripts')
    <script> 
        $(document).ready(function(){
            $('.user').each(function(){
                var row = $(this).closest('tr');
                var id = $(this).data("id");
                $.ajax({
                    url: '{{ url('ajax/option/get-user') }}',
                    type: 'POST',
                    data: {
                        _token: "{{ csrf_token() }}",
                        user_id: this.value,
                        id: id
                    },
                }).done(function(response) {
                        var val = $('input[name="val_'+id+'"]').val();
                        row.find('.email-'+id).val(response.email);
                        row.find('.bu_co-'+id).val(response.bu);
                        row.find('.hp-'+id).val(response.phone);
                        row.find('.jabatan-'+id).val(response.jabatan);
                        $('select[name="detail['+id+'][project_id]"]').html(response.project);
                        $('select[name="detail['+id+'][project_id]"]').selectpicker('refresh');
                        $('select[name="detail['+id+'][project_id]"]').children('option[value="'+val+'"]').prop('selected',true);
                        $('select[name="detail['+id+'][project_id]"]').selectpicker('refresh');
                        // row.find('.data-project-'+id).val(response.project);
                }).fail(function() {
                    console.log("error");
                });
            });

            // $('.project').each(function(){
            //     var row = $(this).closest('tr');
            //     var id = $(this).data("id");
            //     $.ajax({
            //         url: '{{ url('ajax/option/get-project') }}',
            //         type: 'POST',
            //         data: {
            //             _token: "{{ csrf_token() }}",
            //             user_id: this.value,
            //             id: id
            //         },
            //     }).done(function(response) {
            //             row.find('.bu_co-'+id).val(response.email);
            //         // });
            //     }).fail(function() {
            //         console.log("error");
            //     });
            // });
        });

        $(document).ready(function(){
            // reloadMask();
            $('.tanggal').datepicker({
                format: 'mm/dd/yyyy',
                startDate: '-0d',
                orientation: "auto",
                autoclose:true,
            });
            $('.waktu').clockpicker({
                autoclose:true,
                'default': 'now',
            });

            var dor = '{{ $record->rapat->fileHadirJson() }}';
            var filerihadir = '{{ $record->rapat->fileHadir() }}';
            $("#hadir").fileinput({
                autoReplace: true,
                overwriteInitial: true,
                // maxFileCount: 1,
                // initialPreview: filerihadir,
                initialPreview: JSON.parse(filerihadir.replace(/&quot;/g,'"')),
                initialPreviewAsData: true,
                initialPreviewConfig: JSON.parse(dor.replace(/&quot;/g,'"')),
                // initialPreviewConfig: [
                //     {type: "{{ $record->rapat->files()->where('flag','hadir')->pluck('type')->first() }}",caption: "{{ $record->rapat->files()->where('flag','hadir')->pluck('filename')->first() }}", downloadUrl: filerihadir, size: 930321, width: "120px", key: 1},
                // ],
                initialCaption: "{{ $record->rapat->files()->where('flag','hadir')->pluck('filename')->first() }}",
                initialPreviewShowDelete: false,
                showRemove: false,
                showClose: false,
                layoutTemplates: {actionDelete: ''}, // disable thumbnail deletion
                // allowedFileExtensions: ["{{ $record->rapat->files()->where('flag','hadir')->pluck('type')->first() }}"]
            });
            
            // var filerihadir = '{{ $record->rapat->fileHadir() }}';
            // $("#hadir").fileinput({
            //     autoReplace: true,
            //     overwriteInitial: true,
            //     maxFileCount: 1,
            //     initialPreview: [filerihadir],
            //     initialPreviewAsData: true,
            //     initialPreviewConfig: [
            //         {type: "{{ $record->rapat->files()->where('flag','hadir')->pluck('type')->first() }}",caption: "{{ $record->rapat->files()->where('flag','hadir')->pluck('filename')->first() }}", downloadUrl: filerihadir, size: 930321, width: "120px", key: 1},
            //     ],
            //     initialCaption: "{{ $record->rapat->files()->where('flag','hadir')->pluck('filename')->first() }}",
            //     initialPreviewShowDelete: false,
            //     showRemove: false,
            //     showClose: false,
            //     layoutTemplates: {actionDelete: ''}, // disable thumbnail deletion
            //     allowedFileExtensions: ["{{ $record->rapat->files()->where('flag','hadir')->pluck('type')->first() }}"]
            // });
                var obj = '{{ $record->rapat->fileOtherJson() }}';
                var fileUri = '{{ $record->rapat->fileOther() }}';
            $('#other').fileinput({
                autoReplace: true,
                overwriteInitial: true,
                // maxFileCount: 1,
                initialPreview: JSON.parse(fileUri.replace(/&quot;/g,'"')),
                initialPreviewAsData: true,
                initialPreviewConfig: JSON.parse(obj.replace(/&quot;/g,'"')),
                // initialCaption: "{{ $record->rapat->files()->where('flag','hadir')->pluck('filename')->first() }}",
                initialPreviewShowDelete: false,
                showRemove: false,
                showClose: false,
                layoutTemplates: {actionDelete: ''}, // disable thumbnail deletion
            });
            var inputs = document.getElementsByClassName('kv-file-zoom');
            for(var i = 0; i < inputs.length; i++) {
                inputs[i].disabled = false;
            }
            $('#myTabs a').click(function (e) {
                e.preventDefault()
                $(this).tab('show')
            })
        });
        $(document).on('click','.next-tab', function(){
            var next = $('.nav-tabs > .active').next('li');
            if(next.length){
                next.find('a').trigger('click');
            }else{
                $('#myTabs a:first').tab('show');
            }
        });
        $(document).on('click','.previous-tab', function(){
            var prev = $('.nav-tabs > .active').prev('li')
            if(prev.length){
                prev.find('a').trigger('click');
            }else{
                $('#myTabs a:last').tab('show');
            }
        });
    </script>
    @yield('js-extra')
@endpush

