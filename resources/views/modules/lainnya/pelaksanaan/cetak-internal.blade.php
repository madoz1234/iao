<html>
<head>
    {{-- @include('style.pdf') --}}
    <style>
    @page { margin: 5px 30px 30px 50px; }
    body {
        /* TRBL */
        margin: 125px 5px 5px 5px;
        /* border: 1px solid #055998;  */
    }
    header{
        position: fixed;
    }
    .content{
        position: relative;
        padding-left:-5px ;
        padding-right:-5px ;
        text-align: justify;
        text-justify: inter-word;
    }
    p{
        font-size: 12px;
        margin-top: 0px;
        text-align: center;
        text-justify: inter-word;
    }
    i{
        font-size: 12px;
    }
    input[type=checkbox] { 
        display: inline;
        position: relative; 
        vertical-align: baseline;
        /*height: 100%;*/
    }
    .partial{
        -webkit-region-break-inside: avoid;
        position: relative;
        page-break-inside: avoid;
    }

    .tbl-transparant-header{
        width: 100%;
        /* font-size: 17px; */
        text-align: center;
        text-justify: inter-word;
    }
    .tbl-transparant{
        width: 100%;
        /* font-size: 17px; */
        text-align: justify;
        text-justify: inter-word;
    }
    .tbl-transparant-data{
        width: 100%;
        text-align: left;
    }
    .tbl-transparant-data > tbody > tr > td > u > p{
        text-align: left;
    }
    .tbl-transparant-data > tbody > tr > td > p{
        text-align: left;
    }
    .header-content{
        text-align: center;
    }
    
    h2{
        font-size: 30px;
    }
    h5{
        margin-bottom: 0px;
        font-size: 17px;
    }
    .page-break {
        page-break-after: always;
    }
    .p-clasess {
        font-size: 12px;
        margin-top: 5px;
        margin-bottom: 5px;
        text-align: center;
        text-justify: inter-word;
    }
    .p-content {
        font-size: 12px;
        margin-top: 5px;
        text-align: left;
        margin-bottom: 5px;
        text-justify: inter-word;
    }
</style>
</head>
<body>
    {{-- @include('style.temp') --}}
    <script type="text/php">
        if (isset($pdf)) {
            $font = $fontMetrics->getFont("Times New Romans", "bold");
             $pdf->page_text(730, 570, "Halaman {PAGE_NUM} dari {PAGE_COUNT}", $font, 10, array(0, 0, 0));
        }
    </script>
    <header>
        <table class="ui table bordered" style="width: 100%">
            <tr>
                <td width="5%">
                    <img src="{{  asset('src/img/logo.png') }}" height="40px" style="margin-top: -16px;">
                </td>
                <td style="width: 77%; vertical-align: top;">
                    <p style="font-size: 16px;text-align: left; padding-top: 15px;"><b>PT. WASKITA KARYA(Persero) Tbk</b></p>
                </td>
                <td style="width: 18%;vertical-align: top;">
                    <table class="tbl-transparant" style="border : 1px solid black;">
                        <tr style="border : 1px solid black;">
                            <td style="text-align: center">Form. Prod 50</td>
                        </tr>
                    </table>
                    <table class="tbl-transparant" style="border : 1px solid black;">
                        <tr style="border : 1px solid black;">
                            <td style="text-align: center">Edisi : Juni 2017 Rev 7</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table class="ui table bordered" style="width: 100%;border : 2px solid black;">
            <tr>
                <td>
                <h3 style="margin: 0px 0px 0px 0px;text-align: center">RISALAH RAPAT</h3>
                </td>
            </tr>
        </table>
        <table class="" style="width: 100%;margin-top: 3px;border-collapse: collapse;">
            <tr>
                <td style="width: 25%;border : 1px solid black;">
                </td>
                <td style="width: 9%;border : 1px solid black;">
                    <p class="p-clasess">Pen. Jawab</p>
                </td>
                <td style="width: 25%;border : 1px solid black;">
                    <p class="p-clasess">Realisasi</p>
                </td>
                <td style="width: 9%;border : 1px solid black;">
                    <p class="p-clasess">Pen. Jawab</p>
                </td>
                <td style="width: 25%;border : 1px solid black;">
                    <p class="p-clasess">Rencana yang akan datang</p>
                </td>
                <td style="width: 9%;border : 1px solid black;">
                    <p class="p-clasess">Pen. Jawab</p>
                </td>
            </tr>
        </table>
    </header>
    <div class="content">
        <table class="" style="width: 100%;margin-top: 3px;border-collapse: collapse;">
            @if ($records->rapat->risalah->count() == 0)
                @if (isset($records->rapat->risalah) > 0 AND $records->rapat->risalah->count() > 0)
                    @foreach ($records->rapat->risalah as $key => $item)
                        @if ($records->rapat->status == 0)
                            <tr>
                                <td style="width: 25%;border : 1px solid black;vertical-align: top">
                                    <p class="p-content">
                                        {{  $key+1 }} . {{  $item->risalah }}
                                    </p>
                                </td>
                                <td style="width: 9%;border : 1px solid black;vertical-align: top">
                                    <p class="p-content">
                                        {{  $item->pic_1 ? $item->pic_1->name : '' }}
                                    </p>
                                </td>
                                <td style="width: 25%;border : 1px solid black;vertical-align: top">
                                    <p class="p-content">
                                        {{  $item->realisasi }}
                                    </p>
                                </td>
                                <td style="width: 9%;border : 1px solid black;vertical-align: top">
                                    <p class="p-content">
                                        {{  $item->pic_2 ? $item->pic_2->name : ''}}
                                    </p>
                                </td>
                                <td style="width: 25%;border : 1px solid black;vertical-align: top">
                                    <p class="p-content">
                                        {{  $item->rencana }}
                                    </p>
                                </td>
                                <td style="width: 9%;border : 1px solid black;vertical-align: top">
                                    <p class="p-content">
                                        {{  $item->pic_3 ? $item->pic_3->name : '' }}
                                    </p>
                                </td>
                            </tr>
                        @else
                            <tr>
                                <td style="width: 25%;border : 1px solid black;vertical-align: top">
                                    <p class="p-content">
                                        {{  $item->risalah }}
                                    </p>
                                </td>
                                <td style="width: 9%;border : 1px solid black;vertical-align: top">
                                    <p class="p-content">
                                        {{  $item->pen_1 }}
                                    </p>
                                </td>
                                <td style="width: 25%;border : 1px solid black;vertical-align: top">
                                    <p class="p-content">
                                        {{  $item->realisasi }}
                                    </p>
                                </td>
                                <td style="width: 9%;border : 1px solid black;vertical-align: top">
                                    <p class="p-content">
                                        {{  $item->pen_2 }}
                                    </p>
                                </td>
                                <td style="width: 25%;border : 1px solid black;vertical-align: top">
                                    <p class="p-content">
                                        {{  $item->rencana }}
                                    </p>
                                </td>
                                <td style="width: 9%;border : 1px solid black;vertical-align: top">
                                    <p class="p-content">
                                        {{  $item->pen_3 }}
                                    </p>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                @else
                <tr>
                    <td style="width: 25%;border : 1px solid black;vertical-align: top">
                        <p class="p-content">
                            &nbsp;
                        </p>
                    </td>
                    <td style="width: 9%;border : 1px solid black;vertical-align: top">
                        <p class="p-content">

                        </p>
                    </td>
                    <td style="width: 25%;border : 1px solid black;vertical-align: top">
                        <p class="p-content">

                        </p>
                    </td>
                    <td style="width: 9%;border : 1px solid black;vertical-align: top">
                        <p class="p-content">

                        </p>
                    </td>
                    <td style="width: 25%;border : 1px solid black;vertical-align: top">
                        <p class="p-content">

                        </p>
                    </td>
                    <td style="width: 9%;border : 1px solid black;vertical-align: top">
                        <p class="p-content">
                            
                        </p>
                    </td>
                </tr>
                @endif
            @else
                <tr>
                    <td colspan="6" style="text-align: center; border : 1px solid black;vertical-align: top">Tidak Ada Data</td>
                </tr>
            @endif
        </table>
        <br>
        <br>
        <table style="width:100%;font-size: 12px;">
            <tr>
                <td style="text-align:center;width:60%">
                    
                </td>
                <td style="text-align:center">
                    Disetujui oleh,
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <u>Pimpinan Rapat</u>
                </td>
                <td style="text-align:center;width:15%">
                    Jakarta, {{ $today }}
                    <br>
                    Disusun oleh,
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <u>Notulen</u>
                </td>
            </tr>
        </table> 
    </div>
</body>
</html>
