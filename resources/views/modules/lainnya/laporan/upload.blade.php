<form action="{{ route($routes.'.uploadFile', $record->id) }}" method="POST" id="formData">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Upload File Kegiatan Lainnya</h5>
    </div>
    <div class="modal-body">
        <div class="form-group">
            <input type="hidden" name="id" value="{{ $record->id }}">
            <input type="hidden" name="rencana_detail_id" value="{{ $record->rkia->id }}">
            <input type="hidden" name="risalah_id" value="{{ $risalah->id }}">
            <input type="hidden" name="status" value="3">
            <input type="hidden" name="isUpload" value="true">
            <div class="form-group field">
            	<label class="control-label">Upload File</label>
            	<div class="file-loading">
            		<input id="pdf" name="files_risalah[]" type="file" class="file form-control"
            		data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf" multiple>
            	</div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save-as-upload button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script>
        
    </script>
    @yield('js-extra')
@endpush
