@extends('layouts.form')
@section('title', 'Detil')
@section('side-header')
   <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color: transparent !important;">
        <?php $i=1; $last=count($breadcrumb);?>
         @foreach ($breadcrumb as $name => $link)
            @if($i++ != $last)
                <li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
         @endforeach
      </ol>
    </nav>
@endsection
@section('body')
<ul id="myTabs" class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Data Rapat</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Daftar Hadir</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Risalah Rapat</a></li>
</ul>
<form action="{{ route($routes.'.update', $record->rapat->id) }}" method="POST" id="formData">
@method('PATCH')
@csrf
    <div id="myTabContent" class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">
            <div class="panel panel-default">
                <div class="panel-body" style="padding-bottom: 0px;">
                    <input type="hidden" name="id" value="{{$record->rapat->id}}">
                    <input type="hidden" name="id" value="{{$record->rapat->id}}">
                    <div class="form-row">
                        {{-- kiri --}}
                        <div class="form-group col-xs-6" style="padding-right: 14px;">
                            <div class="form-group col-xs-12 field" style="padding-right: 14px;">
                                <label for="inputEmail4">Tahun</label>
                                <input type="text" class="form-control" value="{{ $record->rkia->rencanaaudit->tahun }}" readonly>
                            </div>
                            <div class="form-group col-xs-12 field" style="padding-right: 14px;">
                                <label for="inputEmail4">Rencana Pelaksanaan</label>
                                <input type="text" class="form-control" value="{{ $record->rkia->rencana }}" readonly>
                            </div>
                            <div class="col-xs-12" style="padding-right: 14px;">
                                <div class="form-row">
                                    <div class="form-group col-xs-6 field" style="padding-right: 14px;">
                                        <label for="inputEmail4">Tipe</label>
                                        <input type="text" class="form-control" value="Kegiatan Lainnya" readonly>
                                    </div>
                                    <div class="form-group col-xs-6 field" style="padding-left: 14px;">
                                        <label for="inputEmail4">Jenis</label>
                                        <input type="text" class="form-control" value="{{ $record->jenis }}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 field" style="padding-right: 14px;">
                                <label for="inputEmail4">Nomor Rapat</label>
                                <input type="text" name="nomor" class="form-control" placeholder="Nomor Rapat" value="{{$record->rapat->nomor}}" disabled="">
                                <input type="hidden" class="form-control" name="jumlah_peserta" value="{{$record->rapat->daftarHadir->count()}}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" placeholder="Jumlah Peserta">
                            </div>
                            <div class="form-group col-xs-12 field" style="padding-right: 14px;">
                                <label for="inputEmail4">Tempat</label>
                                <input type="text" name="tempat" class="form-control" placeholder="Tempat" value="{{$record->rapat->tempat}}" disabled="">
                            </div>
                            <div class="col-xs-12" style="padding-right: 14px;">
                                <div class="form-row">
                                    <div class="form-group col-xs-6 field" style="padding-right: 14px;">
                                        <label for="inputEmail4">Hari</label>
                                        <input type="text" name="hari" class="form-control" placeholder="Hari" disabled="" value={{ App\Libraries\CoreSn::Day($record->rapat->tanggal) }}>
                                    </div>
                                    <div class="form-group col-xs-6 field" style="padding-left: 14px;">
                                        <label for="inputEmail4">Tanggal</label>
                                        <div class="input-group" data-placement="left" data-align="top" data-autoclose="true">
                                            <input type="text" class="form-control tanggal" name="tanggal" placeholder="Tanggal Pelaksanaan" value="{{ Carbon::parse($record->rapat->tanggal)->format('d/m/Y') }}" disabled="">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12" style="padding-right: 14px;">
                                <div class="form-row">
                                    <div class="form-group col-xs-6 field" style="padding-right: 14px;">
                                        <label for="inputEmail4">Jam</label>
                                        <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                            <input type="text" class="form-control" name="jam_mulai" placeholder="Mulai" value="{{$record->rapat->jam_mulai}}" disabled="">
                                            <span class="input-group-addon">
                                                <span class="fa fa-clock-o"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-6 field" style="padding-left: 14px;">
                                        <label for="inputEmail4">&nbsp</label>
                                        <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                            <input type="text" class="form-control" name="jam_selesai" placeholder="Selesai" value="{{$record->rapat->jam_selesai}}" disabled="">
                                            <span class="input-group-addon">
                                                <span class="fa fa-clock-o"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- kanan --}}
                        <div class="form-group col-xs-6" style="padding-left: 14px;">
                            <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                                <label for="inputEmail4">Kategori</label>
                                <input type="text" class="form-control" value="{{ $record->rkia->lain->nama }}" readonly>
                            </div>
                            <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                                <label for="inputEmail4">Pimpinan Rapat</label>
                                <input type="text" class="form-control" value="{{ $record->pimpinan->name }}" readonly>
                            </div>
                            <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                                <label for="inputEmail4">Notulen</label>
                                <input type="text" class="form-control" value="{{ $record->notulen->name }}" readonly>
                            </div>
                            <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                                <table id="example-materi" style="width: 100%;font-size: 12px;">
                                    <tbody class="container-materi">
                                        {{-- {{ dd($record->rapat->agendaInternal) }} --}}
                                        @foreach($record->rapat->agendaInternal as $key => $data)
                                            <tr class="data-container-{{$key+1}}" data-id="{{$key+1}}">
                                                @if($key == 0)
                                                <input type="hidden" name="data[{{ $key }}][id]" value="{{ $data->id }}">
                                                <input type="hidden" name="exists[]" value="{{$data->id}}">
                                                <td scope="row">
                                                    <div class="field">
                                                        <label for="inputEmail4">Agenda</label>
                                                        <input type="text" name="data[{{$key}}][agenda]" data-id="{{$key}}" class="form-control" placeholder="Agenda" value="{{ $data->agenda }}" disabled="">
                                                    </div>
                                                </td>
                                                @else
                                                <input type="hidden" name="data[{{ $key }}][id]" value="{{ $data->id }}">
                                                <input type="hidden" name="exists[]" value="{{$data->id}}">
                                                <td scope="row">
                                                    <div class="field">
                                                        <label for="inputEmail4">&nbsp;</label>
                                                        <input type="text" name="data[{{$key}}][agenda]" data-id="{{$key}}" class="form-control" placeholder="Agenda" value="{{ $data->agenda }}" disabled="">
                                                    </div>
                                                </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                                <label for="inputEmail4">Keterangan Rapat</label>
                                <textarea name="keterangan" rows="3" class="form-control" placeholder="Keterangan Rapat" disabled="">{{$record->rapat->keterangan}}</textarea>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="form-row">
                        <div class="form-group col-md-6 field" style="padding-right: 14px;">
                            <label for="inputEmail4">Nomor Rapat</label>
                            <input type="text" name="nomor" value="{{$record->rapat->nomor}}" readonly class="form-control" placeholder="Nomor Rapat">
                        </div>
                        <div class="form-group col-md-6 field" style="padding-left: 14px;">
                            <label for="inputEmail4">Tema / Materi Rapat</label>
                            <input type="text" name="materi" value="{{$record->rapat->materi}}" readonly class="form-control" placeholder="Tema / Materi Rapat">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 field" style="padding-right: 14px;">
                            <label for="inputEmail4">Tanggal Rapat</label>
                            <input type="text" name="tanggal" value="{{ Carbon::createFromFormat('m/d/Y',$record->rapat->tanggal)->format('d/m/Y') }}" readonly class="form-control" placeholder="Tanggal Rapat">
                        </div>
                        <div class="form-group col-md-6 field" style="padding-left: 14px;">
                            <label for="inputEmail4">Pukul</label>
                            <input type="text" name="waktu" value="{{$record->rapat->waktu}}" readonly class="form-control" placeholder="Pukul">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 field" style="padding-right: 14px;">
                            <label for="inputEmail4">Lokasi Rapat</label>
                            <input type="text" name="tempat" value="{{$record->rapat->tempat}}" readonly class="form-control" placeholder="Lokasi Rapat">
                        </div>
                        <div class="form-group col-md-6 field" style="padding-left: 14px;">
                            <label for="inputEmail4">Jumlah Peserta</label>
                            <div class="input-group">
                                <input type="text" readonly class="form-control" name="jumlah_peserta" value="{{$record->rapat->daftarHadir->count()}}" data-inputmask="'alias' : 'numeric'" placeholder="Jumlah Peserta">
                                <span class="input-group-addon">Orang</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12 field">
                            <label for="inputEmail4">Keterangan Rapat</label>
                            <textarea name="keterangan" readonly class="form-control" placeholder="Keterangan Rapat">{{$record->rapat->keterangan}}</textarea>
                        </div>
                    </div> --}}
                    <div class="form-row">
                        @if ($record->rapat->files()->where('flag','hadir')->get()->count() > 0)
                            <div class="form-group col-md-12 field">
                                <label for="hadir" class="control-label">Daftar Hadir</label>
                                <div class="file-loading">
                                    <input id="hadir" name="daftar_hadir" type="file" readonly class="form-control" 
                                    data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf">
                                </div>
                            </div>
                        @endif
                        @if ($record->rapat->files()->where('flag','other')->get()->count() > 0)
                            <div class="form-group col-md-12 field">
                                <label for="other" class="control-label">Surat Undangan & Dokumentasi</label>
                                <div class="file-loading">
                                    <input id="other" name="other[]" multiple type="file" readonly class="form-control" 
                                    data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf,image/x-png,image/gif,image/jpeg">
                                </div>
                            </div>
                        @endif
                    </div>

                    {{-- daftar peserta --}}
                    <div class="col-sm-12">
                        <h4 class="m-t-lg m-b">Peserta Undangan</h4>
                        <table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
                            <thead style="background-color: #f9fafb;">
                                <tr>
                                    <th scope="col" style="text-align: center; width: 5%;">#</th>
                                    <th scope="col" style="text-align: center; width: 40%;">Email</th>
                                    <th scope="col" style="text-align: center; width: 55%;">Perusahaan</th>
                                    {{-- <th scope="col" style="text-align: center; width: 10%;">Status</th> --}}
                                </tr>
                            </thead>
                            <tbody class="container">
                                @foreach($record->rapat->undanganEksternal as $key => $data)
                                <tr class="data-container-{{$key+1}}" data-id="{{$key+1}}">
                                    <input type="hidden" name="detail[{{$key}}][id]" value="{{ $data->id }}">
                                    <input type="hidden" name="exists[]" value="{{$data->id}}">
                                    <td scope="row" style="text-align: center;">
                                        <label class="numboor-{{$key+1}}">{{ $key+1 }}</label>
                                    </td>
                                    <td scope="row">
                                        <input type="text" class="form-control" placeholder="Email" value="{{$data->email}}" disabled="">
                                    </td>
                                    <td scope="row">
                                        <input type="text" class="form-control" placeholder="Perusahaan" value="{{$data->perusahaan}}" disabled="">
                                    </td>
                                    {{-- <td style="text-align: center;">
                                        @if ($data->status == 2)
                                            <span class="btn btn-success">Hadir</span>
                                        @else
                                            <span class="btn btn-warning">Undangan Terkirim</span>
                                        @endif
                                    </td> --}}
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-xs-12">
                            <br>
                            <div class="text-right">
                                <button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
                                <button type="button" class="btn btn-primary next-tab">Selanjutnya</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="profile">
            <div class="panel panel-default">
                <div class="panel-body" style="padding-bottom: 0px;">
                    <div class="form-row">
                        <table class="table table-bordered" style="font-size: 12px;">
                            <thead>
                                <tr>
                                    <th class="text-center">Nama</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Nomor Handphone</th>
                                    @if ($record->rapat->status == 0)
                                        <th class="text-center">Jabatan</th>
                                    @else
                                        <th class="text-center">Perusahaan</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody class="show-daftar">
                                @if ($record->rapat->daftarHadir->count() > 0)
                                    @if ($record->rapat->status == 0)
                                        @foreach ($record->rapat->daftarHadir as $key => $item)
                                            <tr>
                                                @if (isset($item->user_id))
                                                    <td class="form-row">
                                                        {{$item->user->name}}
                                                    </td>
                                                    <td class="form-row">
                                                        {{$item->user->email}}
                                                    </td>
                                                    <td class="form-row">
                                                        {{$item->user->phone}}
                                                    </td>
                                                    <td class="form-row">
                                                        {{$item->user->roles->first()->name}}
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    @else
                                        @foreach ($record->rapat->daftarHadir as $key => $item)
                                            <tr>
                                                <td class="form-row">
                                                    {{$item->nama}}
                                                </td>
                                                <td class="form-row">
                                                    {{$item->email}}
                                                </td>
                                                <td class="form-row">
                                                    {{$item->nomor}}
                                                </td>
                                                <td class="form-row">
                                                    {{$item->perusahaan}}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @else
                                    <tr>
                                        <td colspan="4" class="text-center">
                                            Data tidak ditemukan
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-xs-12">
                            <br>
                            <div class="text-right">
                                <button type="button" class="btn btn-cancel previous-tab">Sebelumnya</button>
                                <button type="button" class="btn btn-primary next-tab">Selanjutnya</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="messages">
            <div class="panel panel-default">
                <div class="panel-body" style="padding-bottom: 0px;">
                    <div class="form-row">
                        <table class="table table-bordered" style="font-size: 12px;">
                            <thead>
                                <tr>
                                    <th class="text-center" width="16%">Risalah</th>
                                    <th class="text-center" width="8%">Pen. Jawab</th>
                                    <th class="text-center" width="15%">Realisasi</th>
                                    <th class="text-center" width="8%">Pen. Jawab</th>
                                    <th class="text-center" width="15%">Rencana yang akan datang</th>
                                    <th class="text-center" width="8%">Pen. Jawab</th>
                                </tr>
                            </thead>
                            <tbody class="show-risalah">
                                @if (isset($record->rapat->risalah) > 0 AND $record->rapat->risalah->count() > 0)
                                    @foreach ($record->rapat->risalah as $item)
                                        @if ($record->rapat->status == 0)
                                            <tr>
                                                <td>
                                                        {{  $item->risalah }}
                                                </td>
                                                <td class="text-center">
                                                        {{  $item->pic_1->name }}
                                                </td>
                                                <td>
                                                        {{  $item->realisasi }}
                                                </td>
                                                <td class="text-center">
                                                        {{  $item->pic_2->name }}
                                                </td>
                                                <td>
                                                        {{  $item->rencana }}
                                                </td>
                                                <td class="text-center">
                                                        {{  $item->pic_3->name }}
                                                </td>
                                            </tr>
                                        @else
                                            <tr>
                                                <td>
                                                        {{  $item->risalah }}
                                                </td>
                                                <td class="text-center">
                                                        {{  $item->pen_1 }}
                                                </td>
                                                <td>
                                                        {{  $item->realisasi }}
                                                </td>
                                                <td class="text-center">
                                                        {{  $item->pen_2 }}
                                                </td>
                                                <td>
                                                        {{  $item->rencana }}
                                                </td>
                                                <td class="text-center">
                                                        {{  $item->pen_3 }}
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @else
                                <tr>
                                    <td colspan="6" class="text-center">
                                        Data tidak ditemukan
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-xs-12">
                            <br>
                            <div class="text-right">
                                <button type="button" class="btn btn-cancel previous-tab">Sebelumnya</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
@push('js')
    <script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
    <script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
        .input-group.file-caption-main{
            display: none;
        }
        td{
            font-size: 12px;
        }
    </style>
@endpush

@push('scripts')
    <script> 
        $(document).ready(function(){
            // reloadMask();
            $('.tanggal').datepicker({
                format: 'mm/dd/yyyy',
                startDate: '-0d',
                orientation: "auto",
                autoclose:true,
            });
            $('.waktu').clockpicker({
                autoclose:true,
                'default': 'now',
            });
            
            var filerihadir = '{{ $record->rapat->fileHadir() }}';
            $("#hadir").fileinput({
                autoReplace: true,
                overwriteInitial: true,
                maxFileCount: 1,
                initialPreview: [filerihadir],
                initialPreviewAsData: true,
                initialPreviewConfig: [
                    {type: "{{ $record->rapat->files()->where('flag','hadir')->pluck('type')->first() }}",caption: "{{ $record->rapat->files()->where('flag','hadir')->pluck('filename')->first() }}", downloadUrl: filerihadir, size: 930321, width: "120px", key: 1},
                ],
                initialCaption: "{{ $record->rapat->files()->where('flag','hadir')->pluck('filename')->first() }}",
                initialPreviewShowDelete: false,
                showRemove: false,
                showClose: false,
                layoutTemplates: {actionDelete: ''}, // disable thumbnail deletion
                allowedFileExtensions: ["{{ $record->rapat->files()->where('flag','hadir')->pluck('type')->first() }}"]
            });
                var obj = '{{ $record->rapat->fileOtherJson() }}';
                var fileUri = '{{ $record->rapat->fileOther() }}';
            $('#other').fileinput({
                autoReplace: true,
                overwriteInitial: true,
                // maxFileCount: 1,
                initialPreview: JSON.parse(fileUri.replace(/&quot;/g,'"')),
                initialPreviewAsData: true,
                initialPreviewConfig: JSON.parse(obj.replace(/&quot;/g,'"')),
                // initialCaption: "{{ $record->rapat->files()->where('flag','hadir')->pluck('filename')->first() }}",
                initialPreviewShowDelete: false,
                showRemove: false,
                showClose: false,
                layoutTemplates: {actionDelete: ''}, // disable thumbnail deletion
            });
            var inputs = document.getElementsByClassName('kv-file-zoom');
            for(var i = 0; i < inputs.length; i++) {
                inputs[i].disabled = false;
            }
            $('#myTabs a').click(function (e) {
                e.preventDefault()
                $(this).tab('show')
            })
        });
        $(document).on('click','.next-tab', function(){
            var next = $('.nav-tabs > .active').next('li');
            if(next.length){
                next.find('a').trigger('click');
            }else{
                $('#myTabs a:first').tab('show');
            }
        });
        $(document).on('click','.previous-tab', function(){
            var prev = $('.nav-tabs > .active').prev('li')
            if(prev.length){
                prev.find('a').trigger('click');
            }else{
                $('#myTabs a:last').tab('show');
            }
        });

        $(document).on('click', '.aktif-button', function () {
            // console.log($(this).data('url'));
            // console.log('tara');
            var target = $(this).data('url');
            swal({
                title: 'Perhatian',
                text: "Apakah anda yakin untuk mengirimkan Undangan kepada peserta?",
                icon: 'warning',
                // showCancelButton: true,
                // confirmButtonText: 'Yakin',
                // cancelButtonText: 'Batal',
                // showLoaderOnConfirm: true,
                preConfirm: function (email) {
                    return new Promise(function (resolve, reject) {
                        $.ajax({
                            type: 'GET',
                            url: target,
                            data: {},
                            dataType: 'html',
                            success: function(data) { 
                                resolve()
                            },
                            error: function() { 
                                reject('Gagal Terkirim, Terjadi kesalahan dalam proses.')
                            }
                        });
                    })
                },
            }).then(function () {
                // console.log('dor');
                swal(
                    'Sukses',
                    'Notifikasi telah terkirim.',
                    'success').then(function(){ location.reload(); })
            })
        });
    </script>
    @yield('js-extra')
@endpush

