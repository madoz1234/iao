@extends('layouts.form')
@section('title', 'Ubah '.$title)
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<ul id="myTabs" class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Data Rapat</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Daftar Hadir</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Risalah Rapat</a></li>
</ul>
<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
@method('PATCH')
@csrf
    <div id="myTabContent" class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">
            <div class="panel panel-default">
                <div class="panel-body" style="padding-bottom: 0px;">
                    <input type="hidden" name="id" value="{{$record->id}}">
                    <div class="form-row">
                        <div class="form-group col-md-6 field" style="padding-right: 14px;">
                            <label for="inputEmail4">Nomor Rapat</label>
                            <input type="text" name="nomor" value="{{$record->nomor}}" class="form-control" placeholder="Nomor Rapat">
                        </div>
                        <div class="form-group col-md-6 field" style="padding-left: 14px;">
                            <label for="inputEmail4">Tema / Materi Rapat</label>
                            <input type="text" name="materi" value="{{$record->materi}}" class="form-control" placeholder="Tema / Materi Rapat">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 field" style="padding-right: 14px;">
                            <label for="inputEmail4">Tanggal Rapat</label>
                            <input type="text" name="tanggal" value="{{$record->tanggal}}" class="form-control tanggal" placeholder="Tanggal Rapat">
                        </div>
                        <div class="form-group col-md-6 field waktu" style="padding-left: 14px;">
                            <label for="inputEmail4">Pukul</label>
                            <input type="text" name="waktu" value="{{$record->waktu}}" class="form-control" placeholder="Pukul">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 field" style="padding-right: 14px;">
                            <label for="inputEmail4">Lokasi Rapat</label>
                            <input type="text" name="tempat" value="{{$record->tempat}}" class="form-control" placeholder="Lokasi Rapat">
                        </div>
                        <div class="form-group col-md-6 field" style="padding-left: 14px;">
                            <label for="inputEmail4">Jumlah Peserta</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="jumlah_peserta" value="{{$record->jumlah_peserta}}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" placeholder="Jumlah Peserta">
                                <span class="input-group-addon">Orang</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12 field">
                            <label for="inputEmail4">Keterangan Rapat</label>
                            <textarea name="keterangan" class="form-control" placeholder="Keterangan Rapat">{{$record->keterangan}}</textarea>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12 field">
                            <label for="hadir" class="control-label">Daftar Hadir</label>
                            <div class="file-loading">
                                <input id="hadir" name="daftar_hadir" type="file" class="form-control" 
                                data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf">
                            </div>
                        </div>
                        <div class="form-group col-md-12 field">
                            <label for="other" class="control-label">Surat Undangan & Dokumentasi</label>
                            <div class="file-loading">
                                <input id="other" name="other[]" multiple type="file" class="form-control" 
                                data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf,image/x-png,image/gif,image/jpeg">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-xs-12">
                            <br>
                            <div class="text-right">
                                <button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
                                <button type="button" class="btn btn-primary next-tab">Selanjutnya</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="profile">
            <div class="panel panel-default">
                <div class="panel-body" style="padding-bottom: 0px;">
                    <div class="form-row">
                        <table class="table" style="font-size: 12px;">
                            <thead>
                                <tr>
                                    <th class="text-center">Nama</th>
                                    <th class="text-center">Jabatan</th>
                                    <th class="text-center" width="5%">#</th>
                                </tr>
                            </thead>
                            <tbody class="show-daftar">
                                @if ($record->daftarHadir->count() > 0)
                                    @foreach ($record->daftarHadir as $key => $item)
                                        <tr>
                                            <td class="form-row">
                                                <div class="form-group col-xs-12 field">
                                                    <input type="hidden" name="key_daftar[]" value="{{$key}}">
                                                    <input type="text" name="nama_peserta[]" class="form-control" value="{{$item->nama}}" placeholder="Nama">
                                                </div>
                                            </td>
                                            <td class="form-row">
                                                <div class="form-group col-xs-12 field">
                                                    <input type="text" name="jabatan[]" class="form-control" value="{{$item->jabatan}}" placeholder="Jabatan">
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <a data-content="Ubah" class="add-line" data-id="1" class="edit button" data-toggle="tooltip" title="" data-original-title="Ubah" style="padding-right: 10px;">
                                                    <i class="fa fa-plus text-info"></i>
                                                </a>
                                                @if ($key != 0)
                                                    <a data-content="Detil" class="remove-line" data-id="1" class="m-l detil button" data-toggle="tooltip" title="" data-original-title="Detil">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td class="form-row">
                                            <div class="form-group col-xs-12 field">
                                                <input type="hidden" name="key_daftar[]" value="0">
                                                <input type="text" name="nama_peserta[]" class="form-control" placeholder="Nama">
                                            </div>
                                        </td>
                                        <td class="form-row">
                                            <div class="form-group col-xs-12 field">
                                                <input type="text" name="jabatan[]" class="form-control" placeholder="Jabatan">
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <a data-content="Ubah" class="add-line" data-id="1" class="edit button" data-toggle="tooltip" title="" data-original-title="Ubah">
                                                <i class="fa fa-plus text-info"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-xs-12">
                            <br>
                            <div class="text-right">
                                <button type="button" class="btn btn-cancel previous-tab">Kembali</button>
                                <button type="button" class="btn btn-primary next-tab">Selanjutnya</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="messages">
            <div class="panel panel-default">
                <div class="panel-body" style="padding-bottom: 0px;">
                    <div class="form-row">
                        <table class="table" style="font-size: 12px;">
                            <thead>
                                <tr>
                                    <th class="text-center" width="15%">Risalah</th>
                                    <th class="text-center" width="7%">Pen. Jawab</th>
                                    <th class="text-center" width="15%">Realisasi</th>
                                    <th class="text-center" width="7%">Pen. Jawab</th>
                                    <th class="text-center" width="15%">Rencana yang akan datang</th>
                                    <th class="text-center" width="7%">Pen. Jawab</th>
                                    <th class="text-center" width="4%">#</th>
                                </tr>
                            </thead>
                            <tbody class="show-risalah">
                                @if ($record->risalah->count() > 0)
                                    @foreach ($record->risalah as $key => $item)
                                        <tr>
                                            <td class="form-row">
                                                <div class="form-group col-xs-12 field">
                                                    <input type="hidden" name="key_risalah[]" value="{{$key}}">
                                                    <textarea name="risalah[]" class="form-control" placeholder="Risalah">{{$item->risalah}}</textarea>
                                                </div>
                                            </td>
                                            <td class="form-row">
                                                <div class="form-group col-xs-12 field">
                                                    <input type="text" name="pen_1[]" value="{{$item->pen_1}}" class="form-control" placeholder="Pen. Jawab">
                                                </div>
                                            </td>
                                            <td class="form-row">
                                                <div class="form-group col-xs-12 field">
                                                    <textarea name="realisasi[]" class="form-control" placeholder="Realisasi">{{$item->realisasi}}</textarea>
                                                </div>
                                            </td>
                                            <td class="form-row">
                                                <div class="form-group col-xs-12 field">
                                                    <input type="text" name="pen_2[]" value="{{$item->pen_2}}" class="form-control" placeholder="Pen. Jawab">
                                                </div>
                                            </td>
                                            <td class="form-row">
                                                <div class="form-group col-xs-12 field">
                                                    <textarea name="rencana[]" class="form-control" placeholder="Rencana yang akan datang">{{$item->rencana}}</textarea>
                                                </div>
                                            </td>
                                            <td class="form-row">
                                                <div class="form-group col-xs-12 field">
                                                    <input type="text" name="pen_3[]" value="{{$item->pen_3}}" class="form-control" placeholder="Pen. Jawab">
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <a data-content="Ubah" class="add-line-risalah" data-id="1" class="edit button" data-toggle="tooltip" title="" data-original-title="Ubah" style="padding-right: 10px;">
                                                    <i class="fa fa-plus text-info"></i>
                                                </a>
                                                @if ($key != 0)
                                                    <a data-content="Detil" class="remove-line-risalah" data-id="1" class="m-l detil button" data-toggle="tooltip" title="" data-original-title="Detil">
                                                        <i class="fa fa-trash text-danger"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td class="form-row">
                                            <div class="form-group col-xs-12 field">
                                                <input type="hidden" name="key_risalah[]" value="0">
                                                <textarea name="risalah[]" class="form-control" placeholder="Risalah"></textarea>
                                            </div>
                                        </td>
                                        <td class="form-row">
                                            <div class="form-group col-xs-12 field">
                                                <input type="text" name="pen_1[]" class="form-control" placeholder="Pen. Jawab">
                                            </div>
                                        </td>
                                        <td class="form-row">
                                            <div class="form-group col-xs-12 field">
                                                <textarea name="realisasi[]" class="form-control" placeholder="Realisasi"></textarea>
                                            </div>
                                        </td>
                                        <td class="form-row">
                                            <div class="form-group col-xs-12 field">
                                                <input type="text" name="pen_2[]" class="form-control" placeholder="Pen. Jawab">
                                            </div>
                                        </td>
                                        <td class="form-row">
                                            <div class="form-group col-xs-12 field">
                                                <textarea name="rencana[]" class="form-control" placeholder="Rencana yang akan datang"></textarea>
                                            </div>
                                        </td>
                                        <td class="form-row">
                                            <div class="form-group col-xs-12 field">
                                                <input type="text" name="pen_3[]" class="form-control" placeholder="Pen. Jawab">
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <a data-content="Ubah" class="add-line-risalah" data-id="1" class="edit button" data-toggle="tooltip" title="" data-original-title="Ubah">
                                                <i class="fa fa-plus text-info"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-xs-12">
                            <br>
                            <div class="text-right">
                                <button type="button" class="btn btn-cancel previous-tab">Kembali</button>
                                <button type="button" class="btn btn-simpan save as page">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script> 
    	$(document).ready(function(){
    		// reloadMask();
            $('.tanggal').datepicker({
                format: 'mm/dd/yyyy',
                startDate: '-0d',
                orientation: "auto",
                autoclose:true,
            });
            $('.waktu').clockpicker({
                autoclose:true,
                'default': 'now',
            });
            $('#myTabs a').click(function (e) {
				e.preventDefault()
				$(this).tab('show')
			})
            var filerihadir = '{{ $record->fileHadir() }}';
            $("#hadir").fileinput({
                autoReplace: true,
                overwriteInitial: true,
                maxFileCount: 1,
                initialPreview: [filerihadir],
                initialPreviewAsData: true,
                initialPreviewConfig: [
                    {type: "{{ $record->files()->where('flag','hadir')->pluck('type')->first() }}",caption: "{{ $record->files()->where('flag','hadir')->pluck('filename')->first() }}", downloadUrl: filerihadir, size: 930321, width: "120px", key: 1},
                ],
                initialCaption: "{{ $record->files()->where('flag','hadir')->pluck('filename')->first() }}",
                initialPreviewShowDelete: false,
                showRemove: false,
                showClose: false,
                layoutTemplates: {actionDelete: ''}, // disable thumbnail deletion
                allowedFileExtensions: ["{{ $record->files()->where('flag','hadir')->pluck('type')->first() }}"]
            });
                var obj = '{{ $record->fileOtherJson() }}';
                var fileUri = '{{ $record->fileOther() }}';
            $('#other').fileinput({
                autoReplace: true,
                overwriteInitial: true,
                // maxFileCount: 1,
                initialPreview: JSON.parse(fileUri.replace(/&quot;/g,'"')),
                initialPreviewAsData: true,
                initialPreviewConfig: JSON.parse(obj.replace(/&quot;/g,'"')),
                // initialCaption: "{{ $record->files()->where('flag','hadir')->pluck('filename')->first() }}",
                initialPreviewShowDelete: false,
                showRemove: false,
                showClose: false,
                layoutTemplates: {actionDelete: ''}, // disable thumbnail deletion
            });
        });
        $(document).on('click','.next-tab', function(){
			var next = $('.nav-tabs > .active').next('li');
			if(next.length){
				next.find('a').trigger('click');
			}else{
				$('#myTabs a:first').tab('show');
			}
		});
		$(document).on('click','.previous-tab', function(){
			var prev = $('.nav-tabs > .active').prev('li')
			if(prev.length){
				prev.find('a').trigger('click');
			}else{
				$('#myTabs a:last').tab('show');
			}
		});
		
		$(document).on('click','.add-line', function(){
			var i = $('.add-line').length;
			var htm = `<tr>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<input type="hidden" name="key_daftar[]" value="`+i+`">
									<input type="text" name="nama_peserta[]" class="form-control" placeholder="Nama">
								</div>
							</td>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<input type="text" name="jabatan[]" class="form-control" placeholder="Jabatan">
								</div>
							</td>
							<td class=" text-center">
								<a data-content="Ubah" class="add-line" data-id="1" class="edit button" data-toggle="tooltip" title="" data-original-title="Ubah" style="padding-right: 10px;">
									<i class="fa fa-plus text-info"></i>
								</a>
								<a data-content="Detil" class="remove-line" data-id="1" class="m-l detil button" data-toggle="tooltip" title="" data-original-title="Detil">
									<i class="fa fa-trash text-danger"></i>
								</a>
							</td>
						</tr>`;
			$('.show-daftar').append(htm);
		});
		$(document).on('click','.remove-line', function(){
			$(this).parent().parent().remove();
		});
		$(document).on('click','.add-line-risalah', function(){
			var i = $('.add-line-risalah').length;
			var htm = `<tr>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<input type="hidden" name="key_risalah[]" value="`+i+`">
									<textarea name="risalah[]" class="form-control" placeholder="Risalah"></textarea>
								</div>
							</td>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<input type="text" name="pen_1[]" class="form-control" placeholder="Pen. Jawab">
								</div>
							</td>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<textarea name="realisasi[]" class="form-control" placeholder="Realisasi"></textarea>
								</div>
							</td>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<input type="text" name="pen_2[]" class="form-control" placeholder="Pen. Jawab">
								</div>
							</td>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<textarea name="rencana[]" class="form-control" placeholder="Rencana yang akan datang"></textarea>
								</div>
							</td>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<input type="text" name="pen_3[]" class="form-control" placeholder="Pen. Jawab">
								</div>
							</td>
							<td class=" text-center">
								<a data-content="Ubah" class="add-line" data-id="1" class="edit button" data-toggle="tooltip" title="" data-original-title="Ubah" style="padding-right: 10px;">
									<i class="fa fa-plus text-info"></i>
								</a>
								<a data-content="Detil" class="remove-line-risalah" data-id="1" class="m-l detil button" data-toggle="tooltip" title="" data-original-title="Detil">
									<i class="fa fa-trash text-danger"></i>
								</a>
							</td>
						</tr>`;
			$('.show-risalah').append(htm);
		});
		$(document).on('click','.remove-line-risalah', function(){
			$(this).parent().parent().remove();
		});
    </script>
    @yield('js-extra')
@endpush

