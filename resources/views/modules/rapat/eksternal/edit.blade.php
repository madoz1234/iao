@extends('layouts.form')
@section('title', 'Ubah Data '.$title)
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData" autocomplete="off">
@method('PATCH')
@csrf
<div class="panel panel-default">
    <div class="panel-body" style="padding-bottom: 0px;">
        <input type="hidden" name="id" value="{{$record->id}}">
        <input type="hidden" name="flag">
        <div class="form-row">
            {{-- kiri --}}
            <div class="form-group col-xs-6" style="padding-right: 14px;">
                <div class="form-group col-xs-12 field" style="padding-right: 14px;">
                    <label for="inputEmail4">Nomor Rapat</label>
                    <input type="text" name="nomor" class="form-control" placeholder="Nomor Rapat" value="{{$record->nomor}}">
                    <input type="hidden" class="form-control" name="jumlah_peserta" value="{{$record->daftarHadir->count()}}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" placeholder="Jumlah Peserta">
                </div>
                <div class="form-group col-xs-12 field" style="padding-right: 14px;">
                    <label for="inputEmail4">Tempat</label>
                    <input type="text" name="tempat" class="form-control" placeholder="Tempat" value="{{$record->tempat}}">
                </div>
                <div class="col-xs-12" style="padding-right: 14px;">
                    <div class="form-row">
                        <div class="form-group col-xs-6 field" style="padding-right: 14px;">
                            <label for="inputEmail4">Hari</label>
                            <input type="text" name="hari" class="form-control hari" placeholder="Hari">
                        </div>
                        <div class="form-group col-xs-6 field" style="padding-left: 14px;">
                            <label for="inputEmail4">Tanggal</label>
                            <div class="input-group" data-placement="left" data-align="top" data-autoclose="true">
                                <input type="text" class="form-control tgl_pelaksanaan" name="tanggal" placeholder="Tanggal Pelaksanaan" value="{{ Carbon::parse($record->tanggal)->format('d-m-Y') }}">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12" style="padding-right: 14px;">
                    <div class="form-row">
                        <div class="form-group col-xs-6 field" style="padding-right: 14px;">
                            <label for="inputEmail4">Jam</label>
                            <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                <input type="text" class="form-control" name="jam_mulai" placeholder="Mulai" value="{{$record->jam_mulai}}">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-xs-6 field" style="padding-left: 14px;">
                            <label for="inputEmail4">&nbsp</label>
                            <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                <input type="text" class="form-control" name="jam_selesai" placeholder="Selesai" value="{{$record->jam_selesai}}">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- kanan --}}
            <div class="form-group col-xs-6" style="padding-left: 14px;">
                <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                    <label for="inputEmail4">Kategori</label>
                    {{-- <input type="text" class="form-control" value="Umum Konsultasi" disabled=""> --}}
                    @if ($record->status_kategori == 0)
                        @if ($record->kategori == 'Kegiatan Konsultasi')
                            <input type="text" class="form-control" value="Kegiatan Konsultasi" disabled="">
                        @elseif($record->kategori == 'Kegiatan Lainnya')
                            <input type="text" class="form-control" value="Kegiatan Lainnya" disabled="">
                        @else
                            <input type="text" class="form-control" value="-" disabled="">
                        @endif
                    @else
                        <select class="selectpicker form-control project" 
                            name="status_kategori"
                            data-style="btn-default" 
                            data-live-search="true" 
                            title="(Kategori)"
                            >
                            <option value="1" @if($record->status_kategori == 1) selected @endif>Umum</option>
                            <option value="2" @if($record->status_kategori == 2) selected @endif>Konsultasi</option>
                        </select>
                    @endif
                </div>
                <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                    <table id="example-materi" style="width: 100%;font-size: 12px;">
                        <tbody class="container-materi">
                            {{-- {{ dd($record->agendaInternal) }} --}}
                            @foreach($record->agendaInternal as $key => $data)
                                <tr class="data-container-{{$key+1}}" data-id="{{$key+1}}">
                                    @if($key == 0)
                                    <input type="hidden" name="data[{{ $key }}][id]" value="{{ $data->id }}">
                                    <input type="hidden" name="exists[]" value="{{$data->id}}">
                                    <td scope="row">
                                        <div class="field">
                                            <label for="inputEmail4">Agenda</label>
                                            <input type="text" name="data[{{$key}}][agenda]" data-id="{{$key}}" class="form-control" placeholder="Agenda" value="{{ $data->agenda }}">
                                        </div>
                                    </td>
                                    <td style="text-align: center; padding-top: 15px;">
                                        <button class="btn btn-sm btn-success tambah_materi" type="button"><i class="fa fa-plus"></i></button>
                                    </td>
                                    @else
                                    <input type="hidden" name="data[{{ $key }}][id]" value="{{ $data->id }}">
                                    <input type="hidden" name="exists[]" value="{{$data->id}}">
                                    <td scope="row">
                                        <div class="field">
                                            <label for="inputEmail4">&nbsp;</label>
                                            <input type="text" name="data[{{$key}}][agenda]" data-id="{{$key}}" class="form-control" placeholder="Agenda" value="{{ $data->agenda }}">
                                        </div>
                                    </td>
                                    <td style="text-align: center; padding-top: 15px;">
                                        <button class="btn btn-sm btn-danger hapus_materi" type="button" data-id="{{$key+1}}"><i class="fa fa-remove"></i></button>
                                    </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                    <label for="inputEmail4">Keterangan Rapat</label>
                    <textarea name="keterangan" rows="3" class="form-control" placeholder="Keterangan Rapat">{{$record->keterangan}}</textarea>
                </div>
            </div>
        </div>
        {{-- <div class="form-row">
            <div class="form-group col-md-6 field" style="padding-right: 14px;">
                <label for="inputEmail4">Nomor Rapat</label>
                <input type="text" name="nomor" value="{{$record->nomor}}" class="form-control" placeholder="Nomor Rapat">
            </div>
            <div class="form-group col-md-6 field" style="padding-left: 14px;">
                <label for="inputEmail4">Tema / Materi Rapat</label>
                <input type="text" name="materi" value="{{$record->materi}}" class="form-control" placeholder="Tema / Materi Rapat">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6 field" style="padding-right: 14px;">
                <label for="inputEmail4">Tanggal Pelaksanaan</label>
                <input type="text" name="tanggal" value="{{ Carbon::createFromFormat('m/d/Y',$record->tanggal)->format('d/m/Y') }}" class="form-control tanggal" placeholder="Tanggal Pelaksanaan">
            </div>
            <div class="form-group col-md-6 field waktu" style="padding-left: 14px;">
                <label for="inputEmail4">Pukul</label>
                <input type="text" name="waktu" value="{{$record->waktu}}" class="form-control" placeholder="Pukul">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12 field">
                <label for="inputEmail4">Lokasi Rapat</label>
                <input type="text" name="tempat" value="{{$record->tempat}}" class="form-control" placeholder="Lokasi Rapat">
            </div>
            <input type="hidden" class="form-control" name="jumlah_peserta" value="{{$record->daftarHadir->count()}}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" placeholder="Jumlah Peserta">
        </div>
        <div class="form-row">
            <div class="form-group col-md-12 field">
                <label for="inputEmail4">Keterangan Rapat</label>
                <textarea name="keterangan" rows="7" class="form-control" placeholder="Keterangan Rapat">{{$record->keterangan}}</textarea>
            </div>
        </div> --}}

		{{-- daftar peserta --}}
        <div class="col-sm-12">
    		<h4 class="m-t-lg m-b">Peserta Undangan</h4>
            <table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
                <thead style="background-color: #f9fafb;">
                    <tr>
                        <th scope="col" style="text-align: center; width: 5%;">#</th>
                        <th scope="col" style="text-align: center; width: 45%;">Email</th>
                        <th scope="col" style="text-align: center; width: 45%;">Perusahaan</th>
                        <th scope="col" style="text-align: center; width: 5%;">Aksi</th>
                    </tr>
                </thead>
                <tbody class="container">
                    @foreach($record->undanganEksternal as $key => $data)
                    <tr class="data-container-{{$key+1}}" data-id="{{$key+1}}">
                        <input type="hidden" name="detail[{{$key}}][id]" value="{{ $data->id }}">
                        <input type="hidden" name="exists[]" value="{{$data->id}}">
                        <td scope="row" style="text-align: center;">
                            <label style="margin-top: 23px;" class="numboor-{{$key+1}}">{{ $key+1 }}</label>
                        </td>
                        <td scope="row">
    						<input type="text" name="detail[{{$key}}][email]" class="form-control" placeholder="Email" value="{{$data->email}}">
                        </td>
                        <td scope="row">
    						<input type="text" name="detail[{{$key}}][perusahaan]" class="form-control" placeholder="Perusahaan" value="{{$data->perusahaan}}">
                        </td>
                        <td style="text-align: center;">
                            {{-- <button class="btn btn-sm btn-success tambah_peserta" type="button"><i class="fa fa-plus"></i></button> --}}
                            @if($key == 0)
                                <button class="btn btn-sm btn-success tambah_peserta" type="button" style="border-radius: 20px;margin-top: 19px;"><i class="fa fa-plus"></i></button>
                            @else
                                <button class="btn btn-sm btn-danger hapus_peserta" type="button" style="border-radius:20px;margin-top: 19px;" data-id="{{$key+1}}"><i class="fa fa-remove"></i></button>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="form-row">
            <div class="form-group col-xs-12">
                <br>
                <div class="text-right">
                    <button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
                    <button type="button" class="btn btn-primary save-draft page flag">Save As Draft</button>
                    <button type="button" class="btn btn-simpan save as page flag">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
@endsection
@push('styles')
    <link rel="stylesheet" href="{{ asset('libs/jquery/jquery-ui-1.12.1/jquery-ui.min.css') }}" type="text/css" />
@endpush

@push('js')
    <script src="{{ asset('libs/jquery/jquery-ui-1.12.1/jquery-ui.js') }}"></script>
    <script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
    <script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script> 
    	$(document).ready(function(){
    		// reloadMask();
            // tanggal
            $( ".tgl_pelaksanaan" ).datepicker({dateFormat: 'dd-mm-yy'});
            local = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum`at', 'Sabtu' ];

            $('.tgl_pelaksanaan').datepicker()
            .on("change", function () {    
                var today = new Date($('.tgl_pelaksanaan').datepicker('getDate'));      
                //alert(local[today.getDay()]);
                $('.hari').val(local[today.getDay()]);
            });
                $('.hari').val('{{ App\Libraries\CoreSn::Day($record->tanggal) }}');
            // waktu
            $('.clockpicker').clockpicker(); 
            $('.tanggal').datepicker({
                format: 'dd-mm-yy',
                startDate: '-0d',
                orientation: "auto",
                autoclose:true,
            });
            $('.waktu').clockpicker({
                autoclose:true,
                'default': 'now',
            });
            $('#myTabs a').click(function (e) {
				e.preventDefault()
				$(this).tab('show')
			})
            var filerihadir = '{{ $record->fileHadir() }}';
            $("#hadir").fileinput({
                autoReplace: true,
                overwriteInitial: true,
                maxFileCount: 1,
                initialPreview: [filerihadir],
                initialPreviewAsData: true,
                initialPreviewConfig: [
                    {type: "{{ $record->files()->where('flag','hadir')->pluck('type')->first() }}",caption: "{{ $record->files()->where('flag','hadir')->pluck('filename')->first() }}", downloadUrl: filerihadir, size: 930321, width: "120px", key: 1},
                ],
                initialCaption: "{{ $record->files()->where('flag','hadir')->pluck('filename')->first() }}",
                initialPreviewShowDelete: false,
                showRemove: false,
                showClose: false,
                layoutTemplates: {actionDelete: ''}, // disable thumbnail deletion
                allowedFileExtensions: ["{{ $record->files()->where('flag','hadir')->pluck('type')->first() }}"]
            });
                var obj = '{{ $record->fileOtherJson() }}';
                var fileUri = '{{ $record->fileOther() }}';
            $('#other').fileinput({
                autoReplace: true,
                overwriteInitial: true,
                // maxFileCount: 1,
                initialPreview: JSON.parse(fileUri.replace(/&quot;/g,'"')),
                initialPreviewAsData: true,
                initialPreviewConfig: JSON.parse(obj.replace(/&quot;/g,'"')),
                // initialCaption: "{{ $record->files()->where('flag','hadir')->pluck('filename')->first() }}",
                initialPreviewShowDelete: false,
                showRemove: false,
                showClose: false,
                layoutTemplates: {actionDelete: ''}, // disable thumbnail deletion
            });
        });
        $(document).on('click','.next-tab', function(){
			var next = $('.nav-tabs > .active').next('li');
			if(next.length){
				next.find('a').trigger('click');
			}else{
				$('#myTabs a:first').tab('show');
			}
		});
		$(document).on('click','.previous-tab', function(){
			var prev = $('.nav-tabs > .active').prev('li')
			if(prev.length){
				prev.find('a').trigger('click');
			}else{
				$('#myTabs a:last').tab('show');
			}
		});
		
		$(document).on('click','.add-line', function(){
			var i = $('.add-line').length;
			var htm = `<tr>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<input type="hidden" name="key_daftar[]" value="`+i+`">
									<input type="text" name="nama_peserta[]" class="form-control" placeholder="Nama">
								</div>
							</td>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<input type="text" name="jabatan[]" class="form-control" placeholder="Jabatan">
								</div>
							</td>
							<td class=" text-center">
								<a data-content="Ubah" class="add-line" data-id="1" class="edit button" data-toggle="tooltip" title="" data-original-title="Ubah" style="padding-right: 10px;">
									<i class="fa fa-plus text-info"></i>
								</a>
								<a data-content="Detil" class="remove-line" data-id="1" class="m-l detil button" data-toggle="tooltip" title="" data-original-title="Detil">
									<i class="fa fa-trash text-danger"></i>
								</a>
							</td>
						</tr>`;
			$('.show-daftar').append(htm);
		});
		$(document).on('click','.remove-line', function(){
			$(this).parent().parent().remove();
		});
		$(document).on('click','.add-line-risalah', function(){
			var i = $('.add-line-risalah').length;
			var htm = `<tr>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<input type="hidden" name="key_risalah[]" value="`+i+`">
									<textarea name="risalah[]" class="form-control" placeholder="Risalah"></textarea>
								</div>
							</td>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<input type="text" name="pen_1[]" class="form-control" placeholder="Pen. Jawab">
								</div>
							</td>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<textarea name="realisasi[]" class="form-control" placeholder="Realisasi"></textarea>
								</div>
							</td>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<input type="text" name="pen_2[]" class="form-control" placeholder="Pen. Jawab">
								</div>
							</td>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<textarea name="rencana[]" class="form-control" placeholder="Rencana yang akan datang"></textarea>
								</div>
							</td>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<input type="text" name="pen_3[]" class="form-control" placeholder="Pen. Jawab">
								</div>
							</td>
							<td class=" text-center">
								<a data-content="Ubah" class="add-line" data-id="1" class="edit button" data-toggle="tooltip" title="" data-original-title="Ubah" style="padding-right: 10px;">
									<i class="fa fa-plus text-info"></i>
								</a>
								<a data-content="Detil" class="remove-line-risalah" data-id="1" class="m-l detil button" data-toggle="tooltip" title="" data-original-title="Detil">
									<i class="fa fa-trash text-danger"></i>
								</a>
							</td>
						</tr>`;
			$('.show-risalah').append(htm);
		});
		$(document).on('click','.remove-line-risalah', function(){
			$(this).parent().parent().remove();
		});

		// peserta
        $(document).on('click', '.tambah_peserta', function(e){
            var rowCount = $('#example > tbody > tr').length;
            var c = rowCount-1;
            var html = `
                    <tr class="data-container-`+(c+2)+`" data-id="`+(c+2)+`">
                            <td scope="row" style="text-align: center;">
                                <label class="numboor-`+(c+2)+`">`+(c+2)+`</label>
                            </td>
                            <td scope="row">
								<input type="text" name="detail[`+(c+2)+`][email]" class="form-control" placeholder="Email">
		                    </td>
		                    <td scope="row">
								<input type="text" name="detail[`+(c+2)+`][perusahaan]" class="form-control" placeholder="Perusahaan">
		                    </td>
                            <td style="text-align: center;">
                                <button class="btn btn-sm btn-danger hapus_peserta" type="button" data-id=`+(c+2)+`><i class="fa fa-remove"></i></button>
                            </td>
                        </tr>
                `;

                $('.container').append(html);
        });

        $(document).on('click', '.hapus_peserta', function (e){
            var row = $(this).closest('tr');
            row.remove();
            var table = $('#example');
            var rows = table.find('tbody tr');

            $.each(rows, function(key, value){
                console.log($(this).data("id"))
                table.find('.numboor-'+$(this).data("id")).html(key+1);
            });
        });

        //agenda
        $(document).on('click', '.tambah_materi', function(e){
            var rowCount = $('#example-materi > tbody > tr').length;
            var c = rowCount-1;
            var html = `
                    <tr class="data-container-materi-`+(c+2)+`" data-id="`+(c+2)+`">
                        <td scope="row">
                            <div class="field">
                                <label for="inputEmail4">&nbsp;</label>
                                <input type="text" name="data[`+(c+2)+`][agenda]" class="form-control" placeholder="Agenda">
                            </div>
                        </td>
                        <td style="text-align: center;">
                            <button class="btn btn-sm btn-danger hapus_materi" type="button" data-id=`+(c+2)+`><i class="fa fa-remove"></i></button>
                        </td>
                    </tr>
                `;

                $('.container-materi').append(html);
        });

        $(document).on('click', '.hapus_materi', function (e){
            var row = $(this).closest('tr');
            row.remove();
            var table = $('#example-materi');
            var rows = table.find('tbody tr');

            $.each(rows, function(key, value){
                console.log($(this).data("id"))
                table.find('.numboor-'+$(this).data("id")).html(key+1);
            });
        });
    </script>
    @yield('js-extra')
@endpush

