@extends('layouts.form')
@section('title', 'Lengkapi Data Rapat')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
    <div class="panel-body" style="padding-bottom: 0px;">
        <div class="form-row">
            {{-- kiri --}}
            <div class="form-group col-xs-6" style="padding-right: 14px;">
                <div class="form-group col-xs-12 field" style="padding-right: 14px;">
                    <label for="inputEmail4">Nomor Rapat</label>
                    <input type="text" name="nomor" class="form-control" placeholder="Nomor Rapat" value="{{$record->nomor}}" disabled="">
                    <input type="hidden" class="form-control" name="jumlah_peserta" value="{{$record->daftarHadir->count()}}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" placeholder="Jumlah Peserta">
                </div>
                <div class="form-group col-xs-12 field" style="padding-right: 14px;">
                    <label for="inputEmail4">Tempat</label>
                    <input type="text" name="tempat" class="form-control" placeholder="Tempat" value="{{$record->tempat}}" disabled="">
                </div>
                <div class="col-xs-12" style="padding-right: 14px;">
                    <div class="form-row">
                        <div class="form-group col-xs-6 field" style="padding-right: 14px;">
                            <label for="inputEmail4">Hari</label>
                            <input type="text" name="hari" class="form-control" placeholder="Hari" disabled="" value={{ App\Libraries\CoreSn::Day($record->tanggal) }}>
                        </div>
                        <div class="form-group col-xs-6 field" style="padding-left: 14px;">
                            <label for="inputEmail4">Tanggal</label>
                            <div class="input-group" data-placement="left" data-align="top" data-autoclose="true">
                                <input type="text" class="form-control tanggal" name="tanggal" placeholder="Tanggal Pelaksanaan" value="{{ Carbon::parse($record->tanggal)->format('d-m-Y') }}" disabled="">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12" style="padding-right: 14px;">
                    <div class="form-row">
                        <div class="form-group col-xs-6 field" style="padding-right: 14px;">
                            <label for="inputEmail4">Jam</label>
                            <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                <input type="text" class="form-control" name="jam_mulai" placeholder="Mulai" value="{{$record->jam_mulai}}" disabled="">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-xs-6 field" style="padding-left: 14px;">
                            <label for="inputEmail4">&nbsp</label>
                            <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                <input type="text" class="form-control" name="jam_selesai" placeholder="Selesai" value="{{$record->jam_selesai}}" disabled="">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- kanan --}}
            <div class="form-group col-xs-6" style="padding-left: 14px;">
                <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                    <label for="inputEmail4">Kategori</label>
                    {{-- <input type="text" class="form-control" value="Umum Konsultasi" disabled=""> --}}
                    @if ($record->status_kategori == 0)
                        @if ($record->kategori == 'Kegiatan Konsultasi')
                            <input type="text" class="form-control" value="Kegiatan Konsultasi" disabled="">
                        @elseif($record->kategori == 'Kegiatan Lainnya')
                            <input type="text" class="form-control" value="Kegiatan Lainnya" disabled="">
                        @else
                            <input type="text" class="form-control" value="-" disabled="">
                        @endif
                    @else
                        @if ($record->status_kategori == 1)
                            <input type="text" class="form-control" value="Umum" disabled="">
                        @else
                            <input type="text" class="form-control" value="Konsultasi" disabled="">
                        @endif
                    @endif
                </div>
                <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                    <table id="example-materi" style="width: 100%;font-size: 12px;">
                        <tbody class="container-materi">
                            {{-- {{ dd($record->agendaInternal) }} --}}
                            @foreach($record->agendaInternal as $key => $data)
                                <tr class="data-container-{{$key+1}}" data-id="{{$key+1}}">
                                    @if($key == 0)
                                    <input type="hidden" name="data[{{ $key }}][id]" value="{{ $data->id }}">
                                    <input type="hidden" name="exists[]" value="{{$data->id}}">
                                    <td scope="row">
                                        <div class="field">
                                            <label for="inputEmail4">Agenda</label>
                                            <input type="text" name="data[{{$key}}][agenda]" data-id="{{$key}}" class="form-control" placeholder="Agenda" value="{{ $data->agenda }}" disabled="">
                                        </div>
                                    </td>
                                    @else
                                    <input type="hidden" name="data[{{ $key }}][id]" value="{{ $data->id }}">
                                    <input type="hidden" name="exists[]" value="{{$data->id}}">
                                    <td scope="row">
                                        <div class="field">
                                            <label for="inputEmail4">&nbsp;</label>
                                            <input type="text" name="data[{{$key}}][agenda]" data-id="{{$key}}" class="form-control" placeholder="Agenda" value="{{ $data->agenda }}" disabled="">
                                        </div>
                                    </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                    <label for="inputEmail4">Keterangan Rapat</label>
                    <textarea name="keterangan" rows="3" class="form-control" placeholder="Keterangan Rapat" disabled="">{{$record->keterangan}}</textarea>
                </div>
            </div>
        </div>
    </div>
</div>
<ul id="myTabs" class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Dokumentasi</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Daftar Hadir</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Risalah Rapat</a></li>
</ul>
<form action="{{ route($routes.'.re-upload', $record->id) }}" method="POST" id="formData">
    @method('PATCH')
    @csrf
    <input type="hidden" name="id" value="{{ $record->id }}">
    <input type="hidden" name="status_rapat">
    <div id="myTabContent" class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">
            <div class="panel panel-default">
                <div class="panel-body" style="padding-bottom: 0px;">
                    <div class="form-row">
                        <div class="form-group col-md-12 field">
                            <label for="hadir" class="control-label">Daftar Hadir</label>
                            <div class="file-loading">
                                <input id="hadir" name="daftar_hadir[]" multiple type="file" class="form-control" 
                                data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf">
                            </div>
                        </div>
                        <div class="form-group col-md-12 field">
                            <label for="other" class="control-label">Surat Undangan & Dokumentasi</label>
                            <div class="file-loading">
                                <input id="other" name="other[]" multiple type="file" class="form-control" 
                                data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf,image/x-png,image/gif,image/jpeg">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-xs-12">
                            <br>
                            <div class="text-right">
                                <button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
                                <button type="button" class="btn btn-primary next-tab">Selanjutnya</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="profile">
            <div class="panel panel-default">
                <div class="panel-body" style="padding-bottom: 0px;">
                    <div class="form-row">
                        <table class="table" style="font-size: 12px;">
                            <thead>
                                @if ($record->status == 0)
                                    <tr>
                                        <th class="text-center">Nama</th>
                                        <th class="text-center">Email</th>
                                        <th class="text-center">Nomor Handphone</th>
                                        <th class="text-center">Jabatan</th>
                                    </tr>
                                @else
                                    <tr>
                                        <th class="text-center">Nama</th>
                                        <th class="text-center">Email</th>
                                        <th class="text-center">Nomor Handphone</th>
                                        <th class="text-center">Perusahaan</th>
                                        <th class="text-center" width="5%">#</th>
                                    </tr>
                                @endif
                            </thead>
                            <tbody class="show-daftar">
                                @if ($record->daftarHadir->count() > 0)
                                    @if ($record->status == 0)
                                        @foreach ($record->daftarHadir as $key => $item)
                                        <tr>
                                        @if (isset($item->user_id))
                                                <td class="form-row">
                                                    {{$item->user->name}}
                                                </td>
                                                <td class="form-row">
                                                    {{$item->user->email}}
                                                </td>
                                                <td class="form-row">
                                                    {{$item->user->phone}}
                                                </td>
                                                <td class="form-row">
                                                    {{$item->user->roles->first()->name}}
                                                </td>
                                            @endif
                                        </tr>
                                        @endforeach
                                    @else
                                        @foreach ($record->daftarHadir as $key => $item)
                                            <tr>
                                                <td class="form-row">
                                                    <div class="form-group col-xs-12 field">
                                                        <input type="hidden" name="key_daftar[]" value="{{$key}}">
                                                        <input type="text" name="nama_peserta[]" class="form-control" value="{{$item->nama}}" placeholder="Nama">
                                                    </div>
                                                </td>
                                                <td class="form-row">
                                                    <div class="form-group col-xs-12 field">
                                                        <input type="text" name="email[]" class="form-control" value="{{$item->email}}" placeholder="Email">
                                                    </div>
                                                </td>
                                                <td class="form-row">
                                                    <div class="form-group col-xs-12 field">
                                                        <input type="text" name="nomor_hp[]" class="form-control" value="{{$item->nomor}}" placeholder="Nomor Handphone">
                                                    </div>
                                                </td>
                                                <td class="form-row">
                                                    <div class="form-group col-xs-12 field">
                                                        <input type="text" name="perusahaan[]" class="form-control" value="{{$item->perusahaan}}" placeholder="Perusahaan">
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <a data-content="Ubah" class="add-line" data-id="1" class="edit button" data-toggle="tooltip" title="" data-original-title="Ubah" style="padding-right: 10px;">
                                                        <i class="fa fa-plus text-info"></i>
                                                    </a>
                                                    @if ($key != 0)
                                                        <a data-content="Detil" class="remove-line" data-id="1" class="m-l detil button" data-toggle="tooltip" title="" data-original-title="Detil">
                                                            <i class="fa fa-trash text-danger"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @else
                                    @if ($record->status == 0)
                                        <td colspan="4" class="text-center">
                                            Data tidak ditemukan
                                        </td>
                                    @else
                                        <tr>
                                            <td class="form-row">
                                                <div class="form-group col-xs-12 field">
                                                    <input type="hidden" name="key_daftar[]" value="0">
                                                    <input type="text" name="nama_peserta[]" class="form-control" placeholder="Nama">
                                                </div>
                                            </td>
                                            <td class="form-row">
                                                <div class="form-group col-xs-12 field">
                                                    <input type="text" name="email[]" class="form-control" placeholder="Email">
                                                </div>
                                            </td>
                                            <td class="form-row">
                                                <div class="form-group col-xs-12 field">
                                                    <input type="text" name="nomor_hp[]" class="form-control" placeholder="Nomor Handphone">
                                                </div>
                                            </td>
                                            <td class="form-row">
                                                <div class="form-group col-xs-12 field">
                                                    <input type="text" name="perusahaan[]" class="form-control" placeholder="Perusahaan">
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <a data-content="Ubah" class="add-line" data-id="1" class="edit button" data-toggle="tooltip" title="" data-original-title="Ubah">
                                                    <i class="fa fa-plus text-info"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endif
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-xs-12">
                            <br>
                            <div class="text-right">
                                <button type="button" class="btn btn-cancel previous-tab">Kembali</button>
                                <button type="button" class="btn btn-primary next-tab">Selanjutnya</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="messages">
            <div class="panel panel-default">
                <div class="panel-body" style="padding-bottom: 0px;">
                    <div class="form-row">
                        <table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
                            <thead style="background-color: #f9fafb;">
                                <tr>
                                    <th scope="col" style="text-align: center; width: 5%;">#</th>
                                    <th scope="col" style="text-align: center; width: 15%;">Risalah</th>
                                    <th scope="col" style="text-align: center; width: 15%;">Pen. Jawab</th>
                                    <th scope="col" style="text-align: center; width: 15%;">Realisasi</th>
                                    <th scope="col" style="text-align: center; width: 15%;">Pen. Jawab</th>
                                    <th scope="col" style="text-align: center; width: 15%;">Rencana yang akan datang</th>
                                    <th scope="col" style="text-align: center; width: 15%;">Pen. Jawab</th>
                                    <th scope="col" style="text-align: center; width: 5%;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="container">
                                @if (count($record->risalah) > 0)
                                @foreach($record->risalah as $key => $data)
                                <tr class="data-container-{{$key+1}}" data-id="{{$key+1}}">
                                    <input type="hidden" name="detail[{{$key}}][id]" value="{{ $data->id }}">
                                    <input type="hidden" name="exists[]" value="{{$data->id}}">
                                    <td scope="row" style="text-align: center;">
                                        <label class="numboor-{{$key+1}}">{{ $key+1 }}</label>
                                    </td>
                                    <td scope="row">
                                        <textarea class="form-control" name="detail[{{$key}}][risalah]" id="exampleFormControlTextarea1" placeholder="Risalah" rows="2">{{$data->risalah}}</textarea>
                                    </td>
                                    <td scope="row">
                                        <div class="field">
                                            <input type="text" name="detail[{{$key}}][pen_1]" value="{{$data->pen_1}}" class="form-control" placeholder="Penanggung Jawab">
                                        </div>
                                    </td>
                                    <td scope="row">
                                        <textarea class="form-control" name="detail[{{$key}}][realisasi]" id="exampleFormControlTextarea1" placeholder="Realisasi" rows="2">{{$data->realisasi}}</textarea>
                                    </td>
                                    <td scope="row">
                                        <div class="field">
                                            <input type="text" name="detail[{{$key}}][pen_2]" value="{{$data->pen_2}}" class="form-control" placeholder="Penanggung Jawab">
                                        </div>
                                    </td>
                                    <td scope="row">
                                        <textarea class="form-control" name="detail[{{$key}}][rencana]" id="exampleFormControlTextarea1" placeholder="Rencana yang akan datang" rows="2">{{$data->rencana}}</textarea>
                                    </td>
                                    <td scope="row">
                                        <div class="field">
                                            <input type="text" name="detail[{{$key}}][pen_3]" value="{{$data->pen_3}}" class="form-control" placeholder="Penanggung Jawab">
                                        </div>
                                    </td>
                                    <td style="text-align: center;">
                                        @if($key == 0)
                                        <button class="btn btn-sm btn-success tambah_risalah" type="button"><i class="fa fa-plus"></i></button>
                                        @else
                                        <button class="btn btn-sm btn-danger hapus_risalah" type="button" data-id="{{$key+1}}"><i class="fa fa-remove"></i></button>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr class="data-container-1" data-id="1">
                                    <td scope="row" style="text-align: center;">
                                        <label class="numboor-1">1</label>
                                    </td>
                                    <td scope="row">
                                        <textarea class="form-control" name="detail[0][risalah]" id="exampleFormControlTextarea1" placeholder="Risalah" rows="2"></textarea>
                                    </td>
                                    <td scope="row">
                                        <div class="field">
                                            <input type="text" name="detail[0][pen_1]" class="form-control" placeholder="Penanggung Jawab">
                                        </div>
                                    </td>
                                    <td scope="row">
                                        <textarea class="form-control" name="detail[0][realisasi]" id="exampleFormControlTextarea1" placeholder="Realisasi" rows="2"></textarea>
                                    </td>
                                    <td scope="row">
                                        <div class="field">
                                            <input type="text" name="detail[0][pen_2]" class="form-control" placeholder="Penanggung Jawab">
                                        </div>
                                    </td>
                                    <td scope="row">
                                        <textarea class="form-control" name="detail[0][rencana]" id="exampleFormControlTextarea1" placeholder="Rencana yang akan datang" rows="2"></textarea>
                                    </td>
                                    <td scope="row">
                                        <div class="field">
                                            <input type="text" name="detail[0][pen_3]" class="form-control" placeholder="Penanggung Jawab">
                                        </div>
                                    </td>
                                    <td style="text-align: center;">
                                        <button class="btn btn-sm btn-success tambah_risalah" type="button"><i class="fa fa-plus"></i></button>
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                <div class="form-row">
                    <div class="form-group col-xs-12">
                        <br>
                        <div class="text-right">
                            <button type="button" class="btn btn-cancel previous-tab">Kembali</button>
                            <button type="button" class="btn btn-primary save-draft page status-rapat">Save As Draft</button>
                            <button type="button" class="btn btn-simpan save as page status-rapat">Submit</button>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script> 
    	$(document).ready(function(){
    		// reloadMask();
            $('.tanggal').datepicker({
                format: 'dd-mm-yyyy',
                startDate: '-0d',
                orientation: "auto",
                autoclose:true,
            });
            $('.waktu').clockpicker({
                autoclose:true,
                'default': 'now',
            });
            $('#myTabs a').click(function (e) {
				e.preventDefault()
				$(this).tab('show')
			})
            var dor = '{{ $record->fileHadirJson() }}';
            var filerihadir = '{{ $record->fileHadir() }}';
            $("#hadir").fileinput({
                autoReplace: true,
                overwriteInitial: true,
                // maxFileCount: 1,
                // initialPreview: filerihadir,
                initialPreview: JSON.parse(filerihadir.replace(/&quot;/g,'"')),
                initialPreviewAsData: true,
                initialPreviewConfig: JSON.parse(dor.replace(/&quot;/g,'"')),
                // initialPreviewConfig: [
                //     {type: "{{ $record->files()->where('flag','hadir')->pluck('type')->first() }}",caption: "{{ $record->files()->where('flag','hadir')->pluck('filename')->first() }}", downloadUrl: filerihadir, size: 930321, width: "120px", key: 1},
                // ],
                initialCaption: "{{ $record->files()->where('flag','hadir')->pluck('filename')->first() }}",
                initialPreviewShowDelete: false,
                showRemove: false,
                showClose: false,
                layoutTemplates: {actionDelete: ''}, // disable thumbnail deletion
                // allowedFileExtensions: ["{{ $record->files()->where('flag','hadir')->pluck('type')->first() }}"]
            });
                var obj = '{{ $record->fileOtherJson() }}';
                var fileUri = '{{ $record->fileOther() }}';
            $('#other').fileinput({
                autoReplace: true,
                overwriteInitial: true,
                // maxFileCount: 1,
                initialPreview: JSON.parse(fileUri.replace(/&quot;/g,'"')),
                initialPreviewAsData: true,
                initialPreviewConfig: JSON.parse(obj.replace(/&quot;/g,'"')),
                // initialCaption: "{{ $record->files()->where('flag','hadir')->pluck('filename')->first() }}",
                initialPreviewShowDelete: false,
                showRemove: false,
                showClose: false,
                layoutTemplates: {actionDelete: ''}, // disable thumbnail deletion
            });
        });
        $(document).on('click','.next-tab', function(){
			var next = $('.nav-tabs > .active').next('li');
			if(next.length){
				next.find('a').trigger('click');
			}else{
				$('#myTabs a:first').tab('show');
			}
		});
		$(document).on('click','.previous-tab', function(){
			var prev = $('.nav-tabs > .active').prev('li')
			if(prev.length){
				prev.find('a').trigger('click');
			}else{
				$('#myTabs a:last').tab('show');
			}
		});
		
        $(document).on('click','.detail-rapats', function(){
            if($('.show-detail').css('display') == 'none'){
                $('.show-detail').fadeIn(1000);
            }else{
                $('.show-detail').fadeOut(1000);
            }
		});
		
		$(document).on('click','.add-line', function(){
			var i = $('.add-line').length;
			var htm = `<tr>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<input type="hidden" name="key_daftar[]" value="`+i+`">
									<input type="text" name="nama_peserta[]" class="form-control" placeholder="Nama">
								</div>
							</td>
							<td class="form-row">
                                <div class="form-group col-xs-12 field">
                                    <input type="text" name="email[]" class="form-control" placeholder="Email">
                                </div>
                            </td>
                            <td class="form-row">
                                <div class="form-group col-xs-12 field">
                                    <input type="text" name="nomor_hp[]" class="form-control" placeholder="Nomor Handphone">
                                </div>
                            </td>
                            <td class="form-row">
                                <div class="form-group col-xs-12 field">
                                    <input type="text" name="perusahaan[]" class="form-control" placeholder="Perusahaan">
                                </div>
                            </td>
							<td class=" text-center">
								<a data-content="Detil" class="remove-line" data-id="1" class="m-l detil button" data-toggle="tooltip" title="" data-original-title="Detil">
									<i class="fa fa-trash text-danger"></i>
								</a>
							</td>
						</tr>`;
			$('.show-daftar').append(htm);
		});
		$(document).on('click','.remove-line', function(){
			$(this).parent().parent().remove();
		});

        // risalah
        $(document).on('click', '.tambah_risalah', function(e){
            var rowCount = $('#example > tbody > tr').length;
            var c = rowCount-1;
            var html = `
                    <tr class="data-container-`+(c+2)+`" data-id="`+(c+2)+`">
                            <td scope="row" style="text-align: center;">
                                <label class="numboor-`+(c+2)+`">`+(c+2)+`</label>
                            </td>
                            <td scope="row">
                            <textarea class="form-control" name="detail[`+(c+2)+`][risalah]" id="exampleFormControlTextarea1" placeholder="Risalah" rows="2"></textarea>
                            </td>
                            <td scope="row">
                            <div class="field">
                            <input type="text" name="detail[`+(c+2)+`][pen_1]" class="form-control" placeholder="Penanggung Jawab">
                            </div>
                            </td>
                            <td scope="row">
                            <textarea class="form-control" name="detail[`+(c+2)+`][realisasi]" id="exampleFormControlTextarea1" placeholder="Realisasi" rows="2"></textarea>
                            </td>
                            <td scope="row">
                            <div class="field">
                            <input type="text" name="detail[`+(c+2)+`][pen_2]" class="form-control" placeholder="Penanggung Jawab">
                            </div>
                            </td>
                            <td scope="row">
                            <textarea class="form-control" name="detail[`+(c+2)+`][rencana]" id="exampleFormControlTextarea1" placeholder="Rencana yang akan datang" rows="2"></textarea>
                            </td>
                            <td scope="row">
                            <div class="field">
                            <input type="text" name="detail[`+(c+2)+`][pen_3]" class="form-control" placeholder="Penanggung Jawab">
                            </div>
                            </td>
                            <td style="text-align: center;">
                                <button class="btn btn-sm btn-danger hapus_risalah" type="button" data-id=`+(c+2)+`><i class="fa fa-remove"></i></button>
                            </td>
                        </tr>
                `;

                $('.container').append(html);
                $('.selectpicker').selectpicker();
        });

        $(document).on('click', '.hapus_risalah', function (e){
            var row = $(this).closest('tr');
            row.remove();
            var table = $('#example');
            var rows = table.find('tbody tr');

            $.each(rows, function(key, value){
                console.log($(this).data("id"))
                table.find('.numboor-'+$(this).data("id")).html(key+1);
            });
        });
    </script>
    @yield('js-extra')
@endpush

