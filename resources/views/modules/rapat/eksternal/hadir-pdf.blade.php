<html>
<head>
	{{-- @include('style.pdf') --}}
	<style>
	@page { margin: 5px 30px 30px 50px; }
	body {
		/* TRBL */
		margin: 155px 5px 5px 5px;
		/* border: 1px solid #055998;  */
    }
    header{
        position: fixed;
    }
	.content{
		position: relative;
        padding-left:-5px ;
        padding-right:-5px ;
		text-align: justify;
		text-justify: inter-word;
	}
	p{
		font-size: 12px;
        margin-top: 0px;
		text-align: center;
  		text-justify: inter-word;
	}
	i{
		font-size: 12px;
	}
	input[type=checkbox] { 
		display: inline;
		position: relative; 
		vertical-align: baseline;
		/*height: 100%;*/
	}
	.partial{
		-webkit-region-break-inside: avoid;
		position: relative;
		page-break-inside: avoid;
	}

	.tbl-transparant-header{
		width: 100%;
		/* font-size: 17px; */
		text-align: center;
  		text-justify: inter-word;
	}
	.tbl-transparant{
		width: 100%;
		/* font-size: 17px; */
		text-align: justify;
  		text-justify: inter-word;
	}
	.tbl-transparant-data{
		width: 100%;
		text-align: left;
	}
	.tbl-transparant-data > tbody > tr > td > u > p{
		text-align: left;
	}
	.tbl-transparant-data > tbody > tr > td > p{
		text-align: left;
	}
	.header-content{
		text-align: center;
	}
	
	h2{
		font-size: 30px;
	}
	h5{
		margin-bottom: 0px;
        font-size: 17px;
	}
	.page-break {
        page-break-after: always;
	}
    .p-clasess {
        font-size: 12px;
        margin-top: 5px;
        margin-bottom: 5px;
		text-align: center;
  		text-justify: inter-word;
    }
    .p-content {
        font-size: 12px;
        margin-top: 5px;
		text-align: left;
        margin-bottom: 5px;
  		text-justify: inter-word;
    }
</style>
</head>
<body>
	{{-- @include('style.temp') --}}
    <script type="text/php">
		if (isset($pdf)) {
			$font = $fontMetrics->getFont("Times New Romans", "bold");
			 $pdf->page_text(730, 570, "Halaman {PAGE_NUM} dari {PAGE_COUNT}", $font, 10, array(0, 0, 0));
		}
    </script>
    <header>
        <table class="ui table bordered" style="width: 100%">
            <tr>
                <td width="25%">
                   
                </td>
                <td style="width: 40%;vertical-align: top;">
                    <p style="font-size: 16px;text-align: center"><b>DAFTAR HADIR RAPAT <br>
                        PEMBAHASAN LAPORAN HASIL AUDIT<br>
                        BULAN {{ $blnthn }}<br>
                        RUANG RAPAT INTERNAL AUDIT<br>
                        {{ $today }}</b></p>
                </td>
                <td style="width: 25%;vertical-align: top;">
                   
                </td>
            </tr>
        </table>
        <table class="" style="width: 100%;margin-top: 3px;border-collapse: collapse;">
            <tr>
                <td style="width: 5%;border : 1px solid black;">
                    <p class="p-clasess">No</p>
                </td>
                <td style="width: 40%;border : 1px solid black;">
                    <p class="p-clasess">Nama</p>
                </td>
                <td style="width: 40%;border : 1px solid black;">
                    <p class="p-clasess">Jabatan</p>
                </td>
                <td style="width: 15%;border : 1px solid black;">
                    <p class="p-clasess">TTD</p>
                </td>
            </tr>
        </table>
    </header>
    <div class="content">
        <table class="" style="width: 100%;margin-top: 3px;border-collapse: collapse;">
            @if (isset($record->daftarHadir) AND $record->daftarHadir->count() > 0)
                @foreach ($record->daftarHadir as $key => $item)
                    @if ($record->status == 0)
                        <tr>
                            <td style="width: 5%;border : 1px solid black;vertical-align: top">
                                <p class="p-content" style="text-align: center">
                                    {{ $key+1 }}
                                </p>
                            </td>
                            <td style="width: 40%;border : 1px solid black;vertical-align: top">
                                <p class="p-content">
                                    {{ $item->user->name }}
                                </p>
                            </td>
                            <td style="width: 40%;border : 1px solid black;vertical-align: top">
                                <p class="p-content">
                                    {{ $item->user->roles->first()->name }}
                                </p>
                            </td>
                            <td style="width: 15%;border : 1px solid black;vertical-align: top">
                                <p class="p-content">

                                </p>
                            </td>
                        </tr>
                    @else
                        <tr>
                            <td style="width: 5%;border : 1px solid black;vertical-align: top">
                                <p class="p-content" style="text-align: center">
                                    {{ $key+1 }}
                                </p>
                            </td>
                            <td style="width: 40%;border : 1px solid black;vertical-align: top">
                                <p class="p-content">
                                    {{ $item->nama }}
                                </p>
                            </td>
                            <td style="width: 40%;border : 1px solid black;vertical-align: top">
                                <p class="p-content">
                                    {{ $item->perusahaan }}
                                </p>
                            </td>
                            <td style="width: 15%;border : 1px solid black;vertical-align: top">
                                <p class="p-content">

                                </p>
                            </td>
                        </tr>
                    @endif
                @endforeach
                @if ($record->daftarHadir->count() < 30)
                    @for ($i = $record->daftarHadir->count(); $i < 30; $i++)
                        <tr>
                            <td style="width: 5%;border : 1px solid black;vertical-align: top">
                                <p class="p-content" style="text-align: center">
                                    {{ $i+1 }}
                                </p>
                            </td>
                            <td style="width: 40%;border : 1px solid black;vertical-align: top">
                                <p class="p-content">

                                </p>
                            </td>
                            <td style="width: 40%;border : 1px solid black;vertical-align: top">
                                <p class="p-content">

                                </p>
                            </td>
                            <td style="width: 15%;border : 1px solid black;vertical-align: top">
                                <p class="p-content">

                                </p>
                            </td>
                        </tr>
                    @endfor
                @endif
            @else
                @for ($i = 0; $i < 30; $i++)
                    <tr>
                        <td style="width: 5%;border : 1px solid black;vertical-align: top">
                            <p class="p-content">
                                &nbsp;
                            </p>
                        </td>
                        <td style="width: 40%;border : 1px solid black;vertical-align: top">
                            <p class="p-content">

                            </p>
                        </td>
                        <td style="width: 40%;border : 1px solid black;vertical-align: top">
                            <p class="p-content">

                            </p>
                        </td>
                        <td style="width: 15%;border : 1px solid black;vertical-align: top">
                            <p class="p-content">

                            </p>
                        </td>
                    </tr>
                @endfor
            @endif
        </table>
    </div>
</body>
</html>
