<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
    @csrf

    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">QR Code Pengisian Daftar Hadir</h5>
    </div>
    {{-- {{ dd($link) }} --}}
    <div class="modal-body" style="text-align: center">
        <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(600)->margin(0)->merge($mark, 0.2, true)->generate($link)) !!} ">
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Tutup</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>