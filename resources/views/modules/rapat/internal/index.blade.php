@extends('layouts.list-grid')

@section('title', $title)

@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection

@section('filters')
    <div class="form-group">
        <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
        <label class="control-label sr-only" for="filter-nomor">Nomor Rapat</label>
        <input type="text" class="form-control filter-control" name="filter[nomor]" data-post="nomor" placeholder="Nomor Rapat">
    </div>
    <div class="form-group">
        <label class="control-label sr-only" for="filter-tema">Tema / Materi Rapat</label>
        <input type="text" class="form-control filter-control" name="filter[tema]" data-post="tema" placeholder="Agenda">
    </div>
    <div class="form-group">
        <label class="control-label sr-only" for="filter-tanggal">Tanggal Dilaksanakan</label>
        <input type="text" class="form-control filter-control tanggal" name="filter[tanggal]" data-post="tanggal" placeholder="Tanggal">
    </div>
    
@endsection
@push('scripts')
    <script> 
    	$(document).ready(function(){
            // $('.tanggal').datepicker({
            //     autoclose: true,
            //     startDate: today,
            //     format: 'dd/mm/yyyy',
            //     daysOfWeekDisabled: "6,0",
            //     language: "en"
            // }).on('changeDate', function(e) {
            //     console.log(e.format());
            // });
            // $('input[filter[tanggal]]').datepicker({
            //     format: 'dd/mm/yyyy',
            //     startDate: '-0d',
            //     orientation: "auto",
            //     autoclose:true,
            // });
            $('.tanggal').datepicker({
                format: 'dd-mm-yyyy',
                // startDate: '-0d',
                orientation: "auto",
                autoclose:true,
            });
        });
        $(document).on('click','.show-modal.button', function(e){
            var idx = $(this).data('id');
            loadModal({
                url: "{!! route($routes.'.index') !!}/code/"+idx,
                modal: '#largeModal',
            }, function(resp){
                $("#largeModal").find('.loading.dimmer').hide();
                $('.selectpicker').selectpicker();
                onShow();
            });
        });
        
        $(document).on('click','.re-upload.button', function(e){
            var url = "{!! route($routes.'.index') !!}/re-upload/"+$(this).data('id');
            window.location.href = url ;
        });

        $(document).on('click', '.cetak.button', function(e){
            var idx = $(this).data('id');
            var url = "{!! route($routes.'.index') !!}/" + idx + '/cetak';
            window.open(url, '_blank');
        });
    </script>
@endpush
