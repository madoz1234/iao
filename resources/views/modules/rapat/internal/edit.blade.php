@extends('layouts.form')
@section('title', 'Ubah Data '.$title)
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData" autocomplete="off">
@method('PATCH')
@csrf
<div class="panel panel-default">
    <div class="panel-body" style="padding-bottom: 0px;">
        <input type="hidden" name="id" value="{{$record->id}}">
        <input type="hidden" name="flag">
        <div class="form-row">
            {{-- kiri --}}
            <div class="form-group col-xs-6" style="padding-right: 14px;">
                <div class="form-group col-xs-12 field" style="padding-right: 14px;">
                    <label for="inputEmail4">Nomor Rapat</label>
                    <input type="text" name="nomor" class="form-control" placeholder="Nomor Rapat" value="{{$record->nomor}}">
                    <input type="hidden" class="form-control" name="jumlah_peserta" value="{{$record->daftarHadir->count()}}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" placeholder="Jumlah Peserta">
                </div>
                <div class="form-group col-xs-12 field" style="padding-right: 14px;">
                    <label for="inputEmail4">Tempat</label>
                    <input type="text" name="tempat" class="form-control" placeholder="Tempat" value="{{$record->tempat}}">
                </div>
                <div class="col-xs-12" style="padding-right: 14px;">
                    <div class="form-row">
                        <div class="form-group col-xs-6 field" style="padding-right: 14px;">
                            <label for="inputEmail4">Hari</label>
                            <input type="text" name="hari" class="form-control hari" placeholder="Hari">
                        </div>
                        <div class="form-group col-xs-6 field" style="padding-left: 14px;">
                            <label for="inputEmail4">Tanggal</label>
                            <div class="input-group" data-placement="left" data-align="top" data-autoclose="true">
                                <input type="text" class="form-control tgl_pelaksanaan" name="tanggal" placeholder="Tanggal Pelaksanaan" value="{{ Carbon::parse($record->tanggal)->format('d-m-Y') }}">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12" style="padding-right: 14px;">
                    <div class="form-row">
                        <div class="form-group col-xs-6 field" style="padding-right: 14px;">
                            <label for="inputEmail4">Jam</label>
                            {{-- {{ App\Libraries\CoreSn::Day($record->tanggal) }} --}}
                            <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                <input type="text" class="form-control" name="jam_mulai" placeholder="Mulai" value="{{$record->jam_mulai}}">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-xs-6 field" style="padding-left: 14px;">
                            <label for="inputEmail4">&nbsp</label>
                            <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                <input type="text" class="form-control" name="jam_selesai" placeholder="Selesai" value="{{$record->jam_selesai}}">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- kanan --}}
            <div class="form-group col-xs-6" style="padding-left: 14px;">
                <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                    <label for="inputEmail4">Kategori</label>
                    {{-- <input type="text" class="form-control" value="Umum Konsultasi" disabled=""> --}}
                    @if ($record->status_kategori == 0)
                        @if ($record->kategori == 'Kegiatan Konsultasi')
                            <input type="text" class="form-control" value="Kegiatan Konsultasi" disabled="">
                        @elseif($record->kategori == 'Kegiatan Lainnya')
                            <input type="text" class="form-control" value="Kegiatan Lainnya" disabled="">
                        @else
                            <input type="text" class="form-control" value="-" disabled="">
                        @endif
                    @else
                        <select class="selectpicker form-control project" 
                            name="status_kategori"
                            data-style="btn-default" 
                            data-live-search="true" 
                            title="(Kategori)"
                            >
                            <option value="1" @if($record->status_kategori == 1) selected @endif>Umum</option>
                            <option value="2" @if($record->status_kategori == 2) selected @endif>Konsultasi</option>
                        </select>
                    @endif
                </div>
                <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                    <table id="example-materi" style="width: 100%;font-size: 12px;">
                        <tbody class="container-materi">
                            {{-- {{ dd($record->agendaInternal) }} --}}
                            @foreach($record->agendaInternal as $key => $data)
                                <tr class="data-container-{{$key+1}}" data-id="{{$key+1}}">
                                    @if($key == 0)
                                    <input type="hidden" name="data[{{ $key }}][id]" value="{{ $data->id }}">
                                    <input type="hidden" name="exists[]" value="{{$data->id}}">
                                    <td scope="row">
                                        <div class="field">
                                            <label for="inputEmail4">Agenda</label>
                                            <input type="text" name="data[{{$key}}][agenda]" data-id="{{$key}}" class="form-control" placeholder="Agenda" value="{{ $data->agenda }}">
                                        </div>
                                    </td>
                                    <td style="text-align: center; padding-top: 15px;">
                                        <button class="btn btn-sm btn-success tambah_materi" type="button"><i class="fa fa-plus"></i></button>
                                    </td>
                                    @else
                                    <input type="hidden" name="data[{{ $key }}][id]" value="{{ $data->id }}">
                                    <input type="hidden" name="exists[]" value="{{$data->id}}">
                                    <td scope="row">
                                        <div class="field">
                                            <label for="inputEmail4">&nbsp;</label>
                                            <input type="text" name="data[{{$key}}][agenda]" data-id="{{$key}}" class="form-control" placeholder="Agenda" value="{{ $data->agenda }}">
                                        </div>
                                    </td>
                                    <td style="text-align: center; padding-top: 15px;">
                                        <button class="btn btn-sm btn-danger hapus_materi" type="button" data-id="{{$key+1}}"><i class="fa fa-remove"></i></button>
                                    </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                    <label for="inputEmail4">Keterangan Rapat</label>
                    <textarea name="keterangan" rows="3" class="form-control" placeholder="Keterangan Rapat">{{$record->keterangan}}</textarea>
                </div>
            </div>
        </div>

        {{-- daftar peserta --}}
        <div class="col-sm-12">
    		<h4 class="m-t-lg m-b" style="font-weight: bold;">Peserta Undangan</h4>
            <table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
                <thead style="background-color: #f9fafb;">
                    <tr>
                        <th scope="col" style="text-align: center; width: 5%;">#</th>
                        <th scope="col" style="text-align: center; width: 20%;">Nama</th>
                        <th scope="col" style="text-align: center; width: 15%;">Email</th>
                        <th scope="col" style="text-align: center; width: 15%;">No Telp/HP</th>
                        <th scope="col" style="text-align: center; width: 15%;">Jabatan</th>
                        <th scope="col" style="text-align: center; width: 15%;">BU/CO</th>
                        <th scope="col" style="text-align: center; width: 40%;">Project</th>
                        <th scope="col" style="text-align: center; width: 5%;">Aksi</th>
                    </tr>
                </thead>
                <tbody class="container">
                    @foreach($record->undanganInternal as $key => $data)
                    {{-- {{ dd($data->undanganInternalProject[0]['project_id'] == 0) }} --}}
                    {{-- @foreach(App\Models\Master\Project::get() as $p)
                        @if ($data->undanganInternalProject[0]['project_id'] == 0)
                            <option value="0">Tidak ada pilihan</option>
                            <option value="{{ $p->id }}" @if(in_array($p->id, $data->undanganInternalProject->pluck('project_id')->toArray(), TRUE)) selected @endif>{{ $p->nama }}</option>

                        @else
                            <option value="{{ $p->id }}" @if(in_array($p->id, $data->undanganInternalProject->pluck('project_id')->toArray(), TRUE)) selected @endif>{{ $p->nama }}</option>
                        @endif
                    @endforeach --}}
                    {{-- {{ dd($data->user->project_pic) }} --}}
                    <tr class="data-container-{{$key+1}}" data-id="{{$key+1}}">
                        <input type="hidden" name="detail[{{$key}}][id]" value="{{ $data->id }}">
                        <input type="hidden" name="exists[]" value="{{$data->id}}">
                        <td scope="row" style="text-align: center;">
                            <label style="margin-top: 6px;" class="numboor-{{$key+1}}">{{ $key+1 }}</label>
                        </td>
                        <td scope="row">
                            <div class="field">
                                <select class="selectpicker form-control user" 
                                name="detail[{{$key}}][user_id]"
                                data-style="btn-default" 
                                data-live-search="true" 
                                title="(Nama)"
                                data-size="5"
                                data-id="{{$key}}"
                                onChange="checkSame(this)"
                                >
                                {!! \App\Models\Auths\User::options('name', 'id', ['selected' => $data->user_id] ,'') !!}
                                </select>
                            </div>
                        </td>
                        <td scope="row">
                            <input type="text" name="detail[{{$key}}][email]" id="email" data-id="{{$key}}" class="form-control email-{{$key}}" placeholder="Email" disabled>
                        </td>
                        <td scope="row">
                            <input type="text" name="detail[{{$key}}][hp]" id="hp" data-id="{{$key}}" class="form-control hp-{{$key}}" placeholder="Email" disabled>
                        </td>
                        <td scope="row">
                            <input type="text" name="detail[{{$key}}][jabatan]" id="jabatan" data-id="{{$key}}" class="form-control jabatan-{{$key}}" placeholder="Email" disabled>
                        </td>
                        <td scope="row">
                            <input type="text" name="detail[{{$key}}][bu_co]" id="bu_co" data-id="{{$key}}" class="form-control bu_co-{{$key}}" placeholder="BU/CO" disabled>
                        </td>
                        <td scope="row">
                            <input type="hidden" name="val_{{$key}}">
                            <select class="selectpicker form-control project"
                                name="detail[{{$key}}][project_id][]"
                                data-style="btn-default" 
                                data-live-search="true" 
                                title="(Project)"
                                data-size="5"
                                id="project"
                                data-id="{{$key}}"
                                multiple=""
                                data-width="500px"
                                >
                                <option value="0" @if($data->undanganInternalProject[0]['project_id'] == 0)selected @endif>Tidak ada pilihan</option>
                                @foreach(App\Models\Master\Project::get() as $p)
                                    {{-- @if ($data->undanganInternalProject[0]['project_id'] == 0)
                                        <option value="0">Tidak ada pilihan</option>
                                    @else --}}
                                        <option value="{{ $p->id }}" @if(in_array($p->id, $data->undanganInternalProject->pluck('project_id')->toArray(), TRUE)) selected @endif>{{ $p->nama }}</option>
                                    {{-- @endif --}}
                                @endforeach
                                    {{-- @foreach($data->user->project_pic as $row)
                                        <option value="{{ $row->project->id }}" @if(in_array($row->project->id, $data->detailProject->pluck('id')->toArray(), TRUE)) selected @endif>{{ $row->project->nama }}</option>
                                    @endforeach --}}
                            </select>
                        </td>
                        <td style="text-align: center;">
                        @if($key == 0)
                            <button class="btn btn-sm btn-success tambah_peserta" type="button"><i class="fa fa-plus"></i></button>
                        @else
                            <button class="btn btn-sm btn-danger hapus_peserta" type="button" data-id="{{$key+1}}"><i class="fa fa-remove"></i></button>
                        @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="form-row">
            <div class="form-group col-xs-12">
                <br>
                <div class="text-right">
                    <button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
                    <button type="button" class="btn btn-primary save-draft page flag">Save As Draft</button>
                    <button type="button" class="btn btn-simpan save as page flag">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
@endsection
@push('styles')
    <link rel="stylesheet" href="{{ asset('libs/jquery/jquery-ui-1.12.1/jquery-ui.min.css') }}" type="text/css" />
    <style>
        div.dropdown-menu.open{
          max-width: 500px !important;
          overflow: hidden;
        }
        ul.dropdown-menu.inner{
          max-width: 500px !important;
          overflow-y: auto;
        }
    </style>
@endpush

@push('js')
    <script src="{{ asset('libs/jquery/jquery-ui-1.12.1/jquery-ui.js') }}"></script>
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script>
    	$(document).ready(function(){
            $('.user').each(function(){
                var row = $(this).closest('tr');
                var id = $(this).data("id");
                $.ajax({
                    url: '{{ url('ajax/option/get-user') }}',
                    type: 'POST',
                    data: {
                        _token: "{{ csrf_token() }}",
                        user_id: this.value,
                        id: id
                    },
                }).done(function(response) {
                		var val = $('input[name="val_'+id+'"]').val();
                        row.find('.email-'+id).val(response.email);
                        row.find('.bu_co-'+id).val(response.bu);
                        row.find('.hp-'+id).val(response.phone);
                        row.find('.jabatan-'+id).val(response.jabatan);
                        $('select[name="detail['+id+'][project_id]"]').html(response.project);
                        $('select[name="detail['+id+'][project_id]"]').selectpicker('refresh');
                        $('select[project_id['+id+']]"]').children('option[value="'+val+'"]').prop('selected',true);
                        // $('select[name="detail['+id+'][project_id]"]').children('option[value="'+val+'"]').prop('selected',true);
		                // $('select[name="detail['+id+'][project_id]"]').selectpicker('refresh');
                        // row.find('.data-project-'+id).val(response.project);
                }).fail(function() {
                    console.log("error");
                });
            });

            $('.project').each(function(){
                var row = $(this).closest('tr');
                var id = $(this).data("id");
                $.ajax({
                    url: '{{ url('ajax/option/get-project') }}',
                    type: 'POST',
                    data: {
                        _token: "{{ csrf_token() }}",
                        user_id: this.value,
                        id: id
                    },
                }).done(function(response) {
                        row.find('.bu_co-'+id).val(response.email);
                    // });
                }).fail(function() {
                    console.log("error");
                });
            });
        });

        // test
        function checkSame(element)
        {
            $(element).on('change', function () {
                var element = $(this)
                $.each($('select[name^="detail"][name$="[user_id]"'), function (index, one) {
                    if(!$(element).is($(one)))
                    {
                        $(one).find('option').show();
                        $(one).find('option[value="'+$(element).val()+'"]').hide();
                        $(one).selectpicker('refresh');
                    }
                });
                var id = $(this).data('id');
                var row = $(this).closest('tr');
                $.ajax({
                    url: '{{ url('ajax/option/get-user') }}',
                    type: 'POST',
                    data: {
                        _token: "{{ csrf_token() }}",
                        user_id: this.value,
                        id: id
                    },
                }).done(function(response) {
                    var val = $('input[name="val_'+id+'"]').val();
                    row.find('.email-'+id).val(response.email);
                    row.find('.bu_co-'+id).val(response.bu);
                    row.find('.hp-'+id).val(response.phone);
                    row.find('.jabatan-'+id).val(response.jabatan);
                    $('select[name="detail['+id+'][project_id]"]').html(response.project);
                    $('select[name="detail['+id+'][project_id]"]').selectpicker('refresh');
                    $('select[project_id['+id+']]"]').children('option[value="'+val+'"]').prop('selected',true);
                }).fail(function() {
                    console.log("error");
                });

                // project
                // $.ajax({
                //     {{-- url: '{{ url('ajax/option/get-project') }}', --}}
                //     type: 'POST',
                //     data: {
                //         {{-- _token: "{{ csrf_token() }}", --}}
                //         user_id: this.value,
                //         id: id
                //     },
                // }).done(function(response) {
                //         row.find('.bu_co-'+id).val(response.email);
                //     // });
                // }).fail(function() {
                //     console.log("error");
                // });
            })
        }
        // test
        $(document).ready(function(){
        // tara
        $('.project').on('change', function(){
                var data = $(this).val();
            
                if(jQuery.inArray('0', data) !== -1){
                    $('.project').selectpicker('deselectAll');
                    $('.project').selectpicker('val', '0');
                }
                $.ajax({
                    url: '{{ url('ajax/option/get-project') }}',
                    type: 'POST',
                    data: {
                        _token: "{{ csrf_token() }}",
                        project_id: this.value
                    },
                })
                .done(function(response) {
                    $('input[name=bu_co]').val(response.bu);
                    $('.user').selectpicker("refresh");
                    $('.project').selectpicker("refresh");
                })
                .fail(function() {
                    console.log("error");
                });
            })
        // tara
            // reloadMask();
            
            // tanggal
            $( ".tgl_pelaksanaan" ).datepicker({dateFormat: 'dd-mm-yy'});
            local = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum`at', 'Sabtu' ];

            $('.tgl_pelaksanaan').datepicker()
            .on("change", function () {    
                var today = new Date($('.tgl_pelaksanaan').datepicker('getDate'));      
                //alert(local[today.getDay()]);
                $('.hari').val(local[today.getDay()]);
            });
                $('.hari').val('{{ App\Libraries\CoreSn::Day($record->tanggal) }}');
            // waktu
            $('.clockpicker').clockpicker();
            $('.waktu').clockpicker({
                autoclose:true,
                'default': 'now',
            });
            $('#myTabs a').click(function (e) {
				e.preventDefault()
				$(this).tab('show')
			})
            var filerihadir = '{{ $record->fileHadir() }}';
            $("#hadir").fileinput({
                autoReplace: true,
                overwriteInitial: true,
                maxFileCount: 1,
                initialPreview: [filerihadir],
                initialPreviewAsData: true,
                initialPreviewConfig: [
                    {type: "{{ $record->files()->where('flag','hadir')->pluck('type')->first() }}",caption: "{{ $record->files()->where('flag','hadir')->pluck('filename')->first() }}", downloadUrl: filerihadir, size: 930321, width: "120px", key: 1},
                ],
                initialCaption: "{{ $record->files()->where('flag','hadir')->pluck('filename')->first() }}",
                initialPreviewShowDelete: false,
                showRemove: false,
                showClose: false,
                layoutTemplates: {actionDelete: ''}, // disable thumbnail deletion
                allowedFileExtensions: ["{{ $record->files()->where('flag','hadir')->pluck('type')->first() }}"]
            });
                var obj = '{{ $record->fileOtherJson() }}';
                var fileUri = '{{ $record->fileOther() }}';
            $('#other').fileinput({
                autoReplace: true,
                overwriteInitial: true,
                // maxFileCount: 1,
                initialPreview: JSON.parse(fileUri.replace(/&quot;/g,'"')),
                initialPreviewAsData: true,
                initialPreviewConfig: JSON.parse(obj.replace(/&quot;/g,'"')),
                // initialCaption: "{{ $record->files()->where('flag','hadir')->pluck('filename')->first() }}",
                initialPreviewShowDelete: false,
                showRemove: false,
                showClose: false,
                layoutTemplates: {actionDelete: ''}, // disable thumbnail deletion
            });
        });

        $(document).on('click','.next-tab', function(){
			var next = $('.nav-tabs > .active').next('li');
			if(next.length){
				next.find('a').trigger('click');
			}else{
				$('#myTabs a:first').tab('show');
			}
		});

		$(document).on('click','.previous-tab', function(){
			var prev = $('.nav-tabs > .active').prev('li')
			if(prev.length){
				prev.find('a').trigger('click');
			}else{
				$('#myTabs a:last').tab('show');
			}
		});
		
		$(document).on('click','.add-line', function(){
			var i = $('.add-line').length;
			var htm = `<tr>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<input type="hidden" name="key_daftar[]" value="`+i+`">
									<input type="text" name="nama_peserta[]" class="form-control" placeholder="Nama">
								</div>
							</td>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<input type="text" name="jabatan[]" class="form-control" placeholder="Jabatan">
								</div>
							</td>
							<td class=" text-center">
								<a data-content="Ubah" class="add-line" data-id="1" class="edit button" data-toggle="tooltip" title="" data-original-title="Ubah" style="padding-right: 10px;">
									<i class="fa fa-plus text-info"></i>
								</a>
								<a data-content="Detil" class="remove-line" data-id="1" class="m-l detil button" data-toggle="tooltip" title="" data-original-title="Detil">
									<i class="fa fa-trash text-danger"></i>
								</a>
							</td>
						</tr>`;
			$('.show-daftar').append(htm);
		});

		$(document).on('click','.remove-line', function(){
			$(this).parent().parent().remove();
		});

		$(document).on('click','.add-line-risalah', function(){
			var i = $('.add-line-risalah').length;
			var htm = `<tr>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<input type="hidden" name="key_risalah[]" value="`+i+`">
									<textarea name="risalah[]" class="form-control" placeholder="Risalah"></textarea>
								</div>
							</td>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<input type="text" name="pen_1[]" class="form-control" placeholder="Pen. Jawab">
								</div>
							</td>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<textarea name="realisasi[]" class="form-control" placeholder="Realisasi"></textarea>
								</div>
							</td>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<input type="text" name="pen_2[]" class="form-control" placeholder="Pen. Jawab">
								</div>
							</td>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<textarea name="rencana[]" class="form-control" placeholder="Rencana yang akan datang"></textarea>
								</div>
							</td>
							<td class="form-row">
								<div class="form-group col-xs-12 field">
									<input type="text" name="pen_3[]" class="form-control" placeholder="Pen. Jawab">
								</div>
							</td>
							<td class=" text-center">
								<a data-content="Ubah" class="add-line" data-id="1" class="edit button" data-toggle="tooltip" title="" data-original-title="Ubah" style="padding-right: 10px;">
									<i class="fa fa-plus text-info"></i>
								</a>
								<a data-content="Detil" class="remove-line-risalah" data-id="1" class="m-l detil button" data-toggle="tooltip" title="" data-original-title="Detil">
									<i class="fa fa-trash text-danger"></i>
								</a>
							</td>
						</tr>`;
			$('.show-risalah').append(htm);
		});

		$(document).on('click','.remove-line-risalah', function(){
			$(this).parent().parent().remove();
		});

        // peserta
        $(document).on('click', '.tambah_peserta', function(e){
            var rowCount = $('#example > tbody > tr').length;
            var c = rowCount-1;
            var html = `
                    <tr class="data-container-`+(c+2)+`" data-id="`+(c+2)+`">
                            <td scope="row" style="text-align: center;">
                                <label class="numboor-`+(c+2)+`">`+(c+2)+`</label>
                            </td>
                            <td scope="row">
								<div class="field">
		                            <select class="selectpicker form-control user" 
		                            name="detail[`+(c+2)+`][user_id]"
		                            data-style="btn-default" 
		                            data-live-search="true" 
		                            title="(Nama)"
		                            data-size="10"
		                            data-idx="`+c+2+`"
		                            >
		                            @foreach(App\Models\Auths\User::where('id','<>', 1)->where('tipe', 1)->whereNotIn('email', ['', '-'])->whereNotNull('email')->get() as $user)
					                    <option value="{{ $user->id }}">{{ $user->name }}</option>
					                @endforeach
		                            </select>
		                        </div>
		                    </td>
		                    <td scope="row">
								<input type="text" name="detail[`+(c+2)+`][email]" id="email`+c+2+`" class="form-control" placeholder="Email" readonly="">
		                    </td>
                            <td scope="row">
                                <input type="text" name="detail[`+(c+2)+`][hp]" id="hp`+c+2+`" class="form-control" placeholder="No Hp" readonly="">
                            </td>
                            <td scope="row">
                                <input type="text" name="detail[`+(c+2)+`][jabatan]" id="jabatan`+c+2+`" class="form-control" placeholder="Jabatan" readonly="">
                            </td>
                            <td scope="row">
                                <input type="text" name="detail[`+(c+2)+`][bu_co]" id="bu_co`+c+2+`" class="form-control" placeholder="BU/CO" readonly="">
                            </td>
                            <td scope="row">
                                <select class="selectpicker form-control project"
                                    name="detail[`+(c+2)+`][project_id][]"
                                    data-style="btn-default" 
                                    data-live-search="true" 
                                    title="(Pilih Project)"
                                    data-size="10"
                                    data-idx="`+c+2+`"
                                    id="project`+c+2+`"
                                    multiple=""
                                    data-width="500px"
                                    >
                                </select>
                            </td>
                            <td style="text-align: center;">
                                <button class="btn btn-sm btn-danger hapus_peserta" type="button" data-id=`+(c+2)+`><i class="fa fa-remove"></i></button>
                            </td>
                        </tr>
                `;

                $('.container').append(html);
                $('.selectpicker').selectpicker();

                $('.user').on('change', function(){
                	// console.log('oe');
                	var idx = $(this).data('idx');
                	// alert(idx);
	    			$.ajax({
	    				url: '{{ url('ajax/option/get-user') }}',
	    				type: 'POST',
	    				data: {
	    					_token: "{{ csrf_token() }}",
	    					user_id: this.value
	    				},
	    			})
	    			.done(function(response) {
	    				// alert(response);
	    				$('#email'+idx).val(response.email);
                        $('#bu_co'+idx).val(response.bu);
                        $('#hp'+idx).val(response.phone);
	    				$('#jabatan'+idx).val(response.jabatan);
	    				$('#project'+idx).html(response.project);
	    				$('.user').selectpicker("refresh");
	    				$('.project').selectpicker("refresh");
	    			})
	    			.fail(function() {
	    				console.log("error");
	    			});
	    		})

	    		$('.project').on('change', function(){
	    			var idx = $(this).data('idx');
                	// alert(idx);
                    // clear selected
                    var data = $(this).val();
                
                    if(jQuery.inArray('0', data) !== -1){
                        $('#project'+idx).selectpicker('deselectAll');
                        $('#project'+idx).selectpicker('val', '0');
                    }

	    			$.ajax({
	    				url: '{{ url('ajax/option/get-project') }}',
	    				type: 'POST',
	    				data: {
	    					_token: "{{ csrf_token() }}",
	    					project_id: this.value
	    				},
	    			})
	    			.done(function(response) {
	    				// $('#bu_co'+idx).val(response.bu);
	    				$('.user').selectpicker("refresh");
	    				$('.project').selectpicker("refresh");
	    			})
	    			.fail(function() {
	    				console.log("error");
	    			});
	    		})
        });

        $(document).on('click', '.hapus_peserta', function (e){
            var row = $(this).closest('tr');
            row.remove();
            var table = $('#example');
            var rows = table.find('tbody tr');

            $.each(rows, function(key, value){
                console.log($(this).data("id"))
                table.find('.numboor-'+$(this).data("id")).html(key+1);
            });
        });

        //agenda
        $(document).on('click', '.tambah_materi', function(e){
            var rowCount = $('#example-materi > tbody > tr').length;
            var c = rowCount-1;
            var html = `
                    <tr class="data-container-materi-`+(c+2)+`" data-id="`+(c+2)+`">
                        <td scope="row">
                            <div class="field">
                                <label for="inputEmail4">&nbsp;</label>
                                <input type="text" name="data[`+(c+2)+`][agenda]" class="form-control" placeholder="Agenda">
                            </div>
                        </td>
                        <td style="text-align: center;">
                            <button class="btn btn-sm btn-danger hapus_materi" type="button" data-id=`+(c+2)+`><i class="fa fa-remove"></i></button>
                        </td>
                    </tr>
                `;

                $('.container-materi').append(html);
        });

        $(document).on('click', '.hapus_materi', function (e){
            var row = $(this).closest('tr');
            row.remove();
            var table = $('#example-materi');
            var rows = table.find('tbody tr');

            $.each(rows, function(key, value){
                console.log($(this).data("id"))
                table.find('.numboor-'+$(this).data("id")).html(key+1);
            });
        });
    </script>
    @yield('js-extra')
@endpush

