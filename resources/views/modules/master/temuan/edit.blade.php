@extends('layouts.form')
@section('title', 'Ubah Data Temuan')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body">
		<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
			@method('PATCH')
		    @csrf
		    <input type="hidden" name="id" value="{{ $record->id }}">
		    <input type="hidden" name="standar" value="{{ $record->standarisasi_id }}">
		    <div class="form-row">
		    	<div class="form-group field col-md-3">
		    		<label class="control-label">Bidang</label>
		            <select class="select selectpicker form-control cari-bidang" name="bidang"  data-style="btn-default">
			               <option style="font-size: 12px;" value="">Bidang</option>
			               <option style="font-size: 12px;" @if($record->standarisasi->bidang == 1) selected @endif value="1">Operasional</option>
			               <option style="font-size: 12px;" @if($record->standarisasi->bidang == 2) selected @endif value="2">Keuangan</option>
			               <option style="font-size: 12px;" @if($record->standarisasi->bidang == 3) selected @endif value="3">Sistem</option>
			        </select> 
		    	</div>
		    	<div class="form-group field col-md-4" style="left: 30px;">
		    		<label class="control-label">Standardisasi Temuan</label>
		            <select class="selectpicker form-control cari cari-data" name="standarisasi_id" data-style="btn-default" data-live-search="true" data-size="5">
		            </select>    
		    	</div>
		    	<div class="form-group field col-md-4" style="left: 70px;top: -9px;">
		    		<table class="table" style="font-size: 12px;">
		    			<tbody>
		    				<tr style="border-color: black;">
		    					<td style="text-align:left;border: black;">Referensi / Kriteria</td>
		    				</tr>
		    				<tr>
		    					<td style="padding-left: 0px;">
		    						<ol class="cetil">
		    						</ol>   
		    					</td>
		    				</tr>
		    			</tbody>
		    		</table>
		    		 
		    	</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
						<thead style="background-color: #f9fafb;">
							<tr>
								<th scope="col" style="text-align: center;width: 50px;">No</th>
								<th scope="col" style="text-align: center;width: 200px;">Kode</th>
								<th scope="col" style="text-align: center;">Kriteria Temuan</th>
								<th scope="col" width="80px" style="text-align: center;">Aksi</th>
							</tr>
						</thead>
						<tbody class="container">
							@foreach($record->detail as $key => $data)
								<tr class="data-container-{{$key+1}}" data-id="{{$key+1}}">
									<input type="hidden" name="detail[{{$key}}][id]" value="{{ $data->id }}">
									<input type="hidden" name="exists[]" value="{{$data->id}}">
									<td scope="row" style="text-align: center;">
										<label style="margin-top: 23px;" class="numboor-{{$key+1}}">{{ $key+1 }}</label>
									</td>
									<td scope="row" style="text-align: center;" class="field">
										<input type="text" name="detail[{{$key}}][kode]" style="margin-top: 16px;" class="form-control" placeholder="Kode Temuan" value="{{$data->kode}}">
									</td>
									<td scope="row">
										<div class="field">
											<textarea class="form-control" name="detail[{{$key}}][deskripsi]" id="exampleFormControlTextarea1" placeholder="Kriteria Temuan" rows="2">{{$data->deskripsi}}</textarea>
										</div>
									</td>
									<td style="text-align: center;">
									@if($key == 0)
										<button class="btn btn-sm btn-success tambah_komponen" type="button" style="border-radius: 20px;margin-top: 19px;"><i class="fa fa-plus"></i></button>
									@else
										<button class="btn btn-sm btn-danger hapus_soal" type="button" style="border-radius:20px;margin-top: 19px;" data-id="{{$key+1}}"><i class="fa fa-remove"></i></button>
									@endif
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-right">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
						<button type="button" class="btn btn-simpan save as page">Simpan</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script>
    	$.ajax({
	        url: '{{ url('ajax/option/get-standarisasiss') }}',
	        type: 'POST',
	        data: {
	            _token: "{{ csrf_token() }}",
	            id: $('select[name=bidang]').val(),
	            val: $('input[name=standar]').val(),
	        },
	    }).done(function(response) {
	    	var val = $('input[name=standar]').val();
	    	$('select[name="standarisasi_id"]').html(response);
	    	$('select[name="standarisasi_id"]').children('option[value="'+val+'"]').prop('selected',true);
			$('select[name="standarisasi_id"]').selectpicker("refresh");
	    	$.ajax({
	            url: '{{ url('ajax/option/get-detil-standarisasi') }}',
	            type: 'POST',
	            data: {
	                _token: "{{ csrf_token() }}",
	                id: val,
	            },
	        }).done(function(response) {
	        	$('.cetil').append(response);
	        }).fail(function() {
	            console.log("error");
	        });
	    }).fail(function() {
	        console.log("error");
	    });


    	$(document).on('click', '.tambah_komponen', function(e){
			var rowCount = $('#example > tbody > tr').length;
			var c = rowCount-1;
			var html = `
					<tr class="data-container-`+(c+2)+`" data-id="`+(c+2)+`">
						<td scope="row" style="text-align: center;">
							<label style="margin-top: 23px;" class="numboor-`+(c+2)+`">`+(c+2)+`</label>
						</td>
						<td scope="row" style="text-align: center;" class="field">
							<input type="text" name="detail[`+(c+1)+`][kode]" style="margin-top: 16px;" class="form-control" placeholder="Kode Temuan">
						</td>
						<td scope="row">
							<div class="field">
								<textarea class="form-control" name="detail[`+(c+1)+`][deskripsi]" id="exampleFormControlTextarea1" placeholder="Temuan" rows="2"></textarea>
							</div>
						</td>
						<td style="text-align: center;">
							<button class="btn btn-sm btn-danger hapus_soal" type="button" style="border-radius:20px;margin-top: 19px;" data-id=`+(c+2)+`><i class="fa fa-remove"></i></button>
						</td>
					</tr>
				`;

				$('.container').append(html);
				$('.rencana').datepicker({
		            format: "mm-yyyy",
				    viewMode: "months", 
				    minViewMode: "months",
		            orientation: "auto",
		            autoclose:true
		        });

		        $('.selectpicker').selectpicker();
		});
		$(document).on('click', '.hapus_soal', function (e){
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#example');
			var rows = table.find('tbody tr');

			$.each(rows, function(key, value){
				console.log($(this).data("id"))
				table.find('.numboor-'+$(this).data("id")).html(key+1);
			});
		});

		$('.cari-bidang').on('change', function () {
            $.ajax({
                url: '{{ url('ajax/option/get-standarisasiss') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: this.value,
                    val: $('input[name=standar]').val(),
                },
            }).done(function(response) {
            	$('select[name="standarisasi_id"]').html(response);
				$('select[name="standarisasi_id"]').selectpicker("refresh");
            }).fail(function() {
                console.log("error");
            });
        });

		 $('.cari').on('change', function () {
            $.ajax({
                url: '{{ url('ajax/option/get-detil-standarisasi') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: this.value,
                },
            }).done(function(response) {
            	$('.cek-cek').remove();
            	$('.cetil').append(response);
            }).fail(function() {
                console.log("error");
            });
        });

    </script>
    @yield('js-extra')
@endpush

