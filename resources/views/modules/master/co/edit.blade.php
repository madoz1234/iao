<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
    @method('PATCH')
    @csrf
    <input type="hidden" name="id" value="{{ $record->id }}">
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Ubah Data Corporate Office (CO)</h5>
    </div>
    <div class="modal-body">
    	<div class="form-group field">
            <label class="control-label">Kode CO</label>
            <input type="text" name="kode" class="form-control" placeholder="Kode CO" value="{{ $record->kode }}">
        </div>
    	<div class="form-group field">
            <label class="control-label">Corporate Office</label>
            <input type="text" name="nama" class="form-control" placeholder="Corporate Office" value="{{ $record->nama }}">
        </div>
        <div class="form-group field">
            <label class="control-label">Alamat</label>
            <textarea class="form-control" name="alamat" id="exampleFormControlTextarea1" placeholder="Alamat" rows="2">{{ $record->alamat }}</textarea>
        </div>
        <div class="form-group field">
            <label class="control-label">No Telepon</label>
            <input type="text" name="no_tlp" class="form-control" placeholder="No Telepon" value="{{ $record->no_tlp }}">
        </div>
        <div class="form-group">
            <label class="control-label">PIC</label>
            <select class="selectpicker form-control" data-size="3" name="pic[]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)" multiple>
                @foreach(App\Models\Auths\User::WhereLike('SVP')->where(function ($user) use ($record) {
                			$user->where(function ($pic) use ($record) {
                				$pic->doesntHave('ap_pic')->doesntHave('bu_pic')->doesntHave('co_pic');
                			})->orWhere(function ($pic) use ($record) {
                				$pic->whereIn('id', $record->detail_pic->pluck('pic')->toArray());
                			});
                		})->get() as $user)
                    <option value="{{ $user->id }}" @if(in_array($user->id, $record->detail_pic->pluck('pic')->toArray(), TRUE)) selected @endif>{{ $user->name }}</option>
                @endforeach
            </select>                  
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>