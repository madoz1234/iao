<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
    @method('PATCH')
    @csrf
    <input type="hidden" name="id" value="{{ $record->id }}">
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Ubah Data Dokumen Pendahuluan</h5>
    </div>
    <div class="modal-body">
    	<div class="form-group field">
            <label class="control-label">Judul</label>
            <input type="text" name="judul" class="form-control" placeholder="Judul" required="" value="{{ $record->judul }}">
        </div>
        <div class="form-group field">
            <label class="control-label">Deskripsi</label>
            <textarea class="form-control" name="deskripsi" id="exampleFormControlTextarea1" placeholder="Deskripsi" rows="3">{{$record->deskripsi}}</textarea>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>