@extends('layouts.list')

@section('title', 'Pembobotan')

@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection

@section('filters')
    <div class="form-group">
        <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
        <label class="control-label sr-only" for="filter-version">Version</label>
        <input type="text" class="form-control filter-control" name="filter[version]" data-post="version" placeholder="Version">
    </div>
    {{-- <div class="form-group">
        <label class="control-label sr-only" for="filter-status">Status</label>
        <input type="text" class="form-control filter-control" name="filter[status]" data-post="status" placeholder="Status">
    </div> --}}
    <div class="form-group">
        <label class="control-label sr-only" for="">Kategori</label>
        <select class="selectpicker filter-control" 
                name="filter[status]" 
                data-post="status"
                data-style="btn-default" 
                data-live-search="true"
        >
                <option value="">Status</option>
                <option value="1">Non-Aktif</option>
                <option value="2">Aktif</option>
        </select>
    </div>
@endsection