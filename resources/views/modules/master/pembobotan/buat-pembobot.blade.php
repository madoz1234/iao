@extends('layouts.form')
@section('title', 'Buat Pembobotan')
@section('side-header')
   <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color: transparent !important;">
        <?php $i=1; $last=count($breadcrumb);?>
         @foreach ($breadcrumb as $name => $link)
            @if($i++ != $last)
                <li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item" aria-current="page"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @endif
         @endforeach
                <li class="breadcrumb-item active" aria-current="page">Buat</li>
      </ol>
    </nav>
@endsection
@section('body')
<div class="panel panel-default">
    <div class="panel-body">
        {{-- header --}}
        <div class="wrapper clearfix form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 text-left font-bold" for="nodok">Version</label>
                <div class="col-sm-4">
                    : {{ $record->version ? $record->version : '-' }}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 text-left font-bold" for="nodok">Deskripsi</label>
                <div class="col-sm-10">
                    : <span>{{ $record->deskripsi ? $record->deskripsi : '' }}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 text-left font-bold" for="nodok">Dibuat Pada</label>
                <div class="col-sm-4">
                    : {{ $record->created_at ? $record->created_at : '-' }}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 text-left font-bold" for="nodok">Status</label>
                <div class="col-sm-4">
                    @if ($record->status == 1)
                        : <span class="label label-success">Aktif</span>
                    @elseif($record->status == 2)
                        : <span class="label label-danger">Non-Aktif</span>
                    @else
                        : <span class="label label-warning">Belum Aktif</span>
                    @endif
                </div>
            </div>
        </div>
        {{-- header --}}
        <form action="{{ route($routes.'.simpanData', $record->id) }}" method="POST" id="formData">
            @csrf
            <input type="hidden" name="status">
            <input type="hidden" name="id" value="{{ $record->id }}">
            <input type="hidden" name="version" value="{{ $record->version }}">

            <div class="form-row">
                <div class="form-group col-md-12">
                    <table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
                        <thead style="background-color: #f9fafb;">
                            <tr>
                                <th scope="col" style="text-align: center; width: 5%;">#</th>
                                <th scope="col" style="text-align: center; width: 37%;">Point of Focus (PoF)</th>
                                <th scope="col" style="text-align: center; width: 14%;">Bobot (%)</th>
                                <th scope="col" style="text-align: center; width: 30%;">Unsur Pemenuhan</th>
                                <th scope="col" style="text-align: center; width: 15%;">Bobot PoF / Atribut</th>
                                <th scope="col" style="text-align: center; width: 5%;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody class="container">
                            @if (count($record->detail) > 0)
                                @foreach($record->detail as $key => $data)
                                    <tr class="data-container-{{$key+1}}" data-id="{{$key+1}}">
                                        <input type="hidden" name="detail[{{$key}}][id]" value="{{ $data->id }}">
                                        <input type="hidden" name="exists[]" value="{{$data->id}}">
                                        <td scope="row" style="text-align: center;">
                                            <label class="numboor-{{$key+1}}">{{ $key+1 }}</label>
                                        </td>
                                        <td scope="row">
                                            <div class="field">
                                                <select class="selectpicker form-control add-pof" @if($record->kertaskerja) disabled @endif name="detail[{{$key}}][pof_id]" data-live-search="true" data-id="{{$key}}" onChange="checkSame(this)" data-width="380px" data-size="5">
                                        			<option value="">Point of Focus (PoF)</option>
                                                    @foreach(\App\Models\Master\SpinPof::where('status', 1)->get() as $val)
	                                                    <option value="{{ $val->id }}" @if($val->id == $data->pof_id) selected @endif>{{ $val->judul }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </td>
                                        <td scope="row">
                                            <div class="input-group">
                                                <input type="text" class="form-control sub subtotal-{{$key}}" data-id="{{$key}}" name="subtotal_{{$key}}" placeholder="Bobot" style="text-align: right;" disabled="">
                                                <span class="input-group-addon" style="font-size: 10px;">% </span>
                                            </div>
                                        </td>
                                        <td scope="row">
                                            <div class="field" style="margin-left: -19px;margin-right: 24px;">
                                                <ul class="list list-more1 data-pof-{{$key}}" data-display="3" style="padding-left: 0px;"></ul>
                                            </div>
                                        </td>
                                        <td scope="row">
                                            <div class="field data-bobot-{{$key}}">
                                            @foreach($data->detailNilai as $kiy => $nilai)
                                                <input type="hidden" name="detailNilai[{{$key}}][data][{{$kiy}}][id]" value="{{ $nilai->id }}">
                                                <input type="hidden" name="data_exist[]" value="{{ $nilai->id }}">
                                                 <div class="input-group field">
                                                    <input type="hidden" class="form-control" name="detailNilai[{{$key}}][data][{{$kiy}}][pemenuhan_detail_id]" placeholder="Bobot" value="{{ $nilai->pemenuhan_detail_id }}">
                                                    <input type="text" @if($record->kertaskerja) disabled @endif class="form-control mask inputmask-{{$key}} cek-data ambil-{{$key}} change-nilai" data-id="{{$key}}" data-detail="{{$kiy}}" name="detailNilai[{{$key}}][data][{{$kiy}}][nilai]" placeholder="Bobot" value="{{ number_format($nilai->nilai) }}"><br>
                                                    <span class="input-group-addon" style="font-size: 10px;">% </span>
                                                </div>
                                                <br>
                                            @endforeach
                                            </div>
                                        </td>
                                        <td style="text-align: center;">
                                        @if($key == 0)
                                            <button class="btn btn-sm btn-success tambah_komponen" type="button"><i class="fa fa-plus"></i></button>
                                        @else
                                            <button class="btn btn-sm btn-danger hapus_soal" @if($record->kertaskerja) disabled @endif type="button" data-id="{{$key+1}}"><i class="fa fa-remove"></i></button>
                                        @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="data-container-1" data-id="1">
                                <td scope="row" style="text-align: center;">
                                    <label style="margin-top: 23px;" class="numboor-1">1</label>
                                </td>
                                <td scope="row">
                                    <div class="field">
                                        <select class="selectpicker form-control add-pof" name="detail[0][pof_id]" data-style="btn-default" data-id="0" data-live-search="true" onChange="checkSame(this)" data-width="380px" data-size="5">
                                        		<option value="">Point of Focus (PoF)</option>
                                                @foreach(\App\Models\Master\SpinPof::where('status', 1)->get() as $data)
                                                    <option value="{{ $data->id }}">{{ $data->judul }}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td scope="row">
                                    <div class="input-group">
                                        <input type="text" style="text-align: right;" class="form-control sub subtotal-0" data-id="0" name="subtotal_0" placeholder="Bobot" maxlength="7" disabled="">
                                        <span class="input-group-addon" style="font-size: 10px;">%</span>
                                    </div>
                                </td>
                                <td scope="row">
                                    <div class="field" style="margin-left: -19px;margin-right: 24px;">
                                        <ul class="list data-pof-0" data-display="3" style="padding-left: 0px;"></ul>
                                    </div>
                                </td>
                                <td scope="row">
                                    <div class="field data-bobot-0"></div>
                                </td>
                                <td style="text-align: center;">
                                    <button class="btn btn-sm btn-success tambah_komponen" type="button"><i class="fa fa-plus"></i></button>
                                </td>
                            </tr>
                            @endif
                        </tbody>
                        <thead style="background-color: #f9fafb;">
                            <tr>
                                <th scope="col" style="text-align: right; width: 30%;" colspan="2">Total :</th>
                                <th scope="col" style="text-align: center; width: 10%;">
                                    <div class="input-group">
                                        <input type="text" style="text-align: right;" class="form-control hasil" id="hasil" name="total" placeholder="Bobot" disabled="">
                                        <span class="input-group-addon" style="font-size: 10px;">%</span>
                                    </div>
                                </th>
                                <th scope="col" style="text-align: right; width: 25%;">Total :</th>
                                <th scope="col" style="text-align: center; width: 25%;">
                                    <div class="field">
                                        <div class="input-group">
                                            <input type="text" style="text-align: right;" class="form-control" name="total" placeholder="Bobot" disabled="">
                                            <span class="input-group-addon" style="font-size: 10px;">% </span>
                                        </div>
                                    </div>
                                </th>
                                <th scope="col" style="text-align: center; width: 5%;"></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <div class="text-right">
                        <button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
                        <button type="button" class="btn btn-info save-draft page">Simpan</button>
                        <button type="button" class="btn btn-simpan save as page">Aktivasi</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@push('js')
    <script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
    <script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
	    div.dropdown-menu.open{
		  max-width: 300px !important;
		  overflow: hidden;
		}
		ul.dropdown-menu.inner{
		  max-width: 300px !important;
		  overflow-y: auto;
		}
    </style>
@endpush

@push('scripts')
    <script>
        $(document).ready(function(){
            $('.add-pof').each(function(){
                var row = $(this).closest('tr');
                var id = $(this).data("id");
                $.ajax({
                    url: '{{ url('ajax/option/get-pof') }}',
                    type: 'POST',
                    data: {
                        _token: "{{ csrf_token() }}",
                        pof: this.value,
                        id: id
                    },
                }).done(function(response) {
                    $.each(response, function(key, value){
                        if(key==0){
                            var html = value;
                        }else{
                            var html = `-`;
                        }
                        row.find('.data-pof-'+id).html('');
                        row.find('.data-pof-'+id).append(html);
                    });
                }).fail(function() {
                    console.log("error");
                });
            });
        });
        $(document).on('click', '.tambah_komponen', function(e){
            var rowCount = $('#example > tbody > tr').length;
            var c = rowCount-1;
            var html = `
                    <tr class="data-container-`+(c+2)+`" data-id="`+(c+2)+`">
                        <td scope="row" style="text-align: center;">
                            <label style="margin-top: 23px;" class="numboor-`+(c+2)+`">`+(c+2)+`</label>
                        </td>
                        <td scope="row">
                            <div class="field">
                               <select class="selectpicker form-control add-pof" name="detail[`+(c+2)+`][pof_id]" data-style="btn-default" data-id="`+(c+2)+`" data-live-search="true" onChange="checkSame(this)" data-width="380px" data-size="5">
                                        <option value="">Point of Focus (PoF)</option>
                                        @foreach(\App\Models\Master\SpinPof::where('status', 1)->get() as $data)
                                            <option value="{{ $data->id }}">{{ $data->judul }}</option>
                                        @endforeach
                                </select>
                            </div>
                        </td>
                        <td scope="row">
                            <div class="field">
                                <div class="input-group">
                                    <input type="text" class="form-control sub subtotal-`+(c+2)+`" data-id="`+(c+2)+`" name="subtotal_`+(c+2)+`" placeholder="Bobot" style="text-align: right;" disabled>
                                    <span class="input-group-addon" style="font-size: 10px;">% </span>
                                </div>
                            </div>
                        </td>
                        <td scope="row">
                            <div class="field" style="margin-left: -19px;margin-right: 24px;">
                                <ul class="list list-more1 data-pof-`+(c+2)+`" data-display="3" style="padding-left: 0px;">
                                </ul>
                            </div>
                        </td>
                        <td scope="row">
                            <div class="field data-bobot-`+(c+2)+`">
                            </div>
                        </td>
                        <td style="text-align: center;">
                            <button class="btn btn-sm btn-danger hapus_soal" type="button" style="border-radius:20px;margin-top: 19px;" data-id=`+(c+2)+`><i class="fa fa-remove"></i></button>
                        </td>
                    </tr>
                `;
                $('.container').append(html);
                $('.rencana').datepicker({
                    format: "mm-yyyy",
                    viewMode: "months", 
                    minViewMode: "months",
                    orientation: "auto",
                    autoclose:true
                });
                $('.selectpicker').selectpicker();
                $.each($('select.add-pof'), function (index, el) {
                    if($(el).val() != "")
                    {
                        $('select[name="detail['+(c+2)+'][pof_id]"]').find('option[value="'+$(el).val()+'"]').hide();
                        $('select[name="detail['+(c+2)+'][pof_id]"]').selectpicker('refresh');
                    }
                });
        });

        function checkSame(element)
        {
            $(element).on('change', function () {
                var element = $(this)
                $.each($('select[name^="detail"][name$="[pof_id]"'), function (index, one) {
                    if(!$(element).is($(one)))
                    {
                        $(one).find('option').show();
                        $(one).find('option[value="'+$(element).val()+'"]').hide();
                        $(one).selectpicker('refresh');
                    }
                });
                var id = $(this).data('id');
                var row = $(this).closest('tr');
                $.ajax({
                    url: '{{ url('ajax/option/get-pof') }}',
                    type: 'POST',
                    data: {
                        _token: "{{ csrf_token() }}",
                        pof: this.value,
                        id: id
                    },
                }).done(function(response) {
                    $.each(response, function(key, value){
                        if(key==0){
                            var html = value;
                        }else{
                            var html = `-`;
                        }
                        row.find('.data-pof-'+id).html('');
                        row.find('.data-pof-'+id).append(html);
                    });
                }).fail(function() {
                    console.log("error");
                });
                $.ajax({
                    url: '{{ url('ajax/option/get-bobot') }}',
                    type: 'POST',
                    data: {
                        _token: "{{ csrf_token() }}",
                        pof: this.value,
                        id: id
                    },
                }).done(function(response) {
                    $.each(response, function(key, value){
                        if(key==0){
                            var htmlbobot = value;
                        }else{
                            var htmlbobot = `-`;
                        }
                        row.find('.data-bobot-'+id).html('');
                        row.find('.data-bobot-'+id).append(htmlbobot);

                        $(".change-nilai").keyup(function(e){
                            var id = $(this).data('id');
                            var detil_id = $(this).data('detail');
                            var value = parseFloat($(this).val());
                            var sum =0;
                            $('.ambil-'+id).each(function(){
                                var angka = $(this).val();
                                var nn =0;
                                if(isEmpty(angka)){
                                    nn = 0;
                                }else{
                                    nn = parseFloat(angka);
                                }
                                sum += nn;
                            });

                            var cek =0;
                            $('.cek-data').each(function(){
                                var nilai = $(this).val();
                                var nn =0;
                                if(isEmpty(nilai)){
                                    a = 0;
                                }else{
                                    a = parseFloat(nilai);
                                }
                                cek += a;
                            });

                            if(cek > 100){
                            	console.log(sum)
                                $('input[name="detailNilai['+id+'][data]['+detil_id+'][nilai]"]').val(0);
                                $('.subtotal-'+id).val(0);
                            }else{
                                $('.subtotal-'+id).val(sum.toFixed(3));
                            }
                            var total  = 0;
                            var tot =0;
                            $('.sub').each(function(){
                                var vil = $(this).val();
                                if(isEmpty(vil)){
                                    tot = 0;
                                }else{
                                    tot = parseFloat(vil);
                                }
                                total += tot;
                            });
                            $('input[name="total"]').val(total.toFixed(3));
                        });
                        $('.inputmask-'+id).inputmask("decimal", {
                            numericInput: true,
			                groupSeparator: '.',
			                radixPoint: ".",
			                digits: 3,
			                autoGroup: true,
			                autoUnmask: true,
			                placeholder: '0,00',
			                androidHack: "rtfm",
			                clearIncomplete: !0,
			                allowMinus: false,
                        });
                    });
                }).fail(function() {
                    console.log("error");
                });
            })
        }

        $('.mask').inputmask("decimal", {
            numericInput: true,
	        groupSeparator: '.',
	        radixPoint: ".",
	        digits: 3,
	        autoGroup: true,
	        autoUnmask: true,
	        placeholder: '0,00',
	        androidHack: "rtfm",
	        clearIncomplete: !0,
	        allowMinus: false,
        });
        
        $(".change-nilai").keyup(function(e){
            var id = $(this).data('id');
            var detil_id = $(this).data('detail');
            var value = parseFloat($(this).val());
            var sum =0;
            $('.ambil-'+id).each(function(){
                var angka = $(this).val();
                var nn =0;
                if(isEmpty(angka)){
                    nn = 0;
                }else{
                    nn = parseFloat(angka);
                }
                sum += nn;
            });

            var cek =0;
            $('.cek-data').each(function(){
                var nilai = $(this).val();
                var nn =0;
                if(isEmpty(nilai)){
                    a = 0;
                }else{
                    a = parseFloat(nilai);
                }
                cek += a;
            });

            if(cek > 100){
                $('input[name="detailNilai['+id+'][data]['+detil_id+'][nilai]"]').val(0);
                $('.subtotal-'+id).val(cek.toFixed(3));
            }else{
                $('.subtotal-'+id).val(sum.toFixed(3));
            }
            var total  = 0;
            var tot =0;
            $('.sub').each(function(){
                var vil = parseFloat($(this).val());
                if(isEmpty(vil)){
                    tot = 0;
                }else{
                    tot = parseFloat(vil);
                }
                total += tot;
            });
            $('input[name="total"]').val(total.toFixed(3));
        });

        function isEmpty(value) {
          return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
        }

        var total =0;
        $('.sub').each(function(){
            var id = $(this).data("id");
            var sum =0;
            $('.ambil-'+id).each(function(){
                var angka = $(this).val();
                var nn =0;
                if(isEmpty(angka)){
                    nn = 0;
                }else{
                    nn = parseFloat(angka);
                }
                sum += nn;
            });
            total += sum;
            $('.subtotal-'+id).val(sum.toFixed(3));
        });
        $('input[name="total"]').val(total.toFixed(3));

        $(document).on('click', '.hapus_soal', function (e){
            var row = $(this).closest('tr');
            var id = $(this).data('id');
            row.remove();
            var table = $('#example');
            var rows = table.find('tbody tr');
            var selected = $('select[name="detail['+id+'][pof_id]"]').children("option:selected").val();
            $.each($('select.add-pof'), function (index, el) {
                var idx = $(this).data('id')
                $('select[name="detail['+idx+'][pof_id]"]').find('option[value="'+selected+'"]').show();
                $('select[name="detail['+idx+'][pof_id]"]').selectpicker('refresh');
            });
            $.each(rows, function(key, value){
                table.find('.numboor-'+$(this).data("id")).html(key+1);
            });

            var total =0;
            $('.sub').each(function(){
                var id = $(this).data("id");
                var sum =0;
                $('.ambil-'+id).each(function(){
                    var angka = $(this).val();
                    var nn =0;
                    if(isEmpty(angka)){
                        nn = 0;
                    }else{
                        nn = parseFloat(angka);
                    }
                    sum += nn;
                });
                total += sum;
                $('.subtotal-'+id).val(sum.toFixed(3));
            });
            $('input[name="total"]').val(total.toFixed(3));
        });

    </script>
    @yield('js-extra')
@endpush
