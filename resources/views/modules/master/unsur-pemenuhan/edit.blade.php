@extends('layouts.form')
@section('title', 'Ubah Data Unsur Pemenuhan')
@section('side-header')
   <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color: transparent !important;">
        <?php $i=1; $last=count($breadcrumb);?>
         @foreach ($breadcrumb as $name => $link)
            @if($i++ != $last)
                <li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item" aria-current="page"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @endif
         @endforeach
                <li class="breadcrumb-item active" aria-current="page">Ubah</li>
      </ol>
    </nav>
@endsection
@section('body')
<div class="panel panel-default">
    <div class="panel-body">
        <form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
            @method('PATCH')
            @csrf
            <input type="hidden" name="id" value="{{ $record->id }}">
            <input type="hidden" name="data_fokus" value="{{ $record->fokus_audit_id }}">
            <div class="form-row">
                <div class="form-group field col-md-12">
                    <label class="control-label">Point of Focus (PoF)</label>
                    <div class="field">
                        <select class="selectpicker form-control" 
                            name="pof_id" 
                            data-style="btn-default" 
                            data-live-search="true" 
                            title="(Point of Focus (PoF))"
                            >
                            @foreach(\App\Models\Master\SpinPof::get() as $data)
                                @if ($data->id == $record->pof_id && $data->status == 1)
                                    <option value="{{ $data->id }}" @if($data->id == $record->pof_id) selected @endif>{{ $data->judul }}</option>
                                @elseif($data->status == 0)
                                    <option value="{{ $data->id }}">{{ $data->judul }}</option>
                                @endif
                                {{-- <option value="{{ $data->id }}" @if($data->id == $record->pof_id) selected @endif>{{ $data->judul }}</option> --}}
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
                        <thead style="background-color: #f9fafb;">
                            <tr>
                                <th scope="col" style="text-align: center; width: 5%;">No</th>
                                <th scope="col" style="text-align: center; width: 50%;">Unsur Pemenuhan</th>
                                <th scope="col" style="text-align: center; width: 5%;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody class="container">
                            @foreach($record->detail as $key => $data)
                                <tr class="data-container-{{$key+1}}" data-id="{{$key+1}}">
                                    <input type="hidden" name="detail[{{$key}}][id]" value="{{ $data->id }}">
                                    <input type="hidden" name="exists[]" value="{{$data->id}}">
                                    <td scope="row" style="text-align: center;">
                                        <label style="margin-top: 23px;" class="numboor-{{$key+1}}">{{ $key+1 }}</label>
                                    </td>
                                    <td scope="row">
                                        <div class="field">
                                            <textarea class="form-control" name="detail[{{$key}}][deskripsi]" id="exampleFormControlTextarea1" placeholder="Unsur Pemenuhan" rows="2">{{$data->deskripsi}}</textarea>
                                        </div>
                                    </td>
                                    <td style="text-align: center;">
                                    @if($key == 0)
                                        <button class="btn btn-sm btn-success tambah_komponen" type="button" style="border-radius: 20px;margin-top: 19px;"><i class="fa fa-plus"></i></button>
                                    @else
                                        <button class="btn btn-sm btn-danger hapus_soal" type="button" style="border-radius:20px;margin-top: 19px;" data-id="{{$key+1}}"><i class="fa fa-remove"></i></button>
                                    @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <div class="text-right">
                        <button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
                        <button type="button" class="btn btn-simpan save as page">Simpan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@push('js')
    <script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
    <script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script>
        $(document).on('click', '.tambah_komponen', function(e){
            var rowCount = $('#example > tbody > tr').length;
            var c = rowCount-1;
            var html = `
                    <tr class="data-container-`+(c+2)+`" data-id="`+(c+2)+`">
                        <td scope="row" style="text-align: center;">
                            <label style="margin-top: 23px;" class="numboor-`+(c+2)+`">`+(c+2)+`</label>
                        </td>
                        <td scope="row">
                            <div class="field">
                                <textarea class="form-control" name="detail[`+(c+2)+`][deskripsi]" id="exampleFormControlTextarea1" placeholder="Unsur Pemenuhan" rows="2"></textarea>
                            </div>
                        </td>
                        <td style="text-align: center;">
                            <button class="btn btn-sm btn-danger hapus_soal" type="button" style="border-radius:20px;margin-top: 19px;" data-id=`+(c+2)+`><i class="fa fa-remove"></i></button>
                        </td>
                    </tr>
                `;

                $('.container').append(html);
                $('.rencana').datepicker({
                    format: "mm-yyyy",
                    viewMode: "months", 
                    minViewMode: "months",
                    orientation: "auto",
                    autoclose:true
                });

                $('.selectpicker').selectpicker();
        });
        $(document).on('click', '.hapus_soal', function (e){
            var row = $(this).closest('tr');
            row.remove();
            var table = $('#example');
            var rows = table.find('tbody tr');

            $.each(rows, function(key, value){
                console.log($(this).data("id"))
                table.find('.numboor-'+$(this).data("id")).html(key+1);
            });
        });

    </script>
    @yield('js-extra')
@endpush

