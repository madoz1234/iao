@extends('layouts.list')

@section('title', 'Kertas Kerja')

@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection

@section('filters')
    <div class="form-group">
        <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
        <label class="control-label sr-only" for="filter-kerja">Kertas Kerja</label>
        <input type="text" class="form-control filter-control" name="filter[kerja]" data-post="kerja" placeholder="Kertas Kerja">
    </div>
@endsection
@push('scripts')
    <script>
    	$(document).on('click', '.buat.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/buat';
            window.location = url;
        });
    </script>
    @yield('js-extra')
@endpush