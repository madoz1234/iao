<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
    @csrf

    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Tambah Data Kertas Kerja</h5>
    </div>
    <div class="modal-body">
    	<div class="form-group">
            <label class="control-label">SPIN</label>
            <select class="selectpicker form-control" data-size="3" name="spin_id" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
                @foreach(App\Models\Master\SpinPembobotan::where('status', 1)->doesntHave('kertaskerja')->get() as $spin)
                    <option value="{{ $spin->id }}">{{ $spin->version }}</option>
                @endforeach
            </select>                  
        </div>
        <div class="form-group field">
            <label class="control-label">Kategori</label>
            <select class="select selectpicker form-control kategori" name="kategori" data-post="kategori" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
		           <option style="font-size: 12px;" value="1">Business Unit (BU)</option>
		           <option style="font-size: 12px;" value="2">Corporate Office (CO)</option>
		    </select>
        </div>
        <div class="form-group field">
            <label class="control-label">Objek Audit</label>
		    <select class="select selectpicker form-control show-tick object" name="kategori_id" data-size="3" data-style="btn-default" data-live-search="true" title="Objek Audit">
			</select>
        </div>
        <div class="form-group field">
            <label class="control-label">Status</label><br>
            <input type="checkbox" name="status" data-width="100" data-toggle="toggle" data-size="mini" data-on="Aktif" data-off="Nonaktif" data-style="ios">
        </div>
        <div class="form-group field">
            <label class="control-label">Deskripsi</label>
            <textarea class="form-control" name="deskripsi" id="exampleFormControlTextarea1" placeholder="Deskripsi" rows="2"></textarea>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>