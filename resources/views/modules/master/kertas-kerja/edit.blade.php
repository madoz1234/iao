<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
    @method('PATCH')
    @csrf
    <input type="hidden" name="id" value="{{ $record->id }}">
    <input type="hidden" name="object" value="{{ $record->kategori_id }}">
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Ubah Data Kertas Kerja</h5>
    </div>
    <div class="modal-body">
    	<div class="form-group">
            <label class="control-label">SPIN</label>
            <select class="selectpicker form-control" data-size="3" name="spin_id" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
                @foreach(App\Models\Master\SpinPembobotan::where('status', 1)->where(function ($user) use ($record) {
                			$user->where(function ($data) use ($record) {
                				$data->doesntHave('kertaskerja');
                			})->orWhere(function ($data) use ($record) {
                				$data->where('id', $record->spin_id);
                			});
                		})->get() as $spin)
                    <option value="{{ $spin->id }}" @if($spin->id == $record->spin_id) selected @endif>{{ $spin->version }}</option>
                @endforeach
            </select>                  
        </div>
        <div class="form-group field">
            <label class="control-label">Kategori</label>
            <select class="select selectpicker form-control kategori" name="kategori" data-post="kategori" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
		           <option style="font-size: 12px;" value="1" @if($record->kategori == 1) selected @endif>Business Unit (BU)</option>
		           <option style="font-size: 12px;" value="2" @if($record->kategori == 2) selected @endif>Corporate Office (CO)</option>
		    </select>
        </div>
        <div class="form-group field">
            <label class="control-label">Objek Audit</label>
		    <select class="select selectpicker form-control show-tick object" name="kategori_id" data-size="3" data-style="btn-default" data-live-search="true" title="Objek Audit">
			</select>
        </div>
        <div class="form-group field">
            <label class="control-label">Status</label><br>
            <input type="checkbox" name="status" data-width="100" @if($record->status == 1) checked @endif data-toggle="toggle" data-size="mini" data-on="Aktif" data-off="Nonaktif" data-style="ios">
        </div>
        <div class="form-group field">
            <label class="control-label">Deskripsi</label>
            <textarea class="form-control" name="deskripsi" id="exampleFormControlTextarea1" placeholder="Deskripsi" rows="2"></textarea>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>