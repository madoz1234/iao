@extends('layouts.form')
@section('title', 'Buat Kertas Kerja')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
    <div class="panel-body">
        {{-- header --}}
        <div class="panel panel-default data-form">
			<table class="table" style="font-size: 12px;">
				<tbody>
					<tr>
						<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">SPIN Version</td>
						<td style="width:2px;border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;" class="field">
							{{ $record->spin->version }}
						</td>
					</tr>
					<tr>
						<td style="border: 1px #ffffff;">Kategori</td>
						<td style="border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;" class="field">
							@if($record->kategori == 1)
								Business Unit (BU)
							@else
								Corporate Office (CO)
							@endif
						</td>
					</tr>
					<tr>
						<td style="border: 1px #ffffff;">Objek Audit</td>
						<td style="border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;" class="field">
							@if($record->kategori == 1)
								{!! object_audit(0, $record->kategori_id)!!}
							@else
								{!! object_audit(1, $record->kategori_id)!!}
							@endif
						</td>
					</tr>
					<tr>
						<td style="border: 1px #ffffff;">Status</td>
						<td style="border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;" class="field">
							@if($record->status == 1)
								<span class="label label-info">Aktif</span>
							@else
								<span class="label label-default">Nonaktif</span>
							@endif
						</td>
					</tr>
					<tr>
						<td style="border: 1px #ffffff;">Deskripsi</td>
						<td style="border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;" class="field">
							{{ $record->deskripsi }}
						</td>
					</tr>
				</tbody>
			</table>
		</div>
        {{-- header --}}
        <form action="{{ route($routes.'.simpanData', $record->id) }}" method="POST" id="formData">
        	@method('PATCH')
            @csrf
            <input type="hidden" name="id" value="{{ $record->id }}">
            <input type="hidden" name="status">
            <div class="form-row">
                <div class="form-group col-md-12">
                    <table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
                        <thead style="background-color: #f9fafb;">
                            <tr>
                                <th scope="col" style="text-align: center; width: 5%;">#</th>
                                <th scope="col" style="text-align: center; width: 30%;">Point of Focus (PoF)</th>
                                <th scope="col" style="text-align: center; width: 10%;">Bobot (%)</th>
                                <th scope="col" style="text-align: center; width: 8%;">Aksi</th>
                                <th scope="col" style="text-align: center; width: 30%;">Unsur Pemenuhan</th>
                                <th scope="col" style="text-align: center; width: 10%;">Bobot PoF / Atribut</th>
                                <th scope="col" style="text-align: center; width: 10%;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody class="container">
                            @if(count($record->spin->detail) > 0)
                                @foreach($record->spin->detail as $key => $data)
                                    <tr class="data-container-{{$key+1}}" data-id="{{$key+1}}">
                                        <td scope="row" style="text-align: center;">
                                            <label class="numboor-{{$key+1}}">{{ $key+1 }}</label>
                                        </td>
                                        <td scope="row">
                                            <div class="field">
                                                {{ $data->pof->judul }}
                                            </div>
                                        </td>
                                        @php 
                                        @endphp
                                        <td scope="row">
                                            <div class="input-group">
                                                <input type="text" class="form-control sub subtotal-{{$key}}" data-id="{{$key}}" name="subtotal_{{$key}}" placeholder="Bobot" style="text-align: right;" disabled="">
                                                <span class="input-group-addon" style="font-size: 10px;">%</span>
                                            </div>
                                        </td>
                                        <td scope="row" width="20px;">
		                                	 <input type="hidden" name="cek[{{ $key }}][id]" value="{{ $data->id}}">
                                             <input type="checkbox" class="kepala" name="cek[{{ $key }}][status]" data-id="{{ $data->id}}" data-width="100" data-toggle="toggle" @if(getstatus($record->id, $data->id) == 1) checked @endif data-size="mini" data-on="Aktif" data-off="Nonaktif" data-style="ios">
                                        </td>
                                        <td scope="row">
                                            <div class="field">
                                        		@foreach($data->pof->pemenuhan as $kiy => $pemenuhan)
                                                <ol class="list list-more1" style="padding-left: 8px;">
                                        			@foreach($pemenuhan->detail as $kuy => $detil)
                                        				<li class="item">{{readMoreText($detil->deskripsi, 100) }}</li>
                                        			@endforeach
                                                </ol>
                                        		@endforeach
                                            </div>
                                        </td>
                                        <td scope="row">
                                            <div class="field data-bobot-{{$key}}">
                                            @foreach($data->detailNilai as $kiy => $nilai)
                                                 <div class="input-group field">
                                                    <input type="text" style="text-align: right;" class="form-control mask ambil-{{$key}}" data-id="{{$key}}" name="detailNilai[{{$key}}][data][{{$kiy}}][nilai]" placeholder="Bobot" value="{{ number_format($nilai->nilai) }}" disabled=""><br>
                                                    <span class="input-group-addon" style="font-size: 10px;">% </span>
                                                </div>
                                                <br>
                                            @endforeach
                                            </div>
                                        </td>
                                        <td scope="row">
                                        	@foreach($data->detailNilai as $kiy => $nilai)
                                        	 <input type="hidden" class="form-control" name="detail[{{ $data->id}}][data][{{$kiy}}][id]" placeholder="Bobot" value="{{ $nilai->id }}">
                                             <input type="checkbox" class="detil-{{ $data->id}}" data-id="{{$kiy}}" name="detail[{{ $data->id}}][data][{{$kiy}}][status]" data-width="100" @if(getstatusa($record->id, $data->id, $nilai->id) == 1) checked @endif data-toggle="toggle-{{ $data->id}}" data-size="mini" data-on="Aktif" data-off="Nonaktif" data-style="ios"><br><br>
                                            @endforeach
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                            @endif
                        </tbody>
                        <thead style="background-color: #f9fafb;">
                            <tr>
                                <th scope="col" style="text-align: right; width: 30%;" colspan="2">Total :</th>
                                <th scope="col" style="text-align: center; width: 10%;">
                                    <div class="input-group">
                                        <input type="text" style="text-align: right;" class="form-control hasil" id="hasil" name="total" placeholder="Bobot" disabled="">
                                        <span class="input-group-addon" style="font-size: 10px;">%</span>
                                    </div>
                                </th>
                                <th scope="col" style="text-align: right; width: 8%;"></th>
                                <th scope="col" style="text-align: right; width: 30%;">Total :</th>
                                <th scope="col" style="text-align: center; width: 10%;">
                                    <div class="field">
                                        <div class="input-group">
                                            <input type="text" style="text-align: right;" class="form-control" name="total" placeholder="Bobot" disabled="">
                                            <span class="input-group-addon" style="font-size: 10px;">% </span>
                                        </div>
                                    </div>
                                </th>
                                <th scope="col" style="text-align: center; width: 5%;"></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <div class="text-right">
                        <button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
                        <button type="button" class="btn btn-info save-draft page">Simpan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@push('js')
    <script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
    <script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script>
    	var total =0;
		$('.mask').inputmask("decimal", {
            numericInput: true,
	        groupSeparator: '.',
	        radixPoint: ".",
	        digits: 3,
	        autoGroup: true,
	        autoUnmask: true,
	        placeholder: '0,00',
	        androidHack: "rtfm",
	        clearIncomplete: !0,
	        allowMinus: false,
        });

    	$('.sub').each(function(){
            var id = $(this).data("id");
            var sum =0;
            $('.ambil-'+id).each(function(){
                var angka = $(this).val();
                nn = parseFloat(angka);
                sum += nn;
            });
            total += sum;
            $('.subtotal-'+id).val(sum);
        });
        $('input[name="total"]').val(total);
    	$('.kepala').change(function(){
	        var id = $(this).data('id');
		    if($(this).is(':checked')){
		        $('.detil-'+id).removeAttr('disabled');
		        $('.detil-'+id).prop('checked', true).change();
		    }
		    else
		    {
	        	$('.detil-'+id).prop('checked', false).change();
	        	$('.detil-'+id).attr('disabled','disabled');
		    }    

		});
    </script>
    @yield('js-extra')
@endpush
