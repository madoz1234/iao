@extends('layouts.list')

@section('title', 'Project')

@push('styles')
<style>
        .swal-text {
		  padding: 17px;
		  display: block;
		  margin: 22px;
		  text-align: center;
		  color: #61534e;
		}
  .btn-upload {
      color: #ffffff !important;
      background-color: #42b538;
      font-size: 12px;
      font-weight: bold;
      padding-top: 5px;
      padding-bottom: 4px;
      border-radius: 15px;
  }
</style>
@endpush

@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection

@section('filters')
    <div class="form-group">
        <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
        <label class="control-label sr-only" for="filter-name">No AB</label>
        <input type="text" class="form-control filter-control" name="filter[project_ab]" data-post="project_ab" placeholder="No AB">
        <label class="control-label sr-only" for="filter-name">Project ID</label>
        <input type="text" class="form-control filter-control" name="filter[project_id]" data-post="project_id" placeholder="Project ID">
        <label class="control-label sr-only" for="filter-name">Project</label>
        <input type="text" class="form-control filter-control" name="filter[project]" data-post="project" placeholder="Project">
        <label class="control-label sr-only" for="filter-name">Business Unit (BU)</label>
        <select class="select selectpicker filter-control" name="filter[bu]" data-style="btn-default" data-post="bu">
        	<option style="font-size: 12px;" value="" default>Business Unit (BU)</option>
            @foreach(App\Models\Master\BU::get() as $bu)
                <option value="{{ $bu->id }}">{{ $bu->nama }}</option>
            @endforeach
        </select>
    </div>
@endsection

@section('buttons')
<button class="btn m-b-xs btn-add btn-addon add button" style="margin-right: 15px;"><i class="fa fa-plus" style="margin-right: -2px;"></i>Tambah Data</button>
<button class="btn m-b-xs btn-upload btn-addon green custom button" data-url="{{ route($routes.'.import') }}" style="margin-right: 15px;"><i class="fa fa-upload" style="margin-right: -2px;"></i>Upload SAP</button>
@endsection
