<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
    @csrf

    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Tambah Data Project</h5>
    </div>
    <div class="modal-body">
    	<div class="form-group col-md-12" style="padding-right: 5px;">
            <label class="control-label">Business Unit (BU)</label>
            <select class="selectpicker form-control" name="bu_id" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
                @foreach(App\Models\Master\BU::has('detail_pic')->get() as $bu)
                    <option value="{{ $bu->id }}">{{ $bu->nama }}</option>
                @endforeach
            </select>                  
        </div>
        <div class="form-group col-md-6 field" style="padding-right: 5px;">
            <label class="control-label">Project ID</label>
            <input type="text" name="project_id" class="form-control" placeholder="Project ID" required="">
        </div>
        <div class="form-group col-md-6 field" style="padding-left: 5px;">
            <label class="control-label">No AB</label>
            <input type="text" name="project_ab" class="form-control" placeholder="No AB" required="">
        </div>
        <div class="form-group col-md-12 field">
            <label class="control-label">Project</label>
            <input type="text" name="nama" class="form-control" placeholder="Project" required="">
        </div>
        <div class="form-group col-md-6 field" style="padding-right: 5px;">
            <label class="control-label">No Telepon</label>
            <input type="text" name="no_tlp" class="form-control" placeholder="No Telepon" required="">
        </div>
        <div class="form-group col-md-6" style="padding-left: 5px;">
            <label class="control-label">PIC</label>
            <select class="selectpicker form-control" data-size="3" name="pic[]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)" multiple>
                @foreach(App\Models\Auths\User::where('id','<>', 1)->WhereLike('Project')->get() as $user)
                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
            </select>                  
        </div>
        <div class="form-group field">
            <label class="control-label">Alamat</label>
            <textarea class="form-control" name="alamat" id="exampleFormControlTextarea1" placeholder="Alamat" rows="2"></textarea>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>