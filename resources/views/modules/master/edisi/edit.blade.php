<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
    @method('PATCH')
    @csrf
    <input type="hidden" name="id" value="{{ $record->id }}">
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Ubah Data Edisi</h5>
    </div>
    <div class="modal-body">
    	<div class="form-group field">
            <label class="control-label">Edisi</label>
            <input type="text" name="edisi" class="form-control bulan-tahun" placeholder="Edisi" required="" value="{{ $record->edisi }}">
        </div>
        <div class="form-group field">
            <label class="control-label">Revisi</label>
            <input type="text" name="revisi" class="form-control" placeholder="Revisi" required="" value="{{ $record->revisi }}">
        </div>
        <div class="form-group field">
            <label class="control-label">Status</label><br>
            <input type="checkbox" name="status" data-width="100" @if($record->status == 1) checked @endif data-toggle="toggle" data-size="mini" data-on="Aktif" data-off="Nonaktif" data-style="ios">
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>