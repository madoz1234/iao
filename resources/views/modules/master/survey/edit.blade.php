<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
    @method('PATCH')
    @csrf
    <input type="hidden" name="id" value="{{ $record->id }}">
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Ubah Data Survey</h5>
    </div>
    <div class="modal-body">
    	<div class="form-group field">
            <label class="control-label">Version</label>
            <input type="text" name="version" class="form-control" placeholder="Version" required="" value="{{ $record->version }}" @if($record->status > 0) readonly="" @endif>
        </div>
        <div class="form-group field">
            <label class="control-label">Status</label><br>
            <input type="checkbox" name="status" data-width="100" data-toggle="toggle" @if($record->status == 1) checked @endif data-size="mini" data-on="Aktif" data-off="Tidak Aktif" data-style="ios" @if(count($record->pertanyaan) == 0) disabled @endif>
        </div>
        <div class="form-group field">
            <label class="control-label">Deskripsi</label>
            <textarea class="form-control" name="description" id="exampleFormControlTextarea1" placeholder="Deskripsi" rows="3" @if($record->status > 0) readonly @endif>{{$record->description}}</textarea>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan edit-survey button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>