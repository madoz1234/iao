@extends('layouts.form')
@section('title', 'Buat Survey')
@section('side-header')
   <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color: transparent !important;">
        <?php $i=1; $last=count($breadcrumb);?>
         @foreach ($breadcrumb as $name => $link)
            @if($i++ != $last)
                <li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item" aria-current="page"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @endif
         @endforeach
                <li class="breadcrumb-item active" aria-current="page">Buat</li>
      </ol>
    </nav>
@endsection
@section('body')
<div class="panel panel-default">
    <div class="panel-body">
        {{-- header --}}
        <div class="wrapper clearfix form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 text-left font-bold" for="nodok">Version</label>
                <div class="col-sm-4">
                    : {{ $record->version ? $record->version : '-' }}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 text-left font-bold" for="nodok">Deskripsi</label>
                <div class="col-sm-10">
                    : <span>{{ $record->description ? $record->description : '' }}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 text-left font-bold" for="nodok">Dibuat Pada</label>
                <div class="col-sm-4">
                    : {{ $record->created_at ? $record->created_at : '-' }}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 text-left font-bold" for="nodok">Status</label>
                <div class="col-sm-4">
                    @if ($record->status == 1)
                        : <span class="label label-success">Aktif</span>
                    @elseif($record->status == 2)
                        : <span class="label label-danger">Non-Aktif</span>
                    @else
                        : <span class="label label-warning">Belum Aktif</span>
                    @endif
                </div>
            </div>
        </div>
        {{-- header --}}
        <form action="{{ route($routes.'.simpanData', $record->id) }}" method="POST" id="formData">
            @csrf
            <input type="hidden" name="status" value="{{ $record->status }}">
            <input type="hidden" name="id" value="{{ $record->id }}">
            <input type="hidden" name="version" value="{{ $record->version }}">
            <input type="hidden" name="arr_pertanyaan" value="{{ count($record->pertanyaan) > 0 ? count($record->pertanyaan) : 1 }}">

            <div class="form-row">
                <div class="form-group col-md-12">
                    <table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
                        <thead style="background-color: #f9fafb;">
                            <tr>
                                <th scope="col" width="50px" style="text-align: center;">No</th>
                                <th scope="col" style="text-align: center;">Pernyataan</th>
                                <th scope="col" width="65px" style="text-align: center;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody class="container">
                            @if(count($record->pertanyaan) > 0)
                                @foreach($record->pertanyaan as $key => $data)
                                    <tr class="data-container-{{$key+1}}" data-id="{{  $key+1 }}">
                                        <td style="text-align: center;">
                                            <label style="margin-top: 7px;" class="numboor-{{ $key+1 }}">{{ $key+1 }}</label>
                                        </td>
                                        <td scope="row" style="border-bottom: 1px solid rgba(34,36,38,.1);">
                                            <input type="hidden" name="exists[]" value="{{$data->id}}">
                                            <input type="hidden" name="detail[{{$key+1}}][id]" value="{{$data->id}}">
                                            <div class="field">
                                                <textarea class="form-control" id="pertanyaan" name="detail[{{$key+1}}][pertanyaan]" placeholder="Pernyataan" rows="2">{{ $data->pertanyaan }}</textarea>
                                                <input type="hidden" class="custom-control-input" id="{{$key+1}}" name="detail[{{$key+1}}][tipe]" value="1" checked="checked">
                                                <input type="hidden" class="form-control number max" min="2" max="10" name="detail[{{$key+1}}][sum]" id="detail[{{$key}}][sum]" value="5">
                                            </div>
                                        </td>
                                        <td style="text-align: center;border-bottom: 1px solid rgba(34,36,38,.1);">
                                            @if($key == 0)
                                                <button style="border-radius: 20px;" class="btn btn-sm btn-success tambah_komponen" type="button"><i class="fa fa-plus"></i></button>
                                            @else 
                                                <button style="border-radius: 20px;" class="btn btn-sm btn-danger hapus_soal" type="button" data-id="{{ $key+1 }}"><i class="fa fa-remove"></i></button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="data-container-0" data-id="0">
                                    <td style="text-align: center;">
                                        <label style="margin-top: 7px;" class="numboor-1">1</label>
                                    </td>
                                    <td scope="row" style="border-bottom: 1px solid rgba(34,36,38,.1);">
                                        <div class="field">
                                            <textarea class="form-control" id="pertanyaan" name="detail[1][pertanyaan]" placeholder="Pernyataan" rows="2"></textarea>
                                        </div>
                                    </td>
                                    <td style="text-align: center;border-bottom: 1px solid rgba(34,36,38,.1);">
                                        <button class="btn btn-sm btn-success tambah_komponen" style="border-radius: 20px;" type="button"><i class="fa fa-plus"></i></button>
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <div class="text-right">
                        <button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
                        <button type="button" class="btn btn-info save as drafting">Simpan</button>
                        <button type="button" class="btn btn-simpan aktivasi">Aktivasi</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@push('js')
    <script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
    <script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
	    div.dropdown-menu.open{
		  max-width: 300px !important;
		  overflow: hidden;
		}
		ul.dropdown-menu.inner{
		  max-width: 300px !important;
		  overflow-y: auto;
		}
        .swal-text {
            text-align: center;
        }
    </style>
@endpush

@push('scripts')
    <script>
        $(document).on('click', '.tambah_komponen', function(e){
            var arr_pertanyaan = parseInt($('input[name=arr_pertanyaan]').val());
            var rowCount = $('#example > tbody > tr').length;
            var c = rowCount-1;
            var html = `
                    <tr class="data-container-`+(c+2)+`" data-id=`+(c+2)+`>
                        <td style="text-align: center;">
                            <label style="margin-top: 7px;" class="numboor-`+(c+2)+`">`+(c+2)+`</label>
                        </td>
                        <td scope="row" style="border-bottom: 1px solid rgba(34,36,38,.1);">
                            <div class="field">
                                <textarea class="form-control" id="pertanyaan" name="detail[`+(arr_pertanyaan+1)+`][pertanyaan]" placeholder="Pernyataan" rows="2"></textarea>
                            </div>
                        </td>
                        <td style="text-align:center;border-bottom: 1px solid rgba(34,36,38,.1);">
                            <button class="btn btn-sm btn-danger hapus_soal" type="button" style="border-radius: 20px;"  data-id=`+(c+2)+`><i class="fa fa-remove"></i></button>
                        </td>
                    </tr>
                `;
            $('.container').append(html);
            $('input[name=arr_pertanyaan]').val(rowCount+1);
        });
        $(document).on('click', '.hapus_soal', function (e){
            var row = $(this).closest('tr');
            row.remove();

            var table = $('#example');
            var rows = table.find('tbody tr');
            $.each(rows, function(key, value){
                table.find('.numboor-'+$(this).data("id")).html(key+1);
            });
        });

        $(document).on('click', '.aktivasi', function(e){
            $('#formData').find('input[name="status"]').val("1");
            var formDom = "formData";
            if($(this).data("form") !== undefined){
                formDom = $(this).data('form');
            }
            swal({
                    title: "Apakah Anda yakin?",
                    text: "Data yang sudah dikirim, tidak dapat diubah!",
                    icon: "warning",
                    buttons: ['Batal', 'OK'],
                    reverseButtons: true
            }).then((result) => {
                if (result) {
                    saveSurvey(formDom);
                }
            })
        });

        function saveSurvey(form="formData"){
            $('#' + form).find('.loading.dimmer').show();
            $("#"+form).ajaxSubmit({
                success: function(resp){
                    $("#formModal").modal('hide');
                    swal({
                        type: 'success',
                        title: 'Tersimpan!',
                        text: 'Data berhasil disimpan.',
                        icon: "success",
                        buttons: false,
                        timer: 2000
                    }).then((result) => {
                        $('#' + form).find('.loading.dimmer').hide();
                        var url = "{!! route($routes.'.index') !!}";
                        window.location = url;
                        return true;
                    })
                },
                error: function(resp){
                    $('#' + form).find('.loading.dimmer').hide();
                    if (resp.responseJSON.status !== "undefined" && resp.responseJSON.status === 'false'){
                        swal(
                            'Gagal!',
                            resp.responseJSON.message,
                            'error'
                            )
                    }

                    $('#cover').hide();
                    var response = resp.responseJSON;
                    $.each(response.errors, function(index, val) {
                        clearFormError(index,val);
                        showFormError(index,val);
                    });
                    time = 5;
                    interval = setInterval(function(){
                        time--;
                        if(time == 0){
                            clearInterval(interval);
                            $('.pointing.prompt.label.transition.visible').remove();
                            $('.field .error-label').slideUp(500, function(e) {
                                $(this).remove();
                                $('.field.has-error').removeClass('has-error');
                                clearTimeout(interval);
                            });
                        }
                    },1000)
                }
            });
        }

    </script>
    @yield('js-extra')
@endpush
