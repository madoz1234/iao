<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
    @csrf

    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Tambah Data Survey</h5>
    </div>
    <div class="modal-body">
        <div class="form-group field">
            <label class="control-label">Version</label>
            <input type="text" name="version" class="form-control" placeholder="Version" required="">
        </div>
        <div class="form-group field">
            <label class="control-label">Status</label><br>
            <input type="checkbox" name="status" data-width="100" data-toggle="toggle" data-size="mini" data-on="Aktif" data-off="Tidak Aktif" data-style="ios" disabled>
        </div>
        <div class="form-group field">
            <label class="control-label">Deskripsi</label>
            <textarea class="form-control" name="description" id="exampleFormControlTextarea1" placeholder="Deskripsi" rows="3"></textarea>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>