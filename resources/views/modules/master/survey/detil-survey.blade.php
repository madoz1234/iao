@extends('layouts.form')
@section('title', 'Detil Survey')
@section('side-header')
   <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color: transparent !important;">
        <?php $i=1; $last=count($breadcrumb);?>
         @foreach ($breadcrumb as $name => $link)
            @if($i++ != $last)
                <li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item" aria-current="page"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @endif
         @endforeach
                <li class="breadcrumb-item active" aria-current="page">Detail</li>
      </ol>
    </nav>
@endsection
@section('body')
<div class="panel panel-default">
    <div class="panel-body">
        {{-- header --}}
        <div class="wrapper clearfix form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 text-left font-bold" for="nodok">Version</label>
                <div class="col-sm-4">
                    : {{ $record->version ? $record->version : '-' }}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 text-left font-bold" for="nodok">Deskripsi</label>
                <div class="col-sm-10">
                    : <span>{{ $record->description ? $record->description : '' }}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 text-left font-bold" for="nodok">Dibuat Pada</label>
                <div class="col-sm-4">
                    : {{ $record->created_at ? $record->created_at : '-' }}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 text-left font-bold" for="nodok">Status</label>
                <div class="col-sm-4">
                    @if ($record->status == 1)
                        : <span class="label label-success">Aktif</span>
                    @elseif($record->status == 2)
                        : <span class="label label-danger">Non-Aktif</span>
                    @else
                        : <span class="label label-warning">Belum Aktif</span>
                    @endif
                </div>
            </div>
        </div>
        {{-- header --}}
        <form action="{{ route($routes.'.simpanData', $record->id) }}" method="POST" id="formData">
            @csrf
            <input type="hidden" name="status">
            <input type="hidden" name="id" value="{{ $record->id }}">
            <input type="hidden" name="version" value="{{ $record->version }}">

            <div class="form-row">
                <div class="form-group col-md-12">
                    <table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
                        <thead style="background-color: #f9fafb;">
                            <tr>
                                <th scope="col" width="50px" style="text-align: center;">No</th>
                                <th scope="col" style="text-align: center;">Pernyataan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($record->pertanyaan) > 0)
                                @foreach($record->pertanyaan as $key => $data)
                                    <tr>
                                        <td style="text-align: center;">{{ $key+1 }}</td>
                                        <td style="text-align: left;">{{ $data->pertanyaan }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <div class="text-right">
                        <button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@push('js')
    <script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
    <script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
	    div.dropdown-menu.open{
		  max-width: 300px !important;
		  overflow: hidden;
		}
		ul.dropdown-menu.inner{
		  max-width: 300px !important;
		  overflow-y: auto;
		}
    </style>
@endpush

@push('scripts')
    <script>

    </script>
    @yield('js-extra')
@endpush
