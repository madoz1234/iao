@extends('layouts.list')

@section('title', 'Survey')

@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection

@section('filters')
    <div class="form-group">
        <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
        <label class="control-label sr-only" for="filter-version">Version</label>
        <input type="text" class="form-control filter-control" name="filter[version]" data-post="version" placeholder="Version">
    </div>
    {{-- <div class="form-group">
        <label class="control-label sr-only" for="filter-status">Status</label>
        <input type="text" class="form-control filter-control" name="filter[status]" data-post="status" placeholder="Status">
    </div> --}}
    <div class="form-group">
        <label class="control-label sr-only" for="">Kategori</label>
        <select class="selectpicker filter-control" 
                name="filter[status]" 
                data-post="status"
                data-style="btn-default" 
                data-live-search="true"
        >
                <option value="">Status</option>
                <option value="2">Non-Aktif</option>
                <option value="1">Aktif</option>
        </select>
    </div>
@endsection

@push('styles')
    <style>
        .swal-text {
            text-align: center;
        }
    </style>
@endpush

@push('scripts')
    <script>
        $(document).on('click', '.edit-survey.button', function(e){
            editData('formData', function(resp){
                $(modal).modal('hide');
                dt.draw(false);
            });
        });

        function editData(formid, callback){
            // show loading
            $('#' + formid).find('.loading.dimmer').show();
            // begin submit
            $("#" + formid).ajaxSubmit({
                success: function(resp){
                    swal({
                        icon: "success",
                        title:'Berhasil!',
                        text:'Data berhasil disimpan.',
                        button: false,
                        timer: 1000,
                    }).then((result) => { 
                        $('#' + formid).find('.loading.dimmer').hide();
                        callback();
                        dt1 = $('#dataTable1').DataTable();
                        dt1.draw();
                        dt2 = $('#dataTable2').DataTable();
                        dt2.draw();
                        dt3 = $('#dataTable3').DataTable();
                        dt3.draw();
                    })
                },
                error: function(resp){
                    $('#' + formid).find('.loading.dimmer').hide();
                    // $('#cover').hide();
                    if (resp.responseJSON.status !== "undefined" && resp.responseJSON.status === 'false'){
                        swal(
                            'Gagal!',
                            resp.responseJSON.message,
                            'error'
                            )
                    }
                    var response = resp.responseJSON;

                    $.each(response.errors, function(index, val) {
                        var name = index.split('.')
                                        .reduce((all, item) => {
                                            all += (index == 0 ? item : '[' + item + ']');
                                            return all;
                                        });
                        var fg = $('[name="'+ name +'"], [name="'+ name +'[]"]').closest('.form-group');
                        fg.addClass('has-error');
                        fg.append('<small class="control-label error-label font-bold">'+ val +'</small>')
                    });

                    var intrv = setInterval(function(){
                        $('.form-group .error-label').slideUp(500, function(e) {
                            $(this).remove();
                            $('.form-group.has-error').removeClass('has-error');
                            clearTimeout(intrv);
                        });
                    }, 3000)
                }
            });
        }
    </script>
@endpush