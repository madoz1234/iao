<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
    @csrf

    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Tambah Data Standardisasi Temuan</h5>
    </div>
    <div class="modal-body">
    	<div class="form-group field">
            <label class="control-label">Bidang</label>
            <select class="selectpicker form-control" name="bidang" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
                   <option style="font-size: 12px;" value="1">Operasional</option>
                   <option style="font-size: 12px;" value="2">Keuangan</option>
                   <option style="font-size: 12px;" value="3">Sistem</option>
            </select>
        </div>
    	<div class="form-group">
            <label class="control-label">Kode</label>
            <input type="text" name="kode" class="form-control" placeholder="Kode" required="">
        </div>
    	<div class="form-group">
            <label class="control-label">Standardisasi Temuan</label>
            <input type="text" name="standarisasi" class="form-control" placeholder="Standardisasi Temuan" required="">
        </div>
        <div class="form">
            <label class="control-label">Referensi / Kriteria</label>
            <table class="table" style="border-top: 1px #000000;" id="table-data">
            	<tbody class="container-data">
            		<tr style="border-top: 1px #000000;" data-id="0" class="data-0">
            			<td style="border-top: 1px #000000;" class="form-group">
	            			<textarea class="form-control" style="margin-left: -14px;" name="detail[0][deskripsi]" id="exampleFormControlTextarea1" placeholder="Referensi / Kriteria" rows="2"></textarea>
            			</td>
            			<td style="border-top: 1px #000000;width: 5px;">
            				<button class="btn btn-sm btn-success add_deskripsi" type="button" style="margin-top: 17px;margin-right: -2px;margin-left: -19px;"><i class="fa fa-plus"></i></button>
            			</td>
            		</tr>
            		<input type="hidden" name="last" value="0">
            	</tbody>
            </table>
            
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>
    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>