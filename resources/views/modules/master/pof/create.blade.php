<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
    @csrf

    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Tambah Data Point of Focus (PoF)</h5>
    </div>
    <div class="modal-body">
        <div class="form-group field">
            <label class="control-label">Point of Focus (PoF)</label>
            <textarea class="form-control" name="judul" id="exampleFormControlTextarea1" placeholder="Point of Focus (PoF)" rows="2"></textarea>
        </div>
        {{-- <div class="form-group field">
            <label class="control-label">Konten</label>
            <textarea class="form-control" name="deskripsi" id="exampleFormControlTextarea1" placeholder="Konten" rows="2"></textarea>
        </div> --}}
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>