<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
    @method('PATCH')
    @csrf
    <input type="hidden" name="id" value="{{ $record->id }}">
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Ubah Data Point of Focus (PoF)</h5>
    </div>
    <div class="modal-body">
    	<div class="form-group field">
            <label class="control-label">Point of Focus (PoF)</label>
            <textarea class="form-control" name="judul" id="exampleFormControlTextarea1" placeholder="Point of Focus (PoF)" rows="3">{{$record->judul}}</textarea>
        </div>
        {{-- <div class="form-group field">
            <label class="control-label">Deskripsi</label>
            <textarea class="form-control" name="deskripsi" id="exampleFormControlTextarea1" placeholder="Deskripsi" rows="3">{{$record->deskripsi}}</textarea>
        </div> --}}
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>