@extends('layouts.form')
@section('title', 'Ubah Data Uraian')
@section('side-header')
<nav aria-label="breadcrumb">
	<ol class="breadcrumb" style="background-color: transparent !important;">
		<?php $i=1; $last=count($breadcrumb);?>
		@foreach ($breadcrumb as $name => $link)
		@if($i++ != $last)
		<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
		@else
		<li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
		@endif
		@endforeach
	</ol>
</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body">
		<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
			@method('PATCH')
			@csrf
			<input type="hidden" name="id" value="{{ $record->id }}">
			<div class="form-row">
				<div class="form-group col-md-12">
					<table id="table-pelaksanaan" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
						<thead>
							<tr>
								<th style="text-align: center;">No</th>
								<th style="text-align: center;">Elemen</th>
								<th style="text-align: center;">Level</th>
								<th style="text-align: center;" colspan="3">Key Process Area</th>
								<th style="text-align: center;">Uraian / Pernyataan</th>
								<th style="text-align: center;">Penjelasan</th>
								<th style="text-align: center;">Contoh Output / Infrastruktur</th>
								<th style="text-align: center;"></th>
							</tr>
						</thead>
						@php 
						$rowspan=0;$rows=0;$rx=0;$ry=0;$rz=0;$yx=0;$yy=0;$yz=0;
						@endphp
						@foreach($data->first()->level as $key => $value)
							@php
								$rowspan += $value->detail->count();
							@endphp
							@foreach($value->detail as $d)
								@php
								$rx += $d->detiluraian->count();
								@endphp
							@endforeach
						@endforeach
						@php 
							$yy =$rowspan+$rx;
						@endphp
						<tbody class="container-pelaksanaan">
							@foreach($data->first()->level as $key => $value)
								@php 
								$rows = $value->detail->count();
								@endphp
								@if($key == 0)
									@php
										$rz =0;
										$ry = $value->detail->first()->detiluraian->count();
									@endphp
									@foreach($value->detail as $az)
										@php
											$rz += $az->detiluraian->count();	
										@endphp
									@endforeach
									@php 
										$yz =$rows+$rz;
										$yx =($ry+1);
									@endphp
									<tr class="detil-{{ $value->detail->first()->id }}" data-id="{{ $value->detail->first()->id }}">
										<td class="rowspan-kepala-{{ $value->detail->first()->id }}" style="width: 30px;text-align: center;" rowspan="{{ $yy }}">{{ $key+1 }}</td>
										<td class="rowspan-kepala-{{ $value->detail->first()->id }}" style="width: 350px;" rowspan="{{ $yy }}">{!! readMoreText($data->first()->elemen->elemen, 100) !!}</td>
										<td class="rowspan-lvl-{{ $value->detail->first()->id }}" style="text-align: center;width: 40px;" rowspan="{{ $yz }}">{{$value->level}}</td>
										<td class="rowspan-kpa-{{ $value->detail->first()->id }}" style="text-align: center;width: 40px;" rowspan="{{ $yx }}">{{ $key+1 }}.1</td>
										<td class="rowspan-kpa-{{ $value->detail->first()->id }}" style="width: 250px;" rowspan="{{ $yx }}">
											{!! readMoreText($value->detail->first()->kpa, 100) !!}</td>
										<td class="rowspan-kpa-{{ $value->detail->first()->id }}" style="width: 250px;" rowspan="{{ $yx }}">
											{!! readMoreText($value->detail->first()->deskripsi, 100) !!}</td>
										<td style="width: 250px;">
											<textarea class="form-control" name="detail[{{ $value->detail->first()->id }}][uraian]" id="exampleFormControlTextarea1" placeholder="Uraian / Pernyataan" rows="2">{{ $value->detail->first()->uraian }}</textarea>
										</td>
										<td style="width: 250px;">
											<textarea class="form-control" name="detail[{{ $value->detail->first()->id }}][penjelasan]" id="exampleFormControlTextarea1" placeholder="Penjelasan" rows="2">{{ $value->detail->first()->penjelasan }}</textarea>
										</td>
										<td style="width: 250px;">
											<textarea class="form-control" name="detail[{{ $value->detail->first()->id }}][output]" id="exampleFormControlTextarea1" placeholder="Contoh Output / Infrastruktur" rows="2">{{ $value->detail->first()->output }}</textarea>
										</td>
										<td style="width: 20px;">
											<button class="btn btn-outline-success btn-sm tambah_detail" type="button" data-toggle="tooltip" data-placement="left" data-id="{{ $value->detail->first()->id }}" data-detail="{{ $value->detail->first()->id }}" title="Tambah Item" style="border-radius: 20px;"><i class="fa fa-plus"></i></button>
										</td>
										@if(count($value->detail->first()->detiluraian) > 0)
											<input type="hidden" name="last_{{$value->detail->first()->id}}" value="{{ (count($value->detail->first()->detiluraian)-1) }}">
										@else 
											<input type="hidden" name="last_{{$value->detail->first()->id}}" value="0">
										@endif
									</tr>
									@if($value->detail->first()->detiluraian)
										@foreach($value->detail->first()->detiluraian as $koy => $a)
											<tr class="anak-{{$a->kpa_detail_id}} data-{{$a->kpa_detail_id}}-detil-{{$koy}}" data-id="{{$a->kpa_detail_id}}" data-last="{{ $koy }}">
												<td style="">
													<textarea class="form-control" name="detail[{{$a->kpa_detail_id}}][data][{{$koy}}][uraian]" id="exampleFormControlTextarea1" placeholder="Uraian / Pernyataan" rows="2">{{$a->uraian}}</textarea>
												</td>
												<td style="">
													<textarea class="form-control" name="detail[{{$a->kpa_detail_id}}][data][{{$koy}}][penjelasan]" id="exampleFormControlTextarea1" placeholder="Penjelasan" rows="2">{{$a->penjelasan}}</textarea>
												</td>
												<td style="">
													<textarea class="form-control" name="detail[{{$a->kpa_detail_id}}][data][{{$koy}}][output]" id="exampleFormControlTextarea1" placeholder="Contoh Output / Infrastruktur" rows="2">{{$a->output}}</textarea>
												</td>
												<td style="width: 20px;">
													<button class="btn btn-outline-danger btn-sm hapus_detail" type="button" data-id="{{$a->kpa_detail_id}}" data-detail="{{$a->kpa_detail_id}}" data-toggle="tooltip" data-placement="left" title="Hapus Item" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
												</td>
											</tr>
										@endforeach
									@endif
									@foreach($value->detail as $kiy => $detil)
										@if($kiy > 0)
											@if(count($detil->detiluraian) > 0)
												@php 
													$yx = ($detil->detiluraian->count()+1);
												@endphp
											@else 
												@php $yx =1;$yz =1; @endphp
											@endif
											<tr class="detil-{{ $detil->id }}">
												<td class="rowspan-kpa-{{ $detil->id }}" rowspan="{{$yx}}" style="text-align: center;">{{ $key+1 }}.{{ $kiy+1 }}</td>
												<td class="rowspan-kpa-{{ $detil->id }}" rowspan="{{$yx}}" style="">{!! readMoreText($detil->kpa, 100) !!}</td>
												<td class="rowspan-kpa-{{ $detil->id }}" rowspan="{{$yx}}" style="">{!! readMoreText($detil->deskripsi, 100) !!}</td>
												<td style="">
													<textarea class="form-control" name="detail[{{ $detil->id }}][uraian]" id="exampleFormControlTextarea1" placeholder="Uraian / Pernyataan" rows="2">{{ $detil->uraian }}</textarea>
												</td>
												<td style="">
													<textarea class="form-control" name="detail[{{ $detil->id }}][penjelasan]" id="exampleFormControlTextarea1" placeholder="Penjelasan" rows="2">{{ $detil->penjelasan }}</textarea>
												</td>
												<td style="">
													<textarea class="form-control" name="detail[{{ $detil->id }}][output]" id="exampleFormControlTextarea1" placeholder="Contoh Output / Infrastruktur" rows="2">{{ $detil->output }}</textarea>
												</td>
												<td style="width: 20px;">
													<button class="btn btn-outline-success btn-sm tambah_detail" type="button" data-toggle="tooltip" data-placement="left" data-id="{{ $value->detail->first()->id }}" data-detail="{{$detil->id}}" title="Tambah Item" style="border-radius: 20px;"><i class="fa fa-plus"></i></button>
												</td>
												@if(count($detil->detiluraian) > 0)
													<input type="hidden" name="last_{{$detil->id}}" value="{{ (count($detil->detiluraian)-1) }}">
												@else 
													<input type="hidden" name="last_{{$detil->id}}" value="0">
												@endif
											</tr>
											@if(count($detil->detiluraian) > 0)
												@foreach($detil->detiluraian as $kuy => $b)
													<tr class="anak-{{$b->kpa_detail_id}} data-{{$b->kpa_detail_id}}-detil-{{$kuy}}" data-id="{{$b->kpa_detail_id}}" data-last="{{ $kuy }}">
														<td style="">
															<textarea class="form-control" name="detail[{{$b->kpa_detail_id}}][data][{{$kuy}}][uraian]" id="exampleFormControlTextarea1" placeholder="Uraian / Pernyataan" rows="2">{{$b->uraian}}</textarea>
														</td>
														<td style="">
															<textarea class="form-control" name="detail[{{$b->kpa_detail_id}}][data][{{$kuy}}][penjelasan]" id="exampleFormControlTextarea1" placeholder="Penjelasan" rows="2">{{$b->penjelasan}}</textarea>
														</td>
														<td style="">
															<textarea class="form-control" name="detail[{{$b->kpa_detail_id}}][data][{{$kuy}}][output]" id="exampleFormControlTextarea1" placeholder="Contoh Output / Infrastruktur" rows="2">{{$b->output}}</textarea>
														</td>
														<td style="width: 20px;">
															<button class="btn btn-outline-danger btn-sm hapus_detail" type="button" data-id="{{$b->kpa_detail_id}}" data-detail="{{$b->kpa_detail_id}}" data-toggle="tooltip" data-placement="left" title="Hapus Item" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
														</td>
													</tr>
												@endforeach
											@endif
										@endif
									@endforeach
								@else
									@php
										$rz= 0;
										$ry = $value->detail->first()->detiluraian->count();
									@endphp
									@foreach($value->detail as $az)
										@php
											$rz += $az->detiluraian->count();	
										@endphp
									@endforeach
									@php 
										$yz = $rows+$rz;
										$yx =($ry+1);
									@endphp
									<tr class="detil-{{ $value->detail->first()->id }}">
										<td class="rowspan-lvl-{{ $value->detail->first()->id }}" style="text-align: center;" rowspan="{{ $yz }}">{{ $key+1 }}</td>
										<td class="rowspan-kpa-{{ $value->detail->first()->id }}" rowspan="{{$yx}}" style="text-align: center;">{{ $key+1 }}.1</td>
										<td class="rowspan-kpa-{{ $value->detail->first()->id }}" rowspan="{{$yx}}" style="">{!! readMoreText($value->detail->first()->kpa, 100) !!}</td>
										<td class="rowspan-kpa-{{ $value->detail->first()->id }}" rowspan="{{$yx}}" style="">{!! readMoreText($value->detail->first()->deskripsi, 100) !!}</td>
										<td style="">
											<textarea class="form-control" name="detail[{{ $value->detail->first()->id }}][uraian]" id="exampleFormControlTextarea1" placeholder="Uraian / Pernyataan" rows="2">{{ $value->detail->first()->uraian }}</textarea>
										</td>
										<td style="">
											<textarea class="form-control" name="detail[{{ $value->detail->first()->id }}][penjelasan]" id="exampleFormControlTextarea1" placeholder="Penjelasan" rows="2">{{ $value->detail->first()->penjelasan }}</textarea>
										</td>
										<td style="">
											<textarea class="form-control" name="detail[{{ $value->detail->first()->id }}][output]" id="exampleFormControlTextarea1" placeholder="Contoh Output / Infrastruktur" rows="2">{{ $value->detail->first()->output }}</textarea>
										</td>
										<td style="width: 20px;">
											<button class="btn btn-outline-success btn-sm tambah_detail" type="button" data-toggle="tooltip" data-placement="left" data-id="{{ $value->detail->first()->id }}" data-detail="{{ $value->detail->first()->id }}" title="Tambah Item" style="border-radius: 20px;"><i class="fa fa-plus"></i></button>
										</td>
										@if(count($value->detail->first()->detiluraian) > 0)
											<input type="hidden" name="last_{{$value->detail->first()->id}}" value="{{ (count($value->detail->first()->detiluraian)-1) }}">
										@else 
											<input type="hidden" name="last_{{$value->detail->first()->id}}" value="0">
										@endif
									</tr>
									@if($value->detail->first()->detiluraian)
										@foreach($value->detail->first()->detiluraian as $kiy => $c)
											<tr class="anak-{{$c->kpa_detail_id}} data-{{$c->kpa_detail_id}}-detil-{{$kiy}}" data-id="{{$c->kpa_detail_id}}" data-last="{{ $kiy }}">
												<td style="">
													<textarea class="form-control" name="detail[{{$c->kpa_detail_id}}][data][{{$kiy}}][uraian]" id="exampleFormControlTextarea1" placeholder="Uraian / Pernyataan" rows="2">{{$c->uraian}}</textarea>
												</td>
												<td style="">
													<textarea class="form-control" name="detail[{{$c->kpa_detail_id}}][data][{{$kiy}}][penjelasan]" id="exampleFormControlTextarea1" placeholder="Penjelasan" rows="2">{{$c->penjelasan}}</textarea>
												</td>
												<td style="">
													<textarea class="form-control" name="detail[{{$c->kpa_detail_id}}][data][{{$kiy}}][output]" id="exampleFormControlTextarea1" placeholder="Contoh Output / Infrastruktur" rows="2">{{$c->output}}</textarea>
												</td>
												<td style="width: 20px;">
													<button class="btn btn-outline-danger btn-sm hapus_detail" type="button" data-id="{{$c->kpa_detail_id}}" data-detail="{{$c->kpa_detail_id}}" data-toggle="tooltip" data-placement="left" title="Hapus Item" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
												</td>
											</tr>
										@endforeach
									@endif
									@foreach($value->detail as $kuy => $detail)
										@if($kuy > 0)
											@if(count($detail->detiluraian) > 0)
												@php 
													$yx = $detail->detiluraian->count()+1;
												@endphp
											@else 
												@php $yx =1; @endphp
											@endif
											<tr class="detil-{{ $detail->id }}">
												<td class="rowspan-kpa-{{ $detail->id }}" rowspan="{{ $yx }}" style="text-align: center;">{{ $key+1 }}.{{ $kuy+1 }}</td>
												<td class="rowspan-kpa-{{ $detail->id }}" rowspan="{{ $yx }}" style="">{!! readMoreText($detail->kpa, 100) !!}</td>
												<td class="rowspan-kpa-{{ $detail->id }}" rowspan="{{ $yx }}" style="">{!! readMoreText($detail->deskripsi, 100) !!}</td>
												<td style="">
													<textarea class="form-control" name="detail[{{$detail->id}}][uraian]" id="exampleFormControlTextarea1" placeholder="Uraian / Pernyataan" rows="2">{{ $detail->uraian }}</textarea>
												</td>
												<td style="">
													<textarea class="form-control" name="detail[{{$detail->id}}][penjelasan]" id="exampleFormControlTextarea1" placeholder="Penjelasan" rows="2">{{ $detail->penjelasan }}</textarea>
												</td>
												<td style="">
													<textarea class="form-control" name="detail[{{$detail->id}}][output]" id="exampleFormControlTextarea1" placeholder="Contoh Output / Infrastruktur" rows="2">{{ $detail->output }}</textarea>
												</td>
												<td style="width: 20px;">
													<button class="btn btn-outline-success btn-sm tambah_detail" type="button" data-toggle="tooltip" data-placement="left" data-id="{{ $value->detail->first()->id }}" data-detail="{{ $detail->id }}" title="Tambah Item" style="border-radius: 20px;"><i class="fa fa-plus"></i></button>
												</td>
												@if(count($detail->detiluraian) > 0)
													<input type="hidden" name="last_{{$detail->id}}" value="{{ (count($detail->detiluraian)-1) }}">
												@else 
													<input type="hidden" name="last_{{$detail->id}}" value="0">
												@endif
											</tr>
											@if(count($detail->detiluraian) > 0)
												@foreach($detail->detiluraian as $kay => $d)
													<tr class="anak-{{$d->kpa_detail_id}} data-{{$d->kpa_detail_id}}-detil-{{$kay}}" data-id="{{$d->kpa_detail_id}}" data-last="{{ $kay }}">
														<td style="">
															<textarea class="form-control" name="detail[{{$d->kpa_detail_id}}][data][{{$kay}}][uraian]" id="exampleFormControlTextarea1" placeholder="Uraian / Pernyataan" rows="2">{{$d->uraian}}</textarea>
														</td>
														<td style="">
															<textarea class="form-control" name="detail[{{$d->kpa_detail_id}}][data][{{$kay}}][penjelasan]" id="exampleFormControlTextarea1" placeholder="Penjelasan" rows="2">{{$d->penjelasan}}</textarea>
														</td>
														<td style="">
															<textarea class="form-control" name="detail[{{$d->kpa_detail_id}}][data][{{$kay}}][output]" id="exampleFormControlTextarea1" placeholder="Contoh Output / Infrastruktur" rows="2">{{$d->output}}</textarea>
														</td>
														<td style="width: 20px;">
															<button class="btn btn-outline-danger btn-sm hapus_detail" type="button" data-id="{{$d->kpa_detail_id}}" data-detail="{{$d->kpa_detail_id}}" data-toggle="tooltip" data-placement="left" title="Hapus Item" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
														</td>
													</tr>
												@endforeach
											@endif
										@endif
									@endforeach
								@endif
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-right">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
						<button type="button" class="btn btn-simpan save as page">Simpan</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@push('js')
<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
<script>
	var table = $('#table-pelaksanaan');
	var rows = table.find('tbody tr.detail_cek');
	$.each(rows, function(key, value){
		table.find('.numbeer-'+$(this).data("id")).html(key+1);
		table.find('.numbuur-'+$(this).data("id")).html(key+1);
	});

	$(document).on('click', '.tambah_detail', function(e){
		var id = $(this).data("id");
		var detail = $(this).data("detail");
		var rows = table.find('tbody tr.anak-'+detail);
		var data = [];
		$.each(rows, function(key, value){
			var isLastElement = key == rows.length -1;
		    if (isLastElement) {
				var last = $(this).data("last");
				data.push(last);
		    }
		});
		var kepala = parseInt($('.rowspan-kepala-'+id).attr('rowspan'));
		var lvl = parseInt($('.rowspan-lvl-'+id).attr('rowspan'));
		var kpa = parseInt($('.rowspan-kpa-'+detail).attr('rowspan'));
		var rowCount = $('#table-pelaksanaan > tbody > tr.detail_cek').length;
		var row = $('#table-pelaksanaan > tbody > tr.anak-'+detail).length;
		var c = rowCount-1;
		var data_last = parseInt($('input[name="last_'+id+'"]').val())+1;
		var html = `
				<tr class="anak-`+detail+` data-`+detail+`-detil-`+data_last+`" data-id="`+detail+`" data-last="`+data_last+`">
					<td style="">
						<textarea class="form-control" name="detail[`+detail+`][data][`+data_last+`][uraian]" id="exampleFormControlTextarea1" placeholder="Uraian / Pernyataan" rows="2"></textarea>
					</td>
					<td style="">
						<textarea class="form-control" name="detail[`+detail+`][data][`+data_last+`][penjelasan]" id="exampleFormControlTextarea1" placeholder="Penjelasan" rows="2"></textarea>
					</td>
					<td style="">
						<textarea class="form-control" name="detail[`+detail+`][data][`+data_last+`][output]" id="exampleFormControlTextarea1" placeholder="Contoh Output / Infrastruktur" rows="2"></textarea>
					</td>
					<td style="width: 20px;">
						<button class="btn btn-outline-danger btn-sm hapus_detail" type="button" data-id="`+id+`" data-detail="`+detail+`" data-toggle="tooltip" data-placement="left" title="Hapus Item" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
					</td>
				</tr>
		`;
		if(row == 0){
			$('.rowspan-kepala-'+id).attr('rowspan', parseInt(kepala+1));
			$('.rowspan-lvl-'+id).attr('rowspan', parseInt(lvl+1));
			$('.rowspan-kpa-'+detail).attr('rowspan', parseInt(kpa+1));
			$(html).insertAfter('.detil-'+detail).hide().show('slow');
			$('input[name="last_'+id+'"]').val((data_last));
		}else{
			$('.rowspan-kepala-'+id).attr('rowspan', parseInt(kepala+1));
			$('.rowspan-lvl-'+id).attr('rowspan', parseInt(lvl+1));
			$('.rowspan-kpa-'+detail).attr('rowspan', parseInt(kpa+1));
			$(html).insertAfter('.data-'+detail+'-detil-'+(data[0])).hide().show('slow');
			$('input[name="last_'+id+'"]').val((data_last));
		}
	});
	$(document).on('click', '.hapus_detail', function (e){
		var id =$(this).data("id");
		var detil =$(this).data("detail");
		var kepala = parseInt($('.rowspan-kepala-1').attr('rowspan'));
		var lvl = parseInt($('.rowspan-lvl-'+id).attr('rowspan'));
		var kpa = parseInt($('.rowspan-kpa-'+detil).attr('rowspan'));

		var row = $(this).closest('tr');
		row.remove();
		$('.rowspan-kepala-1').attr('rowspan', parseInt(kepala-1));
		$('.rowspan-lvl-'+id).attr('rowspan', parseInt(lvl-1));
		if(kpa == 2){
			$('.rowspan-kpa-'+detil).attr('rowspan', parseInt(kpa-1));
		}else{
			$('.rowspan-kpa-'+detil).attr('rowspan', parseInt(kpa-1));
		}
	});
</script>
@yield('js-extra')
@endpush

