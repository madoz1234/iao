@extends('layouts.form')
@section('title', 'Tambah Data Key Process Area')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body">
		<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
		    @csrf
		    <div class="form-row">
		    	<div class="form-group field col-md-6">
		    		<label class="control-label">Elemen</label>
		            <select class="selectpicker form-control show-tick" name="elemen_id" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
		                @foreach(App\Models\Master\Elemen::doesntHave('kpa')->get() as $elemen)
		                    <option value="{{ $elemen->id }}">{{ $elemen->elemen }}</option>
		                @endforeach
		            </select>    
		    	</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<table id="table-pelaksanaan" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
						<thead>
							<tr>
								<th style="text-align: center;">Level</th>
								<th style="text-align: center; text-align: center;" colspan="3">Key Process Area</th>
								<th style="text-align: center;width: 50px;border-right: 1px solid rgba(34,36,38,.1);border-top: 1px solid rgba(34,36,38,.1);" rowspan="2" colspan="2">
									<button class="btn btn-sm btn-success tambah_pelaksanaan" data-toggle="tooltip" data-placement="left" title="Tambah Pelaksanaan" type="button" style="border-radius: 20px;"><i class="fa fa-plus"></i></button>
								</th>
							</tr>
						</thead>
						<tbody class="container-pelaksanaan">
							<tr class="detail_cek data-pelaksanaan-0 detail-0-0 numbur-0" data-id="0" data-detail="0">
								<td class="data_rowspan-0 field" style="text-align: center;width: 50px;" rowspan="">
									<span class="numbeer-0">1</span>
								</td>
								<td style="text-align: center;width: 30px;" class="field">
									<span class="numbuur-0">1</span>.<span class="numboor-0-0">1</span>
								</td>
								<td style="text-align: center;width: 800px;">
									<div class="field">
										<textarea class="form-control" name="data[0][detail][0][kpa]" id="exampleFormControlTextarea1" placeholder="Key Process Area" rows="2"></textarea>
									</div>
								</td>
								<td style="text-align: center;width: 800px;">
									<div class="field">
										<textarea class="form-control" name="data[0][detail][0][deskripsi]" id="exampleFormControlTextarea1" placeholder="Key Process Area" rows="2"></textarea>
									</div>
								</td>
								<td style="text-align: center;width: 50px;">
									<button class="btn btn-outline-success btn-sm tambah_detail" type="button" data-toggle="tooltip" data-placement="left" data-id="0" title="Tambah Item" style="border-radius: 20px;"><i class="fa fa-plus"></i></button>
								</td>
								<td class="data_rowspan-0" style="text-align: center;width: 50px;border-bottom: 1px solid rgba(34,36,38,.1);" rowspan="">
								</td>
								<input type="hidden" name="last_pelaksanaan" value="0">
								<input type="hidden" name="last_detail_0" value="0">
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-right">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
						<button type="button" class="btn btn-simpan save as page">Simpan</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script>
    	$(document).on('click', '.tambah_pelaksanaan', function(e){
    		var last = parseInt($('input[name=last_pelaksanaan]').val()) + 1;
			var rowCount = $('#table-pelaksanaan > tbody > tr.detail_cek').length;
			var c = rowCount-1;
			var html = `
					<tr class="detail_cek data-pelaksanaan-`+(c+2)+` detail-`+(c+2)+`-0 numbur-`+(c+2)+`" data-id=`+(c+2)+` data-detail="1">
						<td class="data_rowspan-`+(c+2)+` field" style="text-align: center;width: 50px;" rowspan="">
							<span class="numbeer-`+(c+2)+`">`+(c+2)+`</span>
						</td>
						<td style="text-align: center;width: 30px;" class="field">
							<span class="numbuur-`+(c+2)+`">`+(c+2)+`</span>.<span class="numboor-`+(c+2)+`-1">1</span>
						</td>
						<td style="text-align: center;width: 800px;" class="field">
							<div class="field">
								<textarea class="form-control" name="data[`+(c+2)+`][detail][0][kpa]" id="exampleFormControlTextarea1" placeholder="Key Process Area" rows="2"></textarea>
							</div>
						</td>
						<td style="text-align: center;width: 800px;" class="field">
							<div class="field">
								<textarea class="form-control" name="data[`+(c+2)+`][detail][0][deskripsi]" id="exampleFormControlTextarea1" placeholder="Key Process Area" rows="2"></textarea>
							</div>
						</td>
						<td style="text-align: center;width: 50px;">
							<button class="btn btn-outline-success btn-sm tambah_detail" type="button" data-id="`+(c+2)+`" data-toggle="tooltip" data-placement="left" title="Tambah Item" style="border-radius: 20px;"><i class="fa fa-plus"></i></button>
						</td>
						<td class="data_rowspan-`+(c+2)+`" style="text-align: center;width: 50px;" rowspan="">
							<button class="btn btn-sm btn-danger hapus_pelaksanaan" type="button" data-id="`+(c+2)+`" data-toggle="tooltip" data-placement="left" title="Hapus Pelaksanaan" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
						</td>
						<input type="hidden" name="last_detail_`+(c+2)+`" value="0">
					</tr>
				`;

				$('.container-pelaksanaan').append(html);
		        $('input[name=last_pelaksanaan]').val((c+2))
		        $('.btn').tooltip('enable');
		        $('.tanggal').datepicker({
		        	format: 'mm/dd/yyyy',
		        	startDate: '-0d',
		        	orientation: "auto",
		        	autoclose:true,
		        });
		        $('.waktu').clockpicker({
		        	autoclose:true,
		        	'default': 'now',
		        });
		});
		$(document).on('click', '.hapus_pelaksanaan', function (e){
			var id =$(this).data("id");
			var row = $(this).closest('tr');
			row.remove();

			var rowz = $('tr.detail-pelaksanaan-'+id).remove();
			var table = $('#table-pelaksanaan');
			var rows = table.find('tbody tr.detail_cek');
			$.each(rows, function(key, value){
				table.find('.numbeer-'+$(this).data("id")).html(key+1);
				table.find('.numbuur-'+$(this).data("id")).html(key+1);
			});
		});

		$(document).on('click', '.tambah_detail', function(e){
			var id = $(this).data("id");
    		var last1 = parseInt($('input[name=last_pelaksanaan]').val());
    		var last2 = parseInt($('input[name=last_detail_'+id+']').val()) + 1;
			var detil_last = $('#table-pelaksanaan > tbody > tr.detail-pelaksanaan-'+id).last().data("detail");
			var row = $('#table-pelaksanaan > tbody > tr.detail-pelaksanaan-'+id).length;
			var c = row-1;
			if(id == 0){
				var angka = (id+1);
			}else{
				var angka = id;
			}
			var html = `
					<tr class="detail-pelaksanaan-`+id+` detail-`+id+`-`+last2+` numbur-`+id+`" data-id="`+id+`" data-detail="`+last2+`">
						<td style="text-align: center;width: 30px;" class="field">
							<span class="numbuur-`+id+`">`+angka+`</span>.<span class="numboor-`+id+`-`+last2+`">`+(c+3)+`</span>
						</td>
						<td style="text-align: center;width: 800px;" class="field">
							<div class="field">
								<textarea class="form-control" name="data[`+id+`][detail][`+last2+`][kpa]" id="exampleFormControlTextarea1" placeholder="Key Process Area" rows="2"></textarea>
							</div>
						</td>
						<td style="text-align: center;width: 800px;" class="field">
							<div class="field">
								<textarea class="form-control" name="data[`+id+`][detail][`+last2+`][deskripsi]" id="exampleFormControlTextarea1" placeholder="Key Process Area" rows="2"></textarea>
							</div>
						</td>
						<td style="text-align: center;width: 50px;border-right: 1px solid rgba(34,36,38,.1);">
							<button class="btn btn-outline-danger btn-sm hapus_detail" type="button" data-id="`+id+`" data-detail="`+last2+`" data-toggle="tooltip" data-placement="left" title="Hapus Item" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
						</td>
					</tr>
				`;
				$('.data_rowspan-'+id).attr('rowspan', parseInt(row+2));

				if(row == 0){
					$(html).insertAfter('.data-pelaksanaan-'+id).hide().show('slow');
				}else{
					$(html).insertAfter('.detail-'+id+'-'+parseInt(detil_last)).hide().show('slow');
				}
		        $('input[name=last_detail_'+id+']').val(last2)
		        $('.btn').tooltip('enable');
		        $('.tanggal').datepicker({
		        	format: 'mm/dd/yyyy',
		        	startDate: '-0d',
		        	orientation: "auto",
		        	autoclose:true,
		        });
		        $('.waktu').clockpicker({
		        	autoclose:true,
		        	'default': 'now',
		        });
		});
		$(document).on('click', '.hapus_detail', function (e){
			var id = $(this).data("id");
			var rowz = $('#table-pelaksanaan > tbody > tr.detail-pelaksanaan-'+id).length;
			var detail = $(this).data("detail");
			$('.data_rowspan-'+id).attr('rowspan', parseInt(rowz));
			var row = $('tr.detail-'+id+'-'+detail).remove();
			row.remove();
			var table = $('#table-pelaksanaan');
			var numur = $('#table-pelaksanaan > tbody > tr.numbur-'+id);
			$.each(numur, function(key, value){
				table.find('.numboor-'+id+'-'+$(this).data("detail")).html(key+1);
			});
		});
    </script>
    @yield('js-extra')
@endpush

