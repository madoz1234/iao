<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
    @csrf

    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Buat Data Counterpart Auditor Eksternal</h5>
    </div>
    <div class="modal-body">
        <div class="form-group field">
            <label class="control-label">Nama</label>
            <input type="text" name="nama" class="form-control" placeholder="Nama" required="">
        </div>
        <div class="form-group field">
            <label class="control-label">Alamat</label>
            <textarea class="form-control" name="alamat" id="exampleFormControlTextarea1" placeholder="Alamat" rows="2"></textarea>
        </div>
        <div class="form-group field">
            <label class="control-label">No Telepon</label>
            <input type="text" name="no_tlp" class="form-control" placeholder="No Telepon" required="">
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>