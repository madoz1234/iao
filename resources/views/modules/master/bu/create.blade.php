<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
    @csrf

    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Tambah Data Business Unit (BU)</h5>
    </div>
    <div class="modal-body">
    	<div class="form-group field">
            <label class="control-label">Kode BU</label>
            <input type="text" name="kode" class="form-control" placeholder="Kode BU" required="">
        </div>
        <div class="form-group field">
            <label class="control-label">Business Unit</label>
            <input type="text" name="nama" class="form-control" placeholder="Business Unit" required="">
        </div>
        <div class="form-group field">
            <label class="control-label">Alamat</label>
            <textarea class="form-control" name="alamat" id="exampleFormControlTextarea1" placeholder="Alamat" rows="2"></textarea>
        </div>
        <div class="form-group field">
            <label class="control-label">No Telepon</label>
            <input type="text" name="no_tlp" class="form-control" placeholder="No Telepon" required="">
        </div>
        <div class="form-group">
            <label class="control-label">PIC</label>
            <select class="selectpicker form-control" data-size="3" name="pic[]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)" multiple>
                @foreach(App\Models\Auths\User::WhereLike('SVP')->doesntHave('ap_pic')->doesntHave('bu_pic')->doesntHave('co_pic')->get() as $user)
                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
            </select>                  
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>