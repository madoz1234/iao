@extends('layouts.list-grid')

@section('title', 'Langkah Kerja')

@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection

@section('filters')
    <div class="form-group">
        <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
        <label class="control-label sr-only" for="filter-bidang">Bidang</label>
        <select class="select selectpicker filter-control" name="filter[bidang]" data-post="bidang" data-style="btn-default">
               <option style="font-size: 12px;" value="">Bidang</option>
               <option style="font-size: 12px;" value="1">Operasional</option>
               <option style="font-size: 12px;" value="2">Keuangan</option>
               <option style="font-size: 12px;" value="3">Sistem</option>
        </select>
        <label class="control-label sr-only" for="filter-audit">Fokus Audit</label>
        <input type="text" class="form-control filter-control" name="filter[audit]" data-post="audit" placeholder="Fokus Audit">
        <label class="control-label sr-only" for="filter-langkah">Langkah Kerja</label>
        <input type="text" class="form-control filter-control" name="filter[langkah]" data-post="langkah" placeholder="Langkah Kerja">
    </div>
@endsection