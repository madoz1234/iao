@extends('layouts.form')
@section('title', 'Tambah Data Langkah Kerja')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body">
		<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
		    @csrf
		    <div class="form-row">
		    	<div class="form-group field col-md-6">
		    		<label class="control-label">Bidang</label>
		    		<div class="field" style="margin-right: 48px;">
		    			<select class="selectpicker form-control" name="bidang" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
		    				<option value="1">Operasional</option>
		    				<option value="2">Keuangan</option>
		    				<option value="3">Sistem</option>
		    			</select>
		    		</div>
		    	</div>
		    	<div class="form-group field col-md-6">
		    		<label class="control-label">Fokus Audit</label>
		    		<select class="selectpicker form-control" name="fokus" data-style="btn-default" data-live-search="true">
		    			<option value="">( Pilih Salah Satu )</option>
		    		</select>
		    	</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
						<thead style="background-color: #f9fafb;">
							<tr>
								<th scope="col" style="text-align: center;width: 50px;">No</th>
								<th scope="col" style="text-align: center;">Langkah Kerja</th>
								<th scope="col" width="80px" style="text-align: center;">Aksi</th>
							</tr>
						</thead>
						<tbody class="container">
							<tr class="data-container-1" data-id="1">
								<td scope="row" style="text-align: center;">
									<label style="margin-top: 23px;" class="numboor-1">1</label>
								</td>
								<td scope="row">
									<div class="field">
										<textarea class="form-control" name="detail[0][deskripsi]" id="exampleFormControlTextarea1" placeholder="Langkah Kerja" rows="2"></textarea>
									</div>
								</td>
								<td style="text-align: center;">
									<button class="btn btn-sm btn-success tambah_komponen" type="button" style="border-radius: 20px;margin-top: 19px;"><i class="fa fa-plus"></i></button>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-right">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
						<button type="button" class="btn btn-simpan save as page">Simpan</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script>
    	 $('select[name=bidang]').on('change', function(){
			$.ajax({
				url: '{{ url('ajax/option/fokus-audit') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					bidang: this.value
				},
			})
			.done(function(response) {
				$('select[name=fokus]').html(response);
				$('select[name=fokus]').selectpicker("refresh");
			})
			.fail(function() {
				console.log("error");
			});
		})
    	$('.rencana').datepicker({
            format: "mm-yyyy",
		    viewMode: "months", 
		    minViewMode: "months",
            orientation: "auto",
            autoclose:true
        });
    	$(document).on('click', '.tambah_komponen', function(e){
			var rowCount = $('#example > tbody > tr').length;
			var c = rowCount-1;
			var html = `
					<tr class="data-container-`+(c+2)+`" data-id="`+(c+2)+`">
						<td scope="row" style="text-align: center;">
							<label style="margin-top: 23px;" class="numboor-`+(c+2)+`">`+(c+2)+`</label>
						</td>
						<td scope="row">
							<div class="field">
								<textarea class="form-control" name="detail[`+(c+1)+`][deskripsi]" id="exampleFormControlTextarea1" placeholder="Langkah Kerja" rows="2"></textarea>
							</div>
						</td>
						<td style="text-align: center;">
							<button class="btn btn-sm btn-danger hapus_soal" type="button" style="border-radius:20px;margin-top: 19px;" data-id=`+(c+2)+`><i class="fa fa-remove"></i></button>
						</td>
					</tr>
				`;

				$('.container').append(html);
				$('.rencana').datepicker({
		            format: "mm-yyyy",
				    viewMode: "months", 
				    minViewMode: "months",
		            orientation: "auto",
		            autoclose:true
		        });

		        $('.selectpicker').selectpicker();
		});
		$(document).on('click', '.hapus_soal', function (e){
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#example');
			var rows = table.find('tbody tr');

			$.each(rows, function(key, value){
				console.log($(this).data("id"))
				table.find('.numboor-'+$(this).data("id")).html(key+1);
			});
		});

    </script>
    @yield('js-extra')
@endpush

