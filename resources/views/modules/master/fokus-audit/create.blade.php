<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
    @csrf

    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Tambah Data Fokus Audit</h5>
    </div>
    <div class="modal-body">
        <div class="form-group field">
            <label class="control-label">Bidang</label>
            <select class="selectpicker form-control" name="bidang" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
                   <option style="font-size: 12px;" value="1">Operasional</option>
                   <option style="font-size: 12px;" value="2">Keuangan</option>
                   <option style="font-size: 12px;" value="3">Sistem</option>
            </select>
        </div>
        <div class="form-group field">
            <label class="control-label">Fokus Audit</label>
            <textarea class="form-control" name="audit" id="exampleFormControlTextarea1" placeholder="Fokus Audit" rows="2"></textarea>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>