<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
    @method('PATCH')
    @csrf
    <input type="hidden" name="id" value="{{ $record->id }}">
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Ubah Data Fokus Audit</h5>
    </div>
    <div class="modal-body">
    	<div class="form-group field">
            <label class="control-label">Bidang</label>
            <select class="selectpicker form-control" name="bidang" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
                   <option style="font-size: 12px;" value="1" @if($record->bidang == 1) selected @endif>Operasional</option>
                   <option style="font-size: 12px;" value="2" @if($record->bidang == 2) selected @endif>Keuangan</option>
                   <option style="font-size: 12px;" value="3" @if($record->bidang == 3) selected @endif>Sistem</option>
            </select>
        </div>
        <div class="form-group field">
            <label class="control-label">Fokus Audit</label>
            <textarea class="form-control" name="audit" id="exampleFormControlTextarea1" placeholder="Fokus Audit" rows="2">{{$record->audit}}</textarea>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>