@extends('layouts.form')
@section('title', 'Buat Resume')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body" style="padding-bottom: 0px;">
		<form action="{{ route($routes.'.simpanResume', $data->id) }}" method="POST" id="formData">
			@method('PATCH')
			@csrf
			<div class="loading dimmer padder-v" style="display: none;">
			    <div class="loader"></div>
			</div>	
			<input type="hidden" name="id" value="{{ $data->id }}">
			<input type="hidden" name="status" value="0">
			@php 
				$data_operasional = array();
				foreach($record->draftkka->program->detailanggota->where('fungsi', 1) as $operasional) {
				 $data_operasional[] = $operasional->user->name;
				}

				$data_keuangan = array();
				foreach($record->draftkka->program->detailanggota->where('fungsi', 2) as $keuangan) {
				 $data_keuangan[] = $keuangan->user->name;
				}

				$data_sistem = array();
				foreach($record->draftkka->program->detailanggota->where('fungsi', 3) as $sistem) {
				 $data_sistem[] = $sistem->user->name;
				}
			@endphp
			@if($data->ket_svp)
				<p style="padding-left: 14px;color:red;">Status <b>Ditolak Ketua Tim</b> pada {{DateToStringWday($data->updated_at)}}</p>
				<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
					<tr>
						<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
							<h5 style="font-weight: bold;font-size: 12px;">Keterangan :</h5>
							<p style="text-align: justify;text-justify: inter-word;">{{ $data->ket_svp }}</p>
						</td>
					</tr>
				</table>
			@endif
			<div class="form-row">
				<table id="table" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
					<tbody>
						<tr>
							<td style="" colspan="2" rowspan="3">&nbsp;</td>
							<td style="width: 200px !important;text-align: center;" colspan="2">Form. IA 05</td>
						</tr>
						<tr>
							<td style="width: 100px !important;">Edisi : @if($edisi) {{ $edisi->edisi }} @else - @endif</td>
							<td style="width: 100px !important;">Revisi : @if($edisi) {{ $edisi->revisi }} @else - @endif</td>
						</tr>
						<tr>
							<td style="" colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td style="width: 70rem !important;">Nomor :{{ $data->memo }} - LHA/WK/IA/{{ DateToStringWday($data->tanggal) }}</td>
							<td style="width: 70rem !important; text-align: left;" colspan="3">Tanggal : {{ DateToStringWday($data->tanggal) }}</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-bottom: 1px #ffffff;text-transform: uppercase;" colspan="6">
								LAPORAN HASIL AUDIT INTERNAL PADA @if($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object ==2) PROYEK @endif {{ object_audit($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id) }} <br>
								PT. WASKITA KARYA (PERSERO) TBK.</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">RESUME</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											Dari hasil audit yang dilakukan oleh Internal Audit pada {{ DateToStringWday($data->tanggal) }} dengan Internal Memo nomor : {{ $data->memo }}/IM/WK/{{ TahunToString($data->tanggal) }} tanggal {{ DateToStringWday($data->tanggal) }}, dan berlangsung pada tanggal {{ DateToStringWday($data->tanggal) }} kami laporkan hal-hal sbb :
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">A. Aspek Operasional</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											<table id="table-operasional" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
												<thead>
													<th style="width: 50px;">No</th>
													<th style="text-align: center;width: 90%;">Keterangan</th>
													<th style="width: 50px;">Aksi</th>
												</thead>
												<tbody class="container-operasional">
													@if($record->detaillha->where('tipe', 0)->count() > 0)
														@php 
															$i=0;
														@endphp
														@foreach($record->detaillha->where('tipe', 0) as $key => $operasional)
															<tr data-id="0">
																<td style="text-align: center;margin-top: 9px;padding-top: 21px;" class="numboor-0-{{$i}}">{{ $i+1 }}</td>
																<td class="field">
																	<textarea class="form-control" name="detail[0][data][{{$i}}][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="4">{{$operasional->keterangan}}</textarea>
																</td>
																<td style="text-align: center;">
																	@if($i == 0)
																		<button class="btn btn-sm btn-success tambah_operasional" data-id="0" data-detail="{{ $i }}" type="button" style="border-radius: 20px;margin-top: 9px;"><i class="fa fa-plus"></i></button>
																	@else
																		<button class="btn btn-sm btn-danger hapus_operasional" type="button" data-id="0" data-detail="{{ $i }}" data-toggle="tooltip" data-placement="left" title="Hapus Operasional" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
																	@endif
																</td>
																@php 
																$i++;
																@endphp
																<input type="hidden" name="last_operasional" value="{{$i}}">
															</tr>
														@endforeach
													@else
														<tr data-id="0">
															<td style="text-align: center;margin-top: 9px;padding-top: 21px;" class="numboor-0-0">1</td>
															<td class="field">
																<textarea class="form-control" name="detail[0][data][0][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="4"></textarea>
															</td>
															<td style="text-align: center;">
																<button class="btn btn-sm btn-success tambah_operasional" type="button" style="border-radius: 20px;margin-top: 9px;"><i class="fa fa-plus"></i></button>
															</td>
															<input type="hidden" name="last_operasional" value="0">
														</tr>
													@endif
												</tbody>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">B. Aspek Keuangan</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											<table id="table-keuangan" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
												<thead>
													<th style="width: 50px;">No</th>
													<th style="text-align: center;width: 90%;">Keterangan</th>
													<th style="width: 50px;">Aksi</th>
												</thead>
												<tbody class="container-keuangan">
													@if($record->detaillha->where('tipe', 1)->count() > 0)
														@php 
															$j=0;
														@endphp
														@foreach($record->detaillha->where('tipe', 1) as $kiy => $keuangan)
															<tr data-id="1">
																<td style="text-align: center;margin-top: 9px;padding-top: 21px;" class="numboor-1-{{$j}}">{{ $j+1 }}</td>
																<td class="field">
																	<textarea class="form-control" name="detail[1][data][{{$j}}][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="4">{{$keuangan->keterangan}}</textarea>
																</td>
																<td style="text-align: center;">
																	@if($j == 0)
																		<button class="btn btn-sm btn-success tambah_keuangan" data-id="1" data-detail="{{ $j }}" type="button" style="border-radius: 20px;margin-top: 9px;"><i class="fa fa-plus"></i></button>
																	@else
																		<button class="btn btn-sm btn-danger hapus_keuangan" type="button" data-id="1" data-detail="{{ $j }}" data-toggle="tooltip" data-placement="left" title="Hapus Keuangan" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
																	@endif
																</td>
																@php 
																	$j++;
																@endphp
																<input type="hidden" name="last_keuangan" value="{{$j}}">
															</tr>
														@endforeach
													@else
														<tr data-id="1">
															<td style="text-align: center;margin-top: 9px;padding-top: 21px;" class="numboor-1-0">1</td>
															<td class="field">
																<textarea class="form-control" name="detail[1][data][0][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="4"></textarea>
															</td>
															<td style="text-align: center;">
																<button class="btn btn-sm btn-success tambah_keuangan" type="button" style="border-radius: 20px;margin-top: 9px;"><i class="fa fa-plus"></i></button>
															</td>
															<input type="hidden" name="last_keuangan" value="0">
														</tr>
													@endif
												</tbody>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">C. Aspek Sistem</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											<table id="table-sistem" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
												<thead>
													<th style="width: 50px;">No</th>
													<th style="text-align: center;width: 90%;">Keterangan</th>
													<th style="width: 50px;">Aksi</th>
												</thead>
												<tbody class="container-sistem">
													@if($record->detaillha->where('tipe', 2)->count() > 0)
														@php 
															$k=0;
														@endphp
														@foreach($record->detaillha->where('tipe', 2) as $koy => $sistem)
															<tr data-id="2">
																<td style="text-align: center;margin-top: 9px;padding-top: 21px;" class="numboor-2-{{$k}}">{{ $k+1 }}</td>
																<td class="field">
																	<textarea class="form-control" name="detail[2][data][{{$k}}][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="4">{{$sistem->keterangan}}</textarea>
																</td>
																<td style="text-align: center;">
																	@if($k == 0)
																		<button class="btn btn-sm btn-success tambah_sistem" data-id="2" data-detail="{{ $k }}" type="button" style="border-radius: 20px;margin-top: 9px;"><i class="fa fa-plus"></i></button>
																	@else
																		<button class="btn btn-sm btn-danger hapus_sistem" type="button" data-id="2" data-detail="{{ $k }}" data-toggle="tooltip" data-placement="left" title="Hapus Sistem" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
																	@endif
																</td>
																<input type="hidden" name="last_sistem" value="{{$k}}">
															</tr>
															@php 
																$k++;
															@endphp
														@endforeach
													@else
														<tr data-id="2">
															<td style="text-align: center;margin-top: 9px;padding-top: 21px;" class="numboor-2-0">1</td>
															<td class="field">
																<textarea class="form-control" name="detail[2][data][0][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="4"></textarea>
															</td>
															<td style="text-align: center;">
																<button class="btn btn-sm btn-success tambah_sistem" type="button" style="border-radius: 20px;margin-top: 9px;"><i class="fa fa-plus"></i></button>
															</td>
															<input type="hidden" name="last_sistem" value="0">
														</tr>
													@endif
												</tbody>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">D. Kesimpulan</h5>
								<table id="table-kesimpulan" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
									<thead>
										<th style="width: 50px;">No</th>
										<th style="text-align: center;width: 90%;">Keterangan</th>
										<th style="width: 50px;">Aksi</th>
									</thead>
									<tbody class="container-kesimpulan">
										@if($record->detailkesimpulan->count() > 0)
											@php 
												$l=0;
											@endphp
											@foreach($record->detailkesimpulan as $kay => $kesimpulan)
												<tr data-id="2">
													<td style="text-align: center;margin-top: 9px;padding-top: 21px;" class="numbuur-{{$l}}">{{ $l+1 }}</td>
													<td class="field">
														<textarea class="form-control" name="data[{{ $kay }}][kesimpulan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="4">{{$kesimpulan->kesimpulan}}</textarea>
													</td>
													<td style="text-align: center;">
														@if($l == 0)
															<button class="btn btn-sm btn-success tambah_kesimpulan" data-id="2" data-detail="{{ $l }}" type="button" style="border-radius: 20px;margin-top: 9px;"><i class="fa fa-plus"></i></button>
														@else
															<button class="btn btn-sm btn-danger hapus_kesimpulan" type="button" data-id="2" data-detail="{{ $l }}" data-toggle="tooltip" data-placement="left" title="Hapus Sistem" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
														@endif
													</td>
													@php 
														$l++;
													@endphp
													<input type="hidden" name="last_kesimpulan" value="{{$l}}">
												</tr>
											@endforeach
										@else
											<tr data-id="2">
												<td style="text-align: center;margin-top: 9px;padding-top: 21px;" class="numboor-2-0">1</td>
												<td class="field">
													<textarea class="form-control" name="data[0][kesimpulan]" id="exampleFormControlTextarea1" placeholder="Kesimpulan" rows="4"></textarea>
												</td>
												<td style="text-align: center;">
													<button class="btn btn-sm btn-success tambah_kesimpulan" type="button" style="border-radius: 20px;margin-top: 9px;"><i class="fa fa-plus"></i></button>
												</td>
												<input type="hidden" name="last_kesimpulan" value="0">
											</tr>
										@endif
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-right">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
						<button type="button" class="btn btn-primary save as drafting">Save As Draft</button>
						<button type="button" class="btn btn-simpan save as page">Submit</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
        .table-responsive{
            overflow-x: auto;
            overflow-y: visible !important;
        }

        .table-responsive .dropdown-menu {
	        position: static !important;
	    }

	    div.dropdown-menu.open{
		  max-width: 300px !important;
		  overflow: hidden;
		}
		ul.dropdown-menu.inner{
		  max-width: 300px !important;
		  overflow-y: auto;
		}
    </style>
@endpush

@push('scripts')
    <script>
    	$(document).on('click', '.tambah_operasional', function(e){
    		var last = parseInt($('input[name=last_operasional]').val()) + 1;
			var rowCount = $('#table-operasional > tbody > tr').length;
			var c = rowCount-1;
			var html = `
					<tr data-id="`+(c+2)+`">
						<td style="text-align: center;margin-top: 9px;padding-top: 21px;" class="numboor-0-`+(c+2)+`">`+(c+2)+`</td>
						<td class="field">
							<textarea class="form-control" name="detail[0][data][`+(c+2)+`][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="4"></textarea>
						</td>
						<td style="text-align: center;">
							<button class="btn btn-sm btn-danger hapus_operasional" type="button" data-id="0" data-detail="`+(c+2)+`" data-toggle="tooltip" data-placement="left" title="Hapus Operasional" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
						</td>
					</tr>
				`;

				$('.container-operasional').append(html);
		        $('input[name=last_operasional]').val((c+2))
		});
		$(document).on('click', '.hapus_operasional', function (e){
			var id = $(this).data("id");
			var detail =$(this).data("detail");
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#table-operasional');
			var rows = table.find('tbody tr');
			$.each(rows, function(key, value){
				table.find('.numboor-0-'+$(this).data("id")).html((key+1));
			});
		});

		$(document).on('click', '.tambah_keuangan', function(e){
    		var last = parseInt($('input[name=last_keuangan]').val()) + 1;
			var rowCount = $('#table-keuangan > tbody > tr').length;
			var c = rowCount-1;
			var html = `
					<tr data-id="`+(c+2)+`">
						<td style="text-align: center;margin-top: 9px;padding-top: 21px;" class="numboor-1-`+(c+2)+`">`+(c+2)+`</td>
						<td class="field">
							<textarea class="form-control" name="detail[1][data][`+(c+2)+`][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="4"></textarea>
						</td>
						<td style="text-align: center;">
							<button class="btn btn-sm btn-danger hapus_keuangan" type="button" data-id="1" data-detail="`+(c+2)+`" data-toggle="tooltip" data-placement="left" title="Hapus Keuangan" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
						</td>
					</tr>
				`;

				$('.container-keuangan').append(html);
		        $('input[name=last_keuangan]').val((c+2))
		});
		$(document).on('click', '.hapus_keuangan', function (e){
			var id = $(this).data("id");
			var detail =$(this).data("detail");
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#table-keuangan');
			var rows = table.find('tbody tr');
			$.each(rows, function(key, value){
				table.find('.numboor-1-'+$(this).data("id")).html((key+1));
			});
		});

		$(document).on('click', '.tambah_sistem', function(e){
    		var last = parseInt($('input[name=last_sistem]').val()) + 1;
			var rowCount = $('#table-sistem > tbody > tr').length;
			var c = rowCount-1;
			var html = `
					<tr data-id="`+(c+2)+`">
						<td style="text-align: center;margin-top: 9px;padding-top: 21px;" class="numboor-2-`+(c+2)+`">`+(c+2)+`</td>
						<td class="field">
							<textarea class="form-control" name="detail[2][data][`+(c+2)+`][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="4"></textarea>
						</td>
						<td style="text-align: center;">
							<button class="btn btn-sm btn-danger hapus_sistem" type="button" data-id="2" data-detail="`+(c+2)+`" data-toggle="tooltip" data-placement="left" title="Hapus Keuangan" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
						</td>
					</tr>
				`;

				$('.container-sistem').append(html);
		        $('input[name=last_sistem]').val((c+2))
		});
		$(document).on('click', '.hapus_sistem', function (e){
			var id = $(this).data("id");
			var detail =$(this).data("detail");
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#table-sistem');
			var rows = table.find('tbody tr');
			$.each(rows, function(key, value){
				table.find('.numboor-2-'+$(this).data("id")).html((key+1));
			});
		});

		$(document).on('click', '.tambah_kesimpulan', function(e){
    		var last = parseInt($('input[name=last_kesimpulan]').val()) + 1;
			var rowCount = $('#table-kesimpulan > tbody > tr').length;
			var c = rowCount-1;
			var html = `
					<tr data-id="`+(c+2)+`">
						<td style="text-align: center;margin-top: 9px;padding-top: 21px;" class="numbuur-`+(c+2)+`">`+(c+2)+`</td>
						<td class="field">
							<textarea class="form-control" name="data[`+(c+2)+`][kesimpulan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="4"></textarea>
						</td>
						<td style="text-align: center;">
							<button class="btn btn-sm btn-danger hapus_kesimpulan" type="button" data-id="2" data-detail="`+(c+2)+`" data-toggle="tooltip" data-placement="left" title="Hapus Keuangan" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
						</td>
					</tr>
				`;

				$('.container-kesimpulan').append(html);
		        $('input[name=last_kesimpulan]').val((c+2))
		});
		$(document).on('click', '.hapus_kesimpulan', function (e){
			var id = $(this).data("id");
			var detail =$(this).data("detail");
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#table-kesimpulan');
			var rows = table.find('tbody tr');
			$.each(rows, function(key, value){
				table.find('.numbuur-'+$(this).data("id")).html((key+1));
			});
		});
    </script>
    @yield('js-extra')
@endpush

