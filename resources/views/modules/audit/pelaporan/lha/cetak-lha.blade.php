<html>
<head>
<style>
        @font-face {
		    font-family: 'tahoma';
		    src: url({{ storage_path('fonts\tahoma.ttf') }}) format("truetype");
		    font-weight: 400;
		    font-style: normal;
		}

        @page {
            margin: 60px 25px;
            counter-reset: page;
        }

       body {
           margin-top: 3cm;
           margin-left: 1.5cm;
           margin-right: 1.5cm;
           margin-bottom: 2cm;
           font-style: normal;
       }

       .ui.table.bordered {
         border: solid black 1px;
         border-collapse: collapse;
         width: 100%;
         font-family : "tahoma", "Verdana", sans-serif;
       }

       .ui.table.bordered td {
         border: solid black .5px;
         border-collapse: collapse;
         padding:5px;
       }

       .ui.table.bordered td.center.aligned {
         text-align : center;
       }

       .page-break {
            page-break-after: always;
       }

       .pad{
       	padding-top: 5px;
       	padding-bottom: 5px;
       }

        header {
            position: fixed;
            top: -40px;
            left: 51px;
            right: 74px;
            height: 10px;
            text-align: center;
            font-size: 12px;
        }
		
       main {
         position: sticky;
         font-size : 12px;
         padding-top : -1cm;
         margin-top : 2px;
         border : none;
         width: 100%;
       }

       main p {
         margin-left: 5px;
         margin-right: 5px;
       }
       footer {
           position: fixed;
           bottom: -60px;
           left: 0px;
           right: 0px;
           height: 50px;
           text-align: center;
           line-height: 35px;
       }
       table.page_content {width: 100%; border: none; padding: 1mm; font-size: 12px;font-family : "tahoma", "Verdana", sans-serif;}
       table.page_header {width: 100%; border: none; padding: 2mm; font-size: 12px;font-family : "tahoma", "Verdana", sans-serif; text-align: left; margin-left: 50px;margin-top: 50px;}
       table.page_content_header {width: 100%; font-size: 12px;border-collapse: collapse;font-family : "tahoma", "Verdana", sans-serif;text-align: center; padding-bottom: 1px;
       	padding-top: 1px;}
</style>
</head>
<body>
	<header>
		<table border="1" class="page_content ui table bordered" style="page-break-inside: auto;">
			<tbody>
				<tr>
					<td style="width: 40%;border-top: 1px solid #ffffff;border-left: 1px solid #ffffff;padding: -2px;" colspan="2" rowspan="2">&nbsp;</td>
					<td style="text-align: center;padding: -2px;padding: -2px;" colspan="2">Form. IA 05</td>
				</tr>
				<tr>
					<td style="text-align: center;border-bottom: 1px solid #000000;padding: -2px;">Edisi : @if($edisi) {{ $edisi->edisi }} @else - @endif</td>
					<td style="text-align: center;border-bottom: 1px solid #000000;padding: -2px;">Revisi : @if($edisi) {{ $edisi->revisi }} @else - @endif</td>
				</tr>
				<tr>
					<td style="border-right: 1px solid #ffffff;border-top: 1px solid #ffffff;border-left: 1px solid #ffffff;padding-top: -12px;" colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<td style="width: 50%;padding: -2px;padding-left: 2px;padding-bottom: 1px;padding-top: 1px;">Nomor : {{ $records->memo }} - LHA/WK/IA/2020</td>
					<td style="width: 50%;padding: -2px;padding-left: 2px;padding-bottom: 1px;padding-top: 1px;" colspan="3">Tanggal : {{ DateToStringWday($records->tanggal) }}</td>
				</tr>
			</tbody>
		</table>
	</header>
	@php 
		$hitung = 0;
		$data_operasional = array();
		foreach($records->draftkka->program->detailanggota->where('fungsi', 1) as $operasional) {
			if($records->draftkka->program->user->name == $operasional->user->name){
				$hitung+=1;
			}else{
			 	$data_operasional[] = $operasional->user->name;
			}
		}

		$data_keuangan = array();
		foreach($records->draftkka->program->detailanggota->where('fungsi', 2) as $keuangan) {
			if($records->draftkka->program->user->name == $keuangan->user->name){
				$hitung+=1;
			}else{
				$data_keuangan[] = $keuangan->user->name;
			}
		}

		$data_sistem = array();
		foreach($records->draftkka->program->detailanggota->where('fungsi', 3) as $sistem) {
			if($records->draftkka->program->user->name == $sistem->user->name){
				$hitung+=1;
			}else{
			  $data_sistem[] = $sistem->user->name;
			}
		}
		$result = array_merge(array_unique($data_operasional), array_unique($data_keuangan), array_unique($data_sistem));
	@endphp
  	<br>
	<main style="padding-top: -100px;">
		<table class="page_content_header table table-bordered m-t-none" style="font-size: 12px;width: 100%;padding:5px;">
			<tbody>
				<tr>
					<td style="border-right: #ffffff;font-weight:bold;text-transform: uppercase;width: 100%;">LAPORAN HASIL AUDIT INTERNAL PADA @if($records->draftkka->program->penugasanaudit->rencanadetail->tipe_object ==2) PROYEK @endif {{ object_audit($records->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $records->draftkka->program->penugasanaudit->rencanadetail->object_id) }} No. AB : {{ getAb($records->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $records->draftkka->program->penugasanaudit->rencanadetail->object_id) }}</td>
				</tr>
				<tr>
					<td style="border-right: #ffffff;font-weight:bold;text-transform: uppercase;width: 100%;">PT. WASKITA KARYA (PERSERO) TBK.</td>
				</tr>
				<tr>
					<td style="text-align: left;">
						<p style="font-weight: bold;">I.<label style="padding-left: 17px;">Dasar Penugasan Audit</label></p>
						<p style="padding-left: 25px;padding-top: -15px;">Berdasarkan Internal Memo Nomor : {{$records->memo}}</p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -25px;text-align: left;">
						<p style="font-weight: bold;">II.<label style="padding-left: 11px;">Tujuan dan Lingkup Audit</label></p>
						<p style="padding-left: 25px;padding-top: -15px;text-align: justify;">{{ $records->draftkka->program->sasaran }}.<br>
									Lingkup Audit Meliputi : {{ $records->draftkka->program->ruang_lingkup }}sadsadas asd asd asd asd asd asdsadas asd asd asd asd</p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -25px;text-align: left;">
						<p style="font-weight: bold;">III.<label style="padding-left: 8px;">Standar Audit</label></p>
						<p style="padding-left: 25px;padding-top: -15px;text-align: justify;">Audit telah dilakukan dengan Standar Audit yang diterbitkan oleh Asosiasi Auditor Internal yang meliputi Standar Umum, Standar Pelaksanaan, Standar Pelaporan dan Standar Tindak Lanjut.<br>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -25px;text-align: left;">
						<p style="font-weight: bold;">IV.<label style="padding-left: 8px;">Auditor</label></p>
						<p style="padding-left: 25px;padding-top: -15px;text-align: justify;">Ketua Tim &nbsp;&nbsp;: {{ $records->draftkka->program->user->name }} @if($hitung > 0)(Ketua Merangkap Anggota)@endif <br>
						Anggota &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{ implode(", ",$result) }} <br>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -25px;text-align: left;">
						<p style="font-weight: bold;">V.<label style="padding-left: 13px;">Periode Pelaksanaan Audit</label></p>
						<p style="padding-left: 23px;padding-top: -15px;text-align: justify;">Audit dilaksanakan pada {{ DateToString($records->draftkka->program->detailpelaksanaan->first()->tgl).' - '.DateToString($records->draftkka->program->detailpelaksanaan->last()->tgl) }}<br>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -25px;text-align: left;">
						<p style="font-weight: bold;">VI.<label style="padding-left: 8px;">Metodologi Audit</label></p>
						<p style="padding-left: 24px;padding-top: -15px;text-align: justify;">
							Metodologi Audit dilaksanakan sesuai dengan Program Kerja Tahunan Internal Audit Tahun 2019 dengan menggunakan metode sampling dengan tahapan sebagai berikut :
							<ol class="list-group" style="padding-left: 45px;padding-top: -10px;">
								<li class="list item">Perencanaan</li>
								<li class="list item">Pelaksanaan
									<ol type="a" style="padding-left: 15px;">
										<li>Prosedur Analitis</li>
										<li>Pengendalian Intern</li>
										<li>Substansi</li>
										<li>Tanggapan Auditee</li>
									</ol>
								</li>
								<li class="list item">Pelaporan</li>
							</ol>
					</td>
				</tr>
				@if($records->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 2)
				<tr>
					<td style="padding-top: -25px;text-align: left;">
						<p style="font-weight: bold;">VII.<label style="padding-left: 8px;">Data Umum Objek Audit</label></p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -20px;text-align: left;">
						<table style="font-size: 12px;padding: 0.5px;text-align: left;" class="ui table bordered">
							<thead>
								<tr style="text-align: left;">
									<th style="text-align: left;background-color: #cccccc;" colspan="3">GAMBARAN UMUM PROYEK</th>
								</tr>
							</thead>
							<tbody>
								<tr style="">
									<td style="width: 150px;">Nama Proyek</td>
									<td style="width: 8px;">:</td>
									<td style="text-align: left;">{{ object_audit($records->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $records->draftkka->program->penugasanaudit->rencanadetail->object_id) }}</td>
								</tr>
								<tr style="">
									<td style="width: 150px;">Lokasi Proyek</td>
									<td style="width: 8px;">:</td>
									<td style="text-align: left;">{{ getlokasi($records->draftkka->program->penugasanaudit->rencanadetail->object_id) }}</td>
								</tr>
								<tr style="">
									<td style="width: 150px;">Pemilik Proyek</td>
									<td style="width: 8px;">:</td>
									<td style="text-align: left;">
										{{$records->pemilik}}
									</td>
								</tr>
								<tr style="">
									<td style="width: 150px;">Sumber Dana</td>
									<td style="width: 8px;">:</td>
									<td style="text-align: left;">
										{{$records->sumber}}
									</td>
								</tr>
								<tr style="">
									<td style="width: 150px;">Sifat Kontrak</td>
									<td style="width: 8px;">:</td>
									<td style="text-align: left;">
										{{$records->sifat}}
									</td>
								</tr>
								<tr style="">
									<td style="width: 150px;">Nomor SPMK</td>
									<td style="width: 8px;">:</td>
									<td style="text-align: left;">
										{{$records->nomor_spmk}}
									</td>
								</tr>
								<tr style="">
									<td style="width: 150px;">Nilai Kontrak (include PPN)</td>
									<td style="width: 8px;">:</td>
									<td style="text-align: left;">
										{{$records->nilai_kontrak}}
									</td>
								</tr>
								<tr style="">
									<td style="width: 150px;">Tanggal Kontrak</td>
									<td style="width: 8px;">:</td>
									<td style="text-align: left;">
										{{ DateToStringWday($records->tanggal_kontrak) }}
									</td>
								</tr>
								<tr style="">
									<td style="width: 150px;">Tanggal Akhir Kontrak</td>
									<td style="width: 8px;">:</td>
									<td style="text-align: left;">
										{{ DateToStringWday($records->tanggal_akhir_kontrak) }}
									</td>
								</tr>
								<tr style="">
									<td style="width: 150px;">Waktu Pelaksanaan</td>
									<td style="width: 8px;">:</td>
									<td style="text-align: left;">
										{{$records->waktu_pelaksanaaan}}
									</td>
								</tr>
								<tr style="">
									<td style="width: 150px;">Waktu Pemeliharaan</td>
									<td style="width: 8px;">:</td>
									<td style="text-align: left;">
										{{$records->waktu_pemeliharaan}}
									</td>
								</tr>
								<tr style="">
									<td style="width: 150px;">Pembayaran</td>
									<td style="width: 8px;">:</td>
									<td style="text-align: left;">
										{{$records->pembayaran}}
									</td>
								</tr>
								<tr style="">
									<td style="width: 150px;">Konsultan Perencana</td>
									<td style="width: 8px;">:</td>
									<td style="text-align: left;">
										{{$records->konsultan_perencana}}
									</td>
								</tr>
								<tr style="">
									<td style="width: 150px;">Konsultan Pengawas</td>
									<td style="width: 8px;">:</td>
									<td style="text-align: left;">
										{{$records->konsultan_pengawas}}
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td style="text-align: left;">
						<table style="font-size: 12px;" class="ui table bordered">
							<thead>
								<tr style="">
									<th style="text-align: left;background-color: #cccccc;" colspan="4">ADENDUM</th>
								</tr>
							</thead>
							<tbody>
								@foreach($records->detailadendum as $key => $adendum)
									<tr data-id="{{$key}}">
										<td style="width: 8px;" rowspan="4">{{($key+1)}}.</td>
										<td style="width: 160px;">Nomor Kontrak Adendum</td>
										<td style="width: 8px;">:</td>
										<td style="text-align: left;">
											{{$adendum->no_kontrak}}
										</td>
									</tr>
									<tr>
										<td style="width: 160px;">Nilai Kontrak (include PPN)</td>
										<td style="width: 8px;">:</td>
										<td style="text-align: left;">
											{{$adendum->nilai}}
										</td>
									</tr>
									<tr>
										<td style="width: 160px;">Tanggal Kontrak Adendum</td>
										<td style="width: 8px;">:</td>
										<td style="text-align: left;">
											{{ DateToStringWday($adendum->tgl_awal)}}
										</td>
									</tr>
									<tr>
										<td style="width: 160px;">Tanggal Akhir Kontrak Adendum</td>
										<td style="width: 8px;">:</td>
										<td style="text-align: left;">
											{{ DateToStringWday($adendum->tgl_akhir)}}
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td style="text-align: left;">
						<table style="font-size: 12px;" class="ui table bordered">
							<thead>
								<tr style="">
									<th style="text-align: left;background-color: #cccccc;" colspan="3">KINERJA S/D BULAN JANUARI {{$records->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun }}</th>
								</tr>
							</thead>
							<tbody>
								<tr style="">
									<td style="width: 200px;">BK/PU s/d {{$records->tgl_bkpu}}</td>
									<td style="width: 8px;">:</td>
									<td style="text-align: left;">
										{{$records->bkpu}}
									</td>
								</tr>
								<tr style="">
									<td style="width: 200px;">Progress s/d {{$records->tgl_progress}}</td>
									<td style="width: 8px;">:</td>
									<td style="text-align: left;">
										{{$records->progress}}
									</td>
								</tr>
								<tr style="">
									<td style="width: 200px;">PU s/d {{$records->tgl_pu}}</td>
									<td style="width: 8px;">:</td>
									<td style="text-align: left;">
										{{$records->pu}}
									</td>
								</tr>
								<tr style="">
									<td style="width: 200px;">Cash s/d {{$records->tgl_cash}}</td>
									<td style="width: 8px;">:</td>
									<td style="text-align: left;">
										{{$records->cash}}
									</td>
								</tr>
								<tr style="">
									<td style="width: 200px;">Piutang s/d {{$records->tgl_piutang}}</td>
									<td style="width: 8px;">:</td>
									<td style="text-align: left;">
										{{$records->piutang}}
									</td>
								</tr>
								<tr style="">
									<td style="width: 200px;">Tagihan Bruto s/d {{$records->tgl_tagihan}}</td>
									<td style="width: 8px;">:</td>
									<td style="text-align: left;">
										{{$records->tagihan}}
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td style="text-align: left;">
						<p style="font-weight: bold;">VIII.<label style="padding-left: 8px;">Ruang Lingkup Pekerjaan</label></p>
					</td>
				</tr>
				<tr>
					<td>
						<table style="font-size: 11px;" class="ui table bordered">
							<thead>
								<tr>
									<td style="width: 8px !important;font-weight: bold;" rowspan="3">NO</td>
									<td style="width: 150px !important;font-weight: bold;" rowspan="3">URAIAN</td>
									<td style="font-weight: bold;" colspan="3">BOBOT</td>
								</tr>
								<tr>
									<td style="width: 15px !important;font-weight: bold;" rowspan="2">KONTRAK</td>
									<td style="width: 150px !important;font-weight: bold;" colspan="2">BULAN INI</td>
								</tr>
								<tr>
									<td style="width: 150px !important;font-weight: bold;">RENCANA</td>
									<td style="width: 150px !important;font-weight: bold;">REALISASI</td>
								</tr>
							</thead>
							<tbody>
								@foreach($records->detaillingkup as $keys => $lingkup)
									<tr class="data-lingkup-{{$keys}}" data-id={{$keys}}>
										<td style="text-align: center;">
											<label>{{$keys+1}}</label>
										</td>
										<td style="">
											{{$lingkup->uraian}}
										</td>
										<td style="">
											{{$lingkup->kontrak}}
										</td>
										<td style="">
											{{$lingkup->rencana}}
										</td>
										<td style="">
											{{$lingkup->realisasi}}
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</td>
				</tr>
				@endif
				<tr>
					<td style="padding-top: -10px;text-align: left;">
						<p style="font-weight: bold;">@if($records->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 2) IX.@else VII.@endif<label style="padding-left: 4px;">Hasil Audit</label></p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -25px;text-align: left;">
						<p style="font-weight: bold;">A.<label style="padding-left: 4px;">Aspek Operasional</label></p>
					</td>
				</tr>
				{{-- OPERASIONAL --}}
				@php 
					$i=0;
				@endphp
				@foreach($records->draftkka->detaildraft->where('tipe', 0) as $key => $operasional)
				<tr>
					<td style="padding-top: -15px;">
						<p style="padding-left: 24px;padding-top: -15px;text-align: justify;font-weight: bold;">{{$key+1}}. {{getstandarisasis($operasional->standarisasi_id)}}</p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -15px;">
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;font-weight: bold;">Kondisi:</p>
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;">{{ $operasional->catatan_kondisi }}</p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -15px;">
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;font-weight: bold;">Kriteria:</p>
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;">
							@foreach(getkriteria($operasional->standarisasi_id) as $keys => $data)
							&nbsp;&nbsp;{{$keys+1}}. {{ $data->deskripsi }}<br>
							@endforeach
						</p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -15px;">
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;font-weight: bold;">Sebab : (Man, Machine, Method, Material, Money, dan termasuk pengendalian intern)</p>
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;">
							{{ $operasional->sebab }}
						</p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -15px;">
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;font-weight: bold;">Risiko :</p>
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;">
							{{ $operasional->risiko }}
						</p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -15px;">
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;font-weight: bold;">Kategori Temuan :</p>
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;">
							{!! getkategoristatus($operasional->kategori_id) !!}
						</p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -15px;">
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;font-weight: bold;">Rekomendasi dan target tindaklanjut :</p>
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;">
							@foreach($operasional->rekomendasi as $x => $datas)
								{{$x+1}}. {{$datas->rekomendasi}}<br>
							@endforeach
						</p>
					</td>
				</tr>
				@php 
					$i++;
				@endphp
				@endforeach
				{{-- KEUANGAN --}}
				<tr>
					<td style="padding-top: -25px;text-align: left;">
						<p style="font-weight: bold;">B.<label style="padding-left: 4px;">Aspek Keuangan</label></p>
					</td>
				</tr>
				@php 
					$j=0;
				@endphp
				@foreach($records->draftkka->detaildraft->where('tipe', 1) as $key => $keuangan)
				<tr>
					<td style="padding-top: -15px;">
						<p style="padding-left: 24px;padding-top: -15px;text-align: justify;font-weight: bold;">{{$j+1}}. {{getstandarisasis($keuangan->standarisasi_id)}}</p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -15px;">
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;font-weight: bold;">Kondisi:</p>
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;">{{ $keuangan->catatan_kondisi }}</p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -15px;">
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;font-weight: bold;">Kriteria:</p>
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;">
							@foreach(getkriteria($keuangan->standarisasi_id) as $keys => $data)
							&nbsp;&nbsp;{{$keys+1}}. {{ $data->deskripsi }}<br>
							@endforeach
						</p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -15px;">
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;font-weight: bold;">Sebab : (Man, Machine, Method, Material, Money, dan termasuk pengendalian intern)</p>
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;">
							{{ $keuangan->sebab }}
						</p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -15px;">
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;font-weight: bold;">Risiko :</p>
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;">
							{{ $keuangan->risiko }}
						</p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -15px;">
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;font-weight: bold;">Kategori Temuan :</p>
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;">
							{!! getkategoristatus($keuangan->kategori_id) !!}
						</p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -15px;">
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;font-weight: bold;">Rekomendasi dan target tindaklanjut :</p>
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;">
							@foreach($keuangan->rekomendasi as $x => $datas)
								{{$x+1}}. {{$datas->rekomendasi}}<br>
							@endforeach
						</p>
					</td>
				</tr>
				@php 
					$j++;
				@endphp
				@endforeach

				{{-- SISTEM --}}
				<tr>
					<td style="padding-top: -25px;text-align: left;">
						<p style="font-weight: bold;">C.<label style="padding-left: 4px;">Aspek Sistem</label></p>
					</td>
				</tr>
				@php 
					$k=0;
				@endphp
				@foreach($records->draftkka->detaildraft->where('tipe', 2) as $key => $sistem)
				<tr>
					<td style="padding-top: -15px;">
						<p style="padding-left: 24px;padding-top: -15px;text-align: justify;font-weight: bold;">{{$k+1}}. {{getstandarisasis($sistem->standarisasi_id)}}</p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -15px;">
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;font-weight: bold;">Kondisi:</p>
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;">{{ $sistem->catatan_kondisi }}</p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -15px;">
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;font-weight: bold;">Kriteria:</p>
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;">
							@foreach(getkriteria($sistem->standarisasi_id) as $keys => $data)
							&nbsp;&nbsp;{{$keys+1}}. {{ $data->deskripsi }}<br>
							@endforeach
						</p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -15px;">
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;font-weight: bold;">Sebab : (Man, Machine, Method, Material, Money, dan termasuk pengendalian intern)</p>
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;">
							{{ $sistem->sebab }}
						</p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -15px;">
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;font-weight: bold;">Risiko :</p>
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;">
							{{ $sistem->risiko }}
						</p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -15px;">
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;font-weight: bold;">Kategori Temuan :</p>
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;">
							{!! getkategoristatus($sistem->kategori_id) !!}
						</p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: -15px;">
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;font-weight: bold;">Rekomendasi dan target tindaklanjut :</p>
						<p style="padding-left: 36px;padding-top: -15px;text-align: justify;">
							@foreach($sistem->rekomendasi as $x => $datas)
								{{$x+1}}. {{$datas->rekomendasi}}<br>
							@endforeach
						</p>
					</td>
				</tr>
				@php 
					$k++;
				@endphp
				@endforeach
				<tr>
					<td style="padding-top: 10px;">
						<p style="padding-top: -15px;text-align: justify;font-weight: bold;">SVP Internal Audit</p>
					</td>
				</tr>
				<tr>
					<td style="padding-top: 60px;">
						<p style="padding-top: -15px;text-align: justify;font-weight: bold;">(Pius Sutrisno Riyanto)</p>
					</td>
				</tr>
			</tbody>
		</table>
		@if(count(getfileskka($records->draftkka->detaildraft->pluck('id')->toArray())) > 0)
		<table border="1" class="page_content ui table" style="padding:5px;">
			<tbody>
				<tr style="">
					<td style="text-align: left;border: 1px #ffffff;" colspan="2">
						<table class="ui table bordered" style="margin-left: -70px;margin-right: -70px;padding-top: -70px;">
							<tbody>
								<tr>
									<td style="text-align: center;font-weight: bold;" colspan="6">FORM DOKUMENTASI AUDIT INTERNAL</td>
								</tr>
								<tr>
									<td style="text-align: right;border-top: 1px solid #ffffff;border-right: 1px solid #ffffff;">Proyek</td>
									<td style="width: 5px;border-top: 1px solid #ffffff;border-right: 1px solid #ffffff;">:</td>
									<td style="border-top: 1px solid #ffffff;" colspan="4">
										{{ object_audit($records->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $records->draftkka->program->penugasanaudit->rencanadetail->object_id) }}
									</td>
								</tr>
								<tr>
									<td style="text-align: right;border-top: 1px solid #ffffff;border-right: 1px solid #ffffff;">No AB</td>
									<td style="width: 5px;border-top: 1px solid #ffffff;border-right: 1px solid #ffffff;">:</td>
									<td style="border-top: 1px solid #ffffff;border-right: 1px solid #ffffff;">
										@if($records->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 2)
											{{ getAb($records->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $records->draftkka->program->penugasanaudit->rencanadetail->object_id) }}
										@else
											-
										@endif
									</td>
									<td style="text-align: right;border-top: 1px solid #ffffff;border-right: 1px solid #ffffff;">Divisi</td>
									<td style="width: 5px;border-top: 1px solid #ffffff;border-right: 1px solid #ffffff;">:</td>
									<td style="border-top: 1px solid #ffffff;" width="100%">
										@if($records->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 2)
											{{ findbus($records->draftkka->program->penugasanaudit->rencanadetail->object_id) }}
										@else 
											{{ object_audit($records->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $records->draftkka->program->penugasanaudit->rencanadetail->object_id) }}
										@endif
									</td>
								</tr>
								<tr>
									<td style="text-align: right;border-top: 1px solid #ffffff;border-right: 1px solid #ffffff;">Tgl Audit</td>
									<td style="width: 5px;border-top: 1px solid #ffffff;border-right: 1px solid #ffffff;">:</td>
									<td style="border-top: 1px solid #ffffff;border-right: 1px solid #ffffff;">{{ DateToString($records->draftkka->program->detailpelaksanaan->first()->tgl).' - '.DateToString($records->draftkka->program->detailpelaksanaan->last()->tgl) }}</td>
									<td style="text-align: right;width: 80px;border-top: 1px solid #ffffff;border-right: 1px solid #ffffff;">Progress</td>
									<td style="width: 5px;border-top: 1px solid #ffffff;border-right: 1px solid #ffffff;">:</td>
									<td style="border-top: 1px solid #ffffff;"></td>
								</tr>
							</tbody>
						</table>
						<table class="ui table bordered" style="margin-left: -70px;margin-right: -71px;padding-top: -2px;">
								@php 
									$i=1;
								@endphp
								@foreach(getfileskka($records->draftkka->detaildraft->pluck('id')->toArray()) as $key => $data)
								<tr>
									@if($i==1 || $i==2)
									<td style="text-align: center;" colspan="2"><img src="{{ asset('storage/'.$data) }}" style="width:360px;height:230px;"></td>
									@else
									<td style="text-align: center;" colspan="2"><img src="{{ asset('storage/'.$data) }}" style="width:360px;height:280px;"></td>
									@endif
								</tr>
								<tr>
									<td style="text-align: justify;" colspan="2">Kondisi : {{ $records->draftkka->detaildraft->where('id', $key)->first()->catatan_kondisi }}</td>
								</tr>
								<tr>
									<td style="text-align: justify;" colspan="2">Risiko : {{ $records->draftkka->detaildraft->where('id', $key)->first()->risiko }}</td>
								</tr>
								<tr>
									<td style="text-align: justify;" colspan="2">Rekomendasi : <br>
										@foreach($records->draftkka->detaildraft->where('id', $key)->first()->rekomendasi as $x => $datas)
										{{$x+1}}. {{$datas->rekomendasi}}<br>
										@endforeach
									</td>
								</tr>
								@php 
									$i++;
								@endphp
								@endforeach
								<tr>
									<td style="" colspan="2">CATATAN :</td>
								</tr>
								<tr>
									<td style="">ASDSAD</td>
									<td style="">
										<p>SADSADSAD</p>
										<p>SADSAD</p>
									</td>
								</tr>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		@endif
	</main>
</body>
</html>