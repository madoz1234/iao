<html>
<head>
<style>
        @font-face {
		    font-family: 'tahoma';
		    src: url({{ storage_path('fonts\tahoma.ttf') }}) format("truetype");
		    font-weight: 400;
		    font-style: normal;
		}

        @page {
            margin: 60px 25px;
            counter-reset: page;
        }

       body {
           margin-top: 3cm;
           margin-left: 1.5cm;
           margin-right: 1.5cm;
           margin-bottom: 2cm;
           font-style: normal;
       }

       .ui.table.bordered {
         border: solid black 1px;
         border-collapse: collapse;
         width: 100%;
         font-family : "tahoma", "Verdana", sans-serif;
       }

       .ui.table.bordered td {
         border: solid black .5px;
         border-collapse: collapse;
         padding:5px;
       }

       .ui.table.bordered td.center.aligned {
         text-align : center;
       }

       .page-break {
            page-break-after: always;
       }

       .pad{
       	padding-top: 5px;
       	padding-bottom: 5px;
       }

        header {
            position: fixed;
            top: -40px;
            left: 51px;
            right: 74px;
            height: 10px;
            text-align: center;
            font-size: 12px;
        }
		
       main {
         position: sticky;
         font-size : 12px;
         padding-top : -1cm;
         margin-top : 2px;
         border : none;
         width: 100%;
       }

       main p {
         margin-left: 5px;
         margin-right: 5px;
       }
       footer {
           position: fixed;
           bottom: -60px;
           left: 0px;
           right: 0px;
           height: 50px;
           text-align: center;
           line-height: 35px;
       }
       table.page_content {width: 100%; border: none; padding: 1mm; font-size: 12px;font-family : "tahoma", "Verdana", sans-serif;}
       table.page_header {width: 100%; border: none; padding: 2mm; font-size: 12px;font-family : "tahoma", "Verdana", sans-serif; text-align: left; margin-left: 50px;margin-top: 50px;}
       table.page_content_header {width: 100%; font-size: 12px;border-collapse: collapse;font-family : "tahoma", "Verdana", sans-serif;text-align: center; padding-bottom: 1px;
       	padding-top: 1px;}
</style>
</head>
<body>
    <header>
    	<table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;">
    		<tbody>
    			<tr style="">
    				<td style="text-align: left;border: 1px #ffffff;" colspan="2">
    					<table style="">
    						<tbody>
    							<tr>
    								<td style="width: 100px;padding-left: -10px;"><img src="{{ asset('src/img/logo.png') }}" width="85px" height="70px"></td>
    								<td style="width: 360px;"><h2>PT WASKITA KARYA <small>(Persero)</small> Tbk</h2></td>
    								<td style="width: 170px">
    								</td>
    							</tr>
    						</tbody>
    					</table>
    				</td>
    			</tr>
    		</tbody>
		</table>
  	</header>
  	<br>
	<main>
		<table class="table table-bordered m-t-none" style="font-size: 12px;">
			<tbody>
				<tr>
					<td style="border-right: #ffffff;">Nomor : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/WK/DIR/2019</td>
					<td style="border-left: #ffffff;text-align: right;" colspan="2">Jakarta, 25 Februari 2020</td>
				</tr>
				<tr>
					<td style="text-align:left;border-top: #ffffff;" colspan="2">Kepada Yth, </td>
				</tr>
				<tr>
					<td style="text-align:left;border-top: #ffffff;" colspan="2">SVP - {{ object_audit($records->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $records->draftkka->program->penugasanaudit->rencanadetail->object_id) }} </td>
				</tr>
				<tr>
					<td style="text-align:left;border-top: #ffffff;" colspan="2">PT. Waskita Karya (Persero), Tbk.</td>
				</tr>
				<tr>
					<td style="text-align:left;border-top: #ffffff;" colspan="2">Di-</td>
				</tr>
				<tr>
					<td style="text-align:left;border-top: #ffffff;" colspan="2">Jakarta</td>
				</tr>
				<tr>
					<td style="text-align:left;border: #ffffff;" colspan="2"></td>
				</tr>
				<tr>
					<td style="text-align:center;border-top: #ffffff;" colspan="3">Perihal : <b>Laporan Hasil Audit Internal</b></td>
				</tr>
				<tr>
					<td style="text-align:left;border: #ffffff;" colspan="2">Dengan Hormat,</td>
				</tr>
				<tr>
					<td style="text-align:left;border: #ffffff;" colspan="2">Sehubungan dengan pemeriksaan internal pada :</td>
				</tr>
				<tr>
					<td style="text-align:center;border:#ffffff;" colspan="3">
						<table style="font-size: 12px;" class="page_content ui table bordered">
							<tbody>
								<tr>
									<td style="text-align: center;">No Laporan</td>
									<td style="text-align: center;">Tanggal Audit</td>
									<td style="text-align: center;">Objek Audit</td>
								</tr>
								<tr>
									<td style="text-align: center;width: 30%;">{{ $records->memo }} - LHA/WK/IA/{{ DateToStringWday($records->tanggal) }}</td>
									<td style="text-align: center;width: 20%;">{{ DateToString($records->draftkka->program->detailpelaksanaan->first()->tgl).' - '.DateToString($records->draftkka->program->detailpelaksanaan->last()->tgl) }}</td>
									<td style="text-align: center;width: 40%;">{{ object_audit($records->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $records->draftkka->program->penugasanaudit->rencanadetail->object_id) }}</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td style="text-align: justify;text-justify: inter-word;border-top:#ffffff;" colspan="3">
						<br>
						Sesuai surat SVP-Internal Audit No : 05/SP/WK/IA/2019 tanggal 3 Oktober 2019, maka diminta agar Saudara melakukan tindaklanjut dari hasil audit tersebut sebagaimana mestinya serta melaporkan pelaksanaan tindaklanjut berikut dengan bukti-buktinya yang ditujukan kepada Internal Audit (IA) selambat-lambatnya tanggal : {{ DateToStringWday($records->tanggal) }}
					</td>
				</tr>
				<tr>
					<td style="text-align: justify;text-justify: inter-word;border-top:#ffffff;" colspan="3">
						Demikian, agar dilaksanakan sebagaimana mestinya.
					</td>
				</tr>
				<tr style="border:#ffffff;">
					<td style="text-align: left;border-top:#ffffff;" colspan="3">
						<br>
						<br>
						<b>President Director</b><br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						(I Gusti Ngurah Putra)
						<br>
						<br>
						<br>
					</td>
				</tr>
				<tr style="border:#ffffff;">
					<td style="text-align: justify;text-justify: inter-word;border:#ffffff;" colspan="3">
						Lampiran : <br>
						<ul>
							<li>Surat No. 05/SP/WK/IA/2019</li>
						</ul> 
					</td>
				</tr>
				<tr style="border:#ffffff;">
					<td style="text-align: justify;text-justify: inter-word;border:#ffffff;" colspan="3">
						Tembusan : <br>
						<ol>
							<li>Board of Commisioners (tanpa lampiran)</li>
							<li>Board of Director (tanpa lampiran)</li>
							<li>SVP-IA</li>
						</ol> 
					</td>
				</tr>
			</tbody>
		</table>
	</main>
</body>
</html>