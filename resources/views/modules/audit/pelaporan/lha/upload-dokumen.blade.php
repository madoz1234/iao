@extends('layouts.form')
@section('title', 'Upload Dokumen')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body" style="padding-bottom: 0px;">
		<form action="{{ route($routes.'.simpanDokumen', $record->id) }}" method="POST" id="formData">
			@method('PATCH')
		    @csrf
		    <input type="hidden" name="id" value="{{ $record->id }}">
			<div class="form-row">
				@if($record->ket_dirut)
					<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
						<tr>
							<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
								<h5 style="font-weight: bold;font-size: 12px;border: 1px #ffffff;">Alasan Penolakan :</h5>
								<p style="text-align: justify;text-justify: inter-word;border: 1px #ffffff;">{{ $record->ket_dirut }}</p>
							</td>
						</tr>
					</table>
				@endif
				<div class="form-group col-md-12">
					<table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
						<thead style="background-color: #f9fafb;">
							<tr>
								<th scope="col" style="text-align: center;">No</th>
								<th scope="col" style="text-align: center;">Judul Dokumen</th>
								<th scope="col" style="text-align: center;">Deksripsi</th>
							</tr>
						</thead>
						<tbody class="container">
							<tr>
								<td style="width: 20px !important;">1</td>
								<td style="width: 300px !important;">LHA</td>
								<td style="" class="field">
									<input  name="berkas[1][data]" type="file" class="file form-control cfile" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf">
								</td>
							</tr>
							<tr>
								<td style="width: 20px !important;">2</td>
								<td style="width: 300px !important;">Resume</td>
								<td style="" class="field">
									<input  name="berkas[2][data]" type="file" class="file form-control cfile" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf">
								</td>
							</tr>
							<tr>
								<td style="width: 20px !important;">3</td>
								<td style="width: 300px !important;">Surat Pengantar</td>
								<td style="" class="field">
									<input  name="berkas[3][data]" type="file" class="file form-control cfile" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf">
								</td>
							</tr>
							<tr>
								<td style="width: 20px !important;">4</td>
								<td style="width: 300px !important;">Surat Disposisi Tindak Lanjut</td>
								<td style="" class="field">
									<input  name="berkas[4][data]" type="file" class="file form-control cfile" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf">
								</td>
							</tr>
						</tbody>
					</table>
					<div class="form-group col-md-12">
						<div class="text-right">
							<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
							<button type="button" class="btn btn-simpan save as page2">Simpan</button>
						</div>
					</div>
				</div>
			</div>
		</form>
    </div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script> 
		$.each( $(".cfile"), function(index,value) {
			var idx = $(this).data("id");
			var url = "{!! route('kegiatan-audit.persiapan.tinjauan-dokumen.index') !!}/" + idx + '/get-file';
			$.ajax({
	            url: url,
	            type: 'GET',
	            data: {
	                id: idx,
	            },
	            cache: true,
	            success: function(response) {
	            }
	        }).done(function(response) {
	        	console.log(idx, response)
			    $('#datafile-'+idx).fileinput({
				    uploadUrl: "/file-upload-batch/1",
				    previewFileType: "pdf",
				    overwriteInitial: false,
				    initialPreviewAsData: true,
				    showUpload: false,
				    initialPreview: [
				        response
				    ],
				    initialPreviewConfig: [
				        {type: 'pdf', size: "auto",height: "auto"}
				    ]
				});
			});
		});
    </script>
    @yield('js-extra')
@endpush

