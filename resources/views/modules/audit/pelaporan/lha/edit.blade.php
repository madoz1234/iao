@extends('layouts.form')
@section('title', 'Ubah LHA')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body" style="padding-bottom: 0px;">
		<form action="{{ route($routes.'.update', $data->id) }}" method="POST" id="formData">
			@method('PATCH')
			@csrf
			<div class="loading dimmer padder-v" style="display: none;">
			    <div class="loader"></div>
			</div>	
			<input type="hidden" name="id" value="{{ $data->id }}">
			<input type="hidden" name="tahun" value="{{ $record->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun }}">
			@if($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 2)
				<input type="hidden" name="status_proyek" value="1">
			@else
				<input type="hidden" name="status_proyek" value="0">
			@endif
			<input type="hidden" name="status" value="0">
			@php 
				$data_operasional = array();
				foreach($record->draftkka->program->detailanggota->where('fungsi', 1) as $operasional) {
				 $data_operasional[] = $operasional->user->name;
				}

				$data_keuangan = array();
				foreach($record->draftkka->program->detailanggota->where('fungsi', 2) as $keuangan) {
				 $data_keuangan[] = $keuangan->user->name;
				}

				$data_sistem = array();
				foreach($record->draftkka->program->detailanggota->where('fungsi', 3) as $sistem) {
				 $data_sistem[] = $sistem->user->name;
				}
			@endphp
			@if($data->ket_svp)
				<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
					<tr>
						<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
							<h5 style="font-weight: bold;font-size: 12px;border: 1px #ffffff;">Alasan Penolakan :</h5>
							<p style="text-align: justify;text-justify: inter-word;border: 1px #ffffff;">{{ $data->ket_svp }}</p>
						</td>
					</tr>
				</table>
			@endif
			<div class="form-row">
				<table id="table-operasional" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
					<tbody>
						<tr>
							<td style="" colspan="2" rowspan="3">&nbsp;</td>
							<td style="width: 200px !important;text-align: center;" colspan="2">Form. IA 04</td>
						</tr>
						<tr>
							<td style="width: 100px !important;">Edisi : @if($edisi) {{ $edisi->edisi }} @else - @endif</td>
							<td style="width: 100px !important;">Revisi : @if($edisi) {{ $edisi->revisi }} @else - @endif</td>
						</tr>
						<tr>
							<td style="" colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td style="width: 70rem !important;">Nomor :............. - LHA/WK/IA/...........</td>
							<td style="width: 70rem !important; text-align: left;" colspan="3" class="field">Tanggal : <input type="text" style="width: 200px;" name="tanggal" class="form-controls tanggal" placeholder="...................................................." required="" value="{{ $record->tanggal }}">.</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-bottom: 1px #ffffff;text-transform: uppercase;" colspan="6">
								LAPORAN HASIL AUDIT INTERNAL PADA @if($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object ==2) PROYEK @endif {{ object_audit($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id) }}<br>
								PT. WASKITA KARYA (PERSERO) TBK.</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">I. Dasar Penugasan Audit</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											<table style="width: 30%;border: 1px #ffffff;font-size: 12px;" width="324">
												<tbody>
													<tr>
														<td style="">Berdasarkan Internal Memo Nomor :</td>
														<td style="text-align: left;" class="field"><input type="text" style="width: 200px;" name="memo" class="form-control" placeholder="Memo" required="" value="{{$record->memo}}"></td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">II. Tujuan dan Lingkup Audit</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											{{ $record->draftkka->program->sasaran }}<br>
											Lingkup Audit Meliputi : {{ $record->draftkka->program->ruang_lingkup }}
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">III. Standar Audit</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											Audit telah dilakukan dengan Standar Audit yang diterbitkan oleh Asosiasi Auditor Internal yang meliputi Standar Umum, Standar Pelaksanaan, Standar Pelaporan dan Standar Tindak Lanjut
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">III. Auditor</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											Ketua Tim &nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;{{ $record->draftkka->program->user->name }} <br>
											Anggota &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;{{ implode(", ",$data_operasional) }}, {{ implode(", ",$data_keuangan) }}, {{ implode(", ",$data_sistem) }} <br>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">V. Periode Pelaksanaan Audit</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											Audit dilaksanakan pada {{ DateToString($record->draftkka->program->detailpelaksanaan->first()->tgl).' - '.DateToString($record->draftkka->program->detailpelaksanaan->last()->tgl) }}
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">VI. Metodologi Audit</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											Metodologi Audit dilaksanakan sesuai dengan Program Kerja Tahunan Internal Audit Tahun
											2019 dengan menggunakan metode sampling dengan tahapan sebagai berikut :<br>
											<ol class="list-group" style="padding-left: 13px;">
												<li class="list item">Perencanaan</li>
												<li class="list item">Pelaksanaan
													<ol type="a" style="padding-left: 15px;">
														<li>Prosedur Analitis</li>
														<li>Pengendalian Intern</li>
														<li>Substansi</li>
														<li>Tanggapan Auditee</li>
													</ol>
												</li>
												<li class="list item">Pelaporan</li>

											</ol>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						@if($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 2)
							<tr>
								<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
									<h5 style="font-weight: bold;">VII. Data Umum Objek Audit</h5>
									<table style="font-size: 12px;" class="table table-bordered">
										<thead>
											<tr style="">
												<th style="text-align: left;background-color: #cccccc;" colspan="3">GAMBARAN UMUM PROYEK</th>
											</tr>
										</thead>
										<tbody>
											<tr style="">
												<td style="width: 200px;">Nama Proyek</td>
												<td style="width: 8px;">:</td>
												<td style="text-align: left;">{{ object_audit($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id) }}</td>
											</tr>
											<tr style="">
												<td style="width: 200px;">Lokasi Proyek</td>
												<td style="width: 8px;">:</td>
												<td style="text-align: left;">{{ getlokasi($record->draftkka->program->penugasanaudit->rencanadetail->object_id) }}</td>
											</tr>
											<tr style="">
												<td style="width: 200px;">Pemilik Proyek</td>
												<td style="width: 8px;">:</td>
												<td style="text-align: left;" class="field">
													<textarea class="form-control" name="pemilik" placeholder="Pemilik" rows="2">{{$record->pemilik}}</textarea>
												</td>
											</tr>
											<tr style="">
												<td style="width: 200px;">Sumber Dana</td>
												<td style="width: 8px;">:</td>
												<td style="text-align: left;" class="field">
													<input class="form-control" name="sumber" placeholder="Sumber Dana" type="text" value="{{$record->sumber}}"/>
												</td>
											</tr>
											<tr style="">
												<td style="width: 200px;">Sifat Kontrak</td>
												<td style="width: 8px;">:</td>
												<td style="text-align: left;" class="field">
													<input class="form-control" name="sifat" placeholder="Sifat Kontrak" type="text" value="{{$record->sifat}}"/>
												</td>
											</tr>
											<tr style="">
												<td style="width: 200px;">Nomor SPMK</td>
												<td style="width: 8px;">:</td>
												<td style="text-align: left;" class="field">
													<input class="form-control" name="nomor_spmk" placeholder="Nomor SPMK" type="text" value="{{$record->nomor_spmk}}"/>
												</td>
											</tr>
											<tr style="">
												<td style="width: 200px;">Nilai Kontrak (include PPN)</td>
												<td style="width: 8px;">:</td>
												<td style="text-align: left;" class="field">
													<input class="form-control" name="nilai_kontrak" placeholder="Nilai Kontrak (include PPN)" type="text" value="{{$record->nilai_kontrak}}"/>
												</td>
											</tr>
											<tr style="">
												<td style="width: 200px;">Tanggal Kontrak</td>
												<td style="width: 8px;">:</td>
												<td style="text-align: left;" class="field">
													<input style="width: 25%;" class="form-control tanggal" id="tanggal" name="tanggal_kontrak" placeholder="Tanggal Kontrak" type="text" value="{{$record->tanggal_kontrak}}"/>
												</td>
											</tr>
											<tr style="">
												<td style="width: 200px;">Tanggal Akhir Kontrak</td>
												<td style="width: 8px;">:</td>
												<td style="text-align: left;" class="field">
													<input style="width: 25%;" class="form-control tanggal" id="tanggal_akhir" name="tanggal_akhir_kontrak" placeholder="Tanggal Akhir Kontrak" type="text" value="{{$record->tanggal_akhir_kontrak}}"/>
												</td>
											</tr>
											<tr style="">
												<td style="width: 200px;">Waktu Pelaksanaan</td>
												<td style="width: 8px;">:</td>
												<td style="text-align: left;" class="field">
													<input class="form-control" name="waktu_pelaksanaaan" placeholder="Waktu Pelaksanaan" type="text" value="{{$record->waktu_pelaksanaaan}}"/>
												</td>
											</tr>
											<tr style="">
												<td style="width: 200px;">Waktu Pemeliharaan</td>
												<td style="width: 8px;">:</td>
												<td style="text-align: left;" class="field">
													<input class="form-control" name="waktu_pemeliharaan" placeholder="Waktu Pemeliharaan" type="text" value="{{$record->waktu_pemeliharaan}}"/>
												</td>
											</tr>
											<tr style="">
												<td style="width: 200px;">Pembayaran</td>
												<td style="width: 8px;">:</td>
												<td style="text-align: left;" class="field">
													<input class="form-control" name="pembayaran" placeholder="Pembayaran" type="text" value="{{$record->pembayaran}}"/>
												</td>
											</tr>
											<tr style="">
												<td style="width: 200px;">Konsultan Perencana</td>
												<td style="width: 8px;">:</td>
												<td style="text-align: left;" class="field">
													<input class="form-control" name="konsultan_perencana" placeholder="Konsultan Perencana" type="text" value="{{$record->konsultan_perencana}}"/>
												</td>
											</tr>
											<tr style="">
												<td style="width: 200px;">Konsultan Pengawas</td>
												<td style="width: 8px;">:</td>
												<td style="text-align: left;" class="field">
													<input class="form-control" name="konsultan_pengawas" placeholder="Konsultan Pengawas" type="text" value="{{$record->konsultan_pengawas}}"/>
												</td>
											</tr>
										</tbody>
									</table>
									<table id="table-adendum" style="font-size: 12px;" class="table table-bordered">
										<thead>
											<tr>
												<th style="text-align: left;background-color: #cccccc;" colspan="4" class="v-middle">Adendum</th>
												<th style="width: 8px;background-color: #cccccc;">
													<button class="btn btn-sm btn-success tambah_adendum" type="button" data-id="0" style="margin-top: 4px;"><i class="fa fa-plus"></i></button>
												</th>
											</tr>
										</thead>
										<tbody class="container-adendum">
											@if($record->detailadendum)
												@foreach($record->detailadendum as $key => $adendum)
													<tr class="adendums adendum-kepala-{{$key}}" data-id="{{$key}}">
														<input type="hidden" name="exist_adendum[]" value="{{ $adendum->id }}">
														<input type="hidden" name="adendum[{{$key}}][id]" value="{{ $adendum->id }}">
														<td style="width: 8px;" rowspan="4" class="numboor-{{$key}}">{{($key+1)}}.</td>
														<td style="width: 160px;">Nomor Kontrak Adendum</td>
														<td style="width: 8px;">:</td>
														<td style="text-align: left;" class="field">
															<input class="form-control" name="adendum[{{$key}}][no_kontrak]" placeholder="Nomor Kontrak Adendum" type="text" value="{{$adendum->no_kontrak}}"/>
														</td>
														<td style="" rowspan="4">
														<button class="btn btn-sm btn-danger hapus_adendum" type="button" style="border-radius:20px;" data-id={{$key}}><i class="fa fa-remove"></i></button>
														</td>
													</tr>
													<tr class="adendum-detil-{{$key}}">
														<td style="width: 160px;">Nilai Kontrak (include PPN)</td>
														<td style="width: 8px;">:</td>
														<td style="text-align: left;" class="field">
															<input class="form-control" name="adendum[{{$key}}][nilai]" placeholder="Nilai Kontrak (include PPN)" type="text" value="{{$adendum->nilai}}"/>
														</td>
													</tr>
													<tr class="adendum-detil-{{$key}}">
														<td style="width: 160px;">Tanggal Kontrak Adendum</td>
														<td style="width: 8px;">:</td>
														<td style="text-align: left;" class="field">
															<input style="width: 25%;" class="form-control tanggal" name="adendum[{{$key}}][tgl_awal]" placeholder="Tanggal Kontrak Adendum" type="text" value="{{$adendum->tgl_awal}}"/>
														</td>
													</tr>
													<tr class="adendum-detil-{{$key}}">
														<td style="width: 160px;">Tanggal Akhir Kontrak Adendum</td>
														<td style="width: 8px;">:</td>
														<td style="text-align: left;" class="field">
															<input style="width: 25%;" class="form-control tanggal" name="adendum[{{$key}}][tgl_akhir]" placeholder="Tanggal Akhir Kontrak Adendum" type="text" value="{{$adendum->tgl_akhir}}"/>
														</td>
													</tr>
													<input type="hidden" name="last_adendum" value="{{$key}}">
												@endforeach
											@else 
												<tr class="adendums adendum-kepala-0" data-id="0">
													<td style="width: 8px;" rowspan="4" class="numboor-0">1.</td>
													<td style="width: 160px;">Nomor Kontrak Adendum</td>
													<td style="width: 8px;">:</td>
													<td style="text-align: left;" class="field">
														<input class="form-control" name="adendum[0][no_kontrak]" placeholder="Nomor Kontrak Adendum" type="text"/>
													</td>
													<td style="" rowspan="4">
													<button class="btn btn-sm btn-danger hapus_adendum" type="button" style="border-radius:20px;" data-id=0><i class="fa fa-remove"></i></button>
													</td>
												</tr>
												<tr class="adendum-detil-0`">
													<td style="width: 160px;">Nilai Kontrak (include PPN)</td>
													<td style="width: 8px;">:</td>
													<td style="text-align: left;" class="field">
														<input class="form-control" name="adendum[0][nilai]" placeholder="Nilai Kontrak (include PPN)" type="text"/>
													</td>
												</tr>
												<tr class="adendum-detil-0">
													<td style="width: 160px;">Tanggal Kontrak Adendum</td>
													<td style="width: 8px;">:</td>
													<td style="text-align: left;" class="field">
														<input style="width: 25%;" class="form-control tanggal" name="adendum[0][tgl_awal]" placeholder="Tanggal Kontrak Adendum" type="text"/>
													</td>
												</tr>
												<tr class="adendum-detil-0">
													<td style="width: 160px;">Tanggal Akhir Kontrak Adendum</td>
													<td style="width: 8px;">:</td>
													<td style="text-align: left;" class="field">
														<input style="width: 25%;" class="form-control tanggal" name="adendum[0][tgl_akhir]" placeholder="Tanggal Akhir Kontrak Adendum" type="text"/>
													</td>
												</tr>
											@endif
										</tbody>
									</table>
									<table style="font-size: 12px;" class="table table-bordered">
										<thead>
											<tr style="">
												<th style="text-align: left;background-color: #cccccc;" colspan="3" class="v-middle">KINERJA S/D BULAN JANUARI {{ $record->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun }}</th>
											</tr>
										</thead>
										<tbody class="adendums">
											<tr style="">
												<td style="width: 290px;" class="field">BK/PU s/d <input class="form-controls" name="tgl_bkpu" placeholder=".........................." type="text" value="{{$record->tgl_bkpu}}" /></td>
												<td style="width: 8px;">:</td>
												<td style="text-align: left;" class="field">
													<textarea class="form-control" name="bkpu" placeholder="BK/PU s/d Januari" rows="2">{{$record->bkpu}}</textarea>
												</td>
											</tr>
											<tr style="">
												<td style="width: 290px;" class="field">Progress s/d <input class="form-controls" name="tgl_progress" placeholder=".........................." type="text" value="{{$record->tgl_progress}}"/></td>
												<td style="width: 8px;">:</td>
												<td style="text-align: left;" class="field">
													<textarea class="form-control" name="progress" placeholder="Progress s/d Januari" rows="2">{{$record->progress}}</textarea>
												</td>
											</tr>
											<tr style="">
												<td style="width: 290px;" class="field">PU s/d <input class="form-controls" name="tgl_pu" placeholder=".........................." type="text" value="{{$record->tgl_pu}}"/></td>
												<td style="width: 8px;">:</td>
												<td style="text-align: left;" class="field">
													<textarea class="form-control" name="pu" placeholder="PU" rows="2">{{$record->pu}}</textarea>
												</td>
											</tr>
											<tr style="">
												<td style="width: 290px;" class="field">Cash s/d <input class="form-controls" name="tgl_cash" placeholder=".........................." type="text" value="{{$record->tgl_cash}}"/></td>
												<td style="width: 8px;">:</td>
												<td style="text-align: left;" class="field">
													<textarea class="form-control" name="cash" placeholder="Cash" rows="2">{{$record->cash}}</textarea>
												</td>
											</tr>
											<tr style="">
												<td style="width: 290px;" class="field">Piutang s/d <input class="form-controls" name="tgl_piutang" placeholder=".........................." type="text" value="{{$record->tgl_piutang}}"/></td>
												<td style="width: 8px;">:</td>
												<td style="text-align: left;" class="field">
													<textarea class="form-control" name="piutang" placeholder="Piutang" rows="2">{{$record->piutang}}</textarea>
												</td>
											</tr>
											<tr style="">
												<td style="width: 290px;" class="field">Tagihan Bruto s/d <input class="form-controls" name="tgl_tagihan" placeholder=".........................." type="text" value="{{$record->tgl_tagihan}}"/></td>
												<td style="width: 8px;">:</td>
												<td style="text-align: left;" class="field">
													<textarea class="form-control" name="tagihan" placeholder="Tagihan Bruto" rows="2">{{$record->tagihan}}</textarea>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
									<h5 style="font-weight: bold;">VIII. Ruang Lingkup Pekerjaan</h5>
									<table id="table-lingkup" style="font-size: 12px;" class="table table-bordered">
										<thead>
											<tr>
												<td style="width: 10px !important;background-color: #cccccc;text-align: center;border: 1px solid #eaeff0;font-weight: bold;"  class="v-middle" rowspan="3">NO</td>
												<td style="width: 900px !important;background-color: #cccccc;text-align: center;border: 1px solid #eaeff0;font-weight: bold;" class="v-middle" rowspan="3">URAIAN</td>
												<td style="background-color: #cccccc;text-align: center;border: 1px solid #eaeff0;font-weight: bold;" class="v-middle"  colspan="3">BOBOT</td>
												<td style="width: 10px !important;background-color: #cccccc;text-align: center;border: 1px solid #eaeff0;font-weight: bold;" class="v-middle"  rowspan="3">
													<button class="btn btn-sm btn-success tambah_lingkup" type="button" data-id="0" style="margin-top: 4px;"><i class="fa fa-plus"></i></button>
												</td>
											</tr>
											<tr>
												<td style="width: 200px !important;background-color: #cccccc;text-align: center;border: 1px solid #eaeff0;font-weight: bold;" class="v-middle"  rowspan="2">KONTRAK</td>
												<td style="width: 200px !important;background-color: #cccccc;text-align: center;border: 1px solid #eaeff0;font-weight: bold;" class="v-middle"  colspan="2">BULAN INI</td>
											</tr>
											<tr>
												<td style="width: 200px !important;background-color: #cccccc;text-align: center;border: 1px solid #eaeff0;font-weight: bold;" class="v-middle" >RENCANA</td>
												<td style="width: 200px !important;background-color: #cccccc;text-align: center;border: 1px solid #eaeff0;font-weight: bold;" class="v-middle" >REALISASI</td>
											</tr>
										</thead>
										<tbody class="container-lingkup">
											@foreach($record->detaillingkup as $keys => $lingkup)
												<tr class="data-lingkup-{{$keys}}" data-id={{$keys}}>
													<input type="hidden" name="exist_lingkup[]" value="{{ $lingkup->id }}">
													<input type="hidden" name="lingkup[{{$keys}}][id]" value="{{$lingkup->id}}">
													<td style="text-align: center;">
														<label style="margin-top: 7px;" class="numbur-{{$keys}}">{{$keys+1}}</label>
													</td>
													<td style="border: 1px solid #eaeff0;" class="field">
														<input class="form-control" name="lingkup[{{$keys}}][uraian]" placeholder="Uraian" type="text" value="{{$lingkup->uraian}}" />
													</td>
													<td style="border: 1px solid #eaeff0;" class="field">
														<input class="form-control" name="lingkup[{{$keys}}][kontrak]" placeholder="Kontrak" type="text" value="{{$lingkup->kontrak}}" />
													</td>
													<td style="border: 1px solid #eaeff0;" class="field">
														<input class="form-control" name="lingkup[{{$keys}}][rencana]" placeholder="Rencana" type="text" value="{{$lingkup->rencana}}" />
													</td>
													<td style="border: 1px solid #eaeff0;" class="field">
														<input class="form-control" name="lingkup[{{$keys}}][realisasi]" placeholder="Realisasi" type="text" value="{{$lingkup->realisasi}}" />
													</td>
													<td style="border: 1px solid #eaeff0;" class="field v-middle">
														<button class="btn btn-sm btn-danger hapus_lingkup" type="button" style="border-radius:20px;" data-id="{{$keys}}"><i class="fa fa-remove"></i></button>
													</td>
												</tr>
												<input type="hidden" name="last_lingkup" value="{{$keys}}">
											@endforeach
										</tbody>
									</table>
								</td>
							</tr>
						@endif
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">@if($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 2) IX. @else VII. @endif Hasil Audit</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											<div class="form-group col-md-12">
												<ul class="nav nav-tabs" id="myTab" role="tablist">
													<li class="nav-item">
														<a style="font-weight: bold;" class="nav-link active" id="a-tab" data-toggle="tab" href="#a" role="tab" aria-controls="home" aria-selected="true">Operasional</a>
													</li>
													<li class="nav-item">
														<a style="font-weight: bold;" class="nav-link" id="b-tab" data-toggle="tab" href="#b" role="tab" aria-controls="profile" aria-selected="false">Keuangan</a>
													</li>
													<li class="nav-item">
														<a style="font-weight: bold;" class="nav-link" id="c-tab" data-toggle="tab" href="#c" role="tab" aria-controls="messages" aria-selected="false">Sistem</a>
													</li>
												</ul>
												<div class="tab-content">
													<div class="tab-pane active" id="a" role="tabpanel" aria-labelledby="a-tab">
														<table id="table-operasional" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
															<tbody class="container-operasional">
																@php 
																	$i=0;
																@endphp
																@foreach($record->draftkka->detaildraft->where('tipe', 0) as $key => $operasional)
																	<tr class="detail-operasional-{{$i}}" data-id="{{$i}}">
																		<td style="text-align: left;font-weight: bold;" colspan="4">Bidang Audit : Operasional</td>
																	</tr>
																	<tr class="data-operasional detail-operasional-{{$i}}">
																		<input type="hidden" name="fokus_audit[0][detail][{{$i}}][data_id]" value="{{$operasional->id}}">
																		<td style="text-align: center;width: 20px;">
																			<label style="margin-top: 7px;" class="operasional-numboor-{{$i}}">{{$i+1}}</label>
																		</td>
																		<td style="width: 150px;">Judul</td>
																		<td style="width: 2px;">:</td>
																		<td style="width: 1600px;text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="cari-operasional kategori_operasional" data-id="{{$i}}" data-val="{{$operasional->id}}" data-cari="{{$operasional->standarisasi_id}}">
																			{{getstandarisasi($operasional->standarisasi_id)}}
																		</td>
																	</tr>
																	<tr class="detail-operasional-{{$i}}">
																		<td rowspan="9" style="border-bottom: 2px solid black;"></td>
																		<td style="">Kondisi</td>
																		<td style="">:</td>
																		<td style="text-align: left;" colspan="2"  class="field">
																			{{ $operasional->catatan_kondisi }}
																		</td>
																	</tr>
																	<tr class="detail-operasional-{{$i}}">
																		<td style="">Kriteria</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field" colspan="2">
																			<ol style="padding-left: 12px;" class="kriteria_operasional_{{$i}}">
																			</ol>
																		</td>
																	</tr>
																	<tr class="detail-operasional-{{$i}}">
																		<td style="">Sebab</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
																			{{ $operasional->sebab }}
																		</td>
																	</tr>
																	<tr class="detail-operasional-{{$i}}">
																		<td style="">Risiko</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
																			{{ $operasional->risiko }}
																		</td>
																	</tr>
																	<tr class="detail-operasional-{{$i}}">
																		<td style="">Kategori Temuan</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
																			{!! getkategoristatus($operasional->kategori_id) !!}
																		</td>
																	</tr>
																	<tr class="detail-operasional-{{$i}}">
																		<td style="">Rekomendasi</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
																			@foreach($operasional->rekomendasi as $x => $operasionals)
																				{{$x+1}}. {{$operasionals->rekomendasi}}<br>
																			@endforeach
																		</td>
																	</tr>
																	<tr class="detail-operasional-{{$i}}">
																		<td style="">Tanggapan Auditee</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
																			@if($operasional->tanggapan == 0)
																				MENYETUJUI
																			@else 
																				TIDAK MENYETUJUI
																				<br>
																				<br>
																				Catatan Tanggapan Auditee : <br>
																				<p style="text-align: justify;text-justify: inter-word;">{{ $operasional->catatan }}</p>
																				@if($operasional->ba)
																					<p class="notif-{{ $operasional->id }} label label-success">Auditee bersedia menandatangani Berita Acara Tidak Setuju</p><br>
																				@else 
																					<p class="notif-{{ $operasional->id }} label label-danger">Auditee tidak bersedia menandatangani Berita Acara Tidak Setuju</p><br><br>
																				@endif
																				<div class="file-loading">
																	            	<input id="ops-{{ $operasional->id }}" name="fokus_audit[0][detail][{{$i}}][lampirans]" data-id="{{ $operasional->id }}" type="file" class="form-control lampirs" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/*">
																	            </div>
																			@endif
																		</td>
																	</tr>
																	<tr class="detail-operasional-end-{{$i}} detail-operasional-{{$i}}">
																		<td style="">Tanggal Tindak Lanjut</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
																			{{ DateToString($operasional->tgl) }}
																		</td>
																	</tr>
																	<tr class="detail-operasional-end-{{$i}} detail-operasional-{{$i}}" style="border-bottom: 2px solid black;">
																		<td style="">Lampiran</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
																			<div class="file-loading">
																            	<input id="op-{{ $operasional->id }}" name="fokus_audit[0][detail][{{$i}}][lampiran]" data-id="{{ $operasional->id }}" type="file" class="form-control lampir" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/*">
																            </div>
																		</td>
																	</tr>
																	@php 
																		$i++;
																	@endphp
																@endforeach
															</tbody>
														</table>
													</div>
													<div class="tab-pane" id="b" role="tabpanel" aria-labelledby="b-tab">
														<table id="table-keuangan" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
															<tbody class="container-keuangan">
																@php 
																	$j=0;
																@endphp
																@foreach($record->draftkka->detaildraft->where('tipe', 1) as $key => $keuangan)
																	<tr class="detail-keuangan-{{$j}}" data-id="{{$j}}">
																		<td style="text-align: left;font-weight: bold;" colspan="4">Bidang Audit : Keuangan</td>
																	</tr>
																	<tr class="data-keuangan detail-keuangan-{{$j}}">
																		<input type="hidden" name="fokus_audit[1][detail][{{$j}}][data_id]" value="{{$keuangan->id}}">
																		<td style="text-align: center;width: 20px;">
																			<label style="margin-top: 7px;" class="keuangan-numboor-{{$j}}">{{$j+1}}</label>
																		</td>
																		<td style="width: 150px;">Judul</td>
																		<td style="width: 2px;">:</td>
																		<td style="width: 1600px;text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="cari-keuangan kategori_keuangan" data-id="{{$j}}" data-val="{{$keuangan->id}}" data-cari="{{$keuangan->standarisasi_id}}">
																			{{getstandarisasi($keuangan->standarisasi_id)}}
																		</td>
																	</tr>
																	<tr class="detail-keuangan-{{$j}}">
																		<td style="border-bottom: 2px solid black;" rowspan="9"></td>
																		<td style="">Kondisi</td>
																		<td style="">:</td>
																		<td style="text-align: left;" colspan="2"  class="field">
																			{{ $keuangan->catatan_kondisi }}
																		</td>
																	</tr>
																	<tr class="detail-keuangan-{{$j}}">
																		<td style="">Kriteria Temuan</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field" colspan="2">
																			<ol style="padding-left: 12px;" class="kriteria_keuangan_{{$j}}">
																			</ol>
																		</td>
																	</tr>
																	<tr class="detail-keuangan-{{$j}}">
																		<td style="">Sebab</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
																			{{ $keuangan->sebab }}
																		</td>
																	</tr>
																	<tr class="detail-keuangan-{{$j}}">
																		<td style="">Risiko</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
																			{{ $keuangan->risiko }}
																		</td>
																	</tr>
																	<tr class="detail-keuangan-{{$j}}">
																		<td style="">Kategori Temuan</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
																			{!! getkategoristatus($keuangan->kategori_id) !!}
																		</td>
																	</tr>
																	<tr class="detail-keuangan-{{$j}}">
																		<td style="">Rekomendasi</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
																			@foreach($keuangan->rekomendasi as $y => $keuangans)
																				{{$y+1}}. {{$keuangans->rekomendasi}}<br>
																			@endforeach
																		</td>
																	</tr>
																	<tr class="detail-keuangan-{{$j}}">
																		<td style="">Tanggapan Auditee</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
																			@if($keuangan->tanggapan == 0)
																				MENYETUJUI
																			@else 
																				TIDAK MENYETUJUI
																				<br>
																				<br>
																				Catatan Tanggapan Auditee : <br>
																				<p style="text-align: justify;text-justify: inter-word;">{{ $keuangan->catatan }}</p>
																				@if($keuangan->ba)
																					<p class="notif-{{ $keuangan->id }} label label-success">Auditee bersedia menandatangani Berita Acara Tidak Setuju</p><br>
																				@else 
																					<p class="notif-{{ $keuangan->id }} label label-danger">Auditee tidak bersedia menandatangani Berita Acara Tidak Setuju</p><br><br>
																				@endif
																				<div class="file-loading">
																	            	<input id="keus-{{ $keuangan->id }}" name="fokus_audit[1][detail][{{$j}}][lampirans]" data-id="{{ $keuangan->id }}" type="file" class="form-control lampirs" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/*">
																	            </div>
																			@endif
																		</td>
																	</tr>
																	<tr class="detail-keuangan-end-{{$j}} detail-keuangan-{{$j}}">
																		<td style="">Tanggal Tindak Lanjut</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
																			{{ DateToString($keuangan->tgl) }}
																		</td>
																	</tr>
																	<tr class="detail-keuangan-end-{{$j}} detail-keuangan-{{$j}}" style="border-bottom: 2px solid black;">
																		<td style="">Lampiran</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
																			<div class="file-loading">
																            	<input id="keu-{{ $keuangan->id }}" name="fokus_audit[1][detail][{{$j}}][lampiran]" data-id="{{ $keuangan->id }}" type="file" class="form-control lampir" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/*">
																            </div>
																		</td>
																	</tr>
																	@php 
																		$j++;
																	@endphp
																@endforeach
															</tbody>
														</table>
													</div>
													<div class="tab-pane" id="c" role="tabpanel" aria-labelledby="c-tab">
														<table id="table-sistem" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
															<tbody class="container-sistem">
																@php 
																	$k=0;
																@endphp
																@foreach($record->draftkka->detaildraft->where('tipe', 2) as $key => $sistem)
																	<tr class="detail-sistem-{{$k}}" data-id="{{$k}}">
																		<td style="text-align: left;font-weight: bold;" colspan="4">Bidang Audit : Sistem</td>
																	</tr>
																	<tr class="data-sistem detail-sistem-{{$k}}">
																		<input type="hidden" name="fokus_audit[2][detail][{{$k}}][data_id]" value="{{$sistem->id}}">
																		<td style="text-align: center;width: 20px;">
																			<label style="margin-top: 7px;" class="sistem-numboor-{{$k}}">{{$k+1}}</label>
																		</td>
																		<td style="width: 150px;">Judul</td>
																		<td style="width: 2px;">:</td>
																		<td style="width: 1600px;text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="cari-sistem kategori_sistem" data-id="{{$k}}" data-val="{{$sistem->id}}" data-cari="{{$sistem->standarisasi_id}}">
																			{{getstandarisasi($sistem->standarisasi_id)}}
																		</td>
																	</tr>
																	<tr class="detail-sistem-{{$k}}">
																		<td style="border-bottom: 2px solid black;" rowspan="9"></td>
																		<td style="">Kondisi</td>
																		<td style="">:</td>
																		<td style="text-align: left;" colspan="2"  class="field">
																			{{ $sistem->catatan_kondisi }}
																		</td>
																	</tr>
																	<tr class="detail-sistem-{{$k}}">
																		<td style="">Kriteria Temuan</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field" colspan="2">
																			<ol style="padding-left: 12px;" class="kriteria_sistem_{{$k}}">
																			</ol>
																		</td>
																	</tr>
																	<tr class="detail-sistem-{{$k}}">
																		<td style="">Sebab</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
																			{{ $sistem->sebab }}
																		</td>
																	</tr>
																	<tr class="detail-sistem-{{$k}}">
																		<td style="">Risiko</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
																			{{ $sistem->risiko }}
																		</td>
																	</tr>
																	<tr class="detail-sistem-{{$k}}">
																		<td style="">Kategori Temuan</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
																			{!! getkategoristatus($sistem->kategori_id) !!}
																		</td>
																	</tr>
																	<tr class="detail-sistem-{{$k}}">
																		<td style="">Rekomendasi</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
																			@foreach($sistem->rekomendasi as $z => $sistems)
																				{{$z+1}}. {{$sistems->rekomendasi}}<br>
																			@endforeach
																		</td>
																	</tr>
																	<tr class="detail-sistem-{{$k}}">
																		<td style="">Tanggapan Auditee</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
																			@if($sistem->tanggapan == 0)
																				MENYETUJUI
																			@else 
																				TIDAK MENYETUJUI
																				<br>
																				<br>
																				Catatan Tanggapan Auditee : <br>
																				<p style="text-align: justify;text-justify: inter-word;">{{ $sistem->catatan }}</p>
																				@if($sistem->ba)
																					<p class="notif-{{ $sistem->id }} label label-success">Auditee bersedia menandatangani Berita Acara Tidak Setuju</p><br>
																				@else 
																					<p class="notif-{{ $sistem->id }} label label-danger">Auditee tidak bersedia menandatangani Berita Acara Tidak Setuju</p><br><br>
																				@endif
																				<div class="file-loading">
																	            	<input id="siss-{{ $sistem->id }}" name="fokus_audit[2][detail][{{$k}}][lampirans]" data-id="{{ $sistem->id }}" type="file" class="form-control lampirs" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/*">
																	            </div>
																			@endif
																		</td>
																	</tr>
																	<tr class="detail-sistem-end-{{$k}} detail-sistem-{{$k}}">
																		<td style="">Tanggal Tindak Lanjut</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
																			{{ DateToString($sistem->tgl) }}
																		</td>
																	</tr>
																	<tr class="detail-sistem-end-{{$k}} detail-sistem-{{$k}}" style="border-bottom: 2px solid black;">
																		<td style="">Lampiran</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
																			<div class="file-loading">
																            	<input id="sis-{{ $sistem->id }}" name="fokus_audit[2][detail][{{$k}}][lampiran]" data-id="{{ $sistem->id }}" type="file" class="form-control lampir" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/*">
																            </div>
																		</td>
																	</tr>
																	@php 
																		$k++;
																	@endphp
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-right">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
						<button type="button" class="btn btn-primary save as drafting">Save As Draft</button>
						<button type="button" class="btn btn-simpan save as page">Submit</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
        .table-responsive{
            overflow-x: auto;
            overflow-y: visible !important;
        }

        .table-responsive .dropdown-menu {
	        position: static !important;
	    }

	    div.dropdown-menu.open{
		  max-width: 300px !important;
		  overflow: hidden;
		}
		ul.dropdown-menu.inner{
		  max-width: 300px !important;
		  overflow-y: auto;
		}
    </style>
@endpush

@push('scripts')
     <script>
     	var d = new Date();
    	var full = d.getFullYear();
    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
    	var last = new Date(lastDate);
    	if($('input[name=tahun]').val() > full){
    		var min = minDates;
    	}else{
    		var min = d;
    	}

    	$('.tanggal').datepicker({
    		orientation: "bottom left",
    		todayHighlight: true,
    		autoclose:true,
    		format: 'dd-mm-yyyy',
    		defaultViewDate: {year:$('input[name=tahun]').val(), month:0, day:1},
    		yearRange: '-0:+1',
    		startDate: minDates,
    		endDate: last,
    		hideIfNoPrevNext: true,
    	});

    	$('.tgl2').datepicker({
    		orientation: "bottom left",
    		todayHighlight: true,
    		autoclose:true,
    		format: 'mm-yyyy',
    		defaultViewDate: {year:$('input[name=tahun]').val(), month:0, day:1},
    		yearRange: '-0:+1',
    		startDate: minDates,
    		endDate: last,
    		hideIfNoPrevNext: true,
    	});

    	$(document).on('click', '.tambah_adendum', function(e){
    		var last = parseInt($('input[name=last_adendum]').val()) + 1;
			var rowCount = $('#table-adendum > tbody > tr.adendums').length;
			var c = rowCount-1;
			var html = `
						<tr class="adendums adendum-kepala-`+(c+2)+`" data-id="`+(c+2)+`">
							<td style="width: 8px;" rowspan="4" class="numboor-`+(c+2)+`">`+(c+2)+`.</td>
							<td style="width: 250px;">Nomor Kontrak Adendum</td>
							<td style="width: 8px;">:</td>
							<td style="text-align: left;" class="field">
								<input class="form-control" name="adendum[`+last+`][no_kontrak]" placeholder="Nomor Kontrak Adendum" type="text"/>
							</td>
							<td style="" rowspan="4">
							<button class="btn btn-sm btn-danger hapus_adendum" type="button" style="border-radius:20px;" data-id=`+(c+2)+`><i class="fa fa-remove"></i></button>
							</td>
						</tr>
						<tr class="adendum-detil-`+(c+2)+`">
							<td style="width: 250px;">Nilai Kontrak (include PPN)</td>
							<td style="width: 8px;">:</td>
							<td style="text-align: left;" class="field">
								<input class="form-control" name="adendum[`+last+`][nilai]" placeholder="Nilai Kontrak (include PPN)" type="text"/>
							</td>
						</tr>
						<tr class="adendum-detil-`+(c+2)+`">
							<td style="width: 250px;">Tanggal Kontrak Adendum</td>
							<td style="width: 8px;">:</td>
							<td style="text-align: left;" class="field">
								<input style="width: 25%;" class="form-control tanggal" name="adendum[`+last+`][tgl_awal]" placeholder="Tanggal Kontrak Adendum" type="text"/>
							</td>
						</tr>
						<tr class="adendum-detil-`+(c+2)+`">
							<td style="width: 250px;">Tanggal Akhir Kontrak Adendum</td>
							<td style="width: 8px;">:</td>
							<td style="text-align: left;" class="field">
								<input style="width: 25%;" class="form-control tanggal" name="adendum[`+last+`][tgl_akhir]" placeholder="Tanggal Akhir Kontrak Adendum" type="text"/>
							</td>
						</tr>
						`;
				$('.container-adendum').append(html);
		        $('input[name=last_adendum]').val(last);
		        var d = new Date();
		    	var full = d.getFullYear();
		    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
		    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
		    	var last = new Date(lastDate);
		    	if($('input[name=tahun]').val() > full){
		    		var min = minDates;
		    	}else{
		    		var min = d;
		    	}

		    	$('.tanggal').datepicker({
		    		orientation: "bottom left",
		    		todayHighlight: true,
		    		autoclose:true,
		    		format: 'dd-mm-yyyy',
		    		defaultViewDate: {year:$('input[name=tahun]').val(), month:0, day:1},
		    		yearRange: '-0:+1',
		    		startDate: minDates,
		    		endDate: last,
		    		hideIfNoPrevNext: true,
		    	});
		});

		$(document).on('click', '.hapus_adendum', function (e){
			var id = $(this).data("id");
			var rowz = $('tr.adendum-kepala-'+id).remove();
			var rowz = $('tr.adendum-detil-'+id).remove();
			var table = $('#table-adendum');
			var rows = table.find('tbody tr.adendums');
			$.each(rows, function(key, value){
				table.find('.numboor-'+$(this).data("id")).html(key+1);
			});
		});

		$(document).on('click', '.tambah_lingkup', function(e){
    		var last = parseInt($('input[name=last_lingkup]').val()) + 1;
			var rowCount = $('#table-lingkup > tbody > tr').length;
			var c = rowCount-1;
			var html = `
					<tr class="data-lingkup-`+(c+2)+`" data-id=`+(c+2)+`>
						<td style="text-align: center;">
							<label style="margin-top: 7px;" class="numbur-`+(c+2)+`">`+(c+2)+`</label>
						</td>
						<td style="" class="field">
							<input class="form-control" name="lingkup[`+last+`][uraian]" placeholder="Uraian" type="text"/>
						</td>
						<td style="" class="field">
							<input class="form-control" name="lingkup[`+last+`][kontrak]" placeholder="Kontrak" type="text"/>
						</td>
						<td style="" class="field">
							<input class="form-control" name="lingkup[`+last+`][rencana]" placeholder="Rencana" type="text"/>
						</td>
						<td style="" class="field">
							<input class="form-control" name="lingkup[`+last+`][realisasi]" placeholder="Realisasi" type="text"/>
						</td>
						<td style="" class="field v-middle">
							<button class="btn btn-sm btn-danger hapus_lingkup" type="button" style="border-radius:20px;" data-id=`+(c+2)+`><i class="fa fa-remove"></i></button>
						</td>
					</tr>
				`;

				$('.container-lingkup').append(html);
		        $('input[name=last_lingkup]').val(last)
		});

		$(document).on('click', '.hapus_lingkup', function (e){
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#table-lingkup');
			var rows = table.find('tbody tr');

			$.each(rows, function(key, value){
				console.log($(this).data("id"))
				table.find('.numbur-'+$(this).data("id")).html(key+1);
			});
		});

    	$('#summernote').summernote({
		  toolbar: [
		    ['style', ['style']],
		    ['fontname', ['fontname']],
		    ['font', ['strikethrough', 'superscript', 'subscript']],
		    ['fontsize', ['fontsize']],
		    ['color', ['color']],
		    ['para', ['ul', 'ol', 'paragraph']],
		    ['height', ['height']]
		  ]
		});
    	$('#myTab li:first-child a').tab('show')
    	var d = new Date();
    	var full = d.getFullYear();
    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
    	var last = new Date(lastDate);
    	if($('input[name=tahun]').val() > full){
    		var min = minDates;
    	}else{
    		var min = d;
    	}

    	$('.tanggal').datepicker({
    		orientation: "bottom left",
    		todayHighlight: true,
    		autoclose:true,
    		format: 'dd-mm-yyyy',
    		defaultViewDate: {year:$('input[name=tahun]').val(), month:0, day:1},
    		yearRange: '-0:+1',
    		startDate: minDates,
    		endDate: last,
    		hideIfNoPrevNext: true,
    	});

    	$('.cari-operasional').each(function(){
            var id = $(this).data("id");
            var op_id = $(this).data("val");
            var cek = $(this).data("cari");
            var val = $(this).val();
            $.ajax({
				url: '{{ url('ajax/option/get-kriteria-temuans') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					id: cek
				},
			})
			.done(function(response) {
				$('.kriteria_operasional_'+id).html(response[1]);
			})
			.fail(function() {
				console.log("error");
			});

			var detail = id;
			var data = $('select[name="fokus_audit[0][detail]['+detail+'][tanggapan]"] option:selected').val();
			if(data == 0){
				$('.tanggapan_operasional-'+detail).hide();
				$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", true);
				$('input[name="fokus_audit[0][detail]['+detail+'][tanggal]"]').prop("disabled", false);
			}else{
				$('.tanggapan_operasional-'+detail).show();
				$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", false);
				$('input[name="fokus_audit[0][detail]['+detail+'][tanggal]"]').val('');
				$('input[name="fokus_audit[0][detail]['+detail+'][tanggal]"]').prop("disabled", true);
			}
            $.ajax({
                url: '{{ url('ajax/option/get-kka') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: op_id
                },
            }).done(function(response) {
            	if(response.length > 0){
	            	$('#op-'+op_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreview: JSON.parse(response[0].replace(/&quot;/g,'"')),
						initialPreviewAsData: true,
						initialPreviewConfig: JSON.parse(response[1].replace(/&quot;/g,'"')),
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}else{
            		$('#op-'+op_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreviewAsData: true,
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						dropZoneTitle: 'Tidak ada data',
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}
				$('.kv-file-upload').prop('disabled', false);
				$('.kv-file-zoom').prop('disabled', false);
            }).fail(function() {
                console.log("error");
            });

            $.ajax({
                url: '{{ url('ajax/option/get-ba') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: op_id
                },
            }).done(function(response) {
            	if(response.length > 0){
	            	$('#ops-'+op_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreview: JSON.parse(response[0].replace(/&quot;/g,'"')),
						initialPreviewAsData: true,
						initialPreviewConfig: JSON.parse(response[1].replace(/&quot;/g,'"')),
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}else{
            		$('#ops-'+op_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreviewAsData: true,
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						dropZoneTitle: 'Tidak ada data',
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}
				$('.kv-file-upload').prop('disabled', false);
				$('.kv-file-zoom').prop('disabled', false);
            }).fail(function() {
                console.log("error");
            });
        });

        $('.cari-keuangan').each(function(){
            var id = $(this).data("id");
            var keu_id = $(this).data("val");
            var cek = $(this).data("cari");
            var val = $(this).val();
			var detail = id;
			$.ajax({
				url: '{{ url('ajax/option/get-kriteria-temuans') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					id: cek
				},
			})
			.done(function(response) {
				$('.kriteria_keuangan_'+id).html(response[1]);
			})
			.fail(function() {
				console.log("error");
			});

			var data = $('select[name="fokus_audit[1][detail]['+detail+'][tanggapan]"] option:selected').val();
			if(data == 0){
				$('.tanggapan_keuangan-'+detail).hide();
				$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", true);
				$('input[name="fokus_audit[1][detail]['+detail+'][tanggal]"]').prop("disabled", false);
			}else{
				$('.tanggapan_keuangan-'+detail).show();
				$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", false);
				$('input[name="fokus_audit[1][detail]['+detail+'][tanggal]"]').val('');
				$('input[name="fokus_audit[1][detail]['+detail+'][tanggal]"]').prop("disabled", true);
			}
			$.ajax({
                url: '{{ url('ajax/option/get-kka') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: keu_id
                },
            }).done(function(response) {
            	if(response.length > 0){
	            	$('#keu-'+keu_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreview: JSON.parse(response[0].replace(/&quot;/g,'"')),
						initialPreviewAsData: true,
						initialPreviewConfig: JSON.parse(response[1].replace(/&quot;/g,'"')),
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}else{
            		$('#keu-'+keu_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreviewAsData: true,
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						dropZoneTitle: 'Tidak ada data',
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}
				$('.kv-file-upload').prop('disabled', false);
				$('.kv-file-zoom').prop('disabled', false);
            }).fail(function() {
                console.log("error");
            });

            $.ajax({
                url: '{{ url('ajax/option/get-ba') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: keu_id
                },
            }).done(function(response) {
            	if(response.length > 0){
	            	$('#keus-'+keu_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreview: JSON.parse(response[0].replace(/&quot;/g,'"')),
						initialPreviewAsData: true,
						initialPreviewConfig: JSON.parse(response[1].replace(/&quot;/g,'"')),
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}else{
            		$('#keus-'+keu_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreviewAsData: true,
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						dropZoneTitle: 'Tidak ada data',
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}
				$('.kv-file-upload').prop('disabled', false);
				$('.kv-file-zoom').prop('disabled', false);
            }).fail(function() {
                console.log("error");
            });
        });

        $('.cari-sistem').each(function(){
            var id = $(this).data("id");
            var sis_id = $(this).data("val");
            var cek = $(this).data("cari");
            var val = $(this).val();

            $.ajax({
				url: '{{ url('ajax/option/get-kriteria-temuans') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					id: cek
				},
			})
			.done(function(response) {
				$('.kriteria_sistem_'+id).html(response[1]);
			})
			.fail(function() {
				console.log("error");
			});

			var detail = id;
			var data = $('select[name="fokus_audit[2][detail]['+detail+'][tanggapan]"] option:selected').val();
			if(data == 0){
				$('.tanggapan_sistem-'+detail).hide();
				$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", true);
				$('input[name="fokus_audit[2][detail]['+detail+'][tanggal]"]').prop("disabled", false);
			}else{
				$('.tanggapan_sistem-'+detail).show();
				$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", false);
				$('input[name="fokus_audit[2][detail]['+detail+'][tanggal]"]').val('');
				$('input[name="fokus_audit[2][detail]['+detail+'][tanggal]"]').prop("disabled", true);
			}

			$.ajax({
                url: '{{ url('ajax/option/get-kka') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: sis_id
                },
            }).done(function(response) {
            	if(response.length > 0){
	            	$('#sis-'+sis_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreview: JSON.parse(response[0].replace(/&quot;/g,'"')),
						initialPreviewAsData: true,
						initialPreviewConfig: JSON.parse(response[1].replace(/&quot;/g,'"')),
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}else{
            		$('#sis-'+sis_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreviewAsData: true,
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						dropZoneTitle: 'Tidak ada data',
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}
				$('.kv-file-upload').prop('disabled', false);
				$('.kv-file-zoom').prop('disabled', false);
            }).fail(function() {
                console.log("error");
            });

            $.ajax({
                url: '{{ url('ajax/option/get-ba') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: sis_id
                },
            }).done(function(response) {
            	if(response.length > 0){
	            	$('#siss-'+sis_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreview: JSON.parse(response[0].replace(/&quot;/g,'"')),
						initialPreviewAsData: true,
						initialPreviewConfig: JSON.parse(response[1].replace(/&quot;/g,'"')),
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}else{
            		$('#siss-'+sis_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreviewAsData: true,
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						dropZoneTitle: 'Tidak ada data',
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}
				$('.kv-file-upload').prop('disabled', false);
				$('.kv-file-zoom').prop('disabled', false);
            }).fail(function() {
                console.log("error");
            });
        });

		$('.pilihan').on('change', function(){
			var id = $(this).data("id");
			var detail = $(this).data("detail");
			var data = $('select[name="fokus_audit['+id+'][detail]['+detail+'][tanggapan]"] option:selected').val();
			if(data == 0){
				if(id == 0){
					$('.tanggapan_operasional-'+detail).hide();
					$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", true);
				}else if(id == 1){
					$('.tanggapan_keuangan-'+detail).hide();
					$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", true);
				}else{
					$('.tanggapan_sistem-'+detail).hide();
					$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", true);
				}
				$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').prop("disabled", false);
			}else{
				if(id == 0){
					$('.tanggapan_operasional-'+detail).show();
					$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", false);
				}else if(id == 1){
					$('.tanggapan_keuangan-'+detail).show();
					$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", false);
				}else{
					$('.tanggapan_sistem-'+detail).show();
					$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", false);
				}
				$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').val('');
				$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').prop("disabled", true);
			}
		});
    	$('#myTab li:first-child a').tab('show')
    </script>
    @yield('js-extra')
@endpush

