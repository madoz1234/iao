<html>
<head>
<style>
        @font-face {
		    font-family: 'tahoma';
		    src: url({{ storage_path('fonts\tahoma.ttf') }}) format("truetype");
		    font-weight: 400;
		    font-style: normal;
		}

        @page {
            margin: 60px 25px;
            counter-reset: page;
        }

       body {
           margin-top: 3cm;
           margin-left: 1.5cm;
           margin-right: 1.5cm;
           margin-bottom: 2cm;
           font-style: normal;
       }

       .ui.table.bordered {
         border: solid black 1px;
         border-collapse: collapse;
         width: 100%;
         font-family : "tahoma", "Verdana", sans-serif;
       }

       .ui.table.bordered td {
         border: solid black .5px;
         border-collapse: collapse;
         padding:5px;
       }

       .ui.table.bordered td.center.aligned {
         text-align : center;
       }

       .page-break {
            page-break-after: always;
       }

       .pad{
       	padding-top: 5px;
       	padding-bottom: 5px;
       }

        header {
            position: fixed;
            top: -40px;
            left: 51px;
            right: 74px;
            height: 10px;
            text-align: center;
            font-size: 12px;
        }
		
       main {
         position: sticky;
         font-size : 12px;
         padding-top : -1cm;
         margin-top : 2px;
         border : none;
         width: 100%;
       }

       main p {
         margin-left: 5px;
         margin-right: 5px;
       }
       footer {
           position: fixed;
           bottom: -60px;
           left: 0px;
           right: 0px;
           height: 50px;
           text-align: center;
           line-height: 35px;
       }
       table.page_content {width: 100%; border: none; padding: 1mm; font-size: 12px;font-family : "tahoma", "Verdana", sans-serif;}
       table.page_header {width: 100%; border: none; padding: 2mm; font-size: 12px;font-family : "tahoma", "Verdana", sans-serif; text-align: left; margin-left: 50px;margin-top: 50px;}
       table.page_content_header {width: 100%; font-size: 12px;border-collapse: collapse;font-family : "tahoma", "Verdana", sans-serif;text-align: center; padding-bottom: 1px;
       	padding-top: 1px;}
</style>
</head>
<body>
	<header>
		<table border="1" class="page_content ui table bordered" style="page-break-inside: auto;">
			<tbody>
				<tr>
					<td style="width: 40%;border-top: 1px solid #ffffff;border-left: 1px solid #ffffff;padding: -2px;" colspan="2" rowspan="2">&nbsp;</td>
					<td style="text-align: center;padding: -2px;padding: -2px;" colspan="2">Form. IA 05</td>
				</tr>
				<tr>
					<td style="text-align: center;border-bottom: 1px solid #000000;padding: -2px;">Edisi : @if($edisi) {{ $edisi->edisi }} @else - @endif</td>
					<td style="text-align: center;border-bottom: 1px solid #000000;padding: -2px;">Revisi : @if($edisi) {{ $edisi->revisi }} @else - @endif</td>
				</tr>
				<tr>
					<td style="border-right: 1px solid #ffffff;border-top: 1px solid #ffffff;border-left: 1px solid #ffffff;padding-top: -12px;" colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<td style="width: 50%;padding: -2px;padding-left: 2px;padding-bottom: 1px;padding-top: 1px;">Nomor : {{ $records->memo }} - LHA/WK/IA/2020</td>
					<td style="width: 50%;padding: -2px;padding-left: 2px;padding-bottom: 1px;padding-top: 1px;" colspan="3">Tanggal : {{ DateToStringWday($records->tanggal) }}</td>
				</tr>
			</tbody>
		</table>
	</header>
  	<br>
	<main>
		<table class="table table-bordered m-t-none" style="font-size: 12px;padding-top: -50px;">
			<tbody>
				<tr>
					<td style="border-right: #ffffff;text-transform: uppercase;width: 100%;">LAPORAN HASIL AUDIT INTERNAL PADA @if($records->draftkka->program->penugasanaudit->rencanadetail->tipe_object ==2) PROYEK @endif {{ object_audit($records->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $records->draftkka->program->penugasanaudit->rencanadetail->object_id) }} No. AB : {{ getAb($records->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $records->draftkka->program->penugasanaudit->rencanadetail->object_id) }}</td>
				</tr>
				<tr>
					<td style="text-align:left;border-top: #ffffff;font-weight: bold;text-decoration: underline;" colspan="2"><br></td>
				</tr>
				<tr>
					<td style="text-align:left;border-top: #ffffff;font-weight: bold;text-decoration: underline;" colspan="2">RESUME</td>
				</tr>
				<tr>
					<td style="text-align:left;border-top: #ffffff;" colspan="2">Dari hasil audit yang dilakukan oleh Internal Audit Pada <b>@if($records->draftkka->program->penugasanaudit->rencanadetail->tipe_object ==2) Proyek @endif {{ object_audit($records->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $records->draftkka->program->penugasanaudit->rencanadetail->object_id) }} No. AB : {{ getAb($records->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $records->draftkka->program->penugasanaudit->rencanadetail->object_id) }} </b> sesuai Internal Memo nomor {{ $records->memo }} - LHA/WK/IA/2020 tanggal {{ DateToStringWday($records->tanggal) }}, dan berlangsung pada tanggal {{ DateToString($records->draftkka->program->detailpelaksanaan->first()->tgl).' - '.DateToString($records->draftkka->program->detailpelaksanaan->last()->tgl) }} kami laporkan hal-hal sbb:</td>
				</tr>
				<tr>
					<td style="text-align: left;font-size: 12px;border-top: 1px #ffffff;">
						<h5 style="font-size: 12px;">A. Aspek Operasional</h5>
						<table class="ui table bordered" style="width: 100%;font-size: 12px;">
							<tbody class="container-operasional">
								<tr>
									<td style="width: 50px;text-align: center;">No</td>
									<td style="text-align: center;">Keterangan</td>
								</tr>
								@if($records->detaillha->where('tipe', 0)->count() > 0)
									@php 
										$i=0;
									@endphp
									@foreach($records->detaillha->where('tipe', 0) as $key => $operasional)
										<tr data-id="0">
											<td style="text-align: center" class="numboor-0-{{$i}}">{{ $i+1 }}</td>
											<td class="field" style="text-align: left;">
												{{$operasional->keterangan}}
											</td>
										</tr>
										@php 
										$i++;
										@endphp
									@endforeach
								@endif
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td style="text-align: left;font-size: 12px;border-top: 1px #ffffff;">
						<h5 style="font-size: 12px;">B. Aspek Keuangan</h5>
						<table class="ui table bordered" style="width: 100%;font-size: 12px;">
							<tbody class="container-keuangan">
								<tr>
									<td style="width: 50px;text-align: center;">No</td>
									<td style="text-align: center;">Keterangan</td>
								</tr>
								@if($records->detaillha->where('tipe', 1)->count() > 0)
									@php 
										$i=0;
									@endphp
									@foreach($records->detaillha->where('tipe', 1) as $key => $keuangan)
										<tr data-id="0">
											<td style="text-align: center" class="numboor-0-{{$i}}">{{ $i+1 }}</td>
											<td class="field" style="text-align: left;">
												{{$keuangan->keterangan}}
											</td>
										</tr>
										@php 
										$i++;
										@endphp
									@endforeach
								@endif
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td style="text-align: left;font-size: 12px;border-top: 1px #ffffff;">
						<h5 style="font-size: 12px;">C. Aspek Sistem</h5>
						<table class="ui table bordered" style="width: 100%;font-size: 12px;">
							<tbody class="container-sistem">
								<tr>
									<td style="width: 50px;text-align: center;">No</td>
									<td style="text-align: center;">Keterangan</td>
								</tr>
								@if($records->detaillha->where('tipe', 2)->count() > 0)
									@php 
										$i=0;
									@endphp
									@foreach($records->detaillha->where('tipe', 2) as $key => $sistem)
										<tr data-id="0">
											<td style="text-align: center" class="numboor-0-{{$i}}">{{ $i+1 }}</td>
											<td class="field" style="text-align: left;">
												{{$sistem->keterangan}}
											</td>
										</tr>
										@php 
										$i++;
										@endphp
									@endforeach
								@endif
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td style="text-align: left;font-size: 12px;border-top: 1px #ffffff;">
						<h5 style="font-size: 12px;">D. Kesimpulan</h5>
						<table class="ui table bordered" style="width: 100%;font-size: 12px;">
							<tbody class="container-keuangan">
								<tr>
									<td style="width: 50px;text-align: center;">No</td>
									<td style="text-align: center;">Keterangan</td>
								</tr>
								@if($records->detailkesimpulan->count() > 0)
									@php 
										$i=0;
									@endphp
									@foreach($records->detailkesimpulan as $koy => $kesimpulan)
										<tr data-id="0">
											<td style="text-align: center" class="numboor-0-{{$i}}">{{ $i+1 }}</td>
											<td class="field" style="text-align: left;">
												{{$kesimpulan->kesimpulan}}
											</td>
										</tr>
										@php 
										$i++;
										@endphp
									@endforeach
								@endif
							</tbody>
						</table>
					</td>
				</tr>
				<tr style="border:#ffffff;">
					<td style="text-align: left;border-top:#ffffff;" colspan="3">
						<br>
						<br>
						<b>SVP - Internal Audit</b><br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						(Pius Sutrisno Riyanto)
						<br>
						<br>
						<br>
					</td>
				</tr>
			</tbody>
		</table>
	</main>
</body>
</html>