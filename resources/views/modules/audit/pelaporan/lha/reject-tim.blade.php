<form action="{{ route($routes.'.saveReject', $record->id) }}" method="POST" id="formData">
	@method('PATCH')
    @csrf
    <input type="hidden" name="id" value="{{$record->id}}">
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Penolakan</h5>
    </div>
    <div class="modal-body">
        <div class="form-group">
            <div class="form-group field">
            	<label class="control-label">Alasan Penolakan</label>
        		<textarea class="form-control" name="ket_svp" id="exampleFormControlTextarea1" placeholder="Alasan Penolakan" rows="3"></textarea>
            </div>           
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>
