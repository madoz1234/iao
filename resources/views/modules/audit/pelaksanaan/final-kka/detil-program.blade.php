@extends('layouts.form')
@section('title', 'Detil Program Audit '.$record->tinjauan->penugasanaudit->rencanadetail->rencanaaudit->tahun)
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
    <div class="panel-body" style="padding-bottom: 0px;">
		<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
			@csrf
			<input type="hidden" name="tinjauan_id" value="{{ $record->tinjauan->id }}">
			@php 
				$data_operasional = array();
				foreach($record->detailanggota->where('fungsi', 1) as $operasional) {
				 $data_operasional[] = $operasional->user->name;
				}

				$data_keuangan = array();
				foreach($record->detailanggota->where('fungsi', 2) as $keuangan) {
				 $data_keuangan[] = $keuangan->user->name;
				}

				$data_sistem = array();
				foreach($record->detailanggota->where('fungsi', 3) as $sistem) {
				 $data_sistem[] = $sistem->user->name;
				}
			@endphp
			<div class="panel-body" style="padding-bottom: 0px;">
				<div class="form-row">
					<div class="form-group col-md-12">
						<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
							<tr>
								<td style="width: 10%;border: 1px #ffffff;">Tipe</td>
								<td style="width: 1%;border: 1px #ffffff;">:</td>
								<td style="text-align: left;border: 1px #ffffff;" class="field">
									@if($record->tinjauan->penugasanaudit->rencanadetail->rencanaaudit->tipe == 0)
										<span class="label label-info">Audit Reguler</span>
									@else 
										<span class="label label-primary">Audit Khusus</span>
									@endif
								</td>
							</tr>
							<tr>
								<td style="width: 10%;border: 1px #ffffff;">Kategori</td>
								<td style="width: 1%;border: 1px #ffffff;">:</td>
								<td style="text-align: left;border: 1px #ffffff;" class="field">
									@if($record->tinjauan->penugasanaudit->rencanadetail->tipe_object == 0)
										<span class="label label-success">Business Unit (BU)</span>
									@elseif($record->tinjauan->penugasanaudit->rencanadetail->tipe_object == 1)
										<span class="label label-info">Corporate Office (CO)</span>
									@elseif($record->tinjauan->penugasanaudit->rencanadetail->tipe_object == 2)
										<span class="label label-default">Project</span>
									@else 
										<span class="label label-primary">Anak Perusahaan</span>
									@endif
								</td>
							</tr>
							<tr>
								<td style="width: 10%;border: 1px #ffffff;">Objek Audit</td>
								<td style="width: 1%;border: 1px #ffffff;">:</td>
								<td style="text-align: left;border: 1px #ffffff;" class="field">
									{!! object_audit($record->tinjauan->penugasanaudit->rencanadetail->tipe_object, $record->tinjauan->penugasanaudit->rencanadetail->object_id)!!}
								</td>
							</tr>
							<tr>
								<td style="width: 10%;border: 1px #ffffff;">Ketua Tim</td>
								<td style="width: 1%;border: 1px #ffffff;">:</td>
								<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
									{{ $record->user->name }}
								</td>
							</tr>
							<tr>
								<td style="width: 10%;border: 1px #ffffff;">Auditor Operasional</td>
								<td style="width: 1%;border: 1px #ffffff;">:</td>
								<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
									<div>
										{{ implode(", ",$data_operasional) }}
									</div>
								</td>
							</tr>
							<tr>
								<td style="width: 10%;border: 1px #ffffff;">Auditor Keuangan</td>
								<td style="width: 1%;border: 1px #ffffff;">:</td>
								<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
									<div>
										{{ implode(", ",$data_keuangan) }}
									</div>
								</td>
							</tr>
							<tr>
								<td style="width: 10%;border: 1px #ffffff;">Auditor Sistem</td>
								<td style="width: 1%;border: 1px #ffffff;">:</td>
								<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
									<div>
										{{ implode(", ",$data_sistem) }}
									</div>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="panel-body" style="margin-top: -14px;">
				<div class="form-row">
					<div class="form-group col-md-12">
						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="a-tab" data-toggle="tab" href="#a" role="tab" aria-controls="home" aria-selected="true">A. RUANG LINGKUP</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="b-tab" data-toggle="tab" href="#b" role="tab" aria-controls="profile" aria-selected="false">B. SASARAN</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="c-tab" data-toggle="tab" href="#c" role="tab" aria-controls="messages" aria-selected="false">C. JADWAL AUDIT</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="d-tab" data-toggle="tab" href="#d" role="tab" aria-controls="settings" aria-selected="false">D. FOKUS AUDIT DAN LANGKAH KERJA</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="a" role="tabpanel" aria-labelledby="a-tab">
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr>
										<td scope="col-md-12" colspan="3" style="text-align: left;" class="field">
											<h5 style="font-weight: bold;font-size: 12px;">A. RUANG LINGKUP</h5>
											<p style="text-align: justify;text-justify: inter-word;">{{ $record->ruang_lingkup }}</p>
										</td>
									</tr>
								</table>
								<div class="form-group col-md-12">
									<div class="text-right">
										<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
										<button type="button" class="btn btn-info page1">Selanjutnya</button>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="b" role="tabpanel" aria-labelledby="b-tab">
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr>
										<td scope="col-md-12" colspan="3" style="text-align: left;" class="field">
											<h5 style="font-weight: bold;font-size: 12px;">B. SASARAN</h5>
											<p style="text-align: justify;text-justify: inter-word;">{{ $record->sasaran }}</p>
										</td>
									</tr>
								</table>
								<div class="form-group col-md-12">
									<div class="text-right">
										<button type="button" class="btn btn-cancel kembali1">Sebelumnya</button>
										<button type="button" class="btn btn-info page2">Selanjutnya</button>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="c" role="tabpanel" aria-labelledby="c-tab">
		    					<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;">
									<tr>
										<td scope="col-md-12" colspan="3" style="text-align: left;">
											<h5 style="font-weight: bold;font-size: 12px;">C. JADWAL AUDIT</h5>
											<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
												<tr style="text-align: center;">
													<td scope="col" style="text-align: left;width: 100%; font-weight: bold;border: 1px #ffffff;">C1. PERSIAPAN
													</td>
												</tr>
												<tr style="text-align: center;">
													<td scope="col" style="text-align: left;border: 1px #ffffff;">
														<table id="table-persiapan" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
															<thead>
																<th style="width: 50px;">No</th>
																<th style="text-align: center;">Persiapan</th>
															</thead>
															<tbody class="container-persiapan">
																	@foreach($record->detailjadwal as $kiy => $jadwal)
																		<tr class="data-persiapan-{{ $kiy+1 }}" data-id="{{ $kiy+1 }}">
																			<td style="text-align: center;margin-top: 9px;padding-top: 21px;">{{ $kiy+1 }}</td>
																			<td class="field">
																				<p style="text-align: justify;text-justify: inter-word;">{{ $jadwal->keterangan }}</p>
																			</td>
																		</tr>
																	@endforeach
															</tbody>
														</table>
													</td>
												</tr>
												<tr style="text-align: center;">
													<td scope="col" style="text-align: left;width: 100%; font-weight: bold;border: 1px #ffffff;font-size: 12px;">C2. PELAKSANAAN AUDIT
													</td>
												</tr>
												<tr style="text-align: center;">
													<td scope="col" style="text-align: left;border: 1px #ffffff;">
														<table id="table-pelaksanaan" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
															<thead>
																<tr>
																	<th style="text-align: center;" rowspan="2">Hari</th>
																	<th style="text-align: center;" rowspan="2">Tanggal</th>
																	<th style="text-align: center; text-align: center;" colspan="2">Waktu</th>
																	<th style="text-align: center;border-left: 1px solid rgba(34,36,38,.1);" rowspan="2">Item</th>
																</tr>
																<tr>
																	<th style="text-align: center;border-top: 1px solid rgba(34,36,38,.1);">Mulai</th>
																	<th style="text-align: center;border-top: 1px solid rgba(34,36,38,.1);">Selesai</th>
																</tr>
															</thead>
															<tbody class="container-pelaksanaan">
																@foreach($record->detailpelaksanaan as $kiy => $pelaksanaan)
																	<tr class="detail_cek data-pelaksanaan-{{$kiy}}" data-id="{{$kiy}}">
																		<td class="data_rowspan-{{$kiy}} numboor-{{$kiy}}" style="text-align: center;width: 100px;" rowspan="{{$pelaksanaan->detailpelaksanaan->count()}}">{{ romawi(($kiy+1)) }}</td>
																		<td class="data_rowspan-{{ $kiy }} field" style="text-align: center;width: 200px;" rowspan="{{$pelaksanaan->detailpelaksanaan->count()}}">
																			{{DateToString($pelaksanaan->tgl)}}
																		</td>
																		<td style="text-align: center;width: 150px;" class="field">
																			{{$pelaksanaan->detailpelaksanaan->first()->mulai}}
																		</td>
																		<td style="text-align: center;width: 150px;" class="field">
																			{{$pelaksanaan->detailpelaksanaan->first()->selesai}}
																		</td>
																		<td style="text-align: left;width: 800px;text-align: justify;text-justify: inter-word;" class="field">
																			{{$pelaksanaan->detailpelaksanaan->first()->keterangan}}
																		</td>
																	</tr>
																	@foreach($pelaksanaan->detailpelaksanaan as $koy => $detail)
																		@if($koy > 0)
																			<tr class="detail-pelaksanaan-{{ $kiy }} detail-{{ $kiy }}-{{ $koy }}">
																				<td style="text-align: center;width: 150px;" class="field">
																					{{$detail->mulai}}
																				</td>
																				<td style="text-align: center;width: 150px;" class="field">
																					{{$detail->selesai}}
																				</td>
																				<td style="text-align: left;width: 800px;text-align: justify;text-justify: inter-word;" class="field">
																					{{$detail->keterangan}}
																				</td>
																			</tr>
																		@endif
																	@endforeach
																@endforeach
															</tbody>
														</table>
													</td>
												</tr>
												<tr style="text-align: center;">
													<td scope="col" style="text-align: left;width: 100%; font-weight: bold;border: 1px #ffffff;font-size: 12px;">C3. PENYELESAIAN TUGAS AUDIT
													</td>
												</tr>
												<tr style="text-align: center;">
													<td scope="col" style="text-align: left;border: 1px #ffffff;">
														<table id="table-penyelesaian" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
															<thead>
																<tr>
																	<th style="text-align: center; text-align: center;border-bottom: 1px solid rgba(34,36,38,.1);" rowspan="2">No</th>
																	<th style="text-align: center; text-align: center;border-bottom: 1px solid rgba(34,36,38,.1);" colspan="2">Tanggal</th>
																	<th style="text-align: center;border-left: 1px solid rgba(34,36,38,.1);" rowspan="2">Item</th>
																</tr>
																<tr>
																	<th style="text-align: center;">Mulai</th>
																	<th style="text-align: center;">Selesai</th>
																</tr>
															</thead>
															<tbody class="container-penyelesaian">
																@foreach($record->detailselesai as $kuy => $selesai)
																	<tr class="data-penyelesaian-{{$kuy+1}}" data-id="{{$kuy+1}}">
																		<td style="text-align: center;width: 50px;" class="numbor-{{$kuy+1}}">{{$kuy+1}}</td>
																		<td style="text-align: center;width: 250px;" class="field">
																			{{DateToString($selesai->tgl_mulai)}}
																		</td>
																		<td style="text-align: center;width: 250px;" class="field">
																			{{DateToString($selesai->tgl_selesai)}}
																		</td>
																		<td style="text-align: left;text-align: justify;text-justify: inter-word;" class="field">{{$selesai->keterangan}}
																		</td>
																	</tr>
																@endforeach
															</tbody>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<div class="form-group col-md-12">
									<div class="text-right">
										<button type="button" class="btn btn-cancel kembali2">Sebelumnya</button>
										<button type="button" class="btn btn-info page3">Selanjutnya</button>
									</div>
								</div>
		    				</div>
							<div class="tab-pane" id="d" role="tabpanel" aria-labelledby="d-tab">
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr>
										<td scope="col-md-12" colspan="3" style="text-align: left;">
											<h5 style="font-weight: bold;font-size: 12px;">D. FOKUS AUDIT DAN LANGKAH KERJA</h5>
											<table id="table-fokus" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
												<thead>
													<tr>
														<th style="text-align: center;" rowspan="2">No</th>
														<th style="text-align: center;" colspan="2" rowspan="2">Uraian</th>
														<th style="text-align: center;border-bottom: 1px solid rgba(34,36,38,.1);border-left: 1px solid rgba(34,36,38,.1);" colspan="2">Waktu Audit</th>
														<th style="text-align: center;border-right: 1px solid rgba(34,36,38,.1);" rowspan="2" colspan="3">Keterangan</td>
														</tr>
														<tr>
															<th style="">Rencana</th>
															<th style="">Realisasi</th>
														</tr>
													</thead>
													<tbody>
														{{-- OPERASIONAL --}}
														<tr>
															<td style="width: 20px;text-align: center;font-weight: bold;">I</td>
															<td style="text-align: left;font-weight: bold;border-right: 1px #ffffff;" colspan="5">OPERASIONAL</td>
															<td style="text-align: center;font-weight: bold;width:50px;border-right: 1px solid rgba(34,36,38,.1);border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
															</td>
														</tr>
															@php 
																$i=1;
															@endphp
														@foreach($record	->detailkerja->where('bidang', 1) as $kiy => $operasional)
															@php
															if(!empty($operasional->fokusaudit->langkahkerja->first()->detail)){
																$row = $operasional->fokusaudit->langkahkerja->first()->detail->count();
															}else{
																$row=0;
															}
															@endphp
															<tr class="data_operasional data_fokus_audit_operasional_{{$kiy}}" data-id="{{$kiy}}">
																<td class="for_rowspan_operasional_{{$kiy}}" style="text-align: center;font-weight: bold;" rowspan="{{$row+3}}">{{$i}}</td>
																<td style="width: 20px;" rowspan="3">&nbsp;</td>
																<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="5">Fokus Audit :</td>
															</tr>
															<tr class="detail_fokus_audit_operasional_{{$kiy}}" style="border-bottom: 1px solid rgba(34,36,38,.1);">
																<td style="width: 800px;" class="field">
																	{{ $operasional->fokusaudit->audit }}
																</td>
																<td style="width: 150px;" class="field">
																	{{ DateToString($operasional->rencana) }}
																</td>
																<td style="width: 150px;" class="field">
																	{{ DateToString($operasional->realisasi) }}
																</td>
																<td colspan="2" style="border-right: 1px solid rgba(34,36,38,.1);text-align: left;text-align: justify;text-justify: inter-word;">
																	@if($operasional->keterangan)
																	{{$operasional->keterangan}}
																	@else 
																	-
																	@endif
																</td>
															</tr>
															<tr class="langkah_kerja_operasional_{{$kiy}}">
																<td style="text-align: left;" colspan="6">Langkah Kerja</td>
															</tr>
															@if(!empty($operasional->fokusaudit->langkahkerja->first()->detail))
																@foreach($operasional->fokusaudit->langkahkerja->first()->detail as $kuy => $detil)
																<tr class="langkah-kerja-operasional-{{$kiy}}">
																	<td>{{column_letter($kuy+1)}}</td>
																	<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);text-align: justify;text-justify: inter-word;" colspan="5">{{$detil->deskripsi}}</td>
																</tr>
																@endforeach
															@else
															@endif
															@php 
															$i++;
															@endphp
														@endforeach

														{{-- KEUANGAN --}}
														<tr>
															<td style="width: 20px;text-align: center;font-weight: bold;">II</td>
															<td style="text-align: left;font-weight: bold;border-right: 1px #ffffff;" colspan="5">KEUANGAN</td>
															<td style="text-align: center;font-weight: bold;width:50px;border-right: 1px solid rgba(34,36,38,.1);border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
															</td>
														</tr>
														@php 
														$i=1;
														@endphp
														@foreach($record	->detailkerja->where('bidang', 2) as $kiy => $keuangan)
														@php
														if(!empty($keuangan->fokusaudit->langkahkerja->first()->detail)){
															$row = $keuangan->fokusaudit->langkahkerja->first()->detail->count();
														}else{
															$row=0;
														}
														@endphp
														<tr class="data_keuangan data_fokus_audit_keuangan_{{$kiy}}" data-id="{{$kiy}}">
															<td class="for_rowspan_keuangan_{{$kiy}}" style="text-align: center;font-weight: bold;" rowspan="{{$row+3}}">{{$i}}</td>
															<td style="width: 20px;" rowspan="3">&nbsp;</td>
															<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="5">Fokus Audit :</td>
														</tr>
														<tr class="detail_fokus_audit_keuangan_{{$kiy}}" style="border-bottom: 1px solid rgba(34,36,38,.1);">
															<td style="width: 800px;" class="field">
																{{ $keuangan->fokusaudit->audit }}
															</td>
															<td style="width: 150px;" class="field">
																{{ DateToString($keuangan->rencana) }}
															</td>
															<td style="width: 150px;" class="field">
																{{ DateToString($keuangan->realisasi) }}
															</td>
															<td colspan="2" style="border-right: 1px solid rgba(34,36,38,.1);text-align: left;text-align: justify;text-justify: inter-word;">
																@if($keuangan->keterangan)
																{{$keuangan->keterangan}}
																@else 
																-
																@endif
															</td>
														</tr>
														<tr class="langkah_kerja_keuangan_{{$kiy}}">
															<td style="text-align: left;" colspan="6">Langkah Kerja</td>
														</tr>
														@if(!empty($keuangan->fokusaudit->langkahkerja->first()->detail))
														@foreach($keuangan->fokusaudit->langkahkerja->first()->detail as $kuy => $detil)
														<tr class="langkah-kerja-keuangan-{{$kiy}}">
															<td>{{column_letter($kuy+1)}}</td>
															<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);text-align: justify;text-justify: inter-word;" colspan="5">{{$detil->deskripsi}}</td>
														</tr>
														@endforeach
														@else
														@endif
														@php 
														$i++;
														@endphp
														@endforeach

														{{-- SISTEM --}}
														<tr>
															<td style="width: 20px;text-align: center;font-weight: bold;">III</td>
															<td style="text-align: left;font-weight: bold;border-right: 1px #ffffff;" colspan="5">SISTEM</td>
															<td style="text-align: center;font-weight: bold;width:50px;border-right: 1px solid rgba(34,36,38,.1);border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
															</td>
														</tr>
														@php 
														$i=1;
														@endphp
														@foreach($record	->detailkerja->where('bidang', 3) as $kiy => $sistem)
														@php
														if(!empty($sistem->fokusaudit->langkahkerja->first()->detail)){
															$row = $sistem->fokusaudit->langkahkerja->first()->detail->count();
														}else{
															$row=0;
														}
														@endphp
														<tr class="data_sistem data_fokus_audit_sistem_{{$kiy}}" data-id="{{$kiy}}">
															<td class="for_rowspan_sistem_{{$kiy}}" style="text-align: center;font-weight: bold;" rowspan="{{$row+3}}">{{$i}}</td>
															<td style="width: 20px;" rowspan="3">&nbsp;</td>
															<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="5">Fokus Audit :</td>
														</tr>
														<tr class="detail_fokus_audit_sistem_{{$kiy}}" style="border-bottom: 1px solid rgba(34,36,38,.1);">
															<td style="width: 800px;" class="field">
																{{ $sistem->fokusaudit->audit }}
															</td>
															<td style="width: 150px;" class="field">
																{{ DateToString($sistem->rencana) }}
															</td>
															<td style="width: 150px;" class="field">
																{{ DateToString($sistem->realisasi) }}
															</td>
															<td colspan="2" style="border-right: 1px solid rgba(34,36,38,.1);text-align: left;text-align: justify;text-justify: inter-word;">
																@if($sistem->keterangan)
																{{$sistem->keterangan}}
																@else 
																-
																@endif
															</td>
														</tr>
														<tr class="langkah_kerja_sistem_{{$kiy}}">
															<td style="text-align: left;" colspan="6">Langkah Kerja</td>
														</tr>
														@if(!empty($sistem->fokusaudit->langkahkerja->first()->detail))
														@foreach($sistem->fokusaudit->langkahkerja->first()->detail as $kuy => $detil)
														<tr class="langkah-kerja-sistem-{{$kiy}}">
															<td>{{column_letter($kuy+1)}}</td>
															<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);text-align: justify;text-justify: inter-word;" colspan="5">{{$detil->deskripsi}}</td>
														</tr>
														@endforeach
														@else
														@endif
														@php 
														$i++;
														@endphp
														@endforeach
													</tbody>
												</table>
											</table>
										</td>
									</tr>
								</table>
								<div class="form-group col-md-12">
									<div class="text-right">
										<button type="button" class="btn btn-cancel kembali3">Sebelumnya</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
    </div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script> 
    	$('#myTab li:first-child a').tab('show')
    	$('.btn').tooltip('enable');
        $('.tanggal').datepicker({
            format: 'mm/dd/yyyy',
		    startDate: '-0d',
            orientation: "auto",
            autoclose:true,
        });
        $('.waktu').clockpicker({
            autoclose:true,
            'default': 'now',
        });

        $(document).on('click', '.page1', function (e){
    		$('#a-tab').removeClass('active');
    		$('#a-tab').closest('li').removeClass('active');
    		$('#b-tab').addClass('active');
    		$('#b-tab').closest('li').addClass('active');
    		$('#a').removeClass('active');
    		$('#b').addClass('active');
		});

		$(document).on('click', '.kembali1', function (e){
    		$('#a-tab').addClass('active');
    		$('#a-tab').closest('li').addClass('active');
    		$('#b-tab').removeClass('active');
    		$('#b-tab').closest('li').removeClass('active');
    		$('#a').addClass('active');
    		$('#b').removeClass('active');
		});

		$(document).on('click', '.page2', function (e){
    		$('#b-tab').removeClass('active');
    		$('#b-tab').closest('li').removeClass('active');
    		$('#c-tab').addClass('active');
    		$('#c-tab').closest('li').addClass('active');
    		$('#b').removeClass('active');
    		$('#c').addClass('active');
		});

		$(document).on('click', '.kembali2', function (e){
    		$('#b-tab').addClass('active');
    		$('#b-tab').closest('li').addClass('active');
    		$('#c-tab').removeClass('active');
    		$('#c-tab').closest('li').removeClass('active');
    		$('#b').addClass('active');
    		$('#c').removeClass('active');
		});

		$(document).on('click', '.page3', function (e){
    		$('#c-tab').removeClass('active');
    		$('#c-tab').closest('li').removeClass('active');
    		$('#d-tab').addClass('active');
    		$('#d-tab').closest('li').addClass('active');
    		$('#c').removeClass('active');
    		$('#d').addClass('active');
		});

		$(document).on('click', '.kembali3', function (e){
    		$('#c-tab').addClass('active');
    		$('#c-tab').closest('li').addClass('active');
    		$('#d-tab').removeClass('active');
    		$('#d-tab').closest('li').removeClass('active');
    		$('#c').addClass('active');
    		$('#d').removeClass('active');
		});

        $('.cari').on('change', function(){
        	var id = $(this).data('id');
        	var value = parseInt(this.value);
        	$.ajax({
				url: '{{ url('ajax/option/get-langkah') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					id: id,
					value: value,
				},
			})
			.done(function(response) {
				var rowz = $('tr.langkah-kerja-'+id).remove();
				var rowspan = response[1].length;
				$('.for_rowspan_'+id).attr('rowspan', parseInt((rowspan+3)));
				$.each(response[1], function(key, value){
					if(key==0){
						$(value).insertAfter('.langkah_kerja_'+id);
					}else{
						$(value).insertAfter('.data_detail-'+id+'-'+(key-1));
					}
				});
			})
		});
        function romawi(number){
        	var map = [1,4,5,9,10,40,50,90,100,400,500,900,1000];  
        	var roma = ["I","IV","V","IX","X","XL","L","XC","C","CD","D","CM","M"];  
        	var hasil='';  

        	for(var i=12; i >=0; i--) {  
        		while(number >= map[i]) {  
        			number = number - map[i];  
        			hasil = hasil + roma[i];  
        		}  
        	}  
		    return hasil;
        }
    	$('#example > tbody  > tr').each(function(key, value){
    		var detail_id = $(this).data("detail");
    		var value = $(this).data("value");
    		$.ajax({
				url: '{{ url('ajax/option/get-kategori') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					kategori: $('select[name="detail['+detail_id+'][kategori]"]').val()
				},
			})
			.done(function(response) {
				$('select[name="detail['+detail_id+'][object_audit]"]').html(response);
				$('select[name="detail['+detail_id+'][object_audit]"]').children('option[value="'+value+'"]').prop('selected',true);
				$('select[name="detail['+detail_id+'][object_audit]"]').selectpicker("refresh");
			})
			.fail(function() {
				console.log("error");
			});
    	});
    	$('.rencana').datepicker({
            format: "mm-yyyy",
		    viewMode: "months", 
		    minViewMode: "months",
            orientation: "auto",
            autoclose:true
        });
    	$(document).on('click', '.tambah_persiapan', function(e){
    		var last = parseInt($('input[name=last_persiapan]').val()) + 1;
			var rowCount = $('#table-persiapan > tbody > tr').length;
			var c = rowCount-1;
			var html = `
					<tr class="data-persiapan-`+(c+2)+`" data-id=`+(c+2)+`>
						<td style="text-align: center;margin-top: 9px;padding-top: 21px;">
							<label style="margin-top: 7px;" class="numboor-`+(c+2)+`">`+(c+2)+`</label></td>
						<td class="field">
							<textarea class="form-control" name="detail[0][persiapan][`+last+`][persiapan]" id="exampleFormControlTextarea1" placeholder="Persiapan" rows="2"></textarea>
						</td>
						<td style="text-align: center;">
							<button class="btn btn-sm btn-danger hapus_persiapan" type="button" style="border-radius:20px;" data-id=`+(c+2)+`><i class="fa fa-remove"></i></button>
						</td>
					</tr>
				`;

				$('.container-persiapan').append(html);
		        $('input[name=last_persiapan]').val(last)
		});

		$(document).on('click', '.hapus_persiapan', function (e){
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#table-persiapan');
			var rows = table.find('tbody tr');

			$.each(rows, function(key, value){
				console.log($(this).data("id"))
				table.find('.numboor-'+$(this).data("id")).html(key+1);
			});
		});

		$(document).on('click', '.tambah_pelaksanaan', function(e){
    		var last = parseInt($('input[name=last_pelaksanaan]').val()) + 1;
			var rowCount = $('#table-pelaksanaan > tbody > tr.detail_cek').length;
			var c = rowCount-1;
			var html = `
					<tr class="detail_cek data-pelaksanaan-`+(c+2)+`" data-id=`+(c+2)+`>
						<td class="data_rowspan-`+(c+2)+` numboor-`+(c+2)+`" style="text-align: center;width: 100px;" rowspan="">`+romawi(c+2)+`</td>
						<td class="data_rowspan-`+(c+2)+` field" style="text-align: center;width: 200px;" rowspan="">
							<input class="form-control tanggal" id="tanggal" name="detail[0][pelaksanaan][`+(c+2)+`][tanggal]" placeholder="Tanggal" type="text"/>
						</td>
						<td style="text-align: center;width: 150px;" class="field">
							<input class="form-control waktu" id="waktu" name="detail[0][pelaksanaan][`+(c+2)+`][detail][0][mulai]" placeholder="Mulai" type="text"/>
						</td>
						<td style="text-align: center;width: 150px;" class="field">
							<input class="form-control waktu" id="waktu" name="detail[0][pelaksanaan][`+(c+2)+`][detail][0][selesai]" placeholder="Selesai" type="text"/>
						</td>
						<td style="text-align: center;width: 800px;" class="field">
							<input class="form-control" id="item" name="detail[0][pelaksanaan][`+(c+2)+`][detail][0][item]" placeholder="Item" type="text"/>
						</td>
						<td style="text-align: center;width: 50px;">
							<button class="btn btn-outline-success btn-sm tambah_detail" type="button" data-id="`+(c+2)+`" data-toggle="tooltip" data-placement="left" title="Tambah Item" style="border-radius: 20px;"><i class="fa fa-plus"></i></button>
						</td>
						<td class="data_rowspan-`+(c+2)+`" style="text-align: center;width: 50px;" rowspan="">
							<button class="btn btn-sm btn-danger hapus_pelaksanaan" type="button" data-id="`+(c+2)+`" data-toggle="tooltip" data-placement="left" title="Hapus Pelaksanaan" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
						</td>
						<input type="hidden" name="last_detail_`+(c+2)+`" value="0">
					</tr>
				`;

				$('.container-pelaksanaan').append(html);
		        $('input[name=last_pelaksanaan]').val((c+2))
		        $('.btn').tooltip('enable');
		        $('.tanggal').datepicker({
		        	format: 'mm/dd/yyyy',
		        	startDate: '-0d',
		        	orientation: "auto",
		        	autoclose:true,
		        });
		        $('.waktu').clockpicker({
		        	autoclose:true,
		        	'default': 'now',
		        });
		});
		$(document).on('click', '.hapus_pelaksanaan', function (e){
			var id =$(this).data("id");
			var row = $(this).closest('tr');
			row.remove();

			var rowz = $('tr.detail-pelaksanaan-'+id).remove();
			var table = $('#table-pelaksanaan');
			var rows = table.find('tbody tr.detail_cek');

			$.each(rows, function(key, value){
				function romawi(number){
					var map = [1,4,5,9,10,40,50,90,100,400,500,900,1000];  
					var roma = ["I","IV","V","IX","X","XL","L","XC","C","CD","D","CM","M"];  
					var hasil='';  

					for(var i=12; i >=0; i--) {  
						while(number >= map[i]) {  
							number = number - map[i];  
							hasil = hasil + roma[i];  
						}  
					}  
					return hasil;
				}
				table.find('.numboor-'+$(this).data("id")).html(romawi(key+1));
			});
		});

		$(document).on('click', '.tambah_detail', function(e){
			var id = $(this).data("id");
    		var last1 = parseInt($('input[name=last_pelaksanaan]').val());
    		var last2 = parseInt($('input[name=last_detail_'+id+']').val()) + 1;
			var row = $('#table-pelaksanaan > tbody > tr.detail-pelaksanaan-'+id).length;
			var c = row-1;
			var html = `
					<tr class="detail-pelaksanaan-`+id+` detail-`+id+`-`+(c+2)+`">
						<td style="text-align: center;width: 150px;" class="field">
							<input class="form-control waktu" id="waktu" name="detail[0][pelaksanaan][`+id+`][detail][`+last2+`][mulai]" placeholder="Mulai" type="text"/>
						</td>
						<td style="text-align: center;width: 150px;" class="field">
							<input class="form-control waktu" id="waktu" name="detail[0][pelaksanaan][`+id+`][detail][`+last2+`][selesai]" placeholder="Selesai" type="text"/>
						</td>
						<td style="text-align: center;width: 800px;" class="field">
							<input class="form-control" id="item" name="detail[0][pelaksanaan][`+id+`][detail][`+last2+`][item]" placeholder="Item" type="text"/>
						</td>
						<td style="text-align: center;width: 50px;border-right: 1px solid rgba(34,36,38,.1);">
							<button class="btn btn-outline-danger btn-sm hapus_detail" type="button" data-id="`+id+`" data-detail="`+(c+2)+`" data-toggle="tooltip" data-placement="left" title="Hapus Item" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
						</td>
					</tr>
				`;
				$('.data_rowspan-'+id).attr('rowspan', parseInt(row+2));

				if(row == 0){
					$(html).insertAfter('.data-pelaksanaan-'+id);
				}else{
					$(html).insertAfter('.detail-'+id+'-'+parseInt(row));
				}
		        $('input[name=last_detail_'+id+']').val(last2)
		        $('.btn').tooltip('enable');
		        $('.tanggal').datepicker({
		        	format: 'mm/dd/yyyy',
		        	startDate: '-0d',
		        	orientation: "auto",
		        	autoclose:true,
		        });
		        $('.waktu').clockpicker({
		        	autoclose:true,
		        	'default': 'now',
		        });
		});
		$(document).on('click', '.hapus_detail', function (e){
			var id = $(this).data("id");
			var rowz = $('#table-pelaksanaan > tbody > tr.detail-pelaksanaan-'+id).length;
			var detail = $(this).data("detail");
			$('.data_rowspan-'+id).attr('rowspan', parseInt(rowz));
			var row = $('tr.detail-'+id+'-'+detail).remove();
			row.remove();
		});

		$(document).on('click', '.tambah_penyelesaian', function(e){
    		var last = parseInt($('input[name=last_penyelesaian]').val()) + 1;
			var rowCount = $('#table-penyelesaian > tbody > tr').length;
			var c = rowCount-1;
			var html = `
					<tr class="data-penyelesaian-`+(c+2)+`" data-id=`+(c+2)+`>
						<td style="text-align: center;width: 50px;" class="numbor-`+(c+2)+`">`+(c+2)+`</td>
						<td style="text-align: center;width: 250px;" class="field">
							<input class="form-control tanggal" id="tanggal" name="detail[0][penyelesaian][`+last+`][mulai]" placeholder="Tanggal" type="text"/>
						</td>
						<td style="text-align: center;width: 250px;" class="field">
							<input class="form-control tanggal" id="tanggal" name="detail[0][penyelesaian][`+last+`][selesai]" placeholder="Tanggal" type="text"/>
						</td>
						<td style="text-align: center;" class="field">
							<input class="form-control" id="item" name="detail[0][penyelesaian][`+last+`][item]" placeholder="Item" type="text"/>
						</td>
						<td style="text-align: center;width: 50px;">
							<button class="btn btn-danger btn-sm hapus_penyelesaian" type="button" data-id="`+(c+2)+`" data-toggle="tooltip" data-placement="left" title="Hapus Penyelesaian" style="border-radius: 20px;"><i class="fa fa-plus"></i></button>
						</td>
					</tr>
				`;

				$('.container-penyelesaian').append(html);
				$('input[name=last_penyelesaian]').val(last)
		        $('.btn').tooltip('enable');
		        $('.tanggal').datepicker({
		        	format: 'mm/dd/yyyy',
		        	startDate: '-0d',
		        	orientation: "auto",
		        	autoclose:true,
		        });
		        $('.waktu').clockpicker({
		        	autoclose:true,
		        	'default': 'now',
		        });
		});
		$(document).on('click', '.hapus_penyelesaian', function (e){
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#table-penyelesaian');
			var rows = table.find('tbody tr');
			$.each(rows, function(key, value){
				table.find('.numbor-'+$(this).data("id")).html(key+1);
			});
		});

		$('.cari').on('change', function(){
			var id = $(this).data('id');
			var tipe = $(this).data('tipe');
			var value = parseInt(this.value);
			$.ajax({
				url: '{{ url('ajax/option/get-langkah') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					id: id,
					value: value,
					tipe: tipe,
				},
			})
			.done(function(response) {
				if(response[2] == 1){
					var rowz = $('tr.langkah-kerja-operasional-'+id).remove();
					var rowspan = response[1].length;
					$('.for_rowspan_operasional_'+id).attr('rowspan', parseInt((rowspan+3)));
					$.each(response[1], function(key, value){
						if(key==0){
							$(value).insertAfter('.langkah_kerja_operasional_'+id);
						}else{
							$(value).insertAfter('.data_detail_operasional-'+id+'-'+(key-1));
						}
					});
				}else if(response[2] == 2){
					var rowz = $('tr.langkah-kerja-keuangan-'+id).remove();
					var rowspan = response[1].length;
					$('.for_rowspan_keuangan_'+id).attr('rowspan', parseInt((rowspan+3)));
					$.each(response[1], function(key, value){
						if(key==0){
							$(value).insertAfter('.langkah_kerja_keuangan_'+id);
						}else{
							$(value).insertAfter('.data_detail_keuangan-'+id+'-'+(key-1));
						}
					});
				}else{
					var rowz = $('tr.langkah-kerja-sistem-'+id).remove();
					var rowspan = response[1].length;
					$('.for_rowspan_sistem_'+id).attr('rowspan', parseInt((rowspan+3)));
					$.each(response[1], function(key, value){
						if(key==0){
							$(value).insertAfter('.langkah_kerja_sistem_'+id);
						}else{
							$(value).insertAfter('.data_detail_sistem-'+id+'-'+(key-1));
						}
					});
				}
			})
		});


		$(document).on('click', '.tambah_langkah_operasional', function(e){
			var id = $(this).data("id");
			var rowCount = $('#table-fokus > tbody > tr.data_operasional').length;
			var c = rowCount-1;
			var html = `
					<tr class="data_operasional data_fokus_audit_operasional_`+(c+1)+`" data-id="`+(c+1)+`">
						<td class="for_rowspan_operasional_`+(c+1)+`" style="text-align: center;font-weight: bold;" rowspan="3">
							<label style="margin-top: 7px;" class="numbur-operasional-`+(c+1)+`">`+(c+2)+`</label>
						</td>
						<td style="width: 20px;" rowspan="3">&nbsp;</td>
						<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="4">Fokus Audit :</td>
						<td style="border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
							<button class="btn btn-outline-danger btn-sm hapus_langkah_operasional" type="button" style="border-radius:20px;" data-id=`+(c+1)+`><i class="fa fa-remove"></i></button>
						</td>
					</tr>
					<tr class="detail_fokus_audit_operasional_`+(c+1)+`" style="border-bottom: 1px solid rgba(34,36,38,.1);">
						<td style="width: 800px;" class="field">
							<select class="selectpicker cari" data-id="`+(c+1)+`" data-tipe="1" name="detail[0][fokus_audit][0][detail][`+(c+1)+`][bidang]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
								@foreach(App\Models\Master\FokusAudit::where('bidang', 1)->get() as $fokus)
									<option value="{{ $fokus->id }}">{{ $fokus->audit }}</option>
								@endforeach
							</select>
						</td>
						<td style="width: 150px;" class="field">
							<input class="form-control tanggal" id="tanggal" name="detail[0][fokus_audit][0][detail][`+(c+1)+`][rencana]" placeholder="Rencana" type="text"/>
						</td>
						<td style="width: 150px;" class="field">
							<input class="form-control tanggal" id="tanggal" name="detail[0][fokus_audit][0][detail][`+(c+1)+`][realisasi]" placeholder="Realisasi" type="text"/>
						</td>
						<td style="width: 350px;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
							<textarea class="form-control" name="detail[0][fokus_audit][0][detail][`+(c+1)+`][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="3"></textarea>
						</td>
					</tr>
					<tr class="langkah_kerja_operasional_`+(c+1)+`">
						<td style="text-align: left;" colspan="6">Langkah Kerja</td>
					</tr>
				`;
				var cek = $('.langkah-kerja-operasional-'+(c)).last();
				if(cek.length == 0){
					$(html).insertAfter('.langkah_kerja_operasional_'+(c));
				}else{
					$(html).insertAfter(cek);
				}
				$('.selectpicker').selectpicker();

				$('.cari').on('change', function(){
		        	var id = $(this).data('id');
		        	var tipe = $(this).data('tipe');
		        	var value = parseInt(this.value);
		        	$.ajax({
						url: '{{ url('ajax/option/get-langkah') }}',
						type: 'POST',
						data: {
							_token: "{{ csrf_token() }}",
							id: id,
							value: value,
							tipe: tipe,
						},
					})
					.done(function(response) {
						if(response[2] == 1){
							var rowz = $('tr.langkah-kerja-operasional-'+id).remove();
							var rowspan = response[1].length;
							$('.for_rowspan_operasional_'+id).attr('rowspan', parseInt((rowspan+3)));
							$.each(response[1], function(key, value){
								if(key==0){
									$(value).insertAfter('.langkah_kerja_operasional_'+id);
								}else{
									$(value).insertAfter('.data_detail_operasional-'+id+'-'+(key-1));
								}
							});
						}else if(response[2] == 2){
							var rowz = $('tr.langkah-kerja-keuangan-'+id).remove();
							var rowspan = response[1].length;
							$('.for_rowspan_keuangan_'+id).attr('rowspan', parseInt((rowspan+3)));
							$.each(response[1], function(key, value){
								if(key==0){
									$(value).insertAfter('.langkah_kerja_keuangan_'+id);
								}else{
									$(value).insertAfter('.data_detail_keuangan-'+id+'-'+(key-1));
								}
							});
						}else{
							var rowz = $('tr.langkah-kerja-sistem-'+id).remove();
							var rowspan = response[1].length;
							$('.for_rowspan_sistem_'+id).attr('rowspan', parseInt((rowspan+3)));
							$.each(response[1], function(key, value){
								if(key==0){
									$(value).insertAfter('.langkah_kerja_sistem_'+id);
								}else{
									$(value).insertAfter('.data_detail_sistem-'+id+'-'+(key-1));
								}
							});
						}
					})
				});

				$('.tanggal').datepicker({
					format: 'mm/dd/yyyy',
					startDate: '-0d',
					orientation: "auto",
					autoclose:true,
				});
		});
		$(document).on('click', '.hapus_langkah_operasional', function (e){
			var id = $(this).data("id");
			var row = $('.data_fokus_audit_operasional_'+id).remove();
			var row = $('.detail_fokus_audit_operasional_'+id).remove();
			var row = $('.langkah-kerja-operasional-'+id).remove();
			var row = $('.langkah_kerja_operasional_'+id).remove();
			var table = $('#table-fokus');
			var rows = table.find('tbody tr.data_operasional');
			$.each(rows, function(key, value){
				console.log($(this).data("id"))
				table.find('.numbur-operasional-'+$(this).data("id")).html(key+1);
			});
		});

		$(document).on('click', '.tambah_langkah_keuangan', function(e){
			var id = $(this).data("id");
			var rowCount = $('#table-fokus > tbody > tr.data_keuangan').length;
			var c = rowCount-1;
			var html = `
					<tr class="data_keuangan data_fokus_audit_keuangan_`+(c+1)+`" data-id="`+(c+1)+`">
						<td class="for_rowspan_keuangan_`+(c+1)+`" style="text-align: center;font-weight: bold;" rowspan="3">
							<label style="margin-top: 7px;" class="numbur-keuangan-`+(c+1)+`">`+(c+2)+`</label>
						</td>
						<td style="width: 20px;" rowspan="3">&nbsp;</td>
						<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="4">Fokus Audit :</td>
						<td style="border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
							<button class="btn btn-outline-danger btn-sm hapus_langkah_keuangan" type="button" style="border-radius:20px;" data-id=`+(c+1)+`><i class="fa fa-remove"></i></button>
						</td>
					</tr>
					<tr class="detail_fokus_audit_keuangan_`+(c+1)+`" style="border-bottom: 1px solid rgba(34,36,38,.1);">
						<td style="width: 800px;" class="field">
							<select class="selectpicker cari" data-id="`+(c+1)+`" data-tipe="2" name="detail[0][fokus_audit][1][detail][`+(c+1)+`][bidang]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
								@foreach(App\Models\Master\FokusAudit::where('bidang', 2)->get() as $fokus)
									<option value="{{ $fokus->id }}">{{ $fokus->audit }}</option>
								@endforeach
							</select>
						</td>
						<td style="width: 150px;" class="field">
							<input class="form-control tanggal" id="tanggal" name="detail[0][fokus_audit][1][detail][`+(c+1)+`][rencana]" placeholder="Rencana" type="text"/>
						</td>
						<td style="width: 150px;" class="field">
							<input class="form-control tanggal" id="tanggal" name="detail[0][fokus_audit][1][detail][`+(c+1)+`][realisasi]" placeholder="Realisasi" type="text"/>
						</td>
						<td style="width: 350px;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
							<textarea class="form-control" name="detail[0][fokus_audit][1][detail][`+(c+1)+`][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="3"></textarea>
						</td>
					</tr>
					<tr class="langkah_kerja_keuangan_`+(c+1)+`">
						<td style="text-align: left;" colspan="6">Langkah Kerja</td>
					</tr>
				`;
				var cek = $('.langkah-kerja-keuangan-'+(c)).last();
				if(cek.length == 0){
					$(html).insertAfter('.langkah_kerja_keuangan_'+(c));
				}else{
					$(html).insertAfter(cek);
				}
				$('.selectpicker').selectpicker();

				$('.cari').on('change', function(){
		        	var id = $(this).data('id');
		        	var tipe = $(this).data('tipe');
		        	var value = parseInt(this.value);
		        	$.ajax({
						url: '{{ url('ajax/option/get-langkah') }}',
						type: 'POST',
						data: {
							_token: "{{ csrf_token() }}",
							id: id,
							value: value,
							tipe: tipe,
						},
					})
					.done(function(response) {
						if(response[2] == 1){
							var rowz = $('tr.langkah-kerja-operasional-'+id).remove();
							var rowspan = response[1].length;
							$('.for_rowspan_operasional_'+id).attr('rowspan', parseInt((rowspan+3)));
							$.each(response[1], function(key, value){
								if(key==0){
									$(value).insertAfter('.langkah_kerja_operasional_'+id);
								}else{
									$(value).insertAfter('.data_detail_operasional-'+id+'-'+(key-1));
								}
							});
						}else if(response[2] == 2){
							var rowz = $('tr.langkah-kerja-keuangan-'+id).remove();
							var rowspan = response[1].length;
							$('.for_rowspan_keuangan_'+id).attr('rowspan', parseInt((rowspan+3)));
							$.each(response[1], function(key, value){
								if(key==0){
									$(value).insertAfter('.langkah_kerja_keuangan_'+id);
								}else{
									$(value).insertAfter('.data_detail_keuangan-'+id+'-'+(key-1));
								}
							});
						}else{
							var rowz = $('tr.langkah-kerja-sistem-'+id).remove();
							var rowspan = response[1].length;
							$('.for_rowspan_sistem_'+id).attr('rowspan', parseInt((rowspan+3)));
							$.each(response[1], function(key, value){
								if(key==0){
									$(value).insertAfter('.langkah_kerja_sistem_'+id);
								}else{
									$(value).insertAfter('.data_detail_sistem-'+id+'-'+(key-1));
								}
							});
						}
					})
				});

				$('.tanggal').datepicker({
					format: 'mm/dd/yyyy',
					startDate: '-0d',
					orientation: "auto",
					autoclose:true,
				});
		});
		$(document).on('click', '.hapus_langkah_keuangan', function (e){
			var id = $(this).data("id");
			var row = $('.data_fokus_audit_keuangan_'+id).remove();
			var row = $('.detail_fokus_audit_keuangan_'+id).remove();
			var row = $('.langkah-kerja-keuangan-'+id).remove();
			var row = $('.langkah_kerja_keuangan_'+id).remove();
			var table = $('#table-fokus');
			var rows = table.find('tbody tr.data_keuangan');
			$.each(rows, function(key, value){
				console.log($(this).data("id"))
				table.find('.numbur-keuangan-'+$(this).data("id")).html(key+1);
			});
		});

		$(document).on('click', '.tambah_langkah_sistem', function(e){
			var id = $(this).data("id");
			var rowCount = $('#table-fokus > tbody > tr.data_sistem').length;
			var c = rowCount-1;
			var html = `
					<tr class="data_sistem data_fokus_audit_sistem_`+(c+1)+`" data-id="`+(c+1)+`">
						<td class="for_rowspan_sistem_`+(c+1)+`" style="text-align: center;font-weight: bold;" rowspan="3">
							<label style="margin-top: 7px;" class="numbur-sistem-`+(c+1)+`">`+(c+2)+`</label>
						</td>
						<td style="width: 20px;" rowspan="3">&nbsp;</td>
						<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="4">Fokus Audit :</td>
						<td style="border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
							<button class="btn btn-outline-danger btn-sm hapus_langkah_sistem" type="button" style="border-radius:20px;" data-id=`+(c+1)+`><i class="fa fa-remove"></i></button>
						</td>
					</tr>
					<tr class="detail_fokus_audit_sistem_`+(c+1)+`" style="border-bottom: 1px solid rgba(34,36,38,.1);">
						<td style="width: 800px;" class="field">
							<select class="selectpicker cari" data-id="`+(c+1)+`" data-tipe="3" name="detail[0][fokus_audit][2][detail][`+(c+1)+`][bidang]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
								@foreach(App\Models\Master\FokusAudit::where('bidang', 3)->get() as $fokus)
									<option value="{{ $fokus->id }}">{{ $fokus->audit }}</option>
								@endforeach
							</select>
						</td>
						<td style="width: 150px;" class="field">
							<input class="form-control tanggal" id="tanggal" name="detail[0][fokus_audit][2][detail][`+(c+1)+`][rencana]" placeholder="Rencana" type="text"/>
						</td>
						<td style="width: 150px;" class="field">
							<input class="form-control tanggal" id="tanggal" name="detail[0][fokus_audit][2][detail][`+(c+1)+`][realisasi]" placeholder="Realisasi" type="text"/>
						</td>
						<td style="width: 350px;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
							<textarea class="form-control" name="detail[0][fokus_audit][2][detail][`+(c+1)+`][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="3"></textarea>
						</td>
					</tr>
					<tr class="langkah_kerja_sistem_`+(c+1)+`">
						<td style="text-align: left;" colspan="6">Langkah Kerja</td>
					</tr>
				`;
				var cek = $('.langkah-kerja-sistem-'+(c)).last();
				if(cek.length == 0){
					$(html).insertAfter('.langkah_kerja_sistem_'+(c));
				}else{
					$(html).insertAfter(cek);
				}
				$('.selectpicker').selectpicker();

				$('.cari').on('change', function(){
		        	var id = $(this).data('id');
		        	var tipe = $(this).data('tipe');
		        	var value = parseInt(this.value);
		        	$.ajax({
						url: '{{ url('ajax/option/get-langkah') }}',
						type: 'POST',
						data: {
							_token: "{{ csrf_token() }}",
							id: id,
							value: value,
							tipe: tipe,
						},
					})
					.done(function(response) {
						if(response[2] == 1){
							var rowz = $('tr.langkah-kerja-operasional-'+id).remove();
							var rowspan = response[1].length;
							$('.for_rowspan_operasional_'+id).attr('rowspan', parseInt((rowspan+3)));
							$.each(response[1], function(key, value){
								if(key==0){
									$(value).insertAfter('.langkah_kerja_operasional_'+id);
								}else{
									$(value).insertAfter('.data_detail_operasional-'+id+'-'+(key-1));
								}
							});
						}else if(response[2] == 2){
							var rowz = $('tr.langkah-kerja-keuangan-'+id).remove();
							var rowspan = response[1].length;
							$('.for_rowspan_keuangan_'+id).attr('rowspan', parseInt((rowspan+3)));
							$.each(response[1], function(key, value){
								if(key==0){
									$(value).insertAfter('.langkah_kerja_keuangan_'+id);
								}else{
									$(value).insertAfter('.data_detail_keuangan-'+id+'-'+(key-1));
								}
							});
						}else{
							var rowz = $('tr.langkah-kerja-sistem-'+id).remove();
							var rowspan = response[1].length;
							$('.for_rowspan_sistem_'+id).attr('rowspan', parseInt((rowspan+3)));
							$.each(response[1], function(key, value){
								if(key==0){
									$(value).insertAfter('.langkah_kerja_sistem_'+id);
								}else{
									$(value).insertAfter('.data_detail_sistem-'+id+'-'+(key-1));
								}
							});
						}
					})
				});

				$('.tanggal').datepicker({
					format: 'mm/dd/yyyy',
					startDate: '-0d',
					orientation: "auto",
					autoclose:true,
				});
		});
		$(document).on('click', '.hapus_langkah_sistem', function (e){
			var id = $(this).data("id");
			var row = $('.data_fokus_audit_sistem_'+id).remove();
			var row = $('.detail_fokus_audit_sistem_'+id).remove();
			var row = $('.langkah-kerja-sistem-'+id).remove();
			var row = $('.langkah_kerja_sistem_'+id).remove();
			var table = $('#table-fokus');
			var rows = table.find('tbody tr.data_sistem');
			$.each(rows, function(key, value){
				console.log($(this).data("id"))
				table.find('.numbur-sistem-'+$(this).data("id")).html(key+1);
			});
		});
    </script>
    @yield('js-extra')
@endpush

