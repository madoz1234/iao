@extends('layouts.form')
@section('title', 'Detil Tinjauan Dokumen')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body" style="padding-bottom: 0px;">
		<form action="#" method="POST" id="formData">
			@method('PATCH')
			@csrf
			<div class="panel panel-default data-form">
				<table class="table" style="font-size: 12px;">
			<tbody>
				<tr>
					<td style="width:180px;">Nomor</td>
					<td style="width:2px;">:</td>
					<td style="">
						{{ $record->nomor }}
					</td>
				</tr>
				<tr>
					<td style="width:180px;">Tempat</td>
					<td style="width:2px;">:</td>
					<td style="">
						{{ $record->tempat }}
					</td>
				</tr>
				<tr>
					<td style="width:180px;">Tanggal</td>
					<td style="width:2px;">:</td>
					<td style="">
						{{ DateToString($record->tanggal) }}
					</td>
				</tr>
				<tr>
					<td style="width:180px;">Judul</td>
					<td style="width:2px;">:</td>
					<td style="">
						{{ $record->judul }}
					</td>
				</tr>
				<tr>
					<td style="width:180px;">Dateline</td>
					<td style="width:2px;">:</td>
					<td style="">
						{{ DateToString($record->dateline) }}
					</td>
				</tr>
				<tr>
					<td style="">Dokumen</td>
					<td style="">:</td>
					<td style="">
						@if($record->cariFile() > 0)
							<input id="other" name="other[]" multiple type="file" readonly class="form-control" 
	                        data-show-upload="true" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf,image/x-png,image/gif,image/jpeg">
	                    @else 
	                    	-
	                    @endif
					</div>
					</td>
				</tr>
				<tr>
					<td style="">PIC</td>
					<td style="">:</td>
					<td style="">
						@php 
							$resultstr = array();
						@endphp
						@foreach($record->detailpenerima as $kiy => $pic)
							@php
							if($pic->user_id == 1){
								$resultstr[] = $pic->nama;
							}else{
								$resultstr[] = $pic->user->name;
							}
							@endphp
						@endforeach
						@php
							$cuk = array_unique($resultstr);
						@endphp
						{{ implode(", ",$cuk) }}
					</td>
				</tr>
				<tr style="border-bottom: 1px solid rgba(34,36,38,.1);">
					<td style="">Tembusan</td>
					<td style="">:</td>
					<td style="">
						@php 
							$result = array();
						@endphp
						@foreach($record->detailtembusan as $kiy => $tembusan)
							@php
							$result[] = $tembusan->tembusan;
							@endphp
						@endforeach
						@php
							$cik = array_unique($result);
						@endphp
						{{ implode(", ",$cik) }}
					</td>
				</tr>
			</tbody>
		</table>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-left">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
        .table-responsive{
            overflow-x: auto;
            overflow-y: visible !important;
        }

        .table-responsive .dropdown-menu {
	        position: static !important;
	    }
		
	    .input-group.file-caption-main{
            display: none;
        }
    </style>
@endpush
@push('scripts')
    <script> 
		var obj = '{{ $record->fileOtherJson() }}';
        var fileUri = '{{ $record->fileOther() }}';
        console.log(obj, fileUri)
		$('#other').fileinput({
			uploadUrl: "/file-upload-batch/1",
            autoReplace: true,
            overwriteInitial: true,
            // maxFileCount: 1,
            initialPreview: JSON.parse(fileUri.replace(/&quot;/g,'"')),
            initialPreviewAsData: true,
            initialPreviewConfig: JSON.parse(obj.replace(/&quot;/g,'"')),
            // initialCaption: "{{ $record->files()->pluck('filename')->first() }}",
            initialPreviewShowDelete: false,
            showRemove: false,
            showClose: false,
            layoutTemplates: {actionDelete: ''}, // disable thumbnail deletion
        });
        $('.kv-file-zoom').prop('disabled', false);
    </script>
    @yield('js-extra')
@endpush
