@extends('layouts.list-grid-uraian')

@section('title', 'Final Kka')

@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection

@section('filters')
    <div class="form-group">
        <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
        <label class="control-label sr-only" for="filter-tahun">Tahun</label>
	    <input type="text" class="form-control filter-control" name="filter[tahun]" data-post="tahun" placeholder="Tahun">
    </div>
@endsection
@push('scripts')
    <script> 
        $(document).on('click', '.detil-project', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/detilProject';
            window.location = url;
		});
		$(document).on('click', '.detil-penugasan', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/detilPenugasan';
            window.location = url;
		});
		$(document).on('click', '.detil-tinjauan', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/detilTinjauan';
            window.location = url;
		});
		$(document).on('click', '.detil-program', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/detilProgram';
            window.location = url;
		});

		$(document).on('click', '.detil-draft', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/detilDraft';
            window.location = url;
		});

		$(document).on('click', '.approve.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/approve';
        	swal({
					title: "Apakah Anda yakin?",
					text: "Data yang sudah disetujui, tidak dapat diubah!",
					icon: "warning",
					buttons: ['Batal', 'OK'],
					reverseButtons: true,
			}).then((result) => {
				if (result) {
					$.ajax({
						url: url,
						type: 'POST',
						data: {
							id : idx,
							tipe : 1,
							'_token' : '{{ csrf_token() }}'
						}
					})
					.done(function(response) {
						swal({
							icon: 'success',
							title:'Berhasil!',
							text:'Data berhasil disetujui!',
							button: false,
							timer: 1000,
					    }).then((res) => {
					    	dt1 = $('#dataTable').DataTable();
				    		dt1.draw();
					    	callback()
					    })
					})
					.fail(function(response) {
						console.log(response);
						swal('Data gagal disetujui!', {
							icon: 'error',
					    }).then((res) => {
					    	dt1 = $('#dataTable').DataTable();
				    		dt1.draw();
					    	callback()
					    })
					})

				}
			})
        });
    </script>
    @yield('js-extra')
@endpush