@extends('layouts.form')
@section('title', 'Detil Closing Meeting')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
    <div class="panel-body" style="padding-bottom: 0px;">
    	<form action="" method="POST" id="formData">
			@method('PATCH')
		    @csrf
		    <div class="loading dimmer padder-v" style="display: none;">
			    <div class="loader"></div>
			</div>
			<input type="hidden" name="status" value="0">
			<input type="hidden" name="tipe" value="1">
			<input type="hidden" name="id" value="{{ $record->id }}">
			<div class="form-row">
				<div class="form-group col-md-12">
					<input type="hidden" name="program_id" value="{{ $record->id }}">
					<div class="form-row" style="margin-left: -15px;">
						<div class="form-group col-md-12">
							<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
								<tr>
									<td style="width:200px;border: 1px #ffffff;border: 1px #ffffff;">Tahun</td>
									<td style="width:2px;border: 1px #ffffff;">:</td>
									<td style="text-align:left;border: 1px #ffffff;" class="field">
										{{ $record->opening->program->penugasanaudit->rencanadetail->rencanaaudit->tahun }}
										<input type="hidden" name="tahun" value="{{ $record->opening->program->penugasanaudit->rencanadetail->rencanaaudit->tahun }}">
									</td>
								</tr>
								<tr>
									<td style="border: 1px #ffffff;">Waktu Pelaksanaan</td>
									<td style="border: 1px #ffffff;">:</td>
									<td style="text-align:left;border: 1px #ffffff;" class="field">
										{{ DateToString($record->opening->program->detailpelaksanaan->first()->tgl).' - '.DateToString($record->opening->program->detailpelaksanaan->last()->tgl)}}
									</td>
								</tr>
								<tr>
									<td style="border: 1px #ffffff;">Tipe</td>
									<td style="border: 1px #ffffff;">:</td>
									<td style="text-align:left;border: 1px #ffffff;" class="field">
										@if($record->opening->program->penugasanaudit->rencanadetail->tipe == 0)
											<span class="label label-primary">Audit</span>
										@elseif($record->opening->program->penugasanaudit->rencanadetail->tipe == 1)
											<span class="label label-success">Kegiatan Konsultasi</span>
										@else
											<span class="label label-info">Kegiatan Audit Lain - Lain</span>
										@endif
									</td>
								</tr>
								<tr>
									<td style="border: 1px #ffffff;">Kategori</td>
									<td style="border: 1px #ffffff;">:</td>
									<td style="text-align:left;border: 1px #ffffff;" class="field">
										@if($record->opening->program->penugasanaudit->rencanadetail->tipe == 0)
											@if($record->opening->program->penugasanaudit->rencanadetail->tipe_object == 0)
												<span class="label label-success">Business Unit (BU)</span>
											@elseif($record->opening->program->penugasanaudit->rencanadetail->tipe_object == 1)
												<span class="label label-info">Corporate Office (CO)</span>
											@elseif($record->opening->program->penugasanaudit->rencanadetail->tipe_object == 2)
												<span class="label label-default">Project</span>
											@else
												<span class="label label-primary">Anak Perusahaan</span>
											@endif
										@elseif($record->opening->program->penugasanaudit->rencanadetail->tipe == 1)
											<span class="label label-warning">{{ $record->opening->program->penugasanaudit->rencanadetail->konsultasi->nama }}</span>
										@else
											<span class="label label-default">{{ $record->opening->program->penugasanaudit->rencanadetail->lain->nama }}</span>
										@endif
									</td>
								</tr>
								<tr>
									<td style="width: 10%;border: 1px #ffffff;">Objek Audit</td>
									<td style="width: 1%;border: 1px #ffffff;">:</td>
									<td style="text-align: left;border: 1px #ffffff;" class="field">
										{{ object_audit($record->opening->program->penugasanaudit->rencanadetail->tipe_object, $record->opening->program->penugasanaudit->rencanadetail->object_id)}}
									</td>
								</tr>
								<tr>
									<td style="width: 10%;border: 1px #ffffff;">Tanggal</td>
									<td style="width: 1%;border: 1px #ffffff;">:</td>
									<td style="text-align: left;border: 1px #ffffff;" class="field">
										{{ $record->tanggal }}
									</td>
								</tr>
								<tr>
									<td style="width: 10%;border: 1px #ffffff;">Tempat</td>
									<td style="width: 1%;border: 1px #ffffff;">:</td>
									<td style="text-align: left;border: 1px #ffffff;" class="field">
										{{ $record->tempat }}
									</td>
								</tr>
								<tr>
									<td style="width: 10%;border: 1px #ffffff;">Waktu Mulai</td>
									<td style="width: 1%;border: 1px #ffffff;">:</td>
									<td style="text-align: left;border: 1px #ffffff;" class="field">
										{{ $record->mulai }}
									</td>
								</tr>
								<tr>
									<td style="width: 10%;border: 1px #ffffff;">Waktu Selesai</td>
									<td style="width: 1%;border: 1px #ffffff;">:</td>
									<td style="text-align: left;border: 1px #ffffff;" class="field">
										{{ $record->selesai }}
									</td>
								</tr>
								<tr>
									<td style="width: 10%;border: 1px #ffffff;">Peserta</td>
									<td style="width: 1%;border: 1px #ffffff;">:</td>
									<td style="text-align: left;border: 1px #ffffff;" class="field">
										@if(count($record->detailanggota->where('user_id', '!=', 1)) > 0)
											@php 
												$data_user = array();
												foreach($record->detailanggota->where('user_id', '!=', 1) as $key => $dat){
													$data_user[]= $dat->user->name;
												}
											@endphp
											{{ implode(",", $data_user) }}
										@else 
										-
										@endif
									</td>
								</tr>
								<tr>
									<td style="width: 10%;border: 1px #ffffff;">Peserta Lainnya</td>
									<td style="width: 1%;border: 1px #ffffff;">:</td>
									<td style="text-align: left;border: 1px #ffffff;" class="field">
										@if(count($record->detailanggota->where('user_id', 1)) > 0)
											@php 
												$data_users = array();
												foreach($record->detailanggota->where('user_id', 1) as $key => $dat){
													$data_users[]= $dat->user->name;
												}
											@endphp
											{{ implode(",", $data_users) }}
										@else 
										-
										@endif
									</td>
								</tr>
							</table>
						</div>
					</div>
					 <div class="form-group">
					 	<label for="inputPassword4">Lampiran</label>
					 	<div class="field">
						    <div class="file-loading">
								<input name="lampiran" type="file" id="lampiran" class="file form-control cfile zfile previewupload" 
								data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf">
							</div>
					 	</div>
					 </div>
					 <div class="form-group">
					 	<label for="inputPassword4">Daftar Hadir</label>
					 	<div class="field">
						    <div class="file-loading">
								<input id="hadir"  name="hadir" type="file" class="file form-control cfile zfile previewupload" 
								data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf">
							</div>
					 	</div>
					 </div>
					 <div class="form-group">
					 	<label for="inputPassword4">Foto Meeting</label>
					 	<div class="field">
						    <div class="file-loading">
								<input id="foto"  name="foto[]" type="file" class="file form-control cfile zfile previewupload" 
								data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/*" multiple>
							</div>
					 	</div>
					 </div>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-left">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
					</div>
				</div>
			</div>
		</form>
    </div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush
@push('styles')
    <style>
        .table-responsive{
            overflow-x: auto;
            overflow-y: visible !important;
        }

        .table-responsive .dropdown-menu {
	        position: static !important;
	    }
		
	    .input-group.file-caption-main{
            display: none;
        }
    </style>
@endpush

@push('scripts')
    <script> 
        var dor = '{{ $record->fileLampiranJson() }}';
        var lampiran = '{{ $record->fileLampiran() }}';
        if(dor == 1){
        }else{
	        $("#lampiran").fileinput({
	            autoReplace: true,
		        overwriteInitial: true,
		        initialPreview: JSON.parse(lampiran.replace(/&quot;/g,'"')),
		        initialPreviewAsData: true,
		        initialPreviewConfig: JSON.parse(dor.replace(/&quot;/g,'"')),
		        initialCaption: "{{ $record->lampiranname }}",
		        initialPreviewShowDelete: false,
		        showRemove: false,
		        showClose: false,
		        layoutTemplates: {actionDelete: ''}, // disable thumbnail deletion
	        });
        }

        var dor2 = '{{ $record->fileHadirJson() }}';
        var hadir = '{{ $record->fileHadir() }}';
        if(dor2 == 1){
        }else{
	        $("#hadir").fileinput({
	            autoReplace: true,
	            overwriteInitial: true,
	            initialPreview: JSON.parse(hadir.replace(/&quot;/g,'"')),
	            initialPreviewAsData: true,
	            initialPreviewConfig: JSON.parse(dor2.replace(/&quot;/g,'"')),
	            initialCaption: "{{ $record->hadirname }}",
	            initialPreviewShowDelete: false,
	            showRemove: false,
	            showClose: false,
	            layoutTemplates: {actionDelete: ''},
	        });
        }

        var dor3 = '{{ $record->fileFotoJson() }}';
        var foto = '{{ $record->fileFoto() }}';
        if(dor2 == 1){
        }else{
	        $("#foto").fileinput({
	            autoReplace: true,
	            overwriteInitial: true,
	            initialPreview: JSON.parse(foto.replace(/&quot;/g,'"')),
	            initialPreviewAsData: true,
	            initialPreviewConfig: JSON.parse(dor3.replace(/&quot;/g,'"')),
	            initialPreviewShowDelete: false,
	            showRemove: false,
	            showClose: false,
	            layoutTemplates: {actionDelete: ''},
	        });
        }
    </script>
    @yield('js-extra')
@endpush
