@extends('layouts.form')
@section('title', 'Detil Draft KKA')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body" style="padding-bottom: 0px;">
		<form action="" method="POST" id="formData">
			@csrf
			<input type="hidden" name="id" value="{{ $record->id }}">
			<div class="form-row">
				<div class="form-group col-md-12">
					<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
						<tr>
							<td style="width: 10%;border: 1px #ffffff;">Objek Audit</td>
							<td style="width: 1%;border: 1px #ffffff;">:</td>
							<td style="text-align: left;border: 1px #ffffff;" class="field">

								{!! object_audit($record->opening->program->penugasanaudit->rencanadetail->tipe_object, $record->opening->program->penugasanaudit->rencanadetail->object_id)!!}
							</td>
						</tr>
						<tr>
							<td style="width: 10%;border: 1px #ffffff;">Tanggal Audit</td>
							<td style="width: 1%;border: 1px #ffffff;">:</td>
							<td style="text-align: left;border: 1px #ffffff;" class="field">
								 {{ DateToStringWday($record->opening->program->detailpelaksanaan->min('tgl')) }}  -  {{DateToStringWday($record->opening->program->detailpelaksanaan->max('tgl'))}}
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="form-row">
					<div class="form-group col-md-12">
						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item">
								<a style="font-weight: bold;" class="nav-link active" id="a-tab" data-toggle="tab" href="#a" role="tab" aria-controls="home" aria-selected="true">Operasional</a>
							</li>
							<li class="nav-item">
								<a style="font-weight: bold;" class="nav-link" id="b-tab" data-toggle="tab" href="#b" role="tab" aria-controls="profile" aria-selected="false">Keuangan</a>
							</li>
							<li class="nav-item">
								<a style="font-weight: bold;" class="nav-link" id="c-tab" data-toggle="tab" href="#c" role="tab" aria-controls="messages" aria-selected="false">Sistem</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="a" role="tabpanel" aria-labelledby="a-tab">
								<table id="table-operasional" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
									<tbody class="container-operasional">
										@foreach($record->detaildraft->where('tipe', 0) as $key => $operasional)
											@if($key == 0)
												<tr class="detail-operasional-{{$key}}" data-id="{{$key}}">
													<td style="text-align: left;font-weight: bold;" colspan="5">Bidang Audit : Operasional</td>
												</tr>
											@else 
											@endif
											<tr class="data-operasional detail-operasional-{{$key}}" data-id="{{ $key }}" data-kategori="{{ $operasional->kategori_id }}" data-kriteria="{{$operasional->kriteria_id}}">
												<input type="hidden" name="exist_operasional[]" value="{{ $operasional->id }}">
												<input type="hidden" name="fokus_audit[0][detail][{{$key}}][id]" value="{{ $operasional->id }}">
												<td style="text-align: center;width: 20px;">{{ $key+1 }}</td>
												<td style="width: 150px;">Judul</td>
												<td style="width: 2px;">:</td>
												<td style="width: 1600px;text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{getkategori($operasional->kategori_id)}}
												</td>
											</tr>
											<tr class="detail-operasional-{{$key}}">
												<td style="border-bottom: 2px solid black;" rowspan="8"></td>
												<td style="">Kondisi</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $operasional->kondisi }}
												</td>
											</tr>
											<tr class="detail-operasional-{{$key}}">
												<td style="">Kriteria Temuan</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{getkriteria($operasional->kriteria_id)}}
												</td>
											</tr>
											<tr class="detail-operasional-{{$key}}">
												<td style="">Kriteria</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $operasional->kriteria }}
												</td>
											</tr>
											<tr class="detail-operasional-{{$key}}">
												<td style="">Sebab</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $operasional->sebab }}
												</td>
											</tr>
											<tr class="detail-operasional-{{$key}}">
												<td style="">Risiko</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $operasional->risiko }}
												</td>
											</tr>
											<tr class="detail-operasional-{{$key}}">
												<td style="">Rekomendasi</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $operasional->rekomendasi }}
												</td>
											</tr>
											<tr class="detail-operasional-{{$key}}">
												<td style="">Audite</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $operasional->user->name }}
												</td>
											</tr>
											<tr class="detail-operasional-end-0 detail-operasional-{{$key}}" style="border-bottom: 2px solid black;">
												<td style="">Tanggal Tindak Lanjut</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ DateToString($operasional->tgl) }}
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							<div class="tab-pane" id="b" role="tabpanel" aria-labelledby="b-tab">
								<table id="table-keuangan" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
									<tbody class="container-keuangan">
										@php 
											$i=0;
										@endphp
										@foreach($record->detaildraft->where('tipe', 1) as $kuy => $keuangan)
											@if($i == 0)
												<tr class="detail-keuangan-{{$i}}" data-id="{{$i}}">
													<td style="text-align: left;font-weight: bold;" colspan="5">Bidang Audit : Keuangan</td>
												</tr>
											@else 
											@endif
											<tr class="data-keuangan detail-keuangan-{{$i}}" data-id="{{ $i }}" data-kategori="{{ $keuangan->kategori_id }}" data-kriteria="{{$keuangan->kriteria_id}}">
												<input type="hidden" name="exist_keuangan[]" value="{{ $keuangan->id }}">
												<input type="hidden" name="fokus_audit[0][detail][{{$i}}][id]" value="{{ $keuangan->id }}">
												<td style="text-align: center;width: 20px;">{{ $i+1 }}</td>
												<td style="width: 150px;">Judul</td>
												<td style="width: 2px;">:</td>
												<td style="width: 1600px;text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{getkategori($keuangan->kategori_id)}}
												</td>
											</tr>
											<tr class="detail-keuangan-{{$i}}">
												<td style="border-bottom: 2px solid black;" rowspan="8"></td>
												<td style="">Kondisi</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $keuangan->kondisi }}
												</td>
											</tr>
											<tr class="detail-keuangan-{{$i}}">
												<td style="">Kriteria Temuan</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{getkriteria($keuangan->kriteria_id)}}
												</td>
											</tr>
											<tr class="detail-keuangan-{{$i}}">
												<td style="">Kriteria</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $keuangan->kriteria }}
												</td>
											</tr>
											<tr class="detail-keuangan-{{$i}}">
												<td style="">Sebab</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $keuangan->sebab }}
												</td>
											</tr>
											<tr class="detail-keuangan-{{$i}}">
												<td style="">Risiko</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $keuangan->risiko }}
												</td>
											</tr>
											<tr class="detail-keuangan-{{$i}}">
												<td style="">Rekomendasi</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $keuangan->rekomendasi }}
												</td>
											</tr>
											<tr class="detail-keuangan-{{$i}}">
												<td style="">Audite</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $keuangan->user->name }}
												</td>
											</tr>
											<tr class="detail-keuangan-end-0 detail-keuangan-{{$i}}" style="border-bottom: 2px solid black;">
												<td style="">Tanggal Tindak Lanjut</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ DateToString($keuangan->tgl) }}
												</td>
											</tr>
											@php 
												$i++;
											@endphp
										@endforeach
									</tbody>
								</table>
							</div>
							<div class="tab-pane" id="c" role="tabpanel" aria-labelledby="c-tab">
								<table id="table-sistem" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
									<tbody class="container-sistem">
										@php 
											$j=0;
										@endphp
										@foreach($record->detaildraft->where('tipe', 2) as $kiy => $sistem)
											@if($j == 0)
												<tr class="detail-sistem-{{$j}}" data-id="{{$j}}">
													<td style="text-align: left;font-weight: bold;" colspan="5">Bidang Audit : Sistem</td>
												</tr>
											@else 
											@endif
											<tr class="data-sistem detail-sistem-{{$j}}" data-id="{{ $j }}" data-kategori="{{ $sistem->kategori_id }}" data-kriteria="{{$sistem->kriteria_id}}">
												<input type="hidden" name="exist_sistem[]" value="{{ $sistem->id }}">
												<input type="hidden" name="fokus_audit[0][detail][{{$j}}][id]" value="{{ $sistem->id }}">
												<td style="text-align: center;width: 20px;">{{ $j+1 }}</td>
												<td style="width: 150px;">Judul</td>
												<td style="width: 2px;">:</td>
												<td style="width: 1600px;text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{getkategori($sistem->kategori_id)}}
												</td>
											</tr>
											<tr class="detail-sistem-{{$j}}">
												<td style="border-bottom: 2px solid black;" rowspan="8"></td>
												<td style="">Kondisi</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $sistem->kondisi }}
												</td>
											</tr>
											<tr class="detail-sistem-{{$j}}">
												<td style="">Kriteria Temuan</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{getkriteria($sistem->kriteria_id)}}
												</td>
											</tr>
											<tr class="detail-sistem-{{$j}}">
												<td style="">Kriteria</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $sistem->kriteria }}
												</td>
											</tr>
											<tr class="detail-sistem-{{$j}}">
												<td style="">Sebab</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $sistem->sebab }}
												</td>
											</tr>
											<tr class="detail-sistem-{{$j}}">
												<td style="">Risiko</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $sistem->risiko }}
												</td>
											</tr>
											<tr class="detail-sistem-{{$j}}">
												<td style="">Rekomendasi</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $sistem->rekomendasi }}
												</td>
											</tr>
											<tr class="detail-sistem-{{$j}}">
												<td style="">Audite</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $sistem->user->name }}
												</td>
											</tr>
											<tr class="detail-sistem-end-0 detail-sistem-{{$j}}" style="border-bottom: 2px solid black;">
												<td style="">Tanggal Tindak Lanjut</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ DateToString($sistem->tgl) }}
												</td>
											</tr>
											@php 
												$j++;
											@endphp
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-left">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
					</div>
				</div>
			</div>
		</form>
    </div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script>
    	$('#myTab li:first-child a').tab('show')
    </script>
    @yield('js-extra')
@endpush

