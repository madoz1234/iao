@extends('layouts.list-multi-grid')

@section('title', 'Closing Meeting')

@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection

@section('filters')
<div class="form-group">
    <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
    <label class="control-label sr-only" for="filter-tahun">Tahun</label>
    <input type="text" class="form-control filter-control tahun" name="filter[tahun]" data-post="tahun" placeholder="Tahun" style="width: 220px;">
    <label class="control-label sr-only" for="filter-tipe">Tipe</label>
    <select class="select selectpicker filter-control" name="filter[tipe]" data-post="tipe" data-style="btn-default">
        <option style="font-size: 12px;" value="">Tipe</option>
        <option style="font-size: 12px;" value="1">Audit Reguler</option>
        <option style="font-size: 12px;" value="2">Audit Khusus</option>
    </select>
    <label class="control-label sr-only" for="filter-kategori">Kategori</label>
    <select class="select selectpicker filter-control kategori" name="filter[kategori]" data-post="kategori" data-style="btn-default">
           <option style="font-size: 12px;" value="">Kategori</option>
           <option style="font-size: 12px;" value="1">Business Unit (BU)</option>
           <option style="font-size: 12px;" value="2">Corporate Office (CO)</option>
           <option style="font-size: 12px;" value="3">Project</option>
           <option style="font-size: 12px;" value="4">Anak Perusahaan</option>
    </select>
    <label class="control-label sr-only" for="filter-object_id">Objek Audit</label>
    <select class="select selectpicker filter-control show-tick object" name="filter[object_id]" data-post="object_id" data-size="3" data-style="btn-default" data-live-search="true" title="Objek Audit">
	</select>
</div>
@endsection
@section('tables')
	<ul class="nav nav-tabs" id="myTab" role="tablist" style="margin-bottom: 7px;">
		<li class="nav-item tab-a">
			<a class="nav-link active" id="a-tab" data-toggle="tab" href="#a" role="tab" aria-selected="true">Baru &nbsp;&nbsp;<span class="badge badge-sm down bg-danger pull-right-xs for-isi1" style="display:none;">{{$nbaru}}</span></a>
		</li>
		<li class="nav-item tab-b">
			<a class="nav-link" id="b-tab" data-toggle="tab" href="#b" role="tab" aria-selected="false">On Progress @if($nprog > 0)&nbsp;&nbsp;<span class="badge badge-sm down bg-danger pull-right-xs for-isi2">{{$nprog}}</span> @endif</a>
		</li>
		<li class="nav-item tab-c">
			<a class="nav-link" id="c-tab" data-toggle="tab" href="#c" role="tab" aria-selected="false">Historis</a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="a" role="tabpanel" aria-labelledby="a-tab">
			<div class="table-responsive">
				@if(isset($structs['listStruct']))
					<table id="dataTable1" class="table table-bordered m-t-none" style="width: 100%">
						<thead>
							<tr>
								@foreach ($structs['listStruct'] as $struct)
								<th class="text-center v-middle">{{ $struct['label'] }}</th>
								@endforeach
							</tr>
						</thead>
						<tbody>
							@yield('tableBody')
						</tbody>
					</table>
				@endif
			</div>
		</div>
		<div class="tab-pane active" id="b" role="tabpanel" aria-labelledby="b-tab">
			<div class="table-responsive">
				@if(isset($structs['listStruct2']))
					<table id="dataTable2" class="table table-bordered m-t-none" style="width: 100%">
						<thead>
							<tr>
								@foreach ($structs['listStruct2'] as $struct)
								<th class="text-center v-middle">{{ $struct['label'] }}</th>
								@endforeach
							</tr>
						</thead>
						<tbody>
							@yield('tableBody')
						</tbody>
					</table>
				@endif
			</div>
		</div>
		<div class="tab-pane active" id="c" role="tabpanel" aria-labelledby="c-tab">
			<div class="table-responsive">
				@if(isset($structs['listStruct3']))
					<table id="dataTable3" class="table table-bordered m-t-none" style="width: 100%">
						<thead>
							<tr>
								@foreach ($structs['listStruct3'] as $struct)
								<th class="text-center v-middle">{{ $struct['label'] }}</th>
								@endforeach
							</tr>
						</thead>
						<tbody>
							@yield('tableBody')
						</tbody>
					</table>
				@endif
			</div>
		</div>
	</div>
@endsection
@push('scripts')
    <script>
    	$('.tahun').datepicker({
            format: "yyyy",
		    viewMode: "years", 
		    minViewMode: "years",
            orientation: "auto",
            autoclose:true
        });
         
    	$('.kategori').on('change', function(){
			$.ajax({
				url: '{{ url('ajax/option/get-kategoris') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					kategori: this.value
				},
			})
			.done(function(response) {
				$('select[name="filter[object_id]"]').html(response);
				$('select[name="filter[object_id]"]').selectpicker("refresh");
			})
			.fail(function() {
				console.log("error");
			});
		});
		
    	$('#myTab li:first-child a').tab('show');
    	$(document).on('click', '.tab-a', function (e){
    		dt1 = $('#dataTable1').DataTable();
    		dt1.draw();
		}); 
		$(document).on('click', '.tab-b', function (e){
    		dt2 = $('#dataTable2').DataTable();
    		dt2.draw();
		}); 
		$(document).on('click', '.tab-c', function (e){
    		dt3 = $('#dataTable3').DataTable();
    		dt3.draw();
		});

		$(document).on('click', '.programaudit-detil.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route('kegiatan-audit.pelaksanaan.opening-meeting.index') !!}/" + idx + '/detil';
            window.location = url;
        });

        $(document).on('click', '.buat.button', function(e){
            var idx = $(this).data('id');
             var url = "{!! route($routes.'.index') !!}/" + idx + '/buat';
            window.location = url;
        });

        $(document).on('click', '.detil-closing.button', function(e){
            var idx = $(this).data('id');
             var url = "{!! route($routes.'.index') !!}/" + idx + '/detilClosing';
            window.location = url;
        });


        $(document).on('click', '.ubah.button', function(e){
            var idx = $(this).data('id');
             var url = "{!! route($routes.'.index') !!}/" + idx + '/ubah';
            window.location = url;
        });

        $(document).on('click', '.detil-project', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/detilProject';
            window.location = url;
		});
		$(document).on('click', '.detil-penugasan', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/detilPenugasan';
            window.location = url;
		});
		$(document).on('click', '.detil-tinjauan', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/detilTinjauan';
            window.location = url;
		});
		$(document).on('click', '.detil-program', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/detilProgram';
            window.location = url;
		});

		$(document).on('click', '.cetak-audit.button', function(e){
            var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/cetakAudit';
            window.open(url, '_blank');
        });

        $(document).on('click', '.cetak-penugasan.button', function(e){
            var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/cetakPenugasan';
            window.open(url, '_blank');
        });

        $(document).on('click', '.download-rkia.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/downloadAudit';
            window.location = url;
        });

        $(document).on('click', '.download-penugasan.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/downloadPenugasan';
            window.location = url;
        });

        $(document).on('click', '.download-program.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/downloadProgramAudit';
            window.location = url;
        });
    </script>
    @yield('js-extra')
@endpush