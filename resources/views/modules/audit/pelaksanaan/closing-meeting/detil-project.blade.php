@extends('layouts.form')
@section('title', 'Detil Project '.$record->rencanaaudit->tahun)
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body" style="padding-bottom: 0px;">
		<form action="#" method="POST" id="formData">
			@method('PATCH')
			@csrf
			<div class="panel panel-default data-form">
					<table class="table" style="font-size: 12px;">
						<tbody>
							<tr>
								<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">Kategori</td>
								<td style="width:2px;border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;" class="field">
									@if($record->tipe_object == 0)
										<span class="label label-success">Business Unit (BU)</span>
									@elseif($record->tipe_object == 1)
										<span class="label label-info">Corporate Office (CO)</span>
									@elseif($record->tipe_object == 2)
										<span class="label label-default">Project</span>
									@else 
										<span class="label label-primary">Anak Perusahaan</span>
									@endif
								</td>
							</tr>
							<tr>
								<td style="border: 1px #ffffff;">Objek Audit</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;" class="field">
									{!! object_audit($record->tipe_object, $record->object_id)!!}
								</td>
							</tr>
							<tr>
								<td style="border: 1px #ffffff;">Rencana</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;" class="field">
									{{ $record->rencana }}
								</td>
							</tr>
							<tr>
								<td style="border: 1px #ffffff;">Keterangan</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;" class="field">
									@if($record->keterangan == 0)
										<span class="label label-info">Tidak Dipilih</span>
									@else 
										<span class="label label-primary">Dipilih</span>
									@endif
								</td>
							</tr>
							<tr class="detil">
								<td style="border: 1px #ffffff;">No AB</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									{{ getNoAb($record->object_id)}}
								</td>
							</tr>
							<tr class="detil">
								<td style="border: 1px #ffffff;">Project ID</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									{{ getId($record->object_id)}}
								</td>
							</tr>
							<tr class="detil">
								<td style="border: 1px #ffffff;">Project</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									{!! object_audit($record->tipe_object, $record->object_id)!!}
								</td>
							</tr>
							<tr class="detil">
								<td style="border: 1px #ffffff;">Lokasi</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									{{ getlokasi($record->object_id)}}
								</td>
							</tr>
							<tr class="detil">
								<td style="border: 1px #ffffff;">Tgl Mulai</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									{{ DateToString($record->tgl_mulai) }}
								</td>
							</tr>
							<tr class="detil">
								<td style="border: 1px #ffffff;">Tgl Selesai</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									{{ DateToString($record->tgl_selesai) }}
								</td>
							</tr>
							<tr class="detil">
								<td style="border: 1px #ffffff;">Jenis Kontrak</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									@if($record->jenis == 0)
										<span class="label label-info">Kontrak Baru</span>
									@else 
										<span class="label label-primary">Sisa Nilai Kontrak</span>
									@endif
								</td>
							</tr>
							<tr class="detil">
								<td style="border: 1px #ffffff;">SNK {{ ($record->rencanaaudit->tahun-1) }}</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									{{ number_format($record->snk, 0, '.', '.') }}
								</td>
							</tr>
							<tr class="detil">
								<td style="border: 1px #ffffff;">Progress s.d Des {{ ($record->rencanaaudit->tahun-1) }}</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									{{ number_format($record->progress, 0, '.', '.') }}
								</td>
							</tr>
							<tr class="detil">
								<td style="border: 1px #ffffff;">NK Total</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									{{ number_format($record->nilai_kontrak, 0, '.', '.') }}
								</td>
							</tr>
							<tr class="detil">
								<td style="border: 1px #ffffff;">Progress (Ra)</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									{{ number_format($record->progress_ra, 0, '.', '.') }}
								</td>
							</tr>
							<tr class="detil">
								<td style="border: 1px #ffffff;">Progress (Ri)</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									{{ number_format($record->progress_ri, 0, '.', '.') }}
								</td>
							</tr>
							<tr class="detil">
								<td style="border: 1px #ffffff;">Deviasi Progress</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									{{ number_format($record->deviasi, 0, '.', '.') }}
								</td>
							</tr>
							<tr class="detil">
								<td style="border: 1px #ffffff;">Progress Risk Impact</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									@if($record->risk_impact == 0)
										<span class="label label-default">Sangat Ringan</span>
									@elseif($record->risk_impact == 1)
										<span class="label label-success">Ringan</span>
									@elseif($record->risk_impact == 2)
										<span class="label label-info">Sedang</span>
									@elseif($record->risk_impact == 3)
										<span class="label label-warning">Berat</span>
									@else 
										<span class="label label-danger">Sangat Berat</span>
									@endif
								</td>
							</tr>
							<tr class="detil">
								<td style="border: 1px #ffffff;">MAPP</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									{{ number_format($record->mapp, 0, '.', '.') }}
								</td>
							</tr>
							<tr class="detil">
								<td style="border: 1px #ffffff;">BK / PU</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									{{ number_format($record->bkpu, 0, '.', '.') }}
								</td>
							</tr>
							<tr class="detil">
								<td style="border: 1px #ffffff;">Deviasi BK / PU</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									{{ number_format($record->deviasi_bkpu, 0, '.', '.') }}
								</td>
							</tr>
							<tr class="detil">
								<td style="border: 1px #ffffff;">BK / PU Risk Impact</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									@if($record->bkpu_ri == 0)
										<span class="label label-default">Sangat Ringan</span>
									@elseif($record->bkpu_ri == 1)
										<span class="label label-success">Ringan</span>
									@elseif($record->bkpu_ri == 2)
										<span class="label label-info">Sedang</span>
									@elseif($record->bkpu_ri == 3)
										<span class="label label-warning">Berat</span>
									@else 
										<span class="label label-danger">Sangat Berat</span>
									@endif
								</td>
							</tr>
						</tbody>
					</table>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-left">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
        .table-responsive{
            overflow-x: auto;
            overflow-y: visible !important;
        }

        .table-responsive .dropdown-menu {
	        position: static !important;
	    }
		
	    /*.datepicker-days > table > tbody > tr:hover {
		    background-color: #808080;
		}*/
    </style>
@endpush