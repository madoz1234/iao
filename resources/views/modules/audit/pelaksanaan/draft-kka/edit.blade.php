@extends('layouts.form')
@section('title', 'Ubah KKA')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body" style="padding-bottom: 0px;">
		<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
			@method('PATCH')
			@csrf
			<div class="loading dimmer padder-v" style="display: none;">
			    <div class="loader"></div>
			</div>
			@php 
				$users = object_users($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
			@endphp
			@if($record->ket_ketua)
				<p style="padding-left: 14px;color:red;">Status <b>Ditolak Ketua Tim</b> pada {{DateToStringWday($record->updated_at)}}</p>
				<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
					<tr>
						<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
							<h5 style="font-weight: bold;font-size: 12px;">Keterangan :</h5>
							<p style="text-align: justify;text-justify: inter-word;">{{ $record->ket_ketua }}</p>
						</td>
					</tr>
				</table>
			@else
			@endif
			<input type="hidden" name="id" value="{{ $record->id }}">
			<input type="hidden" name="tahun" value="{{ $record->program->penugasanaudit->rencanadetail->rencanaaudit->tahun }}">
			<input type="hidden" name="status" value="0">
			<div class="form-row">
				<div class="form-group col-md-12">
					<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
						<tr>
							<td style="width: 10%;border: 1px #ffffff;">Tipe</td>
							<td style="width: 1%;border: 1px #ffffff;">:</td>
							<td style="text-align:left;border: 1px #ffffff;" class="field">
								@if($record->program->penugasanaudit->rencanadetail->tipe == 0)
									<span class="label label-primary">Audit</span>
								@elseif($record->program->penugasanaudit->rencanadetail->tipe == 1)
									<span class="label label-success">Kegiatan Konsultasi</span>
								@else 
									<span class="label label-info">Kegiatan Audit Lain - Lain</span>
								@endif
							</td>
						</tr>
						<tr>
							<td style="width: 10%;border: 1px #ffffff;">Kategori</td>
							<td style="width: 1%;border: 1px #ffffff;">:</td>
							<td style="text-align:left;border: 1px #ffffff;" class="field">
								@if($record->program->penugasanaudit->rencanadetail->tipe == 0)
									@if($record->program->penugasanaudit->rencanadetail->tipe_object == 0)
										<span class="label label-success">Business Unit (BU)</span>
									@elseif($record->program->penugasanaudit->rencanadetail->tipe_object == 1)
										<span class="label label-info">Corporate Office (CO)</span>
									@elseif($record->program->penugasanaudit->rencanadetail->tipe_object == 2)
										<span class="label label-default">Project</span>
									@else 
										<span class="label label-primary">Anak Perusahaan</span>
									@endif
								@elseif($record->program->penugasanaudit->rencanadetail->tipe == 1)
									<span class="label label-warning">{{ $record->program->penugasanaudit->rencanadetail->konsultasi->nama }}</span>
								@else 
									<span class="label label-default">{{ $record->program->penugasanaudit->rencanadetail->lain->nama }}</span>
								@endif
							</td>
						</tr>
						<tr>
							<td style="width: 10%;border: 1px #ffffff;">Objek Audit</td>
							<td style="width: 1%;border: 1px #ffffff;">:</td>
							<td style="text-align: left;border: 1px #ffffff;" class="field">

								{!! object_audit($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id)!!}
							</td>
						</tr>
						<tr>
							<td style="width: 10%;border: 1px #ffffff;">Tanggal Audit</td>
							<td style="width: 1%;border: 1px #ffffff;">:</td>
							<td style="text-align: left;border: 1px #ffffff;" class="field">
								 {{ DateToStringWday($record->program->detailpelaksanaan->min('tgl')) }}  -  {{DateToStringWday($record->program->detailpelaksanaan->max('tgl'))}}
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="form-row">
					<div class="form-group col-md-12">
						<div class="tab-content">
							@if($user == 1)
								<input type="hidden" name="tipe" value="1">
								<table id="table-operasional" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
									<tbody class="container-operasional">
										@php 
											$i=0;
										@endphp
										@foreach($record->detaildraft->where('tipe', 0) as $key => $operasional)
											<tr class="detail-operasional-{{$i}}" data-id="{{$i}}">
												<td style="text-align: left;font-weight: bold;" colspan="5">Bidang Audit : Operasional</td>
											</tr>
											<tr class="data-operasional detail-operasional-{{$i}}">
												<input type="hidden" name="exists_operasional[]" value="{{$operasional->id}}">
												<input type="hidden" name="fokus_audit[0][detail][{{$i}}][data_id]" value="{{$operasional->id}}">
												<input type="hidden" name="fokus_audit[0][detail][{{$i}}][tipe]" value="0">
												<input type="hidden" name="fokus_audit[0][detail][{{$i}}][user_id]" value="{{$users}}">
												<td style="text-align: center;width: 20px;">
													<label style="margin-top: 7px;" class="operasional-numboor-{{$i}}">{{$i+1}}</label>
												</td>
												<td style="width: 150px;">Judul</td>
												<td style="width: 2px;">:</td>
												<td style="width: 150px;text-align: center;padding-top: 15px;" class="kode_operasional1-{{$i}}"></td>
												<td style="width: 1600px;" class="field">
													<div>
														<select class="selectpicker cari-operasional form-control kategori_operasional" name="fokus_audit[0][detail][{{$i}}][standarisasi_id]" data-id="{{$i}}" data-val="{{$operasional->id}}" data-style="btn-default" data-live-search="true">
															<option value="">(Pilih Salah Satu)</option>
															@foreach(App\Models\Master\StandarisasiTemuan::where('bidang', 1)->get() as $standarisasi)
																<option value="{{ $standarisasi->id }}" @if($operasional->standarisasi_id == $standarisasi->id) selected @endif>{{ $standarisasi->deskripsi }}</option>
															@endforeach
														</select>
													</div>
												</td>
												<td style="width: 50px;">
													@if($i == 0)
														<button class="btn btn-sm btn-success tambah_operasional" data-toggle="tooltip" data-placement="left" title="Tambah" type="button" style="border-radius: 20px;"><i class="fa fa-plus"></i></button>
													@else 
														<button class="btn btn-sm btn-danger hapus_operasional" data-toggle="tooltip" data-placement="left" title="Hapus" type="button" style="border-radius: 20px;" data-id="{{$i}}"><i class="fa fa-close"></i></button>
													@endif
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="" rowspan="9"></td>
												<td style="">Kondisi</td>
												<td style="">:</td>
												<td style="" colspan="2"  class="field">
													<textarea class="form-control" name="fokus_audit[0][detail][{{$i}}][catatan_kondisi]" id="exampleFormControlTextarea1" placeholder="Catatan Kondisi" rows="2">{{ $operasional->catatan_kondisi }}</textarea>
												</td>
												<td style="" rowspan="9"></td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="">Kriteria</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field" colspan="2">
													<ol class="kriteria_operasional_{{$i}}">
													</ol>
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="">Sebab</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<textarea class="form-control" name="fokus_audit[0][detail][{{$i}}][sebab]" id="exampleFormControlTextarea1" placeholder="Sebab" rows="2">{{ $operasional->sebab }}
													</textarea>
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="">Risiko</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<textarea class="form-control" name="fokus_audit[0][detail][{{$i}}][risiko]" id="exampleFormControlTextarea1" placeholder="Risiko" rows="2">{{ $operasional->risiko }}
													</textarea>
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="">Kategori Temuan</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
													<div class="field">
														<select class="selectpicker form-control" data-width="310px" name="fokus_audit[0][detail][{{$i}}][kategori_id]" data-style="btn-default" data-live-search="true">
															<option value="">(Pilih Salah Satu)</option>
															@foreach(App\Models\Master\KategoriTemuan::get() as $kategori)
																<option value="{{ $kategori->id }}" @if($kategori->id == $operasional->kategori_id) selected @endif>{{ $kategori->nama }}</option>
															@endforeach
														</select>
													</div>
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="">Rekomendasi</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
													<table id="operasional-rekomendasi-{{$i}}" style="width: 100%;">
														<tbody class="container-operasional-rekomendasi-{{$i}}">
															@foreach($operasional->rekomendasi as $x => $datas)
																<tr>
																	<input type="hidden" name="exist_rekomendasi_operasional[]" value="{{ $datas->id }}">
																	<input type="hidden" name="fokus_audit[0][detail][{{$i}}][detail_rekomen][{{$x}}][id]" value="{{ $datas->id }}">
																	<td style="width: 100%;padding-right: 18px;padding-bottom: 12px;" class="field">
																		<textarea class="form-control" name="fokus_audit[0][detail][{{$i}}][detail_rekomen][{{$x}}][rekomendasi]" id="exampleFormControlTextarea1" placeholder="Rekomendasi" rows="2">{{ $datas->rekomendasi }}</textarea>
																	</td>
																	<td style="width: 5%;">
																		@if($x == 0)
																			<button class="btn btn-sm btn-success tambah_rekomendasi_operasional" data-toggle="tooltip" data-placement="left" title="Tambah" type="button" data-operasional="{{$i}}" style="border-radius: 20px;margin-top: -18px;"><i class="fa fa-plus"></i></button>
																		@else 
																			<button class="btn btn-sm btn-danger hapus_rekomendasi_operasional" data-toggle="tooltip" data-placement="left" data-operasional="{{$i}}" title="Hapus" type="button" style="border-radius: 20px;margin-top: -18px;"><i class="fa fa-close"></i></button>
																		@endif
																	</td>
																</tr>
															@endforeach
															<input type="hidden" name="last_rekomendasi_operasional-{{$i}}" value="{{$x}}">
														</tbody>
													</table>
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="">Tanggapan Auditee</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<div>
														<select data-width="25%" data-id="0" data-detail="{{$i}}" class="selectpicker col-sm-12 show-tick pilihan" data-size="3" name="fokus_audit[0][detail][{{$i}}][tanggapan]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
															<option data-content="<span class='badge' style='background-color:green;'>MENYETUJUI</span>" value="0" @if($operasional->tanggapan == 0) selected @endif>MENYETUJUI</option>
														</select>
													</div>
													<br>
													<textarea class="form-control tanggapan_operasional-{{$i}}" style="display: none;" name="fokus_audit[0][detail][{{$i}}][catatan]" id="exampleFormControlTextarea1" placeholder="Catatan Tanggapan Auditee" rows="2">{{$operasional->catatan}}</textarea>
												</td>
											</tr>
											<tr class="detail-operasional-end-{{$i}} detail-operasional-{{$i}}">
												<td style="">Tanggal Tindak Lanjut</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<input style="width: 25%;" class="form-control tanggal" id="tanggal" name="fokus_audit[0][detail][{{$i}}][tanggal]" placeholder="Tanggal" type="text" value="{{ $operasional->tgl }}" />
												</td>
											</tr>
											<tr class="detail-operasional-end-{{$i}} detail-operasional-{{$i}}" style="border-bottom: 2px solid black;">
												<td style="">Lampiran</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<div class="file-loading">
										            	<input id="op-{{ $operasional->id }}" name="fokus_audit[0][detail][{{$i}}][lampiran]" data-id="{{ $operasional->id }}" type="file" class="form-control lampir" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/jpeg,image/gif,image/png,application/pdf">
										            </div>
												</td>
											</tr>
											@php 
												$i++;
											@endphp
											<input type="hidden" name="last_operasional" value="{{$i}}">
										@endforeach
									</tbody>
								</table>
								<div class="form-group col-md-12">
									<div class="text-right">
										<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
										<button type="button" class="btn btn-primary save as drafting">Save As Draft</button>
										<button type="button" class="btn btn-simpan save as page">Submit</button>
									</div>
								</div>
							@elseif($user == 2)
								<input type="hidden" name="tipe" value="2">
								<table id="table-keuangan" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
									<tbody class="container-keuangan">
										@php 
											$j=0;
										@endphp
										@foreach($record->detaildraft->where('tipe', 1) as $key => $keuangan)
											<tr class="detail-keuangan-{{$j}}" data-id="{{$j}}">
												<td style="text-align: left;font-weight: bold;" colspan="5">Bidang Audit : Keuangan</td>
											</tr>
											<tr class="data-keuangan detail-keuangan-{{$j}}">
												<input type="hidden" name="exists_keuangan[]" value="{{$keuangan->id}}">
												<input type="hidden" name="fokus_audit[1][detail][{{$j}}][data_id]" value="{{$keuangan->id}}">
												<input type="hidden" name="fokus_audit[1][detail][{{$j}}][tipe]" value="1">
												<input type="hidden" name="fokus_audit[1][detail][{{$j}}][user_id]" value="{{$users}}">
												<td style="text-align: center;width: 20px;">
													<label style="margin-top: 7px;" class="keuangan-numboor-{{$j}}">{{$j+1}}</label>
												</td>
												<td style="width: 150px;">Judul</td>
												<td style="width: 2px;">:</td>
												<td style="width: 150px;text-align: center;padding-top: 15px;" class="kode_keuangan1-{{$j}}"></td>
												<td style="width: 1600px;" class="field">
													<div>
														<select class="selectpicker cari-keuangan form-control kategori_keuangan" name="fokus_audit[1][detail][{{$j}}][standarisasi_id]" data-id="{{$j}}" data-val="{{$keuangan->id}}" data-style="btn-default" data-live-search="true">
															<option value="">(Pilih Salah Satu)</option>
															@foreach(App\Models\Master\StandarisasiTemuan::where('bidang', 2)->get() as $standarisasi)
																<option value="{{ $standarisasi->id }}" @if($keuangan->standarisasi_id == $standarisasi->id) selected @endif>{{ $standarisasi->deskripsi }}</option>
															@endforeach
														</select>
													</div>
												</td>
												<td style="width: 50px;">
													@if($j == 0)
														<button class="btn btn-sm btn-success tambah_keuangan" data-toggle="tooltip" data-placement="left" title="Tambah" type="button" style="border-radius: 20px;"><i class="fa fa-plus"></i></button>
													@else 
														<button class="btn btn-sm btn-danger hapus_keuangan" data-toggle="tooltip" data-placement="left" title="Hapus" type="button" style="border-radius: 20px;" data-id="{{$j}}"><i class="fa fa-close"></i></button>
													@endif
												</td>
											</tr>
											<tr class="detail-keuangan-{{$j}}">
												<td style="" rowspan="9"></td>
												<td style="">Kondisi</td>
												<td style="">:</td>
												<td style="" colspan="2"  class="field">
													<textarea class="form-control" name="fokus_audit[1][detail][{{$j}}][catatan_kondisi]" id="exampleFormControlTextarea1" placeholder="Catatan Kondisi" rows="2">{{ $keuangan->catatan_kondisi }}</textarea>
												</td>
												<td style="" rowspan="9"></td>
											</tr>
											<tr class="detail-keuangan-{{$j}}">
												<td style="">Kriteria</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field" colspan="2">
													<ol class="kriteria_keuangan_{{$j}}">
													</ol>
												</td>
											</tr>
											<tr class="detail-keuangan-{{$j}}">
												<td style="">Sebab</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<textarea class="form-control" name="fokus_audit[1][detail][{{$j}}][sebab]" id="exampleFormControlTextarea1" placeholder="Sebab" rows="2">{{ $keuangan->sebab }}
													</textarea>
												</td>
											</tr>
											<tr class="detail-keuangan-{{$j}}">
												<td style="">Risiko</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<textarea class="form-control" name="fokus_audit[1][detail][{{$j}}][risiko]" id="exampleFormControlTextarea1" placeholder="Risiko" rows="2">{{ $keuangan->risiko }}
													</textarea>
												</td>
											</tr>
											<tr class="detail-keuangan-{{$j}}">
												<td style="">Kategori Temuan</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
													<div class="field">
														<select class="selectpicker form-control" data-width="310px" name="fokus_audit[1][detail][{{$j}}][kategori_id]" data-style="btn-default" data-live-search="true">
															<option value="">(Pilih Salah Satu)</option>
															@foreach(App\Models\Master\KategoriTemuan::get() as $kategori)
																<option value="{{ $kategori->id }}" @if($kategori->id == $keuangan->kategori_id) selected @endif>{{ $kategori->nama }}</option>
															@endforeach
														</select>
													</div>
												</td>
											</tr>
											<tr class="detail-keuangan-{{$j}}">
												<td style="">Rekomendasi</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<table id="keuangan-rekomendasi-{{$j}}" style="width: 100%;">
														<tbody class="container-keuangan-rekomendasi-{{$j}}">
															@foreach($keuangan->rekomendasi as $y => $datas)
																<tr>
																	<input type="hidden" name="exist_rekomendasi_keuangan[]" value="{{ $datas->id }}">
																	<input type="hidden" name="fokus_audit[1][detail][{{$j}}][detail_rekomen][{{$y}}][id]" value="{{ $datas->id }}">
																	<td style="width: 100%;padding-right: 18px;padding-bottom: 12px;" class="field">
																		<textarea class="form-control" name="fokus_audit[1][detail][{{$j}}][detail_rekomen][{{$y}}][rekomendasi]" id="exampleFormControlTextarea1" placeholder="Rekomendasi" rows="2">{{ $datas->rekomendasi }}</textarea>
																	</td>
																	<td style="width: 5%;">
																		@if($y == 0)
																			<button class="btn btn-sm btn-success tambah_rekomendasi_keuangan" data-toggle="tooltip" data-placement="left" title="Tambah" type="button" data-keuangan="{{$j}}" style="border-radius: 20px;margin-top: -18px;"><i class="fa fa-plus"></i></button>
																		@else 
																			<button class="btn btn-sm btn-danger hapus_rekomendasi_keuangan" data-toggle="tooltip" data-placement="left" data-keuangan="{{$j}}" title="Hapus" type="button" style="border-radius: 20px;margin-top: -18px;"><i class="fa fa-close"></i></button>
																		@endif
																	</td>
																</tr>
															@endforeach
															<input type="hidden" name="last_rekomendasi_keuangan-{{$j}}" value="{{$y}}">
														</tbody>
													</table>
												</td>
											</tr>
											<tr class="detail-keuangan-{{$j}}">
												<td style="">Tanggapan Auditee</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<div>
														<select data-width="25%" data-id="1" data-detail="{{$j}}" class="selectpicker col-sm-12 show-tick pilihan" data-size="3" name="fokus_audit[1][detail][{{$j}}][tanggapan]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
															<option data-content="<span class='badge' style='background-color:green;'>MENYETUJUI</span>" value="0" @if($keuangan->tanggapan == 0) selected @endif>MENYETUJUI</option>
														</select>
													</div>
													<br>
													<textarea class="form-control tanggapan_keuangan-{{$j}}" style="display: none;" name="fokus_audit[1][detail][{{$j}}][catatan]" id="exampleFormControlTextarea1" placeholder="Catatan Tanggapan Auditee" rows="2">{{$keuangan->catatan}}</textarea>
												</td>
											</tr>
											<tr class="detail-keuangan-end-{{$j}} detail-keuangan-{{$j}}">
												<td style="">Tanggal Tindak Lanjut</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<input style="width: 25%;" class="form-control tanggal" id="tanggal" name="fokus_audit[1][detail][{{$j}}][tanggal]" placeholder="Tanggal" type="text" value="{{ $keuangan->tgl }}" />
												</td>
											</tr>
											<tr class="detail-keuangan-end-{{$j}} detail-keuangan-{{$j}}" style="border-bottom: 2px solid black;">
												<td style="">Lampiran</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<div class="file-loading">
										            	<input id="keu-{{ $keuangan->id }}" name="fokus_audit[1][detail][{{$j}}][lampiran]" data-id="{{ $keuangan->id }}" type="file" class="form-control lampir" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/jpeg,image/gif,image/png,application/pdf">
										            </div>
												</td>
											</tr>
											@php 
												$j++;
											@endphp
											<input type="hidden" name="last_keuangan" value="{{$j}}">
										@endforeach
									</tbody>
								</table>
								<div class="form-group col-md-12">
									<div class="text-right">
										<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
										<button type="button" class="btn btn-primary save as drafting">Save As Draft</button>
										<button type="button" class="btn btn-simpan save as page">Submit</button>
									</div>
								</div>
							@else
								<input type="hidden" name="tipe" value="3">
								<table id="table-sistem" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
									<tbody class="container-sistem">
										@php 
											$k=0;
										@endphp
										@foreach($record->detaildraft->where('tipe', 2) as $key => $sistem)
											<tr class="detail-sistem-{{$k}}" data-id="{{$k}}">
												<td style="text-align: left;font-weight: bold;" colspan="5">Bidang Audit : Sistem</td>
											</tr>
											<tr class="data-sistem detail-sistem-{{$k}}">
												<input type="hidden" name="exists_sistem[]" value="{{$sistem->id}}">
												<input type="hidden" name="fokus_audit[2][detail][{{$k}}][data_id]" value="{{$sistem->id}}">
												<input type="hidden" name="fokus_audit[2][detail][{{$k}}][tipe]" value="2">
												<input type="hidden" name="fokus_audit[2][detail][{{$k}}][user_id]" value="{{$users}}">
												<td style="text-align: center;width: 20px;">
													<label style="margin-top: 7px;" class="sistem-numboor-{{$k}}">{{$k+1}}</label>
												</td>
												<td style="width: 150px;">Judul</td>
												<td style="width: 2px;">:</td>
												<td style="width: 150px;text-align: center;padding-top: 15px;" class="kode_sistem1-{{$k}}"></td>
												<td style="width: 1600px;" class="field">
													<div>
														<select class="selectpicker cari-sistem form-control kategori_sistem" name="fokus_audit[2][detail][{{$k}}][standarisasi_id]" data-id="{{$k}}" data-val="{{$sistem->id}}" data-style="btn-default" data-live-search="true">
															<option value="">(Pilih Salah Satu)</option>
															@foreach(App\Models\Master\StandarisasiTemuan::where('bidang', 3)->get() as $standarisasi)
																<option value="{{ $standarisasi->id }}" @if($sistem->standarisasi_id == $standarisasi->id) selected @endif>{{ $standarisasi->deskripsi }}</option>
															@endforeach
														</select>
													</div>
												</td>
												<td style="width: 50px;">
													@if($k == 0)
														<button class="btn btn-sm btn-success tambah_sistem" data-toggle="tooltip" data-placement="left" title="Tambah" type="button" style="border-radius: 20px;"><i class="fa fa-plus"></i></button>
													@else 
														<button class="btn btn-sm btn-danger hapus_sistem" data-toggle="tooltip" data-placement="left" title="Hapus" type="button" style="border-radius: 20px;" data-id="{{$k}}"><i class="fa fa-close"></i></button>
													@endif
												</td>
											</tr>
											<tr class="detail-sistem-{{$k}}">
												<td style="" rowspan="9"></td>
												<td style="">Kondisi</td>
												<td style="">:</td>
												<td style="" colspan="2"  class="field">
													<textarea class="form-control" name="fokus_audit[2][detail][{{$k}}][catatan_kondisi]" id="exampleFormControlTextarea1" placeholder="Catatan Kondisi" rows="2">{{ $sistem->catatan_kondisi }}</textarea>
												</td>
												<td style="" rowspan="9"></td>
											</tr>
											<tr class="detail-sistem-{{$k}}">
												<td style="">Kriteria</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field" colspan="2">
													<ol class="kriteria_sistem_{{$k}}">
													</ol>
												</td>
											</tr>
											<tr class="detail-sistem-{{$k}}">
												<td style="">Sebab</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<textarea class="form-control" name="fokus_audit[2][detail][{{$k}}][sebab]" id="exampleFormControlTextarea1" placeholder="Sebab" rows="2">{{ $sistem->sebab }}
													</textarea>
												</td>
											</tr>
											<tr class="detail-sistem-{{$k}}">
												<td style="">Risiko</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<textarea class="form-control" name="fokus_audit[2][detail][{{$k}}][risiko]" id="exampleFormControlTextarea1" placeholder="Risiko" rows="2">{{ $sistem->risiko }}
													</textarea>
												</td>
											</tr>
											<tr class="detail-sistem-{{$k}}">
												<td style="">Kategori Temuan</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
													<div class="field">
														<select class="selectpicker form-control" data-width="310px" name="fokus_audit[2][detail][{{$k}}][kategori_id]" data-style="btn-default" data-live-search="true">
															<option value="">(Pilih Salah Satu)</option>
															@foreach(App\Models\Master\KategoriTemuan::get() as $kategori)
																<option value="{{ $kategori->id }}" @if($kategori->id == $sistem->kategori_id) selected @endif>{{ $kategori->nama }}</option>
															@endforeach
														</select>
													</div>
												</td>
											</tr>
											<tr class="detail-sistem-{{$k}}">
												<td style="">Rekomendasi</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<table id="sistem-rekomendasi-{{$k}}" style="width: 100%;">
														<tbody class="container-sistem-rekomendasi-{{$k}}">
															@foreach($sistem->rekomendasi as $z => $datas)
																<tr>
																	<input type="hidden" name="exist_rekomendasi_sistem[]" value="{{ $datas->id }}">
																	<input type="hidden" name="fokus_audit[2][detail][{{$k}}][detail_rekomen][{{$z}}][id]" value="{{ $datas->id }}">
																	<td style="width: 100%;padding-right: 18px;padding-bottom: 12px;" class="field">
																		<textarea class="form-control" name="fokus_audit[2][detail][{{$k}}][detail_rekomen][{{$z}}][rekomendasi]" id="exampleFormControlTextarea1" placeholder="Rekomendasi" rows="2">{{ $datas->rekomendasi }}</textarea>
																	</td>
																	<td style="width: 5%;">
																		@if($z == 0)
																			<button class="btn btn-sm btn-success tambah_rekomendasi_sistem" data-toggle="tooltip" data-placement="left" title="Tambah" type="button" data-sistem="{{$k}}" style="border-radius: 20px;margin-top: -18px;"><i class="fa fa-plus"></i></button>
																		@else 
																			<button class="btn btn-sm btn-danger hapus_rekomendasi_sistem" data-toggle="tooltip" data-placement="left" data-sistem="{{$k}}" title="Hapus" type="button" style="border-radius: 20px;margin-top: -18px;"><i class="fa fa-close"></i></button>
																		@endif
																	</td>
																</tr>
															@endforeach
															<input type="hidden" name="last_rekomendasi_sistem-{{$k}}" value="{{$z}}">
														</tbody>
													</table>
												</td>
											</tr>
											<tr class="detail-sistem-{{$k}}">
												<td style="">Tanggapan Auditee</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<div>
														<select data-width="25%" data-id="2" data-detail="{{$k}}" class="selectpicker col-sm-12 show-tick pilihan" data-size="3" name="fokus_audit[2][detail][{{$k}}][tanggapan]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
															<option data-content="<span class='badge' style='background-color:green;'>MENYETUJUI</span>" value="0" @if($sistem->tanggapan == 0) selected @endif>MENYETUJUI</option>
														</select>
													</div>
													<br>
													<textarea class="form-control tanggapan_sistem-{{$k}}" style="display: none;" name="fokus_audit[2][detail][{{$k}}][catatan]" id="exampleFormControlTextarea1" placeholder="Catatan Tanggapan Auditee" rows="2">{{$sistem->catatan}}</textarea>
												</td>
											</tr>
											<tr class="detail-sistem-end-{{$k}} detail-sistem-{{$k}}">
												<td style="">Tanggal Tindak Lanjut</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<input style="width: 25%;" class="form-control tanggal" id="tanggal" name="fokus_audit[2][detail][{{$k}}][tanggal]" placeholder="Tanggal" type="text" value="{{ $sistem->tgl }}" />
												</td>
											</tr>
											<tr class="detail-sistem-end-{{$k}} detail-sistem-{{$k}}" style="border-bottom: 2px solid black;">
												<td style="">Lampiran</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<div class="file-loading">
										            	<input id="sis-{{ $sistem->id }}" name="fokus_audit[2][detail][{{$k}}][lampiran]" data-id="{{ $sistem->id }}" type="file" class="form-control lampir" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/jpeg,image/gif,image/png,application/pdf">
										            </div>
												</td>
											</tr>
											@php 
												$k++;
											@endphp
											<input type="hidden" name="last_sistem" value="{{$k}}">
										@endforeach
									</tbody>
								</table>
								<div class="form-group col-md-12">
									<div class="text-right">
										<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
										<button type="button" class="btn btn-primary save as drafting">Save As Draft</button>
										<button type="button" class="btn btn-simpan save as page">Submit</button>
									</div>
								</div>
							@endif
						</div>
					</div>
				</div>
		</form>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
        .table-responsive{
            overflow-x: auto;
            overflow-y: visible !important;
        }

        .table-responsive .dropdown-menu {
	        position: static !important;
	    }

	    div.dropdown-menu.open{
		  max-width: 300px !important;
		  overflow: hidden;
		}
		ul.dropdown-menu.inner{
		  max-width: 300px !important;
		  overflow-y: auto;
		}
		.datepicker.dropdown-menu {
		    z-index: 9999 !important;
		}
    </style>
@endpush

@push('scripts')
    <script>
    	$(document).on('click', '.page1', function (e){
    		$('#a-tab').removeClass('active');
    		$('#a-tab').closest('li').removeClass('active');
    		$('#b-tab').addClass('active');
    		$('#b-tab').closest('li').addClass('active');
    		$('#a').removeClass('active');
    		$('#b').addClass('active');
		});

		$(document).on('click', '.kembali1', function (e){
    		$('#a-tab').addClass('active');
    		$('#a-tab').closest('li').addClass('active');
    		$('#b-tab').removeClass('active');
    		$('#b-tab').closest('li').removeClass('active');
    		$('#a').addClass('active');
    		$('#b').removeClass('active');
		});

		$(document).on('click', '.page2', function (e){
    		$('#b-tab').removeClass('active');
    		$('#b-tab').closest('li').removeClass('active');
    		$('#c-tab').addClass('active');
    		$('#c-tab').closest('li').addClass('active');
    		$('#b').removeClass('active');
    		$('#c').addClass('active');
		});

		$(document).on('click', '.kembali2', function (e){
    		$('#b-tab').addClass('active');
    		$('#b-tab').closest('li').addClass('active');
    		$('#c-tab').removeClass('active');
    		$('#c-tab').closest('li').removeClass('active');
    		$('#b').addClass('active');
    		$('#c').removeClass('active');
		});

    	var d = new Date();
    	var full = d.getFullYear();
    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
    	var last = new Date(lastDate);
    	if($('input[name=tahun]').val() > full){
    		var min = minDates;
    	}else{
    		var min = d;
    	}

    	$('.tanggal').datepicker({
    		orientation: "top left",
    		todayHighlight: true,
    		autoclose:true,
    		format: 'dd-mm-yyyy',
    		defaultViewDate: {year:$('input[name=tahun]').val(), month:0, day:1},
    		yearRange: '-0:+1',
    		startDate: minDates,
    		endDate: last,
    		hideIfNoPrevNext: true,
        });

        $('.cari-operasional').each(function(){
            var id = $(this).data("id");
            var op_id = $(this).data("val");
            var val = $(this).val();

            $.ajax({
				url: '{{ url('ajax/option/get-kriteria-temuans') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					id: val
				},
			})
			.done(function(response) {
				console.log(id)
				var kriteria = $('input[name="fokus_audit[0][detail]['+id+'][kriterias]"]').val();
				$('.kode_operasional1-'+id).html('');
				$('.kode_operasional1-'+id).html(response[0]);
				$('.kriteria_operasional_'+id).html(response[1]);

				var cek = 0;
				var detail = id;
				var data = $('select[name="fokus_audit[0][detail]['+detail+'][tanggapan]"] option:selected').val();
				if(data == 0){
					$('.tanggapan_operasional-'+detail).hide();
					$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", true);
					$('input[name="fokus_audit[0][detail]['+detail+'][tanggal]"]').prop("disabled", false);
				}else{
					$('.tanggapan_operasional-'+detail).show();
					$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", false);
					$('input[name="fokus_audit[0][detail]['+detail+'][tanggal]"]').val('');
					$('input[name="fokus_audit[0][detail]['+detail+'][tanggal]"]').prop("disabled", true);
				}
			})
			.fail(function() {
				console.log("error");
			});

            $.ajax({
                url: '{{ url('ajax/option/get-kka') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: op_id
                },
            }).done(function(response) {
            	if(response.length > 0){
	            	$('#op-'+op_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreview: JSON.parse(response[0].replace(/&quot;/g,'"')),
						initialPreviewAsData: true,
						initialPreviewConfig: JSON.parse(response[1].replace(/&quot;/g,'"')),
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
				}else {
					$('#op-'+op_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreviewAsData: true,
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
				}
				$('.kv-file-upload').prop('disabled', false);
				$('.kv-file-zoom').prop('disabled', false);
            }).fail(function() {
                console.log("error");
            });
        });

        $('.cari-keuangan').each(function(){
            var id = $(this).data("id");
            var keu_id = $(this).data("val");
            var val = $(this).val();

            $.ajax({
				url: '{{ url('ajax/option/get-kriteria-temuans') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					id: val
				},
			})
			.done(function(response) {
				var kriteria = $('input[name="fokus_audit[1][detail]['+id+'][kriterias]"]').val();
				$('.kode_keuangan1-'+id).html('');
				$('.kode_keuangan1-'+id).html(response[0]);
				$('.kriteria_keuangan_'+id).html(response[1]);

				var cek = 1;
				var detail = id;
				var data = $('select[name="fokus_audit[1][detail]['+detail+'][tanggapan]"] option:selected').val();
				if(data == 0){
					$('.tanggapan_keuangan-'+detail).hide();
					$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", true);
					$('input[name="fokus_audit[1][detail]['+detail+'][tanggal]"]').prop("disabled", false);
				}else{
					$('.tanggapan_keuangan-'+detail).show();
					$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", false);
					$('input[name="fokus_audit[1][detail]['+detail+'][tanggal]"]').val('');
					$('input[name="fokus_audit[1][detail]['+detail+'][tanggal]"]').prop("disabled", true);
				}
			})
			.fail(function() {
				console.log("error");
			});

			$.ajax({
                url: '{{ url('ajax/option/get-kka') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: keu_id
                },
            }).done(function(response) {
            	if(response.length > 0){
	            	$('#keu-'+keu_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreview: JSON.parse(response[0].replace(/&quot;/g,'"')),
						initialPreviewAsData: true,
						initialPreviewConfig: JSON.parse(response[1].replace(/&quot;/g,'"')),
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}else{
            		$('#keu-'+keu_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreviewAsData: true,
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}
				$('.kv-file-upload').prop('disabled', false);
				$('.kv-file-zoom').prop('disabled', false);
            }).fail(function() {
                console.log("error");
            });
        });

        $('.cari-sistem').each(function(){
            var id = $(this).data("id");
            var sis_id = $(this).data("val");
            var val = $(this).val();

            $.ajax({
				url: '{{ url('ajax/option/get-kriteria-temuans') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					id: val
				},
			})
			.done(function(response) {
				var kriteria = $('input[name="fokus_audit[2][detail]['+id+'][kriterias]"]').val();
				$('.kode_sistem1-'+id).html('');
				$('.kode_sistem1-'+id).html(response[0]);
				$('.kriteria_sistem_'+id).html(response[1]);

				var cek = 2;
				var detail = id;
				var data = $('select[name="fokus_audit[2][detail]['+detail+'][tanggapan]"] option:selected').val();
				if(data == 0){
					if(cek == 0){
						$('.tanggapan_operasional-'+detail).hide();
						$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", true);
					}else if(cek == 1){
						$('.tanggapan_keuangan-'+detail).hide();
						$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", true);
					}else{
						$('.tanggapan_sistem-'+detail).hide();
						$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", true);
					}
					$('input[name="fokus_audit[2][detail]['+detail+'][tanggal]"]').prop("disabled", false);
				}else{
					if(cek == 0){
						$('.tanggapan_operasional-'+detail).show();
						$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", false);
					}else if(cek == 1){
						$('.tanggapan_keuangan-'+detail).show();
						$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", false);
					}else{
						$('.tanggapan_sistem-'+detail).show();
						$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", false);
					}
					$('input[name="fokus_audit[2][detail]['+detail+'][tanggal]"]').val('');
					$('input[name="fokus_audit[2][detail]['+detail+'][tanggal]"]').prop("disabled", true);
				}
			})
			.fail(function() {
				console.log("error");
			});

			$.ajax({
                url: '{{ url('ajax/option/get-kka') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: sis_id
                },
            }).done(function(response) {
            	if(response.length > 0){
	            	$('#sis-'+sis_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreview: JSON.parse(response[0].replace(/&quot;/g,'"')),
						initialPreviewAsData: true,
						initialPreviewConfig: JSON.parse(response[1].replace(/&quot;/g,'"')),
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}else{
            		$('#sis-'+sis_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreviewAsData: true,
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}
				$('.kv-file-upload').prop('disabled', false);
				$('.kv-file-zoom').prop('disabled', false);
            }).fail(function() {
                console.log("error");
            });
        });

		$('.pilihan').on('change', function(){
			var id = $(this).data("id");
			var detail = $(this).data("detail");
			var data = $('select[name="fokus_audit['+id+'][detail]['+detail+'][tanggapan]"] option:selected').val();
			if(data == 0){
				if(id == 0){
					$('.tanggapan_operasional-'+detail).hide();
					$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", true);
				}else if(id == 1){
					$('.tanggapan_keuangan-'+detail).hide();
					$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", true);
				}else{
					$('.tanggapan_sistem-'+detail).hide();
					$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", true);
				}
				$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').prop("disabled", false);
			}else{
				if(id == 0){
					$('.tanggapan_operasional-'+detail).show();
					$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", false);
				}else if(id == 1){
					$('.tanggapan_keuangan-'+detail).show();
					$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", false);
				}else{
					$('.tanggapan_sistem-'+detail).show();
					$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", false);
				}
				$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').val('');
				$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').prop("disabled", true);
			}
		});

		$('.kategori_operasional').on('change', function(){
			var id = $(this).data("id");
			var data = $('select[name="fokus_audit[0][detail]['+id+'][standarisasi_id]"] option:selected').val();
			$.ajax({
				url: '{{ url('ajax/option/get-kriteria-temuans') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					id: data
				},
			})
			.done(function(response) {
				$('.kode_operasional1-'+id).html('');
				$('.kode_operasional1-'+id).html(response[0]);
				$('.kriteria_operasional_'+id).html(response[1]);
			})
			.fail(function() {
				console.log("error");
			});
		});

		$('.kategori_keuangan').on('change', function(){
			var id = $(this).data("id");
			var data = $('select[name="fokus_audit[1][detail]['+id+'][standarisasi_id]"] option:selected').val();
			$.ajax({
				url: '{{ url('ajax/option/get-kriteria-temuans') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					id: data
				},
			})
			.done(function(response) {
				$('.kode_keuangan1-'+id).html('');
				$('.kode_keuangan1-'+id).html(response[0]);
				$('.kriteria_keuangan_'+id).html(response[1]);
			})
			.fail(function() {
				console.log("error");
			});
		});

		$('.kategori_sistem').on('change', function(){
			var id = $(this).data("id");
			var data = $('select[name="fokus_audit[2][detail]['+id+'][standarisasi_id]"] option:selected').val();
			$.ajax({
				url: '{{ url('ajax/option/get-kriteria-temuans') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					id: data
				},
			})
			.done(function(response) {
				$('.kode_sistem1-'+id).html('');
				$('.kode_sistem1-'+id).html(response[0]);
				$('.kriteria_sistem_'+id).html(response[1]);
			})
			.fail(function() {
				console.log("error");
			});
		});

    	$('#myTab li:first-child a').tab('show')
    	//OPERASIONAL
    	$(document).on('click', '.tambah_operasional', function(e){
    		var last_operasional = parseInt($('input[name=last_operasional]').val()) + 2;
			var rowCount = $('#table-operasional > tbody > tr.data-operasional').length;
			var c = rowCount;
			var html = `
					<tr class="data-operasional detail-operasional-`+last_operasional+`" data-id=`+last_operasional+`>
						<input type="hidden" name="fokus_audit[0][detail][`+last_operasional+`][user_id]" value="{{$users}}">
						<input type="hidden" name="fokus_audit[0][detail][`+last_operasional+`][tipe]" value="0">
						<td style="text-align: center;width: 20px;">
							<label style="margin-top: 7px;" class="operasional-numboor-`+last_operasional+`">`+(c+1)+`</label>
						</td>
						<td style="width: 150px;">Judul</td>
						<td style="width: 2px;">:</td>
						<td style="width: 150px;text-align: center;padding-top: 15px;" class="kode_operasional1-`+last_operasional+`"></td>
						<td style="width: 1600px;" class="field">
							<div>
								<select class="selectpicker form-control kategori_operasional" name="fokus_audit[0][detail][`+last_operasional+`][standarisasi_id]" data-id="`+last_operasional+`" data-style="btn-default" data-live-search="true">
									<option value="">(Pilih Salah Satu)</option>
									@foreach(App\Models\Master\StandarisasiTemuan::where('bidang', 1)->get() as $standarisasi)
										<option value="{{ $standarisasi->id }}">{{ $standarisasi->deskripsi }}</option>
									@endforeach
								</select>
							</div>
						</td>
						<td style="width: 50px;">
							<button class="btn btn-sm btn-danger hapus_operasional" data-toggle="tooltip" data-placement="left" title="Hapus" type="button" style="border-radius: 20px;" data-id="`+last_operasional+`"><i class="fa fa-close"></i></button>
						</td>
					</tr>
					<tr class="detail-operasional-`+last_operasional+`">
						<td style="" rowspan="9"></td>
						<td style="">Kondisi</td>
						<td style="">:</td>
						<td style="" colspan="2"  class="field">
							<textarea class="form-control" name="fokus_audit[0][detail][`+last_operasional+`][catatan_kondisi]" id="exampleFormControlTextarea1" placeholder="Catatan Kondisi" rows="2"></textarea>
						</td>
						<td style="" rowspan="9"></td>
					</tr>
					<tr class="detail-operasional-`+last_operasional+`">
						<td style="">Kriteria</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field" colspan="2">
							<ol class="kriteria_operasional_`+last_operasional+`">
							</ol>
						</td>
					</tr>
					<tr class="detail-operasional-`+last_operasional+`">
						<td style="">Sebab</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<textarea class="form-control" name="fokus_audit[0][detail][`+last_operasional+`][sebab]" id="exampleFormControlTextarea1" placeholder="Sebab" rows="2"></textarea>
						</td>
					</tr>
					<tr class="detail-operasional-`+last_operasional+`">
						<td style="">Risiko</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<textarea class="form-control" name="fokus_audit[0][detail][`+last_operasional+`][risiko]" id="exampleFormControlTextarea1" placeholder="Risiko" rows="2"></textarea>
						</td>
					</tr>
					<tr class="detail-operasional-`+last_operasional+`">
						<td style="">Kategori Temuan</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
							<div class="field">
								<select class="selectpicker form-control" data-width="310px" name="fokus_audit[0][detail][`+last_operasional+`][kategori_id]" data-style="btn-default" data-live-search="true">
									<option value="">(Pilih Salah Satu)</option>
									@foreach(App\Models\Master\KategoriTemuan::get() as $kategori)
										<option value="{{ $kategori->id }}">{{ $kategori->nama }}</option>
									@endforeach
								</select>
							</div>
						</td>
					</tr>
					<tr class="detail-operasional-`+last_operasional+`">
						<td style="">Rekomendasi</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
							<table id="operasional-rekomendasi-`+last_operasional+`" style="width: 100%;">
								<tbody class="container-operasional-rekomendasi-`+last_operasional+`">
									<tr>
										<td style="width: 100%;padding-right: 18px;padding-bottom: 12px;" class="field">
											<textarea class="form-control" name="fokus_audit[0][detail][`+last_operasional+`][detail_rekomen][0][rekomendasi]" id="exampleFormControlTextarea1" placeholder="Rekomendasi" rows="2"></textarea>
										</td>
										<td style="width: 5%;">
											<button class="btn btn-sm btn-success tambah_rekomendasi_operasional" data-toggle="tooltip" data-placement="left" title="Tambah" type="button" data-operasional="`+last_operasional+`" style="border-radius: 20px;margin-top: -18px;"><i class="fa fa-plus"></i></button>
										</td>
									</tr>
									<input type="hidden" name="last_rekomendasi_operasional-`+last_operasional+`" value="0">
								</tbody>
							</table>
						</td>
					</tr>
					<tr class="detail-operasional-`+last_operasional+`">
						<td style="">Tanggapan Auditee</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<div>
								<select data-width="25%" data-id="0" data-detail="`+last_operasional+`" class="selectpicker col-sm-12 show-tick pilihan" data-size="3" name="fokus_audit[0][detail][`+last_operasional+`][tanggapan]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
									<option data-content="<span class='badge' style='background-color:green;'>MENYETUJUI</span>" value="0" selected>MENYETUJUI</option>
								</select>
							</div>
							<br>
							<textarea class="form-control tanggapan_operasional-`+last_operasional+`" style="display: none;" name="fokus_audit[0][detail][`+last_operasional+`][catatan]" id="exampleFormControlTextarea1" placeholder="Catatan Tanggapan Auditee" rows="2"></textarea>
						</td>
					</tr>
					<tr class="detail-operasional-end-`+last_operasional+` detail-operasional-`+last_operasional+`">
						<td style="">Tanggal Tindak Lanjut</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<input style="width: 25%;" class="form-control tanggal" id="tanggal" name="fokus_audit[0][detail][`+last_operasional+`][tanggal]" placeholder="Tanggal" type="text"/>
						</td>
					</tr>
					<tr class="detail-operasional-end-`+last_operasional+` detail-operasional-`+last_operasional+`" style="border-bottom: 2px solid black;">
						<td style="">Lampiran</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<div class="file-loading">
				            	<input id="img-0-`+last_operasional+`" name="fokus_audit[0][detail][`+last_operasional+`][lampiran]" type="file" class="file form-control lampir" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/jpeg,image/gif,image/png,application/pdf">
				            </div>
						</td>
					</tr>
				`;

				$('.container-operasional').append(html);
		        $('input[name=last_operasional]').val(last_operasional);

		        $('.lampir').fileinput({
				    language: 'es',
				    uploadUrl: "/file-upload-batch/2",
				    previewFileType: "image",
				    showUpload: false,
				    previewFileIcon: '<i class="fas fa-file"></i>',
				    fileActionSettings: {
					    showRemove: true,
					    showUpload: false, //This remove the upload button
					    showZoom: true,
					    showDrag: true
					},
				    initialPreviewAsData: true,
				    purifyHtml: true,
			        // // allowed image size up to 5 MB
			        // maxFileSize: 2000,
				});

		        var d = new Date();
		    	var full = d.getFullYear();
		    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
		    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
		    	var last = new Date(lastDate);
		    	if($('input[name=tahun]').val() > full){
		    		var min = minDates;
		    	}else{
		    		var min = d;
		    	}

		    	$('.tanggal').datepicker({
		    		orientation: "top left",
		    		todayHighlight: true,
		    		autoclose:true,
		    		format: 'dd-mm-yyyy',
		    		defaultViewDate: {year:$('input[name=tahun]').val(), month:0, day:1},
		    		yearRange: '-0:+1',
		    		startDate: minDates,
		    		endDate: last,
		    		hideIfNoPrevNext: true,
		        });

				$('.selectpicker').selectpicker();

				$('.kategori_operasional').on('change', function(){
					var id = $(this).data("id");
					var data = $('select[name="fokus_audit[0][detail]['+id+'][standarisasi_id]"] option:selected').val();
					$.ajax({
						url: '{{ url('ajax/option/get-kriteria-temuans') }}',
						type: 'POST',
						data: {
							_token: "{{ csrf_token() }}",
							id: data
						},
					})
					.done(function(response) {
						$('.kode_operasional1-'+id).html('');
						$('.kode_operasional1-'+id).html(response[0]);
						$('.kriteria_operasional_'+id).html(response[1]);
					})
					.fail(function() {
						console.log("error");
					});
				});

				$('.pilihan').on('change', function(){
					var id = $(this).data("id");
					var detail = $(this).data("detail");
					var data = $('select[name="fokus_audit['+id+'][detail]['+detail+'][tanggapan]"] option:selected').val();
					if(data == 0){
						if(id == 0){
							$('.tanggapan_operasional-'+detail).hide();
							$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", true);
						}else if(id == 1){
							$('.tanggapan_keuangan-'+detail).hide();
							$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", true);
						}else{
							$('.tanggapan_sistem-'+detail).hide();
							$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", true);
						}
						$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').prop("disabled", false);
					}else{
						if(id == 0){
							$('.tanggapan_operasional-'+detail).show();
							$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", false);
						}else if(id == 1){
							$('.tanggapan_keuangan-'+detail).show();
							$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", false);
						}else{
							$('.tanggapan_sistem-'+detail).show();
							$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", false);
						}
						$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').val('');
						$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').prop("disabled", true);
					}
				});
		});

		$(document).on('click', '.hapus_operasional', function (e){
			var id = $(this).data("id");
			var rowz = $('tr.detail-operasional-'+id).remove();
			var table = $('#table-operasional');
			var rows = table.find('tbody tr.data-operasional');
			$.each(rows, function(key, value){
				table.find('.operasional-numboor-'+$(this).data("id")).html(key+1);
			});
		});

		//KEUANGAN
		$(document).on('click', '.tambah_keuangan', function(e){
    		var last_keuangan = parseInt($('input[name=last_keuangan]').val()) + 2;
			var rowCount = $('#table-keuangan > tbody > tr.data-keuangan').length;
			var c = rowCount;
			var html = `
					<tr class="data-keuangan detail-keuangan-`+last_keuangan+`" data-id=`+last_keuangan+`>
						<input type="hidden" name="fokus_audit[1][detail][`+last_keuangan+`][user_id]" value="{{$users}}">
						<input type="hidden" name="fokus_audit[1][detail][`+last_keuangan+`][tipe]" value="1">
						<td style="text-align: center;width: 20px;">
							<label style="margin-top: 7px;" class="keuangan-numboor-`+last_keuangan+`">`+(c+1)+`</label>
						</td>
						<td style="width: 150px;">Judul</td>
						<td style="width: 2px;">:</td>
						<td style="width: 150px;text-align: center;padding-top: 15px;" class="kode_keuangan1-`+last_keuangan+`"></td>
						<td style="width: 1600px;" class="field">
							<div>
								<select class="selectpicker form-control kategori_keuangan" name="fokus_audit[1][detail][`+last_keuangan+`][standarisasi_id]" data-id="`+last_keuangan+`" data-style="btn-default" data-live-search="true">
									<option value="">(Pilih Salah Satu)</option>
									@foreach(App\Models\Master\StandarisasiTemuan::where('bidang', 2)->get() as $standarisasi)
										<option value="{{ $standarisasi->id }}">{{ $standarisasi->deskripsi }}</option>
									@endforeach
								</select>
							</div>
						</td>
						<td style="width: 50px;">
							<button class="btn btn-sm btn-danger hapus_keuangan" data-toggle="tooltip" data-placement="left" title="Hapus" type="button" style="border-radius: 20px;" data-id="`+last_keuangan+`"><i class="fa fa-close"></i></button>
						</td>
					</tr>
					<tr class="detail-keuangan-`+last_keuangan+`">
						<td style="" rowspan="9"></td>
						<td style="">Kondisi</td>
						<td style="">:</td>
						<td style="" colspan="2"  class="field">
							<textarea class="form-control" name="fokus_audit[1][detail][`+last_keuangan+`][catatan_kondisi]" id="exampleFormControlTextarea1" placeholder="Catatan Kondisi" rows="2"></textarea>
						</td>
						<td style="" rowspan="9"></td>
					</tr>
					<tr class="detail-keuangan-`+last_keuangan+`">
						<td style="">Kriteria</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field" colspan="2">
							<ol class="kriteria_keuangan_`+last_keuangan+`">
							</ol>
						</td>
					</tr>
					<tr class="detail-keuangan-`+last_keuangan+`">
						<td style="">Sebab</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<textarea class="form-control" name="fokus_audit[1][detail][`+last_keuangan+`][sebab]" id="exampleFormControlTextarea1" placeholder="Sebab" rows="2"></textarea>
						</td>
					</tr>
					<tr class="detail-keuangan-`+last_keuangan+`">
						<td style="">Risiko</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<textarea class="form-control" name="fokus_audit[1][detail][`+last_keuangan+`][risiko]" id="exampleFormControlTextarea1" placeholder="Risiko" rows="2"></textarea>
						</td>
					</tr>
					<tr class="detail-keuangan-`+last_keuangan+`">
						<td style="">Kategori Temuan</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
							<div class="field">
								<select class="selectpicker form-control" data-width="310px" name="fokus_audit[1][detail][`+last_keuangan+`][kategori_id]" data-style="btn-default" data-live-search="true">
									<option value="">(Pilih Salah Satu)</option>
									@foreach(App\Models\Master\KategoriTemuan::get() as $kategori)
										<option value="{{ $kategori->id }}">{{ $kategori->nama }}</option>
									@endforeach
								</select>
							</div>
						</td>
					</tr>
					<tr class="detail-keuangan-`+last_keuangan+`">
						<td style="">Rekomendasi</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
							<table id="keuangan-rekomendasi-`+last_keuangan+`" style="width: 100%;">
								<tbody class="container-keuangan-rekomendasi-`+last_keuangan+`">
									<tr>
										<td style="width: 100%;padding-right: 18px;padding-bottom: 12px;" class="field">
											<textarea class="form-control" name="fokus_audit[1][detail][`+last_keuangan+`][detail_rekomen][0][rekomendasi]" id="exampleFormControlTextarea1" placeholder="Rekomendasi" rows="2"></textarea>
										</td>
										<td style="width: 5%;">
											<button class="btn btn-sm btn-success tambah_rekomendasi_keuangan" data-toggle="tooltip" data-placement="left" title="Tambah" type="button" data-keuangan="`+last_keuangan+`" style="border-radius: 20px;margin-top: -18px;"><i class="fa fa-plus"></i></button>
										</td>
									</tr>
									<input type="hidden" name="last_rekomendasi_keuangan-`+last_keuangan+`" value="0">
								</tbody>
							</table>
						</td>
					</tr>
					<tr class="detail-keuangan-`+last_keuangan+`">
						<td style="">Tanggapan Auditee</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<div>
								<select data-width="25%" data-id="1" data-detail="`+last_keuangan+`" class="selectpicker col-sm-12 show-tick pilihan" data-size="3" name="fokus_audit[1][detail][`+last_keuangan+`][tanggapan]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
									<option data-content="<span class='badge' style='background-color:green;'>MENYETUJUI</span>" value="0" selected>MENYETUJUI</option>
								</select>
							</div>
							<br>
							<textarea class="form-control tanggapan_keuangan-`+last_keuangan+`" style="display: none;" name="fokus_audit[1][detail][`+last_keuangan+`][catatan]" id="exampleFormControlTextarea1" placeholder="Catatan Tanggapan Auditee" rows="2"></textarea>
						</td>
					</tr>
					<tr class="detail-keuangan-end-`+last_keuangan+` detail-keuangan-`+last_keuangan+`">
						<td style="">Tanggal Tindak Lanjut</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<input style="width: 25%;" class="form-control tanggal" id="tanggal" name="fokus_audit[1][detail][`+last_keuangan+`][tanggal]" placeholder="Tanggal" type="text"/>
						</td>
					</tr>
					<tr class="detail-keuangan-end-`+last_keuangan+` detail-keuangan-`+last_keuangan+`" style="border-bottom: 2px solid black;">
						<td style="">Lampiran</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<div class="file-loading">
				            	<input id="img-0-`+last_keuangan+`" name="fokus_audit[1][detail][`+last_keuangan+`][lampiran]" type="file" class="file form-control lampir" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/jpeg,image/gif,image/png,application/pdf">
				            </div>
						</td>
					</tr>
				`;

				$('.container-keuangan').append(html);
		        $('input[name=last_keuangan]').val(last_keuangan);

		        $('.lampir').fileinput({
				    language: 'es',
				    uploadUrl: "/file-upload-batch/2",
				    previewFileType: "image",
				    showUpload: false,
				    previewFileIcon: '<i class="fas fa-file"></i>',
				    fileActionSettings: {
					    showRemove: true,
					    showUpload: false, //This remove the upload button
					    showZoom: true,
					    showDrag: true
					},
				    initialPreviewAsData: true,
				    purifyHtml: true,
				});

		        var d = new Date();
		    	var full = d.getFullYear();
		    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
		    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
		    	var last = new Date(lastDate);
		    	if($('input[name=tahun]').val() > full){
		    		var min = minDates;
		    	}else{
		    		var min = d;
		    	}

		    	$('.tanggal').datepicker({
		    		orientation: "top left",
		    		todayHighlight: true,
		    		autoclose:true,
		    		format: 'dd-mm-yyyy',
		    		defaultViewDate: {year:$('input[name=tahun]').val(), month:0, day:1},
		    		yearRange: '-0:+1',
		    		startDate: minDates,
		    		endDate: last,
		    		hideIfNoPrevNext: true,
		        });

				$('.selectpicker').selectpicker();

				$('.kategori_keuangan').on('change', function(){
					var id = $(this).data("id");
					var data = $('select[name="fokus_audit[1][detail]['+id+'][standarisasi_id]"] option:selected').val();
					$.ajax({
						url: '{{ url('ajax/option/get-kriteria-temuans') }}',
						type: 'POST',
						data: {
							_token: "{{ csrf_token() }}",
							id: data
						},
					})
					.done(function(response) {
						$('.kode_keuangan1-'+id).html('');
						$('.kode_keuangan1-'+id).html(response[0]);
						$('.kriteria_keuangan_'+id).html(response[1]);
					})
					.fail(function() {
						console.log("error");
					});
				});

				$('.pilihan').on('change', function(){
					var id = $(this).data("id");
					var detail = $(this).data("detail");
					var data = $('select[name="fokus_audit['+id+'][detail]['+detail+'][tanggapan]"] option:selected').val();
					if(data == 0){
						if(id == 0){
							$('.tanggapan_operasional-'+detail).hide();
							$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", true);
						}else if(id == 1){
							$('.tanggapan_keuangan-'+detail).hide();
							$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", true);
						}else{
							$('.tanggapan_sistem-'+detail).hide();
							$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", true);
						}
						$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').prop("disabled", false);
					}else{
						if(id == 0){
							$('.tanggapan_operasional-'+detail).show();
							$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", false);
						}else if(id == 1){
							$('.tanggapan_keuangan-'+detail).show();
							$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", false);
						}else{
							$('.tanggapan_sistem-'+detail).show();
							$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", false);
						}
						$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').val('');
						$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').prop("disabled", true);
					}
				});
		});

		$(document).on('click', '.hapus_keuangan', function (e){
			var id = $(this).data("id");
			var rowz = $('tr.detail-keuangan-'+id).remove();
			var table = $('#table-keuangan');
			var rows = table.find('tbody tr.data-keuangan');
			$.each(rows, function(key, value){
				table.find('.keuangan-numboor-'+$(this).data("id")).html(key+1);
			});
		});

		// SISTEM
		$(document).on('click', '.tambah_sistem', function(e){
    		var last_sistem = parseInt($('input[name=last_sistem]').val()) + 2;
			var rowCount = $('#table-sistem > tbody > tr.data-sistem').length;
			var c = rowCount;
			var html = `
					<tr class="data-sistem detail-sistem-`+last_sistem+`" data-id=`+last_sistem+`>
						<input type="hidden" name="fokus_audit[2][detail][`+last_sistem+`][user_id]" value="{{$users}}">
						<input type="hidden" name="fokus_audit[2][detail][`+last_sistem+`][tipe]" value="2">
						<td style="text-align: center;width: 20px;">
							<label style="margin-top: 7px;" class="sistem-numboor-`+last_sistem+`">`+(c+1)+`</label>
						</td>
						<td style="width: 150px;">Judul</td>
						<td style="width: 2px;">:</td>
						<td style="width: 150px;text-align: center;padding-top: 15px;" class="kode_sistem1-`+last_sistem+`"></td>
						<td style="width: 1600px;" class="field">
							<div>
								<select class="selectpicker form-control kategori_sistem" name="fokus_audit[2][detail][`+last_sistem+`][standarisasi_id]" data-id="`+last_sistem+`" data-style="btn-default" data-live-search="true">
									<option value="">(Pilih Salah Satu)</option>
									@foreach(App\Models\Master\StandarisasiTemuan::where('bidang', 3)->get() as $standarisasi)
										<option value="{{ $standarisasi->id }}">{{ $standarisasi->deskripsi }}</option>
									@endforeach
								</select>
							</div>
						</td>
						<td style="width: 50px;">
							<button class="btn btn-sm btn-danger hapus_sistem" data-toggle="tooltip" data-placement="left" title="Hapus" type="button" style="border-radius: 20px;" data-id="`+last_sistem+`"><i class="fa fa-close"></i></button>
						</td>
					</tr>
					<tr class="detail-sistem-`+last_sistem+`">
						<td style="" rowspan="9"></td>
						<td style="">Kondisi</td>
						<td style="">:</td>
						<td style="" colspan="2"  class="field">
							<textarea class="form-control" name="fokus_audit[2][detail][`+last_sistem+`][catatan_kondisi]" id="exampleFormControlTextarea1" placeholder="Catatan Kondisi" rows="2"></textarea>
						</td>
						<td style="" rowspan="9"></td>
					</tr>
					<tr class="detail-sistem-`+last_sistem+`">
						<td style="">Kriteria</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field" colspan="2">
							<ol class="kriteria_sistem_`+last_sistem+`">
							</ol>
						</td>
					</tr>
					<tr class="detail-sistem-`+last_sistem+`">
						<td style="">Sebab</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<textarea class="form-control" name="fokus_audit[2][detail][`+last_sistem+`][sebab]" id="exampleFormControlTextarea1" placeholder="Sebab" rows="2"></textarea>
						</td>
					</tr>
					<tr class="detail-sistem-`+last_sistem+`">
						<td style="">Risiko</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<textarea class="form-control" name="fokus_audit[2][detail][`+last_sistem+`][risiko]" id="exampleFormControlTextarea1" placeholder="Risiko" rows="2"></textarea>
						</td>
					</tr>
					<tr class="detail-sistem-`+last_sistem+`">
						<td style="">Kategori Temuan</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
							<div class="field">
								<select class="selectpicker form-control" data-width="310px" name="fokus_audit[2][detail][`+last_sistem+`][kategori_id]" data-style="btn-default" data-live-search="true">
									<option value="">(Pilih Salah Satu)</option>
									@foreach(App\Models\Master\KategoriTemuan::get() as $kategori)
										<option value="{{ $kategori->id }}">{{ $kategori->nama }}</option>
									@endforeach
								</select>
							</div>
						</td>
					</tr>
					<tr class="detail-sistem-`+last_sistem+`">
						<td style="">Rekomendasi</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
							<table id="sistem-rekomendasi-`+last_sistem+`" style="width: 100%;">
								<tbody class="container-sistem-rekomendasi-`+last_sistem+`">
									<tr>
										<td style="width: 100%;padding-right: 18px;padding-bottom: 12px;" class="field">
											<textarea class="form-control" name="fokus_audit[2][detail][`+last_sistem+`][detail_rekomen][0][rekomendasi]" id="exampleFormControlTextarea1" placeholder="Rekomendasi" rows="2"></textarea>
										</td>
										<td style="width: 5%;">
											<button class="btn btn-sm btn-success tambah_rekomendasi_sistem" data-toggle="tooltip" data-placement="left" title="Tambah" type="button" data-sistem="`+last_sistem+`" style="border-radius: 20px;margin-top: -18px;"><i class="fa fa-plus"></i></button>
										</td>
									</tr>
									<input type="hidden" name="last_rekomendasi_sistem-`+last_sistem+`" value="0">
								</tbody>
							</table>
						</td>
					</tr>
					<tr class="detail-sistem-`+last_sistem+`">
						<td style="">Tanggapan Auditee</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<div>
								<select data-width="25%" data-id="2" data-detail="`+last_sistem+`" class="selectpicker col-sm-12 show-tick pilihan" data-size="3" name="fokus_audit[2][detail][`+last_sistem+`][tanggapan]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
									<option data-content="<span class='badge' style='background-color:green;'>MENYETUJUI</span>" value="0" selected>MENYETUJUI</option>
								</select>
							</div>
							<br>
							<textarea class="form-control tanggapan_sistem-`+last_sistem+`" style="display: none;" name="fokus_audit[2][detail][`+last_sistem+`][catatan]" id="exampleFormControlTextarea1" placeholder="Catatan Tanggapan Auditee" rows="2"></textarea>
						</td>
					</tr>
					<tr class="detail-sistem-end-`+last_sistem+` detail-sistem-`+last_sistem+`">
						<td style="">Tanggal Tindak Lanjut</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<input style="width: 25%;" class="form-control tanggal" id="tanggal" name="fokus_audit[2][detail][`+last_sistem+`][tanggal]" placeholder="Tanggal" type="text"/>
						</td>
					</tr>
					<tr class="detail-sistem-end-`+last_sistem+` detail-sistem-`+last_sistem+`" style="border-bottom: 2px solid black;">
						<td style="">Lampiran</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<div class="file-loading">
				            	<input id="img-0-`+last_sistem+`" name="fokus_audit[2][detail][`+last_sistem+`][lampiran]" type="file" class="file form-control lampir" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/jpeg,image/gif,image/png,application/pdf">
				            </div>
						</td>
					</tr>
				`;

				$('.container-sistem').append(html);
		        $('input[name=last_sistem]').val(last_sistem);

		        $('.lampir').fileinput({
				    language: 'es',
				    uploadUrl: "/file-upload-batch/2",
				    previewFileType: "image",
				    showUpload: false,
				    previewFileIcon: '<i class="fas fa-file"></i>',
				    fileActionSettings: {
					    showRemove: true,
					    showUpload: false, //This remove the upload button
					    showZoom: true,
					    showDrag: true
					},
				    initialPreviewAsData: true,
				    purifyHtml: true,
				});

		        var d = new Date();
		    	var full = d.getFullYear();
		    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
		    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
		    	var last = new Date(lastDate);
		    	if($('input[name=tahun]').val() > full){
		    		var min = minDates;
		    	}else{
		    		var min = d;
		    	}

		    	$('.tanggal').datepicker({
		    		orientation: "top left",
		    		todayHighlight: true,
		    		autoclose:true,
		    		format: 'dd-mm-yyyy',
		    		defaultViewDate: {year:$('input[name=tahun]').val(), month:0, day:1},
		    		yearRange: '-0:+1',
		    		startDate: minDates,
		    		endDate: last,
		    		hideIfNoPrevNext: true,
		        });

				$('.selectpicker').selectpicker();

				$('.kategori_sistem').on('change', function(){
					var id = $(this).data("id");
					var data = $('select[name="fokus_audit[2][detail]['+id+'][standarisasi_id]"] option:selected').val();
					$.ajax({
						url: '{{ url('ajax/option/get-kriteria-temuans') }}',
						type: 'POST',
						data: {
							_token: "{{ csrf_token() }}",
							id: data
						},
					})
					.done(function(response) {
						$('.kode_sistem1-'+id).html('');
						$('.kode_sistem1-'+id).html(response[0]);
						$('.kriteria_sistem_'+id).html(response[1]);
					})
					.fail(function() {
						console.log("error");
					});
				});


				$('.pilihan').on('change', function(){
					var id = $(this).data("id");
					var detail = $(this).data("detail");
					var data = $('select[name="fokus_audit['+id+'][detail]['+detail+'][tanggapan]"] option:selected').val();
					if(data == 0){
						if(id == 0){
							$('.tanggapan_operasional-'+detail).hide();
							$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", true);
						}else if(id == 1){
							$('.tanggapan_keuangan-'+detail).hide();
							$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", true);
						}else{
							$('.tanggapan_sistem-'+detail).hide();
							$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", true);
						}
						$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').prop("disabled", false);
					}else{
						if(id == 0){
							$('.tanggapan_operasional-'+detail).show();
							$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", false);
						}else if(id == 1){
							$('.tanggapan_keuangan-'+detail).show();
							$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", false);
						}else{
							$('.tanggapan_sistem-'+detail).show();
							$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", false);
						}
						$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').val('');
						$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').prop("disabled", true);
					}
				});
		});

		$(document).on('click', '.hapus_sistem', function (e){
			var id = $(this).data("id");
			var rowz = $('tr.detail-sistem-'+id).remove();
			var table = $('#table-sistem');
			var rows = table.find('tbody tr.data-sistem');
			$.each(rows, function(key, value){
				table.find('.sistem-numboor-'+$(this).data("id")).html(key+1);
			});
		});

		$(document).on('click', '.tambah_rekomendasi_operasional', function(e){
			var id = $(this).data("operasional");
    		var last_rekomendasi_operasional = parseInt($('input[name=last_rekomendasi_operasional-'+id+']').val()) + 1;
			var row = $('#operasional-rekomendasi-'+id);
			var rowCount = row.find('tbody tr').length;
			var c = rowCount;
			var html = `
					<tr>
						<td style="width: 100%;padding-right: 18px;padding-bottom: 12px;" class="field">
							<textarea class="form-control" name="fokus_audit[0][detail][`+id+`][detail_rekomen][`+last_rekomendasi_operasional+`][rekomendasi]" id="exampleFormControlTextarea1" placeholder="Rekomendasi" rows="2"></textarea>
						</td>
						<td style="width: 5%;">
							<button class="btn btn-sm btn-danger hapus_rekomendasi_operasional" data-toggle="tooltip" data-placement="left" data-operasional="`+id+`" title="Hapus" type="button" style="border-radius: 20px;margin-top: -18px;"><i class="fa fa-close"></i></button>
						</td>
					</tr>
				`;
			$('.container-operasional-rekomendasi-'+id).append(html);
			$('input[name=last_rekomendasi_operasional-'+id+']').val(last_rekomendasi_operasional);
		});

		$(document).on('click', '.hapus_rekomendasi_operasional', function (e){
			var id = $(this).data("id");
			var val = $(this).data("operasional");
			var row = $(this).closest('tr');
			row.remove();
		});

		$(document).on('click', '.tambah_rekomendasi_keuangan', function(e){
			var id = $(this).data("keuangan");
    		var last_rekomendasi_keuangan = parseInt($('input[name=last_rekomendasi_keuangan-'+id+']').val()) + 1;
			var row = $('#keuangan-rekomendasi-'+id);
			var rowCount = row.find('tbody tr').length;
			var c = rowCount;
			var html = `
					<tr>
						<td style="width: 100%;padding-right: 18px;padding-bottom: 12px;" class="field">
							<textarea class="form-control" name="fokus_audit[1][detail][`+id+`][detail_rekomen][`+last_rekomendasi_keuangan+`][rekomendasi]" id="exampleFormControlTextarea1" placeholder="Rekomendasi" rows="2"></textarea>
						</td>
						<td style="width: 5%;">
							<button class="btn btn-sm btn-danger hapus_rekomendasi_keuangan" data-toggle="tooltip" data-placement="left" data-keuangan="`+id+`" title="Hapus" type="button" style="border-radius: 20px;margin-top: -18px;"><i class="fa fa-close"></i></button>
						</td>
					</tr>
				`;
			$('.container-keuangan-rekomendasi-'+id).append(html);
			$('input[name=last_rekomendasi_keuangan-'+id+']').val(last_rekomendasi_keuangan);
		});

		$(document).on('click', '.hapus_rekomendasi_keuangan', function (e){
			var id = $(this).data("id");
			var val = $(this).data("keuangan");
			var row = $(this).closest('tr');
			row.remove();
		});

		$(document).on('click', '.tambah_rekomendasi_sistem', function(e){
			var id = $(this).data("sistem");
    		var last_rekomendasi_sistem = parseInt($('input[name=last_rekomendasi_sistem-'+id+']').val()) + 1;
			var row = $('#sistem-rekomendasi-'+id);
			var rowCount = row.find('tbody tr').length;
			var c = rowCount;
			var html = `
					<tr>
						<td style="width: 100%;padding-right: 18px;padding-bottom: 12px;" class="field">
							<textarea class="form-control" name="fokus_audit[2][detail][`+id+`][detail_rekomen][`+last_rekomendasi_sistem+`][rekomendasi]" id="exampleFormControlTextarea1" placeholder="Rekomendasi" rows="2"></textarea>
						</td>
						<td style="width: 5%;">
							<button class="btn btn-sm btn-danger hapus_rekomendasi_sistem" data-toggle="tooltip" data-placement="left" data-sistem="`+id+`" title="Hapus" type="button" style="border-radius: 20px;margin-top: -18px;"><i class="fa fa-close"></i></button>
						</td>
					</tr>
				`;
			$('.container-sistem-rekomendasi-'+id).append(html);
			$('input[name=last_rekomendasi_sistem-'+id+']').val(last_rekomendasi_sistem);
		});

		$(document).on('click', '.hapus_rekomendasi_sistem', function (e){
			var id = $(this).data("id");
			var val = $(this).data("sistem");
			var row = $(this).closest('tr');
			row.remove();
		});
    </script>
    @yield('js-extra')
@endpush

