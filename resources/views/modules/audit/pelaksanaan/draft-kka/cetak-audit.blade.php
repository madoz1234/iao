<html>
<head>
<style>
        @page {
            margin: 60px 25px;
            counter-reset: page;
        }

       /* Define now the real margins of every page in the PDF */
       body {
           margin-top: 3cm;
           margin-left: -0.5cm;
           margin-right: -0.5cm;
           margin-bottom: 2cm;
           font-style: normal;
       }

       .ui.table.bordered {
         border: solid black 1px;
         border-collapse: collapse;
         width: 100%;
         font-family : "Arial", "Verdana", sans-serif;
       }

       .ui.table.bordered td {
         border: solid black .5px;
         border-collapse: collapse;
         /*padding:10px;*/
         padding:5px;
       }

       .ui.table.bordered td.center.aligned {
         text-align : center;
       }

       .page-break {
            page-break-after: always;
       }

       #watermark {
	        position: fixed;
            /** 
                Set a position in the page for your image
                This should center it vertically
            **/
            top: 2.5cm;
            bottom:   10cm;
            left:     10.5cm;

            /** Change image dimensions**/
            width:    8cm;
            height:   8cm;

            /** Your watermark should be behind every content**/
            z-index:  101;
	    }

       .pad{
       	padding-top: 5px;
       	padding-bottom: 5px;
       }

       /* Define the header rules */
        header {
            position: fixed;
            top: -40px;
            left: 0px;
            right: 0px;
            height: 10px;

            /* Extra personal styles */
            text-align: center;
            /*line-height: 35px;*/

            font-size: 10px;
        }
		
       main {
         position: sticky;
         font-size : 12px;
         padding-top : -1cm;
         margin-top : 2px;
         border : none;
         width: 100%;
       }

       main p {
         margin-left: 5px;
         margin-right: 5px;
       }

       /* Define the footer rules */
       footer {
           position: fixed;
           bottom: -60px;
           left: 0px;
           right: 0px;
           height: 50px;

           /* Extra personal styles */
           text-align: center;
           line-height: 35px;
       }
/*       .page-num:after { 
       	counter-increment: pages; 
       	content: counter(page) " of " counter(pages); 
       }*/
       table.page_content {width: 100%; border: none; padding: 1mm; font-size: 10px;font-family : "Arial", "Verdana", sans-serif;}
       table.page_header {width: 100%; border: none; padding: 2mm; font-size: 10px;font-family : "Arial", "Verdana", sans-serif; text-align: left; margin-left: 50px;margin-top: 50px;}
       table.page_content_header {width: 100%; font-size: 10px;border-collapse: collapse;font-family : "Arial", "Verdana", sans-serif;text-align: center; padding-bottom: 1px;
       	padding-top: 1px;}
       /*.footer .page-number:after {
         content: counter(page);
       }*/
</style>
</head>
<body>
    <!-- Define header and footer blocks before your content -->
{{-- 	<script type="text/php">
	    if (isset($pdf)) {
	        $x = 532;
	        $y = 85;
	        $text = "Halaman {PAGE_NUM} dari {PAGE_COUNT}";
	        $font = null;
	        $size = 7;
	        $color = array(0,0,0);
	        $word_space = 0.0;  //  default
	        $char_space = 0.0;  //  default
	        $angle = 0.0;   //  default
	        $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
	    }
	</script> --}}
	@if(!is_null($records->parent_id))
		@php 
			$max = getmaxparent($records->id);
		@endphp
		@if($max == $records->revisi)
	    @else 
			@if(checkParent($records->parent_id) >= 3)
				<div id="watermark">
			        <img src="{{ asset('src/img/diganti.png') }}" height="40%" width="90%" />
			    </div>
			@endif
	    @endif
	@endif
    <header>
    	<table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;margin-left: 100px;">
    		<tbody>
    			<tr style="">
    				<td style="text-align: left;border: 1px #ffffff;" colspan="2">
    					<table style="">
    						<tbody>
    							<tr>
    								<td style="width: 100px;padding-left: -10px;"><img src="{{ asset('src/img/logo.png') }}" width="85px" height="70px"></td>
    								<td style="width: 600px;text-align: center;"><h2>@if($records->revisi > 0) REVISI @endif PROGRAM KERJA INTERNAL AUDIT TAHUN {{ ($records->tahun) }}</h2><br><h3 style="margin-top:-17px;">PT. WASKITA KARYA (PERSERO) TBK</h3></td>
    								<td style="width: 170px">
    									<table style="text-align: right;font-size: 10px;" class="page_content ui table bordered">
    										<tbody>
    											<tr>
    												<td style="text-align: center;padding-bottom: 10px;">Form Prod 35</td>
    											</tr>
    											<tr>
    												<td style="text-align: center;padding-bottom: 10px;">Edisi : Mei 2019 Revisi : 0</td>
    											</tr>
    										</tbody>
    									</table>
    								</td>
    							</tr>
    						</tbody>
    					</table>
    				</td>
    			</tr>
    		</tbody>
		</table>
  	</header>
	<main style="margin-top: 50px;">
		@if($records->revisi > 0)
		  	<table style="margin-left: 690px;margin-top: -85px;">
		  		<tr>
		  			<td>
					    <span style="color: orange; font-weight: bold; font-style: italic;position: absolute;">Revisi : {{ $records->revisi }}</span>
		  			</td>
		  		</tr>
		  	</table>
		@endif
		<table  border="1" class="page_content ui table bordered" style="margin-top: -80px;page-break-inside: auto;font-size:10px;">
			<tbody>
				<tr>
					<td style="width: 5px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;" rowspan="2">No</td>
					<td style="width: 90px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;" rowspan="2"></td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;" rowspan="2">Lokasi</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;" rowspan="2">NK Total</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;" rowspan="2">SNK {{ ($records->tahun-1) }}</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;" rowspan="2">Progress s.d {{ ($records->tahun-1) }}</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;" rowspan="2">BK/PU</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;" rowspan="2">MAPP</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;" colspan="2">Waktu Pelaksanaan</td>
					<td style="width: 30px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;" colspan="12">Rencana Tahun {{ ($records->tahun) }}</td>
				</tr>
				<tr>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Mulai</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Selesai</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Jan</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Feb</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Mar</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Apr</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Mei</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Jun</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Jul</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Agu</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Sep</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Okt</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Nov</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Des</td>
				</tr>
				<tr>
					<td style="text-align: center;font-size:9.5px;font-weight: bold;">I</td>
					<td style="font-size:9.5px;width: 90px !important;text-align: justify;font-weight: bold;">KEGIATAN AUDIT</td>
					<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
				</tr>
				<tr>
					<td style="text-align: center;font-size:9.5px;font-weight: bold;">A</td>
					<td style="font-size:9.5px;width: 90px !important;text-align: justify;font-weight: bold;">SISA NILAI KONTRAK {{ ($records->tahun-1) }}</td>
					<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
				</tr>
				@foreach(getNamaObjectBU() as $bu)
					<tr>
						<td style="text-align: center;font-size:9px;font-weight: bold;"></td>
						<td style="font-size:9px;width: 90px !important;text-align: justify;font-weight: bold;">{{ $bu->nama }}</td>
						<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					</tr>
					@php 
						$j=1;
					@endphp
					@foreach($records->detailrencana->where('tipe_object', 2)->where('jenis', 1) as $i => $data)
						@if($bu->id == getParentBu($data->object_id))
							@php 
								$pieces = explode(" ", $data->rencana);
							@endphp
							<tr>
								<td style="text-align: center;font-size:9px;">{{ $j }}</td>
								<td style="font-size:9px;width: 90px !important;text-align: justify;">{!! object_audit($data->tipe_object, $data->object_id) !!}</td>
								<td style="font-size:9px;width: 15px !important;text-align: justify;">{{ getlokasi($data->object_id)}}</td>
								<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($data->nilai_kontrak, 0, '.', '.') }}</td>
								<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($data->snk, 0, '.', '.') }}</td>
								<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($data->progress, 0, '.', '.') }}</td>
								<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($data->bkpu, 0, '.', '.') }}</td>
								<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($data->mapp, 0, '.', '.') }}</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;">{{ DateToString($data->tgl_mulai) }}</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;">{{ DateToString($data->tgl_selesai) }}</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Januari') background-color:green; color:white;@endif">@if($pieces[3] == 'Januari') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Februari') background-color:green; color:white;@endif">@if($pieces[3] == 'Februari') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Maret') background-color:green; color:white;@endif">@if($pieces[3] == 'Maret') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'April') background-color:green; color:white;@endif">@if($pieces[3] == 'April') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Mei') background-color:green; color:white;@endif">@if($pieces[3] == 'Mei') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Juni') background-color:green; color:white;@endif">@if($pieces[3] == 'Juni') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Juli') background-color:green; color:white;@endif">@if($pieces[3] == 'Juli') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Agustus') background-color:green; color:white;@endif">@if($pieces[3] == 'Agustus') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'September') background-color:green; color:white;@endif">@if($pieces[3] == 'September') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Oktober') background-color:green; color:white;@endif">@if($pieces[3] == 'Oktober') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'November') background-color:green; color:white;@endif">@if($pieces[3] == 'November') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Desember') background-color:green; color:white;@endif">@if($pieces[3] == 'Desember') {{ $pieces[2] }} @endif</td>
							</tr>
							@php 
							$j++;
							@endphp
						@else 
						@endif
					@endforeach
				@endforeach
				<tr>
					<td style="text-align: center;font-size:9.5px;font-weight: bold;">B</td>
					<td style="font-size:9.5px;width: 90px !important;text-align: justify;font-weight: bold;">NILAI KONTRAK BARU {{ ($records->tahun-1) }}</td>
					<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
				</tr>
				@php 
					$k=1;
				@endphp
				@foreach($records->detailrencana->where('tipe_object', 2)->where('jenis', 0) as $i => $datas)
					@php 
						$piecess = explode(" ", $datas->rencana);
					@endphp
					<tr>
						<td style="text-align: center;font-size:9px;">{{ $k }}</td>
						<td style="font-size:9px;width: 90px !important;text-align: justify;">{!! object_audit($datas->tipe_object, $datas->object_id) !!}</td>
						<td style="font-size:9px;width: 15px !important;text-align: justify;">{{ getlokasi($datas->object_id)}}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($datas->nilai_kontrak, 0, '.', '.') }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($datas->snk, 0, '.', '.') }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($datas->progress, 0, '.', '.') }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($datas->bkpu, 0, '.', '.') }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($datas->mapp, 0, '.', '.') }}</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;">{{ DateToString($datas->tgl_mulai) }}</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;">{{ DateToString($datas->tgl_selesai) }}</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'Januari') background-color:green; color:white;@endif">@if($piecess[3] == 'Januari') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'Februari') background-color:green; color:white;@endif">@if($piecess[3] == 'Februari') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'Maret') background-color:green; color:white;@endif">@if($piecess[3] == 'Maret') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'April') background-color:green; color:white;@endif">@if($piecess[3] == 'April') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'Mei') background-color:green; color:white;@endif">@if($piecess[3] == 'Mei') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'Juni') background-color:green; color:white;@endif">@if($piecess[3] == 'Juni') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'Juli') background-color:green; color:white;@endif">@if($piecess[3] == 'Juli') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'Agustus') background-color:green; color:white;@endif">@if($piecess[3] == 'Agustus') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'September') background-color:green; color:white;@endif">@if($piecess[3] == 'September') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'Oktober') background-color:green; color:white;@endif">@if($piecess[3] == 'Oktober') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'November') background-color:green; color:white;@endif">@if($piecess[3] == 'November') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'Desember') background-color:green; color:white;@endif">@if($piecess[3] == 'Desember') {{ $piecess[2] }} @endif</td>
					</tr>
					@php 
					$k++;
						@endphp
				@endforeach
				<tr>
					<td style="text-align: center;font-size:9.5px;font-weight: bold;">C</td>
					<td style="font-size:9.5px;width: 90px !important;text-align: justify;font-weight: bold;">PROYEK SELESAI</td>
					<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
				</tr>
				<tr>
					<td style="text-align: center;font-size:9.5px;font-weight: bold;">D</td>
					<td style="font-size:9.5px;width: 90px !important;text-align: justify;font-weight: bold;">BUSINESS UNIT (BU)</td>
					<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
				</tr>
				@foreach(getNamaObjectBU() as $keya => $bu)
					@php 
						$rencana1=null;
					@endphp
					@if($records->detailrencana->where('tipe_object', 0)->where('object_id', $bu->id)->first())
					@php 
						$rencana1 = explode(" ", $records->detailrencana->where('tipe_object', 0)->where('object_id', $bu->id)->first()->rencana);
					@endphp
					@endif
					<tr>
						<td style="text-align: center;font-size:9px;font-weight: bold;">{{ $keya+1 }}</td>
						<td style="font-size:9px;width: 90px !important;text-align: justify;font-weight: bold;">{{ $bu->nama }}</td>
						<td style="font-size:9px;width: 15px !important;text-align: justify;">{{ $bu->alamat }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						@if($rencana1)
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'Januari') background-color:green; color:white;@endif">@if($rencana1[3] == 'Januari') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'Februari') background-color:green; color:white;@endif">@if($rencana1[3] == 'Februari') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'Maret') background-color:green; color:white;@endif">@if($rencana1[3] == 'Maret') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'April') background-color:green; color:white;@endif">@if($rencana1[3] == 'April') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'Mei') background-color:green; color:white;@endif">@if($rencana1[3] == 'Mei') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'Juni') background-color:green; color:white;@endif">@if($rencana1[3] == 'Juni') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'Juli') background-color:green; color:white;@endif">@if($rencana1[3] == 'Juli') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'Agustus') background-color:green; color:white;@endif">@if($rencana1[3] == 'Agustus') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'September') background-color:green; color:white;@endif">@if($rencana1[3] == 'September') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'Oktober') background-color:green; color:white;@endif">@if($rencana1[3] == 'Oktober') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'November') background-color:green; color:white;@endif">@if($rencana1[3] == 'November') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'Desember') background-color:green; color:white;@endif">@if($rencana1[3] == 'Desember') {{ $rencana1[2] }} @endif</td>
						@else 
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						@endif
					</tr>
				@endforeach
				<tr>
					<td style="text-align: center;font-size:9.5px;font-weight: bold;">E</td>
					<td style="font-size:9.5px;width: 90px !important;text-align: justify;font-weight: bold;">CORPORATE OFFICE (CO)</td>
					<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
				</tr>
				@foreach(getNamaObjectCO() as $keyb => $co)
					@php 
						$rencana2 = null;
					@endphp
					@if($records->detailrencana->where('tipe_object', 1)->where('object_id', $co->id)->first())
						@php 
							$rencana2 = explode(" ", $records->detailrencana->where('tipe_object', 1)->where('object_id', $co->id)->first()->rencana);
						@endphp
					@endif
					<tr>
						<td style="text-align: center;font-size:9px;font-weight: bold;">{{ $keyb+1 }}</td>
						<td style="font-size:9px;width: 90px !important;text-align: justify;font-weight: bold;">{{ $co->nama }}</td>
						<td style="font-size:9px;width: 15px !important;text-align: justify;">{{ $co->alamat }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						@if($rencana2)
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'Januari') background-color:green; color:white;@endif">@if($rencana2[3] == 'Januari') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'Februari') background-color:green; color:white;@endif">@if($rencana2[3] == 'Februari') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'Maret') background-color:green; color:white;@endif">@if($rencana2[3] == 'Maret') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'April') background-color:green; color:white;@endif">@if($rencana2[3] == 'April') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'Mei') background-color:green; color:white;@endif">@if($rencana2[3] == 'Mei') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'Juni') background-color:green; color:white;@endif">@if($rencana2[3] == 'Juni') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'Juli') background-color:green; color:white;@endif">@if($rencana2[3] == 'Juli') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'Agustus') background-color:green; color:white;@endif">@if($rencana2[3] == 'Agustus') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'September') background-color:green; color:white;@endif">@if($rencana2[3] == 'September') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'Oktober') background-color:green; color:white;@endif">@if($rencana2[3] == 'Oktober') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'November') background-color:green; color:white;@endif">@if($rencana2[3] == 'November') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'Desember') background-color:green; color:white;@endif">@if($rencana2[3] == 'Desember') {{ $rencana2[2] }} @endif</td>
						@else 
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						@endif
					</tr>
				@endforeach
				<tr>
					<td style="text-align: center;font-size:9.5px;font-weight: bold;">F</td>
					<td style="font-size:9.5px;width: 90px !important;text-align: justify;font-weight: bold;">ANAK PERUSAHAAN</td>
					<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
				</tr>
				@foreach(getNamaObjectAP() as $keyc => $ap)
					@php 
						$rencana3 = null;
					@endphp
					@if($records->detailrencana->where('tipe_object', 3)->where('object_id', $ap->id)->first())
						@php 
							$rencana3 = explode(" ", $records->detailrencana->where('tipe_object', 3)->where('object_id', $ap->id)->first()->rencana);
						@endphp
					@endif
					<tr>
						<td style="text-align: center;font-size:9px;font-weight: bold;">{{ $keyc+1 }}</td>
						<td style="font-size:9px;width: 90px !important;text-align: justify;font-weight: bold;">{{ $ap->nama }}</td>
						<td style="font-size:9px;width: 15px !important;text-align: justify;">{{ $ap->alamat }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						@if($rencana3)
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'Januari') background-color:green; color:white;@endif">@if($rencana3[3] == 'Januari') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'Februari') background-color:green; color:white;@endif">@if($rencana3[3] == 'Februari') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'Maret') background-color:green; color:white;@endif">@if($rencana3[3] == 'Maret') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'April') background-color:green; color:white;@endif">@if($rencana3[3] == 'April') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'Mei') background-color:green; color:white;@endif">@if($rencana3[3] == 'Mei') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'Juni') background-color:green; color:white;@endif">@if($rencana3[3] == 'Juni') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'Juli') background-color:green; color:white;@endif">@if($rencana3[3] == 'Juli') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'Agustus') background-color:green; color:white;@endif">@if($rencana3[3] == 'Agustus') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'September') background-color:green; color:white;@endif">@if($rencana3[3] == 'September') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'Oktober') background-color:green; color:white;@endif">@if($rencana3[3] == 'Oktober') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'November') background-color:green; color:white;@endif">@if($rencana3[3] == 'November') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'Desember') background-color:green; color:white;@endif">@if($rencana3[3] == 'Desember') {{ $rencana3[2] }} @endif</td>
						@else 
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						@endif
					</tr>
				@endforeach
				<tr>
					<td style="text-align: center;font-size:9.5px;font-weight: bold;">II</td>
					<td style="font-size:9.5px;width: 90px !important;text-align: justify;font-weight: bold;">KEGIATAN KONSULTASI</td>
					<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
				</tr>
				<tr>
					<td style="text-align: center;font-size:9.5px;font-weight: bold;">III</td>
					<td style="font-size:9.5px;width: 90px !important;text-align: justify;font-weight: bold;">KEGIATAN LAINNYA</td>
					<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
				</tr>
				{{-- @foreach($records->detailrencana as $key => $data)
					@php 
						$pieces = explode(" ", $data->rencana);
					@endphp
					<tr>
						<td style="text-align: center;font-size:9px;">{{$key+1}}</td>
						<td style="font-size:9px;width: 90px !important;text-align: justify;">{!! object_audit($data->tipe_object, $data->object_id) !!}</td>
						<td style="font-size:9px;width: 15px !important;text-justify: inter-word;">{{ getlokasi($data->object_id)}}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($data->nilai_kontrak, 0, '.', '.') }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($data->snk, 0, '.', '.') }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($data->progress, 0, '.', '.') }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($data->bkpu, 0, '.', '.') }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($data->mapp, 0, '.', '.') }}</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;">{{ DateToString($data->tgl_mulai) }}</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;">{{ DateToString($data->tgl_selesai) }}</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Januari') background-color:green; color:white;@endif">@if($pieces[3] == 'Januari') {{ $pieces[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Februari') background-color:green; color:white;@endif">@if($pieces[3] == 'Februari') {{ $pieces[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Maret') background-color:green; color:white;@endif">@if($pieces[3] == 'Maret') {{ $pieces[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'April') background-color:green; color:white;@endif">@if($pieces[3] == 'April') {{ $pieces[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Mei') background-color:green; color:white;@endif">@if($pieces[3] == 'Mei') {{ $pieces[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Juni') background-color:green; color:white;@endif">@if($pieces[3] == 'Juni') {{ $pieces[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Juli') background-color:green; color:white;@endif">@if($pieces[3] == 'Juli') {{ $pieces[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Agustus') background-color:green; color:white;@endif">@if($pieces[3] == 'Agustus') {{ $pieces[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'September') background-color:green; color:white;@endif">@if($pieces[3] == 'September') {{ $pieces[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Oktober') background-color:green; color:white;@endif">@if($pieces[3] == 'Oktober') {{ $pieces[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'November') background-color:green; color:white;@endif">@if($pieces[3] == 'November') {{ $pieces[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Desember') background-color:green; color:white;@endif">@if($pieces[3] == 'Desember') {{ $pieces[2] }} @endif</td>
					</tr>
				@endforeach --}}
			</tbody>
		</table>
		<table class="page_content_header" style="padding-left: -50px;padding-top: 30px;">
            <tr>
                <td style="text-align: left;padding-left: 100px;font-size: 9px;">
                	Keterangan<br>
                    Rencana Minggu Ke : I, II, III, IV<br>
                    Jumlah Auditor : 3 Tim<br>
                </td>
                <td style="text-align: center;font-size: 9px;"width="250px;">
                    <b>President Director</b><br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    (I Gusti Ngurah Putra)
                </td>
                <td style="text-align: center;font-size: 9px;">
                    <b>SVP Internal Audit</b><br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    (Pius Sutrisno Riyanto)
                </td>
            </tr>
        </table>
	</main>
</body>
</html>