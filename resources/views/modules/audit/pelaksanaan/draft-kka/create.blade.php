@extends('layouts.form')
@section('title', 'Buat KKA')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body" style="padding-bottom: 0px;">
		<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
			@csrf
			<div class="loading dimmer padder-v" style="display: none;">
			    <div class="loader"></div>
			</div>
			@php 
				$users = object_users($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
			@endphp
			@if($record->kka)
				<input type="hidden" name="id" value="{{ $record->kka->id }}">
			@endif
			<input type="hidden" name="program_id" value="{{ $record->id }}">
			<input type="hidden" name="tahun" value="{{ $record->penugasanaudit->rencanadetail->rencanaaudit->tahun }}">
			<input type="hidden" name="status" value="0">
			<div class="form-row">
				<div class="form-group col-md-12">
					<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
						<tr>
							<td style="width: 10%;border: 1px #ffffff;">Tipe</td>
							<td style="width: 1%;border: 1px #ffffff;">:</td>
							<td style="text-align:left;border: 1px #ffffff;" class="field">
								@if($record->penugasanaudit->rencanadetail->tipe == 0)
									<span class="label label-primary">Audit</span>
								@elseif($record->penugasanaudit->rencanadetail->tipe == 1)
									<span class="label label-success">Kegiatan Konsultasi</span>
								@else 
									<span class="label label-info">Kegiatan Audit Lain - Lain</span>
								@endif
							</td>
						</tr>
						<tr>
							<td style="width: 10%;border: 1px #ffffff;">Kategori</td>
							<td style="width: 1%;border: 1px #ffffff;">:</td>
							<td style="text-align:left;border: 1px #ffffff;" class="field">
								@if($record->penugasanaudit->rencanadetail->tipe == 0)
									@if($record->penugasanaudit->rencanadetail->tipe_object == 0)
										<span class="label label-success">Business Unit (BU)</span>
									@elseif($record->penugasanaudit->rencanadetail->tipe_object == 1)
										<span class="label label-info">Corporate Office (CO)</span>
									@elseif($record->penugasanaudit->rencanadetail->tipe_object == 2)
										<span class="label label-default">Project</span>
									@else 
										<span class="label label-primary">Anak Perusahaan</span>
									@endif
								@elseif($record->penugasanaudit->rencanadetail->tipe == 1)
									<span class="label label-warning">{{ $record->penugasanaudit->rencanadetail->konsultasi->nama }}</span>
								@else 
									<span class="label label-default">{{ $record->penugasanaudit->rencanadetail->lain->nama }}</span>
								@endif
							</td>
						</tr>
						<tr>
							<td style="width: 10%;border: 1px #ffffff;">Objek Audit</td>
							<td style="width: 1%;border: 1px #ffffff;">:</td>
							<td style="text-align: left;border: 1px #ffffff;" class="field">

								{!! object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id)!!}
							</td>
						</tr>
						<tr>
							<td style="width: 10%;border: 1px #ffffff;">Tanggal Audit</td>
							<td style="width: 1%;border: 1px #ffffff;">:</td>
							<td style="text-align: left;border: 1px #ffffff;" class="field">
								 {{ DateToStringWday($record->detailpelaksanaan->min('tgl')) }}  -  {{DateToStringWday($record->detailpelaksanaan->max('tgl'))}}
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="form-row">
					<div class="form-group col-md-12">
						<div class="tab-content">
							@if($user == 1)
								@php 
									$cek1 = count($record->detailkerja->where('bidang', 1)->where('fokus_audit_id', 1000));
								@endphp
								<input type="hidden" name="tipe" value="1">
								<table id="table-operasional" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
									@if($cek1 > 0)
										<tbody>
											<tr class="detail-operasional-0" data-id="0">
												<td style="text-align: left;font-weight: bold;" colspan="5">Bidang Audit : Operasional</td>
											</tr>
											<tr class="detail-operasional-0" data-id="0">
												<td style="text-align: left;font-weight: bold;" colspan="5">NONE</td>
												<input type="hidden" name="kka" value="0">
											</tr>
										</tbody>
									@else
										<tbody class="container-operasional">
											<tr class="detail-operasional-0" data-id="0">
												<td style="text-align: left;font-weight: bold;" colspan="5">Bidang Audit : Operasional</td>
											</tr>
											<input type="hidden" name="fokus_audit[0][detail][0][tipe]" value="0">
											<input type="hidden" name="fokus_audit[0][detail][0][user_id]" value="{{$users}}">
											<input type="hidden" name="kka" value="1">
											<tr class="data-operasional detail-operasional-0">
												<td style="text-align: center;width: 20px;">1</td>
												<td style="width: 150px;">Judul</td>
												<td style="width: 2px;">:</td>
												<td style="width: 150px;text-align: center;padding-top: 15px;" class="kode_operasional1-0"></td>
												<td style="width: 1600px;" class="field">
													<div>
														<select class="selectpicker form-control kategori_operasional" name="fokus_audit[0][detail][0][standarisasi_id]" data-id="0" data-style="btn-default" data-live-search="true">
															<option value="">(Pilih Salah Satu)</option>
															@foreach(App\Models\Master\StandarisasiTemuan::where('bidang', 1)->get() as $standarisasi)
																<option value="{{ $standarisasi->id }}">{{ $standarisasi->deskripsi }}</option>
															@endforeach
														</select>
													</div>
												</td>
												<td style="width: 50px;">
													<button class="btn btn-sm btn-success tambah_operasional" data-toggle="tooltip" data-placement="left" title="Tambah" type="button" style="border-radius: 20px;"><i class="fa fa-plus"></i></button>
												</td>
											</tr>
											<tr class="detail-operasional-0">
												<td style="" rowspan="9"></td>
												<td style="">Kondisi</td>
												<td style="">:</td>
												<td style="" colspan="2"  class="field">
													<textarea class="form-control" name="fokus_audit[0][detail][0][catatan_kondisi]" id="exampleFormControlTextarea1" placeholder="Kondisi" rows="2"></textarea>
												</td>
												<td style="" rowspan="9"></td>
											</tr>
											<tr class="detail-operasional-0">
												<td style="">Kriteria</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field" colspan="2">
													<ol class="kriteria_operasional_0">
													</ol>
												</td>
											</tr>
											<tr class="detail-operasional-0">
												<td style="">Sebab</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<textarea class="form-control" name="fokus_audit[0][detail][0][sebab]" id="exampleFormControlTextarea1" placeholder="Sebab" rows="2"></textarea>
												</td>
											</tr>
											<tr class="detail-operasional-0">
												<td style="">Risiko</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<textarea class="form-control" name="fokus_audit[0][detail][0][risiko]" id="exampleFormControlTextarea1" placeholder="Risiko" rows="2"></textarea>
												</td>
											</tr>
											<tr class="detail-operasional-0">
												<td style="">Kategori Temuan</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
													<div class="field">
														<select class="selectpicker form-control" data-width="310px" name="fokus_audit[0][detail][0][kategori_id]" data-style="btn-default" data-live-search="true">
															<option value="">(Pilih Salah Satu)</option>
															@foreach(App\Models\Master\KategoriTemuan::get() as $kategori)
																<option value="{{ $kategori->id }}">{{ $kategori->nama }}</option>
															@endforeach
														</select>
													</div>
												</td>
											</tr>
											<tr class="detail-operasional-0">
												<td style="">Rekomendasi</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
													<table id="operasional-rekomendasi-0" style="width: 100%;">
														<tbody class="container-operasional-rekomendasi-0">
															<tr>
																<td style="width: 100%;padding-right: 18px;padding-bottom: 12px;" class="field">
																	<textarea class="form-control" name="fokus_audit[0][detail][0][detail_rekomen][0][rekomendasi]" id="exampleFormControlTextarea1" placeholder="Rekomendasi" rows="2"></textarea>
																</td>
																<td style="width: 5%;">
																	<button class="btn btn-sm btn-success tambah_rekomendasi_operasional" data-toggle="tooltip" data-placement="left" title="Tambah" type="button" data-operasional="0" style="border-radius: 20px;margin-top: -18px;"><i class="fa fa-plus"></i></button>
																</td>
															</tr>
															<input type="hidden" name="last_rekomendasi_operasional-0" value="0">
														</tbody>
													</table>
												</td>
											</tr>
											<tr class="detail-operasional-0">
												<td style="">Tanggapan Auditee</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<div>
														<select data-width="25%" data-id="0" data-detail="0" class="selectpicker col-sm-12 show-tick pilihan" data-size="3" name="fokus_audit[0][detail][0][tanggapan]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
															<option data-content="<span class='badge' style='background-color:green;'>MENYETUJUI</span>" value="0" selected>MENYETUJUI</option>
														</select>
													</div>
													<br>
													<textarea class="form-control tanggapan_operasional-0" style="display: none;" name="fokus_audit[0][detail][0][catatan]" id="exampleFormControlTextarea1" placeholder="Catatan Tanggapan Auditee" rows="2"></textarea>
												</td>
											</tr>
											<tr class="detail-operasional-end-0 detail-operasional-0">
												<td style="">Tanggal Tindak Lanjut</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<input style="width: 25%;" class="form-control tanggal" id="tanggal" name="fokus_audit[0][detail][0][tanggal]" placeholder="Tanggal" type="text"/>
												</td>
											</tr>
											<tr class="detail-operasional-end-0 detail-operasional-0" style="border-bottom: 2px solid black;">
												<td style="">Lampiran</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<div class="file-loading">
										            	<input id="img-0-0" name="fokus_audit[0][detail][0][lampiran]" type="file" class="file form-control lampir" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/jpeg,image/gif,image/png,application/pdf">
										            </div>
												</td>
											</tr>
											<input type="hidden" name="last_operasional" value="0">
										</tbody>
									@endif
								</table>
								<div class="form-group col-md-12">
									<div class="text-right">
										<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
										<button type="button" class="btn btn-primary save as drafting">Save As Draft</button>
										<button type="button" class="btn btn-simpan save as page">Submit</button>
									</div>
								</div>
							@elseif($user == 2)
								@php 
									$cek2 = count($record->detailkerja->where('bidang', 2)->where('fokus_audit_id', 1000));
								@endphp
								<input type="hidden" name="tipe" value="2">
								<table id="table-keuangan" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
									@if($cek2 > 0)
										<tbody>
											<tr class="detail-operasional-0" data-id="0">
												<td style="text-align: left;font-weight: bold;" colspan="5">Bidang Audit : Keuangan</td>
											</tr>
											<tr class="detail-operasional-0" data-id="0">
												<td style="text-align: left;font-weight: bold;" colspan="5">NONE</td>
												<input type="hidden" name="kka" value="0">
											</tr>
										</tbody>
									@else
										<tbody class="container-keuangan">
											<tr class="detail-keuangan-0" data-id="0">
												<td style="text-align: left;font-weight: bold;" colspan="5">Bidang Audit : Keuangan</td>
											</tr>
											<input type="hidden" name="fokus_audit[1][detail][0][tipe]" value="1">
											<input type="hidden" name="fokus_audit[1][detail][0][user_id]" value="{{$users}}">
											<input type="hidden" name="kka" value="1">
											<tr class="data-keuangan detail-keuangan-0">
												<td style="text-align: center;width: 20px;">1</td>
												<td style="width: 150px;">Judul</td>
												<td style="width: 2px;">:</td>
												<td style="width: 150px;text-align: center;padding-top: 15px;" class="kode_keuangan1-0"></td>
												<td style="width: 1600px;" class="field">
													<div>
														<select class="selectpicker form-control kategori_keuangan" name="fokus_audit[1][detail][0][standarisasi_id]" data-id="0" data-style="btn-default" data-live-search="true">
															<option value="">(Pilih Salah Satu)</option>
															@foreach(App\Models\Master\StandarisasiTemuan::where('bidang', 2)->get() as $standarisasi)
																<option value="{{ $standarisasi->id }}">{{ $standarisasi->deskripsi }}</option>
															@endforeach
														</select>
													</div>
												</td>
												<td style="width: 50px;">
													<button class="btn btn-sm btn-success tambah_keuangan" data-toggle="tooltip" data-placement="left" title="Tambah" type="button" style="border-radius: 20px;"><i class="fa fa-plus"></i></button>
												</td>
											</tr>
											<tr class="detail-keuangan-0">
												<td style="" rowspan="9"></td>
												<td style="">Kondisi</td>
												<td style="">:</td>
												<td style="" colspan="2"  class="field">
													<textarea class="form-control" name="fokus_audit[1][detail][0][catatan_kondisi]" id="exampleFormControlTextarea1" placeholder="Kondisi" rows="2"></textarea>
												</td>
												<td style="" rowspan="9"></td>
											</tr>
											<tr class="detail-keuangan-0">
												<td style="">Kriteria</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field" colspan="2">
													<ol class="kriteria_keuangan_0">
													</ol>
												</td>
											</tr>
											<tr class="detail-keuangan-0">
												<td style="">Sebab</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<textarea class="form-control" name="fokus_audit[1][detail][0][sebab]" id="exampleFormControlTextarea1" placeholder="Sebab" rows="2"></textarea>
												</td>
											</tr>
											<tr class="detail-keuangan-0">
												<td style="">Risiko</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<textarea class="form-control" name="fokus_audit[1][detail][0][risiko]" id="exampleFormControlTextarea1" placeholder="Risiko" rows="2"></textarea>
												</td>
											</tr>
											<tr class="detail-keuangan-0">
												<td style="">Kategori Temuan</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
													<div class="field">
														<select class="selectpicker form-control" data-width="310px" name="fokus_audit[1][detail][0][kategori_id]" data-style="btn-default" data-live-search="true">
															<option value="">(Pilih Salah Satu)</option>
															@foreach(App\Models\Master\KategoriTemuan::get() as $kategori)
																<option value="{{ $kategori->id }}">{{ $kategori->nama }}</option>
															@endforeach
														</select>
													</div>
												</td>
											</tr>
											<tr class="detail-keuangan-0">
												<td style="">Rekomendasi</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
													<table id="keuangan-rekomendasi-0" style="width: 100%;">
														<tbody class="container-keuangan-rekomendasi-0">
															<tr>
																<td style="width: 100%;padding-right: 18px;padding-bottom: 12px;" class="field">
																	<textarea class="form-control" name="fokus_audit[1][detail][0][detail_rekomen][0][rekomendasi]" id="exampleFormControlTextarea1" placeholder="Rekomendasi" rows="2"></textarea>
																</td>
																<td style="width: 5%;">
																	<button class="btn btn-sm btn-success tambah_rekomendasi_keuangan" data-toggle="tooltip" data-placement="left" title="Tambah" type="button" data-keuangan="0" style="border-radius: 20px;margin-top: -18px;"><i class="fa fa-plus"></i></button>
																</td>
															</tr>
															<input type="hidden" name="last_rekomendasi_keuangan-0" value="0">
														</tbody>
													</table>
												</td>
											</tr>
											<tr class="detail-keuangan-0">
												<td style="">Tanggapan Auditee</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<div>
														<select data-width="25%" data-id="1" data-detail="0" class="selectpicker col-sm-12 show-tick pilihan" data-size="3" name="fokus_audit[1][detail][0][tanggapan]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
															<option data-content="<span class='badge' style='background-color:green;'>MENYETUJUI</span>" value="0" selected>MENYETUJUI</option>
														</select>
													</div>
													<br>
													<textarea class="form-control tanggapan_keuangan-0" style="display: none;" name="fokus_audit[1][detail][0][catatan]" id="exampleFormControlTextarea1" placeholder="Catatan Tanggapan Auditee" rows="2"></textarea>
												</td>
											</tr>
											<tr class="detail-keuangan-end-0 detail-keuangan-0">
												<td style="">Tanggal Tindak Lanjut</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<input style="width: 25%;" class="form-control tanggal" id="tanggal" name="fokus_audit[1][detail][0][tanggal]" placeholder="Tanggal" type="text"/>
												</td>
											</tr>
											<tr class="detail-keuangan-end-0 detail-keuangan-0" style="border-bottom: 2px solid black;">
												<td style="">Lampirans</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<div class="file-loading">
										            	<input id="img-0-0" name="fokus_audit[1][detail][0][lampiran]" type="file" class="file form-control lampir" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/jpeg,image/gif,image/png,application/pdf">
										            </div>
												</td>
											</tr>
											<input type="hidden" name="last_keuangan" value="0">
										</tbody>
									@endif
								</table>
								<div class="form-group col-md-12">
									<div class="text-right">
										<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
										<button type="button" class="btn btn-primary save as drafting">Save As Draft</button>
										<button type="button" class="btn btn-simpan save as page">Submit</button>
									</div>
								</div>
							@else
								@php 
									$cek3 = count($record->detailkerja->where('bidang', 3)->where('fokus_audit_id', 1000));
								@endphp
								<input type="hidden" name="tipe" value="3">
								<table id="table-sistem" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
									@if($cek3 > 0)
										<tbody>
											<tr class="detail-operasional-0" data-id="0">
												<td style="text-align: left;font-weight: bold;" colspan="5">Bidang Audit : Sistem</td>
											</tr>
											<tr class="detail-operasional-0" data-id="0">
												<td style="text-align: left;font-weight: bold;" colspan="5">NONE</td>
												<input type="hidden" name="kka" value="0">
											</tr>
										</tbody>
									@else
										<tbody class="container-sistem">
											<tr class="detail-sistem-0" data-id="0">
												<td style="text-align: left;font-weight: bold;" colspan="5">Bidang Audit : Sistem</td>
											</tr>
											<input type="hidden" name="fokus_audit[2][detail][0][tipe]" value="2">
											<input type="hidden" name="fokus_audit[2][detail][0][user_id]" value="{{$users}}">
											<input type="hidden" name="kka" value="1">
											<tr class="data-sistem detail-sistem-0">
												<td style="text-align: center;width: 20px;">1</td>
												<td style="width: 150px;">Judul</td>
												<td style="width: 2px;">:</td>
												<td style="width: 150px;text-align: center;padding-top: 15px;" class="kode_sistem1-0"></td>
												<td style="width: 1600px;" class="field">
													<div>
														<select class="selectpicker form-control kategori_sistem" name="fokus_audit[2][detail][0][standarisasi_id]" data-id="0" data-style="btn-default" data-live-search="true">
															<option value="">(Pilih Salah Satu)</option>
															@foreach(App\Models\Master\StandarisasiTemuan::where('bidang', 3)->get() as $standarisasi)
																<option value="{{ $standarisasi->id }}">{{ $standarisasi->deskripsi }}</option>
															@endforeach
														</select>
													</div>
												</td>
												<td style="width: 50px;">
													<button class="btn btn-sm btn-success tambah_sistem" data-toggle="tooltip" data-placement="left" title="Tambah" type="button" style="border-radius: 20px;"><i class="fa fa-plus"></i></button>
												</td>
											</tr>
											<tr class="detail-sistem-0">
												<td style="" rowspan="9"></td>
												<td style="">Kondisi</td>
												<td style="">:</td>
												<td style="" colspan="2"  class="field">
													<textarea class="form-control" name="fokus_audit[2][detail][0][catatan_kondisi]" id="exampleFormControlTextarea1" placeholder="Kondisi" rows="2"></textarea>
												</td>
												<td style="" rowspan="9"></td>
											</tr>
											<tr class="detail-sistem-0">
												<td style="">Kriteria</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field" colspan="2">
													<ol class="kriteria_sistem_0">
													</ol>
												</td>
											</tr>
											<tr class="detail-sistem-0">
												<td style="">Sebab</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<textarea class="form-control" name="fokus_audit[2][detail][0][sebab]" id="exampleFormControlTextarea1" placeholder="Sebab" rows="2"></textarea>
												</td>
											</tr>
											<tr class="detail-sistem-0">
												<td style="">Risiko</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<textarea class="form-control" name="fokus_audit[2][detail][0][risiko]" id="exampleFormControlTextarea1" placeholder="Risiko" rows="2"></textarea>
												</td>
											</tr>
											<tr class="detail-sistem-0">
												<td style="">Kategori Temuan</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
													<div class="field">
														<select class="selectpicker form-control" data-width="310px" name="fokus_audit[2][detail][0][kategori_id]" data-style="btn-default" data-live-search="true">
															<option value="">(Pilih Salah Satu)</option>
															@foreach(App\Models\Master\KategoriTemuan::get() as $kategori)
																<option value="{{ $kategori->id }}">{{ $kategori->nama }}</option>
															@endforeach
														</select>
													</div>
												</td>
											</tr>
											<tr class="detail-sistem-0">
												<td style="">Rekomendasi</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
													<table id="sistem-rekomendasi-0" style="width: 100%;">
														<tbody class="container-sistem-rekomendasi-0">
															<tr>
																<td style="width: 100%;padding-right: 18px;padding-bottom: 12px;" class="field">
																	<textarea class="form-control" name="fokus_audit[2][detail][0][detail_rekomen][0][rekomendasi]" id="exampleFormControlTextarea1" placeholder="Rekomendasi" rows="2"></textarea>
																</td>
																<td style="width: 5%;">
																	<button class="btn btn-sm btn-success tambah_rekomendasi_sistem" data-toggle="tooltip" data-placement="left" title="Tambah" type="button" data-sistem="0" style="border-radius: 20px;margin-top: -18px;"><i class="fa fa-plus"></i></button>
																</td>
															</tr>
															<input type="hidden" name="last_rekomendasi_sistem-0" value="0">
														</tbody>
													</table>
												</td>
											</tr>
											<tr class="detail-sistem-0">
												<td style="">Tanggapan Auditee</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<div>
														<select data-width="25%" data-id="2" data-detail="0" class="selectpicker col-sm-12 show-tick pilihan" data-size="3" name="fokus_audit[2][detail][0][tanggapan]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
															<option data-content="<span class='badge' style='background-color:green;'>MENYETUJUI</span>" value="0" selected>MENYETUJUI</option>
														</select>
													</div>
													<br>
													<textarea class="form-control tanggapan_sistem-0" style="display: none;" name="fokus_audit[2][detail][0][catatan]" id="exampleFormControlTextarea1" placeholder="Catatan Tanggapan Auditee" rows="2"></textarea>
												</td>
											</tr>
											<tr class="detail-sistem-end-0 detail-sistem-0">
												<td style="">Tanggal Tindak Lanjut</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<input style="width: 25%;" class="form-control tanggal" id="tanggal" name="fokus_audit[2][detail][0][tanggal]" placeholder="Tanggal" type="text"/>
												</td>
											</tr>
											<tr class="detail-sistem-end-0 detail-sistem-0" style="border-bottom: 2px solid black;">
												<td style="">Lampirans</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<div class="file-loading">
										            	<input id="img-0-0" name="fokus_audit[2][detail][0][lampiran]" type="file" class="file form-control lampir" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/jpeg,image/gif,image/png,application/pdf">
										            </div>
												</td>
											</tr>
											<input type="hidden" name="last_sistem" value="0">
										</tbody>
									@endif
								</table>
								<div class="form-group col-md-12">
									<div class="text-right">
										<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
										<button type="button" class="btn btn-primary save as drafting">Save As Draft</button>
										<button type="button" class="btn btn-simpan save as page">Submit</button>
									</div>
								</div>
							@endif
						</div>
					</div>
				</div>
		</form>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
        .table-responsive{
            overflow-x: auto;
            overflow-y: visible !important;
        }

        .table-responsive .dropdown-menu {
	        position: static !important;
	    }

	    div.dropdown-menu.open{
		  max-width: 300px !important;
		  overflow: hidden;
		}
		ul.dropdown-menu.inner{
		  max-width: 300px !important;
		  overflow-y: auto;
		}
		.datepicker.dropdown-menu {
		    z-index: 9999 !important;
		}
    </style>
@endpush

@push('scripts')
    <script>
    	$('.lampir').fileinput({
		    language: 'es',
		    uploadUrl: "/file-upload-batch/2",
		    previewFileType: "image",
		    showUpload: false,
		    previewFileIcon: '<i class="fas fa-file"></i>',
		    fileActionSettings: {
			    showRemove: true,
			    showUpload: false, //This remove the upload button
			    showZoom: true,
			    showDrag: true
			},
		    initialPreviewAsData: true,
		    purifyHtml: true,
	        // // allowed image size up to 5 MB
	        // maxFileSize: 2000,
		});

    	$(document).on('click', '.page1', function (e){
    		$('#a-tab').removeClass('active');
    		$('#a-tab').closest('li').removeClass('active');
    		$('#b-tab').addClass('active');
    		$('#b-tab').closest('li').addClass('active');
    		$('#a').removeClass('active');
    		$('#b').addClass('active');
		});

		$(document).on('click', '.kembali1', function (e){
    		$('#a-tab').addClass('active');
    		$('#a-tab').closest('li').addClass('active');
    		$('#b-tab').removeClass('active');
    		$('#b-tab').closest('li').removeClass('active');
    		$('#a').addClass('active');
    		$('#b').removeClass('active');
		});

		$(document).on('click', '.page2', function (e){
    		$('#b-tab').removeClass('active');
    		$('#b-tab').closest('li').removeClass('active');
    		$('#c-tab').addClass('active');
    		$('#c-tab').closest('li').addClass('active');
    		$('#b').removeClass('active');
    		$('#c').addClass('active');
		});

		$(document).on('click', '.kembali2', function (e){
    		$('#b-tab').addClass('active');
    		$('#b-tab').closest('li').addClass('active');
    		$('#c-tab').removeClass('active');
    		$('#c-tab').closest('li').removeClass('active');
    		$('#b').addClass('active');
    		$('#c').removeClass('active');
		});

    	var d = new Date();
    	var full = d.getFullYear();
    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
    	var last = new Date(lastDate);
    	if($('input[name=tahun]').val() > full){
    		var min = minDates;
    	}else{
    		var min = d;
    	}

    	$('.tanggal').datepicker({
    		orientation: "top left",
    		todayHighlight: true,
    		autoclose:true,
    		format: 'dd-mm-yyyy',
    		defaultViewDate: {year:$('input[name=tahun]').val(), month:0, day:1},
    		yearRange: '-0:+1',
    		startDate: minDates,
    		endDate: last,
    		hideIfNoPrevNext: true,
        });

		$('.pilihan').on('change', function(){
			var id = $(this).data("id");
			var detail = $(this).data("detail");
			var data = $('select[name="fokus_audit['+id+'][detail]['+detail+'][tanggapan]"] option:selected').val();
			if(data == 0){
				if(id == 0){
					$('.tanggapan_operasional-'+detail).hide();
					$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", true);
				}else if(id == 1){
					$('.tanggapan_keuangan-'+detail).hide();
					$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", true);
				}else{
					$('.tanggapan_sistem-'+detail).hide();
					$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", true);
				}
				$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').prop("disabled", false);
			}else{
				if(id == 0){
					$('.tanggapan_operasional-'+detail).show();
					$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", false);
				}else if(id == 1){
					$('.tanggapan_keuangan-'+detail).show();
					$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", false);
				}else{
					$('.tanggapan_sistem-'+detail).show();
					$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", false);
				}
				$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').val('');
				$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').prop("disabled", true);
			}
		});

		$('.kategori_operasional').on('change', function(){
			var id = $(this).data("id");
			var data = $('select[name="fokus_audit[0][detail]['+id+'][standarisasi_id]"] option:selected').val();
			$.ajax({
				url: '{{ url('ajax/option/get-kriteria-temuans') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					id: data
				},
			})
			.done(function(response) {
				console.log(response[1])
				$('.kode_operasional1-'+id).html('');
				$('.kode_operasional1-'+id).html(response[0]);
				$('.kriteria_operasional_'+id).html(response[1]);
			})
			.fail(function() {
				console.log("error");
			});
		});

		$('.kategori_keuangan').on('change', function(){
			var id = $(this).data("id");
			var data = $('select[name="fokus_audit[1][detail]['+id+'][standarisasi_id]"] option:selected').val();
			$.ajax({
				url: '{{ url('ajax/option/get-kriteria-temuans') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					id: data
				},
			})
			.done(function(response) {
				$('.kode_keuangan1-'+id).html('');
				$('.kode_keuangan1-'+id).html(response[0]);
				$('.kriteria_keuangan_'+id).html(response[1]);
			})
			.fail(function() {
				console.log("error");
			});
		});

		$('.kategori_sistem').on('change', function(){
			var id = $(this).data("id");
			var data = $('select[name="fokus_audit[2][detail]['+id+'][standarisasi_id]"] option:selected').val();
			$.ajax({
				url: '{{ url('ajax/option/get-kriteria-temuans') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					id: data
				},
			})
			.done(function(response) {
				$('.kode_sistem1-'+id).html('');
				$('.kode_sistem1-'+id).html(response[0]);
				$('.kriteria_sistem_'+id).html(response[1]);
			})
			.fail(function() {
				console.log("error");
			});
		});

    	$('#myTab li:first-child a').tab('show')
    	//OPERASIONAL
    	$(document).on('click', '.tambah_operasional', function(e){
    		var last_operasional = parseInt($('input[name=last_operasional]').val()) + 1;
			var rowCount = $('#table-operasional > tbody > tr.data-operasional').length;
			var c = rowCount;
			var html = `
					<tr class="data-operasional detail-operasional-`+last_operasional+`" data-id=`+last_operasional+`>
						<input type="hidden" name="fokus_audit[0][detail][`+last_operasional+`][user_id]" value="{{$users}}">
						<input type="hidden" name="fokus_audit[0][detail][`+last_operasional+`][tipe]" value="0">
						<td style="text-align: center;width: 20px;">
							<label style="margin-top: 7px;" class="operasional-numboor-`+last_operasional+`">`+(c+1)+`</label>
						</td>
						<td style="width: 150px;">Judul</td>
						<td style="width: 2px;">:</td>
						<td style="width: 150px;text-align: center;padding-top: 15px;" class="kode_operasional1-`+last_operasional+`"></td>
						<td style="width: 1600px;" class="field">
							<div>
								<select class="selectpicker form-control kategori_operasional" name="fokus_audit[0][detail][`+last_operasional+`][standarisasi_id]" data-id="`+last_operasional+`" data-style="btn-default" data-live-search="true">
									<option value="">(Pilih Salah Satu)</option>
									@foreach(App\Models\Master\StandarisasiTemuan::where('bidang', 1)->get() as $standarisasi)
										<option value="{{ $standarisasi->id }}">{{ $standarisasi->deskripsi }}</option>
									@endforeach
								</select>
							</div>
						</td>
						<td style="width: 50px;">
							<button class="btn btn-sm btn-danger hapus_operasional" data-toggle="tooltip" data-placement="left" title="Hapus" type="button" style="border-radius: 20px;" data-id="`+last_operasional+`"><i class="fa fa-close"></i></button>
						</td>
					</tr>
					<tr class="detail-operasional-`+last_operasional+`">
						<td style="" rowspan="9"></td>
						<td style="">Kondisi</td>
						<td style="">:</td>
						<td style="" colspan="2"  class="field">
							<textarea class="form-control" name="fokus_audit[0][detail][`+last_operasional+`][catatan_kondisi]" id="exampleFormControlTextarea1" placeholder="Catatan Kondisi" rows="2"></textarea>
						</td>
						<td style="" rowspan="9"></td>
					</tr>
					<tr class="detail-operasional-`+last_operasional+`">
						<td style="">Kriteria</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field" colspan="2">
							<ol class="kriteria_operasional_`+last_operasional+`">
							</ol>
						</td>
					</tr>
					<tr class="detail-operasional-`+last_operasional+`">
						<td style="">Sebab</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<textarea class="form-control" name="fokus_audit[0][detail][`+last_operasional+`][sebab]" id="exampleFormControlTextarea1" placeholder="Sebab" rows="2"></textarea>
						</td>
					</tr>
					<tr class="detail-operasional-`+last_operasional+`">
						<td style="">Risiko</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<textarea class="form-control" name="fokus_audit[0][detail][`+last_operasional+`][risiko]" id="exampleFormControlTextarea1" placeholder="Risiko" rows="2"></textarea>
						</td>
					</tr>
					<tr class="detail-operasional-`+last_operasional+`">
						<td style="">Kategori Temuan</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
							<div class="field">
								<select class="selectpicker form-control" data-width="310px" name="fokus_audit[0][detail][`+last_operasional+`][kategori_id]" data-style="btn-default" data-live-search="true">
									<option value="">(Pilih Salah Satu)</option>
									@foreach(App\Models\Master\KategoriTemuan::get() as $kategori)
										<option value="{{ $kategori->id }}">{{ $kategori->nama }}</option>
									@endforeach
								</select>
							</div>
						</td>
					</tr>
					<tr class="detail-operasional-`+last_operasional+`">
						<td style="">Rekomendasi</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
							<table id="operasional-rekomendasi-`+last_operasional+`" style="width: 100%;">
								<tbody class="container-operasional-rekomendasi-`+last_operasional+`">
									<tr>
										<td style="width: 100%;padding-right: 18px;padding-bottom: 12px;" class="field">
											<textarea class="form-control" name="fokus_audit[0][detail][`+last_operasional+`][detail_rekomen][0][rekomendasi]" id="exampleFormControlTextarea1" placeholder="Rekomendasi" rows="2"></textarea>
										</td>
										<td style="width: 5%;">
											<button class="btn btn-sm btn-success tambah_rekomendasi_operasional" data-toggle="tooltip" data-placement="left" title="Tambah" type="button" data-operasional="`+last_operasional+`" style="border-radius: 20px;margin-top: -18px;"><i class="fa fa-plus"></i></button>
										</td>
									</tr>
									<input type="hidden" name="last_rekomendasi_operasional-`+last_operasional+`" value="0">
								</tbody>
							</table>
						</td>
					</tr>
					<tr class="detail-operasional-`+last_operasional+`">
						<td style="">Tanggapan Auditee</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<div>
								<select data-width="25%" data-id="0" data-detail="`+last_operasional+`" class="selectpicker col-sm-12 show-tick pilihan" data-size="3" name="fokus_audit[0][detail][`+last_operasional+`][tanggapan]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
									<option data-content="<span class='badge' style='background-color:green;'>MENYETUJUI</span>" value="0" selected>MENYETUJUI</option>
								</select>
							</div>
							<br>
							<textarea class="form-control tanggapan_operasional-`+last_operasional+`" style="display: none;" name="fokus_audit[0][detail][`+last_operasional+`][catatan]" id="exampleFormControlTextarea1" placeholder="Catatan Tanggapan Auditee" rows="2"></textarea>
						</td>
					</tr>
					<tr class="detail-operasional-end-`+last_operasional+` detail-operasional-`+last_operasional+`">
						<td style="">Tanggal Tindak Lanjut</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<input style="width: 25%;" class="form-control tanggal" id="tanggal" name="fokus_audit[0][detail][`+last_operasional+`][tanggal]" placeholder="Tanggal" type="text"/>
						</td>
					</tr>
					<tr class="detail-operasional-end-`+last_operasional+` detail-operasional-`+last_operasional+`" style="border-bottom: 2px solid black;">
						<td style="">Lampiran</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<div class="file-loading">
				            	<input id="img-0-`+last_operasional+`" name="fokus_audit[0][detail][`+last_operasional+`][lampiran]" type="file" class="file form-control lampir" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/jpeg,image/gif,image/png,application/pdf">
				            </div>
						</td>
					</tr>
				`;

				$('.container-operasional').append(html);
		        $('input[name=last_operasional]').val(last_operasional);

		        $('.lampir').fileinput({
				    language: 'es',
				    uploadUrl: "/file-upload-batch/2",
				    previewFileType: "image",
				    showUpload: false,
				    previewFileIcon: '<i class="fas fa-file"></i>',
				    fileActionSettings: {
					    showRemove: true,
					    showUpload: false, //This remove the upload button
					    showZoom: true,
					    showDrag: true
					},
				    initialPreviewAsData: true,
				    purifyHtml: true,
			        // // allowed image size up to 5 MB
			        // maxFileSize: 2000,
				});

		        var d = new Date();
		    	var full = d.getFullYear();
		    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
		    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
		    	var last = new Date(lastDate);
		    	if($('input[name=tahun]').val() > full){
		    		var min = minDates;
		    	}else{
		    		var min = d;
		    	}

		    	$('.tanggal').datepicker({
		    		orientation: "top left",
		    		todayHighlight: true,
		    		autoclose:true,
		    		format: 'dd-mm-yyyy',
		    		defaultViewDate: {year:$('input[name=tahun]').val(), month:0, day:1},
		    		yearRange: '-0:+1',
		    		startDate: minDates,
		    		endDate: last,
		    		hideIfNoPrevNext: true,
		        });

				$('.selectpicker').selectpicker();

				$('.kategori_operasional').on('change', function(){
					var id = $(this).data("id");
					var data = $('select[name="fokus_audit[0][detail]['+id+'][standarisasi_id]"] option:selected').val();
					$.ajax({
						url: '{{ url('ajax/option/get-kriteria-temuans') }}',
						type: 'POST',
						data: {
							_token: "{{ csrf_token() }}",
							id: data
						},
					})
					.done(function(response) {
						$('.kode_operasional1-'+id).html('');
						$('.kode_operasional1-'+id).html(response[0]);
						$('.kriteria_operasional_'+id).html(response[1]);
					})
					.fail(function() {
						console.log("error");
					});
				});

				$('.pilihan').on('change', function(){
					var id = $(this).data("id");
					var detail = $(this).data("detail");
					var data = $('select[name="fokus_audit['+id+'][detail]['+detail+'][tanggapan]"] option:selected').val();
					if(data == 0){
						if(id == 0){
							$('.tanggapan_operasional-'+detail).hide();
							$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", true);
						}else if(id == 1){
							$('.tanggapan_keuangan-'+detail).hide();
							$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", true);
						}else{
							$('.tanggapan_sistem-'+detail).hide();
							$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", true);
						}
						$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').prop("disabled", false);
					}else{
						if(id == 0){
							$('.tanggapan_operasional-'+detail).show();
							$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", false);
						}else if(id == 1){
							$('.tanggapan_keuangan-'+detail).show();
							$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", false);
						}else{
							$('.tanggapan_sistem-'+detail).show();
							$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", false);
						}
						$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').val('');
						$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').prop("disabled", true);
					}
				});
		});

		$(document).on('click', '.hapus_operasional', function (e){
			var id = $(this).data("id");
			var rowz = $('tr.detail-operasional-'+id).remove();
			var table = $('#table-operasional');
			var rows = table.find('tbody tr.data-operasional');
			$.each(rows, function(key, value){
				table.find('.operasional-numboor-'+$(this).data("id")).html(key+1);
			});
		});

		//KEUANGAN
		$(document).on('click', '.tambah_keuangan', function(e){
    		var last_keuangan = parseInt($('input[name=last_keuangan]').val()) + 1;
			var rowCount = $('#table-keuangan > tbody > tr.data-keuangan').length;
			var c = rowCount;
			var html = `
					<tr class="data-keuangan detail-keuangan-`+last_keuangan+`" data-id=`+last_keuangan+`>
						<input type="hidden" name="fokus_audit[1][detail][`+last_keuangan+`][user_id]" value="{{$users}}">
						<input type="hidden" name="fokus_audit[1][detail][`+last_keuangan+`][tipe]" value="1">
						<td style="text-align: center;width: 20px;">
							<label style="margin-top: 7px;" class="keuangan-numboor-`+last_keuangan+`">`+(c+1)+`</label>
						</td>
						<td style="width: 150px;">Judul</td>
						<td style="width: 2px;">:</td>
						<td style="width: 150px;text-align: center;padding-top: 15px;" class="kode_keuangan1-`+last_keuangan+`"></td>
						<td style="width: 1600px;" class="field">
							<div>
								<select class="selectpicker form-control kategori_keuangan" name="fokus_audit[1][detail][`+last_keuangan+`][standarisasi_id]" data-id="`+last_keuangan+`" data-style="btn-default" data-live-search="true">
									<option value="">(Pilih Salah Satu)</option>
									@foreach(App\Models\Master\StandarisasiTemuan::where('bidang', 2)->get() as $standarisasi)
										<option value="{{ $standarisasi->id }}">{{ $standarisasi->deskripsi }}</option>
									@endforeach
								</select>
							</div>
						</td>
						<td style="width: 50px;">
							<button class="btn btn-sm btn-danger hapus_keuangan" data-toggle="tooltip" data-placement="left" title="Hapus" type="button" style="border-radius: 20px;" data-id="`+last_keuangan+`"><i class="fa fa-close"></i></button>
						</td>
					</tr>
					<tr class="detail-keuangan-`+last_keuangan+`">
						<td style="" rowspan="9"></td>
						<td style="">Kondisi</td>
						<td style="">:</td>
						<td style="" colspan="2"  class="field">
							<textarea class="form-control" name="fokus_audit[1][detail][`+last_keuangan+`][catatan_kondisi]" id="exampleFormControlTextarea1" placeholder="Catatan Kondisi" rows="2"></textarea>
						</td>
						<td style="" rowspan="9"></td>
					</tr>
					<tr class="detail-keuangan-`+last_keuangan+`">
						<td style="">Kriteria</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field" colspan="2">
							<ol class="kriteria_keuangan_`+last_keuangan+`">
							</ol>
						</td>
					</tr>
					<tr class="detail-keuangan-`+last_keuangan+`">
						<td style="">Sebab</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<textarea class="form-control" name="fokus_audit[1][detail][`+last_keuangan+`][sebab]" id="exampleFormControlTextarea1" placeholder="Sebab" rows="2"></textarea>
						</td>
					</tr>
					<tr class="detail-keuangan-`+last_keuangan+`">
						<td style="">Risiko</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<textarea class="form-control" name="fokus_audit[1][detail][`+last_keuangan+`][risiko]" id="exampleFormControlTextarea1" placeholder="Risiko" rows="2"></textarea>
						</td>
					</tr>
					<tr class="detail-keuangan-`+last_keuangan+`">
						<td style="">Kategori Temuan</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
							<div class="field">
								<select class="selectpicker form-control" data-width="310px" name="fokus_audit[1][detail][`+last_keuangan+`][kategori_id]" data-style="btn-default" data-live-search="true">
									<option value="">(Pilih Salah Satu)</option>
									@foreach(App\Models\Master\KategoriTemuan::get() as $kategori)
										<option value="{{ $kategori->id }}">{{ $kategori->nama }}</option>
									@endforeach
								</select>
							</div>
						</td>
					</tr>
					<tr class="detail-keuangan-`+last_keuangan+`">
						<td style="">Rekomendasi</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
							<table id="keuangan-rekomendasi-`+last_keuangan+`" style="width: 100%;">
								<tbody class="container-keuangan-rekomendasi-`+last_keuangan+`">
									<tr>
										<td style="width: 100%;padding-right: 18px;padding-bottom: 12px;" class="field">
											<textarea class="form-control" name="fokus_audit[1][detail][`+last_keuangan+`][detail_rekomen][0][rekomendasi]" id="exampleFormControlTextarea1" placeholder="Rekomendasi" rows="2"></textarea>
										</td>
										<td style="width: 5%;">
											<button class="btn btn-sm btn-success tambah_rekomendasi_keuangan" data-toggle="tooltip" data-placement="left" title="Tambah" type="button" data-keuangan="`+last_keuangan+`" style="border-radius: 20px;margin-top: -18px;"><i class="fa fa-plus"></i></button>
										</td>
									</tr>
									<input type="hidden" name="last_rekomendasi_keuangan-`+last_keuangan+`" value="0">
								</tbody>
							</table>
						</td>
					</tr>
					<tr class="detail-keuangan-`+last_keuangan+`">
						<td style="">Tanggapan Auditee</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<div>
								<select data-width="25%" data-id="1" data-detail="`+last_keuangan+`" class="selectpicker col-sm-12 show-tick pilihan" data-size="3" name="fokus_audit[1][detail][`+last_keuangan+`][tanggapan]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
									<option data-content="<span class='badge' style='background-color:green;'>MENYETUJUI</span>" value="0" selected>MENYETUJUI</option>
								</select>
							</div>
							<br>
							<textarea class="form-control tanggapan_keuangan-`+last_keuangan+`" style="display: none;" name="fokus_audit[1][detail][`+last_keuangan+`][catatan]" id="exampleFormControlTextarea1" placeholder="Catatan Tanggapan Auditee" rows="2"></textarea>
						</td>
					</tr>
					<tr class="detail-keuangan-end-`+last_keuangan+` detail-keuangan-`+last_keuangan+`">
						<td style="">Tanggal Tindak Lanjut</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<input style="width: 25%;" class="form-control tanggal" id="tanggal" name="fokus_audit[1][detail][`+last_keuangan+`][tanggal]" placeholder="Tanggal" type="text"/>
						</td>
					</tr>
					<tr class="detail-keuangan-end-`+last_keuangan+` detail-keuangan-`+last_keuangan+`" style="border-bottom: 2px solid black;">
						<td style="">Lampiran</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<div class="file-loading">
				            	<input id="img-0-`+last_keuangan+`" name="fokus_audit[1][detail][`+last_keuangan+`][lampiran]" type="file" class="file form-control lampir" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/jpeg,image/gif,image/png,application/pdf">
				            </div>
						</td>
					</tr>
				`;

				$('.container-keuangan').append(html);
		        $('input[name=last_keuangan]').val(last_keuangan);

		        $('.lampir').fileinput({
				    language: 'es',
				    uploadUrl: "/file-upload-batch/2",
				    previewFileType: "image",
				    showUpload: false,
				    previewFileIcon: '<i class="fas fa-file"></i>',
				    fileActionSettings: {
					    showRemove: true,
					    showUpload: false, //This remove the upload button
					    showZoom: true,
					    showDrag: true
					},
				    initialPreviewAsData: true,
				    purifyHtml: true,
				});

		        var d = new Date();
		    	var full = d.getFullYear();
		    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
		    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
		    	var last = new Date(lastDate);
		    	if($('input[name=tahun]').val() > full){
		    		var min = minDates;
		    	}else{
		    		var min = d;
		    	}

		    	$('.tanggal').datepicker({
		    		orientation: "top left",
		    		todayHighlight: true,
		    		autoclose:true,
		    		format: 'dd-mm-yyyy',
		    		defaultViewDate: {year:$('input[name=tahun]').val(), month:0, day:1},
		    		yearRange: '-0:+1',
		    		startDate: minDates,
		    		endDate: last,
		    		hideIfNoPrevNext: true,
		        });

				$('.selectpicker').selectpicker();

				$('.kategori_keuangan').on('change', function(){
					var id = $(this).data("id");
					var data = $('select[name="fokus_audit[1][detail]['+id+'][standarisasi_id]"] option:selected').val();
					$.ajax({
						url: '{{ url('ajax/option/get-kriteria-temuans') }}',
						type: 'POST',
						data: {
							_token: "{{ csrf_token() }}",
							id: data
						},
					})
					.done(function(response) {
						$('.kode_keuangan1-'+id).html('');
						$('.kode_keuangan1-'+id).html(response[0]);
						$('.kriteria_keuangan_'+id).html(response[1]);
					})
					.fail(function() {
						console.log("error");
					});
				});

				$('.pilihan').on('change', function(){
					var id = $(this).data("id");
					var detail = $(this).data("detail");
					var data = $('select[name="fokus_audit['+id+'][detail]['+detail+'][tanggapan]"] option:selected').val();
					if(data == 0){
						if(id == 0){
							$('.tanggapan_operasional-'+detail).hide();
							$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", true);
						}else if(id == 1){
							$('.tanggapan_keuangan-'+detail).hide();
							$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", true);
						}else{
							$('.tanggapan_sistem-'+detail).hide();
							$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", true);
						}
						$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').prop("disabled", false);
					}else{
						if(id == 0){
							$('.tanggapan_operasional-'+detail).show();
							$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", false);
						}else if(id == 1){
							$('.tanggapan_keuangan-'+detail).show();
							$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", false);
						}else{
							$('.tanggapan_sistem-'+detail).show();
							$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", false);
						}
						$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').val('');
						$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').prop("disabled", true);
					}
				});
		});

		$(document).on('click', '.hapus_keuangan', function (e){
			var id = $(this).data("id");
			var rowz = $('tr.detail-keuangan-'+id).remove();
			var table = $('#table-keuangan');
			var rows = table.find('tbody tr.data-keuangan');
			$.each(rows, function(key, value){
				table.find('.keuangan-numboor-'+$(this).data("id")).html(key+1);
			});
		});

		// SISTEM
		$(document).on('click', '.tambah_sistem', function(e){
    		var last_sistem = parseInt($('input[name=last_sistem]').val()) + 1;
			var rowCount = $('#table-sistem > tbody > tr.data-sistem').length;
			var c = rowCount;
			var html = `
					<tr class="data-sistem detail-sistem-`+last_sistem+`" data-id=`+last_sistem+`>
						<input type="hidden" name="fokus_audit[2][detail][`+last_sistem+`][user_id]" value="{{$users}}">
						<input type="hidden" name="fokus_audit[2][detail][`+last_sistem+`][tipe]" value="2">
						<td style="text-align: center;width: 20px;">
							<label style="margin-top: 7px;" class="sistem-numboor-`+last_sistem+`">`+(c+1)+`</label>
						</td>
						<td style="width: 150px;">Judul</td>
						<td style="width: 2px;">:</td>
						<td style="width: 150px;text-align: center;padding-top: 15px;" class="kode_sistem1-`+last_sistem+`"></td>
						<td style="width: 1600px;" class="field">
							<div>
								<select class="selectpicker form-control kategori_sistem" name="fokus_audit[2][detail][`+last_sistem+`][standarisasi_id]" data-id="`+last_sistem+`" data-style="btn-default" data-live-search="true">
									<option value="">(Pilih Salah Satu)</option>
									@foreach(App\Models\Master\StandarisasiTemuan::where('bidang', 3)->get() as $standarisasi)
										<option value="{{ $standarisasi->id }}">{{ $standarisasi->deskripsi }}</option>
									@endforeach
								</select>
							</div>
						</td>
						<td style="width: 50px;">
							<button class="btn btn-sm btn-danger hapus_sistem" data-toggle="tooltip" data-placement="left" title="Hapus" type="button" style="border-radius: 20px;" data-id="`+last_sistem+`"><i class="fa fa-close"></i></button>
						</td>
					</tr>
					<tr class="detail-sistem-`+last_sistem+`">
						<td style="" rowspan="9"></td>
						<td style="">Kondisi</td>
						<td style="">:</td>
						<td style="" colspan="2"  class="field">
							<textarea class="form-control" name="fokus_audit[2][detail][`+last_sistem+`][catatan_kondisi]" id="exampleFormControlTextarea1" placeholder="Catatan Kondisi" rows="2"></textarea>
						</td>
						<td style="" rowspan="9"></td>
					</tr>
					<tr class="detail-sistem-`+last_sistem+`">
						<td style="">Kriteria</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field" colspan="2">
							<ol class="kriteria_sistem_`+last_sistem+`">
							</ol>
						</td>
					</tr>
					<tr class="detail-sistem-`+last_sistem+`">
						<td style="">Sebab</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<textarea class="form-control" name="fokus_audit[2][detail][`+last_sistem+`][sebab]" id="exampleFormControlTextarea1" placeholder="Sebab" rows="2"></textarea>
						</td>
					</tr>
					<tr class="detail-sistem-`+last_sistem+`">
						<td style="">Risiko</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<textarea class="form-control" name="fokus_audit[2][detail][`+last_sistem+`][risiko]" id="exampleFormControlTextarea1" placeholder="Risiko" rows="2"></textarea>
						</td>
					</tr>
					<tr class="detail-sistem-`+last_sistem+`">
						<td style="">Kategori Temuan</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
							<div class="field">
								<select class="selectpicker form-control" data-width="310px" name="fokus_audit[2][detail][`+last_sistem+`][kategori_id]" data-style="btn-default" data-live-search="true">
									<option value="">(Pilih Salah Satu)</option>
									@foreach(App\Models\Master\KategoriTemuan::get() as $kategori)
										<option value="{{ $kategori->id }}">{{ $kategori->nama }}</option>
									@endforeach
								</select>
							</div>
						</td>
					</tr>
					<tr class="detail-sistem-`+last_sistem+`">
						<td style="">Rekomendasi</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
							<table id="sistem-rekomendasi-`+last_sistem+`" style="width: 100%;">
								<tbody class="container-sistem-rekomendasi-`+last_sistem+`">
									<tr>
										<td style="width: 100%;padding-right: 18px;padding-bottom: 12px;" class="field">
											<textarea class="form-control" name="fokus_audit[2][detail][`+last_sistem+`][detail_rekomen][0][rekomendasi]" id="exampleFormControlTextarea1" placeholder="Rekomendasi" rows="2"></textarea>
										</td>
										<td style="width: 5%;">
											<button class="btn btn-sm btn-success tambah_rekomendasi_sistem" data-toggle="tooltip" data-placement="left" title="Tambah" type="button" data-sistem="`+last_sistem+`" style="border-radius: 20px;margin-top: -18px;"><i class="fa fa-plus"></i></button>
										</td>
									</tr>
									<input type="hidden" name="last_rekomendasi_sistem-`+last_sistem+`" value="0">
								</tbody>
							</table>
						</td>
					</tr>
					<tr class="detail-sistem-`+last_sistem+`">
						<td style="">Tanggapan Auditee</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<div>
								<select data-width="25%" data-id="2" data-detail="`+last_sistem+`" class="selectpicker col-sm-12 show-tick pilihan" data-size="3" name="fokus_audit[2][detail][`+last_sistem+`][tanggapan]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
									<option data-content="<span class='badge' style='background-color:green;'>MENYETUJUI</span>" value="0" selected>MENYETUJUI</option>
								</select>
							</div>
							<br>
							<textarea class="form-control tanggapan_sistem-`+last_sistem+`" style="display: none;" name="fokus_audit[2][detail][`+last_sistem+`][catatan]" id="exampleFormControlTextarea1" placeholder="Catatan Tanggapan Auditee" rows="2"></textarea>
						</td>
					</tr>
					<tr class="detail-sistem-end-`+last_sistem+` detail-sistem-`+last_sistem+`">
						<td style="">Tanggal Tindak Lanjut</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<input style="width: 25%;" class="form-control tanggal" id="tanggal" name="fokus_audit[2][detail][`+last_sistem+`][tanggal]" placeholder="Tanggal" type="text"/>
						</td>
					</tr>
					<tr class="detail-sistem-end-`+last_sistem+` detail-sistem-`+last_sistem+`" style="border-bottom: 2px solid black;">
						<td style="">Lampiran</td>
						<td style="">:</td>
						<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
							<div class="file-loading">
				            	<input id="img-0-`+last_sistem+`" name="fokus_audit[2][detail][`+last_sistem+`][lampiran]" type="file" class="file form-control lampir" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/jpeg,image/gif,image/png,application/pdf">
				            </div>
						</td>
					</tr>
				`;

				$('.container-sistem').append(html);
		        $('input[name=last_sistem]').val(last_sistem);

		        $('.lampir').fileinput({
				    language: 'es',
				    uploadUrl: "/file-upload-batch/2",
				    previewFileType: "image",
				    showUpload: false,
				    previewFileIcon: '<i class="fas fa-file"></i>',
				    fileActionSettings: {
					    showRemove: true,
					    showUpload: false, //This remove the upload button
					    showZoom: true,
					    showDrag: true
					},
				    initialPreviewAsData: true,
				    purifyHtml: true,
				});

		        var d = new Date();
		    	var full = d.getFullYear();
		    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
		    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
		    	var last = new Date(lastDate);
		    	if($('input[name=tahun]').val() > full){
		    		var min = minDates;
		    	}else{
		    		var min = d;
		    	}

		    	$('.tanggal').datepicker({
		    		orientation: "top left",
		    		todayHighlight: true,
		    		autoclose:true,
		    		format: 'dd-mm-yyyy',
		    		defaultViewDate: {year:$('input[name=tahun]').val(), month:0, day:1},
		    		yearRange: '-0:+1',
		    		startDate: minDates,
		    		endDate: last,
		    		hideIfNoPrevNext: true,
		        });

				$('.selectpicker').selectpicker();

				$('.kategori_sistem').on('change', function(){
					var id = $(this).data("id");
					var data = $('select[name="fokus_audit[2][detail]['+id+'][standarisasi_id]"] option:selected').val();
					$.ajax({
						url: '{{ url('ajax/option/get-kriteria-temuans') }}',
						type: 'POST',
						data: {
							_token: "{{ csrf_token() }}",
							id: data
						},
					})
					.done(function(response) {
						$('.kode_sistem1-'+id).html('');
						$('.kode_sistem1-'+id).html(response[0]);
						$('.kriteria_sistem_'+id).html(response[1]);
					})
					.fail(function() {
						console.log("error");
					});
				});


				$('.pilihan').on('change', function(){
					var id = $(this).data("id");
					var detail = $(this).data("detail");
					var data = $('select[name="fokus_audit['+id+'][detail]['+detail+'][tanggapan]"] option:selected').val();
					if(data == 0){
						if(id == 0){
							$('.tanggapan_operasional-'+detail).hide();
							$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", true);
						}else if(id == 1){
							$('.tanggapan_keuangan-'+detail).hide();
							$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", true);
						}else{
							$('.tanggapan_sistem-'+detail).hide();
							$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", true);
						}
						$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').prop("disabled", false);
					}else{
						if(id == 0){
							$('.tanggapan_operasional-'+detail).show();
							$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", false);
						}else if(id == 1){
							$('.tanggapan_keuangan-'+detail).show();
							$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", false);
						}else{
							$('.tanggapan_sistem-'+detail).show();
							$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", false);
						}
						$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').val('');
						$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').prop("disabled", true);
					}
				});
		});

		$(document).on('click', '.hapus_sistem', function (e){
			var id = $(this).data("id");
			var rowz = $('tr.detail-sistem-'+id).remove();
			var table = $('#table-sistem');
			var rows = table.find('tbody tr.data-sistem');
			$.each(rows, function(key, value){
				table.find('.sistem-numboor-'+$(this).data("id")).html(key+1);
			});
		});

		$(document).on('click', '.tambah_rekomendasi_operasional', function(e){
			var id = $(this).data("operasional");
    		var last_rekomendasi_operasional = parseInt($('input[name=last_rekomendasi_operasional-'+id+']').val()) + 1;
			var row = $('#operasional-rekomendasi-'+id);
			var rowCount = row.find('tbody tr').length;
			var c = rowCount;
			var html = `
					<tr>
						<td style="width: 100%;padding-right: 18px;padding-bottom: 12px;" class="field">
							<textarea class="form-control" name="fokus_audit[0][detail][`+id+`][detail_rekomen][`+last_rekomendasi_operasional+`][rekomendasi]" id="exampleFormControlTextarea1" placeholder="Rekomendasi" rows="2"></textarea>
						</td>
						<td style="width: 5%;">
							<button class="btn btn-sm btn-danger hapus_rekomendasi_operasional" data-toggle="tooltip" data-placement="left" data-operasional="`+id+`" title="Hapus" type="button" style="border-radius: 20px;margin-top: -18px;"><i class="fa fa-close"></i></button>
						</td>
					</tr>
				`;
			$('.container-operasional-rekomendasi-'+id).append(html);
			$('input[name=last_rekomendasi_operasional-'+id+']').val(last_rekomendasi_operasional);
		});

		$(document).on('click', '.hapus_rekomendasi_operasional', function (e){
			var id = $(this).data("id");
			var val = $(this).data("operasional");
			var row = $(this).closest('tr');
			row.remove();
		});

		$(document).on('click', '.tambah_rekomendasi_keuangan', function(e){
			var id = $(this).data("keuangan");
    		var last_rekomendasi_keuangan = parseInt($('input[name=last_rekomendasi_keuangan-'+id+']').val()) + 1;
			var row = $('#keuangan-rekomendasi-'+id);
			var rowCount = row.find('tbody tr').length;
			var c = rowCount;
			var html = `
					<tr>
						<td style="width: 100%;padding-right: 18px;padding-bottom: 12px;" class="field">
							<textarea class="form-control" name="fokus_audit[1][detail][`+id+`][detail_rekomen][`+last_rekomendasi_keuangan+`][rekomendasi]" id="exampleFormControlTextarea1" placeholder="Rekomendasi" rows="2"></textarea>
						</td>
						<td style="width: 5%;">
							<button class="btn btn-sm btn-danger hapus_rekomendasi_keuangan" data-toggle="tooltip" data-placement="left" data-keuangan="`+id+`" title="Hapus" type="button" style="border-radius: 20px;margin-top: -18px;"><i class="fa fa-close"></i></button>
						</td>
					</tr>
				`;
			$('.container-keuangan-rekomendasi-'+id).append(html);
			$('input[name=last_rekomendasi_keuangan-'+id+']').val(last_rekomendasi_keuangan);
		});

		$(document).on('click', '.hapus_rekomendasi_keuangan', function (e){
			var id = $(this).data("id");
			var val = $(this).data("keuangan");
			var row = $(this).closest('tr');
			row.remove();
		});

		$(document).on('click', '.tambah_rekomendasi_sistem', function(e){
			var id = $(this).data("sistem");
    		var last_rekomendasi_sistem = parseInt($('input[name=last_rekomendasi_sistem-'+id+']').val()) + 1;
			var row = $('#sistem-rekomendasi-'+id);
			var rowCount = row.find('tbody tr').length;
			var c = rowCount;
			var html = `
					<tr>
						<td style="width: 100%;padding-right: 18px;padding-bottom: 12px;" class="field">
							<textarea class="form-control" name="fokus_audit[2][detail][`+id+`][detail_rekomen][`+last_rekomendasi_sistem+`][rekomendasi]" id="exampleFormControlTextarea1" placeholder="Rekomendasi" rows="2"></textarea>
						</td>
						<td style="width: 5%;">
							<button class="btn btn-sm btn-danger hapus_rekomendasi_sistem" data-toggle="tooltip" data-placement="left" data-sistem="`+id+`" title="Hapus" type="button" style="border-radius: 20px;margin-top: -18px;"><i class="fa fa-close"></i></button>
						</td>
					</tr>
				`;
			$('.container-sistem-rekomendasi-'+id).append(html);
			$('input[name=last_rekomendasi_sistem-'+id+']').val(last_rekomendasi_sistem);
		});

		$(document).on('click', '.hapus_rekomendasi_sistem', function (e){
			var id = $(this).data("id");
			var val = $(this).data("sistem");
			var row = $(this).closest('tr');
			row.remove();
		});
    </script>
    @yield('js-extra')
@endpush

