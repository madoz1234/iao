@extends('layouts.form')
@section('title', 'Detil KKA')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body" style="padding-bottom: 0px;">
		<form action="" method="POST" id="formData">
			@method('PATCH')
			@csrf
			<div class="loading dimmer padder-v" style="display: none;">
			    <div class="loader"></div>
			</div>
			@php 
				$users = object_users($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
			@endphp
			<input type="hidden" name="id" value="{{ $record->id }}">
			<input type="hidden" name="tahun" value="{{ $record->program->penugasanaudit->rencanadetail->rencanaaudit->tahun }}">
			<input type="hidden" name="status" value="0">
			<div class="form-row">
				<div class="form-group col-md-12">
					<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
						<tr>
							<td style="width: 10%;border: 1px #ffffff;">Tipe</td>
							<td style="width: 1%;border: 1px #ffffff;">:</td>
							<td style="text-align:left;border: 1px #ffffff;" class="field">
								@if($record->program->penugasanaudit->rencanadetail->tipe == 0)
									<span class="label label-primary">Audit</span>
								@elseif($record->program->penugasanaudit->rencanadetail->tipe == 1)
									<span class="label label-success">Kegiatan Konsultasi</span>
								@else 
									<span class="label label-info">Kegiatan Audit Lain - Lain</span>
								@endif
							</td>
						</tr>
						<tr>
							<td style="width: 10%;border: 1px #ffffff;">Kategori</td>
							<td style="width: 1%;border: 1px #ffffff;">:</td>
							<td style="text-align:left;border: 1px #ffffff;" class="field">
								@if($record->program->penugasanaudit->rencanadetail->tipe == 0)
									@if($record->program->penugasanaudit->rencanadetail->tipe_object == 0)
										<span class="label label-success">Business Unit (BU)</span>
									@elseif($record->program->penugasanaudit->rencanadetail->tipe_object == 1)
										<span class="label label-info">Corporate Office (CO)</span>
									@elseif($record->program->penugasanaudit->rencanadetail->tipe_object == 2)
										<span class="label label-default">Project</span>
									@else 
										<span class="label label-primary">Anak Perusahaan</span>
									@endif
								@elseif($record->program->penugasanaudit->rencanadetail->tipe == 1)
									<span class="label label-warning">{{ $record->program->penugasanaudit->rencanadetail->konsultasi->nama }}</span>
								@else 
									<span class="label label-default">{{ $record->program->penugasanaudit->rencanadetail->lain->nama }}</span>
								@endif
							</td>
						</tr>
						<tr>
							<td style="width: 10%;border: 1px #ffffff;">Objek Audit</td>
							<td style="width: 1%;border: 1px #ffffff;">:</td>
							<td style="text-align: left;border: 1px #ffffff;" class="field">

								{!! object_audit($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id)!!}
							</td>
						</tr>
						<tr>
							<td style="width: 10%;border: 1px #ffffff;">Tanggal Audit</td>
							<td style="width: 1%;border: 1px #ffffff;">:</td>
							<td style="text-align: left;border: 1px #ffffff;" class="field">
								 {{ DateToStringWday($record->program->detailpelaksanaan->min('tgl')) }}  -  {{DateToStringWday($record->program->detailpelaksanaan->max('tgl'))}}
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="form-row">
					<div class="form-group col-md-12">
						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item">
								<a style="font-weight: bold;" class="nav-link active" id="a-tab" data-toggle="tab" href="#a" role="tab" aria-controls="home" aria-selected="true">Operasional</a>
							</li>
							<li class="nav-item">
								<a style="font-weight: bold;" class="nav-link" id="b-tab" data-toggle="tab" href="#b" role="tab" aria-controls="profile" aria-selected="false">Keuangan</a>
							</li>
							<li class="nav-item">
								<a style="font-weight: bold;" class="nav-link" id="c-tab" data-toggle="tab" href="#c" role="tab" aria-controls="messages" aria-selected="false">Sistem</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="a" role="tabpanel" aria-labelledby="a-tab">
								<table id="table-operasional" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
									@php 
										$i=0;
										$cek1 = count($record->program->detailkerja->where('bidang', 1)->where('fokus_audit_id', 1000));
									@endphp
									@if($cek1 > 0)
										<tbody>
											<tr class="detail-operasional-0" data-id="0">
												<td style="text-align: left;font-weight: bold;" colspan="5">Bidang Audit : Operasional</td>
											</tr>
											<tr class="detail-operasional-0" data-id="0">
												<td style="text-align: left;font-weight: bold;" colspan="5">NONE</td>
												<input type="hidden" name="kka" value="0">
											</tr>
										</tbody>
										<input type="hidden" name="tipe" value="0">
									@else
										<input type="hidden" name="kka" value="1">
									@endif
									<tbody class="container-operasional">
										@foreach($record->detaildraft->where('tipe', 0) as $key => $operasional)
											<tr class="detail-operasional-{{$i}}" data-id="{{$i}}">
												<td style="text-align: left;font-weight: bold;" colspan="4">Bidang Audit : Operasional</td>
											</tr>
											<tr class="data-operasional detail-operasional-{{$i}}">
												<input type="hidden" name="fokus_audit[0][detail][{{$i}}][data_id]" value="{{$operasional->id}}">
												<td style="text-align: center;width: 20px;">
													<label style="margin-top: 7px;" class="operasional-numboor-{{$i}}">{{$i+1}}</label>
												</td>
												<td style="width: 150px;">Judul</td>
												<td style="width: 2px;">:</td>
												<td style="width: 1600px;text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="cari-operasional kategori_operasional" data-id="{{$i}}" data-val="{{$operasional->id}}" data-cari="{{$operasional->standarisasi_id}}">
													{{getstandarisasi($operasional->standarisasi_id)}}
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="" rowspan="9"></td>
												<td style="">Kondisi</td>
												<td style="">:</td>
												<td style="text-align: left;" colspan="2"  class="field">
													{{ $operasional->catatan_kondisi }}
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="">Kriteria</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field" colspan="2">
													<ol style="padding-left: 12px;" class="kriteria_operasional_{{$i}}">
													</ol>
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="">Sebab</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													{{ $operasional->sebab }}
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="">Risiko</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													{{ $operasional->risiko }}
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="">Kategori Temuan</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
													{!! getkategoristatus($operasional->kategori_id) !!}
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="">Rekomendasi</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													@foreach($operasional->rekomendasi as $x => $operasionals)
														{{$x+1}}. {{$operasionals->rekomendasi}}<br>
													@endforeach
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="">Tanggapan Auditee</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													@if($operasional->tanggapan == 0)
														<label class="label label-success">MENYETUJUI</label>
													@else 
														<label class="label label-danger">TIDAK MENYETUJUI</label>
														<br>
														<br>
														Catatan Tanggapan Auditee : <br>
														<p style="text-align: justify;text-justify: inter-word;">{{ $operasional->catatan }}</p>
														@if($operasional->ba)
															<label class="label label-success">Auditee bersedia menandatangani Berita Acara Tidak Setuju</label><br>
														@else 
															<label class="label label-danger">Auditee tidak bersedia menandatangani Berita Acara Tidak Setuju</label><br>
														@endif
														<br>
														<br>
														<div class="file-loading">
											            	<input id="ops-{{ $operasional->id }}" name="fokus_audit[0][detail][{{$i}}][lampirans]" data-id="{{ $operasional->id }}" type="file" class="form-control lampirs" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/jpeg,image/gif,image/png,application/pdf">
											            </div>
													@endif
												</td>
											</tr>
											<tr class="detail-operasional-end-{{$i}} detail-operasional-{{$i}}">
												<td style="">Tanggal Tindak Lanjut</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													{{ DateToString($operasional->tgl) }}
												</td>
											</tr>
											<tr class="detail-operasional-end-{{$i}} detail-operasional-{{$i}}" style="border-bottom: 2px solid black;">
												<td style="">Lampiran</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<div class="file-loading">
										            	<input id="op-{{ $operasional->id }}" name="fokus_audit[0][detail][{{$i}}][lampiran]" data-id="{{ $operasional->id }}" type="file" class="form-control lampir" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/jpeg,image/gif,image/png,application/pdf">
										            </div>
												</td>
											</tr>
											@php 
												$i++;
											@endphp
										@endforeach
									</tbody>
								</table>
								<div class="form-group col-md-12">
									<div class="text-right">
										<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
										<button type="button" class="btn btn-info page1">Selanjutnya</button>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="b" role="tabpanel" aria-labelledby="b-tab">
								<table id="table-keuangan" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
										@php 
											$j=0;
											$cek2 = count($record->program->detailkerja->where('bidang', 2)->where('fokus_audit_id', 1000));
										@endphp
										@if($cek2 > 0)
											<tbody>
												<tr class="detail-operasional-0" data-id="0">
													<td style="text-align: left;font-weight: bold;" colspan="5">Bidang Audit : Keuangan</td>
												</tr>
												<tr class="detail-operasional-0" data-id="0">
													<td style="text-align: left;font-weight: bold;" colspan="5">NONE</td>
													<input type="hidden" name="kka" value="0">
												</tr>
											</tbody>
											<input type="hidden" name="tipe" value="1">
										@else
											<input type="hidden" name="kka" value="1">
										@endif
									<tbody class="container-keuangan">
										@foreach($record->detaildraft->where('tipe', 1) as $key => $keuangan)
											<tr class="detail-keuangan-{{$j}}" data-id="{{$j}}">
												<td style="text-align: left;font-weight: bold;" colspan="4">Bidang Audit : Keuangan</td>
											</tr>
											<tr class="data-keuangan detail-keuangan-{{$j}}">
												<input type="hidden" name="fokus_audit[1][detail][{{$j}}][data_id]" value="{{$keuangan->id}}">
												<td style="text-align: center;width: 20px;">
													<label style="margin-top: 7px;" class="keuangan-numboor-{{$j}}">{{$j+1}}</label>
												</td>
												<td style="width: 150px;">Judul</td>
												<td style="width: 2px;">:</td>
												<td style="width: 1600px;text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="cari-keuangan kategori_keuangan" data-id="{{$j}}" data-val="{{$keuangan->id}}" data-cari="{{$keuangan->standarisasi_id}}">
													{{getstandarisasi($keuangan->standarisasi_id)}}
												</td>
											</tr>
											<tr class="detail-keuangan-{{$j}}">
												<td style="" rowspan="9"></td>
												<td style="">Kondisi</td>
												<td style="">:</td>
												<td style="text-align: left;" colspan="2"  class="field">
													{{ $keuangan->catatan_kondisi }}
												</td>
											</tr>
											<tr class="detail-keuangan-{{$j}}">
												<td style="">Kriteria Temuan</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field" colspan="2">
													<ol style="padding-left: 12px;" class="kriteria_keuangan_{{$j}}">
													</ol>
												</td>
											</tr>
											<tr class="detail-keuangan-{{$j}}">
												<td style="">Sebab</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													{{ $keuangan->sebab }}
												</td>
											</tr>
											<tr class="detail-keuangan-{{$j}}">
												<td style="">Risiko</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													{{ $keuangan->risiko }}
												</td>
											</tr>
											<tr class="detail-keuangan-{{$j}}">
												<td style="">Kategori Temuan</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
													{!! getkategoristatus($keuangan->kategori_id) !!}
												</td>
											</tr>
											<tr class="detail-keuangan-{{$j}}">
												<td style="">Rekomendasi</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													@foreach($keuangan->rekomendasi as $y => $keuangans)
														{{$y+1}}. {{$keuangans->rekomendasi}}<br>
													@endforeach
												</td>
											</tr>
											<tr class="detail-keuangan-{{$j}}">
												<td style="">Tanggapan Auditee</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													@if($keuangan->tanggapan == 0)
														<label class="label label-success">MENYETUJUI</label>
													@else 
														<label class="label label-danger">TIDAK MENYETUJUI</label>
														<br>
														<br>
														Catatan Tanggapan Auditee : <br>
														<p style="text-align: justify;text-justify: inter-word;">{{ $keuangan->catatan }}</p>
														@if($keuangan->ba)
															<label class="label label-success">Auditee bersedia menandatangani Berita Acara Tidak Setuju</label><br>
														@else 
															<label class="label label-danger">Auditee tidak bersedia menandatangani Berita Acara Tidak Setuju</label><br>
														@endif
														<br>
														<br>
														<div class="file-loading">
											            	<input id="keus-{{ $keuangan->id }}" name="fokus_audit[1][detail][{{$j}}][lampirans]" data-id="{{ $keuangan->id }}" type="file" class="form-control lampirs" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/jpeg,image/gif,image/png,application/pdf">
											            </div>
													@endif
												</td>
											</tr>
											<tr class="detail-keuangan-end-{{$j}} detail-keuangan-{{$j}}">
												<td style="">Tanggal Tindak Lanjut</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													{{ DateToString($keuangan->tgl) }}
												</td>
											</tr>
											<tr class="detail-keuangan-end-{{$j}} detail-keuangan-{{$j}}" style="border-bottom: 2px solid black;">
												<td style="">Lampiran</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<div class="file-loading">
										            	<input id="keu-{{ $keuangan->id }}" name="fokus_audit[1][detail][{{$j}}][lampiran]" data-id="{{ $keuangan->id }}" type="file" class="form-control lampir" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/jpeg,image/gif,image/png,application/pdf">
										            </div>
												</td>
											</tr>
											@php 
												$j++;
											@endphp
										@endforeach
									</tbody>
								</table>
								<div class="form-group col-md-12">
									<div class="text-right">
										<button type="button" class="btn btn-cancel kembali1">Sebelumnya</button>
										<button type="button" class="btn btn-info page2">Selanjutnya</button>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="c" role="tabpanel" aria-labelledby="c-tab">
								<table id="table-sistem" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
										@php 
											$k=0;
											$cek3 = count($record->program->detailkerja->where('bidang', 3)->where('fokus_audit_id', 1000));
										@endphp
										@if($cek3 > 0)
											<tbody>
												<tr class="detail-operasional-0" data-id="0">
													<td style="text-align: left;font-weight: bold;" colspan="5">Bidang Audit : Sistem</td>
												</tr>
												<tr class="detail-operasional-0" data-id="0">
													<td style="text-align: left;font-weight: bold;" colspan="5">NONE</td>
													<input type="hidden" name="kka" value="0">
												</tr>
											</tbody>
											<input type="hidden" name="tipe" value="2">
										@else
											<input type="hidden" name="kka" value="1">
										@endif
									<tbody class="container-sistem">
										@foreach($record->detaildraft->where('tipe', 2) as $key => $sistem)
											<tr class="detail-sistem-{{$k}}" data-id="{{$k}}">
												<td style="text-align: left;font-weight: bold;" colspan="4">Bidang Audit : Sistem</td>
											</tr>
											<tr class="data-sistem detail-sistem-{{$k}}">
												<input type="hidden" name="fokus_audit[2][detail][{{$k}}][data_id]" value="{{$sistem->id}}">
												<td style="text-align: center;width: 20px;">
													<label style="margin-top: 7px;" class="sistem-numboor-{{$k}}">{{$k+1}}</label>
												</td>
												<td style="width: 150px;">Judul</td>
												<td style="width: 2px;">:</td>
												<td style="width: 1600px;text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="cari-sistem kategori_sistem" data-id="{{$k}}" data-val="{{$sistem->id}}" data-cari="{{$sistem->standarisasi_id}}">
													{{getstandarisasi($sistem->standarisasi_id)}}
												</td>
											</tr>
											<tr class="detail-sistem-{{$k}}">
												<td style="" rowspan="9"></td>
												<td style="">Kondisi</td>
												<td style="">:</td>
												<td style="text-align: left;" colspan="2"  class="field">
													{{ $sistem->catatan_kondisi }}
												</td>
											</tr>
											<tr class="detail-sistem-{{$k}}">
												<td style="">Kriteria Temuan</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field" colspan="2">
													<ol style="padding-left: 12px;" class="kriteria_sistem_{{$k}}">
													</ol>
												</td>
											</tr>
											<tr class="detail-sistem-{{$k}}">
												<td style="">Sebab</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													{{ $sistem->sebab }}
												</td>
											</tr>
											<tr class="detail-sistem-{{$k}}">
												<td style="">Risiko</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													{{ $sistem->risiko }}
												</td>
											</tr>
											<tr class="detail-sistem-{{$k}}">
												<td style="">Kategori Temuan</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
													{!! getkategoristatus($sistem->kategori_id) !!}
												</td>
											</tr>
											<tr class="detail-sistem-{{$k}}">
												<td style="">Rekomendasi</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													@foreach($sistem->rekomendasi as $z => $sistems)
														{{$z+1}}. {{$sistems->rekomendasi}}<br>
													@endforeach
												</td>
											</tr>
											<tr class="detail-sistem-{{$k}}">
												<td style="">Tanggapan Auditee</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													@if($sistem->tanggapan == 0)
														<label class="label label-success">MENYETUJUI</label>
													@else 
														<label class="label label-danger">TIDAK MENYETUJUI</label>
														<br>
														<br>
														Catatan Tanggapan Auditee : <br>
														<p style="text-align: justify;text-justify: inter-word;">{{ $sistem->catatan }}</p>
														@if($sistem->ba)
															<label class="label label-success">Auditee bersedia menandatangani Berita Acara Tidak Setuju</label><br>
														@else 
															<label class="label label-danger">Auditee tidak bersedia menandatangani Berita Acara Tidak Setuju</label><br>
														@endif
														<br>
														<br>
														<div class="file-loading">
											            	<input id="siss-{{ $sistem->id }}" name="fokus_audit[2][detail][{{$k}}][lampirans]" data-id="{{ $sistem->id }}" type="file" class="form-control lampirs" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/jpeg,image/gif,image/png,application/pdf">
											            </div>
													@endif
												</td>
											</tr>
											<tr class="detail-sistem-end-{{$k}} detail-sistem-{{$k}}">
												<td style="">Tanggal Tindak Lanjut</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													{{ DateToString($sistem->tgl) }}
												</td>
											</tr>
											<tr class="detail-sistem-end-{{$k}} detail-sistem-{{$k}}" style="border-bottom: 2px solid black;">
												<td style="">Lampiran</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="2"  class="field">
													<div class="file-loading">
										            	<input id="sis-{{ $sistem->id }}" name="fokus_audit[2][detail][{{$k}}][lampiran]" data-id="{{ $sistem->id }}" type="file" class="form-control lampir" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/jpeg,image/gif,image/png,application/pdf">
										            </div>
												</td>
											</tr>
											@php 
												$k++;
											@endphp
										@endforeach
									</tbody>
								</table>
								<div class="form-group col-md-12">
									<div class="text-right">
										<button type="button" class="btn btn-cancel kembali2">Sebelumnya</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		</form>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
        .table-responsive{
            overflow-x: auto;
            overflow-y: visible !important;
        }

        .table-responsive .dropdown-menu {
	        position: static !important;
	    }

	    div.dropdown-menu.open{
		  max-width: 300px !important;
		  overflow: hidden;
		}
		ul.dropdown-menu.inner{
		  max-width: 300px !important;
		  overflow-y: auto;
		}
		.datepicker.dropdown-menu {
		    z-index: 9999 !important;
		}
		.input-group.file-caption-main{
            display: none;
        }
    </style>
@endpush

@push('scripts')
    <script>
    	$(document).on('click', '.page1', function (e){
    		$('#a-tab').removeClass('active');
    		$('#a-tab').closest('li').removeClass('active');
    		$('#b-tab').addClass('active');
    		$('#b-tab').closest('li').addClass('active');
    		$('#a').removeClass('active');
    		$('#b').addClass('active');
		});

		$(document).on('click', '.kembali1', function (e){
    		$('#a-tab').addClass('active');
    		$('#a-tab').closest('li').addClass('active');
    		$('#b-tab').removeClass('active');
    		$('#b-tab').closest('li').removeClass('active');
    		$('#a').addClass('active');
    		$('#b').removeClass('active');
		});

		$(document).on('click', '.page2', function (e){
    		$('#b-tab').removeClass('active');
    		$('#b-tab').closest('li').removeClass('active');
    		$('#c-tab').addClass('active');
    		$('#c-tab').closest('li').addClass('active');
    		$('#b').removeClass('active');
    		$('#c').addClass('active');
		});

		$(document).on('click', '.kembali2', function (e){
    		$('#b-tab').addClass('active');
    		$('#b-tab').closest('li').addClass('active');
    		$('#c-tab').removeClass('active');
    		$('#c-tab').closest('li').removeClass('active');
    		$('#b').addClass('active');
    		$('#c').removeClass('active');
		});

        $('.cari-operasional').each(function(){
            var id = $(this).data("id");
            var op_id = $(this).data("val");
            var cek = $(this).data("cari");
            var val = $(this).val();
            $.ajax({
				url: '{{ url('ajax/option/get-kriteria-temuans') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					id: cek
				},
			})
			.done(function(response) {
				$('.kriteria_operasional_'+id).html(response[1]);
			})
			.fail(function() {
				console.log("error");
			});

			var detail = id;
			var data = $('select[name="fokus_audit[0][detail]['+detail+'][tanggapan]"] option:selected').val();
			if(data == 0){
				$('.tanggapan_operasional-'+detail).hide();
				$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", true);
				$('input[name="fokus_audit[0][detail]['+detail+'][tanggal]"]').prop("disabled", false);
			}else{
				$('.tanggapan_operasional-'+detail).show();
				$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", false);
				$('input[name="fokus_audit[0][detail]['+detail+'][tanggal]"]').val('');
				$('input[name="fokus_audit[0][detail]['+detail+'][tanggal]"]').prop("disabled", true);
			}
            $.ajax({
                url: '{{ url('ajax/option/get-kka') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: op_id
                },
            }).done(function(response) {
            	if(response.length > 0){
	            	$('#op-'+op_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreview: JSON.parse(response[0].replace(/&quot;/g,'"')),
						initialPreviewAsData: true,
						initialPreviewConfig: JSON.parse(response[1].replace(/&quot;/g,'"')),
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}else{
            		$('#op-'+op_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreviewAsData: true,
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						dropZoneTitle: 'Tidak ada data',
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}
				$('.kv-file-upload').prop('disabled', false);
				$('.kv-file-zoom').prop('disabled', false);
            }).fail(function() {
                console.log("error");
            });

            $.ajax({
                url: '{{ url('ajax/option/get-ba') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: op_id
                },
            }).done(function(response) {
            	if(response.length > 0){
	            	$('#ops-'+op_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreview: JSON.parse(response[0].replace(/&quot;/g,'"')),
						initialPreviewAsData: true,
						initialPreviewConfig: JSON.parse(response[1].replace(/&quot;/g,'"')),
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}else{
            		$('#ops-'+op_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreviewAsData: true,
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						dropZoneTitle: 'Tidak ada data',
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}
				$('.kv-file-upload').prop('disabled', false);
				$('.kv-file-zoom').prop('disabled', false);
            }).fail(function() {
                console.log("error");
            });
        });

        $('.cari-keuangan').each(function(){
            var id = $(this).data("id");
            var keu_id = $(this).data("val");
            var cek = $(this).data("cari");
            var val = $(this).val();
			var detail = id;
			$.ajax({
				url: '{{ url('ajax/option/get-kriteria-temuans') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					id: cek
				},
			})
			.done(function(response) {
				$('.kriteria_keuangan_'+id).html(response[1]);
			})
			.fail(function() {
				console.log("error");
			});

			var data = $('select[name="fokus_audit[1][detail]['+detail+'][tanggapan]"] option:selected').val();
			if(data == 0){
				$('.tanggapan_keuangan-'+detail).hide();
				$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", true);
				$('input[name="fokus_audit[1][detail]['+detail+'][tanggal]"]').prop("disabled", false);
			}else{
				$('.tanggapan_keuangan-'+detail).show();
				$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", false);
				$('input[name="fokus_audit[1][detail]['+detail+'][tanggal]"]').val('');
				$('input[name="fokus_audit[1][detail]['+detail+'][tanggal]"]').prop("disabled", true);
			}
			$.ajax({
                url: '{{ url('ajax/option/get-kka') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: keu_id
                },
            }).done(function(response) {
            	if(response.length > 0){
	            	$('#keu-'+keu_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreview: JSON.parse(response[0].replace(/&quot;/g,'"')),
						initialPreviewAsData: true,
						initialPreviewConfig: JSON.parse(response[1].replace(/&quot;/g,'"')),
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}else{
            		$('#keu-'+keu_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreviewAsData: true,
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						dropZoneTitle: 'Tidak ada data',
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}
				$('.kv-file-upload').prop('disabled', false);
				$('.kv-file-zoom').prop('disabled', false);
            }).fail(function() {
                console.log("error");
            });

            $.ajax({
                url: '{{ url('ajax/option/get-ba') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: keu_id
                },
            }).done(function(response) {
            	if(response.length > 0){
	            	$('#keus-'+keu_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreview: JSON.parse(response[0].replace(/&quot;/g,'"')),
						initialPreviewAsData: true,
						initialPreviewConfig: JSON.parse(response[1].replace(/&quot;/g,'"')),
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}else{
            		$('#keus-'+keu_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreviewAsData: true,
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						dropZoneTitle: 'Tidak ada data',
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}
				$('.kv-file-upload').prop('disabled', false);
				$('.kv-file-zoom').prop('disabled', false);
            }).fail(function() {
                console.log("error");
            });
        });

        $('.cari-sistem').each(function(){
            var id = $(this).data("id");
            var sis_id = $(this).data("val");
            var cek = $(this).data("cari");
            var val = $(this).val();

            $.ajax({
				url: '{{ url('ajax/option/get-kriteria-temuans') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					id: cek
				},
			})
			.done(function(response) {
				$('.kriteria_sistem_'+id).html(response[1]);
			})
			.fail(function() {
				console.log("error");
			});

			var detail = id;
			var data = $('select[name="fokus_audit[2][detail]['+detail+'][tanggapan]"] option:selected').val();
			if(data == 0){
				$('.tanggapan_sistem-'+detail).hide();
				$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", true);
				$('input[name="fokus_audit[2][detail]['+detail+'][tanggal]"]').prop("disabled", false);
			}else{
				$('.tanggapan_sistem-'+detail).show();
				$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", false);
				$('input[name="fokus_audit[2][detail]['+detail+'][tanggal]"]').val('');
				$('input[name="fokus_audit[2][detail]['+detail+'][tanggal]"]').prop("disabled", true);
			}

			$.ajax({
                url: '{{ url('ajax/option/get-kka') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: sis_id
                },
            }).done(function(response) {
            	if(response.length > 0){
	            	$('#sis-'+sis_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreview: JSON.parse(response[0].replace(/&quot;/g,'"')),
						initialPreviewAsData: true,
						initialPreviewConfig: JSON.parse(response[1].replace(/&quot;/g,'"')),
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}else{
            		$('#sis-'+sis_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreviewAsData: true,
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						dropZoneTitle: 'Tidak ada data',
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}
				$('.kv-file-upload').prop('disabled', false);
				$('.kv-file-zoom').prop('disabled', false);
            }).fail(function() {
                console.log("error");
            });

            $.ajax({
                url: '{{ url('ajax/option/get-ba') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: sis_id
                },
            }).done(function(response) {
            	if(response.length > 0){
	            	$('#siss-'+sis_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreview: JSON.parse(response[0].replace(/&quot;/g,'"')),
						initialPreviewAsData: true,
						initialPreviewConfig: JSON.parse(response[1].replace(/&quot;/g,'"')),
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}else{
            		$('#siss-'+sis_id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreviewAsData: true,
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						dropZoneTitle: 'Tidak ada data',
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}
				$('.kv-file-upload').prop('disabled', false);
				$('.kv-file-zoom').prop('disabled', false);
            }).fail(function() {
                console.log("error");
            });
        });

		$('.pilihan').on('change', function(){
			var id = $(this).data("id");
			var detail = $(this).data("detail");
			var data = $('select[name="fokus_audit['+id+'][detail]['+detail+'][tanggapan]"] option:selected').val();
			if(data == 0){
				if(id == 0){
					$('.tanggapan_operasional-'+detail).hide();
					$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", true);
				}else if(id == 1){
					$('.tanggapan_keuangan-'+detail).hide();
					$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", true);
				}else{
					$('.tanggapan_sistem-'+detail).hide();
					$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", true);
				}
				$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').prop("disabled", false);
			}else{
				if(id == 0){
					$('.tanggapan_operasional-'+detail).show();
					$('textarea[name="fokus_audit[0][detail]['+detail+'][catatan]"]').prop("disabled", false);
				}else if(id == 1){
					$('.tanggapan_keuangan-'+detail).show();
					$('textarea[name="fokus_audit[1][detail]['+detail+'][catatan]"]').prop("disabled", false);
				}else{
					$('.tanggapan_sistem-'+detail).show();
					$('textarea[name="fokus_audit[2][detail]['+detail+'][catatan]"]').prop("disabled", false);
				}
				$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').val('');
				$('input[name="fokus_audit['+id+'][detail]['+detail+'][tanggal]"]').prop("disabled", true);
			}
		});
    	$('#myTab li:first-child a').tab('show')
    </script>
    @yield('js-extra')
@endpush

