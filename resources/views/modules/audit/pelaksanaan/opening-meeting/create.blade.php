@extends('layouts.form')
@section('title', 'Buat Opening Meeting')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
    <div class="panel-body" style="padding-bottom: 0px;">
    	<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
			@csrf
			<div class="loading dimmer padder-v" style="display: none;">
			    <div class="loader"></div>
			</div>
			<input type="hidden" name="id" value="{{ $record->id }}">
			<input type="hidden" name="tahun" value="{{ $record->penugasanaudit->rencanadetail->rencanaaudit->tahun }}">
			<input type="hidden" name="status" value="0">
			<input type="hidden" name="tipe" value="0">
			<div class="form-row">
				<div class="form-group col-md-12">
					<input type="hidden" name="program_id" value="{{ $record->id }}">
					<div class="form-row" class="form-row" style="margin-left: -15px;">
						<div class="form-group col-md-12">
							<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
								<tr>
									<td style="width:200px;border: 1px #ffffff;border: 1px #ffffff;">Tahun</td>
									<td style="width:2px;border: 1px #ffffff;">:</td>
									<td style="text-align:left;border: 1px #ffffff;" class="field">
										{{ $record->penugasanaudit->rencanadetail->rencanaaudit->tahun }}
										<input type="hidden" name="tahun" value="{{ $record->penugasanaudit->rencanadetail->rencanaaudit->tahun }}">
									</td>
								</tr>
								<tr>
									<td style="border: 1px #ffffff;">Waktu Pelaksanaan</td>
									<td style="border: 1px #ffffff;">:</td>
									<td style="text-align:left;border: 1px #ffffff;" class="field">
										{{ DateToString($record->detailpelaksanaan->first()->tgl).' - '.DateToString($record->detailpelaksanaan->last()->tgl)}}
									</td>
								</tr>
								<tr>
									<td style="border: 1px #ffffff;">Tipe</td>
									<td style="border: 1px #ffffff;">:</td>
									<td style="text-align:left;border: 1px #ffffff;" class="field">
										@if($record->penugasanaudit->rencanadetail->tipe == 0)
											<span class="label label-primary">Audit</span>
										@elseif($record->penugasanaudit->rencanadetail->tipe == 1)
											<span class="label label-success">Kegiatan Konsultasi</span>
										@else
											<span class="label label-info">Kegiatan Audit Lain - Lain</span>
										@endif
									</td>
								</tr>
								<tr>
									<td style="border: 1px #ffffff;">Kategori</td>
									<td style="border: 1px #ffffff;">:</td>
									<td style="text-align:left;border: 1px #ffffff;" class="field">
										@if($record->penugasanaudit->rencanadetail->tipe == 0)
											@if($record->penugasanaudit->rencanadetail->tipe_object == 0)
												<span class="label label-success">Business Unit (BU)</span>
											@elseif($record->penugasanaudit->rencanadetail->tipe_object == 1)
												<span class="label label-info">Corporate Office (CO)</span>
											@elseif($record->penugasanaudit->rencanadetail->tipe_object == 2)
												<span class="label label-default">Project</span>
											@else
												<span class="label label-primary">Anak Perusahaan</span>
											@endif
										@elseif($record->penugasanaudit->rencanadetail->tipe == 1)
											<span class="label label-warning">{{ $record->penugasanaudit->rencanadetail->konsultasi->nama }}</span>
										@else
											<span class="label label-default">{{ $record->penugasanaudit->rencanadetail->lain->nama }}</span>
										@endif
									</td>
								</tr>
								<tr>
									<td style="width: 10%;border: 1px #ffffff;">Objek Audit</td>
									<td style="width: 1%;border: 1px #ffffff;">:</td>
									<td style="text-align: left;border: 1px #ffffff;" class="field">
										{{ object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id)}}
									</td>
								</tr>
							</table>
						</div>
					</div>
			        <div class="form-row">
					    <div class="form-group col-md-6 field" style="padding-right: 10px;">
					      <label for="inputEmail4">Tanggal</label>
			           	  <input type="text" name="tanggal" class="form-control tanggal" placeholder="Tanggal" required="" value="{{ \Carbon\Carbon::parse($record->detailpelaksanaan->first()->tgl)->format('d-m-Y') }}">
					    </div>
					    <div class="form-group col-md-6 field" style="padding-left: 10px;">
					       <label for="inputPassword4">Tempat</label>
					       <input type="text" name="tempat" class="form-control" placeholder="Tempat" required="">
					    </div>
					 </div>
					 <div class="form-row">
					    <div class="form-group col-md-6 field" style="padding-right: 10px;">
					      <label for="inputPassword4">Waktu Mulai</label>
			            	<input type="text" name="mulai" class="form-control waktu" placeholder="Waktu Mulai" required="" value="{{ $record->detailpelaksanaan->first()->detailpelaksanaan->first()->mulai }}">
					    </div>
					    <div class="form-group col-md-6 field" style="padding-left: 10px;"> 
					      <label for="inputEmail4">Waktu Selesai</label>
			            	<input type="text" name="selesai" class="form-control waktu" placeholder="Waktu Selesai" required="">
					    </div>
					 </div>
					 <table id="peserta" class="table table-bordered m-t-none" style="font-size: 12px;">
					 	<thead>
					 		<tr>
					 			<td style="text-align: left;" colspan="3">Peserta</td>
					 		</tr>
					 	</thead>
					 	<tbody class="peserta">
					 		<tr class="data-peserta-0" data-id="0">
					 			<td style="width: 20px;">
					 				<label style="margin-top: 7px;" class="numbor-0">1</label>
					 			</td>
					 			<td style="border-bottom: 1px solid rgba(34,36,38,.1);" class="field">
					 				<select class="selectpicker form-control add-peserta" name="detail[0][peserta]" data-size="5" data-style="btn-default" data-live-search="true" onChange="checkSame(this)">
					 					<option value="">(Pilih Salah Satu)</option>
					 					@foreach(App\Models\Auths\User::where('id', '!=', 1)->get() as $user)
					 					<option value="{{ $user->id }}">{{ $user->name }}</option>
					 					@endforeach
					 				</select>
					 			</td>
					 			<td style="width: 80px;border-bottom: 1px solid rgba(34,36,38,.1);"><button class="btn btn-sm btn-success tambah_peserta" type="button" style="border-radius: 20px;margin-top: 2px;"><i class="fa fa-plus"></i></button></td>
					 		</tr>
					 	</tbody>
					 </table>
					 <table id="peserta_lainnya" class="table table-bordered m-t-none" style="font-size: 12px;">
					 	<thead>
					 		<tr>
					 			<td style="text-align: left;" colspan="3">Peserta Lainnya</td>
					 		</tr>
					 	</thead>
					 	<tbody class="peserta_lainnya">
					 		<tr class="data-peserta_lainnya-0" data-id="0">
					 			<td style="width: 20px;">
					 				<label style="margin-top: 7px;" class="numbor-0">1</label>
					 			</td>
					 			<td style="border-bottom: 1px solid rgba(34,36,38,.1);" class="field">
					 				<input type="text" name="details[0][peserta_lainnya]" class="form-control" placeholder="Peserta Lainnya" required="">
					 			</td>
					 			<td style="width: 80px;border-bottom: 1px solid rgba(34,36,38,.1);"><button class="btn btn-sm btn-success tambah_peserta_lainnya" type="button" style="border-radius: 20px;margin-top: 2px;"><i class="fa fa-plus"></i></button></td>
					 		</tr>
					 	</tbody>
					 </table>
					 <div class="form-group">
					 	<label for="inputPassword4">Lampiran</label>
					 	<div class="field">
						    <div class="file-loading">
								<input id="datafile-0"  name="lampiran" type="file" class="file form-control cfile zfile previewupload" 
								data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf">
							</div>
					 	</div>
					 </div>
					 <div class="form-group">
					 	<label for="inputPassword4">Daftar Hadir</label>
					 	<div class="field">
						    <div class="file-loading">
								<input id="datafile-0"  name="hadir" type="file" class="file form-control cfile zfile previewupload" 
								data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf">
							</div>
					 	</div>
					 </div>
					 <div class="form-group">
					 	<label for="inputPassword4">Foto Meeting</label>
					 	<div class="field">
						    <div class="file-loading">
								<input id="datafile-0"  name="foto[]" type="file" class="file form-control cfile zfile previewupload" 
								data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="image/*" multiple>
							</div>
					 	</div>
					 </div>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-right">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
						<button type="button" class="btn btn-primary save as drafting">Save As Draft</button>
						<button type="button" class="btn btn-simpan save as page">Submit</button>
					</div>
				</div>
			</div>
		</form>
    </div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script> 
    	var d = new Date();
    	var full = d.getFullYear();
    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
    	var last = new Date(lastDate);
    	if($('input[name=tahun]').val() > full){
    		var min = minDates;
    	}else{
    		var min = d;
    	}

    	$('.tanggal').datepicker({
    		orientation: "bottom left",
    		todayHighlight: true,
    		autoclose:true,
    		format: 'dd-mm-yyyy',
    		yearRange: '-0:+1',
    		startDate: minDates,
    		endDate: last,
    		hideIfNoPrevNext: true,
        });

    	$('.waktu').clockpicker({
            autoclose:true,
            'default': 'now',
        });
    	$(document).on('click', '.tambah_peserta', function(e){
			var rowCount = $('#peserta > tbody > tr').length;
			var c = rowCount-1;
			var html = `
					<tr class="data-container-`+(c+1)+`" data-id=`+(c+1)+`>
						<td style="width: 20px;">
							<label style="margin-top: 7px;" class="numbuur-`+(c+1)+`">`+(c+2)+`</label>
						</td>
						<td style="border-bottom: 1px solid rgba(34,36,38,.1);" class="field">
							<select class="selectpicker form-control add-peserta" name="detail[`+(c+1)+`][peserta]" data-size="5" data-style="btn-default" data-live-search="true" onChange="checkSame(this)">
								<option value="">(Pilih Salah Satu)</option>
			 					@foreach(App\Models\Auths\User::where('id', '!=', 1)->get() as $user)
			 					<option value="{{ $user->id }}">{{ $user->name }}</option>
			 					@endforeach
			 				</select>
						</td>
						<td style="text-align:center;border-bottom: 1px solid rgba(34,36,38,.1);">
							<button class="btn btn-sm btn-danger hapus_peserta" type="button" style="border-radius:20px;" data-id=`+(c+1)+`><i class="fa fa-remove"></i></button>
						</td>
					</tr>
				`;

				$('.peserta').append(html).children(':last').hide().fadeIn(1000);
		        $('.selectpicker').selectpicker();
		        $.each($('select.add-peserta'), function (index, el) {
	    			if($(el).val() != "")
	    			{
	    				$('select[name="detail['+(c+1)+'][peserta]"]').find('option[value="'+$(el).val()+'"]').hide();
	    				$('select[name="detail['+(c+1)+'][peserta]"]').selectpicker('refresh');
	    			}
	    		});
		});

		$(document).on('click', '.hapus_peserta', function (e){
			var id = $(this).data('id');
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#peserta');
			var rows = table.find('tbody tr');

			var selected = $('select[name="detail['+id+'][peserta]"]').children("option:selected").val();
			$.each($('select.add-pof'), function (index, el) {
				var idx = $(this).data('id')
				$('select[name="detail['+idx+'][peserta]"]').find('option[value="'+selected+'"]').show();
                $('select[name="detail['+idx+'][peserta]"]').selectpicker('refresh');
            });

			$.each(rows, function(key, value){
				console.log($(this).data("id"))
				table.find('.numbuur-'+$(this).data("id")).html(key+1);
			});
		});

		function checkSame(element)
        {
            $(element).on('change', function () {
                var element = $(this)
                $.each($('select[name^="detail"][name$="[peserta]"'), function (index, one) {
                    if(!$(element).is($(one)))
                    {
                        $(one).find('option').show();
                        $(one).find('option[value="'+$(element).val()+'"]').hide();
                        $(one).selectpicker('refresh');
                    }
                });
            })
        }

        $(document).on('click', '.tambah_peserta_lainnya', function(e){
			var rowCount = $('#peserta_lainnya > tbody > tr').length;
			var c = rowCount-1;
			var html = `
					<tr class="data-container-`+(c+1)+`" data-id=`+(c+1)+`>
						<td style="width: 20px;">
							<label style="margin-top: 7px;" class="numbuur-`+(c+1)+`">`+(c+2)+`</label>
						</td>
						<td style="border-bottom: 1px solid rgba(34,36,38,.1);" class="field">
						    <input type="text" name="details[`+(c+1)+`][peserta_lainnya]" class="form-control" placeholder="Peserta Lainnya" required="">
						</td>
						<td style="text-align:center;border-bottom: 1px solid rgba(34,36,38,.1);">
							<button class="btn btn-sm btn-danger hapus_peserta_lainnya" type="button" style="border-radius:20px;" data-id=`+(c+1)+`><i class="fa fa-remove"></i></button>
						</td>
					</tr>
				`;

				$('.peserta_lainnya').append(html).children(':last').hide().fadeIn(1000);
		        $('.selectpicker').selectpicker();
		        $.each($('select.add-peserta_lainnya'), function (index, el) {
	    			if($(el).val() != "")
	    			{
	    				$('select[name="details['+(c+1)+'][peserta_lainnya]"]').find('option[value="'+$(el).val()+'"]').hide();
	    				$('select[name="details['+(c+1)+'][peserta_lainnya]"]').selectpicker('refresh');
	    			}
	    		});
		});

		$(document).on('click', '.hapus_peserta_lainnya', function (e){
			var id = $(this).data('id');
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#peserta_lainnya');
			var rows = table.find('tbody tr');

			var selected = $('select[name="details['+id+'][peserta_lainnya]"]').children("option:selected").val();
			$.each($('select.add-pof'), function (index, el) {
				var idx = $(this).data('id')
				$('select[name="details['+idx+'][peserta_lainnya]"]').find('option[value="'+selected+'"]').show();
                $('select[name="details['+idx+'][peserta_lainnya]"]').selectpicker('refresh');
            });

			$.each(rows, function(key, value){
				console.log($(this).data("id"))
				table.find('.numbuur-'+$(this).data("id")).html(key+1);
			});
		});

		function checkSame(element)
        {
            $(element).on('change', function () {
                var element = $(this)
                $.each($('select[name^="details"][name$="[peserta_lainnya]"'), function (index, one) {
                    if(!$(element).is($(one)))
                    {
                        $(one).find('option').show();
                        $(one).find('option[value="'+$(element).val()+'"]').hide();
                        $(one).selectpicker('refresh');
                    }
                });
            })
        }
    </script>
    @yield('js-extra')
@endpush
