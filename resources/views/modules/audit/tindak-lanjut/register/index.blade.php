@extends('layouts.list-tindak-lanjut')
@section('title', 'Register Tindak Lanjut')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('filters')
    <div class="form-group">
        <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
        <label class="control-label sr-only" for="filter-tahun">Tahun</label>
	    <input type="text" class="form-control filter-control tahun" name="filter[tahun]" data-post="tahun" placeholder="Tahun">
	    <label class="control-label sr-only" for="filter-tipe">Tipe</label>
	    <select class="select selectpicker filter-control" name="filter[tipe]" data-post="tipe" data-style="btn-default">
	        <option style="font-size: 12px;" value="">Tipe</option>
	        <option style="font-size: 12px;" value="1">Audit Reguler</option>
	        <option style="font-size: 12px;" value="2">Audit Khusus</option>
	    </select>
	    <label class="control-label sr-only" for="filter-kategori">Kategori</label>
	    <select class="select selectpicker filter-control kategori" name="filter[kategori]" data-post="kategori" data-style="btn-default">
	           <option style="font-size: 12px;" value="">Kategori</option>
	           <option style="font-size: 12px;" value="1">Business Unit (BU)</option>
	           <option style="font-size: 12px;" value="2">Corporate Office (CO)</option>
	           <option style="font-size: 12px;" value="3">Project</option>
	           <option style="font-size: 12px;" value="4">Anak Perusahaan</option>
	    </select>
	    <label class="control-label sr-only" for="filter-object_id">Objek Audit</label>
	    <select class="select selectpicker filter-control show-tick object" name="filter[object_id]" data-post="object_id" data-size="3" data-style="btn-default" data-live-search="true" title="Objek Audit">
		</select>
    </div>
@endsection
@push('scripts')
    <script>
    	$('.tahun').datepicker({
            format: "yyyy",
		    viewMode: "years", 
		    minViewMode: "years",
            orientation: "auto",
            autoclose:true
        });

    	$(document).on('click', '.buat', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/buat';
            window.location = url;
		});

		$(document).on('click', '.ubah', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/ubah';
            window.location = url;
		});
		
		$(document).on('click', '.udata', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/udata';
            window.location = url;
		});

		$(document).on('click', '.udata-svp', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/udataSvp';
            window.location = url;
		});

		$(document).on('click', '.detil-svp', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/detil';
            window.location = url;
		});

		$(document).on('click', '.detil-tl', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/detil';
            window.location = url;
		});

		$(document).on('click', '.cetak-disposisi.button', function(e){
            var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/cetakSuratDisposisi';
            window.open(url, '_blank');
        });

        $(document).on('click', '.cetak-kka.button', function(e){
            var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/cetakKka';
            window.open(url, '_blank');
        });

    	$('.kategori').on('change', function(){
			$.ajax({
				url: '{{ url('ajax/option/get-kategoris') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					kategori: this.value
				},
			}).done(function(response) {
				$('select[name="filter[object_id]"]').html(response);
				$('select[name="filter[object_id]"]').selectpicker("refresh");
			})
			.fail(function() {
				console.log("error");
			});
		});
    </script>
    @yield('js-extra')
@endpush