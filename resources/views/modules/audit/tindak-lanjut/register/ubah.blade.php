@extends('layouts.form')
@section('title', 'Ubah Register Tindak Lanjut')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
    <div class="panel-body" style="padding-bottom: 0px;">
		<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
			<div class="panel-body" style="padding-bottom: 0px;">
				<div class="form-row">
					@method('PATCH')
					@csrf
					<input type="hidden" name="id" value="{{ $record->id }}">
					<input type="hidden" name="status" value="0">
					<table class="table table-bordered m-t-none" style="font-size: 12px;border: #ffffff;">
						<tbody>
							<tr>
								<td style="width: 600px;border-right: #ffffff;border-top: #ffffff;">&nbsp;</td>
								<td style="width: 600px;border-right: #ffffff;border-top: #ffffff;">REGISTER TINDAK LANJUT</td>
								<td style="border-right: #ffffff;border-top: #ffffff;">&nbsp;</td>
								<td style="border-right: #ffffff;border-top: #ffffff;">&nbsp;</td>
								<td style="border-right: #ffffff;border-top: #ffffff;">&nbsp;</td>
							</tr>
							<tr>
								<td style="border-right: #ffffff;border-top: #ffffff;">&nbsp;</td>
								<td style="border-top: #ffffff;">HASIL AUDIT INTERNAL</td>
								<td style="text-align: center;" colspan="2">Form IA 04</td>
								<td style="text-align: center;border-top: #ffffff;" colspan="2"></td>
							</tr>
							<tr>
								<td style="border-right: #ffffff;border-top: #ffffff;">&nbsp;</td>
								<td style="border-top: #ffffff;">PADA : {{ DateToString(date("d-m-Y")) }}</td>
								<td style="width: 100px;">Edisi : Mei 2019</td>
								<td style="width: 100px;">Revisi 0</td>
								<td style="width: 100px;border-top: #ffffff;"></td>
							</tr>
							<tr>
								<td style="border-right: #ffffff;border-top: #ffffff;">&nbsp;</td>
								<td style="border-top: #ffffff;border-right: #ffffff;">NO AB : {{ getAb($record->kka->program->penugasanaudit->rencanadetail->tipe_object, $record->kka->program->penugasanaudit->rencanadetail->object_id) }}</td>
								<td style="border-right: #ffffff;" colspan="2" rowspan="2">&nbsp;</td>
								<td style="border-top: #ffffff;">&nbsp;</td>
							</tr>
							<tr>
								<td style="border-right: #ffffff;border-top: #ffffff;">&nbsp;</td>
								<td style="text-align: left;border-top: #ffffff;border-right: #ffffff;">TANGGAL AUDIT : {{ DateToString($record->kka->program->detailpelaksanaan->first()->tgl).' - '.DateToString($record->kka->program->detailpelaksanaan->last()->tgl) }}</td>
								<td style="text-align: left;border-top: #ffffff;"></td>
							</tr>
						</tbody>
					</table>
					<table class="table table-bordered m-t-none" style="font-size: 12px;border-color: #cccccc;">
						<thead>
							<tr>
								<th style="width: 20px;padding-top: 31px;font-weight: bold;background: #f5f5ef;border-color: #cccccc;" rowspan="2">No</th>
								<th style="width: 200px;text-align: center;padding-top: 31px;font-weight: bold;background: #f5f5ef;border-color: #cccccc;" rowspan="2">Uraian Temuan</th>
								<th style="width: 400px;text-align: center;padding-top: 31px;font-weight: bold;background: #f5f5ef;border-color: #cccccc;" rowspan="2">Rekomendasi</th>
								<th style="width: 400px;text-align: center;padding-top: 31px;font-weight: bold;background: #f5f5ef;border-color: #cccccc;" rowspan="2">Tindak Lanjut</th>
								<th style="width: 400px;text-align: center;padding-top: 31px;font-weight: bold;background: #f5f5ef;border-color: #cccccc;" rowspan="2">Lampiran</th>
								@if($record->kka->program->penugasanaudit->rencanadetail->tipe_object == 2)
									<th style="width: 50px;text-align: center;padding-top: 31px;font-weight: bold;background: #f5f5ef;border-color: #cccccc;" rowspan="2">Status SVP</th>
								@endif
								<th style="width: 180px;text-align: center;font-weight: bold;background: #f5f5ef;border-color: #cccccc;" colspan="3">Status</th>
							</tr>
							<tr>
								<th style="text-align: center;font-weight: bold;background: #f5f5ef;border-color: #cccccc;">Sudah</th>
								<th style="text-align: center;font-weight: bold;background: #f5f5ef;border-color: #cccccc;">Belum</th>
								<th style="text-align: center;font-weight: bold;background: #f5f5ef;border-color: #cccccc;">Dalam Proses</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="font-weight: bold;border-color: #cccccc;padding-left: 20px;">A.</td>
								<td style="text-align: left;font-weight: bold;border-color: #cccccc;" @if($record->kka->program->penugasanaudit->rencanadetail->tipe_object == 2) colspan="8" @else colspan="7" @endif>Aspek Operasional</td>
							</tr>
							@php
								$i=1;
							@endphp
							@foreach($record->kka->detaildraft->where('tipe', 0) as $key => $operasional)
								@php
									$row = count($operasional->rekomendasi->where('status_monitoring', 0));
								@endphp
								@foreach($operasional->rekomendasi->where('status_monitoring', 0) as $a => $operasionals)
									@php 
										$cek = count($operasionals->register->detaillampiran);
									@endphp
									@if($a == 0)
										<tr>
											<td style="text-align: center;width: 20px;" rowspan="{{$row}}">{{$i}}</td>
											<td style="width: 200px;text-align: justify;" rowspan="{{$row}}">{{getstandarisasi($operasional->standarisasi_id)}}</td>
											<td style="width: 400px;text-align: justify;">{{ $operasionals->rekomendasi }}</td>
											<td style="width: 400px;">
												<textarea class="form-control" name="data[operasional][{{$operasionals->id}}][tl]" id="exampleFormControlTextarea1" placeholder="Tindak Lanjut" rows="3">{{ $operasionals->register->tl }}</textarea>
											</td>
											<td style="width: 400px;" class="cari-operasional" data-id="{{ $operasionals->id }} ">
												@if($operasionals->register->status == 0)
													<input id="operasional-input-{{$operasionals->id}}" name="data[operasional][{{$operasionals->id}}][lampiran]" type="file" class="file form-control" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf">
												@else 
													<input id="operasional-show-{{$operasionals->id}}" name="data[operasional][{{$operasionals->id}}][lampiran]" type="file" class="form-control" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf" readonly>
												@endif
											</td>
											@if($record->kka->program->penugasanaudit->rencanadetail->tipe_object == 2)
												<td style="width: 50px;" class="cari-operasional" data-id="{{ $operasionals->id }} ">
													<input type="checkbox" name="data[operasional][{{$operasionals->id}}][svp]" data-val="operasional" data-cek="nol" data-toggle="toggle" data-on="Ya" data-off="Tidak" data-onstyle="info" @if($operasionals->register->status != 1) disabled @endif class="toggle mytoggle" @if($operasionals->register->status >= 2) checked @endif disabled>
												</td>
											@endif
											<td style="width: 50px;">
												<input type="checkbox" name="data[operasional][{{$operasionals->id}}][pilihan]" data-val="operasional" data-cek="satu" data-toggle="toggle" data-id="{{$a}}" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" disabled class="toggle mytoggle operasional-one-{{$a}}" value="4" @if($operasionals->register->stage == 4) checked @endif>
											</td>
											<td style="width: 50px;">
												<input type="checkbox" name="data[operasional][{{$operasionals->id}}][pilihan]" data-val="operasional" data-cek="dua" data-toggle="toggle" data-id="{{$a}}" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" disabled class="toggle mytoggle operasional-two-{{$a}}" value="2" @if($operasionals->register->stage == 2 || $operasionals->register->stage == 0) checked @endif>
											</td>
											<td style="width: 50px;">
												<input type="checkbox" name="data[operasional][{{$operasionals->id}}][pilihan]" data-val="operasional" data-cek="tiga" data-toggle="toggle" data-id="{{$a}}" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" disabled class="toggle mytoggle operasional-three-{{$a}}" value="3" @if($operasionals->register->stage == 3) checked @endif>
											</td>
										</tr>
									@else 
										<tr>
											<td style="width: 400px;">{{ $operasionals->rekomendasi }}</td>
											<td style="width: 400px;">
												<textarea class="form-control" name="data[operasional][{{$operasionals->id}}][tl]" id="exampleFormControlTextarea1" placeholder="Tindak Lanjut" rows="3">{{ $operasionals->register->tl }}</textarea>
											</td>
											<td style="width: 400px;" class="cari-operasional" data-id="{{ $operasionals->id }} ">
												@if($operasionals->register->status == 0)
													<input id="operasional-input-{{$operasionals->id}}" name="data[operasional][{{$operasionals->id}}][lampiran]" type="file" class="file form-control" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf">
												@else 
													<input id="operasional-show-{{$operasionals->id}}" name="data[operasional][{{$operasionals->id}}][lampiran]" type="file" class="form-control" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf" readonly>
												@endif
											</td>
											@if($record->kka->program->penugasanaudit->rencanadetail->tipe_object == 2)
												<td style="width: 50px;" class="cari-operasional" data-id="{{ $operasionals->id }} ">
													<input type="checkbox" name="data[operasional][{{$operasionals->id}}][svp]" data-val="operasional" data-cek="nol" data-toggle="toggle" data-on="Ya" data-off="Tidak" data-onstyle="info" @if($operasionals->register->status != 1) disabled @endif class="toggle mytoggle" @if($operasionals->register->status >= 2) checked @endif disabled>
												</td>
											@endif
											<td style="width: 50px;">
												<input type="checkbox" name="data[operasional][{{$operasionals->id}}][pilihan]" data-val="operasional" data-cek="satu" data-toggle="toggle" data-id="{{$a}}" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" disabled class="toggle mytoggle operasional-one-{{$a}}" value="4" @if($operasionals->register->stage == 4) checked @endif>
											</td>
											<td style="width: 50px;">
												<input type="checkbox" name="data[operasional][{{$operasionals->id}}][pilihan]" data-val="operasional" data-cek="dua" data-toggle="toggle" data-id="{{$a}}" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" disabled class="toggle mytoggle operasional-two-{{$a}}" value="2" @if($operasionals->register->stage == 2 || $operasionals->register->stage == 0) checked @endif>
											</td>
											<td style="width: 50px;">
												<input type="checkbox" name="data[operasional][{{$operasionals->id}}][pilihan]" data-val="operasional" data-cek="tiga" data-toggle="toggle" data-id="{{$a}}" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" disabled class="toggle mytoggle operasional-three-{{$a}}" value="3" @if($operasionals->register->stage == 3) checked @endif>
											</td>
										</tr>
									@endif
								@endforeach
								@php 
									$i++;
								@endphp
							@endforeach

							<tr>
								<td style="font-weight: bold;border-color: #cccccc;padding-left: 20px;">B.</td>
								<td style="text-align: left;font-weight: bold;border-color: #cccccc;" @if($record->kka->program->penugasanaudit->rencanadetail->tipe_object == 2) colspan="8" @else colspan="7" @endif>Aspek Keuangan</td>
							</tr>
							@php
								$j=1;
							@endphp
							@foreach($record->kka->detaildraft->where('tipe', 1) as $key => $keuangan)
								@php
									$row = count($keuangan->rekomendasi->where('status_monitoring', 0));
								@endphp
								@foreach($keuangan->rekomendasi->where('status_monitoring', 0) as $a => $keuangans)
									@php 
										$cek = count($keuangans->register->detaillampiran);
									@endphp
									@if($a == 0)
										<tr>
											<td style="text-align: center;width: 20px;" rowspan="{{$row}}">{{$j}}</td>
											<td style="width: 200px;text-align:justify;" rowspan="{{$row}}">{{getstandarisasi($keuangan->standarisasi_id)}}</td>
											<td style="width: 400px;text-align:justify;">{{ $keuangans->rekomendasi }}</td>
											<td style="width: 400px;">
												<textarea class="form-control" name="data[keuangan][{{$keuangans->id}}][tl]" id="exampleFormControlTextarea1" placeholder="Tindak Lanjut" rows="3">{{ $keuangans->register->tl }}</textarea>
											</td>
											<td style="width: 400px;" class="cari-keuangan" data-id="{{ $keuangans->id }} ">
												@if($keuangans->register->status == 0)
													<input id="keuangan-input-{{$keuangans->id}}" name="data[keuangan][{{$keuangans->id}}][lampiran]" type="file" class="file form-control" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf">
												@else 
													<input id="keuangan-show-{{$keuangans->id}}" name="data[keuangan][{{$keuangans->id}}][lampiran]" type="file" class="form-control" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf" readonly>
												@endif
											</td>
											@if($record->kka->program->penugasanaudit->rencanadetail->tipe_object == 2)
												<td style="width: 50px;" class="cari-keuangan" data-id="{{ $keuangans->id }} ">
													<input type="checkbox" name="data[keuangan][{{$keuangans->id}}][svp]" data-val="keuangan" data-cek="nol" data-toggle="toggle" data-on="Ya" data-off="Tidak" data-onstyle="info" @if($keuangans->register->status != 1) disabled @endif class="toggle mytoggle" @if($keuangans->register->status >= 2) checked @endif disabled>
												</td>
											@endif
											<td style="width: 50px;">
												<input type="checkbox" name="data[keuangan][{{$keuangans->id}}][pilihan]" data-val="keuangan" data-cek="satu" data-toggle="toggle" data-id="{{$a}}" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" disabled class="toggle mytoggle keuangan-one-{{$a}}" value="4" @if($keuangans->register->stage == 4) checked @endif>
											</td>
											<td style="width: 50px;">
												<input type="checkbox" name="data[keuangan][{{$keuangans->id}}][pilihan]" data-val="keuangan" data-cek="dua" data-toggle="toggle" data-id="{{$a}}" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" disabled class="toggle mytoggle keuangan-two-{{$a}}" value="2" @if($keuangans->register->stage == 2 || $keuangans->register->stage == 0) checked @endif>
											</td>
											<td style="width: 50px;">
												<input type="checkbox" name="data[keuangan][{{$keuangans->id}}][pilihan]" data-val="keuangan" data-cek="tiga" data-toggle="toggle" data-id="{{$a}}" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" disabled class="toggle mytoggle keuangan-three-{{$a}}" value="3" @if($keuangans->register->stage == 3) checked @endif>
											</td>
										</tr>
									@else 
										<tr>
											<td style="width: 400px;text-align:justify;">{{ $keuangans->rekomendasi }}</td>
											<td style="width: 400px;">
												<textarea class="form-control" name="data[keuangan][{{$keuangans->id}}][tl]" id="exampleFormControlTextarea1" placeholder="Tindak Lanjut" rows="3">{{ $keuangans->register->tl }}</textarea>
											</td>
											<td style="width: 400px;" class="cari-keuangan" data-id="{{ $keuangans->id }} ">
												@if($keuangans->register->status == 0)
													<input id="keuangan-input-{{$keuangans->id}}" name="data[keuangan][{{$keuangans->id}}][lampiran]" type="file" class="file form-control" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf">
												@else 
													<input id="keuangan-show-{{$keuangans->id}}" name="data[keuangan][{{$keuangans->id}}][lampiran]" type="file" class="form-control" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf" readonly>
												@endif
											</td>
											@if($record->kka->program->penugasanaudit->rencanadetail->tipe_object == 2)
												<td style="width: 50px;" class="cari-keuangan" data-id="{{ $keuangans->id }} ">
													<input type="checkbox" name="data[keuangan][{{$keuangans->id}}][svp]" data-val="keuangan" data-cek="nol" data-toggle="toggle" data-on="Ya" data-off="Tidak" data-onstyle="info" @if($keuangans->register->status != 1) disabled @endif class="toggle mytoggle" @if($keuangans->register->status >= 2) checked @endif disabled>
												</td>
											@endif
											<td style="width: 50px;">
												<input type="checkbox" name="data[keuangan][{{$keuangans->id}}][pilihan]" data-val="keuangan" data-cek="satu" data-toggle="toggle" data-id="{{$a}}" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" disabled class="toggle mytoggle keuangan-one-{{$a}}" value="4" @if($keuangans->register->stage == 4) checked @endif>
											</td>
											<td style="width: 50px;">
												<input type="checkbox" name="data[keuangan][{{$keuangans->id}}][pilihan]" data-val="keuangan" data-cek="dua" data-toggle="toggle" data-id="{{$a}}" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" disabled class="toggle mytoggle keuangan-two-{{$a}}" value="2" @if($keuangans->register->stage == 2 || $keuangans->register->stage == 0) checked @endif>
											</td>
											<td style="width: 50px;">
												<input type="checkbox" name="data[keuangan][{{$keuangans->id}}][pilihan]" data-val="keuangan" data-cek="tiga" data-toggle="toggle" data-id="{{$a}}" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" disabled class="toggle mytoggle keuangan-three-{{$a}}" value="3" @if($keuangans->register->stage == 3) checked @endif>
											</td>
										</tr>
									@endif
								@endforeach
								@php 
									$j++;
								@endphp
							@endforeach

							<tr>
								<td style="font-weight: bold;border-color: #cccccc;padding-left: 20px;">C.</td>
								<td style="text-align: left;font-weight: bold;border-color: #cccccc;" @if($record->kka->program->penugasanaudit->rencanadetail->tipe_object == 2) colspan="8" @else colspan="7" @endif>Aspek Sistem</td>
							</tr>
							@php
								$k=1;
							@endphp
							@foreach($record->kka->detaildraft->where('tipe', 2) as $key => $sistem)
								@php
									$row = count($sistem->rekomendasi->where('status_monitoring', 0));
								@endphp
								@foreach($sistem->rekomendasi->where('status_monitoring', 0) as $a => $sistems)
									@php 
										$cek = count($sistems->register->detaillampiran);
									@endphp
									@if($a == 0)
										<tr>
											<td style="text-align: center;width: 20px;" rowspan="{{$row}}">{{$k}}</td>
											<td style="width: 200px;text-align:justify;" rowspan="{{$row}}">{{getstandarisasi($sistem->standarisasi_id)}}</td>
											<td style="width: 400px;text-align:justify;">{{ $sistems->rekomendasi }}</td>
											<td style="width: 400px;">
												<textarea class="form-control" name="data[sistem][{{$sistems->id}}][tl]" id="exampleFormControlTextarea1" placeholder="Tindak Lanjut" rows="3">{{ $sistems->register->tl }}</textarea>
											</td>
											<td style="width: 400px;" class="cari-sistem" data-id="{{ $sistems->id }} ">
												@if($sistems->register->status == 0)
													<input id="sistem-input-{{$sistems->id}}" name="data[sistem][{{$sistems->id}}][lampiran]" type="file" class="file form-control" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf">
												@else 
													<input id="sistem-show-{{$sistems->id}}" name="data[sistem][{{$sistems->id}}][lampiran]" type="file" class="form-control" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf" readonly>
												@endif
											</td>
											@if($record->kka->program->penugasanaudit->rencanadetail->tipe_object == 2)
												<td style="width: 50px;" class="cari-sistem" data-id="{{ $sistems->id }} ">
													<input type="checkbox" name="data[sistem][{{$sistems->id}}][svp]" data-val="sistem" data-cek="nol" data-toggle="toggle" data-on="Ya" data-off="Tidak" data-onstyle="info" @if($sistems->register->status != 1) disabled @endif class="toggle mytoggle" @if($sistems->register->status >= 2) checked @endif disabled>
												</td>
											@endif
											<td style="width: 50px;">
												<input type="checkbox" name="data[sistem][{{$sistems->id}}][pilihan]" data-val="sistem" data-cek="satu" data-toggle="toggle" data-id="{{$a}}" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" disabled class="toggle mytoggle sistem-one-{{$a}}" value="4" @if($sistems->register->stage == 4) checked @endif>
											</td>
											<td style="width: 50px;">
												<input type="checkbox" name="data[sistem][{{$sistems->id}}][pilihan]" data-val="sistem" data-cek="dua" data-toggle="toggle" data-id="{{$a}}" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" disabled class="toggle mytoggle sistem-two-{{$a}}" value="2" @if($sistems->register->stage == 2 || $sistems->register->stage == 0) checked @endif>
											</td>
											<td style="width: 50px;">
												<input type="checkbox" name="data[sistem][{{$sistems->id}}][pilihan]" data-val="sistem" data-cek="tiga" data-toggle="toggle" data-id="{{$a}}" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" disabled class="toggle mytoggle sistem-three-{{$a}}" value="3" @if($sistems->register->stage == 3) checked @endif>
											</td>
										</tr>
									@else 
										<tr>
											<td style="width: 400px;text-align:justify;">{{ $sistems->rekomendasi }}</td>
											<td style="width: 400px;">
												<textarea class="form-control" name="data[sistem][{{$sistems->id}}][tl]" id="exampleFormControlTextarea1" placeholder="Tindak Lanjut" rows="3">{{ $sistems->register->tl }}</textarea>
											</td>
											<td style="width: 400px;" class="cari-sistem" data-id="{{ $sistems->id }} ">
												@if($sistems->register->status == 0)
													<input id="sistem-input-{{$sistems->id}}" name="data[sistem][{{$sistems->id}}][lampiran]" type="file" class="file form-control" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf">
												@else 
													<input id="sistem-show-{{$sistems->id}}" name="data[sistem][{{$sistems->id}}][lampiran]" type="file" class="form-control" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf" readonly>
												@endif
											</td>
											@if($record->kka->program->penugasanaudit->rencanadetail->tipe_object == 2)
												<td style="width: 50px;" class="cari-sistem" data-id="{{ $sistems->id }} ">
													<input type="checkbox" name="data[sistem][{{$sistems->id}}][svp]" data-val="sistem" data-cek="nol" data-toggle="toggle" data-on="Ya" data-off="Tidak" data-onstyle="info" @if($sistems->register->status != 1) disabled @endif class="toggle mytoggle" @if($sistems->register->status >= 2) checked @endif disabled>
												</td>
											@endif
											<td style="width: 50px;">
												<input type="checkbox" name="data[sistem][{{$sistems->id}}][pilihan]" data-val="sistem" data-cek="satu" data-toggle="toggle" data-id="{{$a}}" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" disabled class="toggle mytoggle sistem-one-{{$a}}" value="4" @if($sistems->register->stage == 4) checked @endif>
											</td>
											<td style="width: 50px;">
												<input type="checkbox" name="data[sistem][{{$sistems->id}}][pilihan]" data-val="sistem" data-cek="dua" data-toggle="toggle" data-id="{{$a}}" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" disabled class="toggle mytoggle sistem-two-{{$a}}" value="2" @if($sistems->register->stage == 2 || $sistems->register->stage == 0) checked @endif>
											</td>
											<td style="width: 50px;">
												<input type="checkbox" name="data[sistem][{{$sistems->id}}][pilihan]" data-val="sistem" data-cek="tiga" data-toggle="toggle" data-id="{{$a}}" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" disabled class="toggle mytoggle sistem-three-{{$a}}" value="3" @if($sistems->register->stage == 3) checked @endif>
											</td>
										</tr>
									@endif
								@endforeach
								@php 
									$k++;
								@endphp
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			<div class="form-group col-md-12">
				<div class="text-right">
					<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
					<button type="button" class="btn btn-simpan save as page">Submit</button>
				</div>
			</div>
		</form>
    </div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
	    .datepicker.dropdown-menu {
		    z-index: 9999 !important;
		}
    </style>
@endpush

@push('scripts')
    <script> 
    	$("[data-toggle='toggle']").bootstrapToggle('destroy')                 
		$("[data-toggle='toggle']").bootstrapToggle();
		  $('.mytoggle').change(function() {
		  	var id = $(this).data('id');
		  	var val = $(this).data('val');
		  	var cek = $(this).data('cek');
		  	if ($(this).is(':checked')) {
		  		if(val == 'operasional'){
		  			if(cek == 'satu'){
		  				$('.operasional-two-'+id).prop('checked', false).change();
		  				$('.operasional-three-'+id).prop('checked', false).change();
		  			}else if(cek == 'dua'){
		  				$('.operasional-one-'+id).prop('checked', false).change();
		  				$('.operasional-three-'+id).prop('checked', false).change();
		  			}else{
		  				$('.operasional-one-'+id).prop('checked', false).change();
		  				$('.operasional-two-'+id).prop('checked', false).change();
		  			}
		  		}else if(val == 'keuangan'){
		  			if(cek == 'satu'){
		  				$('.keuangan-two-'+id).prop('checked', false).change();
		  				$('.keuangan-three-'+id).prop('checked', false).change();
		  			}else if(cek == 'dua'){
		  				$('.keuangan-one-'+id).prop('checked', false).change();
		  				$('.keuangan-three-'+id).prop('checked', false).change();
		  			}else{
		  				$('.keuangan-one-'+id).prop('checked', false).change();
		  				$('.keuangan-two-'+id).prop('checked', false).change();
		  			}
		  		}else{
		  			if(cek == 'satu'){
		  				$('.sistem-two-'+id).prop('checked', false).change();
		  				$('.sistem-three-'+id).prop('checked', false).change();
		  			}else if(cek == 'dua'){
		  				$('.sistem-one-'+id).prop('checked', false).change();
		  				$('.sistem-three-'+id).prop('checked', false).change();
		  			}else{
		  				$('.sistem-one-'+id).prop('checked', false).change();
		  				$('.sistem-two-'+id).prop('checked', false).change();
		  			}
		  		}
		    }
		  });

		$('.cari-operasional').each(function(){
            var id = $(this).data("id");

			$.ajax({
                url: '{{ url('ajax/option/get-tl') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id
                },
            }).done(function(response) {
            	if(response.length > 0){
	            	var row = $(this).closest('tr');
	            	$('#operasional-show-'+id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreview: JSON.parse(response[0].replace(/&quot;/g,'"')),
						initialPreviewAsData: true,
						initialPreviewConfig: JSON.parse(response[1].replace(/&quot;/g,'"')),
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disa
					});
            	}else{
            		$('#operasional-input-'+id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreviewAsData: true,
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						dropZoneTitle: 'Tidak ada data',
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}
				$('.kv-file-upload').prop('disabled', false);
				$('.kv-file-zoom').prop('disabled', false);
            }).fail(function() {
            });
        });

        $('.cari-keuangan').each(function(){
            var id = $(this).data("id");

			$.ajax({
                url: '{{ url('ajax/option/get-tl') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id
                },
            }).done(function(response) {
            	if(response.length > 0){
	            	var row = $(this).closest('tr');
	            	$('#keuangan-show-'+id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreview: JSON.parse(response[0].replace(/&quot;/g,'"')),
						initialPreviewAsData: true,
						initialPreviewConfig: JSON.parse(response[1].replace(/&quot;/g,'"')),
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disa
					});
            	}else{
            		$('#keuangan-input-'+id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreviewAsData: true,
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						dropZoneTitle: 'Tidak ada data',
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}
				$('.kv-file-upload').prop('disabled', false);
				$('.kv-file-zoom').prop('disabled', false);
            }).fail(function() {
            });
        });

        $('.cari-sistem').each(function(){
            var id = $(this).data("id");

			$.ajax({
                url: '{{ url('ajax/option/get-tl') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id
                },
            }).done(function(response) {
            	if(response.length > 0){
	            	var row = $(this).closest('tr');
	            	$('#sistem-show-'+id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreview: JSON.parse(response[0].replace(/&quot;/g,'"')),
						initialPreviewAsData: true,
						initialPreviewConfig: JSON.parse(response[1].replace(/&quot;/g,'"')),
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disa
					});
            	}else{
            		$('#sistem-input-'+id).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		autoReplace: true,
						initialPreviewAsData: true,
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						dropZoneTitle: 'Tidak ada data',
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}
				$('.kv-file-upload').prop('disabled', false);
				$('.kv-file-zoom').prop('disabled', false);
            }).fail(function() {
            });
        });
    </script>
    @yield('js-extra')
@endpush

