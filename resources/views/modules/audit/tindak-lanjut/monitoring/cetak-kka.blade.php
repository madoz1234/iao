<html>
<style>	
		@font-face {
		    font-family: 'tahoma';
		    src: url({{ storage_path('fonts\tahoma.ttf') }}) format("truetype");
		    font-weight: 400;
		    font-style: normal;
		}

        @page {
            margin: 60px 25px;
            counter-reset: page;
        }

       /* Define now the real margins of every page in the PDF */
       body {
           margin-top: 1cm;
           margin-left: -0.5cm;
           margin-right: -0.5cm;
           margin-bottom: 1cm;
           font-style: normal;
       }

       .ui.table.bordered {
         border: solid black 1px;
         border-collapse: collapse;
         width: 100%;
         font-family : "tahoma", "Verdana", sans-serif;
       }

       .ui.table.bordered td {
         border: solid black .5px;
         border-collapse: collapse;
         /*padding:10px;*/
         padding:5px;
       }

       .ui.table.bordered td.center.aligned {
         text-align : center;
       }

       .page-break {
            page-break-after: always;
       }

       #watermark {
	        position: fixed;
            /** 
                Set a position in the page for your image
                This should center it vertically
            **/
            top: 2.5cm;
            bottom:   2.5cm;
            left:     10.5cm;

            /** Change image dimensions**/
            width:    8cm;
            height:   8cm;

            /** Your watermark should be behind every content**/
            z-index:  101;
	    }

       .pad{
       	padding-top: 5px;
       	padding-bottom: 5px;
       }

       /* Define the header rules */
        header {
            position: fixed;
            top: -40px;
            left: 0px;
            right: 0px;
            height: 10px;

            /* Extra personal styles */
            text-align: center;
            /*line-height: 35px;*/

            font-size: 11px;
        }
		
       main {
         position: sticky;
         font-size : 12px;
         padding-top : -1cm;
         margin-top : 2px;
         border : none;
         width: 100%;
       }

       main p {
         margin-left: 5px;
         margin-right: 5px;
       }

       /* Define the footer rules */
       footer {
           position: fixed;
           bottom: -60px;
           left: 0px;
           right: 0px;
           height: 50px;

           /* Extra personal styles */
           text-align: center;
           line-height: 35px;
       }
/*       .page-num:after { 
       	counter-increment: pages; 
       	content: counter(page) " of " counter(pages); 
       }*/
       table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;font-family : "tahoma", "Verdana", sans-serif;}
       table.page_header {width: 100%; border: none; padding: 2mm; font-size: 11px;font-family : "tahoma", "Verdana", sans-serif; text-align: left; margin-left: 50px;margin-top: 50px;}
       table.page_content_header {width: 100%; font-size: 11px;border-collapse: collapse;font-family : "tahoma", "Verdana", sans-serif;text-align: center; padding-bottom: 1px;
       	padding-top: 1px;}
</style>
<head>
	<table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;padding-top: -90px;">
		<tbody>
			<tr style="">
				<td style="text-align: left;border: 1px #ffffff;" colspan="2">
					<table style="border: 1px #ffffff;" class="page_content ui table bordered">
						<tbody>
			    			<tr style="">
			    				<td style="width: 150px;border: 1px #ffffff;text-align: center;" rowspan="4"><img src="{{ asset('src/img/logo.png') }}" width="85px" height="70px"></td>
			    				<td style="text-align: center;font-weight: bold;border: 1px #ffffff;font-size: 14px;" colspan="3">KERTAS KERJA AUDIT</td>
			    				<td style="width: 1px;border: 1px #ffffff;" rowspan="4">&nbsp;</td>
			    				<td style="width: 210px;text-align: center;" colspan="2">Form IA 03</td>
			    			</tr>
			    			<tr style="">
			    				<td style="width: 130px;border: 1px #ffffff;">NAMA OBJEK AUDIT </td>
			    				<td style="width:5px;border: 1px #ffffff;">:</td>
			    				<td style="border: 1px #ffffff;">{!! object_audit($records->program->penugasanaudit->rencanadetail->tipe_object, $records->program->penugasanaudit->rencanadetail->object_id)!!}</td>
			    				<td style="text-align: center;">Edisi : Januari 2020</td>
			    				<td style="text-align: center;">Revisi : 0</td>
			    			</tr>
			    			<tr style="">
			    				<td style="border: 1px #ffffff;">NO. AB </td>
			    				<td style="border: 1px #ffffff;width:5px;">:</td>
			    				<td style="border: 1px #ffffff;">{!! getAb($records->program->penugasanaudit->rencanadetail->tipe_object, $records->program->penugasanaudit->rencanadetail->object_id)!!}</td>
			    				<td style="border: 1px #ffffff;" colspan="2" rowspan="2">&nbsp;</td>
			    			</tr>
			    			<tr style="">
			    				<td style="border: 1px #ffffff;">TANGGAL AUDIT </td>
			    				<td style="width:5px;border: 1px #ffffff;">:</td>
			    				<td style="border: 1px #ffffff;">{{ DateToStringWday($records->program->detailpelaksanaan->min('tgl')) }}  -  {{DateToStringWday($records->program->detailpelaksanaan->max('tgl'))}}</td>
			    			</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</head>
<body>
	<main>
		<table border="1" class="page_content ui table bordered" style="margin-top: 10px;page-break-inside: auto;padding:5px;padding-top: 20px;">
			<tbody>
				<tr style="">
					<td style="" rowspan="2">No</td>
					<td style="" rowspan="2">URAIAN</td>
					<td style="" rowspan="2">REKOMENDASI</td>
					<td style="" rowspan="2">TANGGAL TINDAK LANJUT</td>
					<td style="" colspan="2">Verifikasi</td>
				</tr>
				<tr style="">
					<td style="">Auditee </td>
					<td style="">Auditor</td>
				</tr>
				<tr style="">
					<td style="">A</td>
					<td style="">Operasional</td>
					<td style="">&nbsp;</td>
					<td style="">&nbsp;</td>
					<td style="">&nbsp;</td>
					<td style="">&nbsp;</td>
				</tr>
				@php 
					$i=0;
				@endphp
				@foreach($records->detaildraft->where('tipe', 0) as $key => $operasional)
					<tr style="">
						<td style="">{{ $i+1 }}</td>
						<td style="">
							{{getstandarisasi($operasional->standarisasi_id)}}<br>
							Kondisi :<br>
							{{ $operasional->catatan_kondisi }}<br>
							Kriteria : (Peraturan, No Pasal, Uraian)
							@foreach(getkriteria($operasional->standarisasi_id) as $keys => $data)
								{{$keys+1}}. {{ $data->deskripsi }}<br>
							@endforeach
							<br><br>
							Sebab : (Man, Machine, Method, Material, Money, dan termasuk pengendalian intern)<br>
							{{ $operasional->sebab }}<br>
							Risiko : <br>{{ $operasional->risiko }}<br>
							Kategori Temuan : {!! getkategoristatus($operasional->kategori_id) !!} <br>
							Tanggapan Auditee : <br>
							@if($operasional->tanggapan == 0)
								Menyetujui
							@else 
								Tidak Menyetujui
								<br>
								<br>
								Catatan Tanggapan Auditee : <br>
								<p style="text-align: justify;text-justify: inter-word;">{{ $operasional->catatan }}</p>
							@endif

						</td>
						<td style="">
							@foreach($operasional->rekomendasi as $x => $datas)
								{{$x+1}}. {{$datas->rekomendasi}}<br>
							@endforeach
						</td>
						<td style="">{{ DateToString($operasional->tgl) }}</td>
						<td style="">({{ $operasional->user->name }})
						</td>
						<td style="">({{ $operasional->user_auditor->name }})
						</td>
					</tr>
				@php 
					$i++;
				@endphp
				@endforeach
				<tr style="">
					<td style="">B</td>
					<td style="">Keuangan</td>
					<td style="">&nbsp;</td>
					<td style="">&nbsp;</td>
					<td style="">&nbsp;</td>
					<td style="">&nbsp;</td>
				</tr>
				@php 
					$j=0;
				@endphp
				@foreach($records->detaildraft->where('tipe', 1) as $key => $keuangan)
					<tr style="">
						<td style="">{{ $j+1 }}</td>
						<td style="">
							{{getstandarisasi($keuangan->standarisasi_id)}}<br>
							Kondisi :<br>
							{{ $keuangan->catatan_kondisi }}<br>
							Kriteria : (Peraturan, No Pasal, Uraian)
							@foreach(getkriteria($keuangan->standarisasi_id) as $keys => $data)
								{{$keys+1}}. {{ $data->deskripsi }}<br>
							@endforeach
							<br>
							Sebab : (Man, Machine, Method, Material, Money, dan termasuk pengendalian intern)<br>
							{{ $keuangan->sebab }}<br>
							Risiko : <br>{{ $keuangan->risiko }}<br>
							Kategori Temuan : {!! getkategoristatus($keuangan->kategori_id) !!} <br>
							Tanggapan Auditee : <br>
							@if($keuangan->tanggapan == 0)
								Menyetujui
							@else 
								Tidak Menyetujui
								<br>
								<br>
								Catatan Tanggapan Auditee : <br>
								<p style="text-align: justify;text-justify: inter-word;">{{ $keuangan->catatan }}</p>
							@endif

						</td>
						<td style="">
							@foreach($keuangan->rekomendasi as $x => $datas)
								{{$x+1}}. {{$datas->rekomendasi}}<br>
							@endforeach
						</td>
						<td style="">{{ DateToString($keuangan->tgl) }}</td>
						<td style="">({{ $keuangan->user->name }})
						</td>
						<td style="">({{ $keuangan->user_auditor->name }})
						</td>
					</tr>
				@php 
					$j++;
				@endphp
				@endforeach
				<tr style="">
					<td colspan="2">&nbsp;</td>
					<td colspan="4">Disetujui Oleh : <br><br>
						Ketua Tim Auditor<br><br><br><br><br><br>


						({{$records->program->user->name }})
					</td>
				</tr>
				<tr style="">
					<td style="">C</td>
					<td style="">Sistem</td>
					<td style="">&nbsp;</td>
					<td style="">&nbsp;</td>
					<td style="">&nbsp;</td>
					<td style="">&nbsp;</td>
				</tr>
				@php 
					$k=0;
				@endphp
				@foreach($records->detaildraft->where('tipe', 2) as $key => $sistem)
					<tr style="">
						<td style="">{{ $k+1 }}</td>
						<td style="">
							{{getstandarisasi($sistem->standarisasi_id)}}<br>
							Kondisi :<br>
							{{ $sistem->catatan_kondisi }}<br>
							Kriteria : (Peraturan, No Pasal, Uraian)
							@foreach(getkriteria($sistem->standarisasi_id) as $keys => $data)
								{{$keys+1}}. {{ $data->deskripsi }}<br>
							@endforeach
							<br>
							Sebab : (Man, Machine, Method, Material, Money, dan termasuk pengendalian intern)<br>
							{{ $sistem->sebab }}<br>
							Risiko : <br>{{ $sistem->risiko }}<br>
							Kategori Temuan : {!! getkategoristatus($sistem->kategori_id) !!} <br>
							Tanggapan Auditee : <br>
							@if($sistem->tanggapan == 0)
								Menyetujui
							@else 
								Tidak Menyetujui
								<br>
								<br>
								Catatan Tanggapan Auditee : <br>
								<p style="text-align: justify;text-justify: inter-word;">{{ $sistem->catatan }}</p>
							@endif

						</td>
						<td style="">
							@foreach($sistem->rekomendasi as $x => $datas)
								{{$x+1}}. {{$datas->rekomendasi}}<br>
							@endforeach
						</td>
						<td style="">{{ DateToString($sistem->tgl) }}</td>
						<td style="">({{ $sistem->user->name }})
						</td>
						<td style="">({{ $sistem->user_auditor->name }})
						</td>
					</tr>
				@php 
					$k++;
				@endphp
				@endforeach
			</tbody>
		</table>
	</main>
</body>
</html>