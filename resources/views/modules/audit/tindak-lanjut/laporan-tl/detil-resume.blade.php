@extends('layouts.form')
@section('title', 'Detil Resume')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body" style="padding-bottom: 0px;">
		<form action="" method="POST" id="formData">
			@php 
				$data_operasional = array();
				foreach($record->draftkka->program->detailanggota->where('fungsi', 1) as $operasional) {
				 $data_operasional[] = $operasional->user->name;
				}

				$data_keuangan = array();
				foreach($record->draftkka->program->detailanggota->where('fungsi', 2) as $keuangan) {
				 $data_keuangan[] = $keuangan->user->name;
				}

				$data_sistem = array();
				foreach($record->draftkka->program->detailanggota->where('fungsi', 3) as $sistem) {
				 $data_sistem[] = $sistem->user->name;
				}
			@endphp
			<div class="form-row">
				<table id="table" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
					<tbody>
						<tr>
							<td style="" colspan="2" rowspan="3">&nbsp;</td>
							<td style="width: 200px !important;text-align: center;" colspan="2">Form. IA 04</td>
						</tr>
						<tr>
							<td style="width: 100px !important;">Edisi : Mei 2019</td>
							<td style="width: 100px !important;">Revisi : 0</td>
						</tr>
						<tr>
							<td style="" colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td style="width: 70rem !important;">Nomor :{{ $data->memo }} - LHA/WK/IA/{{ DateToStringWday($data->tanggal) }}</td>
							<td style="width: 70rem !important; text-align: left;" colspan="3">TANGGAL : {{ DateToStringWday($data->tanggal) }}</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-bottom: 1px #ffffff;" colspan="6">
								LAPORAN HASIL AUDIT INTERNAL PADA :  {{ DateToStringWday($data->tanggal) }} <br>
								PT WASKITA KARYA (PERSERO) TBK</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">RESUME</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											Dari hasil audit yang dilakukan oleh Internal Audit pada {{ DateToStringWday($data->tanggal) }} dengan Internal Memo nomor : {{ $data->memo }}/IM/WK/{{ TahunToString($data->tanggal) }} tanggal {{ DateToStringWday($data->tanggal) }}, dan berlangsung pada tanggal {{ DateToStringWday($data->tanggal) }} kami laporkan hal-hal sbb :
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">A. Aspek Operasional</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											<table id="table-operasional" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
												<thead>
													<th style="width: 50px;text-align: center;">No</th>
													<th style="text-align: center;">Keterangan</th>
												</thead>
												<tbody class="container-operasional">
													@if($record->detaillha->where('tipe', 0)->count() > 0)
														@php 
															$i=0;
														@endphp
														@foreach($record->detaillha->where('tipe', 0) as $key => $operasional)
															<tr data-id="0">
																<td style="text-align: center" class="numboor-0-{{$i}}">{{ $i+1 }}</td>
																<td class="field" style="text-align: left;">
																	{{$operasional->keterangan}}
																</td>
															</tr>
															@php 
															$i++;
															@endphp
														@endforeach
													@endif
												</tbody>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">B. Aspek Keuangan</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											<table id="table-keuangan" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
												<thead>
													<th style="width: 50px;text-align: center;">No</th>
													<th style="text-align: center;">Keterangan</th>
												</thead>
												<tbody class="container-keuangan">
													@if($record->detaillha->where('tipe', 1)->count() > 0)
														@php 
															$j=0;
														@endphp
														@foreach($record->detaillha->where('tipe', 1) as $kiy => $keuangan)
															<tr data-id="1">
																<td style="text-align: center;" class="numboor-1-{{$j}}">{{ $j+1 }}</td>
																<td class="field" style="text-align: left;">
																	{{$keuangan->keterangan}}
																</td>
															</tr>
															@php 
																$j++;
															@endphp
														@endforeach
													@endif
												</tbody>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">C. Aspek Sistem</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											<table id="table-sistem" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
												<thead>
													<th style="width: 50px;text-align: center;">No</th>
													<th style="text-align: center;">Keterangan</th>
												</thead>
												<tbody class="container-sistem">
													@if($record->detaillha->where('tipe', 2)->count() > 0)
														@php 
															$k=0;
														@endphp
														@foreach($record->detaillha->where('tipe', 2) as $koy => $sistem)
															<tr data-id="2">
																<td style="text-align: center;" class="numboor-2-{{$k}}">{{ $k+1 }}</td>
																<td class="field" style="text-align: left;">
																	{{$sistem->keterangan}}
																</td>
															</tr>
															@php 
																$k++;
															@endphp
														@endforeach
													@endif
												</tbody>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">D. Kesimpulan</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;" class="field">
											@if($record->kesimpulan)
												{{$record->kesimpulan}}
											@endif
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-left">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
        .table-responsive{
            overflow-x: auto;
            overflow-y: visible !important;
        }

        .table-responsive .dropdown-menu {
	        position: static !important;
	    }

	    div.dropdown-menu.open{
		  max-width: 300px !important;
		  overflow: hidden;
		}
		ul.dropdown-menu.inner{
		  max-width: 300px !important;
		  overflow-y: auto;
		}
    </style>
@endpush

@push('scripts')
    <script>
    	$(document).on('click', '.tambah_operasional', function(e){
    		var last = parseInt($('input[name=last_operasional]').val()) + 1;
			var rowCount = $('#table-operasional > tbody > tr').length;
			var c = rowCount-1;
			var html = `
					<tr data-id="`+(c+2)+`">
						<td style="text-align: center;margin-top: 9px;padding-top: 21px;" class="numboor-0-`+(c+2)+`">`+(c+2)+`</td>
						<td class="field">
							<textarea class="form-control" name="detail[0][data][`+(c+2)+`][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="2"></textarea>
						</td>
						<td style="text-align: center;">
							<button class="btn btn-sm btn-danger hapus_operasional" type="button" data-id="0" data-detail="`+(c+2)+`" data-toggle="tooltip" data-placement="left" title="Hapus Operasional" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
						</td>
					</tr>
				`;

				$('.container-operasional').append(html);
		        $('input[name=last_operasional]').val((c+2))
		});
		$(document).on('click', '.hapus_operasional', function (e){
			var id = $(this).data("id");
			var detail =$(this).data("detail");
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#table-operasional');
			var rows = table.find('tbody tr');
			$.each(rows, function(key, value){
				table.find('.numboor-0-'+$(this).data("id")).html((key+1));
			});
		});

		$(document).on('click', '.tambah_keuangan', function(e){
    		var last = parseInt($('input[name=last_keuangan]').val()) + 1;
			var rowCount = $('#table-keuangan > tbody > tr').length;
			var c = rowCount-1;
			var html = `
					<tr data-id="`+(c+2)+`">
						<td style="text-align: center;margin-top: 9px;padding-top: 21px;" class="numboor-1-`+(c+2)+`">`+(c+2)+`</td>
						<td class="field">
							<textarea class="form-control" name="detail[1][data][`+(c+2)+`][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="2"></textarea>
						</td>
						<td style="text-align: center;">
							<button class="btn btn-sm btn-danger hapus_keuangan" type="button" data-id="1" data-detail="`+(c+2)+`" data-toggle="tooltip" data-placement="left" title="Hapus Keuangan" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
						</td>
					</tr>
				`;

				$('.container-keuangan').append(html);
		        $('input[name=last_keuangan]').val((c+2))
		});
		$(document).on('click', '.hapus_keuangan', function (e){
			var id = $(this).data("id");
			var detail =$(this).data("detail");
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#table-keuangan');
			var rows = table.find('tbody tr');
			$.each(rows, function(key, value){
				table.find('.numboor-1-'+$(this).data("id")).html((key+1));
			});
		});

		$(document).on('click', '.tambah_sistem', function(e){
    		var last = parseInt($('input[name=last_sistem]').val()) + 1;
			var rowCount = $('#table-sistem > tbody > tr').length;
			var c = rowCount-1;
			var html = `
					<tr data-id="`+(c+2)+`">
						<td style="text-align: center;margin-top: 9px;padding-top: 21px;" class="numboor-2-`+(c+2)+`">`+(c+2)+`</td>
						<td class="field">
							<textarea class="form-control" name="detail[2][data][`+(c+2)+`][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="2"></textarea>
						</td>
						<td style="text-align: center;">
							<button class="btn btn-sm btn-danger hapus_sistem" type="button" data-id="2" data-detail="`+(c+2)+`" data-toggle="tooltip" data-placement="left" title="Hapus Keuangan" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
						</td>
					</tr>
				`;

				$('.container-sistem').append(html);
		        $('input[name=last_sistem]').val((c+2))
		});
		$(document).on('click', '.hapus_sistem', function (e){
			var id = $(this).data("id");
			var detail =$(this).data("detail");
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#table-sistem');
			var rows = table.find('tbody tr');
			$.each(rows, function(key, value){
				table.find('.numboor-2-'+$(this).data("id")).html((key+1));
			});
		});
    </script>
    @yield('js-extra')
@endpush

