@extends('layouts.form')
@section('title', 'Detil LHA')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body" style="padding-bottom: 0px;">
		<form action="" method="POST" id="formData">
			@php 
				$data_operasional = array();
				foreach($record->draftkka->program->detailanggota->where('fungsi', 1) as $operasional) {
				 $data_operasional[] = $operasional->user->name;
				}

				$data_keuangan = array();
				foreach($record->draftkka->program->detailanggota->where('fungsi', 2) as $keuangan) {
				 $data_keuangan[] = $keuangan->user->name;
				}

				$data_sistem = array();
				foreach($record->draftkka->program->detailanggota->where('fungsi', 3) as $sistem) {
				 $data_sistem[] = $sistem->user->name;
				}
			@endphp
			<div class="form-row">
				<table id="table-operasional" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
					<tbody>
						<tr>
							<td style="" colspan="2" rowspan="3">&nbsp;</td>
							<td style="width: 200px !important;text-align: center;" colspan="2">Form. IA 04</td>
						</tr>
						<tr>
							<td style="width: 100px !important;">Edisi : Mei 2019</td>
							<td style="width: 100px !important;">Revisi : 0</td>
						</tr>
						<tr>
							<td style="" colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td style="width: 70rem !important;">Nomor :............. - LHA/WK/IA/...........</td>
							<td style="width: 70rem !important; text-align: left;" colspan="3">TANGGAL : {{ DateToStringWday($data->tanggal) }}</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-bottom: 1px #ffffff;" colspan="6">
								LAPORAN HASIL AUDIT INTERNAL PADA : {{ DateToStringWday($data->tanggal) }} </br>
								PT WASKITA KARYA (PERSERO) TBK</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">I. Dasar Penugasan Audit</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											Berdasarkan Internal Memo Nomor : {{ $data->memo }}
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">II. Tujuan dan Lingkup Audit</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											{{ $record->draftkka->program->sasaran }}<br>
											Lingkup Audit Meliputi : {{ $record->draftkka->program->ruang_lingkup }}
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">III. Standar Audit</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											Audit telah dilakukan dengan Standar Audit yang diterbitkan oleh Asosiasi Auditor Internal yang meliputi Standar Umum, Standar Pelaksanaan, Standar Pelaporan dan Standar Tindak Lanjut
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">III. Auditor</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											Ketua Tim &nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;{{ $record->draftkka->program->user->name }} <br>
											Anggota &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;{{ implode(", ",$data_operasional) }}, {{ implode(", ",$data_keuangan) }}, {{ implode(", ",$data_sistem) }} <br>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">V. Periode Pelaksanaan Audit</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											Audit dilaksanakan pada {{ $record->draftkka->program->tinjauan->penugasanaudit->rencanadetail->rencana }}
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">VI. Metodologi Audit</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											Metodologi Audit dilaksanakan sesuai dengan Program Kerja Tahunan Internal Audit Tahun
											2019 dengan menggunakan metode sampling dengan tahapan sebagai berikut :<br>
											<ol class="list-group" style="padding-left: 13px;">
												<li class="list item">Perencanaan</li>
												<li class="list item">Pelaksanaan
													<ol type="a" style="padding-left: 15px;">
														<li>Prosedur Analitis</li>
														<li>Pengendalian Intern</li>
														<li>Substansi</li>
														<li>Tanggapan Auditee</li>
													</ol>
												</li>
												<li class="list item">Pelaporan</li>

											</ol>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">VII. Data Umum Objek Audit</h5>
								<table id="table-fokus" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
									<thead>
										<tr>
											<th style="text-align: center;" rowspan="2">No</th>
											<th style="text-align: center;" colspan="2" rowspan="2">Uraian</th>
											<th style="text-align: center;border-bottom: 1px solid rgba(34,36,38,.1);border-left: 1px solid rgba(34,36,38,.1);" colspan="2">Waktu Audit</th>
											<th style="text-align: center;border-right: 1px solid rgba(34,36,38,.1);" rowspan="2" colspan="3">Keterangan</td>
										</tr>
										<tr>
											<th style="">Rencana</th>
											<th style="">Realisasi</th>
										</tr>
									</thead>
									<tbody>
											{{-- OPERASIONAL --}}
										<tr>
											<td style="width: 20px;text-align: center;font-weight: bold;">I</td>
											<td style="text-align: left;font-weight: bold;border-right: 1px #ffffff;" colspan="5">OPERASIONAL</td>
											<td style="text-align: center;font-weight: bold;width:50px;border-right: 1px solid rgba(34,36,38,.1);border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
											</td>
										</tr>
										@php 
										$i=1;
										@endphp
										@foreach($record->draftkka->program->detailkerja->where('bidang', 1) as $kiy => $operasional)
											@php
											if(!empty($operasional->fokusaudit->langkahkerja->first()->detail)){
												$row = $operasional->fokusaudit->langkahkerja->first()->detail->count();
											}else{
												$row=0;
											}
											@endphp
											<tr class="data_operasional data_fokus_audit_operasional_{{$kiy}}" data-id="{{$kiy}}">
												<td class="for_rowspan_operasional_{{$kiy}}" style="text-align: center;font-weight: bold;" rowspan="{{$row+3}}">{{$i}}</td>
												<td style="width: 20px;" rowspan="3">&nbsp;</td>
												<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="5">Fokus Audit :</td>
											</tr>
											<tr class="detail_fokus_audit_operasional_{{$kiy}}" style="border-bottom: 1px solid rgba(34,36,38,.1);">
												<td style="width: 800px;" class="field">
													{{ $operasional->fokusaudit->audit }}
												</td>
												<td style="width: 150px;" class="field">
													{{ DateToStrings($operasional->rencana) }}
												</td>
												<td style="width: 150px;" class="field">
													{{ DateToStrings($operasional->realisasi) }}
												</td>
												<td colspan="2" style="border-right: 1px solid rgba(34,36,38,.1);text-align: left;text-align: justify;text-justify: inter-word;">
													@if($operasional->keterangan)
													{{$operasional->keterangan}}
													@else 
													-
													@endif
												</td>
											</tr>
											<tr class="langkah_kerja_operasional_{{$kiy}}">
												<td style="text-align: left;" colspan="6">Langkah Kerja</td>
											</tr>
											@if(!empty($operasional->fokusaudit->langkahkerja->first()->detail))
												@foreach($operasional->fokusaudit->langkahkerja->first()->detail as $kuy => $detil)
												<tr class="langkah-kerja-operasional-{{$kiy}}">
													<td>{{column_letter($kuy+1)}}</td>
													<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);text-align: justify;text-justify: inter-word;" colspan="5">{{$detil->deskripsi}}</td>
												</tr>
												@endforeach
											@endif
											@php 
											$i++;
											@endphp
										@endforeach

										{{-- KEUANGAN --}}
										<tr>
											<td style="width: 20px;text-align: center;font-weight: bold;">II</td>
											<td style="text-align: left;font-weight: bold;border-right: 1px #ffffff;" colspan="5">KEUANGAN</td>
											<td style="text-align: center;font-weight: bold;width:50px;border-right: 1px solid rgba(34,36,38,.1);border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
											</td>
										</tr>
										@php 
										$i=1;
										@endphp
										@foreach($record->draftkka->program->detailkerja->where('bidang', 2) as $kiy => $keuangan)
											@php
											if(!empty($keuangan->fokusaudit->langkahkerja->first()->detail)){
												$row = $keuangan->fokusaudit->langkahkerja->first()->detail->count();
											}else{
												$row=0;
											}
											@endphp
											<tr class="data_keuangan data_fokus_audit_keuangan_{{$kiy}}" data-id="{{$kiy}}">
												<td class="for_rowspan_keuangan_{{$kiy}}" style="text-align: center;font-weight: bold;" rowspan="{{$row+3}}">{{$i}}</td>
												<td style="width: 20px;" rowspan="3">&nbsp;</td>
												<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="5">Fokus Audit :</td>
											</tr>
											<tr class="detail_fokus_audit_keuangan_{{$kiy}}" style="border-bottom: 1px solid rgba(34,36,38,.1);">
												<td style="width: 800px;" class="field">
													{{ $keuangan->fokusaudit->audit }}
												</td>
												<td style="width: 150px;" class="field">
													{{ DateToStrings($keuangan->rencana) }}
												</td>
												<td style="width: 150px;" class="field">
													{{ DateToStrings($keuangan->realisasi) }}
												</td>
												<td colspan="2" style="border-right: 1px solid rgba(34,36,38,.1);text-align: left;text-align: justify;text-justify: inter-word;">
													@if($keuangan->keterangan)
													{{$keuangan->keterangan}}
													@else 
													-
													@endif
												</td>
											</tr>
											<tr class="langkah_kerja_keuangan_{{$kiy}}">
												<td style="text-align: left;" colspan="6">Langkah Kerja</td>
											</tr>
											@if(!empty($keuangan->fokusaudit->langkahkerja->first()->detail))
												@foreach($keuangan->fokusaudit->langkahkerja->first()->detail as $kuy => $detil)
												<tr class="langkah-kerja-keuangan-{{$kiy}}">
													<td>{{column_letter($kuy+1)}}</td>
													<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);text-align: justify;text-justify: inter-word;" colspan="5">{{$detil->deskripsi}}</td>
												</tr>
												@endforeach
											@endif
											@php 
											$i++;
											@endphp
										@endforeach

										{{-- SISTEM --}}
										<tr>
											<td style="width: 20px;text-align: center;font-weight: bold;">III</td>
											<td style="text-align: left;font-weight: bold;border-right: 1px #ffffff;" colspan="5">SISTEM</td>
											<td style="text-align: center;font-weight: bold;width:50px;border-right: 1px solid rgba(34,36,38,.1);border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
											</td>
										</tr>
										@php 
										$i=1;
										@endphp
										@foreach($record->draftkka->program->detailkerja->where('bidang', 3) as $kiy => $sistem)
											@php
											if(!empty($sistem->fokusaudit->langkahkerja->first()->detail)){
												$row = $sistem->fokusaudit->langkahkerja->first()->detail->count();
											}else{
												$row=0;
											}
											@endphp
											<tr class="data_sistem data_fokus_audit_sistem_{{$kiy}}" data-id="{{$kiy}}">
												<td class="for_rowspan_sistem_{{$kiy}}" style="text-align: center;font-weight: bold;" rowspan="{{$row+3}}">{{$i}}</td>
												<td style="width: 20px;" rowspan="3">&nbsp;</td>
												<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="5">Fokus Audit :</td>
											</tr>
											<tr class="detail_fokus_audit_sistem_{{$kiy}}" style="border-bottom: 1px solid rgba(34,36,38,.1);">
												<td style="width: 800px;" class="field">
													{{ $sistem->fokusaudit->audit }}
												</td>
												<td style="width: 150px;" class="field">
													{{ DateToStrings($sistem->rencana) }}
												</td>
												<td style="width: 150px;" class="field">
													{{ DateToStrings($sistem->realisasi) }}
												</td>
												<td colspan="2" style="border-right: 1px solid rgba(34,36,38,.1);text-align: left;text-align: justify;text-justify: inter-word;">
													@if($sistem->keterangan)
													{{$sistem->keterangan}}
													@else 
													-
													@endif
												</td>
											</tr>
											<tr class="langkah_kerja_sistem_{{$kiy}}">
												<td style="text-align: left;" colspan="6">Langkah Kerja</td>
											</tr>
											@if(!empty($sistem->fokusaudit->langkahkerja->first()->detail))
												@foreach($sistem->fokusaudit->langkahkerja->first()->detail as $kuy => $detil)
												<tr class="langkah-kerja-sistem-{{$kiy}}">
													<td>{{column_letter($kuy+1)}}</td>
													<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);text-align: justify;text-justify: inter-word;" colspan="5">{{$detil->deskripsi}}</td>
												</tr>
												@endforeach
											@endif
											@php 
											$i++;
											@endphp
										@endforeach
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td style="text-align: left;font-weight: bold;font-size: 14px;border-top: 1px #ffffff;" colspan="6">
								<h5 style="font-weight: bold;">VIII. Hasil Audit</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											<div class="form-group col-md-12">
												<ul class="nav nav-tabs" id="myTab" role="tablist">
													<li class="nav-item">
														<a style="font-weight: bold;" class="nav-link active" id="a-tab" data-toggle="tab" href="#a" role="tab" aria-controls="home" aria-selected="true">Operasional</a>
													</li>
													<li class="nav-item">
														<a style="font-weight: bold;" class="nav-link" id="b-tab" data-toggle="tab" href="#b" role="tab" aria-controls="profile" aria-selected="false">Keuangan</a>
													</li>
													<li class="nav-item">
														<a style="font-weight: bold;" class="nav-link" id="c-tab" data-toggle="tab" href="#c" role="tab" aria-controls="messages" aria-selected="false">Sistem</a>
													</li>
												</ul>
												<div class="tab-content">
													<div class="tab-pane active" id="a" role="tabpanel" aria-labelledby="a-tab">
														<table id="table-operasional" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
															<tbody class="container-operasional">
																@php 
																	$i=0;
																@endphp
																@foreach($record->draftkka->detaildraft->where('tipe', 0) as $kay => $operasional)
																	@if($i == 0)
																		<tr class="detail-keuangan-{{$i}}" data-id="{{$i}}">
																			<td style="text-align: left;font-weight: bold;" colspan="5">Bidang Audit : Operasional</td>
																		</tr>
																	@else 
																	@endif
																	<tr class="data-operasional detail-operasional-{{$i}}" data-id="{{ $i }}" data-kategori="{{ $operasional->kategori_id }}" data-kriteria="{{$operasional->kriteria_id}}">
																		<input type="hidden" name="exist_operasional[]" value="{{ $operasional->id }}">
																		<input type="hidden" name="fokus_audit[0][detail][{{$i}}][id]" value="{{ $operasional->id }}">
																		<td style="text-align: center;width: 20px;">{{ $i+1 }}</td>
																		<td style="width: 150px;">Judul</td>
																		<td style="width: 2px;">:</td>
																		<td style="width: 1600px;text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{getstandarisasi($operasional->standarisasi_id)}}
																		</td>
																	</tr>
																	<tr class="detail-operasional-{{$i}}">
																		<td style="border-bottom: 2px solid black;" rowspan="10"></td>
																		<td style="">Kondisi</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			<ol style="padding-left: 10px;">
																				{!! getkondisi($operasional->standarisasi_id) !!}
																			</ol>
																			Catatan Tambahan : <br>
																			<p style="text-align: justify;text-justify: inter-word;">{{ $operasional->catatan_kondisi }}</p>
																		</td>
																	</tr>
																	<tr class="detail-operasional-{{$i}}">
																		<td style="">Temuan</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{gettemuan($operasional->temuan_id)}}
																		</td>
																	</tr>
																	<tr class="detail-operasional-{{$i}}">
																		<td style="">Kriteria</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{ $operasional->kriteria }}
																		</td>
																	</tr>
																	<tr class="detail-operasional-{{$i}}">
																		<td style="">Sebab</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{ $operasional->sebab }}
																		</td>
																	</tr>
																	<tr class="detail-operasional-{{$i}}">
																		<td style="">Risiko</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{ $operasional->risiko }}
																		</td>
																	</tr>
																	<tr class="detail-operasional-{{$i}}">
																		<td style="">Kategori Temuan</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{!! getkategoristatus($operasional->kategori_id) !!}
																		</td>
																	</tr>
																	<tr class="detail-operasional-{{$i}}">
																		<td style="">Rekomendasi</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{ $operasional->rekomendasi }}
																		</td>
																	</tr>
																	<tr class="detail-operasional-{{$i}}">
																		<td style="">Audite</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{ $operasional->user->name }}
																		</td>
																	</tr>
																	<tr class="detail-operasional-{{$i}}">
																		<td style="">Tanggapan Auditee</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			@if($operasional->tanggapan == 0)
																				Menyetujui
																			@else 
																				Tidak Menyetujui
																				<br>
																				<br>
																				Catatan Tanggapan Auditee : <br>
																				<p style="text-align: justify;text-justify: inter-word;">{{ $operasional->catatan }}</p>
																			@endif
																		</td>
																	</tr>
																	<tr class="detail-operasional-end-0 detail-operasional-{{$i}}" style="border-bottom: 2px solid black;">
																		<td style="">Tanggal Tindak Lanjut</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{ DateToString($operasional->tgl) }}
																		</td>
																	</tr>
																@endforeach
															</tbody>
														</table>
													</div>

													<div class="tab-pane" id="b" role="tabpanel" aria-labelledby="b-tab">
														<table id="table-keuangan" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
															<tbody class="container-keuangan">
																@php 
																	$h=0;
																@endphp
																@foreach($record->draftkka->detaildraft->where('tipe', 1) as $kay => $keuangan)
																	@if($h == 0)
																		<tr class="detail-keuangan-{{$h}}" data-id="{{$h}}">
																			<td style="text-align: left;font-weight: bold;" colspan="5">Bidang Audit : Keuangan</td>
																		</tr>
																	@else 
																	@endif
																	<tr class="data-keuangan detail-keuangan-{{$h}}" data-id="{{ $h }}" data-kategori="{{ $keuangan->kategori_id }}" data-kriteria="{{$keuangan->kriteria_id}}">
																		<input type="hidden" name="exist_keuangan[]" value="{{ $keuangan->id }}">
																		<input type="hidden" name="fokus_audit[1][detail][{{$h}}][id]" value="{{ $keuangan->id }}">
																		<td style="text-align: center;width: 20px;">{{ $h+1 }}</td>
																		<td style="width: 150px;">Judul</td>
																		<td style="width: 2px;">:</td>
																		<td style="width: 1600px;text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{getstandarisasi($keuangan->standarisasi_id)}}
																		</td>
																	</tr>
																	<tr class="detail-keuangan-{{$h}}">
																		<td style="border-bottom: 2px solid black;" rowspan="10"></td>
																		<td style="">Kondisi</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			<ol style="padding-left: 10px;">
																				{!! getkondisi($keuangan->standarisasi_id) !!}
																			</ol>
																			Catatan Tambahan : <br>
																			<p style="text-align: justify;text-justify: inter-word;">{{ $keuangan->catatan_kondisi }}</p>
																		</td>
																	</tr>
																	<tr class="detail-keuangan-{{$h}}">
																		<td style="">Temuan</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{gettemuan($keuangan->temuan_id)}}
																		</td>
																	</tr>
																	<tr class="detail-keuangan-{{$h}}">
																		<td style="">Kriteria</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{ $keuangan->kriteria }}
																		</td>
																	</tr>
																	<tr class="detail-keuangan-{{$h}}">
																		<td style="">Sebab</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{ $keuangan->sebab }}
																		</td>
																	</tr>
																	<tr class="detail-keuangan-{{$h}}">
																		<td style="">Risiko</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{ $keuangan->risiko }}
																		</td>
																	</tr>
																	<tr class="detail-keuangan-{{$h}}">
																		<td style="">Kategori Temuan</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{!! getkategoristatus($keuangan->kategori_id) !!}
																		</td>
																	</tr>
																	<tr class="detail-keuangan-{{$h}}">
																		<td style="">Rekomendasi</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{ $keuangan->rekomendasi }}
																		</td>
																	</tr>
																	<tr class="detail-keuangan-{{$h}}">
																		<td style="">Audite</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{ $keuangan->user->name }}
																		</td>
																	</tr>
																	<tr class="detail-keuangan-{{$h}}">
																		<td style="">Tanggapan Auditee</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			@if($keuangan->tanggapan == 0)
																				Menyetujui
																			@else 
																				Tidak Menyetujui
																				<br>
																				<br>
																				Catatan Tanggapan Auditee : <br>
																				<p style="text-align: justify;text-justify: inter-word;">{{ $keuangan->catatan }}</p>
																			@endif
																		</td>
																	</tr>
																	<tr class="detail-keuangan-end-0 detail-keuangan-{{$h}}" style="border-bottom: 2px solid black;">
																		<td style="">Tanggal Tindak Lanjut</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{ DateToString($keuangan->tgl) }}
																		</td>
																	</tr>
																@endforeach
															</tbody>
														</table>
													</div>

													<div class="tab-pane" id="c" role="tabpanel" aria-labelledby="c-tab">
														<table id="table-sistem" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
															<tbody class="container-sistem">
																@php 
																	$j=0;
																@endphp
																@foreach($record->draftkka->detaildraft->where('tipe', 2) as $koy => $sistem)
																	@if($j == 0)
																		<tr class="detail-keuangan-{{$j}}" data-id="{{$j}}">
																			<td style="text-align: left;font-weight: bold;" colspan="5">Bidang Audit : Sistem</td>
																		</tr>
																	@else 
																	@endif
																	<tr class="data-sistem detail-sistem-{{$j}}" data-id="{{ $j }}" data-kategori="{{ $sistem->kategori_id }}" data-kriteria="{{$sistem->kriteria_id}}">
																		<input type="hidden" name="exist_sistem[]" value="{{ $sistem->id }}">
																		<input type="hidden" name="fokus_audit[1][detail][{{$j}}][id]" value="{{ $sistem->id }}">
																		<td style="text-align: center;width: 20px;">{{ $j+1 }}</td>
																		<td style="width: 150px;">Judul</td>
																		<td style="width: 2px;">:</td>
																		<td style="width: 1600px;text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{getstandarisasi($sistem->standarisasi_id)}}
																		</td>
																	</tr>
																	<tr class="detail-sistem-{{$j}}">
																		<td style="border-bottom: 2px solid black;" rowspan="10"></td>
																		<td style="">Kondisi</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			<ol style="padding-left: 10px;">
																				{!! getkondisi($sistem->standarisasi_id) !!}
																			</ol>
																			Catatan Tambahan : <br>
																			<p style="text-align: justify;text-justify: inter-word;">{{ $sistem->catatan_kondisi }}</p>
																		</td>
																	</tr>
																	<tr class="detail-sistem-{{$j}}">
																		<td style="">Temuan</td>
																		<td style="">:</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{gettemuan($sistem->temuan_id)}}
																		</td>
																	</tr>
																	<tr class="detail-sistem-{{$j}}">
																		<td style="">Kriteria</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{ $sistem->kriteria }}
																		</td>
																	</tr>
																	<tr class="detail-sistem-{{$j}}">
																		<td style="">Sebab</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{ $sistem->sebab }}
																		</td>
																	</tr>
																	<tr class="detail-sistem-{{$j}}">
																		<td style="">Risiko</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{ $sistem->risiko }}
																		</td>
																	</tr>
																	<tr class="detail-sistem-{{$j}}">
																		<td style="">Kategori Temuan</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{!! getkategoristatus($sistem->kategori_id) !!}
																		</td>
																	</tr>
																	<tr class="detail-sistem-{{$j}}">
																		<td style="">Rekomendasi</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{ $sistem->rekomendasi }}
																		</td>
																	</tr>
																	<tr class="detail-sistem-{{$j}}">
																		<td style="">Audite</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{ $sistem->user->name }}
																		</td>
																	</tr>
																	<tr class="detail-sistem-{{$j}}">
																		<td style="">Tanggapan Auditee</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			@if($sistem->tanggapan == 0)
																				Menyetujui
																			@else 
																				Tidak Menyetujui
																				<br>
																				<br>
																				Catatan Tanggapan Auditee : <br>
																				<p style="text-align: justify;text-justify: inter-word;">{{ $sistem->catatan }}</p>
																			@endif
																		</td>
																	</tr>
																	<tr class="detail-sistem-end-0 detail-sistem-{{$j}}" style="border-bottom: 2px solid black;">
																		<td style="">Tanggal Tindak Lanjut</td>
																		<td style="">:</td>
																		<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
																			{{ DateToString($sistem->tgl) }}
																		</td>
																	</tr>
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-left">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
        .table-responsive{
            overflow-x: auto;
            overflow-y: visible !important;
        }

        .table-responsive .dropdown-menu {
	        position: static !important;
	    }

	    div.dropdown-menu.open{
		  max-width: 300px !important;
		  overflow: hidden;
		}
		ul.dropdown-menu.inner{
		  max-width: 300px !important;
		  overflow-y: auto;
		}
    </style>
@endpush

@push('scripts')
    <script>
    	$('.tanggal').datepicker({
			format: 'dd-mm-yyyy',
			startDate: '-0d',
			orientation: "bottom",
			autoclose:true,
		});
    </script>
    @yield('js-extra')
@endpush

