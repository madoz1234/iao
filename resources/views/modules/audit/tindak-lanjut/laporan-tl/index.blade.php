@extends('layouts.list-tindak-lanjut')
@section('title', 'Laporan Tindak Lanjut')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('filters')
    <div class="form-group">
        <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
        <label class="control-label sr-only" for="filter-tahun">Tahun</label>
	    <input type="text" class="form-control filter-control tahun" name="filter[tahun]" data-post="tahun" placeholder="Tahun">
	    <label class="control-label sr-only" for="filter-tipe">Tipe</label>
	    <select class="select selectpicker filter-control" name="filter[tipe]" data-post="tipe" data-style="btn-default">
	        <option style="font-size: 12px;" value="">Tipe</option>
	        <option style="font-size: 12px;" value="1">Audit Reguler</option>
	        <option style="font-size: 12px;" value="2">Audit Khusus</option>
	    </select>
	    <label class="control-label sr-only" for="filter-kategori">Kategori</label>
	    <select class="select selectpicker filter-control kategori" name="filter[kategori]" data-post="kategori" data-style="btn-default">
	           <option style="font-size: 12px;" value="">Kategori</option>
	           <option style="font-size: 12px;" value="1">Business Unit (BU)</option>
	           <option style="font-size: 12px;" value="2">Corporate Office (CO)</option>
	           <option style="font-size: 12px;" value="3">Project</option>
	           <option style="font-size: 12px;" value="4">Anak Perusahaan</option>
	    </select>
	    <label class="control-label sr-only" for="filter-object_id">Objek Audit</label>
	    <select class="select selectpicker filter-control show-tick object" name="filter[object_id]" data-post="object_id" data-size="3" data-style="btn-default" data-live-search="true" title="Objek Audit">
		</select>
    </div>
@endsection
@push('scripts')
    <script>
    	$('.tahun').datepicker({
            format: "yyyy",
		    viewMode: "years", 
		    minViewMode: "years",
            orientation: "auto",
            autoclose:true
        });

    	$('.kategori').on('change', function(){
			$.ajax({
				url: '{{ url('ajax/option/get-kategoris') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					kategori: this.value
				},
			}).done(function(response) {
				$('select[name="filter[object_id]"]').html(response);
				$('select[name="filter[object_id]"]').selectpicker("refresh");
			})
			.fail(function() {
				console.log("error");
			});
		});
		
		$(document).on('click', '.save2.button', function(e){
            saveData('formData', function(resp){
                var url = "{!! route($routes.'.index') !!}";
	            window.location = url;
				return true;
            });
        });
    	$(document).on('click', '.download', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/download';
            window.location = url;
		});

		$(document).on('click', '.ubah', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/edit';
            window.location = url;
		});

		$(document).on('click', '.laporan-detil', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/laporanDetil';
            window.location = url;
		});

        $(document).on('click', '.resume.button', function(e){
            var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/resume';
            window.location = url;
        });

        $(document).on('click', '.cetak.button', function(e){
            var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/cetak';
            window.open(url, '_blank');
        });

        $(document).on('click', '.detil-project', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/detilProject';
            window.location = url;
		});
		$(document).on('click', '.detil-penugasan', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/detilPenugasan';
            window.location = url;
		});
		$(document).on('click', '.detil-tinjauan', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/detilTinjauan';
            window.location = url;
		});
		$(document).on('click', '.detil-program', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/detilProgram';
            window.location = url;
		});

		$(document).on('click', '.detil-draft', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/detilDraft';
            window.location = url;
		});

		$(document).on('click', '.detil-lha', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/detilLha';
            window.location = url;
		});

		$(document).on('click', '.detil-resume', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/detilResume';
            window.location = url;
		});

		$(document).on('click', '.cetak-audit.button', function(e){
            var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/cetakAudit';
            window.open(url, '_blank');
        });

        $(document).on('click', '.cetak-penugasan.button', function(e){
            var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/cetakPenugasan';
            window.open(url, '_blank');
        });

        $(document).on('click', '.cetak-surat.button', function(e){
            var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/cetakSurat';
            window.open(url, '_blank');
        });

        $(document).on('click', '.cetak-register.button', function(e){
            var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/cetakRegister';
            window.open(url, '_blank');
        });

        $(document).on('click', '.download-rkia.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/downloadAudit';
            window.location = url;
        });

        $(document).on('click', '.download-penugasan.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/downloadPenugasan';
            window.location = url;
        });

        $(document).on('click', '.download-program.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/downloadProgramAudit';
            window.location = url;
        });

        $(document).on('click', '.approve.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/approval';
        	swal({
					title: "Apakah Anda yakin?",
					text: "Data yang sudah disetujui, tidak dapat diubah!",
					icon: "warning",
					buttons: ['Batal', 'Approve'],
					reverseButtons: true
			}).then((result) => {
				if (result) {
					$.ajax({
						url: url,
						type: 'POST',
						data: {
							id : idx,
							tipe : 1,
							'_token' : '{{ csrf_token() }}'
						}
					}).done(function(response) {
						swal({
							icon: 'success',
							title:'Berhasil!',
							text:'Data berhasil disetujui!',
							button: false,
							timer: 1000,
					    }).then((res) => {
							dt1 = $('#dataTable').DataTable();
							dt1.draw();
					    	// callback();
					    })
					}).fail(function(response) {
						console.log(response);
						swal('Data gagal disetujui!', {
							icon: 'error',
					    }).then((res) => {
							dt1 = $('#dataTable').DataTable();
							dt1.draw();
					    	// callback();
					    })
					})

				}
			})
        });

        $(document).on('click', '.reject.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/reject';
            loadModals({
                url: url,
                modal: '#mediumModal',
            }, function(resp){
            	$('#mediumModal').find('.loading.dimmer').hide();
                onShow();
            });
        });
    </script>
    @yield('js-extra')
@endpush