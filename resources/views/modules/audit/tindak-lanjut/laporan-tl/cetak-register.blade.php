<html>
<head>
<style>
        @page {
            margin: 60px 25px;
            counter-reset: page;
        }

       body {
           margin-top: 3cm;
           margin-left: 1.5cm;
           margin-right: 1.5cm;
           margin-bottom: 2cm;
           font-style: normal;
       }

       .ui.table.bordered {
         border: solid black 1px;
         border-collapse: collapse;
         width: 100%;
         font-family : "Arial", "Verdana", sans-serif;
       }

       .ui.table.bordered td {
         border: solid black .5px;
         border-collapse: collapse;
         padding:5px;
       }

       .ui.table.bordered td.center.aligned {
         text-align : center;
       }

       .page-break {
            page-break-after: always;
       }

       .pad{
       	padding-top: 5px;
       	padding-bottom: 5px;
       }

        header {
            position: fixed;
            top: -40px;
            left: 51px;
            right: 74px;
            height: 10px;
            text-align: center;
            font-size: 10px;
        }
		
       main {
         position: sticky;
         font-size : 12px;
         padding-top : -1cm;
         margin-top : 2px;
         border : none;
         width: 100%;
       }

       main p {
         margin-left: 5px;
         margin-right: 5px;
       }
       footer {
           position: fixed;
           bottom: -60px;
           left: 0px;
           right: 0px;
           height: 50px;
           text-align: center;
           line-height: 35px;
       }
       table.page_content {width: 100%; border: none; padding: 1mm; font-size: 10px;font-family : "Arial", "Verdana", sans-serif;}
       table.page_header {width: 100%; border: none; padding: 2mm; font-size: 10px;font-family : "Arial", "Verdana", sans-serif; text-align: left; margin-left: 50px;margin-top: 50px;}
       table.page_content_header {width: 100%; font-size: 10px;border-collapse: collapse;font-family : "Arial", "Verdana", sans-serif;text-align: center; padding-bottom: 1px;
       	padding-top: 1px;}
</style>
</head>
<body>
    <header>
    	<table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;">
    		<tbody>
    			<tr style="">
    				<td style="text-align: left;border: 1px #ffffff;" colspan="2">
    					<table style="">
    						<tbody>
    							<tr>
    								<td style="width: 100px;padding-left: -10px;"><img src="{{ asset('src/img/logo.png') }}" width="85px" height="70px"></td>
    								<td style="width: 360px;"><h2>PT WASKITA KARYA <small>(Persero)</small> Tbk</h2></td>
    								<td style="width: 170px">
    									<table style="text-align: right;font-size: 10px;" class="page_content ui table bordered">
    										<tbody>
    											<tr>
    												<td style="text-align: center;padding-bottom: 10px;">Form Prod IA. 04</td>
    											</tr>
    											<tr>
    												<td style="text-align: center;padding-bottom: 10px;">Edisi : Mei 2019 Revisi : 0</td>
    											</tr>
    										</tbody>
    									</table>
    								</td>
    							</tr>
    						</tbody>
    					</table>
    				</td>
    			</tr>
    		</tbody>
		</table>
  	</header>
  	<br>
	<main>
		<table class="table table-bordered m-t-none" style="font-size: 10px;">
			<tbody>
				<tr>
					<td style="padding-left: 200px;border-right: #ffffff;text-align:center;">REGISTER TINDAK LANJUT</td>
				</tr>
				<tr>
					<td style="padding-left: 200px;text-align:center;border-top: #ffffff;">HASIL AUDIT INTERNAL</td>
				</tr>
				<tr>
					<td style="padding-left: 200px;text-align:center;border-top: #ffffff;">PADA : {{ DateToString($records->created_at) }}</td>
				</tr>
				<tr>
					<td style="padding-left: 200px;text-align:center;border-top: #ffffff;">NO AB : {{ getAb($records->surat->lha->draftkka->program->tinjauan->penugasanaudit->rencanadetail->tipe_object, $records->surat->lha->draftkka->program->tinjauan->penugasanaudit->rencanadetail->object_id) }}</td>
				</tr>
				<tr>
					<td style="padding-left: 200px;text-align:center;border: #ffffff;">TANGGAL AUDIT : {{ DateToString($records->surat->lha->draftkka->program->detailpelaksanaan->first()->tgl).' - '.DateToString($records->surat->lha->draftkka->program->detailpelaksanaan->last()->tgl) }}</td>
				</tr>
			</tbody>
		</table>
		<table class="page_content ui table bordered" style="font-size: 10px;">
			<thead>
				<tr>
					<td style="padding-top: 31px;font-weight: bold;background: #f2f2f2;" rowspan="2">No</td>
					<td style="text-align: center;padding-top: 31px;font-weight: bold;background: #f2f2f2;" rowspan="2">Uraian Temuan</td>
					<td style="text-align: center;padding-top: 31px;font-weight: bold;background: #f2f2f2;" rowspan="2">Rekomendasi</td>
					<td style="text-align: center;padding-top: 31px;font-weight: bold;background: #f2f2f2;" rowspan="2">Tindak Lanjut</td>
					<td style="text-align: center;font-weight: bold;background: #f2f2f2;" colspan="3">Status</td>
				</tr>
				<tr>
					<td style="text-align: center;font-weight: bold;background: #f2f2f2;">Sudah</td>
					<td style="text-align: center;font-weight: bold;background: #f2f2f2;">Belum</td>
					<td style="text-align: center;font-weight: bold;background: #f2f2f2;">Dalam Proses</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="font-weight: bold;">A.</td>
					<td style="text-align: left;font-weight: bold;" colspan="6">Aspek Operasional</td>
				</tr>
				@php
					$i=1;
				@endphp
				@foreach($records->surat->lha->draftkka->detaildraft->where('tipe', 0) as $key => $operasional)
					<tr>
						<input type="hidden" name="data[operasional][{{$operasional->id}}][id]" value="{{$operasional->id}}">
						<td style="padding-top: 33px;">{{ $i }}.</td>
						<td style="text-align: justify;">{{ $operasional->risiko }}</td>
						<td style="text-align: justify;">{{ $operasional->rekomendasi }}</td>
						<td style="" class="field">
							{{ $operasional->tindak_lanjut }}
						</td>
						<td style="text-align: center;" class="field">
							@if($operasional->status_tl == 1) <img src="{{ asset('src/img/check.png') }}" width="12px" height="12px"> @endif
						</td>
						<td style="text-align: center;" class="field">
							@if($operasional->status_tl == 2) <img src="{{ asset('src/img/check.png') }}" width="12px" height="12px"> @endif
						</td>
						<td style="text-align: center;" class="field">
							@if($operasional->status_tl == 3) <img src="{{ asset('src/img/check.png') }}" width="12px" height="12px"> @endif
						</td>
					</tr>
					@php 
						$i++;
					@endphp
				@endforeach
				<tr>
					<td style="font-weight: bold;">B</td>
					<td style="text-align: left;font-weight: bold;" colspan="6">Aspek Keuangan</td>
				</tr>
				@php
					$j=1;
				@endphp
				@foreach($records->surat->lha->draftkka->detaildraft->where('tipe', 1) as $key => $keuangan)
					<tr>
						<input type="hidden" name="data[keuangan][{{$keuangan->id}}][id]" value="{{$keuangan->id}}">
						<td style="padding-top: 33px;">{{ $j }}.</td>
						<td style="text-align: justify;">{{ $keuangan->risiko }}</td>
						<td style="text-align: justify;">{{ $keuangan->rekomendasi }}</td>
						<td style="" class="field">
							{{ $keuangan->tindak_lanjut }}
						</td>
						<td style="text-align: center;" class="field">
							@if($keuangan->status_tl == 1) <img src="{{ asset('src/img/check.png') }}" width="12px" height="12px"> @endif
						</td>
						<td style="text-align: center;" class="field">
							@if($keuangan->status_tl == 2) <img src="{{ asset('src/img/check.png') }}" width="12px" height="12px"> @endif
						</td>
						<td style="text-align: center;" class="field">
							@if($keuangan->status_tl == 3) <img src="{{ asset('src/img/check.png') }}" width="12px" height="12px" style="margin-top: 15px;"> @endif
						</td>
					</tr>
					@php
						$j++;
					@endphp
				@endforeach
				<tr>
					<td style="font-weight: bold;">C.</td>
					<td style="text-align: left;font-weight: bold;" colspan="6">Aspek Sistem</td>
				</tr>
				@php
					$k=1;
				@endphp
				@foreach($records->surat->lha->draftkka->detaildraft->where('tipe', 2) as $key => $sistem)
					<tr>
						<input type="hidden" name="data[sistem][{{$sistem->id}}][id]" value="{{$sistem->id}}">
						<td style="padding-top: 33px;">{{ $k }}.</td>
						<td style="text-align: justify;">{{ $sistem->risiko }}</td>
						<td style="text-align: justify;">{{ $sistem->rekomendasi }}</td>
						<td style="" class="field">
							{{ $sistem->tindak_lanjut }}
						</td>
						<td style="text-align: center;" class="field">
							@if($sistem->status_tl == 1) <img src="{{ asset('src/img/check.png') }}" width="12px" height="12px"> @endif
						</td>
						<td style="text-align: center;" class="field">
							@if($sistem->status_tl == 2) <img src="{{ asset('src/img/check.png') }}" width="12px" height="12px"> @endif
						</td>
						<td style="text-align: center;" class="field">
							@if($sistem->status_tl == 3) <img src="{{ asset('src/img/check.png') }}" width="12px" height="12px" style="margin-top: 15px;"> @endif
						</td>
					</tr>
					@php
						$k++;
					@endphp
				@endforeach
			</tbody>
		</table>
	</main>
</body>
</html>