@extends('layouts.form')
@section('title', 'Detil KKA')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body" style="padding-bottom: 0px;">
		<form action="" method="POST" id="formData">
			@csrf
			<input type="hidden" name="id" value="{{ $record->id }}">
			<div class="form-row">
				<div class="form-group col-md-12">
					<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
						<tr>
							<td style="width: 10%;border: 1px #ffffff;">Tipe</td>
							<td style="width: 1%;border: 1px #ffffff;">:</td>
							<td style="text-align:left;border: 1px #ffffff;" class="field">
								@if($record->program->tinjauan->penugasanaudit->rencanadetail->tipe == 0)
									<span class="label label-primary">Audit</span>
								@elseif($record->program->tinjauan->penugasanaudit->rencanadetail->tipe == 1)
									<span class="label label-success">Kegiatan Konsultasi</span>
								@else 
									<span class="label label-info">Kegiatan Audit Lain - Lain</span>
								@endif
							</td>
						</tr>
						<tr>
							<td style="width: 10%;border: 1px #ffffff;">Kategori</td>
							<td style="width: 1%;border: 1px #ffffff;">:</td>
							<td style="text-align:left;border: 1px #ffffff;" class="field">
								@if($record->program->tinjauan->penugasanaudit->rencanadetail->tipe == 0)
									@if($record->program->tinjauan->penugasanaudit->rencanadetail->tipe_object == 0)
										<span class="label label-success">Business Unit (BU)</span>
									@elseif($record->program->tinjauan->penugasanaudit->rencanadetail->tipe_object == 1)
										<span class="label label-info">Corporate Office (CO)</span>
									@elseif($record->program->tinjauan->penugasanaudit->rencanadetail->tipe_object == 2)
										<span class="label label-default">Project</span>
									@else 
										<span class="label label-primary">Anak Perusahaan</span>
									@endif
								@elseif($record->program->tinjauan->penugasanaudit->rencanadetail->tipe == 1)
									<span class="label label-warning">{{ $record->program->tinjauan->penugasanaudit->rencanadetail->konsultasi->nama }}</span>
								@else 
									<span class="label label-default">{{ $record->program->tinjauan->penugasanaudit->rencanadetail->lain->nama }}</span>
								@endif
							</td>
						</tr>
						<tr>
							<td style="width: 10%;border: 1px #ffffff;">Objek Audit</td>
							<td style="width: 1%;border: 1px #ffffff;">:</td>
							<td style="text-align: left;border: 1px #ffffff;" class="field">

								{!! object_audit($record->program->tinjauan->penugasanaudit->rencanadetail->tipe_object, $record->program->tinjauan->penugasanaudit->rencanadetail->object_id)!!}
							</td>
						</tr>
						<tr>
							<td style="width: 10%;border: 1px #ffffff;">Tanggal Audit</td>
							<td style="width: 1%;border: 1px #ffffff;">:</td>
							<td style="text-align: left;border: 1px #ffffff;" class="field">
								 {{ DateToStringWday($record->program->detailpelaksanaan->min('tgl')) }}  -  {{DateToStringWday($record->program->detailpelaksanaan->max('tgl'))}}
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="form-row">
					<div class="form-group col-md-12">
						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item">
								<a style="font-weight: bold;" class="nav-link active" id="a-tab" data-toggle="tab" href="#a" role="tab" aria-controls="home" aria-selected="true">Operasional</a>
							</li>
							<li class="nav-item">
								<a style="font-weight: bold;" class="nav-link" id="b-tab" data-toggle="tab" href="#b" role="tab" aria-controls="profile" aria-selected="false">Keuangan</a>
							</li>
							<li class="nav-item">
								<a style="font-weight: bold;" class="nav-link" id="c-tab" data-toggle="tab" href="#c" role="tab" aria-controls="messages" aria-selected="false">Sistem</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="a" role="tabpanel" aria-labelledby="a-tab">
								<table id="table-operasional" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
									<tbody class="container-operasional">
										@php 
											$i=0;
										@endphp
										@foreach($record->detaildraft->where('tipe', 0) as $kay => $operasional)
											@if($i == 0)
												<tr class="detail-keuangan-{{$i}}" data-id="{{$i}}">
													<td style="text-align: left;font-weight: bold;" colspan="5">Bidang Audit : Operasional</td>
												</tr>
											@else 
											@endif
											<tr class="data-operasional detail-operasional-{{$i}}" data-id="{{ $i }}" data-kategori="{{ $operasional->kategori_id }}" data-kriteria="{{$operasional->kriteria_id}}">
												<input type="hidden" name="exist_operasional[]" value="{{ $operasional->id }}">
												<input type="hidden" name="fokus_audit[0][detail][{{$i}}][id]" value="{{ $operasional->id }}">
												<td style="text-align: center;width: 20px;">{{ $i+1 }}</td>
												<td style="width: 150px;">Judul</td>
												<td style="width: 2px;">:</td>
												<td style="width: 1600px;text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{getstandarisasi($operasional->standarisasi_id)}}
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="border-bottom: 2px solid black;" rowspan="10"></td>
												<td style="">Kondisi</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													<ol style="padding-left: 10px;">
														{!! getkondisi($operasional->standarisasi_id) !!}
													</ol>
													Catatan Tambahan : <br>
													<p style="text-align: justify;text-justify: inter-word;">{{ $operasional->catatan_kondisi }}</p>
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="">Temuan</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{gettemuan($operasional->temuan_id)}}
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="">Kriteria</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $operasional->kriteria }}
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="">Sebab</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $operasional->sebab }}
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="">Risiko</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $operasional->risiko }}
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="">Kategori Temuan</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{!! getkategoristatus($operasional->kategori_id) !!}
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="">Rekomendasi</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $operasional->rekomendasi }}
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="">Audite</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $operasional->user->name }}
												</td>
											</tr>
											<tr class="detail-operasional-{{$i}}">
												<td style="">Tanggapan Auditee</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													@if($operasional->tanggapan == 0)
														Menyetujui
													@else 
														Tidak Menyetujui
														<br>
														<br>
														Catatan Tanggapan Auditee : <br>
														<p style="text-align: justify;text-justify: inter-word;">{{ $operasional->catatan }}</p>
													@endif
												</td>
											</tr>
											<tr class="detail-operasional-end-0 detail-operasional-{{$i}}" style="border-bottom: 2px solid black;">
												<td style="">Tanggal Tindak Lanjut</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ DateToString($operasional->tgl) }}
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>

							<div class="tab-pane" id="b" role="tabpanel" aria-labelledby="b-tab">
								<table id="table-keuangan" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
									<tbody class="container-keuangan">
										@php 
											$h=0;
										@endphp
										@foreach($record->detaildraft->where('tipe', 1) as $kay => $keuangan)
											@if($h == 0)
												<tr class="detail-keuangan-{{$h}}" data-id="{{$h}}">
													<td style="text-align: left;font-weight: bold;" colspan="5">Bidang Audit : Keuangan</td>
												</tr>
											@else 
											@endif
											<tr class="data-keuangan detail-keuangan-{{$h}}" data-id="{{ $h }}" data-kategori="{{ $keuangan->kategori_id }}" data-kriteria="{{$keuangan->kriteria_id}}">
												<input type="hidden" name="exist_keuangan[]" value="{{ $keuangan->id }}">
												<input type="hidden" name="fokus_audit[1][detail][{{$h}}][id]" value="{{ $keuangan->id }}">
												<td style="text-align: center;width: 20px;">{{ $h+1 }}</td>
												<td style="width: 150px;">Judul</td>
												<td style="width: 2px;">:</td>
												<td style="width: 1600px;text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{getstandarisasi($keuangan->standarisasi_id)}}
												</td>
											</tr>
											<tr class="detail-keuangan-{{$h}}">
												<td style="border-bottom: 2px solid black;" rowspan="10"></td>
												<td style="">Kondisi</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													<ol style="padding-left: 10px;">
														{!! getkondisi($keuangan->standarisasi_id) !!}
													</ol>
													Catatan Tambahan : <br>
													<p style="text-align: justify;text-justify: inter-word;">{{ $keuangan->catatan_kondisi }}</p>
												</td>
											</tr>
											<tr class="detail-keuangan-{{$h}}">
												<td style="">Temuan</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{gettemuan($keuangan->temuan_id)}}
												</td>
											</tr>
											<tr class="detail-keuangan-{{$h}}">
												<td style="">Kriteria</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $keuangan->kriteria }}
												</td>
											</tr>
											<tr class="detail-keuangan-{{$h}}">
												<td style="">Sebab</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $keuangan->sebab }}
												</td>
											</tr>
											<tr class="detail-keuangan-{{$h}}">
												<td style="">Risiko</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $keuangan->risiko }}
												</td>
											</tr>
											<tr class="detail-keuangan-{{$h}}">
												<td style="">Kategori Temuan</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{!! getkategoristatus($keuangan->kategori_id) !!}
												</td>
											</tr>
											<tr class="detail-keuangan-{{$h}}">
												<td style="">Rekomendasi</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $keuangan->rekomendasi }}
												</td>
											</tr>
											<tr class="detail-keuangan-{{$h}}">
												<td style="">Audite</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $keuangan->user->name }}
												</td>
											</tr>
											<tr class="detail-keuangan-{{$h}}">
												<td style="">Tanggapan Auditee</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													@if($keuangan->tanggapan == 0)
														Menyetujui
													@else 
														Tidak Menyetujui
														<br>
														<br>
														Catatan Tanggapan Auditee : <br>
														<p style="text-align: justify;text-justify: inter-word;">{{ $keuangan->catatan }}</p>
													@endif
												</td>
											</tr>
											<tr class="detail-keuangan-end-0 detail-keuangan-{{$h}}" style="border-bottom: 2px solid black;">
												<td style="">Tanggal Tindak Lanjut</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ DateToString($keuangan->tgl) }}
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>

							<div class="tab-pane" id="c" role="tabpanel" aria-labelledby="c-tab">
								<table id="table-sistem" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
									<tbody class="container-sistem">
										@php 
											$j=0;
										@endphp
										@foreach($record->detaildraft->where('tipe', 2) as $koy => $sistem)
											@if($j == 0)
												<tr class="detail-keuangan-{{$j}}" data-id="{{$j}}">
													<td style="text-align: left;font-weight: bold;" colspan="5">Bidang Audit : Sistem</td>
												</tr>
											@else 
											@endif
											<tr class="data-sistem detail-sistem-{{$j}}" data-id="{{ $j }}" data-kategori="{{ $sistem->kategori_id }}" data-kriteria="{{$sistem->kriteria_id}}">
												<input type="hidden" name="exist_sistem[]" value="{{ $sistem->id }}">
												<input type="hidden" name="fokus_audit[1][detail][{{$j}}][id]" value="{{ $sistem->id }}">
												<td style="text-align: center;width: 20px;">{{ $j+1 }}</td>
												<td style="width: 150px;">Judul</td>
												<td style="width: 2px;">:</td>
												<td style="width: 1600px;text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{getstandarisasi($sistem->standarisasi_id)}}
												</td>
											</tr>
											<tr class="detail-sistem-{{$j}}">
												<td style="border-bottom: 2px solid black;" rowspan="10"></td>
												<td style="">Kondisi</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													<ol style="padding-left: 10px;">
														{!! getkondisi($sistem->standarisasi_id) !!}
													</ol>
													Catatan Tambahan : <br>
													<p style="text-align: justify;text-justify: inter-word;">{{ $sistem->catatan_kondisi }}</p>
												</td>
											</tr>
											<tr class="detail-sistem-{{$j}}">
												<td style="">Temuan</td>
												<td style="">:</td>
												<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{gettemuan($sistem->temuan_id)}}
												</td>
											</tr>
											<tr class="detail-sistem-{{$j}}">
												<td style="">Kriteria</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $sistem->kriteria }}
												</td>
											</tr>
											<tr class="detail-sistem-{{$j}}">
												<td style="">Sebab</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $sistem->sebab }}
												</td>
											</tr>
											<tr class="detail-sistem-{{$j}}">
												<td style="">Risiko</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $sistem->risiko }}
												</td>
											</tr>
											<tr class="detail-sistem-{{$j}}">
												<td style="">Kategori Temuan</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{!! getkategoristatus($sistem->kategori_id) !!}
												</td>
											</tr>
											<tr class="detail-sistem-{{$j}}">
												<td style="">Rekomendasi</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $sistem->rekomendasi }}
												</td>
											</tr>
											<tr class="detail-sistem-{{$j}}">
												<td style="">Audite</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ $sistem->user->name }}
												</td>
											</tr>
											<tr class="detail-sistem-{{$j}}">
												<td style="">Tanggapan Auditee</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													@if($sistem->tanggapan == 0)
														Menyetujui
													@else 
														Tidak Menyetujui
														<br>
														<br>
														Catatan Tanggapan Auditee : <br>
														<p style="text-align: justify;text-justify: inter-word;">{{ $sistem->catatan }}</p>
													@endif
												</td>
											</tr>
											<tr class="detail-sistem-end-0 detail-sistem-{{$j}}" style="border-bottom: 2px solid black;">
												<td style="">Tanggal Tindak Lanjut</td>
												<td style="">:</td>
												<td style="text-align: justify;border-right: 1px solid rgba(34,36,38,.1);" class="field">
													{{ DateToString($sistem->tgl) }}
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-left">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
					</div>
				</div>
			</div>
		</form>
    </div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script>
    	$('#myTab li:first-child a').tab('show')
    </script>
    @yield('js-extra')
@endpush

