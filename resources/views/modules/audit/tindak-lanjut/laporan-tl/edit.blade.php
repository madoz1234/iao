@extends('layouts.form')
@if($record->ket_svp)
@section('title', 'Ubah Laporan Tindak Lanjut')
@else 
@section('title', 'Buat Laporan Tindak Lanjut')
@endif
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body" style="padding-bottom: 0px;">
		<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
			@method('PATCH')
			@csrf
			<input type="hidden" name="id" value="{{ $record->id }}">
			<input type="hidden" name="status" value="0">
			<table class="table table-bordered m-t-none" style="font-size: 12px;border: #ffffff;">
				<tbody>
					<tr>
						<td style="width: 600px;border-right: #ffffff;border-top: #ffffff;">&nbsp;</td>
						<td style="width: 600px;border-right: #ffffff;border-top: #ffffff;">REGISTER TINDAK LANJUT</td>
						<td style="border-right: #ffffff;border-top: #ffffff;">&nbsp;</td>
						<td style="border-right: #ffffff;border-top: #ffffff;">&nbsp;</td>
						<td style="border-right: #ffffff;border-top: #ffffff;">&nbsp;</td>
					</tr>
					<tr>
						<td style="border-right: #ffffff;border-top: #ffffff;">&nbsp;</td>
						<td style="border-top: #ffffff;">HASIL AUDIT INTERNAL</td>
						<td style="text-align: center;" colspan="2">Form IA 04</td>
						<td style="text-align: center;border-top: #ffffff;" colspan="2"></td>
					</tr>
					<tr>
						<td style="border-right: #ffffff;border-top: #ffffff;">&nbsp;</td>
						<td style="border-top: #ffffff;">PADA : {{ DateToString(date("d-m-Y")) }}</td>
						<td style="width: 100px;">Edisi : Mei 2019</td>
						<td style="width: 100px;">Revisi 0</td>
						<td style="width: 100px;border-top: #ffffff;"></td>
					</tr>
					<tr>
						<td style="border-right: #ffffff;border-top: #ffffff;">&nbsp;</td>
						<td style="border-top: #ffffff;border-right: #ffffff;">NO AB : {{ getAb($record->surat->lha->draftkka->program->tinjauan->penugasanaudit->rencanadetail->tipe_object, $record->surat->lha->draftkka->program->tinjauan->penugasanaudit->rencanadetail->object_id) }}</td>
						<td style="border-right: #ffffff;" colspan="2" rowspan="2">&nbsp;</td>
						<td style="border-top: #ffffff;">&nbsp;</td>
					</tr>
					<tr>
						<td style="border-right: #ffffff;border-top: #ffffff;">&nbsp;</td>
						<td style="text-align: left;border-top: #ffffff;border-right: #ffffff;">TANGGAL AUDIT : {{ DateToString($record->surat->lha->draftkka->program->detailpelaksanaan->first()->tgl).' - '.DateToString($record->surat->lha->draftkka->program->detailpelaksanaan->last()->tgl) }}</td>
						<td style="text-align: left;border-top: #ffffff;"></td>
					</tr>
				</tbody>
			</table>
			@if($record->ket_svp)
				<p style="padding-left: 14px;color:red;">Status <b>Ditolak</b> SVP pada {{DateToStringWday($record->updated_at)}}</p>
				<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
					<tr>
						<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
							<h5 style="font-weight: bold;font-size: 12px;">Keterangan :</h5>
							<p style="text-align: justify;text-justify: inter-word;">{{ $record->ket_svp }}</p>
						</td>
					</tr>
				</table>
			@endif
			<table class="table table-bordered m-t-none" style="font-size: 12px;border-color: #cccccc;">
				<tbody>
					<tr>
						<td style="width: 20px;padding-top: 31px;font-weight: bold;background: #f5f5ef;border-color: #cccccc;" rowspan="2">No</td>
						<td style="width: 400px;text-align: center;padding-top: 31px;font-weight: bold;background: #f5f5ef;border-color: #cccccc;" rowspan="2">Uraian Temuan</td>
						<td style="width: 400px;text-align: center;padding-top: 31px;font-weight: bold;background: #f5f5ef;border-color: #cccccc;" rowspan="2">Rekomendasi</td>
						<td style="width: 400px;text-align: center;padding-top: 31px;font-weight: bold;background: #f5f5ef;border-color: #cccccc;" rowspan="2">Tindak Lanjut</td>
						<td style="width: 180px;text-align: center;font-weight: bold;background: #f5f5ef;border-color: #cccccc;" colspan="3">Status</td>
					</tr>
					<tr>
						<td style="text-align: center;font-weight: bold;background: #f5f5ef;border-color: #cccccc;">Sudah</td>
						<td style="text-align: center;font-weight: bold;background: #f5f5ef;border-color: #cccccc;">Belum</td>
						<td style="text-align: center;font-weight: bold;background: #f5f5ef;border-color: #cccccc;width: 80px;">Dalam Proses</td>
					</tr>
					<tr>
						<td style="font-weight: bold;border-color: #cccccc;padding-left: 20px;">A.</td>
						<td style="text-align: left;font-weight: bold;border-color: #cccccc;" colspan="6">Aspek Operasional</td>
					</tr>
					@php
						$i=1;
					@endphp
					@foreach($record->surat->lha->draftkka->detaildraft->where('tipe', 0) as $key => $operasional)
						<tr>
							<input type="hidden" name="data[operasional][{{$operasional->id}}][id]" value="{{$operasional->id}}">
							<td style="border-color: #cccccc;padding-top: 33px;padding-left: 20px;">{{ $i }}.</td>
							<td style="text-align: justify;border-color: #cccccc;">{!! readMoreText($operasional->risiko, 200) !!}</td>
							<td style="text-align: justify;border-color: #cccccc;">{!! readMoreText($operasional->rekomendasi, 200) !!}</td>
							<td style="border-color: #cccccc;" class="field">
								<textarea class="form-control" name="data[operasional][{{$operasional->id}}][tl]" id="exampleFormControlTextarea1" placeholder="Tindak Lanjut" rows="2">{{$operasional->tindak_lanjut }}</textarea>
							</td>
							<td style="border-color: #cccccc;" class="field">
								<input type="checkbox" name="data[operasional][{{$operasional->id}}][pilihan]" data-val="operasional" data-cek="satu" data-id="{{$i}}" data-toggle="toggle" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" class="toggle mytoggle operasional-one-{{$i}}" value="1" @if($operasional->status_tl == 1) checked @endif>
							</td>
							<td style="border-color: #cccccc;" class="field">
								<input type="checkbox" name="data[operasional][{{$operasional->id}}][pilihan]" data-val="operasional" data-cek="dua" data-id="{{$i}}" data-toggle="toggle" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" class="toggle mytoggle operasional-two-{{$i}}" value="2" @if($operasional->status_tl == 2) checked @endif>
							</td>
							<td style="border-color: #cccccc;" class="field">
								<input type="checkbox" name="data[operasional][{{$operasional->id}}][pilihan]" data-val="operasional" data-cek="tiga" data-id="{{$i}}" data-toggle="toggle" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" class="toggle mytoggle operasional-three-{{$i}}" value="3" @if($operasional->status_tl == 3) checked @endif>
							</td>
						</tr>
						@php 
							$i++;
						@endphp
					@endforeach
					<tr>
						<td style="font-weight: bold;border-color: #cccccc;padding-left: 20px;">B</td>
						<td style="text-align: left;font-weight: bold;border-color: #cccccc;" colspan="6">Aspek Keuangan</td>
					</tr>
					@php
						$j=1;
					@endphp
					@foreach($record->surat->lha->draftkka->detaildraft->where('tipe', 1) as $key => $keuangan)
						<tr>
							<input type="hidden" name="data[keuangan][{{$keuangan->id}}][id]" value="{{$keuangan->id}}">
							<td style="border-color: #cccccc;padding-top: 33px;padding-left: 20px;">{{ $j }}.</td>
							<td style="text-align: justify;border-color: #cccccc;">{!! readMoreText($keuangan->risiko, 200) !!}</td>
							<td style="text-align: justify;border-color: #cccccc;">{!! readMoreText($keuangan->rekomendasi, 200) !!}</td>
							<td style="border-color: #cccccc;" class="field">
								<textarea class="form-control" name="data[keuangan][{{$keuangan->id}}][tl]" id="exampleFormControlTextarea1" placeholder="Tindak Lanjut" rows="2">{{$keuangan->tindak_lanjut }}</textarea>
							</td>
							<td style="border-color: #cccccc;" class="field">
								<input type="checkbox" name="data[keuangan][{{$keuangan->id}}][pilihan]" data-val="keuangan" data-cek="satu" data-id="{{$j}}" data-toggle="toggle" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" class="toggle mytoggle keuangan-one-{{$j}}" value="1" @if($keuangan->status_tl == 1) checked @endif>
							</td>
							<td style="border-color: #cccccc;" class="field">
								<input type="checkbox" name="data[keuangan][{{$keuangan->id}}][pilihan]" data-val="keuangan" data-cek="dua" data-id="{{$j}}" data-toggle="toggle" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" class="toggle mytoggle keuangan-two-{{$j}}" value="2" @if($keuangan->status_tl == 2) checked @endif>
							</td>
							<td style="border-color: #cccccc;" class="field">
								<input type="checkbox" name="data[keuangan][{{$keuangan->id}}][pilihan]" data-val="keuangan" data-cek="tiga" data-id="{{$j}}" data-toggle="toggle" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" class="toggle mytoggle keuangan-three-{{$j}}" value="3" @if($keuangan->status_tl == 3) checked @endif>
							</td>
						</tr>
						@php
							$j++;
						@endphp
					@endforeach
					<tr>
						<td style="font-weight: bold;border-color: #cccccc;padding-left: 20px;">C.</td>
						<td style="text-align: left;font-weight: bold;border-color: #cccccc;" colspan="6">Aspek Sistem</td>
					</tr>
					@php
						$k=1;
					@endphp
					@foreach($record->surat->lha->draftkka->detaildraft->where('tipe', 2) as $key => $sistem)
						<tr>
							<input type="hidden" name="data[sistem][{{$sistem->id}}][id]" value="{{$sistem->id}}">
							<td style="border-color: #cccccc;padding-top: 33px;padding-left: 20px;">{{ $k }}.</td>
							<td style="text-align: justify;border-color: #cccccc;">{!! readMoreText($sistem->risiko, 200) !!}</td>
							<td style="text-align: justify;border-color: #cccccc;">{!! readMoreText($sistem->rekomendasi, 200) !!}</td>
							<td style="border-color: #cccccc;" class="field">
								<textarea class="form-control" name="data[sistem][{{$sistem->id}}][tl]" id="exampleFormControlTextarea1" placeholder="Tindak Lanjut" rows="2">{{$sistem->tindak_lanjut }}</textarea>
							</td>
							<td style="border-color: #cccccc;" class="field">
								<input type="checkbox" name="data[sistem][{{$sistem->id}}][pilihan]" data-val="sistem" data-cek="satu" data-id="{{$k}}" data-toggle="toggle" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" class="toggle mytoggle sistem-one-{{$k}}" value="1" @if($sistem->status_tl == 1) checked @endif>
							</td>
							<td style="border-color: #cccccc;" class="field">
								<input type="checkbox" name="data[sistem][{{$sistem->id}}][pilihan]" data-val="sistem" data-cek="dua" data-id="{{$k}}" data-toggle="toggle" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" class="toggle mytoggle sistem-two-{{$k}}" value="2" @if($sistem->status_tl == 2) checked @endif>
							</td>
							<td style="border-color: #cccccc;" class="field">
								<input type="checkbox" name="data[sistem][{{$sistem->id}}][pilihan]" data-val="sistem" data-cek="tiga" data-id="{{$k}}" data-toggle="toggle" data-on="Ya" data-off="Tidak" data-onstyle="info" data-style="ios" class="toggle mytoggle sistem-three-{{$k}}" value="3" @if($sistem->status_tl == 3) checked @endif>
							</td>
						</tr>
						@php
							$k++;
						@endphp
					@endforeach
				</tbody>
			</table>
			<p>&nbsp;</p>
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-right">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
						<button type="button" class="btn btn-simpan save as page">Submit</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
        .table-responsive{
            overflow-x: auto;
            overflow-y: visible !important;
        }

        .table-responsive .dropdown-menu {
	        position: static !important;
	    }

	    div.dropdown-menu.open{
		  max-width: 300px !important;
		  overflow: hidden;
		}
		ul.dropdown-menu.inner{
		  max-width: 300px !important;
		  overflow-y: auto;
		}
		.toggle{
			top: 15px;
		}
	    </style>
@endpush

@push('scripts')
    <script>
    	$("[data-toggle='toggle']").bootstrapToggle('destroy')                 
		$("[data-toggle='toggle']").bootstrapToggle();
		  $('.mytoggle').change(function() {
		  	var id = $(this).data('id');
		  	var val = $(this).data('val');
		  	var cek = $(this).data('cek');
		  	if ($(this).is(':checked')) {
		  		console.log(id, val, cek)
		  		if(val == 'operasional'){
		  			if(cek == 'satu'){
		  				$('.operasional-two-'+id).prop('checked', false).change();
		  				$('.operasional-three-'+id).prop('checked', false).change();
		  			}else if(cek == 'dua'){
		  				$('.operasional-one-'+id).prop('checked', false).change();
		  				$('.operasional-three-'+id).prop('checked', false).change();
		  			}else{
		  				$('.operasional-one-'+id).prop('checked', false).change();
		  				$('.operasional-two-'+id).prop('checked', false).change();
		  			}
		  		}else if(val == 'keuangan'){
		  			if(cek == 'satu'){
		  				$('.keuangan-two-'+id).prop('checked', false).change();
		  				$('.keuangan-three-'+id).prop('checked', false).change();
		  			}else if(cek == 'dua'){
		  				$('.keuangan-one-'+id).prop('checked', false).change();
		  				$('.keuangan-three-'+id).prop('checked', false).change();
		  			}else{
		  				$('.keuangan-one-'+id).prop('checked', false).change();
		  				$('.keuangan-two-'+id).prop('checked', false).change();
		  			}
		  		}else{
		  			if(cek == 'satu'){
		  				$('.sistem-two-'+id).prop('checked', false).change();
		  				$('.sistem-three-'+id).prop('checked', false).change();
		  			}else if(cek == 'dua'){
		  				$('.sistem-one-'+id).prop('checked', false).change();
		  				$('.sistem-three-'+id).prop('checked', false).change();
		  			}else{
		  				$('.sistem-one-'+id).prop('checked', false).change();
		  				$('.sistem-two-'+id).prop('checked', false).change();
		  			}
		  		}
		    }
		  });
    </script>
    @yield('js-extra')
@endpush

