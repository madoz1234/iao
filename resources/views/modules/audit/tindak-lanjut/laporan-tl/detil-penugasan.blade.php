@extends('layouts.form')
@section('title', 'Detil Surat Penugasan')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body" style="padding-bottom: 0px;">
		<form action="#" method="POST" id="formData">
			@method('PATCH')
			@csrf
			<div class="panel panel-default data-form">
				<table class="table" style="font-size: 12px;">
					<tbody>
						<tr>
							<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">Ketua Tim</td>
							<td style="width:2px;border: 1px #ffffff;">:</td>
							<td style="border: 1px #ffffff;" class="field">
								{{ $record->user->name }}
							</td>
						</tr>
						<tr>
							<td style="border: 1px #ffffff;">Auditor Operasional</td>
							<td style="border: 1px #ffffff;">:</td>
							<td style="border: 1px #ffffff;" class="field">
								{{ $record->anggota->where('fungsi', 1)->first()->user->name }}
							</td>
						</tr>
						<tr>
							<td style="border: 1px #ffffff;">Auditor Keuangan</td>
							<td style="border: 1px #ffffff;">:</td>
							<td style="border: 1px #ffffff;" class="field">
								{{ $record->anggota->where('fungsi', 2)->first()->user->name }}
							</td>
						</tr>
						<tr>
							<td style="border: 1px #ffffff;">Auditor Sistem</td>
							<td style="border: 1px #ffffff;">:</td>
							<td style="border: 1px #ffffff;" class="field">
								{{ $record->anggota->where('fungsi', 3)->first()->user->name }}
							</td>
						</tr>
					</tbody>
				</table>
				<div class="tab-pane" id="b" role="tabpanel" aria-labelledby="b-tab">
					<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
						<tr>
							<td scope="col-md-12" colspan="3" style="text-align: left;">
								<h5 style="text-align: center;font-weight: bold;">JADWAL</h5>
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr style="text-align: center;">
										<td scope="col" style="text-align: left;border: 1px #ffffff;">
											<table id="table-pelaksanaan" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
												<thead>
													<tr>
														<th style="text-align: center;" rowspan="2">Hari</th>
														<th style="text-align: center;" rowspan="2">Tanggal</th>
														<th style="text-align: center; text-align: center;" colspan="2">Waktu</th>
														<th style="text-align: center;border-left: 1px solid rgba(34,36,38,.1);" rowspan="2">Item</th>
													</tr>
													<tr>
														<th style="text-align: center;border-top: 1px solid rgba(34,36,38,.1);">Mulai</th>
														<th style="text-align: center;border-top: 1px solid rgba(34,36,38,.1);">Selesai</th>
													</tr>
												</thead>
												<tbody class="container-pelaksanaan">
													@foreach($record->jadwal as $kiy => $pelaksanaan)
													<tr class="detail_cek data-pelaksanaan-{{$kiy}}" data-id="{{$kiy}}">
														<td class="data_rowspan-{{$kiy}} numboor-{{$kiy}}" style="text-align: center;width: 100px;" rowspan="{{$pelaksanaan->detail->count()}}">{{ romawi(($kiy+1)) }}</td>
														<td class="data_rowspan-{{ $kiy }} field" style="text-align: center;width: 200px;" rowspan="{{$pelaksanaan->detail->count()}}">
															{{DateToString($pelaksanaan->tgl)}}
														</td>
														<td style="text-align: center;width: 150px;" class="field">
															{{$pelaksanaan->detail->first()->mulai}}
														</td>
														<td style="text-align: center;width: 150px;" class="field">
															{{$pelaksanaan->detail->first()->selesai}}
														</td>
														<td style="text-align: left;width: 800px;text-align: justify;text-justify: inter-word;" class="field">
															{{$pelaksanaan->detail->first()->keterangan}}
														</td>
													</tr>
													@foreach($pelaksanaan->detail as $koy => $detail)
													@if($koy > 0)
													<tr class="detail-pelaksanaan-{{ $kiy }} detail-{{ $kiy }}-{{ $koy }}">
														<td style="text-align: center;width: 150px;" class="field">
															{{$detail->mulai}}
														</td>
														<td style="text-align: center;width: 150px;" class="field">
															{{$detail->selesai}}
														</td>
														<td style="text-align: left;width: 800px;text-align: justify;text-justify: inter-word;" class="field">
															{{$detail->keterangan}}
														</td>
													</tr>
													@endif
													@endforeach
													@endforeach
												</tbody>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-left">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
        .table-responsive{
            overflow-x: auto;
            overflow-y: visible !important;
        }

        .table-responsive .dropdown-menu {
	        position: static !important;
	    }
		
	    /*.datepicker-days > table > tbody > tr:hover {
		    background-color: #808080;
		}*/
    </style>
@endpush