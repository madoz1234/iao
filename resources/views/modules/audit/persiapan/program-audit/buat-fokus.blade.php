@extends('layouts.form')
@section('title', 'Ubah Program Audit '.$record->penugasanaudit->rencanadetail->rencanaaudit->tahun)
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
    <div class="panel-body" style="padding-bottom: 0px;">
		<form action="{{ route($routes.'.updateFokus', $record->id) }}" method="POST" id="formData">
			@method('PATCH')
		    @csrf
		    <div class="loading dimmer padder-v" style="display: none;">
			    <div class="loader"></div>
			</div>
			@if($record->ket_svp)
				<p style="padding-left: 14px;color:red;">Status <b>Ditolak SVP</b> pada {{DateToStringWday($record->updated_at)}}</p>
				<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
					<tr>
						<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
							<h5 style="font-weight: bold;font-size: 12px;">Keterangan :</h5>
							<p style="text-align: justify;text-justify: inter-word;">{{ $record->ket_svp }}</p>
						</td>
					</tr>
				</table>
			@endif
			<input type="hidden" name="id" value="{{ $record->id }}">
			<input type="hidden" name="penugasan_id" value="{{ $record->penugasan_id }}">
			<input type="hidden" name="tahun" value="{{ $record->penugasanaudit->rencanadetail->rencanaaudit->tahun }}">
			<input type="hidden" name="status" value="0">
			<input type="hidden" name="tipe" value="{{ $user }}">
			@php 
				$data_operasional = array();
				foreach($record->detailanggota->where('fungsi', 1) as $operasional) {
				 $data_operasional[] = $operasional->user->name;
				}

				$data_keuangan = array();
				foreach($record->detailanggota->where('fungsi', 2) as $keuangan) {
				 $data_keuangan[] = $keuangan->user->name;
				}

				$data_sistem = array();
				foreach($record->detailanggota->where('fungsi', 3) as $sistem) {
				 $data_sistem[] = $sistem->user->name;
				}
			@endphp
			<div class="panel-body" style="padding-bottom: 0px;">
				<div class="form-row">
					<div class="form-group col-md-12">
						<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
							<tr>
								<td style="width:200px;border: 1px #ffffff;border: 1px #ffffff;">Tahun</td>
								<td style="width:2px;border: 1px #ffffff;">:</td>
								<td style="text-align:left;border: 1px #ffffff;" class="field">
									{{ $record->penugasanaudit->rencanadetail->rencanaaudit->tahun }}
									<input type="hidden" name="tahun" value="{{ $record->penugasanaudit->rencanadetail->rencanaaudit->tahun }}">
								</td>
							</tr>
							<tr>
								<td style="border: 1px #ffffff;width: 14%;">Rencana Pelaksanaan</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="text-align:left;border: 1px #ffffff;" class="field">
									{{ $record->penugasanaudit->rencanadetail->rencana }}
								</td>
							</tr>
							<tr>
								<td style="border: 1px #ffffff;">Tipe</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="text-align:left;border: 1px #ffffff;" class="field">
									@if($record->penugasanaudit->rencanadetail->tipe == 0)
										<span class="label label-primary">Audit</span>
									@elseif($record->penugasanaudit->rencanadetail->tipe == 1)
										<span class="label label-success">Kegiatan Konsultasi</span>
									@else
										<span class="label label-info">Kegiatan Audit Lain - Lain</span>
									@endif
								</td>
							</tr>
							<tr>
								<td style="border: 1px #ffffff;">Kategori</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="text-align:left;border: 1px #ffffff;" class="field">
									@if($record->penugasanaudit->rencanadetail->tipe == 0)
										@if($record->penugasanaudit->rencanadetail->tipe_object == 0)
											<span class="label label-success">Business Unit (BU)</span>
										@elseif($record->penugasanaudit->rencanadetail->tipe_object == 1)
											<span class="label label-info">Corporate Office (CO)</span>
										@elseif($record->penugasanaudit->rencanadetail->tipe_object == 2)
											<span class="label label-default">Project</span>
										@else
											<span class="label label-primary">Anak Perusahaan</span>
										@endif
									@elseif($record->penugasanaudit->rencanadetail->tipe == 1)
										<span class="label label-warning">{{ $record->penugasanaudit->rencanadetail->konsultasi->nama }}</span>
									@else
										<span class="label label-default">{{ $record->penugasanaudit->rencanadetail->lain->nama }}</span>
									@endif
								</td>
							</tr>
							<tr class="detil">
								<td style="border: 1px #ffffff;">Objek Audit</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="text-align:left;border: 1px #ffffff;">
									{{ object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id) }}
								</td>
							</tr>
							<tr>
								<td style="width: 10%;border: 1px #ffffff;">Ketua Tim</td>
								<td style="width: 1%;border: 1px #ffffff;">:</td>
								<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
									{{ $record->penugasanaudit->anggota->where('fungsi', 0)->where('data_id', $record->penugasanaudit->rencanadetail->id)->first()->user->name }}
								</td>
							</tr>
							<tr>
								<td style="width: 10%;border: 1px #ffffff;">Auditor Operasional</td>
								<td style="width: 1%;border: 1px #ffffff;">:</td>
								<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
									<div>
										{{ implode(", ",$data_operasional) }}
									</div>
								</td>
							</tr>
							<tr>
								<td style="width: 10%;border: 1px #ffffff;">Auditor Keuangan</td>
								<td style="width: 1%;border: 1px #ffffff;">:</td>
								<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
									<div>
										{{ implode(", ",$data_keuangan) }}
									</div>
								</td>
							</tr>
							<tr>
								<td style="width: 10%;border: 1px #ffffff;">Auditor Sistem</td>
								<td style="width: 1%;border: 1px #ffffff;">:</td>
								<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
									<div>
										{{ implode(", ",$data_sistem) }}
									</div>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="panel-body" style="margin-top: -14px;">
				<div class="form-row">
					<div class="form-group col-md-12">
						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="a-tab" data-toggle="tab" href="#a" role="tab" aria-controls="home" aria-selected="true">A. RUANG LINGKUP</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="b-tab" data-toggle="tab" href="#b" role="tab" aria-controls="profile" aria-selected="false">B. SASARAN</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="c-tab" data-toggle="tab" href="#c" role="tab" aria-controls="messages" aria-selected="false">C. JADWAL AUDIT</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="d-tab" data-toggle="tab" href="#d" role="tab" aria-controls="settings" aria-selected="false">D. FOKUS AUDIT DAN LANGKAH KERJA</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="a" role="tabpanel" aria-labelledby="a-tab">
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr>
										<td scope="col-md-12" colspan="3" style="text-align: left;" class="field">
											<h5 style="font-weight: bold;font-size: 12px;">A. RUANG LINGKUP</h5>
											<p style="text-align: justify;text-justify: inter-word;">{{ $record->ruang_lingkup }}</p>
										</td>
									</tr>
								</table>
								<div class="form-group col-md-12">
									<div class="text-right">
										<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
										<button type="button" class="btn btn-info page1">Selanjutnya</button>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="b" role="tabpanel" aria-labelledby="b-tab">
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr>
										<td scope="col-md-12" colspan="3" style="text-align: left;" class="field">
											<h5 style="font-weight: bold;font-size: 12px;">B. SASARAN</h5>
											<p style="text-align: justify;text-justify: inter-word;">{{ $record->sasaran }}</p>
										</td>
									</tr>
								</table>
								<div class="form-group col-md-12">
									<div class="text-right">
										<button type="button" class="btn btn-cancel kembali1">Sebelumnya</button>
										<button type="button" class="btn btn-info page2">Selanjutnya</button>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="c" role="tabpanel" aria-labelledby="c-tab">
		    					<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;">
									<tr>
										<td scope="col-md-12" colspan="3" style="text-align: left;">
											<h5 style="font-weight: bold;font-size: 12px;">C. JADWAL AUDIT</h5>
											<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
												<tr style="text-align: center;">
													<td scope="col" style="text-align: left;width: 100%; font-weight: bold;border: 1px #ffffff;">C1. PERSIAPAN
													</td>
												</tr>
												<tr style="text-align: center;">
													<td scope="col" style="text-align: left;border: 1px #ffffff;">
														<table id="table-persiapan" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
															<thead>
																<th style="width: 50px;">No</th>
																<th style="text-align: center;">Persiapan</th>
															</thead>
															<tbody class="container-persiapan">
																	@foreach($record->detailjadwal as $kiy => $jadwal)
																		<tr class="data-persiapan-{{ $kiy+1 }}" data-id="{{ $kiy+1 }}">
																			<td style="text-align: center;margin-top: 9px;padding-top: 21px;">{{ $kiy+1 }}</td>
																			<td class="field">
																				<p style="text-align: justify;text-justify: inter-word;">{{ $jadwal->keterangan }}</p>
																			</td>
																		</tr>
																	@endforeach
															</tbody>
														</table>
													</td>
												</tr>
												<tr style="text-align: center;">
													<td scope="col" style="text-align: left;width: 100%; font-weight: bold;border: 1px #ffffff;font-size: 12px;">C2. PELAKSANAAN AUDIT
													</td>
												</tr>
												<tr style="text-align: center;">
													<td scope="col" style="text-align: left;border: 1px #ffffff;">
														@foreach($record->detailpelaksanaan->groupBy('tgl_id') as $a => $cek)
															<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
																<h6 style="font-weight: bold;">{{ object_audit(getRencana($cek->first()->tgl_id)[0], getRencana($cek->first()->tgl_id)[1]) }} -	AB : {{ getAb(getRencana($cek->first()->tgl_id)[0], getRencana($cek->first()->tgl_id)[1]) }}</h6>
																<tr style="text-align: center;">
																	<td scope="col" style="text-align: left;border: 1px #ffffff;">
																		<table id="table-pelaksanaan-{{ $cek->first()->tgl_id }}" data-id="{{ $cek->first()->tgl_id }}" class="table table-bordered m-t-none cek-table" style="width: 100%;font-size: 12px;">
																			<thead>
																				<tr>
																					<th style="text-align: center;" rowspan="2">Hari</th>
																					<th style="text-align: center;" rowspan="2">Tanggal</th>
																					<th style="text-align: center; text-align: center;" colspan="2">Waktu</th>
																					<th style="text-align: center;border-left: 1px solid rgba(34,36,38,.1);" rowspan="2">Item</th>
																				</tr>
																				<tr>
																					<th style="text-align: center;border-top: 1px solid rgba(34,36,38,.1);">Mulai</th>
																					<th style="text-align: center;border-top: 1px solid rgba(34,36,38,.1);">Selesai</th>
																				</tr>
																			</thead>
																			<tbody class="container-pelaksanaan-{{ $cek->first()->tgl_id }}">
																				@php 
																					$iii=0;
																				@endphp
																				@foreach($record->detailpelaksanaan->where('tgl_id', $cek->first()->tgl_id) as $kiy => $pelaksanaan)
																					<tr class="detail_cek-{{ $cek->first()->tgl_id }} data-pelaksanaan-{{ $cek->first()->tgl_id }}-{{$iii}}" data-id="{{$iii}}" data-parent="{{ $cek->first()->tgl_id }}">
																						<td class="data_rowspan-{{ $cek->first()->tgl_id }}-{{$iii}} numboor-{{ $cek->first()->tgl_id }}-{{$iii}}" style="text-align: center;width: 100px;" rowspan="{{$pelaksanaan->detailpelaksanaan->count()}}">{{ romawi(($iii+1)) }}</td>
																						<td class="data_rowspan-{{ $cek->first()->tgl_id }}-{{ $iii }} field" style="text-align: center;width: 200px;" rowspan="{{$pelaksanaan->detailpelaksanaan->count()}}"> 
																							{{$pelaksanaan->tgl}}
																						</td>
																						<td style="text-align: center;width: 150px;" class="field">
																							{{ $pelaksanaan->detailpelaksanaan->first()->mulai }}
																						</td>
																						<td style="text-align: center;width: 150px;" class="field">
																							{{ $pelaksanaan->detailpelaksanaan->first()->selesai }}
																						</td>
																						<td style="text-align: left;width: 800px;text-align: justify;text-justify: inter-word;" class="field">
																							{{ $pelaksanaan->detailpelaksanaan->first()->keterangan }}
																						</td>
																					</tr>
																					@php 
																						$jjj=0;
																					@endphp
																					@foreach($pelaksanaan->detailpelaksanaan as $koy => $detail)
																						@if($jjj > 0)
																							<tr class="detail-pelaksanaan-{{ $cek->first()->tgl_id }}-{{ $iii }} detail-{{ $cek->first()->tgl_id }}-{{ $iii }}-{{ $jjj }}">
																								<td style="text-align: center;width: 150px;" class="field">
																									{{ $detail->mulai }}
																								</td>
																								<td style="text-align: center;width: 150px;" class="field">
																									{{ $detail->selesai }}
																								</td>
																								<td style="text-align: left;width: 800px;text-align: justify;text-justify: inter-word;" class="field">
																									{{ $detail->keterangan }}
																								</td>
																							</tr>
																						@endif
																							@php 
																								$jjj++;
																							@endphp
																					@endforeach
																					@php 
																						$iii++;
																					@endphp
																			@endforeach
																			</tbody>
																		</table>
																	</td>
																</tr>
															</table>
														@endforeach
													</td>
												</tr>
												<tr style="text-align: center;">
													<td scope="col" style="text-align: left;width: 100%; font-weight: bold;border: 1px #ffffff;font-size: 12px;">C3. PENYELESAIAN TUGAS AUDIT
													</td>
												</tr>
												<tr style="text-align: center;">
													<td scope="col" style="text-align: left;border: 1px #ffffff;">
														<table id="table-penyelesaian" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
															<thead>
																<tr>
																	<th style="text-align: center; text-align: center;border-bottom: 1px solid rgba(34,36,38,.1);" rowspan="2">No</th>
																	<th style="text-align: center; text-align: center;border-bottom: 1px solid rgba(34,36,38,.1);" colspan="2">Tanggal</th>
																	<th style="text-align: center;border-left: 1px solid rgba(34,36,38,.1);" rowspan="2">Item</th>
																</tr>
																<tr>
																	<th style="text-align: center;">Mulai</th>
																	<th style="text-align: center;">Selesai</th>
																</tr>
															</thead>
															<tbody class="container-penyelesaian">
																@foreach($record->detailselesai as $kuy => $selesai)
																	<tr class="data-penyelesaian-{{$kuy+1}}" data-id="{{$kuy+1}}">
																		<td style="text-align: center;width: 50px;" class="numbor-{{$kuy+1}}">{{$kuy+1}}</td>
																		<td style="text-align: center;width: 250px;" class="field">
																			{{DateToStrings($selesai->tgl_mulai)}}
																		</td>
																		<td style="text-align: center;width: 250px;" class="field">
																			{{DateToStrings($selesai->tgl_selesai)}}
																		</td>
																		<td style="text-align: left;text-align: justify;text-justify: inter-word;" class="field">{{$selesai->keterangan}}
																		</td>
																	</tr>
																@endforeach
															</tbody>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<div class="form-group col-md-12">
									<div class="text-right">
										<button type="button" class="btn btn-cancel kembali2">Sebelumnya</button>
										<button type="button" class="btn btn-info page3">Selanjutnya</button>
									</div>
								</div>
		    				</div>
							<div class="tab-pane" id="d" role="tabpanel" aria-labelledby="d-tab">
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr>
										<td scope="col-md-12" colspan="3" style="text-align: left;">
											<h5 style="font-weight: bold;">D. FOKUS AUDIT DAN LANGKAH KERJA</h5>
											<table id="table-fokus" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
												<thead>
													<tr>
														<th style="text-align: center;" rowspan="2">No</th>
														<th style="text-align: center;" colspan="2" rowspan="2">Uraian</th>
														<th style="text-align: center;border-bottom: 1px solid rgba(34,36,38,.1);border-left: 1px solid rgba(34,36,38,.1);" colspan="2">Waktu Audit</th>
														<th style="text-align: center;border-right: 1px solid rgba(34,36,38,.1);" rowspan="2" colspan="3">Keterangan</td>
														</tr>
														<tr>
															<th style="">Rencana</th>
															<th style="">Realisasi</th>
														</tr>
													</thead>
													<tbody>
														{{-- OPERASIONAL --}}
														@if($user == 1)
															<tr>
																<td style="width: 20px;text-align: center;font-weight: bold;">I</td>
																<td style="text-align: left;font-weight: bold;border-right: 1px #ffffff;" colspan="5">OPERASIONAL</td>
																<td style="text-align: center;font-weight: bold;width:50px;border-right: 1px solid rgba(34,36,38,.1);border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
																</td>
															</tr>
															@php 
																$i=1;
															@endphp
															@if(count($record->detailkerja->where('bidang', 1)) > 0)
																@foreach($record->detailkerja->where('bidang', 1) as $kiy => $operasional)
																	@php
																		if($operasional->fokus_audit_id == 1000){
																			$row=0;
																		}else{
																			if(!empty($operasional->fokusaudit->langkahkerja->first()->detail)){
																				$row = $operasional->fokusaudit->langkahkerja->first()->detail->count();
																			}else{
																				$row=0;
																			}
																		}
																	@endphp
																	@if($operasional->fokus_audit_id == 1000)
																		<tr class="data_operasional data_fokus_audit_operasional_{{$kiy}}" data-id="{{$kiy}}">
																			<td class="for_rowspan_operasional_{{$kiy}}" style="text-align: center;font-weight: bold;" rowspan="{{$row+3}}">
																				<label style="margin-top: 7px;" class="numbur-operasional-{{$kiy}}">{{romawi($kiy+1)}}</label>
																			</td>
																			<td style="width: 20px;" rowspan="3">&nbsp;</td>
																			<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="4">Fokus Audit :</td>
																			<td style="border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
																			</td>
																		</tr>
																		<tr class="detail_fokus_audit_operasional_{{$kiy}}" data-id="{{$kiy}}" style="border-bottom: 1px solid rgba(34,36,38,.1);">
																			<td style="width: 800px;" class="field">
																				NONE
																				<p>Dengan memilih NONE, <span style="color: red;">Maka semua Fokus Audit otomatis dikosongkan.</span></p>
																			</td>
																			<td style="width: 150px;" class="field">
																				-
																			</td>
																			<td style="width: 150px;" class="field">
																				-
																			</td>
																			<td colspan="2" style="border-right: 1px solid rgba(34,36,38,.1);text-align: left;text-align: justify;text-justify: inter-word;">
																				-
																			</td>
																		</tr>
																		<tr class="langkah_kerja_operasional_{{$kiy}}">
																			<td style="text-align: left;" colspan="6">Langkah Kerja</td>
																		</tr>
																	@else
																		<tr class="data_operasional data_fokus_audit_operasional_{{$kiy}}" data-id="{{$kiy}}">
																			<td class="for_rowspan_operasional_{{$kiy}}" style="text-align: center;font-weight: bold;" rowspan="{{$row+3}}">
																				<label style="margin-top: 7px;" class="numbur-operasional-{{$kiy}}">{{romawi($kiy+1)}}</label>
																			</td>
																			<td style="width: 20px;" rowspan="3">&nbsp;</td>
																			<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="4">Fokus Audit :</td>
																			<td style="border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
																				@if($kiy == 0)
																					<button class="btn btn-outline-success btn-sm tambah_langkah_operasional" data-id="{{$kiy}}" type="button" style="border-radius: 20px;margin-top: -4px;margin-bottom: -4px;"><i class="fa fa-plus"></i></button>
																				@else 
																					<button class="btn btn-outline-danger btn-sm hapus_langkah_operasional" type="button" style="border-radius:20px;" data-id={{$kiy}}><i class="fa fa-remove"></i></button>
																				@endif
																			</td>
																		</tr>
																		<tr class="detail_fokus_audit_operasional_{{$kiy}}" data-id="{{$kiy}}" style="border-bottom: 1px solid rgba(34,36,38,.1);">
																			<td style="width: 800px;" class="field">
																				<select data-width="100%" class="selectpicker cari" data-id="{{$kiy}}" data-tipe="1" name="detail[1000][fokus_audit][0][detail][{{$kiy}}][bidang]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
																					@foreach(App\Models\Master\FokusAudit::where('bidang', 1)->get() as $fokus)
																					<option value="{{ $fokus->id }}" @if($fokus->id == $operasional->fokus_audit_id) selected @endif>{{ $fokus->audit }}</option>
																					@endforeach
																					<option value="1000">NONE</option>
																				</select>
																				<p class="notif_operasional_{{$kiy}}" style="display: none;">Dengan memilih NONE, <span style="color: red;">Maka semua Fokus Audit otomatis dikosongkan.</span></p>
																			</td>
																			<td style="width: 150px;" class="field">
																				<input class="form-control tgl_operasional rencana_operasional_{{$kiy}}" id="tanggal" name="detail[1000][fokus_audit][0][detail][{{$kiy}}][rencana]" placeholder="Rencana" type="text" value="{{ $operasional->rencana }}"/>
																			</td>
																			<td style="width: 150px;" class="field">
																				<input class="form-control tgl_end_operasional-{{$kiy}}" id="tanggal" name="detail[1000][fokus_audit][0][detail][{{$kiy}}][realisasi]" placeholder="Realisasi" type="text" value="{{ $operasional->realisasi }}" readonly/>
																			</td>
																			<td colspan="2" style="border-right: 1px solid rgba(34,36,38,.1);text-align: left;text-align: justify;text-justify: inter-word;">
																				<textarea class="form-control rencana_operasional_{{$kiy}}" name="detail[1000][fokus_audit][0][detail][{{$kiy}}][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="3">{{$operasional->keterangan}}</textarea>
																			</td>
																		</tr>
																		<tr class="langkah_kerja_operasional_{{$kiy}}">
																			<td style="text-align: left;" colspan="6">Langkah Kerja</td>
																		</tr>
																		@if(!empty($operasional->fokusaudit->langkahkerja->first()->detail))
																			@foreach($operasional->fokusaudit->langkahkerja->first()->detail as $kuy => $detil)
																			<tr class="langkah-kerja-operasional-{{$kiy}}">
																				<td>{{column_letter($kuy+1)}}</td>
																				<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);text-align: justify;text-justify: inter-word;" colspan="5">{{$detil->deskripsi}}</td>
																			</tr>
																			@endforeach
																		@endif
																	@endif
																	@php 
																	$i++;
																	@endphp
																@endforeach
															@else 
																<tr class="data_operasional data_fokus_audit_operasional_0" data-id="0">
																	<td class="for_rowspan_operasional_0" style="text-align: center;font-weight: bold;" rowspan="3">1</td>
																	<td style="width: 20px;" rowspan="3">&nbsp;</td>
																	<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="4">Fokus Audit :</td>
																	<td style="border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
																		<button class="btn btn-outline-success btn-sm tambah_langkah_operasional" data-id="0" type="button" style="border-radius: 20px;margin-top: -4px;margin-bottom: -4px;"><i class="fa fa-plus"></i></button>
																	</td>
																</tr>
																<tr class="detail_fokus_audit_operasional_0" style="border-bottom: 1px solid rgba(34,36,38,.1);">
																	<td style="width: 800px;" class="field">
																		<select data-width="100%" class="selectpicker cari" data-id="0" data-tipe="1" name="detail[1000][fokus_audit][0][detail][0][bidang]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
																			@foreach(App\Models\Master\FokusAudit::where('bidang', 1)->get() as $fokus)
																			<option value="{{ $fokus->id }}">{{ $fokus->audit }}</option>
																			@endforeach
																			<option value="1000">NONE</option>
																		</select>
																		<p class="notif_operasional_0" style="display: none;">Dengan memilih NONE, <span style="color: red;">Maka semua Fokus Audit otomatis dikosongkan.</span></p>
																	</td>
																	<td style="width: 150px;" class="field">
																		<input class="form-control tgl_operasional rencana_operasional_0" id="tanggal" name="detail[1000][fokus_audit][0][detail][0][rencana]" placeholder="Rencana" type="text"/>
																	</td>
																	<td style="width: 150px;" class="field">
																		<input class="form-control tgl_end_operasional-0" id="tanggal" name="detail[1000][fokus_audit][0][detail][0][realisasi]" placeholder="Realisasi" type="text" readonly/>
																	</td>
																	<td style="width: 350px;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
																		<textarea class="form-control rencana_operasional_0" name="detail[1000][fokus_audit][0][detail][0][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="3"></textarea>
																	</td>
																</tr>
																<tr class="langkah_kerja_operasional_0">
																	<td style="text-align: left;" colspan="6">Langkah Kerja</td>
																</tr>
															@endif
														{{-- KEUANGAN --}}
														@elseif($user == 2)
															<tr>
																<td style="width: 20px;text-align: center;font-weight: bold;">II</td>
																<td style="text-align: left;font-weight: bold;border-right: 1px #ffffff;" colspan="5">KEUANGAN</td>
																<td style="text-align: center;font-weight: bold;width:50px;border-right: 1px solid rgba(34,36,38,.1);border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
																</td>
															</tr>
															@php 
																$j=0;
															@endphp
															@if(count($record->detailkerja->where('bidang', 2)) > 0)
																@foreach($record->detailkerja->where('bidang', 2) as $key => $keuangan)
																	@php
																		if(!empty($keuangan->fokusaudit->langkahkerja->first()->detail)){
																			$row = $keuangan->fokusaudit->langkahkerja->first()->detail->count();
																		}else{
																			$row=0;
																		}
																	@endphp
																	<tr class="data_keuangan data_fokus_audit_keuangan_{{$j}}" data-id="{{$j}}">
																		<td class="for_rowspan_keuangan_{{$j}}" style="text-align: center;font-weight: bold;" rowspan="{{$row+3}}">
																			<label style="margin-top: 7px;" class="numbur-keuangan-{{$j}}">{{romawi($j+1)}}</label>
																		</td>
																		<td style="width: 20px;" rowspan="3">&nbsp;</td>
																		<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="4">Fokus Audit :</td>
																		<td style="border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
																			@if($j == 0)
																				<button class="btn btn-outline-success btn-sm tambah_langkah_keuangan" data-id="{{ $j }}" type="button" style="border-radius: 20px;margin-top: -4px;margin-bottom: -4px;"><i class="fa fa-plus"></i></button>
																			@else 
																				<button class="btn btn-outline-danger btn-sm hapus_langkah_keuangan" type="button" style="border-radius:20px;" data-id={{$j}}><i class="fa fa-remove"></i></button>
																			@endif
																		</td>
																	</tr>
																	<tr class="detail_fokus_audit_keuangan_{{$j}}" data-id="{{$j}}" style="border-bottom: 1px solid rgba(34,36,38,.1);">
																		<td style="width: 800px;" class="field">
																			<select data-width="100%" class="selectpicker cari" data-id="{{$j}}" data-tipe="2" name="detail[1000][fokus_audit][1][detail][{{$j}}][bidang]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
																				@foreach(App\Models\Master\FokusAudit::where('bidang', 2)->get() as $fokus)
																				<option value="{{ $fokus->id }}" @if($fokus->id == $keuangan->fokus_audit_id) selected @endif>{{ $fokus->audit }}</option>
																				@endforeach
																				<option value="1000">NONE</option>
																			</select>
																			<p class="notif_keuangan_{{$j}}" style="display: none;">Dengan memilih NONE, <span style="color: red;">Maka semua Fokus Audit otomatis dikosongkan.</span></p>
																		</td>
																		<td style="width: 150px;" class="field">
																			<input class="form-control tgl_keuangan rencana_keuangan_{{$j}}" id="tanggal" name="detail[1000][fokus_audit][1][detail][{{$j}}][rencana]" placeholder="Rencana" type="text" value="{{ $keuangan->rencana }}"/>
																		</td>
																		<td style="width: 150px;" class="field">
																			<input class="form-control tgl_end_keuangan-{{$j}}" id="tanggal" name="detail[1000][fokus_audit][1][detail][{{$j}}][realisasi]" placeholder="Realisasi" type="text" value="{{ $keuangan->realisasi }}" readonly/>
																		</td>
																		<td colspan="2" style="border-right: 1px solid rgba(34,36,38,.1);text-align: left;text-align: justify;text-justify: inter-word;">
																			<textarea class="form-control rencana_keuangan_{{$j}}" name="detail[1000][fokus_audit][1][detail][{{$j}}][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="3">{{$keuangan->keterangan}}</textarea>
																		</td>
																	</tr>
																	<tr class="langkah_kerja_keuangan_{{$j}}">
																		<td style="text-align: left;" colspan="6">Langkah Kerja</td>
																	</tr>
																	@if(!empty($keuangan->fokusaudit->langkahkerja->first()->detail))
																		@foreach($keuangan->fokusaudit->langkahkerja->first()->detail as $kuy => $detil)
																		<tr class="langkah-kerja-keuangan-{{$j}}">
																			<td>{{column_letter($kuy+1)}}</td>
																			<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);text-align: justify;text-justify: inter-word;" colspan="5">{{$detil->deskripsi}}</td>
																		</tr>
																		@endforeach
																	@endif
																	@php 
																	$j++;
																	@endphp
																@endforeach
															@else 
																<tr class="data_keuangan data_fokus_audit_keuangan_0" data-id="0">
																	<td class="for_rowspan_keuangan_0" style="text-align: center;font-weight: bold;" rowspan="3">1</td>
																	<td style="width: 20px;" rowspan="3">&nbsp;</td>
																	<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="4">Fokus Audit :</td>
																	<td style="border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
																		<button class="btn btn-outline-success btn-sm tambah_langkah_keuangan" data-id="0" type="button" style="border-radius: 20px;margin-top: -4px;margin-bottom: -4px;"><i class="fa fa-plus"></i></button>
																	</td>
																</tr>
																<tr class="detail_fokus_audit_keuangan_0" style="border-bottom: 1px solid rgba(34,36,38,.1);">
																	<td style="width: 800px;" class="field">
																		<select data-width="100%" class="selectpicker cari" data-id="0" data-tipe="2" name="detail[1000][fokus_audit][1][detail][0][bidang]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
																			@foreach(App\Models\Master\FokusAudit::where('bidang', 2)->get() as $fokus)
																			<option value="{{ $fokus->id }}">{{ $fokus->audit }}</option>
																			@endforeach
																			<option value="1000">NONE</option>
																		</select>
																		<br>
																		<br>
																		<p class="notif_keuangan_0" style="display: none;">Dengan memilih NONE, <span style="color: red;">Maka semua Fokus Audit otomatis dikosongkan.</span></p>
																	</td>
																	<td style="width: 150px;" class="field">
																		<input class="form-control tgl_keuangan rencana_keuangan_0" id="tanggal" name="detail[1000][fokus_audit][1][detail][0][rencana]" placeholder="Rencana" type="text"/>
																	</td>
																	<td style="width: 150px;" class="field">
																		<input class="form-control tgl_end_keuangan-0" id="tanggal" name="detail[1000][fokus_audit][1][detail][0][realisasi]" placeholder="Realisasi" type="text" readonly/>
																	</td>
																	<td style="width: 350px;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
																		<textarea class="form-control rencana_keuangan_0" name="detail[1000][fokus_audit][1][detail][0][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="3"></textarea>
																	</td>
																</tr>
																<tr class="langkah_kerja_keuangan_0">
																	<td style="text-align: left;" colspan="6">Langkah Kerja</td>
																</tr>
															@endif
														@else
															{{-- SISTEM --}}
															<tr>
																<td style="width: 20px;text-align: center;font-weight: bold;">III</td>
																<td style="text-align: left;font-weight: bold;border-right: 1px #ffffff;" colspan="5">SISTEM</td>
																<td style="text-align: center;font-weight: bold;width:50px;border-right: 1px solid rgba(34,36,38,.1);border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
																</td>
															</tr>
															@php 
																$k=0;
															@endphp

															@if(count($record->detailkerja->where('bidang', 3)) > 0)
																@foreach($record->detailkerja->where('bidang', 3) as $key => $sistem)
																	@php
																		if($sistem->fokus_audit_id == 1000){
																			$row=0;
																		}else{
																			if(!empty($sistem->fokusaudit->langkahkerja->first()->detail)){
																				$row = $sistem->fokusaudit->langkahkerja->first()->detail->count();
																			}else{
																				$row=0;
																			}
																		}
																	@endphp
																	@if($sistem->fokus_audit_id == 1000)
																		<tr class="data_sistem data_fokus_audit_sistem_{{$k}}" data-id="{{$k}}">
																			<td class="for_rowspan_sistem_{{$k}}" style="text-align: center;font-weight: bold;" rowspan="{{$row+3}}">
																				<label style="margin-top: 7px;" class="numbur-sistem-{{$k}}">{{romawi($k+1)}}</label>
																			</td>
																			<td style="width: 20px;" rowspan="3">&nbsp;</td>
																			<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="4">Fokus Audit :</td>
																			<td style="border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
																			</td>
																		</tr>
																		<tr class="detail_fokus_audit_sistem_{{$k}}" data-id="{{$k}}" style="border-bottom: 1px solid rgba(34,36,38,.1);">
																			<td style="width: 800px;" class="field">
																				NONE
																				<p>Dengan memilih NONE, <span style="color: red;">Maka semua Fokus Audit otomatis dikosongkan.</span></p>
																			</td>
																			<td style="width: 150px;" class="field">
																				-
																			</td>
																			<td style="width: 150px;" class="field">
																				-
																			</td>
																			<td colspan="2" style="border-right: 1px solid rgba(34,36,38,.1);text-align: left;text-align: justify;text-justify: inter-word;">
																				-
																			</td>
																		</tr>
																		<tr class="langkah_kerja_sistem_{{$k}}">
																			<td style="text-align: left;" colspan="6">Langkah Kerja</td>
																		</tr>
																	@else
																		<tr class="data_sistem data_fokus_audit_sistem_{{$k}}" data-id="{{$k}}">
																			<td class="for_rowspan_sistem_{{$k}}" style="text-align: center;font-weight: bold;" rowspan="{{$row+3}}">
																				<label style="margin-top: 7px;" class="numbur-sistem-{{$k}}">{{romawi($k+1)}}</label>
																			</td>
																			<td style="width: 20px;" rowspan="3">&nbsp;</td>
																			<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="4">Fokus Audit :</td>
																			<td style="border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
																				@if($k == 0)
																					<button class="btn btn-outline-success btn-sm tambah_langkah_sistem" data-id="{{ $k }}" type="button" style="border-radius: 20px;margin-top: -4px;margin-bottom: -4px;"><i class="fa fa-plus"></i></button>
																				@else 
																					<button class="btn btn-outline-danger btn-sm hapus_langkah_sistem" type="button" style="border-radius:20px;" data-id={{$k}}><i class="fa fa-remove"></i></button>
																				@endif
																			</td>
																		</tr>
																		<tr class="detail_fokus_audit_sistem_{{$k}}" data-id="{{$k}}" style="border-bottom: 1px solid rgba(34,36,38,.1);">
																			<td style="width: 800px;" class="field">
																				<select data-width="100%" class="selectpicker cari" data-id="{{$k}}" data-tipe="3" name="detail[1000][fokus_audit][2][detail][{{$k}}][bidang]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
																					@foreach(App\Models\Master\FokusAudit::where('bidang', 3)->get() as $fokus)
																					<option value="{{ $fokus->id }}" @if($fokus->id == $sistem->fokus_audit_id) selected @endif>{{ $fokus->audit }}</option>
																					@endforeach
																					<option value="1000">NONE</option>
																				</select>
																				<p class="notif_sistem_{{$k}}" style="display: none;">Dengan memilih NONE, <span style="color: red;">Maka semua Fokus Audit otomatis dikosongkan.</span></p>
																			</td>
																			<td style="width: 150px;" class="field">
																				<input class="form-control tgl_sistem rencana_sistem_{{$k}}" id="tanggal" name="detail[1000][fokus_audit][2][detail][{{$k}}][rencana]" placeholder="Rencana" type="text" value="{{ $sistem->rencana }}"/>
																			</td>
																			<td style="width: 150px;" class="field">
																				<input class="form-control tgl_end_sistem-{{$k}}" id="tanggal" name="detail[1000][fokus_audit][2][detail][{{$k}}][realisasi]" placeholder="Realisasi" type="text" value="{{ $sistem->realisasi }}" readonly/>
																			</td>
																			<td colspan="2" style="border-right: 1px solid rgba(34,36,38,.1);text-align: left;text-align: justify;text-justify: inter-word;">
																				<textarea class="form-control rencana_sistem_{{$k}}" name="detail[1000][fokus_audit][2][detail][{{$k}}][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="3">{{$sistem->keterangan}}</textarea>
																			</td>
																		</tr>
																		<tr class="langkah_kerja_sistem_{{$k}}">
																			<td style="text-align: left;" colspan="6">Langkah Kerja</td>
																		</tr>
																		@if(!empty($sistem->fokusaudit->langkahkerja->first()->detail))
																			@foreach($sistem->fokusaudit->langkahkerja->first()->detail as $kuy => $detil)
																			<tr class="langkah-kerja-sistem-{{$k}}">
																				<td>{{column_letter($kuy+1)}}</td>
																				<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);text-align: justify;text-justify: inter-word;" colspan="5">{{$detil->deskripsi}}</td>
																			</tr>
																			@endforeach
																		@endif
																	@endif
																	@php 
																	$k++;
																	@endphp
																@endforeach
															@else 
																<tr class="data_sistem data_fokus_audit_sistem_0" data-id="0">
																	<td class="for_rowspan_sistem_0" style="text-align: center;font-weight: bold;" rowspan="3">1</td>
																	<td style="width: 20px;" rowspan="3">&nbsp;</td>
																	<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="4">Fokus Audit :</td>
																	<td style="border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
																		<button class="btn btn-outline-success btn-sm tambah_langkah_sistem" data-id="0" type="button" style="border-radius: 20px;margin-top: -4px;margin-bottom: -4px;"><i class="fa fa-plus"></i></button>
																	</td>
																</tr>
																<tr class="detail_fokus_audit_sistem_0" style="border-bottom: 1px solid rgba(34,36,38,.1);">
																	<td style="width: 800px;" class="field">
																		<select data-width="100%" class="selectpicker cari" data-id="0" data-tipe="3" name="detail[1000][fokus_audit][2][detail][0][bidang]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
																			@foreach(App\Models\Master\FokusAudit::where('bidang', 3)->get() as $fokus)
																			<option value="{{ $fokus->id }}">{{ $fokus->audit }}</option>
																			@endforeach
																			<option value="1000">NONE</option>
																		</select>
																		<p class="notif_sistem_{{$k}}" style="display: none;">Dengan memilih NONE, <span style="color: red;">Maka semua Fokus Audit otomatis dikosongkan.</span></p>
																	</td>
																	<td style="width: 150px;" class="field">
																		<input class="form-control tgl_sistem rencana_sistem_0" id="tanggal" name="detail[1000][fokus_audit][2][detail][0][rencana]" placeholder="Rencana" type="text"/>
																	</td>
																	<td style="width: 150px;" class="field">
																		<input class="form-control tgl_end_sistem-0" id="tanggal" name="detail[1000][fokus_audit][2][detail][0][realisasi]" placeholder="Realisasi" type="text" readonly/>
																	</td>
																	<td style="width: 350px;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
																		<textarea class="form-control rencana_sistem_0" name="detail[1000][fokus_audit][2][detail][0][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="3"></textarea>
																	</td>
																</tr>
																<tr class="langkah_kerja_sistem_0">
																	<td style="text-align: left;" colspan="6">Langkah Kerja</td>
																</tr>
															@endif
														@endif
													</tbody>
												</table>
											</table>
										</td>
									</tr>
								</table>
								<div class="form-group col-md-12">
									<div class="text-right">
										<button type="button" class="btn btn-cancel kembali3">Sebelumnya</button>
										<button type="button" class="btn btn-primary save as drafting">Save As Draft</button>
										<button type="button" class="btn btn-simpan save as page">Submit</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
    </div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
	    .datepicker.dropdown-menu {
		    z-index: 9999 !important;
		}
    </style>
@endpush

@push('scripts')
    <script> 
    	$('#myTab li:first-child a').tab('show')
    	$('.btn').tooltip('enable');
        var dateDis = [];
    	var dd = [];
    	var mindate ='';
    	function disabledKoplok(element)
    	{
    		var d = new Date();
	    	var full = d.getFullYear();
	    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
	    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
	    	var last = new Date(lastDate);

    		var parent = parseInt(element.getAttribute("data-parent"));
    		var e0 = parseInt(element.getAttribute("data-val"))-1;
    		var e1 = parseInt(element.getAttribute("data-val"));
    		var e2 = parseInt(element.getAttribute("data-val"))+1;
    		var cek = $('.datefak-'+parent+'-'+e0).val();
    		var cek2 = $('.datefak-'+parent+'-'+e1).val();
    		if(!isEmpty(cek2)){
	    		var tgl = cek2.split("-").slice(0)[0];
	    		var bln = cek2.split("-").slice(1)[0];
	    		var thn = cek2.split("-").slice(2)[0];
	    		var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	    		var month = ('0' + (months.indexOf(bln)+1)).slice(-2);
	    		var dates = thn+'-'+month+'-'+tgl;
	    		var dat = new Date(dates);
	    		dat.setDate(dat.getDate() + 1);
    		}else{
    			var dat = minDates;
    		}
    		var retval = getDates(minDates, dat);
    		$('.datefak-'+parent+'-'+e2).prop('readonly', false);
    		$('.datefak-'+parent+'-'+e2).datepicker('destroy').datepicker({
    			orientation: "bottom right",
    			todayHighlight: true,
    			defaultViewDate: {year:$('input[name=tahun]').val(), month:(months.indexOf(bln)), day:tgl},
    			autoclose:true,
    			format: 'dd-M-yyyy',
    			yearRange: '-0:+1',
    			startDate: dat,
    			endDate: last,
    			hideIfNoPrevNext: true,
    			datesDisabled: retval,
    		});
    	}

    	function getDates(minDates, dat){
		  var oneDay = 24*3600*1000;
		  for (var dd=[],ms=minDates*1,last=dat*1;ms<last;ms+=oneDay){
		    dd.push( new Date(ms) );
		  }
		  return dd;
		}

        $('.waktu').clockpicker({
            autoclose:true,
            'default': 'now',
        });

        $(document).on('click', '.page1', function (e){
    		$('#a-tab').removeClass('active');
    		$('#a-tab').closest('li').removeClass('active');
    		$('#b-tab').addClass('active');
    		$('#b-tab').closest('li').addClass('active');
    		$('#a').removeClass('active');
    		$('#b').addClass('active');
		});

		$(document).on('click', '.kembali1', function (e){
    		$('#a-tab').addClass('active');
    		$('#a-tab').closest('li').addClass('active');
    		$('#b-tab').removeClass('active');
    		$('#b-tab').closest('li').removeClass('active');
    		$('#a').addClass('active');
    		$('#b').removeClass('active');
		});

		$(document).on('click', '.page2', function (e){
    		$('#b-tab').removeClass('active');
    		$('#b-tab').closest('li').removeClass('active');
    		$('#c-tab').addClass('active');
    		$('#c-tab').closest('li').addClass('active');
    		$('#b').removeClass('active');
    		$('#c').addClass('active');
		});

		$(document).on('click', '.kembali2', function (e){
    		$('#b-tab').addClass('active');
    		$('#b-tab').closest('li').addClass('active');
    		$('#c-tab').removeClass('active');
    		$('#c-tab').closest('li').removeClass('active');
    		$('#b').addClass('active');
    		$('#c').removeClass('active');
		});

		$(document).on('click', '.page3', function (e){
    		$('#c-tab').removeClass('active');
    		$('#c-tab').closest('li').removeClass('active');
    		$('#d-tab').addClass('active');
    		$('#d-tab').closest('li').addClass('active');
    		$('#c').removeClass('active');
    		$('#d').addClass('active');
		});

		$(document).on('click', '.kembali3', function (e){
    		$('#c-tab').addClass('active');
    		$('#c-tab').closest('li').addClass('active');
    		$('#d-tab').removeClass('active');
    		$('#d-tab').closest('li').removeClass('active');
    		$('#c').addClass('active');
    		$('#d').removeClass('active');
		});   



        $('.cari').on('change', function(){
        	var id = $(this).data('id');
        	var tipe = $(this).data('tipe');
        	var value = parseInt(this.value);
			if(value == 1000){
				if(tipe == 1){
					$('.rencana_operasional_'+id).attr('disabled', 'disabled');
					$('.notif_operasional_'+id).show();
					$('.tambah_langkah_operasional').attr('disabled', 'disabled');
				}else if(tipe == 2){
					$('.rencana_keuangan_'+id).attr('disabled', 'disabled');
					$('.notif_keuangan_'+id).show();
					$('.tambah_langkah_keuangan').attr('disabled', 'disabled');
				}else{
					$('.rencana_sistem_'+id).attr('disabled', 'disabled');
					$('.notif_sistem_'+id).show();
					$('.tambah_langkah_sistem').attr('disabled', 'disabled');
				}
			}else{
				if(tipe == 1){
					$('.rencana_operasional_'+id).removeAttr("disabled");
					$('.notif_operasional_'+id).hide();
					$('.tambah_langkah_operasional').removeAttr("disabled");
				}else if(tipe == 2){
					$('.rencana_keuangan_'+id).removeAttr("disabled");
					$('.notif_keuangan_'+id).hide();
					$('.tambah_langkah_keuangan').removeAttr("disabled");
				}else{
					$('.rencana_sistem_'+id).removeAttr("disabled");
					$('.notif_sistem_'+id).hide();
					$('.tambah_langkah_sistem').removeAttr("disabled");
				}
			}
        	$.ajax({
				url: '{{ url('ajax/option/get-langkah') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					id: id,
					value: value,
				},
			})
			.done(function(response) {
				var rowz = $('tr.langkah-kerja-'+id).remove();
				var rowspan = response[1].length;
				$('.for_rowspan_'+id).attr('rowspan', parseInt((rowspan+3)));
				$.each(response[1], function(key, value){
					if(key==0){
						$(value).insertAfter('.langkah_kerja_'+id);
					}else{
						$(value).insertAfter('.data_detail-'+id+'-'+(key-1));
					}
				});
			})
		});
        function romawi(number){
        	var map = [1,4,5,9,10,40,50,90,100,400,500,900,1000];  
        	var roma = ["I","IV","V","IX","X","XL","L","XC","C","CD","D","CM","M"];  
        	var hasil='';  

        	for(var i=12; i >=0; i--) {  
        		while(number >= map[i]) {  
        			number = number - map[i];  
        			hasil = hasil + roma[i];  
        		}  
        	}  
		    return hasil;
        }

        var isEmpty = function(data) {
		    if(typeof(data) === 'object'){
		        if(JSON.stringify(data) === '{}' || JSON.stringify(data) === '[]'){
		            return true;
		        }else if(!data){
		            return true;
		        }
		        return false;
		    }else if(typeof(data) === 'string'){
		        if(!data.trim()){
		            return true;
		        }
		        return false;
		    }else if(typeof(data) === 'undefined'){
		        return true;
		    }else{
		        return false;
		    }
		}

		var table = $('#table-penyelesaian');
		var ros = table.find('tbody tr.detail_cek');
    	$.each(ros, function(key, value){
            
		});

        var d = new Date();
    	var full = d.getFullYear();
    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
    	var last = new Date(lastDate);
    	if($('input[name=tahun]').val() > full){
    		var min = minDates;
    	}else{
    		var min = d;
    	}

    	$('.tgl_start').datepicker({
            orientation: "bottom right",
    		todayHighlight: true,
    		autoclose:true,
    		format: 'dd/mm/yyyy',
    		yearRange: '-0:+1',defaultViewDate: {year:$('input[name=tahun]').val(), month:0, day:1},
    		startDate: minDates,
    		endDate: last,
    		hideIfNoPrevNext: true,
        }).on('changeDate', function (selected) {
        	var mins = new Date(selected.date.valueOf());
        	var months = (mins.getMonth()+1);
        	var tgl = mins.getDate();
        	var id = $(this).parents('tr').data("id");
		    $('.tgl_end-'+id).datepicker('destroy').datepicker({
	            orientation: "bottom right",
	    		todayHighlight: true,
	    		autoclose:true,
	    		format: 'dd/mm/yyyy',
	    		defaultViewDate: {year:$('input[name=tahun]').val(), month:(months-1), day:tgl},
	    		yearRange: '-0:+1',
	    		startDate: mins,
	    		endDate: last,
	    		hideIfNoPrevNext: true,
	        })
        	$('.tgl_end-'+id).val('');
	        $('.tgl_end-'+id).focus();
		});

		$('.tgl_operasional').datepicker({
            orientation: "bottom right",
    		todayHighlight: true,
    		autoclose:true,
    		format: 'dd/mm/yyyy',
    		yearRange: '-0:+1',
    		defaultViewDate: {year:$('input[name=tahun]').val(), month:0, day:1},
    		startDate: minDates,
    		endDate: last,
    		hideIfNoPrevNext: true,
        });

		$('.tgl_keuangan').datepicker({
            orientation: "bottom right",
    		todayHighlight: true,
    		autoclose:true,
    		format: 'dd/mm/yyyy',
    		yearRange: '-0:+1',
    		defaultViewDate: {year:$('input[name=tahun]').val(), month:0, day:1},
    		startDate: minDates,
    		endDate: last,
    		hideIfNoPrevNext: true,
        });

		$('.tgl_sistem').datepicker({
            orientation: "bottom right",
    		todayHighlight: true,
    		autoclose:true,
    		format: 'dd/mm/yyyy',
    		yearRange: '-0:+1',
    		defaultViewDate: {year:$('input[name=tahun]').val(), month:0, day:1},
    		startDate: minDates,
    		endDate: last,
    		hideIfNoPrevNext: true,
        });

    	$('#example > tbody  > tr').each(function(key, value){
    		var detail_id = $(this).data("detail");
    		var value = $(this).data("value");
    		$.ajax({
				url: '{{ url('ajax/option/get-kategori') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					kategori: $('select[name="detail['+detail_id+'][kategori]"]').val()
				},
			})
			.done(function(response) {
				$('select[name="detail['+detail_id+'][object_audit]"]').html(response);
				$('select[name="detail['+detail_id+'][object_audit]"]').children('option[value="'+value+'"]').prop('selected',true);
				$('select[name="detail['+detail_id+'][object_audit]"]').selectpicker("refresh");
			})
			.fail(function() {
				console.log("error");
			});
    	});
    	$('.rencana').datepicker({
            format: "mm-yyyy",
		    viewMode: "months", 
		    minViewMode: "months",
            orientation: "auto",
            autoclose:true
        });

		$('.cari').on('change', function(){
			var id = $(this).data('id');
			var tipe = $(this).data('tipe');
			var value = parseInt(this.value);
			$.ajax({
				url: '{{ url('ajax/option/get-langkah') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					id: id,
					value: value,
					tipe: tipe,
				},
			})
			.done(function(response) {
				if(response[2] == 1){
					var rowz = $('tr.langkah-kerja-operasional-'+id).remove();
					var rowspan = response[1].length;
					$('.for_rowspan_operasional_'+id).attr('rowspan', parseInt((rowspan+3)));
					$.each(response[1], function(key, value){
						if(key==0){
							$(value).insertAfter('.langkah_kerja_operasional_'+id);
						}else{
							$(value).insertAfter('.data_detail_operasional-'+id+'-'+(key-1));
						}
					});
				}else if(response[2] == 2){
					var rowz = $('tr.langkah-kerja-keuangan-'+id).remove();
					var rowspan = response[1].length;
					$('.for_rowspan_keuangan_'+id).attr('rowspan', parseInt((rowspan+3)));
					$.each(response[1], function(key, value){
						if(key==0){
							$(value).insertAfter('.langkah_kerja_keuangan_'+id);
						}else{
							$(value).insertAfter('.data_detail_keuangan-'+id+'-'+(key-1));
						}
					});
				}else{
					var rowz = $('tr.langkah-kerja-sistem-'+id).remove();
					var rowspan = response[1].length;
					$('.for_rowspan_sistem_'+id).attr('rowspan', parseInt((rowspan+3)));
					$.each(response[1], function(key, value){
						if(key==0){
							$(value).insertAfter('.langkah_kerja_sistem_'+id);
						}else{
							$(value).insertAfter('.data_detail_sistem-'+id+'-'+(key-1));
						}
					});
				}
			})
		});


		$(document).on('click', '.tambah_langkah_operasional', function(e){
			var id = $(this).data("id");
			var rowCount = $('#table-fokus > tbody > tr.data_operasional').length;
			var c = rowCount-1;
			var html = `
					<tr class="data_operasional data_fokus_audit_operasional_`+(c+1)+`" data-id="`+(c+1)+`">
						<td class="for_rowspan_operasional_`+(c+1)+`" style="text-align: center;font-weight: bold;" rowspan="3">
							<label style="margin-top: 7px;" class="numbur-operasional-`+(c+1)+`">`+(romawi(c+2))+`</label>
						</td>
						<td style="width: 20px;" rowspan="3">&nbsp;</td>
						<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="4">Fokus Audit :</td>
						<td style="border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
							<button class="btn btn-outline-danger btn-sm hapus_langkah_operasional" type="button" style="border-radius:20px;" data-id=`+(c+1)+`><i class="fa fa-remove"></i></button>
						</td>
					</tr>
					<tr class="detail_fokus_audit_operasional_`+(c+1)+`" data-id="`+(c+1)+`" style="border-bottom: 1px solid rgba(34,36,38,.1);">
						<td style="width: 800px;" class="field">
							<select data-width="100%" class="selectpicker cari" data-id="`+(c+1)+`" data-tipe="1" name="detail[1000][fokus_audit][0][detail][`+(c+1)+`][bidang]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
								@foreach(App\Models\Master\FokusAudit::where('bidang', 1)->get() as $fokus)
									<option value="{{ $fokus->id }}">{{ $fokus->audit }}</option>
								@endforeach
								<option value="1000">NONE</option>
							</select>
							<br>
							<br>
							<p class="notif_operasional_`+(c+1)+`" style="display: none;">Dengan memilih NONE, <span style="color: red;">Maka semua Fokus Audit otomatis dikosongkan.</span></p>
						</td>
						<td style="width: 150px;" class="field">
							<input class="form-control tgl_operasional rencana_operasional_`+(c+1)+`" id="tanggal" name="detail[1000][fokus_audit][0][detail][`+(c+1)+`][rencana]" placeholder="Rencana" type="text"/>
						</td>
						<td style="width: 150px;" class="field">
							<input class="form-control tgl_end_operasional-`+(c+1)+`" id="tanggal" name="detail[1000][fokus_audit][0][detail][`+(c+1)+`][realisasi]" placeholder="Realisasi" type="text" readonly/>
						</td>
						<td style="width: 350px;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
							<textarea class="form-control rencana_operasional_`+(c+1)+`" name="detail[1000][fokus_audit][0][detail][`+(c+1)+`][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="3"></textarea>
						</td>
					</tr>
					<tr class="langkah_kerja_operasional_`+(c+1)+`">
						<td style="text-align: left;" colspan="6">Langkah Kerja</td>
					</tr>
				`;
				var cek = $('.langkah-kerja-operasional-'+(c)).last();
				if(cek.length == 0){
					$(html).insertAfter('.langkah_kerja_operasional_'+(c));
				}else{
					$(html).insertAfter(cek);
				}
				$('.selectpicker').selectpicker();

				$('.cari').on('change', function(){
		        	var id = $(this).data('id');
		        	var tipe = $(this).data('tipe');
		        	var value = parseInt(this.value);
		        	if(value == 1000){
						$('.rencana_operasional_'+id).attr('disabled', 'disabled');
						$('.notif_operasional_'+id).show();
						$('.tambah_langkah_operasional').attr('disabled', 'disabled');
					}else{
						$('.rencana_operasional_'+id).removeAttr("disabled");
						$('.notif_operasional_'+id).hide();
						$('.tambah_langkah_operasional').removeAttr("disabled");
					}
		        	$.ajax({
						url: '{{ url('ajax/option/get-langkah') }}',
						type: 'POST',
						data: {
							_token: "{{ csrf_token() }}",
							id: id,
							value: value,
							tipe: tipe,
						},
					})
					.done(function(response) {
						if(response[2] == 1){
							var rowz = $('tr.langkah-kerja-operasional-'+id).remove();
							var rowspan = response[1].length;
							$('.for_rowspan_operasional_'+id).attr('rowspan', parseInt((rowspan+3)));
							$.each(response[1], function(key, value){
								if(key==0){
									$(value).insertAfter('.langkah_kerja_operasional_'+id);
								}else{
									$(value).insertAfter('.data_detail_operasional-'+id+'-'+(key-1));
								}
							});
						}else if(response[2] == 2){
							var rowz = $('tr.langkah-kerja-keuangan-'+id).remove();
							var rowspan = response[1].length;
							$('.for_rowspan_keuangan_'+id).attr('rowspan', parseInt((rowspan+3)));
							$.each(response[1], function(key, value){
								if(key==0){
									$(value).insertAfter('.langkah_kerja_keuangan_'+id);
								}else{
									$(value).insertAfter('.data_detail_keuangan-'+id+'-'+(key-1));
								}
							});
						}else{
							var rowz = $('tr.langkah-kerja-sistem-'+id).remove();
							var rowspan = response[1].length;
							$('.for_rowspan_sistem_'+id).attr('rowspan', parseInt((rowspan+3)));
							$.each(response[1], function(key, value){
								if(key==0){
									$(value).insertAfter('.langkah_kerja_sistem_'+id);
								}else{
									$(value).insertAfter('.data_detail_sistem-'+id+'-'+(key-1));
								}
							});
						}
					})
				});

				$('.tgl_operasional').datepicker({
		            orientation: "bottom right",
		    		todayHighlight: true,
		    		autoclose:true,
		    		defaultViewDate: {year:$('input[name=tahun]').val(), month:0, day:1},
		    		format: 'dd/mm/yyyy',
		    		yearRange: '-0:+1',
		    		startDate: minDates,
		    		endDate: last,
		    		hideIfNoPrevNext: true,
		        });
		});
		$(document).on('click', '.hapus_langkah_operasional', function (e){
			var id = $(this).data("id");
			var row = $('.data_fokus_audit_operasional_'+id).remove();
			var row = $('.detail_fokus_audit_operasional_'+id).remove();
			var row = $('.langkah-kerja-operasional-'+id).remove();
			var row = $('.langkah_kerja_operasional_'+id).remove();
			var table = $('#table-fokus');
			var rows = table.find('tbody tr.data_operasional');
			$.each(rows, function(key, value){
				console.log($(this).data("id"))
				table.find('.numbur-operasional-'+$(this).data("id")).html(romawi(key+1));
			});
		});

		$(document).on('click', '.tambah_langkah_keuangan', function(e){
			var id = $(this).data("id");
			var rowCount = $('#table-fokus > tbody > tr.data_keuangan').length;
			var c = rowCount-1;
			var html = `
					<tr class="data_keuangan data_fokus_audit_keuangan_`+(c+1)+`" data-id="`+(c+1)+`">
						<td class="for_rowspan_keuangan_`+(c+1)+`" style="text-align: center;font-weight: bold;" rowspan="3">
							<label style="margin-top: 7px;" class="numbur-keuangan-`+(c+1)+`">`+(romawi(c+2))+`</label>
						</td>
						<td style="width: 20px;" rowspan="3">&nbsp;</td>
						<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="4">Fokus Audit :</td>
						<td style="border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
							<button class="btn btn-outline-danger btn-sm hapus_langkah_keuangan" type="button" style="border-radius:20px;" data-id=`+(c+1)+`><i class="fa fa-remove"></i></button>
						</td>
					</tr>
					<tr class="detail_fokus_audit_keuangan_`+(c+1)+`" data-id="`+(c+1)+`" style="border-bottom: 1px solid rgba(34,36,38,.1);">
						<td style="width: 800px;" class="field">
							<select data-width="100%" class="selectpicker cari" data-id="`+(c+1)+`" data-tipe="2" name="detail[1000][fokus_audit][1][detail][`+(c+1)+`][bidang]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
								@foreach(App\Models\Master\FokusAudit::where('bidang', 2)->get() as $fokus)
									<option value="{{ $fokus->id }}">{{ $fokus->audit }}</option>
								@endforeach
								<option value="1000">NONE</option>
							</select>
							<br>
							<br>
							<p class="notif_keuangan_`+(c+1)+`" style="display: none;">Dengan memilih NONE, <span style="color: red;">Maka semua Fokus Audit otomatis dikosongkan.</span></p>
						</td>
						<td style="width: 150px;" class="field">
							<input class="form-control tgl_keuangan rencana_keuangan_`+(c+1)+`" id="tanggal" name="detail[1000][fokus_audit][1][detail][`+(c+1)+`][rencana]" placeholder="Rencana" type="text"/>
						</td>
						<td style="width: 150px;" class="field">
							<input class="form-control tgl_end_keuangan-`+(c+1)+`" id="tanggal" name="detail[1000][fokus_audit][1][detail][`+(c+1)+`][realisasi]" placeholder="Realisasi" type="text" readonly/>
						</td>
						<td style="width: 350px;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
							<textarea class="form-control rencana_keuangan_`+(c+1)+`" name="detail[1000][fokus_audit][1][detail][`+(c+1)+`][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="3"></textarea>
						</td>
					</tr>
					<tr class="langkah_kerja_keuangan_`+(c+1)+`">
						<td style="text-align: left;" colspan="6">Langkah Kerja</td>
					</tr>
				`;
				var cek = $('.langkah-kerja-keuangan-'+(c)).last();
				if(cek.length == 0){
					$(html).insertAfter('.langkah_kerja_keuangan_'+(c));
				}else{
					$(html).insertAfter(cek);
				}
				$('.selectpicker').selectpicker();

				$('.cari').on('change', function(){
		        	var id = $(this).data('id');
		        	var tipe = $(this).data('tipe');
		        	var value = parseInt(this.value);
		        	if(value == 1000){
						$('.rencana_keuangan_'+id).attr('disabled', 'disabled');
						$('.notif_keuangan_'+id).show();
						$('.tambah_langkah_keuangan').attr('disabled', 'disabled');
					}else{
						$('.rencana_keuangan_'+id).removeAttr("disabled");
						$('.notif_keuangan_'+id).hide();
						$('.tambah_langkah_keuangan').removeAttr("disabled");
					}
		        	$.ajax({
						url: '{{ url('ajax/option/get-langkah') }}',
						type: 'POST',
						data: {
							_token: "{{ csrf_token() }}",
							id: id,
							value: value,
							tipe: tipe,
						},
					})
					.done(function(response) {
						if(response[2] == 1){
							var rowz = $('tr.langkah-kerja-operasional-'+id).remove();
							var rowspan = response[1].length;
							$('.for_rowspan_operasional_'+id).attr('rowspan', parseInt((rowspan+3)));
							$.each(response[1], function(key, value){
								if(key==0){
									$(value).insertAfter('.langkah_kerja_operasional_'+id);
								}else{
									$(value).insertAfter('.data_detail_operasional-'+id+'-'+(key-1));
								}
							});
						}else if(response[2] == 2){
							var rowz = $('tr.langkah-kerja-keuangan-'+id).remove();
							var rowspan = response[1].length;
							$('.for_rowspan_keuangan_'+id).attr('rowspan', parseInt((rowspan+3)));
							$.each(response[1], function(key, value){
								if(key==0){
									$(value).insertAfter('.langkah_kerja_keuangan_'+id);
								}else{
									$(value).insertAfter('.data_detail_keuangan-'+id+'-'+(key-1));
								}
							});
						}else{
							var rowz = $('tr.langkah-kerja-sistem-'+id).remove();
							var rowspan = response[1].length;
							$('.for_rowspan_sistem_'+id).attr('rowspan', parseInt((rowspan+3)));
							$.each(response[1], function(key, value){
								if(key==0){
									$(value).insertAfter('.langkah_kerja_sistem_'+id);
								}else{
									$(value).insertAfter('.data_detail_sistem-'+id+'-'+(key-1));
								}
							});
						}
					})
				});

				$('.tgl_keuangan').datepicker({
		            orientation: "bottom right",
		    		todayHighlight: true,
		    		autoclose:true,
		    		defaultViewDate: {year:$('input[name=tahun]').val(), month:0, day:1},
		    		format: 'dd/mm/yyyy',
		    		yearRange: '-0:+1',
		    		startDate: minDates,
		    		endDate: last,
		    		hideIfNoPrevNext: true,
		        });
		});
		$(document).on('click', '.hapus_langkah_keuangan', function (e){
			var id = $(this).data("id");
			var row = $('.data_fokus_audit_keuangan_'+id).remove();
			var row = $('.detail_fokus_audit_keuangan_'+id).remove();
			var row = $('.langkah-kerja-keuangan-'+id).remove();
			var row = $('.langkah_kerja_keuangan_'+id).remove();
			var table = $('#table-fokus');
			var rows = table.find('tbody tr.data_keuangan');
			$.each(rows, function(key, value){
				console.log($(this).data("id"))
				table.find('.numbur-keuangan-'+$(this).data("id")).html(romawi(key+1));
			});
		});

		$(document).on('click', '.tambah_langkah_sistem', function(e){
			var id = $(this).data("id");
			var rowCount = $('#table-fokus > tbody > tr.data_sistem').length;
			var c = rowCount-1;
			var html = `
					<tr class="data_sistem data_fokus_audit_sistem_`+(c+1)+`" data-id="`+(c+1)+`">
						<td class="for_rowspan_sistem_`+(c+1)+`" style="text-align: center;font-weight: bold;" rowspan="3">
							<label style="margin-top: 7px;" class="numbur-sistem-`+(c+1)+`">`+(romawi(c+2))+`</label>
						</td>
						<td style="width: 20px;" rowspan="3">&nbsp;</td>
						<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="4">Fokus Audit :</td>
						<td style="border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
							<button class="btn btn-outline-danger btn-sm hapus_langkah_sistem" type="button" style="border-radius:20px;" data-id=`+(c+1)+`><i class="fa fa-remove"></i></button>
						</td>
					</tr>
					<tr class="detail_fokus_audit_sistem_`+(c+1)+`" data-id="`+(c+1)+`" style="border-bottom: 1px solid rgba(34,36,38,.1);">
						<td style="width: 800px;" class="field">
							<select data-width="100%" class="selectpicker cari" data-id="`+(c+1)+`" data-tipe="3" name="detail[1000][fokus_audit][2][detail][`+(c+1)+`][bidang]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
								@foreach(App\Models\Master\FokusAudit::where('bidang', 3)->get() as $fokus)
									<option value="{{ $fokus->id }}">{{ $fokus->audit }}</option>
								@endforeach
								<option value="1000">NONE</option>
							</select>
							<br>
							<br>
							<p class="notif_sistem_`+(c+1)+`" style="display: none;">Dengan memilih NONE, <span style="color: red;">Maka semua Fokus Audit otomatis dikosongkan.</span></p>
						</td>
						<td style="width: 150px;" class="field">
							<input class="form-control tgl_sistem rencana_sistem_`+(c+1)+`" id="tanggal" name="detail[1000][fokus_audit][2][detail][`+(c+1)+`][rencana]" placeholder="Rencana" type="text"/>
						</td>
						<td style="width: 150px;" class="field">
							<input class="form-control tgl_end_sistem-`+(c+1)+`" id="tanggal" name="detail[1000][fokus_audit][2][detail][`+(c+1)+`][realisasi]" placeholder="Realisasi" type="text" readonly/>
						</td>
						<td style="width: 350px;border-right: 1px solid rgba(34,36,38,.1);" colspan="2">
							<textarea class="form-control rencana_sistem_`+(c+1)+`" name="detail[1000][fokus_audit][2][detail][`+(c+1)+`][keterangan]" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="3"></textarea>
						</td>
					</tr>
					<tr class="langkah_kerja_sistem_`+(c+1)+`">
						<td style="text-align: left;" colspan="6">Langkah Kerja</td>
					</tr>
				`;
				var cek = $('.langkah-kerja-sistem-'+(c)).last();
				if(cek.length == 0){
					$(html).insertAfter('.langkah_kerja_sistem_'+(c));
				}else{
					$(html).insertAfter(cek);
				}
				$('.selectpicker').selectpicker();

				$('.cari').on('change', function(){
		        	var id = $(this).data('id');
		        	var tipe = $(this).data('tipe');
		        	var value = parseInt(this.value);
		        	if(value == 1000){
						$('.rencana_sistem_'+id).attr('disabled', 'disabled');
						$('.notif_sistem_'+id).show();
						$('.tambah_langkah_sistem').attr('disabled', 'disabled');
					}else{
						$('.rencana_sistem_'+id).removeAttr("disabled");
						$('.notif_sistem_'+id).hide();
						$('.tambah_langkah_sistem').removeAttr("disabled");
					}
		        	$.ajax({
						url: '{{ url('ajax/option/get-langkah') }}',
						type: 'POST',
						data: {
							_token: "{{ csrf_token() }}",
							id: id,
							value: value,
							tipe: tipe,
						},
					})
					.done(function(response) {
						if(response[2] == 1){
							var rowz = $('tr.langkah-kerja-operasional-'+id).remove();
							var rowspan = response[1].length;
							$('.for_rowspan_operasional_'+id).attr('rowspan', parseInt((rowspan+3)));
							$.each(response[1], function(key, value){
								if(key==0){
									$(value).insertAfter('.langkah_kerja_operasional_'+id);
								}else{
									$(value).insertAfter('.data_detail_operasional-'+id+'-'+(key-1));
								}
							});
						}else if(response[2] == 2){
							var rowz = $('tr.langkah-kerja-keuangan-'+id).remove();
							var rowspan = response[1].length;
							$('.for_rowspan_keuangan_'+id).attr('rowspan', parseInt((rowspan+3)));
							$.each(response[1], function(key, value){
								if(key==0){
									$(value).insertAfter('.langkah_kerja_keuangan_'+id);
								}else{
									$(value).insertAfter('.data_detail_keuangan-'+id+'-'+(key-1));
								}
							});
						}else{
							var rowz = $('tr.langkah-kerja-sistem-'+id).remove();
							var rowspan = response[1].length;
							$('.for_rowspan_sistem_'+id).attr('rowspan', parseInt((rowspan+3)));
							$.each(response[1], function(key, value){
								if(key==0){
									$(value).insertAfter('.langkah_kerja_sistem_'+id);
								}else{
									$(value).insertAfter('.data_detail_sistem-'+id+'-'+(key-1));
								}
							});
						}
					})
				});

				$('.tgl_sistem').datepicker({
		            orientation: "bottom right",
		    		todayHighlight: true,
		    		autoclose:true,
		    		defaultViewDate: {year:$('input[name=tahun]').val(), month:0, day:1},
		    		format: 'dd/mm/yyyy',
		    		yearRange: '-0:+1',
		    		startDate: minDates,
		    		endDate: last,
		    		hideIfNoPrevNext: true,
		        });
		});
		$(document).on('click', '.hapus_langkah_sistem', function (e){
			var id = $(this).data("id");
			var row = $('.data_fokus_audit_sistem_'+id).remove();
			var row = $('.detail_fokus_audit_sistem_'+id).remove();
			var row = $('.langkah-kerja-sistem-'+id).remove();
			var row = $('.langkah_kerja_sistem_'+id).remove();
			var table = $('#table-fokus');
			var rows = table.find('tbody tr.data_sistem');
			$.each(rows, function(key, value){
				console.log($(this).data("id"))
				table.find('.numbur-sistem-'+$(this).data("id")).html(romawi(key+1));
			});
		});
    </script>
    @yield('js-extra')
@endpush

