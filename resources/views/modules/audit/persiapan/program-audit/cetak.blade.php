<html>
<head>
<style>
		@font-face {
		    font-family: 'tahoma';
		    src: url({{ storage_path('fonts\tahoma.ttf') }}) format("truetype");
		    font-weight: 400;
		    font-style: normal;
		}

        @page {
            margin: 60px 25px;
            counter-reset: page;
        }

       /* Define now the real margins of every page in the PDF */
       body {
           margin-top: 3cm;
           margin-left: 2cm;
           margin-right: 2cm;
           margin-bottom: 2cm;
           font-style: normal;
       }

       .ui.table.bordered {
         border: solid black 1px;
         border-collapse: collapse;
         width: 100%;
         font-family : "tahoma", "Verdana", sans-serif;
       }

       .ui.table.bordered td {
         border: solid black .5px;
         border-collapse: collapse;
         /*padding:10px;*/
         padding:5px;
       }

       .ui.table.bordered td.center.aligned {
         text-align : center;
       }

       .page-break {
            page-break-after: always;
       }

       .pad{
       	padding-top: 5px;
       	padding-bottom: 5px;
       }
        header {
            position: fixed;
            top: -40px;
            left: 36px;
            right: 36px;
            height: 10px;
            text-align: center;
            font-size: 11px;
        }
		
       main {
         position: sticky;
         font-size : 12px;
         padding-top : -1cm;
         margin-top : 2px;
         border : none;
         width: 100%;
       }

       main p {
         margin-left: 5px;
         margin-right: 5px;
       }
       footer {
           position: fixed;
           bottom: -60px;
           left: 0px;
           right: 0px;
           height: 50px;
           text-align: center;
           line-height: 35px;
       }
       table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;font-family : "tahoma", "Verdana", sans-serif;}
       table.page_header {width: 100%; border: none; padding: 2mm; font-size: 11px;font-family : "tahoma", "Verdana", sans-serif; text-align: left; margin-left: 50px;margin-top: 50px;}
       table.page_content_header {width: 100%; font-size: 11px;border-collapse: collapse;font-family : "tahoma", "Verdana", sans-serif;text-align: center; padding-bottom: 1px;
       	padding-top: 1px;}
</style>
</head>
<body>
    <header>
    	<table class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;">
    		<tbody>
    			<tr style="">
    				<td style="text-align: left;border: 1px #ffffff;" colspan="2">
    					<table style="">
    						<tbody>
    							<tr>
    								<td style="width: 100px;padding-left: 29px;"><img src="{{ asset('src/img/logo.png') }}" width="85px" height="70px"></td>
    								<td style="width: 312px;text-align: center;"><h2>PROGRAM AUDIT</h2></td>
    								<td style="width: 170px;">
    									<table style="text-align: right;font-size: 11px;" class="page_content ui table bordered">
    										<tbody>
    											<tr>
    												<td style="" colspan="2">&nbsp;</td>
    											</tr>
    											<tr style="">
    												<td style="text-align: center;padding-bottom: 10px;"></td>
    												<td style="text-align: center;padding-bottom: 10px;"></td>
    											</tr>
    										</tbody>
    									</table>
    								</td>
    							</tr>
    						</tbody>
    					</table>
    				</td>
    			</tr>
    		</tbody>
		</table>
  	</header>
  	<br>
  	@php 
		$data_operasional = array();
		foreach($records->detailanggota->where('fungsi', 1) as $operasional) {
		 $data_operasional[] = $operasional->user->name;
		}

		$data_keuangan = array();
		foreach($records->detailanggota->where('fungsi', 2) as $keuangan) {
		 $data_keuangan[] = $keuangan->user->name;
		}

		$data_sistem = array();
		foreach($records->detailanggota->where('fungsi', 3) as $sistem) {
		 $data_sistem[] = $sistem->user->name;
		}
	@endphp
	<main>
		<table class="page_content ui table" style="font-size: 9.5px;">
			<tbody>
				<tr>
					<td style="width: 90px;">Nama Objek Audit</td>
					<td style="width: 10px;">:</td>
					<td>{{ object_audit($records->penugasanaudit->rencanadetail->tipe_object, $records->penugasanaudit->rencanadetail->object_id) }}</td>
					<td rowspan="3" style="width: 100px;">&nbsp;</td>
					<td style="width: 20px;">Auditor</td>
					<td style="width: 10px;">:</td>
					<td>1. {{ implode(", ",$data_operasional) }}</td>
				</tr>
				<tr>
					<td>Surat Penugasan</td>
					<td>:</td>
					<td>{{ $records->penugasanaudit->no_surat }}/IM/WK/{{ $records->penugasanaudit->rencanadetail->rencanaaudit->tahun }}</td>
					<td" colspan="2" rowspan="3">&nbsp;</td>
					<td>2. {{ implode(", ",$data_keuangan) }}</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
					<td>Tgl {{ DateToString($records->penugasanaudit->tgl_surat) }}</td>
					<td>3. {{ implode(", ",$data_sistem) }}</td>
				</tr>
			</tbody>
		</table>
		<br>
		<table class="page_content ui table bordered" style="font-size: 9.5px;">
			<tbody>
				<tr>
					<td style="text-align: center;width: 50%">
						<p>Disusun Oleh</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>{{ $records->user->name }}</p>
						<p style="padding-top: -10px;">(Ketua Tim)</p>
					</td>
					<td style="text-align: center;width: 50%">
						<p>Disetujui Oleh</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>Pius Sutrisno Riyanto</p>
						<p style="padding-top: -10px;">(SVP Internal Audit)</p>
					</td>
				</tr>
			</tbody>
		</table>
		<table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;">
			<tbody>
				<tr style="">
					<td style="border: 1px #ffffff;font-weight: bold;font-size: 9.5px;">A. RUANG LINGKUP</td>
				</tr>
				<tr style="">
					<td style="border: 1px #ffffff;font-size: 9.5px;text-align: justify;">{{ $records->ruang_lingkup }}</td>
				</tr>
			</tbody>
		</table>
		<table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;">
			<tbody>
				<tr style="">
					<td style="border: 1px #ffffff;font-weight: bold;font-size: 9.5px;">B. SASARAN</td>
				</tr>
				<tr style="">
					<td style="border: 1px #ffffff;font-size: 9.5px;text-align: justify;">{{ $records->sasaran }}</td>
				</tr>
			</tbody>
		</table>
	</main>
</body>
</html>