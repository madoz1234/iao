<form action="{{ route($routes.'.saveUbahTim', $record->id) }}" method="POST" id="formData" class="was-validated">
    @method('PATCH')
    @csrf
    <div class="loading dimmer padder-v" style="display: none;">
	    <div class="loader"></div>
	</div>
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Ubah Tim</h5>
    </div>
    @php 
		$data_operasional = array();
		foreach($record->detailanggota->where('fungsi', 1) as $operasional) {
		 $data_operasional[] = $operasional->user_id;
		}

		$data_keuangan = array();
		foreach($record->detailanggota->where('fungsi', 2) as $keuangan) {
		 $data_keuangan[] = $keuangan->user_id;
		}

		$data_sistem = array();
		foreach($record->detailanggota->where('fungsi', 3) as $sistem) {
		 $data_sistem[] = $sistem->user_id;
		}
	@endphp
    <div class="modal-body">
    	<input type="hidden" name="id" value="{{ $record->id }}">
        <div class="form-group field">
            <label class="control-label">Ketua Tim</label>
            <select class="selectpicker form-control show-tick" name="ketua" data-style="btn-default" data-size="3" data-live-search="true">
				@foreach(App\Models\Auths\User::whereHas('roles', function($z){
		                	$z->where('name', ['auditor']);
		                })->get() as $user)
					<option value="{{ $user->id }}" @if($user->id == $record->user_id) selected @endif>{{ $user->name }}</option>
				@endforeach
			</select>
        </div>
        <div class="form-group field">
            <label class="control-label">Auditor Operasional</label>
            <select class="selectpicker form-control show-tick" name="operasional[]" data-style="btn-default" data-size="3" data-live-search="true" multiple>
				@foreach(App\Models\Auths\User::whereHas('roles', function($z){
		                	$z->where('name', ['auditor']);
		                })->get() as $user)
					<option value="{{ $user->id }}" @if(in_array($user->id, $data_operasional)) selected @endif>{{ $user->name }}</option>
				@endforeach
			</select>
        </div>
        <div class="form-group field">
            <label class="control-label">Auditor Keuangan</label>
            <select class="selectpicker form-control show-tick" name="keuangan[]" data-style="btn-default" data-size="3" data-live-search="true" multiple>
				@foreach(App\Models\Auths\User::whereHas('roles', function($z){
		                	$z->where('name', ['auditor']);
		                })->get() as $user)
					<option value="{{ $user->id }}" @if(in_array($user->id, $data_keuangan)) selected @endif>{{ $user->name }}</option>
				@endforeach
			</select>
        </div>
        <div class="form-group field">
            <label class="control-label">Auditor Sistem</label>
            <select class="selectpicker form-control show-tick" name="sistem[]" data-style="btn-default" data-size="3" data-live-search="true" multiple>
				@foreach(App\Models\Auths\User::whereHas('roles', function($z){
		                	$z->where('name', ['auditor']);
		                })->get() as $user)
					<option value="{{ $user->id }}" @if(in_array($user->id, $data_sistem)) selected @endif>{{ $user->name }}</option>
				@endforeach
			</select>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script>

    </script>
    @yield('js-extra')
@endpush