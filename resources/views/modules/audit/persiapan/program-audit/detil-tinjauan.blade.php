@extends('layouts.form')
@section('title', 'Detil Tinjauan Dokumen')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body" style="padding-bottom: 0px;">
		<form action="#" method="POST" id="formData">
			@method('PATCH')
			@csrf
			<div class="panel panel-default data-form">
				<table class="table" style="font-size: 12px;">
			<tbody>
				<tr>
					<td style="width:180px;">Nomor</td>
					<td style="width:2px;">:</td>
					<td style="">
						{{ $record->nomor }}
					</td>
				</tr>
				<tr>
					<td style="width:180px;">Objek Audit</td>
					<td style="width:2px;">:</td>
					<td style="">
						@php 
							$object=array();
						@endphp
						@foreach($cek as $obj)
							@php 
								$object[]= object_audit($obj->penugasanaudit->rencanadetail->tipe_object, $obj->penugasanaudit->rencanadetail->object_id);
							@endphp
						@endforeach
						<table style="font-size: 12px;">
							<tbody>
								@php 
									$j=1;
								@endphp
								@foreach(array_unique($object) as $koy => $ob)
									@if($ob)
										<tr>
											<td style="width: 15px;">{{ $j }}.</td>
											<td style="">{{ $ob }}</td>
										</tr>
										@php 
											$j++;
										@endphp
									@endif
								@endforeach
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td style="width:180px;">Tempat</td>
					<td style="width:2px;">:</td>
					<td style="">
						{{ $record->tempat }}
					</td>
				</tr>
				<tr>
					<td style="width:180px;">Tanggal</td>
					<td style="width:2px;">:</td>
					<td style="">
						{{ DateToString($record->tanggal) }}
					</td>
				</tr>
				<tr>
					<td style="width:180px;">Judul</td>
					<td style="width:2px;">:</td>
					<td style="">
						{{ $record->judul }}
					</td>
				</tr>
				<tr>
					<td style="width:180px;">Deadline</td>
					<td style="width:2px;">:</td>
					<td style="">
						{{ DateToString($record->dateline) }}
					</td>
				</tr>
				<tr>
					<td style="">PIC</td>
					<td style="">:</td>
					<td style="">
						@php 
							$resultstr = array();
						@endphp
						@foreach(object_user_nama($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id) as $kiy => $pic)
							@php
								$resultstr[] = $pic;
							@endphp
						@endforeach
						@php
							$cuk = array_unique($resultstr);
						@endphp
						{{ implode(", ",$cuk) }}
					</td>
				</tr>
				<tr>
					<td style="">Email lain</td>
					<td style="">:</td>
					<td style="">
						@php 
							$resultemail = array();
						@endphp
						@foreach($record->detailemail as $kiy => $em)
							@php
								$resultemail[] = $em->email;
							@endphp
						@endforeach
						@php
							$ema = array_unique($resultemail);
						@endphp
						{{ implode(", ",$ema) }}
					</td>
				</tr>
				<tr style="border-bottom: 1px solid rgba(34,36,38,.1);">
					<td style="">Tembusan</td>
					<td style="">:</td>
					<td style="">
						@php 
							$result = array();
						@endphp
						@foreach($record->detailtembusan as $kiy => $tembusan)
							@php
							$result[] = $tembusan->tembusan;
							@endphp
						@endforeach
						@php
							$cik = array_unique($result);
						@endphp
						{{ implode(", ",$cik) }}
					</td>
				</tr>
				<tr>
					@php 
						$dok = array();
					@endphp
					@foreach($record->detaildokumen as $kiy => $doc)
						@php
							$dok[] = $doc->dokumen->judul;
						@endphp
					@endforeach
					@php
						$data_dok = array_unique($dok);
					@endphp
					<td style="">Dokumen Pendahuluan</td>
					<td style="">:</td>
					<td style="">
						<table class="table table-bordered m-t-none" style="font-size: 12px;">
							<tbody>
								<tr>
									<td style="width: 20px;text-align: center;">No</td>
									<td style="width: 400px;text-align: center;">Judul</td>
									<td style="width: 600px;text-align: center;">File</td>
								</tr>
								@foreach($record->detaildokumen as $kiy => $doc)
									@php
										$dok[] = $doc->dokumen->judul;
									@endphp
									<tr>
										<td style="text-align: center;">{{ $kiy+1 }}</td>
										<td style="">{{ $doc->dokumen->judul }} @if($doc->status > 0) * @endif</td>
										<td style="">
											@if($record->status >= 3)
												@if($record->cariFile() > 0)
													<input id="other-{{$kiy}}" name="other[{{$kiy}}]" data-id="{{ $doc->id }}" data-val="{{$kiy}}" multiple type="file" readonly class="form-control cari" 
							                        data-show-upload="true" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf,image/x-png,image/gif,image/jpeg,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
							                    @else 
							                    	-
							                    @endif
											@else 
											-
											@endif
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					</td>
				</tr>
			</tbody>
		</table>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-left">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
        .table-responsive{
            overflow-x: auto;
            overflow-y: visible !important;
        }

        .table-responsive .dropdown-menu {
	        position: static !important;
	    }
		
	    .input-group.file-caption-main{
            display: none;
        }
    </style>
@endpush
@push('scripts')
    <script> 
    	$('.cari').each(function(){
            var id = $(this).data("id");
            var val = $(this).data("val");
            $.ajax({
                url: '{{ url('ajax/option/get-file') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id
                },
            }).done(function(response) {
            	$('#other-'+val).fileinput({
            		uploadUrl: "/file-upload-batch/1",
            		autoReplace: true,
            		overwriteInitial: true,
					initialPreview: JSON.parse(response[0].replace(/&quot;/g,'"')),
					initialPreviewAsData: true,
					initialPreviewConfig: JSON.parse(response[1].replace(/&quot;/g,'"')),
					initialPreviewShowDelete: false,
					showRemove: false,
					showClose: false,
					layoutTemplates: {actionDelete: ''}, // disable thumbnail deletion
					});
					            	$('.kv-file-zoom').prop('disabled', false);
                
            }).fail(function() {
                console.log("error");
            });
        });
    </script>
    @yield('js-extra')
@endpush
