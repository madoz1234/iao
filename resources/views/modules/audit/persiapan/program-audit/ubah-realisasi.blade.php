@extends('layouts.form')
@section('title', 'Detil Program Audit '.$record->penugasanaudit->rencanadetail->rencanaaudit->tahun)
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
    <div class="panel-body" style="padding-bottom: 0px;">
		<form action="{{ route($routes.'.saveUbahRealisasi', $record->id) }}" method="POST" id="formData">
			@method('PATCH')
		    @csrf
		    <div class="loading dimmer padder-v" style="display: none;">
			    <div class="loader"></div>
			</div>
			<input type="hidden" name="id" value="{{ $record->id }}">
			<input type="hidden" name="penugasan_id" value="{{ $record->penugasan_id }}">
			<input type="hidden" name="tahun" value="{{ $record->penugasanaudit->rencanadetail->rencanaaudit->tahun }}">
			<input type="hidden" name="status" value="0">
			@php 
				$data_operasional = array();
				foreach($record->detailanggota->where('fungsi', 1) as $operasional) {
				 $data_operasional[] = $operasional->user->name;
				}

				$data_keuangan = array();
				foreach($record->detailanggota->where('fungsi', 2) as $keuangan) {
				 $data_keuangan[] = $keuangan->user->name;
				}

				$data_sistem = array();
				foreach($record->detailanggota->where('fungsi', 3) as $sistem) {
				 $data_sistem[] = $sistem->user->name;
				}
			@endphp
			<div class="panel-body" style="padding-bottom: 0px;">
				<div class="form-row">
					<div class="form-group col-md-12">
						<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
							<tr>
								<td style="width: 10%;border: 1px #ffffff;">Tipe</td>
								<td style="width: 1%;border: 1px #ffffff;">:</td>
								<td style="text-align:left;border: 1px #ffffff;" class="field">
									@if($record->penugasanaudit->rencanadetail->tipe == 0)
										<span class="label label-primary">Audit</span>
									@elseif($record->penugasanaudit->rencanadetail->tipe == 1)
										<span class="label label-success">Kegiatan Konsultasi</span>
									@else 
										<span class="label label-info">Kegiatan Audit Lain - Lain</span>
									@endif
								</td>
							</tr>
							<tr>
								<td style="width: 10%;border: 1px #ffffff;">Kategori</td>
								<td style="width: 1%;border: 1px #ffffff;">:</td>
								<td style="text-align:left;border: 1px #ffffff;" class="field">
									@if($record->penugasanaudit->rencanadetail->tipe == 0)
										@if($record->penugasanaudit->rencanadetail->tipe_object == 0)
											<span class="label label-success">Business Unit (BU)</span>
										@elseif($record->penugasanaudit->rencanadetail->tipe_object == 1)
											<span class="label label-info">Corporate Office (CO)</span>
										@elseif($record->penugasanaudit->rencanadetail->tipe_object == 2)
											<span class="label label-default">Project</span>
										@else 
											<span class="label label-primary">Anak Perusahaan</span>
										@endif
									@elseif($record->penugasanaudit->rencanadetail->tipe == 1)
										<span class="label label-warning">{{ $record->penugasanaudit->rencanadetail->konsultasi->nama }}</span>
									@else 
										<span class="label label-default">{{ $record->penugasanaudit->rencanadetail->lain->nama }}</span>
									@endif
								</td>
							</tr>
							<tr>
								<td style="width: 10%;border: 1px #ffffff;">Objek Audit</td>
								<td style="width: 1%;border: 1px #ffffff;">:</td>
								<td style="text-align: left;border: 1px #ffffff;" class="field">
									{!! object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id)!!}
								</td>
							</tr>
							<tr>
								<td style="width: 10%;border: 1px #ffffff;">Ketua Tim</td>
								<td style="width: 1%;border: 1px #ffffff;">:</td>
								<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
									{{ $record->user->name }}
								</td>
							</tr>
							<tr>
								<td style="width: 10%;border: 1px #ffffff;">Auditor Operasional</td>
								<td style="width: 1%;border: 1px #ffffff;">:</td>
								<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
									<div>
										{{ implode(", ",$data_operasional) }}
									</div>
								</td>
							</tr>
							<tr>
								<td style="width: 10%;border: 1px #ffffff;">Auditor Keuangan</td>
								<td style="width: 1%;border: 1px #ffffff;">:</td>
								<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
									<div>
										{{ implode(", ",$data_keuangan) }}
									</div>
								</td>
							</tr>
							<tr>
								<td style="width: 10%;border: 1px #ffffff;">Auditor Sistem</td>
								<td style="width: 1%;border: 1px #ffffff;">:</td>
								<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
									<div>
										{{ implode(", ",$data_sistem) }}
									</div>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="panel-body" style="margin-top: -14px;">
				<div class="form-row">
					<div class="form-group col-md-12">
						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="a-tab" data-toggle="tab" href="#a" role="tab" aria-controls="home" aria-selected="true">A. RUANG LINGKUP</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="b-tab" data-toggle="tab" href="#b" role="tab" aria-controls="profile" aria-selected="false">B. SASARAN</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="c-tab" data-toggle="tab" href="#c" role="tab" aria-controls="messages" aria-selected="false">C. JADWAL AUDIT</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="d-tab" data-toggle="tab" href="#d" role="tab" aria-controls="settings" aria-selected="false">D. FOKUS AUDIT DAN LANGKAH KERJA</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="a" role="tabpanel" aria-labelledby="a-tab">
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr>
										<td scope="col-md-12" colspan="3" style="text-align: left;" class="field">
											<h5 style="font-weight: bold;font-size: 12px;">A. RUANG LINGKUP</h5>
											<p style="text-align: justify;text-justify: inter-word;">{{ $record->ruang_lingkup }}</p>
										</td>
									</tr>
								</table>
								<div class="form-group col-md-12">
									<div class="text-right">
										<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
										<button type="button" class="btn btn-info page1">Selanjutnya</button>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="b" role="tabpanel" aria-labelledby="b-tab">
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr>
										<td scope="col-md-12" colspan="3" style="text-align: left;" class="field">
											<h5 style="font-weight: bold;font-size: 12px;">B. SASARAN</h5>
											<p style="text-align: justify;text-justify: inter-word;">{{ $record->sasaran }}</p>
										</td>
									</tr>
								</table>
								<div class="form-group col-md-12">
									<div class="text-right">
										<button type="button" class="btn btn-cancel kembali1">Sebelumnya</button>
										<button type="button" class="btn btn-info page2">Selanjutnya</button>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="c" role="tabpanel" aria-labelledby="c-tab">
		    					<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;">
									<tr>
										<td scope="col-md-12" colspan="3" style="text-align: left;">
											<h5 style="font-weight: bold;font-size: 12px;">C. JADWAL AUDIT</h5>
											<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
												<tr style="text-align: center;">
													<td scope="col" style="text-align: left;width: 100%; font-weight: bold;border: 1px #ffffff;">C1. PERSIAPAN
													</td>
												</tr>
												<tr style="text-align: center;">
													<td scope="col" style="text-align: left;border: 1px #ffffff;">
														<table id="table-persiapan" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
															<thead>
																<th style="width: 50px;">No</th>
																<th style="text-align: center;">Persiapan</th>
															</thead>
															<tbody class="container-persiapan">
																	@foreach($record->detailjadwal as $kiy => $jadwal)
																		<tr class="data-persiapan-{{ $kiy+1 }}" data-id="{{ $kiy+1 }}">
																			<td style="text-align: center;margin-top: 9px;padding-top: 21px;">{{ $kiy+1 }}</td>
																			<td class="field">
																				<p style="text-align: justify;text-justify: inter-word;">{{ $jadwal->keterangan }}</p>
																			</td>
																		</tr>
																	@endforeach
															</tbody>
														</table>
													</td>
												</tr>
												<tr style="text-align: center;">
													<td scope="col" style="text-align: left;width: 100%; font-weight: bold;border: 1px #ffffff;font-size: 12px;">C2. PELAKSANAAN AUDIT
													</td>
												</tr>
												<tr style="text-align: center;">
													<td scope="col" style="text-align: left;border: 1px #ffffff;">
														@foreach($record->detailpelaksanaan->groupBy('tgl_id') as $a => $cek)
															<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
																<h6 style="font-weight: bold;">{{ object_audit(getRencana($cek->first()->tgl_id)[0], getRencana($cek->first()->tgl_id)[1]) }} -	AB : {{ getAb(getRencana($cek->first()->tgl_id)[0], getRencana($cek->first()->tgl_id)[1]) }}</h6>
																<tr style="text-align: center;">
																	<td scope="col" style="text-align: left;border: 1px #ffffff;">
																		<table id="table-pelaksanaan-{{ $cek->first()->tgl_id }}" data-id="{{ $cek->first()->tgl_id }}" class="table table-bordered m-t-none cek-table" style="width: 100%;font-size: 12px;">
																			<thead>
																				<tr>
																					<th style="text-align: center;" rowspan="2">Hari</th>
																					<th style="text-align: center;" rowspan="2">Tanggal</th>
																					<th style="text-align: center; text-align: center;" colspan="2">Waktu</th>
																					<th style="text-align: center;border-left: 1px solid rgba(34,36,38,.1);" rowspan="2">Item</th>
																				</tr>
																				<tr>
																					<th style="text-align: center;border-top: 1px solid rgba(34,36,38,.1);">Mulai</th>
																					<th style="text-align: center;border-top: 1px solid rgba(34,36,38,.1);">Selesai</th>
																				</tr>
																			</thead>
																			<tbody class="container-pelaksanaan-{{ $cek->first()->tgl_id }}">
																				@php 
																					$iii=0;
																				@endphp
																				@foreach($record->detailpelaksanaan->where('tgl_id', $cek->first()->tgl_id) as $kiy => $pelaksanaan)
																					<tr class="detail_cek-{{ $cek->first()->tgl_id }} data-pelaksanaan-{{ $cek->first()->tgl_id }}-{{$iii}}" data-id="{{$iii}}" data-parent="{{ $cek->first()->tgl_id }}">
																						<td class="data_rowspan-{{ $cek->first()->tgl_id }}-{{$iii}} numboor-{{ $cek->first()->tgl_id }}-{{$iii}}" style="text-align: center;width: 100px;" rowspan="{{$pelaksanaan->detailpelaksanaan->count()}}">{{ romawi(($iii+1)) }}</td>
																						<td class="data_rowspan-{{ $cek->first()->tgl_id }}-{{ $iii }} field" style="text-align: center;width: 200px;" rowspan="{{$pelaksanaan->detailpelaksanaan->count()}}"> 
																							{{$pelaksanaan->tgl}}
																						</td>
																						<td style="text-align: center;width: 150px;" class="field">
																							{{ $pelaksanaan->detailpelaksanaan->first()->mulai }}
																						</td>
																						<td style="text-align: center;width: 150px;" class="field">
																							{{ $pelaksanaan->detailpelaksanaan->first()->selesai }}
																						</td>
																						<td style="text-align: left;width: 800px;text-align: justify;text-justify: inter-word;" class="field">
																							{{ $pelaksanaan->detailpelaksanaan->first()->keterangan }}
																						</td>
																					</tr>
																					@php 
																						$jjj=0;
																					@endphp
																					@foreach($pelaksanaan->detailpelaksanaan as $koy => $detail)
																						@if($jjj > 0)
																							<tr class="detail-pelaksanaan-{{ $cek->first()->tgl_id }}-{{ $iii }} detail-{{ $cek->first()->tgl_id }}-{{ $iii }}-{{ $jjj }}">
																								<td style="text-align: center;width: 150px;" class="field">
																									{{ $detail->mulai }}
																								</td>
																								<td style="text-align: center;width: 150px;" class="field">
																									{{ $detail->selesai }}
																								</td>
																								<td style="text-align: left;width: 800px;text-align: justify;text-justify: inter-word;" class="field">
																									{{ $detail->keterangan }}
																								</td>
																							</tr>
																						@endif
																							@php 
																								$jjj++;
																							@endphp
																					@endforeach
																					@php 
																						$iii++;
																					@endphp
																			@endforeach
																			</tbody>
																		</table>
																	</td>
																</tr>
															</table>
														@endforeach
													</td>
												</tr>
												<tr style="text-align: center;">
													<td scope="col" style="text-align: left;width: 100%; font-weight: bold;border: 1px #ffffff;font-size: 12px;">C3. PENYELESAIAN TUGAS AUDIT
													</td>
												</tr>
												<tr style="text-align: center;">
													<td scope="col" style="text-align: left;border: 1px #ffffff;">
														<table id="table-penyelesaian" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
															<thead>
																<tr>
																	<th style="text-align: center; text-align: center;border-bottom: 1px solid rgba(34,36,38,.1);" rowspan="2">No</th>
																	<th style="text-align: center; text-align: center;border-bottom: 1px solid rgba(34,36,38,.1);" colspan="2">Tanggal</th>
																	<th style="text-align: center;border-left: 1px solid rgba(34,36,38,.1);" rowspan="2">Item</th>
																</tr>
																<tr>
																	<th style="text-align: center;">Mulai</th>
																	<th style="text-align: center;">Selesai</th>
																</tr>
															</thead>
															<tbody class="container-penyelesaian">
																@foreach($record->detailselesai as $kuy => $selesai)
																	<tr class="data-penyelesaian-{{$kuy+1}}" data-id="{{$kuy+1}}">
																		<td style="text-align: center;width: 50px;" class="numbor-{{$kuy+1}}">{{$kuy+1}}</td>
																		<td style="text-align: center;width: 250px;" class="field">
																			{{DateToStrings($selesai->tgl_mulai)}}
																		</td>
																		<td style="text-align: center;width: 250px;" class="field">
																			{{DateToStrings($selesai->tgl_selesai)}}
																		</td>
																		<td style="text-align: left;text-align: justify;text-justify: inter-word;" class="field">{{$selesai->keterangan}}
																		</td>
																	</tr>
																@endforeach
															</tbody>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<div class="form-group col-md-12">
									<div class="text-right">
										<button type="button" class="btn btn-cancel kembali2">Sebelumnya</button>
										<button type="button" class="btn btn-info page3">Selanjutnya</button>
									</div>
								</div>
		    				</div>
							<div class="tab-pane" id="d" role="tabpanel" aria-labelledby="d-tab">
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<tr>
										<td scope="col-md-12" colspan="3" style="text-align: left;">
											<h5 style="font-weight: bold;font-size: 12px;">D. FOKUS AUDIT DAN LANGKAH KERJA</h5>
											<table id="table-fokus" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
												<thead>
													<tr>
														<th style="text-align: center;" rowspan="2">No</th>
														<th style="text-align: center;" colspan="2" rowspan="2">Uraian</th>
														<th style="text-align: center;border-bottom: 1px solid rgba(34,36,38,.1);border-left: 1px solid rgba(34,36,38,.1);" colspan="2">Waktu Audit</th>
														<th style="text-align: center;border-right: 1px solid rgba(34,36,38,.1);" rowspan="2" colspan="3">Keterangan</td>
														</tr>
														<tr>
															<th style="">Rencana</th>
															<th style="">Realisasi</th>
														</tr>
													</thead>
													<tbody>
														{{-- OPERASIONAL --}}
														@if($user == 1)
															<tr>
																<td style="width: 20px;text-align: center;font-weight: bold;">I</td>
																<td style="text-align: left;font-weight: bold;border-right: 1px #ffffff;" colspan="5">OPERASIONAL</td>
																<td style="text-align: center;font-weight: bold;width:50px;border-right: 1px solid rgba(34,36,38,.1);border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
																</td>
															</tr>
																@php 
																	$i=1;
																@endphp
															@foreach($record->detailkerja->where('bidang', 1) as $kiy => $operasional)
																@php
																if($operasional->fokus_audit_id == 1000){
																	$row=0;
																}else{
																	if(!empty($operasional->fokusaudit->langkahkerja->first()->detail)){
																		$row = $operasional->fokusaudit->langkahkerja->first()->detail->count();
																	}else{
																		$row=0;
																	}
																}
																@endphp
																<input type="hidden" name="detail[1000][fokus_audit][0][detail][{{$kiy}}][id]" value="{{ $operasional->id }}">
																<tr class="data_operasional data_fokus_audit_operasional_{{$kiy}}" data-id="{{$kiy}}">
																	<td class="for_rowspan_operasional_{{$kiy}}" style="text-align: center;font-weight: bold;" rowspan="{{$row+3}}">{{$i}}</td>
																	<td style="width: 20px;" rowspan="3">&nbsp;</td>
																	<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="5">Fokus Audit :</td>
																</tr>
																<tr class="detail_fokus_audit_operasional_{{$kiy}}" style="border-bottom: 1px solid rgba(34,36,38,.1);">
																	<td style="width: 800px;" class="field">
																		@if($operasional->fokus_audit_id == 1000)
																			NONE
																		@else 
																			{{ $operasional->fokusaudit->audit }}
																		@endif
																	</td>
																	<td style="width: 150px;" class="field">
																		{{ DateToStrings($operasional->rencana) }}
																	</td>
																	<td style="width: 150px;" class="field">
																		@if($operasional->fokus_audit_id == 1000)
																			-
																		@else
																			<input class="form-control tanggal" id="tanggal" name="detail[1000][fokus_audit][0][detail][{{$kiy}}][realisasi]" placeholder="Realisasi" type="text" value="{{ $operasional->realisasi }}" />
																		@endif
																	</td>
																	<td colspan="2" style="border-right: 1px solid rgba(34,36,38,.1);text-align: left;text-align: justify;text-justify: inter-word;">
																		@if($operasional->keterangan)
																		{{$operasional->keterangan}}
																		@else 
																		-
																		@endif
																	</td>
																</tr>
																<tr class="langkah_kerja_operasional_{{$kiy}}">
																	<td style="text-align: left;" colspan="6">Langkah Kerja</td>
																</tr>
																@if($operasional->fokus_audit_id == 1000)
																@else 
																	@if(!empty($operasional->fokusaudit->langkahkerja->first()->detail))
																	@foreach($operasional->fokusaudit->langkahkerja->first()->detail as $kuy => $detil)
																	<tr class="langkah-kerja-operasional-{{$kiy}}">
																		<td>{{column_letter($kuy+1)}}</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);text-align: justify;text-justify: inter-word;" colspan="5">{{$detil->deskripsi}}</td>
																	</tr>
																	@endforeach
																	@else
																	@endif
																@endif
																@php 
																$i++;
																@endphp
															@endforeach
														@elseif($user == 2)
															{{-- KEUANGAN --}}
															<tr>
																<td style="width: 20px;text-align: center;font-weight: bold;">II</td>
																<td style="text-align: left;font-weight: bold;border-right: 1px #ffffff;" colspan="5">KEUANGAN</td>
																<td style="text-align: center;font-weight: bold;width:50px;border-right: 1px solid rgba(34,36,38,.1);border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
																</td>
															</tr>
															@php 
															$j=1;
															@endphp
															@foreach($record->detailkerja->where('bidang', 2) as $kiy => $keuangan)
																@php
																if($keuangan->fokus_audit_id == 1000){
																	$row=0;
																}else{
																	if(!empty($keuangan->fokusaudit->langkahkerja->first()->detail)){
																		$row = $keuangan->fokusaudit->langkahkerja->first()->detail->count();
																	}else{
																		$row=0;
																	}
																}
																@endphp
																<input type="hidden" name="detail[1000][fokus_audit][1][detail][{{$j}}][id]" value="{{ $keuangan->id }}">
																<tr class="data_keuangan data_fokus_audit_keuangan_{{$kiy}}" data-id="{{$kiy}}">
																	<td class="for_rowspan_keuangan_{{$kiy}}" style="text-align: center;font-weight: bold;" rowspan="{{$row+3}}">{{$j}}</td>
																	<td style="width: 20px;" rowspan="3">&nbsp;</td>
																	<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="5">Fokus Audit :</td>
																</tr>
																<tr class="detail_fokus_audit_keuangan_{{$kiy}}" style="border-bottom: 1px solid rgba(34,36,38,.1);">
																	<td style="width: 800px;" class="field">
																		@if($keuangan->fokus_audit_id == 1000)
																			NONE
																		@else 
																			{{ $keuangan->fokusaudit->audit }}
																		@endif
																	</td>
																	<td style="width: 150px;" class="field">
																		{{ DateToStrings($keuangan->rencana) }}
																	</td>
																	<td style="width: 150px;" class="field">
																		@if($keuangan->fokus_audit_id == 1000)
																			-
																		@else
																			<input class="form-control tanggal" id="tanggal" name="detail[1000][fokus_audit][1][detail][{{$j}}][realisasi]" placeholder="Realisasi" type="text" value="{{ $keuangan->realisasi }}" />
																		@endif
																	</td>
																	<td colspan="2" style="border-right: 1px solid rgba(34,36,38,.1);text-align: left;text-align: justify;text-justify: inter-word;">
																		@if($keuangan->keterangan)
																		{{$keuangan->keterangan}}
																		@else 
																		-
																		@endif
																	</td>
																</tr>
																<tr class="langkah_kerja_keuangan_{{$kiy}}">
																	<td style="text-align: left;" colspan="6">Langkah Kerja</td>
																</tr>
																@if($keuangan->fokus_audit_id == 1000)
																@else
																	@if(!empty($keuangan->fokusaudit->langkahkerja->first()->detail))
																	@foreach($keuangan->fokusaudit->langkahkerja->first()->detail as $kuy => $detil)
																	<tr class="langkah-kerja-keuangan-{{$kiy}}">
																		<td>{{column_letter($kuy+1)}}</td>
																		<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);text-align: justify;text-justify: inter-word;" colspan="5">{{$detil->deskripsi}}</td>
																	</tr>
																	@endforeach
																	@else
																	@endif
																@endif
																@php 
																$j++;
																@endphp
															@endforeach
														@else
															{{-- SISTEM --}}
															<tr>
																<td style="width: 20px;text-align: center;font-weight: bold;">III</td>
																<td style="text-align: left;font-weight: bold;border-right: 1px #ffffff;" colspan="5">SISTEM</td>
																<td style="text-align: center;font-weight: bold;width:50px;border-right: 1px solid rgba(34,36,38,.1);border-right: 1px solid rgba(34,36,38,.1);border-bottom: 1px solid rgba(34,36,38,.1);">
																</td>
															</tr>
															@php 
															$k=1;
															@endphp
															@foreach($record->detailkerja->where('bidang', 3) as $kiy => $sistem)
															@php
															if($sistem->fokus_audit_id == 1000){
																$row=0;
															}else{
																if(!empty($sistem->fokusaudit->langkahkerja->first()->detail)){
																	$row = $sistem->fokusaudit->langkahkerja->first()->detail->count();
																}else{
																	$row=0;
																}
															}
															@endphp
															<input type="hidden" name="detail[1000][fokus_audit][2][detail][{{$k}}][id]" value="{{ $sistem->id }}">
															<tr class="data_sistem data_fokus_audit_sistem_{{$kiy}}" data-id="{{$kiy}}">
																<td class="for_rowspan_sistem_{{$kiy}}" style="text-align: center;font-weight: bold;" rowspan="{{$row+3}}">{{$k}}</td>
																<td style="width: 20px;" rowspan="3">&nbsp;</td>
																<td style="text-align: left;font-weight: bold;border-right: 1px solid rgba(34,36,38,.1);" colspan="5">Fokus Audit :</td>
															</tr>
															<tr class="detail_fokus_audit_sistem_{{$kiy}}" style="border-bottom: 1px solid rgba(34,36,38,.1);">
																<td style="width: 800px;" class="field">
																	@if($sistem->fokus_audit_id == 1000)
																		NONE
																	@else 
																		{{ $sistem->fokusaudit->audit }}
																	@endif
																</td>
																<td style="width: 150px;" class="field">
																	{{ DateToStrings($sistem->rencana) }}
																</td>
																<td style="width: 150px;" class="field">
																	@if($sistem->fokus_audit_id == 1000)
																		-
																	@else
																		<input class="form-control tanggal" id="tanggal" name="detail[1000][fokus_audit][2][detail][{{$k}}][realisasi]" placeholder="Realisasi" type="text" value="{{ $sistem->realisasi }}" />
																	@endif
																</td>
																<td colspan="2" style="border-right: 1px solid rgba(34,36,38,.1);text-align: left;text-align: justify;text-justify: inter-word;">
																	@if($sistem->keterangan)
																	{{$sistem->keterangan}}
																	@else 
																	-
																	@endif
																</td>
															</tr>
															<tr class="langkah_kerja_sistem_{{$kiy}}">
																<td style="text-align: left;" colspan="6">Langkah Kerja</td>
															</tr>
															@if($sistem->fokus_audit_id == 1000)
															@else 
																@if(!empty($sistem->fokusaudit->langkahkerja->first()->detail))
																@foreach($sistem->fokusaudit->langkahkerja->first()->detail as $kuy => $detil)
																<tr class="langkah-kerja-sistem-{{$kiy}}">
																	<td>{{column_letter($kuy+1)}}</td>
																	<td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);text-align: justify;text-justify: inter-word;" colspan="5">{{$detil->deskripsi}}</td>
																</tr>
																@endforeach
																@else
																@endif
															@endif
															@php 
															$k++;
															@endphp
															@endforeach
														@endif
													</tbody>
												</table>
											</table>
										</td>
									</tr>
								</table>
								<div class="form-group col-md-12">
									<div class="text-right">
										<button type="button" class="btn btn-cancel kembali3">Sebelumnya</button>
										<button type="button" class="btn btn-simpan save as page">Submit</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
    </div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script> 
    	$('#myTab li:first-child a').tab('show')
    	$('.btn').tooltip('enable');
        $('.tanggal').datepicker({
            format: 'mm/dd/yyyy',
		    startDate: '-0d',
            orientation: "auto",
            autoclose:true,
        });
        $('.waktu').clockpicker({
            autoclose:true,
            'default': 'now',
        });

        $(document).on('click', '.page1', function (e){
    		$('#a-tab').removeClass('active');
    		$('#a-tab').closest('li').removeClass('active');
    		$('#b-tab').addClass('active');
    		$('#b-tab').closest('li').addClass('active');
    		$('#a').removeClass('active');
    		$('#b').addClass('active');
		});

		$(document).on('click', '.kembali1', function (e){
    		$('#a-tab').addClass('active');
    		$('#a-tab').closest('li').addClass('active');
    		$('#b-tab').removeClass('active');
    		$('#b-tab').closest('li').removeClass('active');
    		$('#a').addClass('active');
    		$('#b').removeClass('active');
		});

		$(document).on('click', '.page2', function (e){
    		$('#b-tab').removeClass('active');
    		$('#b-tab').closest('li').removeClass('active');
    		$('#c-tab').addClass('active');
    		$('#c-tab').closest('li').addClass('active');
    		$('#b').removeClass('active');
    		$('#c').addClass('active');
		});

		$(document).on('click', '.kembali2', function (e){
    		$('#b-tab').addClass('active');
    		$('#b-tab').closest('li').addClass('active');
    		$('#c-tab').removeClass('active');
    		$('#c-tab').closest('li').removeClass('active');
    		$('#b').addClass('active');
    		$('#c').removeClass('active');
		});

		$(document).on('click', '.page3', function (e){
    		$('#c-tab').removeClass('active');
    		$('#c-tab').closest('li').removeClass('active');
    		$('#d-tab').addClass('active');
    		$('#d-tab').closest('li').addClass('active');
    		$('#c').removeClass('active');
    		$('#d').addClass('active');
		});

		$(document).on('click', '.kembali3', function (e){
    		$('#c-tab').addClass('active');
    		$('#c-tab').closest('li').addClass('active');
    		$('#d-tab').removeClass('active');
    		$('#d-tab').closest('li').removeClass('active');
    		$('#c').addClass('active');
    		$('#d').removeClass('active');
		});
    </script>
    @yield('js-extra')
@endpush

