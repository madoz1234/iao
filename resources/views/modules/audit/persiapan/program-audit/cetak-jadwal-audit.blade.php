<html>
<head>
<style>
        @page {
            margin: 60px 25px;
            counter-reset: page;
        }

       /* Define now the real margins of every page in the PDF */
       body {
           margin-top: 3cm;
           margin-left: 0cm;
           margin-right: 0cm;
           margin-bottom: 2cm;
           font-style: normal;
       }

       .ui.table.bordered {
         border: solid black 1px;
         border-collapse: collapse;
         width: 100%;
         font-family : "Arial", "Verdana", sans-serif;
       }

       .ui.table.bordered td {
         border: solid black .5px;
         border-collapse: collapse;
         /*padding:10px;*/
         padding:5px;
       }

       .ui.table.bordered td.center.aligned {
         text-align : center;
       }

       .page-break {
            page-break-after: always;
       }

       .pad{
       	padding-top: 5px;
       	padding-bottom: 5px;
       }
        header {
            position: fixed;
            top: -40px;
            left: 36px;
            right: 36px;
            height: 10px;
            text-align: center;
            font-size: 10px;
        }
		
       main {
         position: sticky;
         font-size : 12px;
         padding-top : -1cm;
         margin-top : 2px;
         border : none;
         width: 100%;
       }

       main p {
         margin-left: 5px;
         margin-right: 5px;
       }
       footer {
           position: fixed;
           bottom: -60px;
           left: 0px;
           right: 0px;
           height: 50px;
           text-align: center;
           line-height: 35px;
       }
       table.page_content {width: 100%; border: none; padding: 1mm; font-size: 10px;font-family : "Arial", "Verdana", sans-serif;}
       table.page_header {width: 100%; border: none; padding: 2mm; font-size: 10px;font-family : "Arial", "Verdana", sans-serif; text-align: left; margin-left: 50px;margin-top: 50px;}
       table.page_content_header {width: 100%; font-size: 10px;border-collapse: collapse;font-family : "Arial", "Verdana", sans-serif;text-align: center; padding-bottom: 1px;
       	padding-top: 1px;}
</style>
</head>
<body>
	<main>
		{{-- OPERASIONAL --}}
		<table class="page_content ui table bordered" style="margin-top: 10px;page-break-inside: auto;padding-right: 0px;padding-left: 0px;padding-top: -110px;">
			<thead>
				<tr>
					<th style="text-align: center;width: 20px;" rowspan="2">No</th>
					<th style="text-align: center;" colspan="2" rowspan="2">Uraian</th>
					<th style="text-align: center;" colspan="2">Waktu Audit</th>
					<th style="text-align: center;" rowspan="2">Keterangan</td>
				</tr>
				<tr>
					<th style="">Rencana</th>
					<th style="">Realisasi</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="text-align: center;font-weight: bold;">I</td>
					<td style="text-align: left;font-weight: bold;" colspan="4">OPERASIONAL</td>
					<td style="text-align: center;font-weight: bold;width:50px;">
					</td>
				</tr>
					@php 
						$i=1;
					@endphp
					@foreach($records->detailkerja->where('bidang', 1) as $kiy => $operasional)
						@php
						if(!empty($operasional->fokusaudit->langkahkerja->first()->detail)){
							$row = $operasional->fokusaudit->langkahkerja->first()->detail->count();
						}else{
							$row=0;
						}
						@endphp
						<tr>
							<td style="text-align: center;font-weight: bold;" rowspan="{{$row+3}}">{{$i}}</td>
							<td style="" rowspan="3">&nbsp;</td>
							<td style="text-align: left;font-weight: bold;" colspan="4">Fokus Audit :</td>
						</tr>
						<tr>
							<td style="width: 300px;">
								{{ $operasional->fokusaudit->audit }}
							</td>
							<td style="width: 100px;">
								{{ DateToStrings($operasional->rencana) }}
							</td>
							<td style="width: 100px;">
								{{ DateToStrings($operasional->realisasi) }}
							</td>
							<td style="text-align: left;text-align: justify;text-justify: inter-word;width: 200px;">
								@if($operasional->keterangan)
								{{$operasional->keterangan}}
								@else 
								-
								@endif
							</td>
						</tr>
						<tr>
							<td style="text-align: left;" colspan="4">Langkah Kerja</td>
						</tr>
						@if(!empty($operasional->fokusaudit->langkahkerja->first()->detail))
							@foreach($operasional->fokusaudit->langkahkerja->first()->detail as $kuy => $detil)
							<tr>
								<td style="text-align: center;width: 10px;">{{column_letter($kuy+1)}}</td>
								<td style="text-align: left;text-align: justify;text-justify: inter-word;" colspan="4">{{$detil->deskripsi}}</td>
							</tr>
							@endforeach
						@else
						@endif
						@php 
						$i++;
						@endphp
					@endforeach
			</tbody>
		</table>

		{{-- KEUANGAN --}}
		<table class="page_content ui table bordered" style="margin-top: 10px;page-break-inside: avoid;padding-right: 0px;padding-left: 0px;padding-top: -110px;">
			<thead>
				<tr>
					<th style="text-align: center;width: 20px;" rowspan="2">No</th>
					<th style="text-align: center;" colspan="2" rowspan="2">Uraian</th>
					<th style="text-align: center;" colspan="2">Waktu Audit</th>
					<th style="text-align: center;" rowspan="2">Keterangan</td>
				</tr>
				<tr>
					<th style="">Rencana</th>
					<th style="">Realisasi</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="text-align: center;font-weight: bold;">II</td>
					<td style="text-align: left;font-weight: bold;" colspan="4">KEUANGAN</td>
					<td style="text-align: center;font-weight: bold;width:50px;">
					</td>
				</tr>
					@php 
						$j=1;
					@endphp
					@foreach($records->detailkerja->where('bidang', 2) as $kiy => $keuangan)
						@php
						if(!empty($keuangan->fokusaudit->langkahkerja->first()->detail)){
							$row = $keuangan->fokusaudit->langkahkerja->first()->detail->count();
						}else{
							$row=0;
						}
						@endphp
						<tr>
							<td style="text-align: center;font-weight: bold;" rowspan="{{$row+3}}">{{$j}}</td>
							<td style="" rowspan="3">&nbsp;</td>
							<td style="text-align: left;font-weight: bold;" colspan="4">Fokus Audit :</td>
						</tr>
						<tr>
							<td style="width: 300px;">
								{{ $keuangan->fokusaudit->audit }}
							</td>
							<td style="width: 100px;">
								{{ DateToStrings($keuangan->rencana) }}
							</td>
							<td style="width: 100px;">
								{{ DateToStrings($keuangan->realisasi) }}
							</td>
							<td style="text-align: left;text-align: justify;text-justify: inter-word;width: 200px;">
								@if($keuangan->keterangan)
								{{$keuangan->keterangan}}
								@else 
								-
								@endif
							</td>
						</tr>
						<tr>
							<td style="text-align: left;" colspan="4">Langkah Kerja</td>
						</tr>
						@if(!empty($keuangan->fokusaudit->langkahkerja->first()->detail))
							@foreach($keuangan->fokusaudit->langkahkerja->first()->detail as $kuy => $detil)
							<tr>
								<td style="text-align: center;width: 10px;">{{column_letter($kuy+1)}}</td>
								<td style="text-align: left;text-align: justify;text-justify: inter-word;" colspan="4">{{$detil->deskripsi}}</td>
							</tr>
							@endforeach
						@else
						@endif
						@php 
						$j++;
						@endphp
					@endforeach
			</tbody>
		</table>

		{{-- SISTEM --}}
		<table class="page_content ui table bordered" style="margin-top: 10px;page-break-inside: avoid;padding-right: 0px;padding-left: 0px;padding-top: -110px;">
			<thead>
				<tr>
					<th style="text-align: center;width: 20px;" rowspan="2">No</th>
					<th style="text-align: center;" colspan="2" rowspan="2">Uraian</th>
					<th style="text-align: center;" colspan="2">Waktu Audit</th>
					<th style="text-align: center;" rowspan="2">Keterangan</td>
				</tr>
				<tr>
					<th style="">Rencana</th>
					<th style="">Realisasi</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="text-align: center;font-weight: bold;">III</td>
					<td style="text-align: left;font-weight: bold;" colspan="4">SISTEM</td>
					<td style="text-align: center;font-weight: bold;width:50px;">
					</td>
				</tr>
					@php 
						$k=1;
					@endphp
					@foreach($records->detailkerja->where('bidang', 3) as $kiy => $sistem)
						@php
						if(!empty($sistem->fokusaudit->langkahkerja->first()->detail)){
							$row = $sistem->fokusaudit->langkahkerja->first()->detail->count();
						}else{
							$row=0;
						}
						@endphp
						<tr>
							<td style="text-align: center;font-weight: bold;" rowspan="{{$row+3}}">{{$k}}</td>
							<td style="" rowspan="3">&nbsp;</td>
							<td style="text-align: left;font-weight: bold;" colspan="4">Fokus Audit :</td>
						</tr>
						<tr>
							<td style="width: 300px;">
								{{ $sistem->fokusaudit->audit }}
							</td>
							<td style="width: 100px;">
								{{ DateToStrings($sistem->rencana) }}
							</td>
							<td style="width: 100px;">
								{{ DateToStrings($sistem->realisasi) }}
							</td>
							<td style="text-align: left;text-align: justify;text-justify: inter-word;width: 200px;">
								@if($sistem->keterangan)
								{{$sistem->keterangan}}
								@else 
								-
								@endif
							</td>
						</tr>
						<tr>
							<td style="text-align: left;" colspan="4">Langkah Kerja</td>
						</tr>
						@if(!empty($sistem->fokusaudit->langkahkerja->first()->detail))
							@foreach($sistem->fokusaudit->langkahkerja->first()->detail as $kuy => $detil)
							<tr>
								<td style="text-align: center;width: 10px;">{{column_letter($kuy+1)}}</td>
								<td style="text-align: left;text-align: justify;text-justify: inter-word;" colspan="4">{{$detil->deskripsi}}</td>
							</tr>
							@endforeach
						@else
						@endif
						@php 
						$k++;
						@endphp
					@endforeach
			</tbody>
		</table>
	</main>
</body>
</html>