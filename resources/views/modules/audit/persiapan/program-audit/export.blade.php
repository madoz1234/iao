<style>
	table{
		border:silver 3px solid;
	}
	table td{
		border-bottom:silver 3px solid;
		border-right:silver 3px solid;
		padding:0 0 0 5px;
	}
	td.v-mid {
	   vertical-align:middle;
	}
</style>
<table border="1" cellpadding="5" style="border:1em solid black;">
	@php 
		$data_operasional = array();
		foreach($record['data']->detailanggota->where('fungsi', 1) as $operasional) {
		 $data_operasional[] = $operasional->user->name;
		}

		$data_keuangan = array();
		foreach($record['data']->detailanggota->where('fungsi', 2) as $keuangan) {
		 $data_keuangan[] = $keuangan->user->name;
		}

		$data_sistem = array();
		foreach($record['data']->detailanggota->where('fungsi', 3) as $sistem) {
		 $data_sistem[] = $sistem->user->name;
		}
	@endphp
    <thead>
    	<tr>
            <th colspan="7" style="text-align: center; font-size: 12px;font-family: 'Tahoma'; font-weight: bold;">PROGRAM AUDIT</th>
        </tr>
        <tr>
            <th colspan="7" style="text-align: left; font-size: 12px;font-family: 'Tahoma'; font-weight: bold;">Nama Objek Audit : {{ object_audit($record['data']->penugasanaudit->rencanadetail->tipe_object, $record['data']->penugasanaudit->rencanadetail->object_id) }}</th>
        </tr>
        <tr>
            <th colspan="7" style="text-align: left; font-size: 12px;font-family: 'Tahoma'; font-weight: bold;">Surat Penugasan : {{ $record['data']->penugasanaudit->no_surat }}/IM/WK/{{ $record['data']->penugasanaudit->rencanadetail->rencanaaudit->tahun }}</th>
        </tr>
        <tr>
            <th colspan="7" style="text-align: left; font-size: 12px;font-family: 'Tahoma'; font-weight: bold;">Auditor :</th>
        </tr>
        <tr>
            <th colspan="7" style="text-align: left; font-size: 12px;font-family: 'Tahoma';">1. {{ implode(", ",$data_operasional) }} <br> 2. {{ implode(", ",$data_keuangan) }}</th>
        </tr>
        <tr>
            <th colspan="7" style="text-align: left; font-size: 12px;font-family: 'Tahoma';">2. {{ implode(", ",$data_keuangan) }}</th>
        </tr>
        <tr>
            <th colspan="7" style="text-align: left; font-size: 12px;font-family: 'Tahoma';">3. {{ implode(", ",$data_sistem) }}</th>
        </tr>
        <tr>
            <th colspan="7" style="text-align: left; font-size: 12px;font-family: 'Tahoma'; font-weight: bold;"></th>
        </tr>
        <tr>
            <th colspan="7" style="text-align: left; font-size: 12px;font-family: 'Tahoma'; font-weight: bold;"></th>
        </tr>
    </thead>
    <tbody>
    	<tr>
            <th colspan="3" rowspan="6" style="text-align: center; font-size: 12px;font-family: 'Tahoma';font-family: 'Tahoma'; font-weight: bold;">{{ $record['data']->user->name }}<br style="mso-data-placement:same-cell;"/><br style="mso-data-placement:same-cell;"/><br style="mso-data-placement:same-cell;"/><br style="mso-data-placement:same-cell;"/>(Ketua Tim)</th>
            <th colspan="4" rowspan="6" style="text-align: center; font-size: 12px;font-family: 'Tahoma';font-family: 'Tahoma'; font-weight: bold;">SVP Internal Audit<br style="mso-data-placement:same-cell;"/><br style="mso-data-placement:same-cell;"/><br style="mso-data-placement:same-cell;"/><br style="mso-data-placement:same-cell;"/>Pius Sutrisno Riyanto</th>
        </tr>
        <tr>
            <th colspan="7" style=""></th>
        </tr>
        <tr>
            <th colspan="7" style=""></th>
        </tr>
        <tr>
            <th colspan="7" style=""></th>
        </tr>
        <tr>
            <th colspan="7" style=""></th>
        </tr>
        <tr>
            <th colspan="7" style=""></th>
        </tr>
        <tr>
            <th colspan="7" style=""></th>
        </tr>
        <tr>
            <th colspan="7" style=""></th>
        </tr>
    	<tr>
            <th colspan="7" style="text-align: left; font-size: 12px;font-family: 'Tahoma'; font-weight: bold;">A. RUANG LINGKUP</th>
        </tr>
        <tr>
            <th colspan="7" style="text-align: left; font-size: 12px;font-family: 'Tahoma';">{{ $record['data']->ruang_lingkup }}</th>
        </tr>
        <tr>
            <th colspan="7" style="text-align: left; font-size: 12px;font-family: 'Tahoma'; font-weight: bold;">B. SASARAN</th>
        </tr>
        <tr>
            <th colspan="7" style="text-align: left; font-size: 12px;font-family: 'Tahoma';">{{ $record['data']->sasaran }}</th>
        </tr>
    	<tr>
            <th colspan="7" style="text-align: left; font-size: 12px;font-family: 'Tahoma'; font-weight: bold;">C. JADWAL AUDIT</th>
        </tr>
        <tr>
            <th colspan="7" style="text-align: left; font-size: 12px;font-family: 'Tahoma'; font-weight: bold;">C.1 PERSIAPAN</th>
        </tr>
    	<tr>
			<td style="width:5px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';">No</td>
			<td colspan="6" style="width:100px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';">Persiapan</td>
		</tr>
		@foreach($record['data']->detailjadwal as $kiy => $jadwal)
			<tr>
				<td style="text-align: center;width: 5px;font-size: 12px;font-family: 'Tahoma';">{{ $kiy+1 }}</td>
				<td colspan="6" style="text-align: justify;font-size: 12px;font-family: 'Tahoma';">{{ $jadwal->keterangan }}</td>
			</tr>
		@endforeach
		<tr>
            <th colspan="7" style="text-align: left; font-size: 12px;font-family: 'Tahoma'; font-weight: bold;"></th>
        </tr>
        <tr>
            <th colspan="7" style="text-align: left; font-size: 12px;font-family: 'Tahoma'; font-weight: bold;">C.2 PELAKSANAAN AUDIT DI PROYEK</th>
        </tr>
        <tr>
			<th style="width:5px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';" rowspan="2">No</th>
			<th style="width:100px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';" colspan="2" rowspan="2">Rangkaian Kegiatan Audit</th>
			<th style="width:25px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';" rowspan="2">Kategori</th>
			<th style="width:25px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';" rowspan="2">Tanggal</th>
			<th style="width:20px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';" colspan="2">Waktu</th>
		</tr>
		<tr>
			<th style="width:20px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';">Mulai</th>
			<th style="width:20px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';">Selesai</th>
		</tr>
        <tr>
			<td style="font-weight: bold;background-color: #ececec;font-size: 12px;font-family: 'Tahoma';text-align: center;">1</td>
			<td style="font-weight: bold;background-color: #ececec;font-size: 12px;font-family: 'Tahoma';" colspan="2">{{ object_audit($record['data']->penugasanaudit->rencanadetail->tipe_object, $record['data']->penugasanaudit->rencanadetail->object_id) }} -	AB : {{ getAb($record['data']->penugasanaudit->rencanadetail->tipe_object, $record['data']->penugasanaudit->rencanadetail->object_id) }}</td>
			<td style="text-align: center;font-weight: bold;background-color: #ececec;">
				@if($record['data']->penugasanaudit->rencanadetail->tipe_object == 0)
					Business Unit (BU)
				@elseif($record['data']->penugasanaudit->rencanadetail->tipe_object == 1)
					Corporate Office (CO)
				@elseif($record['data']->penugasanaudit->rencanadetail->tipe_object == 2)
					{{ getParentBuNama($record['data']->penugasanaudit->rencanadetail->object_id) }}
				@else 
					Anak Perusahaan
				@endif
			</td>
			<td style="background-color: #ececec;">&nbsp;</td>
			<td style="background-color: #ececec;">&nbsp;</td>
			<td style="background-color: #ececec;">&nbsp;</td>
		</tr>
		@php 
			$j=0;
		@endphp
		@foreach($record['data']->detailpelaksanaan as $kiy => $pelaksanaan)
			<tr>
				<td style="">&nbsp;</td>
				<td style="width: 10px;text-align: center;font-size: 12px;display: table-cell;vertical-align: baseline; mso-vertical-align-alt: middle;font-family: 'Tahoma';" valign="middle" rowspan="{{$pelaksanaan->detailpelaksanaan->count()}}">HARI {{ romawi(($j+1)) }}</td>
				<td style="width: 90px;font-size: 12px;font-family: 'Tahoma';">{{ $pelaksanaan->detailpelaksanaan->first()->keterangan }}</td>
				<td style="">&nbsp;</td>
				<td style="text-align: center;font-size: 12px;font-family: 'Tahoma';" rowspan="{{$pelaksanaan->detailpelaksanaan->count()}}">{{DateToStringWday($pelaksanaan->tgl)}}</td>
				<td style="text-align: center;font-size: 12px;font-family: 'Tahoma';">{{ $pelaksanaan->detailpelaksanaan->first()->mulai }}</td>
				<td style="text-align: center;font-size: 12px;font-family: 'Tahoma';">{{ $pelaksanaan->detailpelaksanaan->first()->selesai }}</td>
			</tr>
			@php 
				$k=0;
			@endphp
			@foreach($pelaksanaan->detailpelaksanaan as $koy => $detail)
				@if($k > 0)
					<tr>
						<td style="">&nbsp;</td>
						<td style="font-size: 12px;font-family: 'Tahoma';">{{ $detail->keterangan }}</td>
						<td style=""></td>
						<td style="text-align: center;font-size: 12px;font-family: 'Tahoma';">{{ $detail->mulai }}</td>
						<td style="text-align: center;font-size: 12px;font-family: 'Tahoma';">{{ $detail->selesai }}</td>
						<td style="">&nbsp;</td>
					</tr>
				@endif
				@php 
					$k++;
				@endphp
			@endforeach
			@php 
	    		$j++;
	    	@endphp
		@endforeach

		{{-- PENYELESAIAN --}}
    	<tr>
            <th colspan="7" style="text-align: left; font-size: 12px;font-family: 'Tahoma'; font-weight: bold;"></th>
        </tr>
        <tr>
            <th colspan="7" style="text-align: left; font-size: 12px;font-family: 'Tahoma'; font-weight: bold;">C.3 PENYELESAIAN TUGAS AUDIT</th>
        </tr>
        <tr>
			<th style="width:5px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';" rowspan="2">No</th>
			<th style="width:100px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';" colspan="2">Tanggal</th>
			<th style="width:25px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';" rowspan="2" colspan="4">Item</th>
		</tr>
		<tr>
			<th style="width:20px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';">Mulai</th>
			<th style="width:20px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';">Selesai</th>
		</tr>
		@foreach($record['data']->detailselesai as $kuy => $selesai)
			<tr>
				<td style="text-align: center;width: 5px;font-size: 12px;font-family: 'Tahoma';">{{$kuy+1}}</td>
				<td style="text-align: center;width: 20px;font-size: 12px;font-family: 'Tahoma';">
					{{DateToStrings($selesai->tgl_mulai)}}
				</td>
				<td style="text-align: center;width: 20px;font-size: 12px;font-family: 'Tahoma';">
					{{DateToStrings($selesai->tgl_selesai)}}
				</td>
				<td style="text-align: left;text-align: justify;text-justify: inter-word;font-size: 12px;font-family: 'Tahoma';" colspan="4">{{$selesai->keterangan}}</td>
			</tr>
		@endforeach

		<tr>
            <th colspan="7" style="text-align: left; font-size: 12px;font-family: 'Tahoma'; font-weight: bold;"></th>
        </tr>
        <tr>
            <th colspan="7" style="text-align: left; font-size: 12px;font-family: 'Tahoma'; font-weight: bold;">D. FOKUS AUDIT DAN LANGKAH KERJA</th>
        </tr>
        <tr>
			<th style="text-align: center;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';" rowspan="2">No</th>
			<th style="text-align: center;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';" colspan="2" rowspan="2">Uraian</th>
			<th style="text-align: center;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';" colspan="2">Waktu Audit</th>
			<th style="text-align: center;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';" rowspan="2" colspan="2">Keterangan</th>
		</tr>
		<tr>
			<th style="background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';">Rencana</th>
			<th style="background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';">Realisasi</th>
		</tr>
		<tr>
			<td style="width: 5px;text-align: center;font-weight: bold;font-size: 12px;font-family: 'Tahoma';">I</td>
			<td style="text-align: left;font-weight: bold;font-size: 12px;font-family: 'Tahoma';" colspan="6">OPERASIONAL</td>
			<td style="text-align: center;font-weight: bold;width:50px;font-size: 12px;font-family: 'Tahoma';">
			</td>
		</tr>
		@php 
			$i=1;
		@endphp
		@foreach($record['data']->detailkerja->where('bidang', 1) as $kiy => $operasional)
			@php
			if($operasional->fokus_audit_id == 1000){
				$row=0;
			}else{
				if(!empty($operasional->fokusaudit->langkahkerja->first()->detail)){
					$row = $operasional->fokusaudit->langkahkerja->first()->detail->count();
				}else{
					$row=0;
				}
			}
			@endphp
			<tr>
				<td style="text-align: center;font-weight: bold;font-size: 12px;font-family: 'Tahoma';" rowspan="{{$row+3}}">{{$i}}</td>
				<td style="width: 20px;" rowspan="3">&nbsp;</td>
				<td style="text-align: left;font-weight: bold;font-size: 12px;font-family: 'Tahoma';" colspan="5">Fokus Audit :</td>
			</tr>
			<tr style="border-bottom: 1px solid rgba(34,36,38,.1);">
				<td style="width: 50px;font-size: 12px;font-family: 'Tahoma';">
					@if($operasional->fokus_audit_id == 1000)
						NONE
					@else 
						{{ $operasional->fokusaudit->audit }}
					@endif
				</td>
				<td style="width: 25px;text-align:center;font-size: 12px;font-family: 'Tahoma';">
					{{ DateToStrings($operasional->rencana) }}
				</td>
				<td style="width: 25px;text-align:center;font-size: 12px;font-family: 'Tahoma';">
					{{ DateToStrings($operasional->realisasi) }}
				</td>
				<td colspan="2" style="text-align: left;text-align: justify;text-justify: inter-word;font-size: 12px;font-family: 'Tahoma';">
					@if($operasional->keterangan)
					{{$operasional->keterangan}}
					@else 
					-
					@endif
				</td>
			</tr>
			<tr>
				<td style="text-align: left;font-size: 12px;font-family: 'Tahoma';" colspan="5">Langkah Kerja</td>
			</tr>
			@if($operasional->fokus_audit_id == 1000)
			@else 
				@if(!empty($operasional->fokusaudit->langkahkerja->first()->detail))
					@foreach($operasional->fokusaudit->langkahkerja->first()->detail as $kuy => $detil)
					<tr>
						<td style="text-align:center;font-size: 12px;font-family: 'Tahoma';">{{column_letter($kuy+1)}}</td>
						<td style="text-align: left;text-align: justify;text-justify: inter-word;font-size: 12px;font-family: 'Tahoma';" colspan="5">{{$detil->deskripsi}}</td>
					</tr>
					@endforeach
				@else
				@endif
			@endif
			@php 
			$i++;
			@endphp
		@endforeach
		{{-- KEUANGAN --}}

		<tr>
			<td style="width: 5px;text-align: center;font-weight: bold;font-size: 12px;font-family: 'Tahoma';">II</td>
			<td style="text-align: left;font-weight: bold;font-size: 12px;font-family: 'Tahoma';" colspan="6">KEUANGAN</td>
			<td style="text-align: center;font-weight: bold;width:50px;font-size: 12px;font-family: 'Tahoma';">
			</td>
		</tr>
		@php 
			$i=1;
		@endphp
		@foreach($record['data']->detailkerja->where('bidang', 2) as $kiy => $keuangan)
			@php
			if($keuangan->fokus_audit_id == 1000){
				$row=0;
			}else{
				if(!empty($keuangan->fokusaudit->langkahkerja->first()->detail)){
					$row = $keuangan->fokusaudit->langkahkerja->first()->detail->count();
				}else{
					$row=0;
				}
			}
			@endphp
			<tr>
				<td style="text-align: center;font-weight: bold;font-size: 12px;font-family: 'Tahoma';" rowspan="{{$row+3}}">{{$i}}</td>
				<td style="width: 20px;font-size: 12px;font-family: 'Tahoma';" rowspan="3">&nbsp;</td>
				<td style="text-align: left;font-weight: bold;font-size: 12px;font-family: 'Tahoma';" colspan="5">Fokus Audit :</td>
			</tr>
			<tr style="border-bottom: 1px solid rgba(34,36,38,.1);">
				<td style="width: 50px;font-size: 12px;font-family: 'Tahoma';">
					@if($keuangan->fokus_audit_id == 1000)
						NONE
					@else 
						{{ $keuangan->fokusaudit->audit }}
					@endif
				</td>
				<td style="width: 25px;text-align:center;font-size: 12px;font-family: 'Tahoma';">
					{{ DateToStrings($keuangan->rencana) }}
				</td>
				<td style="width: 25px;text-align:center;font-size: 12px;font-family: 'Tahoma';">
					{{ DateToStrings($keuangan->realisasi) }}
				</td>
				<td colspan="2" style="text-align: left;text-align: justify;text-justify: inter-word;font-size: 12px;font-family: 'Tahoma';">
					@if($keuangan->keterangan)
					{{$keuangan->keterangan}}
					@else 
					-
					@endif
				</td>
			</tr>
			<tr>
				<td style="text-align: left;font-size: 12px;font-family: 'Tahoma';" colspan="5">Langkah Kerja</td>
			</tr>
			@if($keuangan->fokus_audit_id == 1000)
			@else
				@if(!empty($keuangan->fokusaudit->langkahkerja->first()->detail))
					@foreach($keuangan->fokusaudit->langkahkerja->first()->detail as $kuy => $detil)
					<tr>
						<td style="text-align:center;font-size: 12px;font-family: 'Tahoma';">{{column_letter($kuy+1)}}</td>
						<td style="text-align: left;text-align: justify;text-justify: inter-word;font-size: 12px;font-family: 'Tahoma';" colspan="5">{{$detil->deskripsi}}</td>
					</tr>
					@endforeach
				@else
				@endif
			@endif
			@php 
			$i++;
			@endphp
		@endforeach
		{{-- SISTEM --}}

		<tr>
			<td style="width: 5px;text-align: center;font-weight: bold;font-size: 12px;font-family: 'Tahoma';">III</td>
			<td style="text-align: left;font-weight: bold;font-size: 12px;font-family: 'Tahoma';" colspan="6">SISTEM</td>
			<td style="text-align: center;font-weight: bold;width:50px;font-size: 12px;font-family: 'Tahoma';">
			</td>
		</tr>
		@php 
			$i=1;
		@endphp
		@foreach($record['data']->detailkerja->where('bidang', 3) as $kiy => $sistem)
			@php
			if($sistem->fokus_audit_id == 1000){
				$row=0;
			}else{
				if(!empty($sistem->fokusaudit->langkahkerja->first()->detail)){
					$row = $sistem->fokusaudit->langkahkerja->first()->detail->count();
				}else{
					$row=0;
				}
			}
			@endphp
			<tr>
				<td style="text-align: center;font-weight: bold;font-size: 12px;font-family: 'Tahoma';" rowspan="{{$row+3}}">{{$i}}</td>
				<td style="width: 20px;font-size: 12px;font-family: 'Tahoma';" rowspan="3">&nbsp;</td>
				<td style="text-align: left;font-weight: bold;font-size: 12px;font-family: 'Tahoma';" colspan="5">Fokus Audit :</td>
			</tr>
			<tr style="border-bottom: 1px solid rgba(34,36,38,.1);font-size: 12px;font-family: 'Tahoma';">
				<td style="width: 50px;font-size: 12px;font-family: 'Tahoma';">
					@if($sistem->fokus_audit_id == 1000)
						NONE
					@else 
						{{ $sistem->fokusaudit->audit }}
					@endif
				</td>
				<td style="width: 25px;text-align:center;font-size: 12px;font-family: 'Tahoma';">
					{{ DateToStrings($sistem->rencana) }}
				</td>
				<td style="width: 25px;text-align:center;font-size: 12px;font-family: 'Tahoma';">
					{{ DateToStrings($sistem->realisasi) }}
				</td>
				<td colspan="2" style="text-align: left;text-align: justify;text-justify: inter-word;font-size: 12px;font-family: 'Tahoma';">
					@if($sistem->keterangan)
					{{$sistem->keterangan}}
					@else 
					-
					@endif
				</td>
			</tr>
			<tr>
				<td style="text-align: left;font-size: 12px;font-family: 'Tahoma';" colspan="5">Langkah Kerja</td>
			</tr>
			@if($sistem->fokus_audit_id == 1000)
			@else 
				@if(!empty($sistem->fokusaudit->langkahkerja->first()->detail))
					@foreach($sistem->fokusaudit->langkahkerja->first()->detail as $kuy => $detil)
					<tr>
						<td style="text-align:center;font-size: 12px;font-family: 'Tahoma';">{{column_letter($kuy+1)}}</td>
						<td style="text-align: left;text-align: justify;text-justify: inter-word;font-size: 12px;font-family: 'Tahoma';" colspan="5">{{$detil->deskripsi}}</td>
					</tr>
					@endforeach
				@else
				@endif
			@endif
			@php 
			$i++;
			@endphp
		@endforeach
    </tbody>
</table>
