<html>
<head>
<style>
		@font-face {
		    font-family: 'tahoma';
		    src: url({{ storage_path('fonts\tahoma.ttf') }}) format("truetype");
		    font-weight: 400;
		    font-style: normal;
		}

        @page {
            margin: 60px 25px;
            counter-reset: page;
        }

       /* Define now the real margins of every page in the PDF */
       body {
           margin-top: 3cm;
           margin-left: 2cm;
           margin-right: 2cm;
           margin-bottom: 2cm;
           font-style: normal;
       }

       .ui.table.bordered {
         border: solid black 1px;
         border-collapse: collapse;
         width: 100%;
         font-family : "tahoma", "Verdana", sans-serif;
       }

       .ui.table.bordered td {
         border: solid black .5px;
         border-collapse: collapse;
         /*padding:10px;*/
         padding:5px;
       }

       .ui.table.bordered td.center.aligned {
         text-align : center;
       }

       .page-break {
            page-break-after: always;
       }

       .pad{
       	padding-top: 5px;
       	padding-bottom: 5px;
       }
        header {
            position: fixed;
            top: -40px;
            left: 74px;
            right: 74px;
            height: 10px;
            text-align: center;
            font-size: 11px;
        }
		
       main {
         position: sticky;
         font-size : 12px;
         padding-top : -1cm;
         margin-top : 2px;
         border : none;
         width: 100%;
       }

       main p {
         margin-left: 5px;
         margin-right: 5px;
       }
       footer {
           position: fixed;
           bottom: -60px;
           left: 0px;
           right: 0px;
           height: 50px;
           text-align: center;
           line-height: 35px;
       }
       table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;font-family : "tahoma", "Verdana", sans-serif;}
       table.page_header {width: 100%; border: none; padding: 2mm; font-size: 11px;font-family : "tahoma", "Verdana", sans-serif; text-align: left; margin-left: 50px;margin-top: 50px;}
       table.page_content_header {width: 100%; font-size: 11px;border-collapse: collapse;font-family : "tahoma", "Verdana", sans-serif;text-align: center; padding-bottom: 1px;
       	padding-top: 1px;}
</style>
</head>
<body>
    <header>
    	<table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;">
    		<tbody>
    			<tr style="">
    				<td style="text-align: left;border: 1px #ffffff;" colspan="2">
    					<table style="">
    						<tbody>
    							<tr>
    								<td style="width: 100px;padding-left: -10px;"><img src="{{ asset('src/img/logo.png') }}" width="85px" height="70px"></td>
    								<td style="width: 305px;"><h2>PT. WASKITA KARYA <small>(Persero)</small> Tbk.</h2></td>
    								<td style="width: 170px">
    								</td>
    							</tr>
    						</tbody>
    					</table>
    				</td>
    			</tr>
    			<tr style="">
    				<td style="border: 1px #ffffff;width: 320px;">Nomor : {{$records->nomor}}</td>
    				<td style="border: 1px #ffffff;text-align: center;">Jakarta, {{ DateToStringWday($records->tanggal) }}</td>
    			</tr>
    		</tbody>
		</table>
  	</header>
  	<br>
	<main>
		<table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;">
			<tbody>
				<tr style="">
					<td style="text-align: left;border: 1px #ffffff;" colspan="2">Kepada Yth.</td>
				</tr>
				@foreach($records->detailpenerima as $kiy => $pic)
					<tr style="">
						<td style="border: 1px #ffffff;width: 5px !important;">{{ $kiy+1 }}.</td>
						@if($pic->user_id == 1)
							<td style="border: 1px #ffffff;">{{ $pic->nama }}</td>
						@else 
							<td style="border: 1px #ffffff;">{{ $pic->user->name }}</td>
						@endif
					</tr>
				@endforeach
			</tbody>
		</table>
		<table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;">
			<tbody>
				<tr>
					<td style="border: 1px #ffffff;">Di-</td>
				</tr>
				<tr>
					<td style="border: 1px #ffffff;">Tempat</td>
				</tr>
				<tr style="text-align: center;">
					<td style="border: 1px #ffffff;font-weight: bold;">Perihal : Permintaan Data</td>
				</tr>
				<tr style="">
					<td style="border: 1px #ffffff;">Dengan hormat, </td>
				</tr>
				<tr style="">
					<td style="border: 1px #ffffff;"></td>
				</tr>
				<tr style="">
					<td style="border: 1px #ffffff;"></td>
				</tr>
				<tr style="">
					<td style="border: 1px #ffffff;"></td>
				</tr>
				<tr style="">
					@php 
						$object=array();
						$objects=array();
					@endphp
					@foreach($cek as $obj)
						@php 
							$object[]= object_audit($obj->penugasanaudit->rencanadetail->tipe_object, $obj->penugasanaudit->rencanadetail->object_id);
							if($obj->penugasanaudit->rencanadetail->tipe_object == 0){
								$objects[]='Business Unit';
							}elseif($obj->penugasanaudit->rencanadetail->tipe_object == 1){
								$objects[]='Corporate Office';
							}elseif($obj->penugasanaudit->rencanadetail->tipe_object == 2){
								$objects[]='Proyek';
							}else{
								$objects[]='Anak Perusahaan';
							}
						@endphp
					@endforeach
					@php 
						$object = implode(", ",$object);
						$objects = implode(" / ",array_unique($objects));
					@endphp
					<td style="border: 1px #ffffff;">Sehubungan dengan akan dilaksanakan audit internal pada {{ $objects }}, {{ ($object) }}, mohon dikirimkan {{ ($records->judul)}} yaitu sebagai berikut : </td>
				</tr>
			</tbody>
		</table>
		<table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;">
			<tbody>
				@foreach($records->detaildokumen as $key => $doc)
					<tr style="">
						<td style="border: 1px #ffffff;width: 5px !important;">{{ $key+1 }}.</td>
						<td style="border: 1px #ffffff;">{{ $doc->dokumen->judul }} @if($doc->status > 0) * @endif</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;">
			<tbody>
				<tr style="">
					<td style="border: 1px #ffffff;">Lampiran tersebut agar dikirimkan ke {{$records->email}} paling lambat tanggal {{ DateToStringWday($records->dateline) }} (untuk yang diberi tanda * dalam format Ms.Excel.</td>
				</tr>
				<tr style="">
					<td style="border: 1px #ffffff;"><br>Demikian disampaikan, atas perhatiannya diucapkan terimakasih.</td>
				</tr>
			</tbody>
		</table>
		<table class="page_content_header" style="padding-top: 30px;">
            <tr>
                <td style="text-align: left;padding-left: 9px;">
                    <b>SVP Internal Audit</b><br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    (Pius Sutrisno Riyanto)
                </td>
            </tr>
        </table>
        <table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;">
			<tbody>
				<tr style="">
					<td style="text-align: left;border: 1px #ffffff;" colspan="2">Tembusan :</td>
				</tr>
				@if(count($records->detailtembusan) > 0)
					@foreach($records->detailtembusan as $key => $tembusan)
						<tr style="">
							<td style="border: 1px #ffffff;width: 5px !important;">{{ $key+1 }}.</td>
							<td style="border: 1px #ffffff;">{{ $tembusan->tembusan }}</td>
						</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</main>
</body>
</html>