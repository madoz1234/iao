@extends('layouts.form')
@section('title', 'Upload Dokumen')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body" style="padding-bottom: 0px;">
		<form action="{{ route($routes.'.simpanDokumen', $record->id) }}" method="POST" id="formData" enctype="multipart/form-data">
			@method('PATCH')
		    @csrf
		    <div class="loading dimmer padder-v" style="display: none;">
			    <div class="loader"></div>
			</div>
		    <input type="hidden" name="id" value="{{ $record->id }}">
		    <input type="hidden" name="akhir" value="{{ $record->status }}">
		    <input type="hidden" name="status" value="0">
			<div class="form-row">
				@if($record->ket_svp)
					<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
						<tr>
							<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
								<h5 style="font-weight: bold;font-size: 12px;border: 1px #ffffff;">Alasan Penolakan :</h5>
								<p style="text-align: justify;text-justify: inter-word;border: 1px #ffffff;">{{ $record->ket_svp }}</p>
							</td>
						</tr>
					</table>
				@endif
				<div class="form-group col-md-12">
					<table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
						<thead style="background-color: #f9fafb;">
							<tr>
								<th scope="col" style="text-align: center;width: 50px;">No</th>
								<th scope="col" style="text-align: center;">Judul Dokumen</th>
								<th scope="col" style="text-align: center;">Deksripsi</th>
								<th scope="col" width="400px" style="text-align: center;">File</th>
							</tr>
						</thead>
						<tbody class="container">
							@foreach($record->detaildokumen as $key => $value)
								<tr>
									<td>
										{{ $key+1 }}
									</td>
									<td style="width: 200px;text-align: center;">
										{{ $value->dokumen->judul }} @if($value->status == 1) * @endif
									</td>
									<td style="width: 700px;text-align: justify;text-justify: inter-word;">
										{{ $value->dokumen->deskripsi }}
									</td>
									<td class="field">
										@if($value->status == 1)
											<div class="file-loading">
												<input id="other-{{ $key }}"  name="detail[{{ $value->id }}][surat]" data-id="{{$value->id}}" data-val="{{$key}}" type="file" class="cari form-control cfiles" 
												data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
											</div>
										@else 
											<div class="file-loading">
												<input id="other-{{ $key }}"  name="detail[{{ $value->id }}][surat]" data-id="{{$value->id}}" data-val="{{$key}}" type="file" class="cari form-control cfile" 
												data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf">
											</div>
										@endif
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					<div class="form-group col-md-12">
						<div class="text-right">
							<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
							<button type="button" class="btn btn-primary save as drafting">Save As Draft</button>
							<button type="button" class="btn btn-simpan save as page">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</form>
    </div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script> 
		$('.cari').each(function(){
            var id = $(this).data("id");
            var val = $(this).data("val");
            $.ajax({
                url: '{{ url('ajax/option/get-file') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id
                },
            }).done(function(response) {
            	if(response.length > 0){
	            	$('#other-'+val).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		validateInitialCount: true,
	            		autoReplace: true,
						initialPreview: JSON.parse(response[0].replace(/&quot;/g,'"')),
						initialPreviewAsData: true,
						initialPreviewConfig: JSON.parse(response[1].replace(/&quot;/g,'"')),
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}else{
            		$('#other-'+val).fileinput({
	            		uploadUrl: "/file-upload-batch/1",
	            		overwriteInitial: true,
	            		validateInitialCount: true,
	            		autoReplace: true,
						initialPreviewAsData: true,
						initialPreviewShowDelete: false,
						showRemove: false,
						showClose: false,
						showUpload: false,
						layoutTemplates: {
							actionDelete: '',
							actionUpload: '',
						}, // disable thumbnail deletion
					});
            	}
					$('.kv-file-upload').prop('disabled', false);
					$('.kv-file-zoom').prop('disabled', false);
            }).fail(function() {
                console.log("error");
            });
        });
    </script>
    @yield('js-extra')
@endpush
