@extends('layouts.form')
@section('title', 'Tinjauan Dokumen '.$record->penugasanaudit->rencanadetail->rencanaaudit->tahun)
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-heading" style="font-weight: bold;font-size: 13px;">Rencana Audit</div>
	<div class="panel-body">
		<table class="table" style="font-size: 12px;">
			<tbody>
				<tr>
					<td style="width:180px;">Kategori</td>
					<td style="width:2px;">:</td>
					<td style="">
						@if($record->penugasanaudit->rencanadetail->tipe_object == 0)
							<span class="label label-success">Business Unit (BU)</span>
						@elseif($record->penugasanaudit->rencanadetail->tipe_object == 1)
							<span class="label label-info">Corporate Office (CO)</span>
						@elseif($record->penugasanaudit->rencanadetail->tipe_object == 2)
							<span class="label label-default">Project</span>
						@else 
							<span class="label label-primary">Anak Perusahaan</span>
						@endif
					</td>
				</tr>
				<tr>
					<td style="">Objek Audit</td>
					<td style="">:</td>
					<td style="">
						{!! object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id)!!}
					</td>
				</tr>
				<tr>
					<td style="">Nama Proyek</td>
					<td style="">:</td>
					<td style="">
						@if($record->penugasanaudit->rencanadetail->nama)
							{{ $record->penugasanaudit->rencanadetail->nama }}
						@else 
							-
						@endif
					</td>
				</tr>
				<tr>
					<td style="">Rencana</td>
					<td style="">:</td>
					<td style="">
						{{ BulanWithYear($record->penugasanaudit->rencanadetail->rencana) }}
					</td>
				</tr>
				<tr>
					<td style="">Nilai Kontrak</td>
					<td style="">:</td>
					<td style="">{{ number_format($record->penugasanaudit->rencanadetail->nilai, 0, '.', '.') }}</td>
				</tr>
				<tr>
					<td style="">Progress (Ra)</td>
					<td style="">:</td>
					<td style="">{{ number_format($record->penugasanaudit->rencanadetail->progress_ra, 0, '.', '.') }}</td>
				</tr>
				<tr>
					<td style="">Progress (Ri)</td>
					<td style="">:</td>
					<td style="">{{ number_format($record->penugasanaudit->rencanadetail->progress_ri, 0, '.', '.') }}</td>
				</tr>
				<tr>
					<td style="">Deviasi Progress</td>
					<td style="">:</td>
					<td style="">{{ number_format($record->penugasanaudit->rencanadetail->deviasi, 0, '.', '.') }}</td>
				</tr>
				<tr>
					<td style="">Progress Risk Impact</td>
					<td style="">:</td>
					<td style="">
						@if($record->penugasanaudit->rencanadetail->risk_impact == 0)
							<span class="label label-default">Sangat Ringan</span>
						@elseif($record->penugasanaudit->rencanadetail->risk_impact == 1)
							<span class="label label-success">Ringan</span>
						@elseif($record->penugasanaudit->rencanadetail->risk_impact == 2)
							<span class="label label-info">Sedang</span>
						@elseif($record->penugasanaudit->rencanadetail->risk_impact == 3)
							<span class="label label-warning">Berat</span>
						@else 
							<span class="label label-danger">Sangat Berat</span>
						@endif
					</td>
				</tr>
				<tr>
					<td style="">MAPP</td>
					<td style="">:</td>
					<td style="">{{ number_format($record->penugasanaudit->rencanadetail->mapp, 0, '.', '.') }}</td>
				</tr>
				<tr>
					<td style="">BK / PU</td>
					<td style="">:</td>
					<td style="">{{ number_format($record->penugasanaudit->rencanadetail->bkpu, 0, '.', '.') }}</td>
				</tr>
				<tr>
					<td style="">Deviasi BK / PU</td>
					<td style="">:</td>
					<td style="">{{ number_format($record->penugasanaudit->rencanadetail->deviasi_bkpu, 0, '.', '.') }}</td>
				</tr>
				<tr>
					<td style="">BK / PU Risk Impact</td>
					<td style="">:</td>
					<td style="">
						@if($record->penugasanaudit->rencanadetail->bkpu_ri == 0)
							<span class="label label-default">Sangat Ringan</span>
						@elseif($record->penugasanaudit->rencanadetail->bkpu_ri == 1)
							<span class="label label-success">Ringan</span>
						@elseif($record->penugasanaudit->rencanadetail->bkpu_ri == 2)
							<span class="label label-info">Sedang</span>
						@elseif($record->penugasanaudit->rencanadetail->bkpu_ri == 3)
							<span class="label label-warning">Berat</span>
						@else 
							<span class="label label-danger">Sangat Berat</span>
						@endif
					</td>
				</tr>
				<tr style="border-bottom: 1px solid rgba(34,36,38,.1);">
					<td style="">Keterangan</td>
					<td style="">:</td>
					<td style="">
						@if($record->penugasanaudit->rencanadetail->keterangan == 0)
							<span class="label label-default">Tidak Dipilih</span>
						@else 
							<span class="label label-info">Dipilih</span>
						@endif
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="panel-heading" style="font-weight: bold;font-size: 13px;">Penugasan Audit</div>
	<div class="panel-body">
		<table class="table" style="font-size: 12px;">
			<tbody>
				<tr>
					<td style="width:180px;">No Surat</td>
					<td style="width:2px;">:</td>
					<td style="">
						{{ $record->penugasanaudit->no_surat }}
					</td>
				</tr>
				<tr>
					<td style="">Surat Penugasan</td>
					<td style="">:</td>
					<td style="">
						<input id="surat" name="other[]" multiple type="file" readonly class="form-control" 
	                        data-show-upload="true" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf,image/x-png,image/gif,image/jpeg">
					</div>
					</td>
				</tr>
				<tr>
					<td style="">Final Surat Penugasan</td>
					<td style="">:</td>
					<td style="">
						<input id="final" name="other[]" multiple type="file" readonly class="form-control" 
	                        data-show-upload="true" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf,image/x-png,image/gif,image/jpeg">
					</div>
					</td>
				</tr>
				<tr style="border-bottom: 1px solid rgba(34,36,38,.1);">
					<td style="">Realisasi</td>
					<td style="">:</td>
					<td style="">
						@if($record->penugasanaudit->realisasi)
							{{ BulanWithYear($record->penugasanaudit->realisasi) }}
						@else 
							{{ BulanWithYear($record->penugasanaudit->rencanadetail->rencana) }}
						@endif
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="panel-heading" style="font-weight: bold;font-size: 13px;">Tinjauan Dokumen</div>
	<div class="panel-body">
		<table class="table" style="font-size: 12px;">
			<tbody>
				<tr>
					<td style="width:180px;">Nomor</td>
					<td style="width:2px;">:</td>
					<td style="">
						{{ $record->nomor }}
					</td>
				</tr>
				<tr>
					<td style="width:180px;">Tempat</td>
					<td style="width:2px;">:</td>
					<td style="">
						{{ $record->tempat }}
					</td>
				</tr>
				<tr>
					<td style="width:180px;">Tanggal</td>
					<td style="width:2px;">:</td>
					<td style="">
						{{ $record->tanggal }}
					</td>
				</tr>
				<tr>
					<td style="width:180px;">Judul</td>
					<td style="width:2px;">:</td>
					<td style="">
						{{ $record->judul }}
					</td>
				</tr>
				<tr>
					<td style="width:180px;">Dateline</td>
					<td style="width:2px;">:</td>
					<td style="">
						{{ $record->dateline }}
					</td>
				</tr>
				<tr>
					<td style="">Dokumen</td>
					<td style="">:</td>
					<td style="">
						@if($record->status > 3)
							@if($record->cariFile() > 0)
								<input id="other" name="other[]" multiple type="file" readonly class="form-control" 
			                        data-show-upload="true" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf,image/x-png,image/gif,image/jpeg">
			                @else 
			                	-a
			                @endif
						@else 
						-a
						@endif
					</div>
					</td>
				</tr>
				<tr>
					<td style="">PIC</td>
					<td style="">:</td>
					<td style="">
						@php 
							$resultstr = array();
						@endphp
						@foreach($record->detailpenerima as $kiy => $pic)
							@php
							$resultstr[] = $pic->user->name;
							@endphp
							{{ implode(", ",$resultstr) }}
						@endforeach
					</td>
				</tr>
				<tr style="border-bottom: 1px solid rgba(34,36,38,.1);">
					<td style="">Tembusan</td>
					<td style="">:</td>
					<td style="">
						@php 
							$result = array();
						@endphp
						@foreach($record->detailtembusan as $kiy => $tembusan)
							@php
							$result[] = $tembusan->tembusan;
							@endphp
							{{ implode(", ",$result) }}
						@endforeach
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
        .input-group.file-caption-main{
            display: none;
        }
    </style>
@endpush

@push('scripts')
    <script> 
    	$('[data-toggle="tooltip"]').tooltip();
    	var modal = '#mediumModal';
    	$(document).on('click', '.draft.penugasan', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route('kegiatan-audit.persiapan.tinjauan-dokumen.index') !!}/" + idx + '/draftSurat';
            loadModalPreview({
                url: url,
                modal: modal,
            }, function(resp){
                $('#largeModal').find('.loading.dimmer').hide();
                onShow();
            });
        });

        $(document).on('click', '.draft-final.penugasan', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route('kegiatan-audit.persiapan.tinjauan-dokumen.index') !!}/" + idx + '/draftFinal';
            loadModalPreview({
                url: url,
                modal: modal,
            }, function(resp){
                $('#largeModal').find('.loading.dimmer').hide();
                onShow();
            });
        });

        $(document).on('click', '.dokumen-penugasan', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route('kegiatan-audit.persiapan.tinjauan-dokumen.index') !!}/" + idx + '/dokumenPenugasan';
            loadModalPreview({
                url: url,
                modal: modal,
            }, function(resp){
                $('#largeModal').find('.loading.dimmer').hide();
                onShow();
            });
        });

    	$('#myTab li:first-child a').tab('show')
    	$('.btn').tooltip('enable');
        $('.tanggal').datepicker({
            format: 'mm/dd/yyyy',
		    startDate: '-0d',
            orientation: "auto",
            autoclose:true,
        });
        $('.waktu').clockpicker({
            autoclose:true,
            'default': 'now',
        });

        $('.cari').on('change', function(){
        	var id = $(this).data('id');
        	var value = parseInt(this.value);
        	$.ajax({
				url: '{{ url('ajax/option/get-langkah') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					id: id,
					value: value,
				},
			})
			.done(function(response) {
				var rowz = $('tr.langkah-kerja-'+id).remove();
				var rowspan = response[1].length;
				$('.for_rowspan_'+id).attr('rowspan', parseInt((rowspan+3)));
				$.each(response[1], function(key, value){
					if(key==0){
						$(value).insertAfter('.langkah_kerja_'+id);
					}else{
						$(value).insertAfter('.data_detail-'+id+'-'+(key-1));
					}
				});
			})
		});
        function romawi(number){
        	var map = [1,4,5,9,10,40,50,90,100,400,500,900,1000];  
        	var roma = ["I","IV","V","IX","X","XL","L","XC","C","CD","D","CM","M"];  
        	var hasil='';  

        	for(var i=12; i >=0; i--) {  
        		while(number >= map[i]) {  
        			number = number - map[i];  
        			hasil = hasil + roma[i];  
        		}  
        	}  
		    return hasil;
        }

        function loadModalPreview(param, callback) 
		{
			var url    = (typeof param['url'] === 'undefined') ? '#' : param['url'];
			var modal  = '#largeModal';
			var formId = (typeof param['formId'] === 'undefined') ? 'formData' : param['formId'];
			var onShow = (typeof callback === 'undefined') ? function(){} : callback;
			var modals = $(modal);
			modals.find('.modal-content').html(`
                <div class="loading dimmer" style="height: auto; top: calc(50% - 100px)">
                    <div class="loader"></div>
                </div>
			`);

			modals.modal('show');
			
			modals.off('shown.bs.modal');
			modals.on('shown.bs.modal', function(event) {
				modals.find('.modal-content').load(url, callback);
			});
		}

		var fileSurat = '{{ $record->penugasanaudit->fileSurat() }}';
		var fileFinal = '{{ $record->penugasanaudit->fileFinal() }}';
		var fileFinalName = '{{ $record->penugasanaudit->fileFinalName() }}';
		$("#surat").fileinput({
	        autoReplace: true,
	        overwriteInitial: true,
	        maxFileCount: 1,
	        initialPreview: [fileSurat],
	        initialPreviewAsData: true,
	        initialPreviewConfig: [
	            {
	            	type: "pdf",
	            	caption: "{{ $record->penugasanaudit->filename }}", 
	            	downloadUrl: fileSurat, 
	            	size: 930321, 
	            	width: "120px", 
	            	key: 1
	        	},
	        ],
	        initialCaption: "{{ $record->penugasanaudit->filename }}",
	        initialPreviewShowDelete: false,
	        showRemove: false,
	        showClose: false,
	        layoutTemplates: {actionDelete: ''}, // disable thumbnail deletion
	        allowedFileExtensions: ["pdf"]
	    });

	    $("#final").fileinput({
	        autoReplace: true,
	        overwriteInitial: true,
	        maxFileCount: 1,
	        initialPreview: [fileFinal],
	        initialPreviewAsData: true,
	        initialPreviewConfig: [
	            {
	            	type: "pdf",
	            	caption: fileFinalName, 
	            	downloadUrl: fileFinal, 
	            	size: 930321, 
	            	width: "120px", 
	            	key: 1
	        	},
	        ],
	        initialCaption: fileFinalName,
	        initialPreviewShowDelete: false,
	        showRemove: false,
	        showClose: false,
	        layoutTemplates: {actionDelete: ''}, // disable thumbnail deletion
	        allowedFileExtensions: ["pdf"]
	    });

		var obj = '{{ $record->fileOtherJson() }}';
        var fileUri = '{{ $record->fileOther() }}';
		$('#other').fileinput({
			uploadUrl: "/file-upload-batch/1",
            autoReplace: true,
            overwriteInitial: true,
            // maxFileCount: 1,
            initialPreview: JSON.parse(fileUri.replace(/&quot;/g,'"')),
            initialPreviewAsData: true,
            initialPreviewConfig: JSON.parse(obj.replace(/&quot;/g,'"')),
            // initialCaption: "{{ $record->files()->pluck('filename')->first() }}",
            initialPreviewShowDelete: false,
            showRemove: false,
            showClose: false,
            layoutTemplates: {actionDelete: ''}, // disable thumbnail deletion
        });
        $('.kv-file-zoom').prop('disabled', false);
    </script>
    @yield('js-extra')
@endpush

