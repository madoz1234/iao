@extends('layouts.form')
@section('title', 'Ubah Surat Permintaan Dokumen')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
    <div class="panel-body" style="padding-bottom: 0px;">
		<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
			@method('PATCH')
		    @csrf
		    <div class="loading dimmer padder-v" style="display: none;">
			    <div class="loader"></div>
			</div>
			<input type="hidden" name="status" value="0">
			<input type="hidden" name="id" value="{{ $record->id }}">
			<input type="hidden" name="penugasan_id" value="{{ $record->penugasan_id }}">
			<input type="hidden" name="tahun" value="{{ $record->penugasanaudit->rencanadetail->rencanaaudit->tahun }}">
			@if($record->ket_svp)
				<p style="padding-left: 14px;color:red;">Status <b>Ditolak SVP</b> pada {{DateToStringWday($record->updated_at)}}</p>
				<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
					<tr>
						<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
							<h5 style="font-weight: bold;font-size: 12px;">Keterangan :</h5>
							<p style="text-align: justify;text-justify: inter-word;">{{ $record->ket_svp }}</p>
						</td>
					</tr>
				</table>
			@endif
			@csrf
			<table class="table" style="font-size: 12px;">
				<tbody>
					<tr>
						<td style="width:200px;border: 1px #ffffff;border: 1px #ffffff;">Tahun</td>
						<td style="width:2px;border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;" class="field">
							@php 
								$tahun=array();
							@endphp
							@foreach($records as $thn)
								@php 
									$tahun[]= $thn->rencanadetail->rencanaaudit->tahun;
								@endphp
							@endforeach
							{{ implode(", ", array_unique($tahun))}}
							<input type="hidden" name="tahun" value="{{ min(array_unique($tahun))}}">
						</td>
					</tr>
					<tr>
						<td style="border: 1px #ffffff;">Rencana Pelaksanaan</td>
						<td style="border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;" class="field">
							@php 
								$rencana=array();
							@endphp
							@foreach($records as $rnc)
								@php 
									$rencana[]= $rnc->rencanadetail->rencana;
								@endphp
							@endforeach
							{{ implode(", ", array_unique($rencana))}}
						</td>
					</tr>
					<tr>
						<td style="border: 1px #ffffff;">Tipe</td>
						<td style="border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;" class="field">
							@php 
								$tipe=array();
							@endphp
							@foreach($records as $tip)
								@php 
								if($tip->rencanadetail->tipe == 0){
									$tipe[]= '<span class="label label-primary">Audit</span>';
								}elseif($tip->rencanadetail->tipe == 1){
									$tipe[]= '<span class="label label-success">Kegiatan Konsultasi</span>';
								}else{
									$tipe[]= '<span class="label label-info">Kegiatan Audit Lain - Lain</span>';
								}
								@endphp
							@endforeach
							{!! implode(" - ", array_unique($tipe))!!}
						</td>
					</tr>
					<tr>
						<td style="border: 1px #ffffff;">Kategori</td>
						<td style="border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;" class="field">
							@php 
								$kategori=array();
							@endphp
							@foreach($records as $kat)
								@php
								if($kat->rencanadetail->tipe == 0) {
									if($kat->rencanadetail->tipe_object == 0){
										$kategori[]= '<span class="label label-success">Business Unit (BU)</span>';
									}elseif($kat->rencanadetail->tipe_object == 1){
										$kategori[]= '<span class="label label-info">Corporate Office (CO)</span>';
									}elseif($kat->rencanadetail->tipe_object == 2){
										$kategori[]= '<span class="label label-default">Project</span>';
									}else{
										$kategori[]= '<span class="label label-primary">Anak Perusahaan</span>';
									}
								}elseif($kat->rencanadetail->tipe == 1) {
									$kategori[]= '<span class="label label-warning">'.$kat->rencanadetail->konsultasi->nama.'</span>';
								}else{
									$kategori[]= '<span class="label label-default">'.$kat->rencanadetail->lain->nama.'</span>';
								}
								@endphp
							@endforeach
							{!! implode(" - ", array_unique($kategori))!!}
						</td>
					</tr>
					<tr class="detil">
						<td style="border: 1px #ffffff;">Objek Audit</td>
						<td style="border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;">
							@php 
								$object=array();
							@endphp
							@foreach($records as $obj)
								@php 
									$object[]= object_audit($obj->rencanadetail->tipe_object, $obj->rencanadetail->object_id);
								@endphp
							@endforeach
							<table style="font-size: 12px;">
								<tbody>
									@php
										$i =1;
									@endphp
									@foreach(array_unique($object) as $koy => $ob)
										@if($ob)
											<tr>
												<td style="width: 15px;">{{ $i }}.</td>
												<td style="">{{ $ob }}</td>
											</tr>
										@endif
										@php
											$i++;
										@endphp
									@endforeach
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
			<input type="hidden" name="surat_penugasan_id[]" value="{{ $record->id }}">
			<table id="example" class="table table-bordered m-t-none" style="border: 1px #ffffff; font-size: 12px;">
				<tbody class="exe">
					<tr>
						<td style="width: 200px;border: 1px #ffffff;">Nomor</td>
						<td style="width: 20px;border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;text-align: left;" colspan="2" class="field">
							<input type="text" name="nomor" class="form-control" placeholder="Nomor" value="{{$record->nomor}}">
						</td>
					</tr>
					<tr>
						<td style="width: 200px;border: 1px #ffffff;">Tempat / Tanggal</td>
						<td style="width: 20px;border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;text-align: left;" class="field"><input type="text" name="tempat" class="form-control" placeholder="Tempat" value="{{$record->tempat}}"></td>
						<td style="border: 1px #ffffff;text-align: left;font-size: 12px;" class="field"><input type="text" name="tanggal" class="form-control tanggal" placeholder="Tanggal" value="{{$record->tanggal}}"></td>
					</tr>
					<tr>
						<td style="width: 200px;border: 1px #ffffff;">Judul Dokumen</td>
						<td style="width: 20px;border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;text-align: left;" colspan="2" class="field"><input type="text" name="judul" class="form-control" placeholder="Judul Dokumen" value="{{$record->judul}}"></td>
					</tr>
					<tr>
						<td style="width: 200px;border: 1px #ffffff;">Email</td>
						<td style="width: 20px;border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;text-align: left;" colspan="2" class="field">
							<input type="text" name="email" class="form-control" placeholder="Email" value="{{$record->email}}">
						</td>
					</tr>
					<tr>
						<td style="width: 200px;border: 1px #ffffff;">Deadline</td>
						<td style="width: 20px;border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;text-align: left;" colspan="2" class="field"><input type="text" name="dateline" class="form-control tanggal" placeholder="Deadline" value="{{$record->dateline}}"></td>
					</tr>
					<tr>
						<td style="width: 200px;border: 1px #ffffff;">Kepada (internal)</td>
						<td style="width: 20px;border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;text-align: left;" colspan="2" class="field">
							<table id="penerima" class="table table-bordered m-t-none">
								<thead>
									<tr>
										<td style="font-size: 12px;text-align: center;width: 5%;">
											No
										</td>
										<td style="font-size: 12px;text-align: center;">
											Penerima
										</td>
										<td style="font-size: 12px;text-align: center;width: 10%;">
											<button class="btn btn-sm btn-success tambah_penerima" type="button" style="border-radius:20px;"><i class="fa fa-plus"></i></button>
										</td>
									</tr>
								</thead>
								<tbody class="pennerima">
									@if(count($record->detailpenerima) > 0)
										@foreach($record->detailpenerima as $key => $userz)
											<tr class="pen penerima penerima-{{ $key+1 }}" data-id="{{ $key+1 }}">
												<td style="text-align: center;font-size: 12px;" class="numbuur-{{ $key+1 }}">
													{{ $key+1 }}
												</td>
												<td style="text-align: left;" class="field">
													<select class="selectpicker form-control" name="data_user[{{ $key+1 }}][users]" data-style="btn-default" data-size="5" data-live-search="true" title="(Pilih Salah Satu)">
														@foreach(App\Models\Auths\User::where('id', '<>', 1)->get() as $user)
															<option value="{{ $user->id }}" @if($userz->user_id == $user->id) selected @endif>{{ $user->name }}</option>
														@endforeach
													</select> 
												</td>
												<td style="width: 80px;">
														<button class="btn btn-sm btn-danger hapus_penerima" data-id="{{ $key+1 }}" type="button" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
												</td>
											</tr>
										@endforeach
										<input type="hidden" name="last_penerima" value="{{ $key+1 }}">
									@else 
										<tr class="pen penerima penerima-0" data-id="0">
											<td style="text-align: center;font-size: 12px;" class="numbuur-0">
												1
											</td>
											<td style="text-align: left;" class="field">
												<select class="selectpicker form-control" name="data_user[0][users]" data-style="btn-default" data-size="5" data-live-search="true" title="(Pilih Salah Satu)">
													@foreach(App\Models\Auths\User::where('id', '<>', 1)->get() as $user)
														<option value="{{ $user->id }}" selected>{{ $user->name }}</option>
													@endforeach
												</select> 
											</td>
											<td style="width: 80px;">
													<button class="btn btn-sm btn-danger hapus_penerima" data-id="0" type="button" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
											</td>
										</tr>
										<input type="hidden" name="last_penerima" value="0">
									@endif
								</tbody>
							</table>
						</td>
						<td style="width: 80px;border: 1px #ffffff;">
						</td>
					</tr>
					@if(count($record->detailemail) > 0)
						@foreach($record->detailemail as $kiy => $emails)
							@if($kiy == 0)
								<tr class="data_email email-{{$kiy}}">
									<td style="width: 200px;border: 1px #ffffff;">Kepada (email lain)</td>
									<td style="width: 20px;border: 1px #ffffff;" class="numbors-{{$kiy}}">{{ $kiy+1 }}</td>
									<td style="border: 1px #ffffff;text-align: left;" colspan="2" class="field">
										<input type="text" name="data_email[{{$kiy}}][email]" class="form-control" placeholder="Email" value="{{ $emails->email }}">
									</td>
									<td style="width: 80px;border: 1px #ffffff;"><button class="btn btn-sm btn-success tambah_email" type="button" style="border-radius: 20px;margin-top: 2px;"><i class="fa fa-plus"></i></button></td>
								</tr>
							@else 
								<tr class="data_email email email-{{$kiy}}" data-id="{{$kiy}}">
									<td style="width: 200px;border: 1px #ffffff;"></td>
									<td style="width: 20px;border: 1px #ffffff;" class="numbors-{{$kiy}}">{{ $kiy+1 }}</td>
									<td style="border: 1px #ffffff;text-align: left;" colspan="2" class="field">
										<input type="text" name="data_email[{{$kiy}}][email]" class="form-control" placeholder="Email" value="{{ $emails->email }}">
									</td>
									<td style="text-align:center;border: 1px #ffffff;">
										<button class="btn btn-sm btn-danger hapus_email" type="button" style="border-radius:20px;" data-id={{$kiy}}><i class="fa fa-remove"></i></button>
									</td>
							    </tr>
							@endif
						@endforeach
						<input type="hidden" name="last_email" value="{{$kiy}}">
					@else 
						<tr class="data_email email-0">
							<td style="width: 200px;border: 1px #ffffff;">Kepada (email lain)</td>
							<td style="width: 20px;border: 1px #ffffff;" class="numbors-0">1</td>
							<td style="border: 1px #ffffff;text-align: left;" colspan="2" class="field">
								<input type="text" name="data_email[0][email]" class="form-control" placeholder="Email">
							</td>
							<td style="width: 80px;border: 1px #ffffff;"><button class="btn btn-sm btn-success tambah_email" type="button" style="border-radius: 20px;margin-top: 2px;"><i class="fa fa-plus"></i></button></td>
						</tr>
						<input type="hidden" name="last_email" value="0">
					@endif
				</tbody>
			</table>
			<table id="tembusan" class="table table-bordered m-t-none" style="font-size: 12px;">
				<thead>
					<tr>
						<td style="text-align: center;width: 3.5%;">No</td>
						<td style="text-align: center;">Tembusan</td>
						<td style="text-align: center;width: 5%;">
							<button class="btn btn-sm btn-success tambah_tembusan" type="button" style="border-radius:20px;"><i class="fa fa-plus"></i></button>
						</td>
					</tr>
				</thead>
				<tbody class="tembusan">
					@if(count($record->detailtembusan) > 0)
						@foreach($record->detailtembusan as $kiy => $tembusanz)
							<tr class="tem data-tembusan-{{$kiy}}" data-id="{{$kiy}}">
								<td style="width: 20px;">
									<label style="margin-top: 7px;padding-left: 5px;" class="num-{{$kiy}}">{{$kiy+1}}</label>
								</td>
								<td style="border-bottom: 1px solid rgba(34,36,38,.1);" class="field">
									<select class="selectpicker form-control field" name="data_tembusan[{{$kiy}}][tembusan]" data-style="btn-default" data-size="5" data-live-search="true" title="(Pilih Salah Satu)">
										@foreach(App\Models\Auths\User::where('id', '<>', 1)->get() as $user)
											<option value="{{ $user->id }}" @if($user->id == $tembusanz->user_id) selected @endif>{{ $user->name }}</option>
										@endforeach
									</select>
								</td>
								<td style="width: 80px;border-bottom: 1px solid rgba(34,36,38,.1);">
									<button class="btn btn-sm btn-danger hapus_tembusanz" data-id="{{$kiy}}" type="button" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
								</td>
							</tr>
						@endforeach
						<input type="hidden" name="last_tembusan" value="{{ $kiy+2 }}">
					@else 
						<tr class="tem data-tembusan-0" data-id=0>
							<td style="width: 20px;">
								<label style="margin-top: 7px;padding-left: 5px;" class="num-0">1</label>
							</td>
							<td style="border-bottom: 1px solid rgba(34,36,38,.1);" class="field">
								<select class="selectpicker form-control" name="data_tembusan[0][tembusan]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)" data-size="5">
										@foreach(App\Models\Auths\User::where('id', '<>', 1)->get() as $user)
											<option value="{{ $user->id }}">{{ $user->name }}</option>
										@endforeach
								</select>
							</td>
							<td style="text-align:center;border-bottom: 1px solid rgba(34,36,38,.1);">
								<button class="btn btn-sm btn-danger hapus_tembusan" type="button" style="border-radius:20px;" data-id=0><i class="fa fa-remove"></i></button>
							</td>
						</tr>
						<input type="hidden" name="last_tembusan" value="0">
					@endif
				</tbody>
			</table>
			<table id="dokumen" class="table table-bordered m-t-none" style="font-size: 12px;">
				<thead>
					<tr>
						<td style="text-align: left;" colspan="4">Dokumen Pendahuluan</td>
					</tr>
				</thead>
				<tbody class="container">
					@if(count($record->detaildokumen) > 0)
						@foreach($record->detaildokumen as $kay => $dok)
							<tr class="data-container-{{$kay}}" data-id="{{$kay}}">
								<td style="width: 20px;">
									<label style="margin-top: 7px;padding-left: 5px;" class="numbur-{{$kay}}">{{ $kay+1 }}</label>
								</td>
								<td style="border-bottom: 1px solid rgba(34,36,38,.1);" class="field">
									<select data-id="{{$kay}}" class="selectpicker form-control cari" name="detail[{{$kay}}][dokumen]" data-style="btn-default" data-live-search="true" data-size="3" title="(Pilih Salah Satu)">
										@foreach(App\Models\Master\Dokumen::all() as $dokumen)
											<option value="{{ $dokumen->id }}" class="cuy" @if($dokumen->id == $dok->dokumen_id) selected @endif>{{ $dokumen->judul }}</option>
										@endforeach
									</select>  
								</td>
								<td style="width: 100px;border-bottom: 1px solid rgba(34,36,38,.1);">
									<input type="checkbox" class="view check" name="detail[{{$kay}}][status]" data-toggle="toggle" data-size="mini" data-on="Penting" data-off="Tidak Penting" data-width="100" data-style="ios" @if($dok->status == 1) checked @endif value="{{$dok->status}}">
								</td>
								<td style="width: 80px;border-bottom: 1px solid rgba(34,36,38,.1);">
									@if($kay== 0)
										<button class="btn btn-sm btn-success tambah_dokumen" type="button" style="border-radius: 20px;margin-top: 2px;"><i class="fa fa-plus"></i></button>
									@else 
										<button class="btn btn-sm btn-danger hapus_dokumen" type="button" style="border-radius:20px;" data-id={{$kay}}><i class="fa fa-remove"></i></button>
									@endif
								</td>
							</tr>
						@endforeach
					@else 
						<tr class="data-container-0" data-id="0">
							<td style="width: 20px;">
								<label style="margin-top: 7px;padding-left: 5px;" class="numbur-0">1</label>
							</td>
							<td style="border-bottom: 1px solid rgba(34,36,38,.1);" class="field">
								<select data-id="0" class="selectpicker form-control cari" name="detail[0][dokumen]" data-style="btn-default" data-live-search="true" data-size="3" title="(Pilih Salah Satu)">
									@foreach(App\Models\Master\Dokumen::all() as $dokumen)
										<option value="{{ $dokumen->id }}" class="cuy">{{ $dokumen->judul }}</option>
									@endforeach
								</select>  
							</td>
							<td style="width: 100px;border-bottom: 1px solid rgba(34,36,38,.1);">
								<input type="checkbox" class="view check" name="detail[0][status]" data-toggle="toggle" data-size="mini" data-on="Penting" data-off="Tidak Penting" data-width="100" data-style="ios">
							</td>
							<td style="width: 80px;border-bottom: 1px solid rgba(34,36,38,.1);"><button class="btn btn-sm btn-success tambah_dokumen" type="button" style="border-radius: 20px;margin-top: 2px;"><i class="fa fa-plus"></i></button></td>
						</tr>
					@endif
				</tbody>
			</table>
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-right">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
						<button type="button" class="btn btn-primary save as drafting">Save As Draft</button>
						<button type="button" class="btn btn-simpan save as page">Submit</button>
					</div>
				</div>
			</div>
		</form>
    </div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script> 
    	var d = new Date();
    	var full = d.getFullYear();
    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
    	var last = new Date(lastDate);
    	if($('input[name=tahun]').val() > full){
    		var min = minDates;
    	}else{
    		var min = d;
    	}

		$('.tanggal').datepicker({
    		orientation: "bottom left",
			todayHighlight: true,
			autoclose:true,
    		format: 'dd-mm-yyyy',
    		yearRange: '-0:+1',
    		startDate: minDates,
    		endDate: last,
			hideIfNoPrevNext: true,
    		defaultViewDate: {
            	year: $('input[name=tahun]').val(),
            }
    	});
    	$(document).on('click', '.tambah_dokumen', function(e){
			var rowCount = $('#dokumen > tbody > tr').length;
			var c = rowCount-1;
			var html = `
					<tr class="data-container-`+(c+2)+`" data-id=`+(c+2)+`>
						<td style="width: 20px;">
							<label style="margin-top: 7px;padding-left: 5px;" class="numboor-`+(c+2)+`">`+(c+2)+`</label>
						</td>
						<td style="border-bottom: 1px solid rgba(34,36,38,.1);" class="field">
							<select data-id="`+(c+2)+`" class="selectpicker form-control cari" name="detail[`+(c+2)+`][dokumen]" data-style="btn-default" data-live-search="true" data-size="3" title="(Pilih Salah Satu)">
								@foreach(App\Models\Master\Dokumen::all() as $dokumen)
									<option value="{{ $dokumen->id }}" class="cuy">{{ $dokumen->judul }}</option>
								@endforeach
							</select>  
						</td>
						<td style="width: 80px;border-bottom: 1px solid rgba(34,36,38,.1);">
							<input type="checkbox" class="view check" name="detail[`+(c+2)+`][status]" data-width="100" data-toggle="toggle" data-size="mini" data-on="Penting" data-off="Tidak Penting" data-style="ios">
						</td>
						<td style="text-align:center;border-bottom: 1px solid rgba(34,36,38,.1);">
							<button class="btn btn-sm btn-danger hapus_dokumen" type="button" style="border-radius:20px;" data-id=`+(c+2)+`><i class="fa fa-remove"></i></button>
						</td>
					</tr>
				`;

				$('.container').append(html).children(':last').hide().fadeIn(1000);
		        $('.selectpicker').selectpicker();
		        $("[data-toggle='toggle']").bootstrapToggle('destroy')                 
			    $("[data-toggle='toggle']").bootstrapToggle();

				var list = $(".cari").map(function(){return $(this).val();}).get();
				var newarr=[];
				for(var i=0; i<(list.length);i++){

				   if(list[i] !== "" && list[i] !== null){
				    newarr.push(list[i]);
				   }
				}
				console.log(newarr)
				$(newarr).each(function(key, vil){
	    	// 			$('select[name="detail['+data_id+'][dokumen]"]').selectpicker("refresh");
						// $('select[name="detail['+data_id+'][dokumen]"]').children('option[value="'+vil+'"]').hide();
						// $('select[name="detail['+data_id+'][dokumen]"]').selectpicker("refresh");
				});
		});

		$(document).on('click', '.tambah_email', function(e){
			var last = parseInt($('input[name=last_email]').val()) + 1;
			var rowz = $('#example > tbody > tr.data_email').length;
			var html = `
					    <tr class="data_email email email-`+last+`" data-id="`+last+`">
							<td style="width: 200px;border: 1px #ffffff;"></td>
							<td style="width: 20px;border: 1px #ffffff;" class="numbors-`+last+`">`+(rowz+1)+`</td>
							<td style="border: 1px #ffffff;text-align: left;" colspan="2" class="field">
								<input type="text" name="data_email[`+last+`][email]" class="form-control" placeholder="Email">
							</td>
							<td style="text-align:center;border: 1px #ffffff;">
								<button class="btn btn-sm btn-danger hapus_email" type="button" style="border-radius:20px;" data-id=`+last+`><i class="fa fa-remove"></i></button>
							</td>
					    </tr>
				`;
				$('.exe').append(html).children(':last').hide().fadeIn(1000);
				$('input[name=last_email]').val(last)
		});

		$(document).on('click', '.hapus_email', function (e){
			var row = $(this).closest('tr');
			row.remove();

			var table = $('#example');
			var rows = table.find('tbody tr.data_email');
			$.each(rows, function(key, value){
				console.log($(this).data("id"))
				table.find('.numbors-'+$(this).data("id")).html(key+1);
			});
		});

		$(document).on('click', '.tambah_penerima', function(e){
			var rowz = $('#penerima > tbody > tr.pen').length;
			var last = parseInt($('input[name=last_penerima]').val()) + 1;
			var html = `
					    <tr class="pen penerima penerima-`+last+`" data-id="`+last+`">
							<td style="text-align: center;font-size: 12px;" class="numbuur-`+last+`">
								`+(rowz+1)+`
							</td>
							<td style="text-align: left;" class="field">
								<select class="selectpicker form-control" name="data_user[`+last+`][users]" data-style="btn-default" data-size="5" data-live-search="true" title="(Pilih Salah Satu)">
									@foreach(App\Models\Auths\User::where('id', '<>', 1)->get() as $user)
										<option value="{{ $user->id }}">{{ $user->name }}</option>
									@endforeach
								</select> 
							</td>
							<td style="width: 80px;">
									<button class="btn btn-sm btn-danger hapus_penerimas" data-id="`+last+`" type="button" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
							</td>
						</tr>
				`;
			$('.pennerima').append(html).children(':last').hide().fadeIn(1000);
			$('.selectpicker').selectpicker();
			$('input[name=last_penerima]').val(last);
		});

		$(document).on('click', '.hapus_penerima', function (e){
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#penerima');
			var rows = table.find('tbody tr');

			$.each(rows, function(key, value){
				console.log($(this).data("id"))
				table.find('.numbuur-'+$(this).data("id")).html(key+1);
			});
		});

		$(document).on('click', '.hapus_penerimas', function (e){
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#example');
			var rows = table.find('tbody tr.pen');
			$.each(rows, function(key, value){
				console.log($(this).data("id"))
				table.find('.numbuur-'+$(this).data("id")).html(key+1);
			});
		});

		function isEmpty(value) {
		  return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
		}

		$(document).on('click', '.hapus_dokumen', function (e){
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#dokumen');
			var rows = table.find('tbody tr');

			$.each(rows, function(key, value){
				console.log($(this).data("id"))
				table.find('.numbuur-'+$(this).data("id")).html(key+1);
			});
		});

		$(document).on('click', '.tambah_tembusan', function(e){
			var rowCount = $('#tembusan > tbody > tr').length;
			var rowz = $('#tembusan > tbody > tr.tem').length;
			var last = parseInt($('input[name=last_tembusan]').val()) + 1;
			var c = rowCount-1;
			var html = `
					<tr class="tem data-container-`+last+`" data-id=`+last+`>
						<td style="width: 20px;">
							<label style="margin-top: 7px;padding-left: 5px;" class="num-`+last+`">`+(rowz+1)+`</label>
						</td>
						<td style="border-bottom: 1px solid rgba(34,36,38,.1);" class="field">
							<select class="selectpicker form-control" name="data_tembusan[`+last+`][tembusan]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)" data-size="5">
									@foreach(App\Models\Auths\User::where('id', '<>', 1)->get() as $user)
										<option value="{{ $user->id }}">{{ $user->name }}</option>
									@endforeach
							</select>
						</td>
						<td style="text-align:center;border-bottom: 1px solid rgba(34,36,38,.1);">
							<button class="btn btn-sm btn-danger hapus_tembusan" type="button" style="border-radius:20px;" data-id=`+last+`><i class="fa fa-remove"></i></button>
						</td>
					</tr>
				`;
				$('.tembusan').append(html).children(':last').hide().fadeIn(1000);
				$('input[name=last_tembusan]').val(last);
		        $('.selectpicker').selectpicker();
		});
		$(document).on('click', '.hapus_tembusan', function (e){
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#tembusan');
			var rows = table.find('tbody tr.tem');
			$.each(rows, function(key, value){
				console.log($(this).data("id"))
				table.find('.num-'+$(this).data("id")).html(key+1);
			});
		});

		$(document).on('click', '.hapus_tembusanz', function (e){
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#tembusan');
			var rows = table.find('tbody tr.tem');
			$.each(rows, function(key, value){
				console.log($(this).data("id"))
				table.find('.num-'+$(this).data("id")).html(key+1);
			});
		});
    </script>
    @yield('js-extra')
@endpush

