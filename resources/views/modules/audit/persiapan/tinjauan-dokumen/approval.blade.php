<form action="{{ route($routes.'.saveApproval', 1) }}" method="POST" id="formData">
	@method('PATCH')
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Approval</h5>
    </div>
    <div class="modal-body">
    	<input type="hidden" name="data" value="{{$record}}">
        <div class="form-group">
            <div class="form-group field">
            	<label class="control-label">Pilihan</label>
            	<div>
	        		<select class="selectpicker form-control cari" name="approval" data-style="btn-default" data-live-search="true">
						<option value="">( Pilih Salah Satu )</option>
						<option value="1">Approve</option>
						<option value="2">Reject</option>
					</select>
				</div>
            </div>           
        </div>
        <div class="form-group alasan" style="display: none;">
            <div class="form-group field">
            	<label class="control-label">Alasan Penolakan</label>
        		<textarea class="form-control" name="ket_svp" id="exampleFormControlTextarea1" placeholder="Alasan Penolakan" rows="3"></textarea>
            </div>           
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>
