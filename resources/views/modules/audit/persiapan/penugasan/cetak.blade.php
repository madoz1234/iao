<html>
<head>
<style>
		@font-face {
		    font-family: 'tahoma';
		    src: url({{ storage_path('fonts\tahoma.ttf') }}) format("truetype");
		    font-weight: 400;
		    font-style: normal;
		}

        @page {
            margin: 60px 25px;
            counter-reset: page;
        }

       /* Define now the real margins of every page in the PDF */
       body {
           margin-top: 3cm;
           margin-left: 1cm;
           margin-right: 1cm;
           margin-bottom: 2cm;
           font-style: normal;
       }

       .ui.table.bordered {
         border: solid black 1px;
         border-collapse: collapse;
         width: 100%;
         font-family : "Tahoma", "Verdana", sans-serif;
       }

       .ui.table.bordered td {
         border: solid black .5px;
         border-collapse: collapse;
         /*padding:10px;*/
         padding:5px;
       }

       .ui.table.bordered td.center.aligned {
         text-align : center;
       }

       .page-break {
            page-break-after: always;
       }

       .pad{
       	padding-top: 5px;
       	padding-bottom: 5px;
       }
        header {
            position: fixed;
            top: -40px;
            left: 36px;
            right: 36px;
            height: 10px;
            text-align: center;
            font-size: 11px;
        }
		
       main {
         position: sticky;
         font-size : 11px;
         padding-top : -1cm;
         margin-top : 2px;
         border : none;
         width: 100%;
       }

       main p {
         margin-left: 5px;
         margin-right: 5px;
       }
       footer {
           position: fixed;
           bottom: -60px;
           left: 0px;
           right: 0px;
           height: 50px;
           text-align: center;
           line-height: 35px;
       }
       table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;font-family : "Tahoma", "Verdana", sans-serif;}
       table.page_header {width: 100%; border: none; padding: 2mm; font-size: 11px;font-family : "Tahoma", "Verdana", sans-serif; text-align: left; margin-left: 50px;margin-top: 50px;}
       table.page_content_header {width: 100%; font-size: 11px;border-collapse: collapse;font-family : "Tahoma", "Verdana", sans-serif;text-align: center; padding-bottom: 1px;
       	padding-top: 1px;}
</style>
</head>
<body>
    <header>
    	<table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;">
    		<tbody>
    			<tr style="">
    				<td style="text-align: left;border: 1px #ffffff;" colspan="2">
    					<table style="">
    						<tbody>
    							<tr>
    								<td style="width: 100px;padding-left: -10px;"><img src="{{ asset('src/img/logo.png') }}" width="85px" height="70px"></td>
    								<td style="width: 390px;"><h2>PT. WASKITA KARYA <small>(Persero)</small> Tbk.</h2></td>
    								<td style="width: 170px">
    									{{-- <table style="text-align: right;font-size: 11px;" class="page_content ui table bordered">
    										<tbody>
    											<tr>
    												<td style="text-align: center;padding-bottom: 10px;">Form Prod 35</td>
    											</tr>
    											<tr>
    												<td style="text-align: center;padding-bottom: 10px;">Edisi : Mei 2019 Revisi : 0</td>
    											</tr>
    										</tbody>
    									</table> --}}
    								</td>
    							</tr>
    						</tbody>
    					</table>
    				</td>
    			</tr>
    			<tr style="">
    				<td style="border: 1px #ffffff;">Nomor : {{ $records->no_surat }}</td>
    			</tr>
    			<tr style="">
    				<td style="border: 1px #ffffff;">Tanggal : {{ DateToStringWday($records->tgl_surat) }}</td>
    			</tr>
    		</tbody>
		</table>
  	</header>
  	<br>
	<main>
		<table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;">
			<tbody>
				<tr style="">
					<td style="text-align: center;border: 1px #ffffff;font-weight: bold;font-size: 12px;">INTERNAL MEMO</td>
				</tr>
			</tbody>
		</table>
		<table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;">
			<tbody>
				<tr>
					<td style="text-align: left;border: 1px #ffffff;width: 10px;">Kepada : SVP Internal Audit</td>
				</tr>
				<tr>
					<td style="text-align: left;border: 1px #ffffff;width: 10px;padding-left: 44px;">PT. Waskita Karya (Persero) Tbk.</td>
				</tr>
			</tbody>
		</table>
		<table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;">
			<tbody>
				<tr>
					<td style="text-align: left;border: 1px #ffffff;width: 10px;">Perihal : Audit Internal pada :</td>
				</tr>
				@if($records->group > 0)
						@php 
							$j=1;
						@endphp
						@foreach(checkGroup($records->group) as $kay => $grop)
							 @if($grop->rencanadetail->tipe == 0)
								<tr>
									<td style="text-align: left;border: 1px #ffffff;width: 10px;padding-left: 41px;">
									 	{{$j}}. {{ object_audit($grop->rencanadetail->tipe_object, $grop->rencanadetail->object_id) }}
										 @php 
											$j++;
										 @endphp
									</td>
								</tr>
							 @endif
						@endforeach
				@else 
					<tr>
						<td style="text-align: left;border: 1px #ffffff;width: 10px;padding-left: 41px;">1. {{ object_audit($records->rencanadetail->tipe_object, $records->rencanadetail->object_id) }}</td>
					</tr>
				@endif
			</tbody>
		</table>
		<hr width="98%">
		<table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;">
			<tbody>
				<tr style="">
					@php
					   $tahun='';
					   $tahunsz='';
					@endphp
					@if($records->group > 0)
						@php
						   $tahuns=[];
						@endphp
						@foreach(checkGroup($records->group) as $kay => $grop)
							@php 
								$tahuns[]= $grop->rencanadetail->rencanaaudit->tahun;
							@endphp
						@endforeach
						@php 
							array_unique($tahuns);
						@endphp
						@if(count(array_unique($tahuns)) > 2)
							@php
								$tahunsz = implode(", ", array_unique($tahuns));
							@endphp
						@else 
							@php
								$tahunsz = implode(" & ", array_unique($tahuns));
							@endphp
						@endif
					@else
						@php
						   $tahunsz= $records->rencanadetail->rencanaaudit->tahun;
						@endphp
					@endif
					<td style="border: 1px #ffffff;text-align:justify;">Untuk kepentingan manajemen sesuai dengan Rencana Kerja Internal Audit Tahun {{ $tahunsz }} ( RKIA Th {{ $tahunsz }} ), maka dengan ini kami perintahkan Saudara untuk melaksanakan audit pada :</td>
				</tr>
				@if($records->group > 0)
						@php 
							$j=1;
						@endphp
						@foreach(checkGroup($records->group) as $kay => $grop)
							 @if($grop->rencanadetail->tipe == 0)
								<tr>
									<td style="text-align: left;border: 1px #ffffff;width: 10px;padding-left: 41px;">
									 	{{$j}}. {{ object_audit($grop->rencanadetail->tipe_object, $grop->rencanadetail->object_id) }}
										 @php 
											$j++;
										 @endphp
									</td>
								</tr>
							 @endif
						@endforeach
				@else 
					<tr>
						<td style="text-align: left;border: 1px #ffffff;width: 10px;padding-left: 41px;">1. {{ object_audit($records->rencanadetail->tipe_object, $records->rencanadetail->object_id) }}</td>
					</tr>
				@endif
			</tbody>
		</table>
		<table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;">
			<tbody>
				<tr style="">
					@php
					   $tgl='';
					@endphp
					@if($records->group > 0)
						@php 
							$tgls=[];
						@endphp
						@foreach(checkGroup($records->group) as $kay => $grop)
							 @foreach($grop->jadwal as $kuy => $jadwal)
							 	@php 
							 		$tgls[]= \Carbon\Carbon::parse($jadwal->tgl);
							 	@endphp
							 @endforeach
						@endforeach
						@php 
							$tgl = DateToStringWday(date(min($tgls))).' s/d '.DateToStringWday(date(max($tgls)))
						@endphp
					@else
						@php
						   $tgl= DateToStringWday($records->jadwal->min('tgl')).' s/d '.DateToStringWday($records->jadwal->max('tgl'));
						@endphp
					@endif
					<td style="border: 1px #ffffff;">mulai tanggal {{ $tgl }} sesuai jadwal terlampir.</td>
				</tr>
			</tbody>
		</table>
		<table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;">
			<tbody>
				<tr style="">
					<td style="border: 1px #ffffff;">Hasil Audit segera dilaporkan kepada kami.</td>
				</tr>
			</tbody>
		</table>
		<table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;">
			<tbody>
				<tr style="">
					<td style="border: 1px #ffffff;">Demikian, agar dilaksanakan sebagaimana mestinya.</td>
				</tr>
			</tbody>
		</table>
		<table class="page_content_header" style="padding-top: 30px;">
            <tr>
                <td style="text-align: left;padding-left: 9px;">
                    <b>President Director</b><br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <b>(I Gusti Ngurah Putra)</b>
                </td>
            </tr>
        </table>
        <table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;">
			<tbody>
				<tr style="">
					<td style="text-align: left;border: 1px #ffffff;" colspan="2">Tembusan : </td>
					@if(count($records->tembusans) > 0)
					<ol style="padding-top: -15px;">
						@foreach($records->tembusans as $data)
						  <li>{{ $data->user->name }}</li>
						@endforeach
					</ol> 
					@endif
				</tr>
			</tbody>
		</table>
	</main>
</body>
</html>