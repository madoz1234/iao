<form action="{{ route($routes.'.saveRejectDirut', $record->id) }}" method="POST" id="formData3">
	@method('PATCH')
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Penolakan</h5>
    </div>
    <div class="modal-body">
        <div class="form-group">
            <div class="form-group field">
            	<label class="control-label">Alasan Penolakan</label>
        		<textarea class="form-control" name="ket_svp" id="exampleFormControlTextarea1" placeholder="Alasan Penolakan" rows="3"></textarea>
            </div>           
        </div>
    </div>
    <div class="modal-footer">
    	<button type="button" class="btn btn-cancel" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-simpan save3 button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>
