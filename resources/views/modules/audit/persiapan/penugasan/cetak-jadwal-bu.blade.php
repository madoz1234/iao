<html>
<head>
<style>
        @page {
            margin: 60px 25px;
            counter-reset: page;
        }

       /* Define now the real margins of every page in the PDF */
       body {
           margin-top: 0.5cm;
           margin-left: 0cm;
           margin-right: 0cm;
           margin-bottom: 2cm;
           font-style: normal;
       }

       .ui.table.bordered {
         border: solid black 1px;
         border-collapse: collapse;
         width: 100%;
         font-family : "Arial", "Verdana", sans-serif;
       }

       .ui.table.bordered td {
         border: solid black .5px;
         border-collapse: collapse;
         /*padding:10px;*/
         padding:5px;
       }

       .ui.table.bordered td.center.aligned {
         text-align : center;
       }

       .page-break {
            page-break-after: always;
       }

       .pad{
       	padding-top: 5px;
       	padding-bottom: 5px;
       }
        header {
            position: fixed;
            top: -40px;
            left: 36px;
            right: 36px;
            height: 10px;
            text-align: center;
            font-size: 10px;
        }
		
       main {
         position: sticky;
         font-size : 12px;
         padding-top : -1cm;
         margin-top : 2px;
         border : none;
         width: 100%;
       }

       main p {
         margin-left: 5px;
         margin-right: 5px;
       }
       footer {
           position: fixed;
           bottom: -60px;
           left: 0px;
           right: 0px;
           height: 50px;
           text-align: center;
           line-height: 35px;
       }
       table.page_content {width: 100%; border: none; padding: 1mm; font-size: 10px;font-family : "Arial", "Verdana", sans-serif;}
       table.page_header {width: 100%; border: none; padding: 2mm; font-size: 10px;font-family : "Arial", "Verdana", sans-serif; text-align: left; margin-left: 50px;margin-top: 50px;}
       table.page_content_header {width: 100%; font-size: 10px;border-collapse: collapse;font-family : "Arial", "Verdana", sans-serif;text-align: center; padding-bottom: 1px;
       	padding-top: 1px;}
</style>
</head>
<body>
	<main>
		<table class="page_content ui table bordered" style="font-size: 10px;">
				<tr>
					<th style="text-align: center;border-right: 1px solid #7f7f7f;" rowspan="2">No</th>
					<th style="text-align: center;border-right: 1px solid #7f7f7f;" rowspan="2" colspan="2">Rangkaian Kegiatan Audit</th>
					<th style="text-align: center;border-right: 1px solid #7f7f7f;" rowspan="2">Kategori</th>
					<th style="text-align: center;border-right: 1px solid #7f7f7f;" rowspan="2">Tanggal</th>
					<th style="text-align: center; text-align: center;border-bottom: 1px solid #7f7f7f;border-right: 1px solid #7f7f7f;" colspan="2">Waktu</th>
					<th style="text-align: center; text-align: center;" rowspan="2">Keterangan</th>
				</tr>
				<tr>
					<th style="text-align: center;border-right: 1px solid #7f7f7f;">Mulai</th>
					<th style="text-align: center;border-right: 1px solid #7f7f7f;">Selesai</th>
				</tr>
				@if($records->group > 0)
					@foreach(checkGroup($records->group) as $kay => $grop)
		                <tr>
		                	@php 
		            			$rowz=0;
		                	@endphp
		                	@foreach($grop->jadwal as $kiy => $pelaksanaan)
	                			@php
	                				$rowz +=$pelaksanaan->detail->count();
	                			@endphp
	                		@endforeach
		                	<td style="text-align: center;width: 20px;" rowspan="{{ ($rowz+1) }}">{{ ($kay+1) }}</td>
							<td style="text-align: left;" colspan="2">{!! object_audit($grop->rencanadetail->tipe_object, $grop->rencanadetail->object_id) !!} @if(getAb($grop->rencanadetail->tipe_object, $grop->rencanadetail->object_id)) - @endif
								{{ getAb($grop->rencanadetail->tipe_object, $grop->rencanadetail->object_id) }}
							</td>
							<td class="" style="text-align: center;">
								@if($grop->rencanadetail->tipe_object == 0)
									<span class="label label-success">Business Unit (BU)</span>
								@elseif($grop->rencanadetail->tipe_object == 1)
									<span class="label label-info">Corporate Office (CO)</span>
								@elseif($grop->rencanadetail->tipe_object == 2)
									<span class="label label-default">Project</span>
								@else 
									<span class="label label-primary">Anak Perusahaan</span>
								@endif
							</td>
							<td class="" style="text-align: center;">
							</td>
							<td style="text-align: center;">
							</td>
							<td style="text-align: center;">
							</td>
							<td style="text-align: center;">
							</td>
						</tr>
		                @foreach($grop->jadwal as $kiy => $pelaksanaan)
								<tr class="detail_cek data-pelaksanaan-{{$kiy}}" data-id="{{$kiy}}">
									<td class="data_rowspan-{{$kiy}} numboor-{{$kiy}}" style="text-align: center;width: 80px;" rowspan="{{$pelaksanaan->detail->count()}}">Hari {{ romawi(($kiy+1)) }}</td>
									<td style="text-align: left;text-align: justify;text-justify: inter-word;width: 400px;" class="field">
										1. {{ $pelaksanaan->detail->first()->keterangan }}
									</td>
									<td class="data_rowspan-{{ $kiy }} field" style="text-align: center;width: 100px;">
									</td>
									<td class="data_rowspan-{{ $kiy }} field" style="text-align: center;width: 80px;" rowspan="{{$pelaksanaan->detail->count()}}">
										{{$pelaksanaan->tgl}}
									</td>
									<td style="text-align: center;width: 80px;" class="field">
										{{ $pelaksanaan->detail->first()->mulai }}
									</td>
									<td style="text-align: center;width: 80px;" class="field">
										{{ $pelaksanaan->detail->first()->selesai }}
									</td>
									@if($kiy == 0)
										<td style="text-align: center;width: 100px;" class="field" rowspan="{{ ($rowz) }}">
											KeteranganqW
										</td>
									@endif
								</tr>
								@php 
									$i=1;
								@endphp
								@foreach($pelaksanaan->detail as $koy => $detail)
									@if($koy > 0)
										<tr class="detail-pelaksanaan-{{ $kiy }} detail-{{ $kiy }}-{{ $koy }}">
											<td style="text-align: left;text-align: justify;text-justify: inter-word;" class="field">
												{{ ($i) }}. {{ $detail->keterangan }}
											</td>
											<td style="text-align: left;text-align: justify;text-justify: inter-word;" class="field">
											</td>
											<td style="text-align: center;" class="field">
												{{ $detail->mulai }}
											</td>
											<td style="text-align: center;" class="field">
												{{ $detail->selesai }}
											</td>
										</tr>
									@endif
									@php 
										$i++;
									@endphp
								@endforeach
								@php 
									$i=1;
								@endphp
						@endforeach
					@endforeach
				@else 
	                @php 
	                	$row=0;
	                @endphp
	                <tr>
	                	@php 
	                		$rowz=0;
	                	@endphp
	                	@foreach($records->jadwal as $kiy => $pelaksanaan)
                			@php
                				$rowz +=$pelaksanaan->detail->count();
                			@endphp
	                	@endforeach
	                	<td style="text-align: center;width: 20px;" rowspan="{{ ($rowz+1) }}">1</td>
						<td style="text-align: left;" colspan="2">{!! object_audit($records->rencanadetail->tipe_object, $records->rencanadetail->object_id) !!} @if(getAb($records->rencanadetail->tipe_object, $records->rencanadetail->object_id)) - @endif
							{{ getAb($records->rencanadetail->tipe_object, $records->rencanadetail->object_id) }}
						</td>
						<td class="" style="text-align: center;">
							@if($records->rencanadetail->tipe_object == 0)
								<span class="label label-success">Business Unit (BU)</span>
							@elseif($records->rencanadetail->tipe_object == 1)
								<span class="label label-info">Corporate Office (CO)</span>
							@elseif($records->rencanadetail->tipe_object == 2)
								<span class="label label-default">Project</span>
							@else 
								<span class="label label-primary">Anak Perusahaan</span>
							@endif
						</td>
						<td class="" style="text-align: center;">
						</td>
						<td style="text-align: center;">
						</td>
						<td style="text-align: center;">
						</td>
						<td style="text-align: center;">
						</td>
					</tr>
	                @foreach($records->jadwal as $kiy => $pelaksanaan)
							<tr class="detail_cek data-pelaksanaan-{{$kiy}}" data-id="{{$kiy}}">
								<td class="data_rowspan-{{$kiy}} numboor-{{$kiy}}" style="text-align: center;width: 80px;" rowspan="{{$pelaksanaan->detail->count()}}">Hari {{ romawi(($kiy+1)) }}</td>
								<td style="text-align: left;text-align: justify;text-justify: inter-word;width: 400px;" class="field">
									{{ $pelaksanaan->detail->first()->keterangan }}
								</td>
								<td class="data_rowspan-{{ $kiy }} field" style="text-align: center;width: 100px;">
								</td>
								<td class="data_rowspan-{{ $kiy }} field" style="text-align: center;width: 80px;" rowspan="{{$pelaksanaan->detail->count()}}">
									{{$pelaksanaan->tgl}}
								</td>
								<td style="text-align: center;width: 80px;" class="field">
									{{ $pelaksanaan->detail->first()->mulai }}
								</td>
								<td style="text-align: center;width: 80px;" class="field">
									{{ $pelaksanaan->detail->first()->selesai }}
								</td>
								<td style="text-align: center;width: 100px;" class="field" rowspan="{{$pelaksanaan->detail->count()}}">
									Keterangan
								</td>
							</tr>
							@foreach($pelaksanaan->detail as $koy => $detail)
								@if($koy > 0)
									<tr class="detail-pelaksanaan-{{ $kiy }} detail-{{ $kiy }}-{{ $koy }}">
										<td style="text-align: left;text-align: justify;text-justify: inter-word;" class="field">
											{{ $detail->keterangan }}
										</td>
										<td style="text-align: left;text-align: justify;text-justify: inter-word;" class="field">
										</td>
										<td style="text-align: center;" class="field">
											{{ $detail->mulai }}
										</td>
										<td style="text-align: center;" class="field">
											{{ $detail->selesai }}
										</td>
									</tr>
								@endif
							@endforeach
					@endforeach
				@endif
		</table>
	</main>
</body>
</html>