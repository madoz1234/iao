<form action="{{ route($routes.'.saveApproval', $id[0]) }}" method="POST" id="formData" class="was-validated">
	@method('PATCH')
    @csrf
    @foreach($id as $key => $cek)
 	 	<input type="hidden" name="id[]" value="{{ $cek }}">
	@endforeach
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Data Penugasan Audit</h5>
    </div>
    <div class="modal-body">
		<table id="table-pelaksanaan" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
			<thead>
				<tr>
					<th style="text-align: center;width: 20px;">No</th>
					<th style="text-align: center;">Objek Audit</th>
					<th style="text-align: center;">Rencana Pelaksanaan</th>
				</tr>
			</thead>
			<tbody class="container-pelaksanaan">
				@foreach($record as $key => $data)
					<tr>
						<td>{{ $key+1 }}</td>
						<td>{{ object_audit($data->rencanadetail->tipe_object, $data->rencanadetail->object_id) }}</td>
						<td>{{ $data->rencanadetail->rencana }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
    </div>
    <div class="modal-footer">
    	<button type="button" class="btn btn-cancel" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-info approve-save button" data-id="{{ $id[0] }}">OK, Submit</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script>
    </script>
    @yield('js-extra')
@endpush