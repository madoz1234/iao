@extends('layouts.form')
@section('title', 'Detil Surat Penugasan')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body" style="padding-bottom: 0px;">
		<form action="#" method="POST" id="formData">
			@method('PATCH')
			@csrf
			<table class="table" style="font-size: 12px;">
				<tbody>
					<tr>
						<td style="width:200px;border: 1px #ffffff;border: 1px #ffffff;">Tahun</td>
						<td style="width:2px;border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;" class="field">
							@php 
								$tahun=array();
							@endphp
							@foreach($data as $thn)
								@php 
									$tahun[]= $thn->rencanadetail->rencanaaudit->tahun;
								@endphp
							@endforeach
							{{ implode(", ", array_unique($tahun))}}
						</td>
					</tr>
					<tr>
						<td style="border: 1px #ffffff;">Rencana Pelaksanaan</td>
						<td style="border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;" class="field">
							@php 
								$rencana=array();
							@endphp
							@foreach($data as $rnc)
								@php 
									$rencana[]= $rnc->rencanadetail->rencana;
								@endphp
							@endforeach
							{{ implode(", ", $rencana)}}
						</td>
					</tr>
					<tr>
						<td style="border: 1px #ffffff;">Tipe</td>
						<td style="border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;" class="field">
							@php 
								$tipe=array();
							@endphp
							@foreach($data as $tip)
								@php 
								if($tip->rencanadetail->tipe == 0){
									$tipe[]= '<span class="label label-primary">Audit</span>';
								}elseif($tip->rencanadetail->tipe == 1){
									$tipe[]= '<span class="label label-success">Kegiatan Konsultasi</span>';
								}else{
									$tipe[]= '<span class="label label-info">Kegiatan Audit Lain - Lain</span>';
								}
								@endphp
							@endforeach
							{!! implode(" - ", array_unique($tipe))!!}
						</td>
					</tr>
					<tr>
						<td style="border: 1px #ffffff;">Kategori</td>
						<td style="border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;" class="field">
							@php 
								$kategori=array();
							@endphp
							@foreach($data as $kat)
								@php
								if($kat->rencanadetail->tipe == 0) {
									if($kat->rencanadetail->tipe_object == 0){
										$kategori[]= '<span class="label label-success">Business Unit (BU)</span>';
									}elseif($kat->rencanadetail->tipe_object == 1){
										$kategori[]= '<span class="label label-info">Corporate Office (CO)</span>';
									}elseif($kat->rencanadetail->tipe_object == 2){
										$kategori[]= '<span class="label label-default">Project</span>';
									}else{
										$kategori[]= '<span class="label label-primary">Anak Perusahaan</span>';
									}
								}elseif($kat->rencanadetail->tipe == 1) {
									$kategori[]= '<span class="label label-warning">'.$kat->rencanadetail->konsultasi->nama.'</span>';
								}else{
									$kategori[]= '<span class="label label-default">'.$kat->rencanadetail->lain->nama.'</span>';
								}
								@endphp
							@endforeach
							{!! implode(" - ", array_unique($kategori))!!}
						</td>
					</tr>
					<tr class="detil">
						<td style="border: 1px #ffffff;">Objek Audit</td>
						<td style="border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;">
							@php 
								$object=array();
							@endphp
							@foreach($data as $obj)
								@php 
									$object[]= object_audit($obj->rencanadetail->tipe_object, $obj->rencanadetail->object_id);
								@endphp
							@endforeach
							<table style="font-size: 12px;">
								<tbody>
									@php 
										$j=1;
									@endphp
									@foreach(array_unique($object) as $koy => $ob)
										@if($ob)
											<tr>
												<td style="width: 15px;">{{ $j }}.</td>
												<td style="">{{ $ob }}</td>
											</tr>
											@php 
												$j++;
											@endphp
										@endif
									@endforeach
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
			<input type="hidden" name="id" value="{{ $record->id }}">
			<div class="panel panel-default data-form">
				@foreach($record->jadwal->groupBy('tgl_id') as $a => $cek)
					<table class="table" style="font-size: 12px;">
						<tbody>
							<h6 style="font-weight: bold;padding-left: 7px;">{{ object_audit($cek->first()->rencanadetail->tipe_object, $cek->first()->rencanadetail->object_id) }} -	AB : {{ getAb($cek->first()->rencanadetail->tipe_object, $cek->first()->rencanadetail->object_id) }}</h6>
							<tr>
								<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">Ketua Tim</td>
								<td style="width:2px;border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;" class="field">
									@if($record->anggota->where('fungsi', 0)->where('data_id', $cek->first()->rencanadetail->id)->first())
										{{ $record->anggota->where('fungsi', 0)->where('data_id', $cek->first()->rencanadetail->id)->first()->user->name }}
									@else 
										-
									@endif
								</td>
							</tr>
							<tr>
								<td style="border: 1px #ffffff;">Auditor Operasional</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;" class="field">
									@php 
										$data_operasional = array();
										foreach($record->anggota->where('fungsi', 1)->where('data_id', $cek->first()->rencanadetail->id) as $operasional) {
										 $data_operasional[] = $operasional->user->name;
										}
									@endphp
									{{ implode(",", $data_operasional) }}
								</td>
							</tr>
							<tr>
								<td style="border: 1px #ffffff;">Auditor Keuangan</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;" class="field">
									@php 
										$data_keuangan = array();
										foreach($record->anggota->where('fungsi', 2)->where('data_id', $cek->first()->rencanadetail->id) as $keuangan) {
										 $data_keuangan[] = $keuangan->user->name;
										}
									@endphp
									{{ implode(",", $data_keuangan) }}
								</td>
							</tr>
							<tr>
								<td style="border: 1px #ffffff;">Auditor Sistem</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;" class="field">
									@php 
										$data_sistem = array();
										foreach($record->anggota->where('fungsi', 3)->where('data_id', $cek->first()->rencanadetail->id) as $sistem) {
										 $data_sistem[] = $sistem->user->name;
										}
									@endphp
									{{ implode(",", $data_sistem) }}
								</td>
							</tr>
						</tbody>
					</table>
				@endforeach
				<div class="tab-pane" id="b" role="tabpanel" aria-labelledby="b-tab">
					<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
						<tr>
							<td scope="col-md-12" colspan="3" style="text-align: left;">
								<h5 style="text-align: center;font-weight: bold;">JADWAL</h5>
								@foreach($record->jadwal->groupBy('tgl_id') as $a => $cek)
									<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
										<h6 style="font-weight: bold;">{{ object_audit($cek->first()->rencanadetail->tipe_object, $cek->first()->rencanadetail->object_id) }} -	AB : {{ getAb($cek->first()->rencanadetail->tipe_object, $cek->first()->rencanadetail->object_id) }}</h6>
										<tr style="text-align: center;">
											<td scope="col" style="text-align: left;border: 1px #ffffff;">
												<table id="table-pelaksanaan-{{ $cek->first()->tgl_id }}" data-id="{{ $cek->first()->tgl_id }}" class="table table-bordered m-t-none cek-table" style="width: 100%;font-size: 12px;">
													<thead>
														<tr>
															<th style="text-align: center;" rowspan="2">Hari</th>
															<th style="text-align: center;" rowspan="2">Tanggal</th>
															<th style="text-align: center; text-align: center;" colspan="2">Waktu</th>
															<th style="text-align: center;border-left: 1px solid rgba(34,36,38,.1);" rowspan="2">Item</th>
														</tr>
														<tr>
															<th style="text-align: center;border-top: 1px solid rgba(34,36,38,.1);">Mulai</th>
															<th style="text-align: center;border-top: 1px solid rgba(34,36,38,.1);">Selesai</th>
														</tr>
													</thead>
													<tbody class="container-pelaksanaan-{{ $cek->first()->tgl_id }}">
														@php 
															$i=0;
														@endphp
														@foreach($record->jadwal->where('tgl_id', $cek->first()->tgl_id) as $kiy => $pelaksanaan)
															<tr class="detail_cek-{{ $cek->first()->tgl_id }} data-pelaksanaan-{{ $cek->first()->tgl_id }}-{{$i}}" data-id="{{$i}}" data-parent="{{ $cek->first()->tgl_id }}">
																<td class="data_rowspan-{{ $cek->first()->tgl_id }}-{{$i}} numboor-{{ $cek->first()->tgl_id }}-{{$i}}" style="text-align: center;width: 100px;" rowspan="{{$pelaksanaan->detail->count()}}">{{ romawi(($i+1)) }}</td>
																<td class="data_rowspan-{{ $cek->first()->tgl_id }}-{{ $i }} field" style="text-align: center;width: 200px;" rowspan="{{$pelaksanaan->detail->count()}}"> 
																	{{DateToStringWday($pelaksanaan->tgl)}}
																</td>
																<td style="text-align: center;width: 150px;" class="field">
																	{{ $pelaksanaan->detail->first()->mulai }}
																</td>
																<td style="text-align: center;width: 150px;" class="field">
																	{{ $pelaksanaan->detail->first()->selesai }}
																</td>
																<td style="text-align: left;width: 800px;text-align: justify;text-justify: inter-word;" class="field">
																	{{ $pelaksanaan->detail->first()->keterangan }}
																</td>
															</tr>
															@php 
																$j=0;
															@endphp
															@foreach($pelaksanaan->detail as $koy => $detail)
																@if($j > 0)
																	<tr class="detail-pelaksanaan-{{ $cek->first()->tgl_id }}-{{ $i }} detail-{{ $cek->first()->tgl_id }}-{{ $i }}-{{ $j }}">
																		<td style="text-align: center;width: 150px;" class="field">
																			{{ $detail->mulai }}
																		</td>
																		<td style="text-align: center;width: 150px;" class="field">
																			{{ $detail->selesai }}
																		</td>
																		<td style="text-align: left;width: 800px;text-align: justify;text-justify: inter-word;" class="field">
																			{{ $detail->keterangan }}
																		</td>
																	</tr>
																	<input type="hidden" name="last_detail_{{ $cek->first()->tgl_id }}_{{$i}}" value="{{$j}}">
																@endif
																	@php 
																		$j++;
																	@endphp
															@endforeach
															<input type="hidden" name="last_pelaksanaan_{{ $cek->first()->tgl_id }}" value="{{ $i }}">
															@php 
																$i++;
															@endphp
													@endforeach
													</tbody>
												</table>
											</td>
										</tr>
									</table>
								@endforeach
							</td>
						</tr>
					</table>
					@if(count($record->tembusans) > 0)
						<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
							<thead>
								<tr>
									<td style="text-align: left;" colspan="3">Tembusan</td>
								</tr>
							</thead>
							<tbody class="container-tembusan">
									@foreach($record->tembusans as $key => $datas)
										<tr class="data-container-{{$key}}" data-id="{{$key}}">
											<td style="width: 20px;">
												<label style="margin-top: 7px;padding-left: 5px;" class="numbur-{{$key}}">{{$key+1}}</label>
											</td>
											<td style="border-bottom: 1px solid rgba(34,36,38,.1);text-align: left;" class="field">
												{{$datas->user->name}}
											</td>
										</tr>
									@endforeach
							</tbody>
						</table>
					@endif
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-left">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
        .table-responsive{
            overflow-x: auto;
            overflow-y: visible !important;
        }

        .table-responsive .dropdown-menu {
	        position: static !important;
	    }
    </style>
@endpush