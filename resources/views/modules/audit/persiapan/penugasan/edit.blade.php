@extends('layouts.form')
@section('title', 'Ubah Surat Penugasan')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	@if($record->ket_svp)
		<p style="padding-left: 14px;color:red;">Status <b>Ditolak</b> pada {{DateToStringWday($record->updated_at)}}</p>
		<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
			<tr>
				<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
					<h5 style="font-weight: bold;font-size: 12px;">Keterangan :</h5>
					<p style="text-align: justify;text-justify: inter-word;">{{ $record->ket_svp }}</p>
				</td>
			</tr>
		</table>
	@endif
	<div class="panel-body" style="padding-bottom: 0px;">
		<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
			@method('PATCH')
		    @csrf
		    <div class="loading dimmer padder-v" style="display: none;">
			    <div class="loader"></div>
			</div>
			<input type="hidden" name="id" value="{{ $record->id }}">
			<input type="hidden" name="tahun" value="{{ $record->rencanadetail->rencanaaudit->tahun }}">
			<input type="hidden" name="status" value="0">
			<table class="table" style="font-size: 12px;">
				<tbody>
					<tr>
						<td style="width:200px;border: 1px #ffffff;border: 1px #ffffff;">Tahun</td>
						<td style="width:2px;border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;" class="field">
							@php 
								$tahun=array();
							@endphp
							@foreach($data as $thn)
								@php 
									$tahun[]= $thn->rencanadetail->rencanaaudit->tahun;
								@endphp
							@endforeach
							{{ implode(", ", array_unique($tahun))}}
						</td>
					</tr>
					<tr>
						<td style="border: 1px #ffffff;">Rencana Pelaksanaan</td>
						<td style="border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;" class="field">
							@php 
								$rencana=array();
							@endphp
							@foreach($data as $rnc)
								@php 
									$rencana[]= $rnc->rencanadetail->rencana;
								@endphp
							@endforeach
							{{ implode(", ", $rencana)}}
						</td>
					</tr>
					<tr>
						<td style="border: 1px #ffffff;">Tipe</td>
						<td style="border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;" class="field">
							@php 
								$tipe=array();
							@endphp
							@foreach($data as $tip)
								@php 
								if($tip->rencanadetail->tipe == 0){
									$tipe[]= '<span class="label label-primary">Audit</span>';
								}elseif($tip->rencanadetail->tipe == 1){
									$tipe[]= '<span class="label label-success">Kegiatan Konsultasi</span>';
								}else{
									$tipe[]= '<span class="label label-info">Kegiatan Audit Lain - Lain</span>';
								}
								@endphp
							@endforeach
							{!! implode(" - ", array_unique($tipe))!!}
						</td>
					</tr>
					<tr>
						<td style="border: 1px #ffffff;">Kategori</td>
						<td style="border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;" class="field">
							@php 
								$kategori=array();
							@endphp
							@foreach($data as $kat)
								@php
								if($kat->rencanadetail->tipe == 0) {
									if($kat->rencanadetail->tipe_object == 0){
										$kategori[]= '<span class="label label-success">Business Unit (BU)</span>';
									}elseif($kat->rencanadetail->tipe_object == 1){
										$kategori[]= '<span class="label label-info">Corporate Office (CO)</span>';
									}elseif($kat->rencanadetail->tipe_object == 2){
										$kategori[]= '<span class="label label-default">Project</span>';
									}else{
										$kategori[]= '<span class="label label-primary">Anak Perusahaan</span>';
									}
								}elseif($kat->rencanadetail->tipe == 1) {
									$kategori[]= '<span class="label label-warning">'.$kat->rencanadetail->konsultasi->nama.'</span>';
								}else{
									$kategori[]= '<span class="label label-default">'.$kat->rencanadetail->lain->nama.'</span>';
								}
								@endphp
							@endforeach
							{!! implode(" - ", array_unique($kategori))!!}
						</td>
					</tr>
					<tr class="detil">
						<td style="border: 1px #ffffff;">Objek Audit</td>
						<td style="border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;">
							@php 
								$object=array();
							@endphp
							@foreach($data as $obj)
								@php 
									$object[]= object_audit($obj->rencanadetail->tipe_object, $obj->rencanadetail->object_id);
								@endphp
							@endforeach
							<table style="font-size: 12px;">
								<tbody>
									@php
										$i=1;
									@endphp
									@foreach(array_unique($object) as $koy => $ob)
										@if($ob)
											<tr>
												<td style="width: 15px;">{{ $i }}.</td>
												<td style="">{{ $ob }}</td>
											</tr>
											@php
												$i++;
											@endphp
										@endif
									@endforeach
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="form-row">
				<div class="form-group col-md-12">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="a-tab" data-toggle="tab" href="#a" role="tab" aria-controls="home" aria-selected="true">A. TIM</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="b-tab" data-toggle="tab" href="#b" role="tab" aria-controls="messages" aria-selected="false">B. JADWAL</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="c-tab" data-toggle="tab" href="#c" role="tab" aria-controls="messages" aria-selected="false">C. TEMBUSAN</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="a" role="tabpanel" aria-labelledby="a-tab">
							@foreach($record->jadwal->groupBy('tgl_id') as $a => $cek)
								@php 
									$data_ketua = array();
									foreach($record->anggota->where('fungsi', 0)->where('data_id', $cek->first()->rencanadetail->id) as $ketua) {
									 $data_ketua = $ketua->user_id;
									}

									$data_operasional = array();
									foreach($record->anggota->where('fungsi', 1)->where('data_id', $cek->first()->rencanadetail->id) as $operasional) {
									 $data_operasional[] = $operasional->user_id;
									}

									$data_keuangan = array();
									foreach($record->anggota->where('fungsi', 2)->where('data_id', $cek->first()->rencanadetail->id) as $keuangan) {
									 $data_keuangan[] = $keuangan->user_id;
									}

									$data_sistem = array();
									foreach($record->anggota->where('fungsi', 3)->where('data_id', $cek->first()->rencanadetail->id) as $sistem) {
									 $data_sistem[] = $sistem->user_id;
									}
								@endphp
								<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
									<h6 style="font-weight: bold;">{{ object_audit($cek->first()->rencanadetail->tipe_object, $cek->first()->rencanadetail->object_id) }} -	AB : {{ getAb($cek->first()->rencanadetail->tipe_object, $cek->first()->rencanadetail->object_id) }}</h6>
									<tr>
										<td style="width: 10%;border: 1px #ffffff;">Ketua Tim</td>
										<td style="width: 1%;border: 1px #ffffff;">:</td>
										<td scope="col-md-12" colspan="3" style="text-align: left;" class="field">
											<select data-width="100%" class="selectpicker form-control" name="detail[{{ $cek->first()->rencanadetail->id }}][ketua]" data-size="3" data-style="btn-default" data-live-search="true" title="(Pilih salah satu)">
												@foreach(App\Models\Auths\User::WhereLike('auditor')->get() as $user)
													<option value="{{ $user->id }}" @if($user->id == $data_ketua) selected @endif>{{ $user->name }}</option>
												@endforeach
											</select>
										</td>
									</tr>
									<tr>
										<td style="width: 10%;border: 1px #ffffff;">Auditor Operasional</td>
										<td style="width: 1%;border: 1px #ffffff;">:</td>
										<td scope="col-md-12" colspan="3" style="text-align: left;" class="field">
											<div>
												<select data-width="100%" class="selectpicker form-control" data-size="5" name="detail[{{ $cek->first()->rencanadetail->id }}][operasional][]" data-style="btn-default" data-live-search="true" multiple title="(Pilih salah satu atau beberapa)">
													@foreach(App\Models\Auths\User::WhereLike('auditor')->where('fungsi', 1)->get() as $user)
														<option value="{{ $user->id }}" @if(in_array($user->id, $data_operasional)) selected @endif>{{ $user->name }}</option>
													@endforeach
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<td style="width: 10%;border: 1px #ffffff;">Auditor Keuangan</td>
										<td style="width: 1%;border: 1px #ffffff;">:</td>
										<td scope="col-md-12" colspan="3" style="text-align: left;" class="field">
											<div>
												<select data-width="100%" class="selectpicker form-control" data-size="5" name="detail[{{ $cek->first()->rencanadetail->id }}][keuangan][]" data-style="btn-default" data-live-search="true" multiple title="(Pilih salah satu atau beberapa)">
													@foreach(App\Models\Auths\User::WhereLike('auditor')->where('fungsi', 2)->get() as $user)
														<option value="{{ $user->id }}" @if(in_array($user->id, $data_keuangan)) selected @endif>{{ $user->name }}</option>
													@endforeach
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<td style="width: 10%;border: 1px #ffffff;">Auditor Sistem</td>
										<td style="width: 1%;border: 1px #ffffff;">:</td>
										<td scope="col-md-12" colspan="3" style="text-align: left;" class="field">
											<div>
												<select data-width="100%" class="selectpicker form-control" name="detail[{{ $cek->first()->rencanadetail->id }}][sistem][]" data-style="btn-default" data-size="5" data-live-search="true" multiple title="(Pilih salah satu atau beberapa)">
													@foreach(App\Models\Auths\User::WhereLike('auditor')->where('fungsi', 3)->get() as $user)
														<option value="{{ $user->id }}" @if(in_array($user->id, $data_sistem)) selected @endif>{{ $user->name }}</option>
													@endforeach
												</select>
											</div>
										</td>
									</tr>
								</table>
							@endforeach
							<div class="form-group col-md-12">
								<div class="text-right">
									<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
									<button type="button" class="btn btn-info page1">Selanjutnya</button>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="b" role="tabpanel" aria-labelledby="b-tab">
							<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
								<tr>
									<td scope="col-md-12" colspan="3" style="text-align: left;">
										@foreach($record->jadwal->groupBy('tgl_id') as $a => $cek)
											<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
												<h6 style="font-weight: bold;">{{ object_audit($cek->first()->rencanadetail->tipe_object, $cek->first()->rencanadetail->object_id) }} -	AB : {{ getAb($cek->first()->rencanadetail->tipe_object, $cek->first()->rencanadetail->object_id) }}</h6>
												<tr style="text-align: center;">
													<td scope="col" style="text-align: left;border: 1px #ffffff;">
														<table id="table-pelaksanaan-{{ $cek->first()->tgl_id }}" data-id="{{ $cek->first()->tgl_id }}" class="table table-bordered m-t-none cek-table" style="width: 100%;font-size: 12px;">
															<thead>
																<tr>
																	<th style="text-align: center;" rowspan="2">Hari</th>
																	<th style="text-align: center;" rowspan="2">Tanggal</th>
																	<th style="text-align: center; text-align: center;" colspan="2">Waktu</th>
																	<th style="text-align: center;border-left: 1px solid rgba(34,36,38,.1);" rowspan="2">Item</th>
																	<th style="text-align: center;width: 50px;border-right: 1px solid rgba(34,36,38,.1);border-top: 1px solid rgba(34,36,38,.1);" rowspan="2" colspan="2">
																		<button class="btn btn-sm btn-success tambah_pelaksanaan" data-toggle="tooltip" data-placement="left" title="Tambah Pelaksanaan" type="button" style="border-radius: 20px;" data-parent="{{ $cek->first()->tgl_id }}"><i class="fa fa-plus"></i></button>
																	</th>
																</tr>
																<tr>
																	<th style="text-align: center;border-top: 1px solid rgba(34,36,38,.1);">Mulai</th>
																	<th style="text-align: center;border-top: 1px solid rgba(34,36,38,.1);">Selesai</th>
																</tr>
															</thead>
															<tbody class="container-pelaksanaan-{{ $cek->first()->tgl_id }}">
																@php 
																	$i=0;
																@endphp
																@foreach($record->jadwal->where('tgl_id', $cek->first()->tgl_id) as $kiy => $pelaksanaan)
																	<tr class="detail_cek-{{ $cek->first()->tgl_id }} data-pelaksanaan-{{ $cek->first()->tgl_id }}-{{$i}}" data-id="{{$i}}" data-parent="{{ $cek->first()->tgl_id }}">
																		<td class="data_rowspan-{{ $cek->first()->tgl_id }}-{{$i}} numboor-{{ $cek->first()->tgl_id }}-{{$i}}" style="text-align: center;width: 100px;" rowspan="{{$pelaksanaan->detail->count()}}">{{ romawi(($i+1)) }}</td>
																		<td class="data_rowspan-{{ $cek->first()->tgl_id }}-{{ $i }} field" style="text-align: center;width: 200px;" rowspan="{{$pelaksanaan->detail->count()}}"> 
																			<input type="hidden" name="detail[{{ $cek->first()->tgl_id }}][pelaksanaan][{{ $i }}][tgl_id]" value="{{ $cek->first()->tgl_id }}">
																			<input class="form-control cekcek-{{ $cek->first()->tgl_id }}-{{$i}} cekdate-{{ $cek->first()->tgl_id }} datefak-{{ $cek->first()->tgl_id }}-{{$i}}" data-val="0" data-parent="{{ $cek->first()->tgl_id }}" id="tanggal" name="detail[{{ $cek->first()->tgl_id }}][pelaksanaan][{{ $i }}][tanggal]" placeholder="Tanggal" type="text" value="{{$pelaksanaan->tgl}}" onChange="disabledKoplok(this)"/>
																		</td>
																		<td style="text-align: center;width: 150px;" class="field">
																			<input class="form-control waktu" id="waktu" name="detail[{{ $cek->first()->tgl_id }}][pelaksanaan][{{ $i }}][detail][0][mulai]" placeholder="Mulai" type="text" value="{{ $pelaksanaan->detail->first()->mulai }}" />
																		</td>
																		<td style="text-align: center;width: 150px;" class="field">
																			<input class="form-control waktu" id="waktu" name="detail[{{ $cek->first()->tgl_id }}][pelaksanaan][{{ $i }}][detail][0][selesai]" placeholder="Selesai" type="text" value="{{ $pelaksanaan->detail->first()->selesai }}"/>
																		</td>
																		<td style="text-align: left;width: 800px;text-align: justify;text-justify: inter-word;" class="field">
																			<input class="form-control" id="item" name="detail[{{ $cek->first()->tgl_id }}][pelaksanaan][{{ $i }}][detail][0][item]" placeholder="Item" type="text" value="{{ $pelaksanaan->detail->first()->keterangan }}"/>
																		</td>
																		<td style="text-align: center;width: 50px;">
																			<button class="btn btn-outline-success btn-sm tambah_detail" type="button" data-toggle="tooltip" data-placement="left" data-id="{{ $i }}" data-parent="{{ $cek->first()->tgl_id }}" title="Tambah Item" style="border-radius: 20px;"><i class="fa fa-plus"></i></button>
																		</td>
																		<td class="data_rowspan-{{ $cek->first()->tgl_id }}-{{$i}}" style="text-align: center;width: 50px;" rowspan="">
																			<button class="btn btn-sm btn-danger hapus_pelaksanaan" type="button" data-id="{{$i}}"  data-parent="{{ $cek->first()->tgl_id }}" data-toggle="tooltip" data-placement="left" title="Hapus Pelaksanaan" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
																		</td>
																	</tr>
																	@php 
																		$j=0;
																	@endphp
																	@foreach($pelaksanaan->detail as $koy => $detail)
																		@if($j > 0)
																			<tr class="detail-pelaksanaan-{{ $cek->first()->tgl_id }}-{{ $i }} detail-{{ $cek->first()->tgl_id }}-{{ $i }}-{{ $j }}">
																				<td style="text-align: center;width: 150px;" class="field">
																					<input class="form-control waktu" id="waktu" name="detail[{{ $cek->first()->tgl_id }}][pelaksanaan][{{ $i }}][detail][{{ $j }}][mulai]" placeholder="Mulai" type="text" value="{{ $detail->mulai }}" />
																				</td>
																				<td style="text-align: center;width: 150px;" class="field">
																					<input class="form-control waktu" id="waktu" name="detail[{{ $cek->first()->tgl_id }}][pelaksanaan][{{ $i }}][detail][{{ $j }}][selesai]" placeholder="Mulai" type="text" value="{{ $detail->selesai }}" />
																				</td>
																				<td style="text-align: left;width: 800px;text-align: justify;text-justify: inter-word;" class="field">
																					<input class="form-control waktu" id="waktu" name="detail[{{ $cek->first()->tgl_id }}][pelaksanaan][{{ $i }}][detail][{{ $j }}][item]" placeholder="Mulai" type="text" value="{{ $detail->keterangan }}" />
																				</td>
																				<td style="text-align: center;width: 50px;border-right: 1px solid rgba(34,36,38,.1);">
																					<button class="btn btn-outline-danger btn-sm hapus_detail" type="button" data-id="{{ $i }}" data-detail="{{ $j }}" data-parent="{{ $cek->first()->tgl_id }}" data-toggle="tooltip" data-placement="left" title="Hapus Item" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
																				</td>
																			</tr>
																			<input type="hidden" name="last_detail_{{ $cek->first()->tgl_id }}_{{$i}}" value="{{$j}}">
																		@endif
																			@php 
																				$j++;
																			@endphp
																	@endforeach
																	<input type="hidden" name="last_pelaksanaan_{{ $cek->first()->tgl_id }}" value="{{ $i }}">
																	@php 
																		$i++;
																	@endphp
															@endforeach
															</tbody>
														</table>
													</td>
												</tr>
											</table>
										@endforeach
									</td>
								</tr>
							</table>
							<div class="form-group col-md-12">
								<div class="text-right">
									<button type="button" class="btn btn-cancel kembali1">Sebelumnya</button>
									<button type="button" class="btn btn-info page2">Selanjutnya</button>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="c" role="tabpanel" aria-labelledby="c-tab">
							<table id="tembusan" class="table table-bordered m-t-none" style="font-size: 12px;">
								<thead>
									<tr>
										<td style="text-align: left;" colspan="4">Tembusan</td>
									</tr>
								</thead>
								<tbody class="container-tembusan">
									@if(count($record->tembusans) > 0)
										@foreach($record->tembusans as $key => $tembusans)
											<tr class="data-container-{{$key}}" data-id="{{$key}}">
												<td style="width: 20px;">
													<label style="margin-top: 7px;padding-left: 5px;" class="numboor-{{$key}}">{{$key+1}}</label>
												</td>
												<td style="border-bottom: 1px solid rgba(34,36,38,.1);" class="field">
													<select data-id="{{$key}}" class="selectpicker form-control show-tick cari" name="details[{{$key}}][tembusan]" data-style="btn-default" data-live-search="true" data-size="3" title="(Pilih Salah Satu)">
														@foreach(App\Models\Auths\User::where('id', '!=', 1)->get() as $user)
															<option value="{{ $user->id }}" class="cuy" @if($user->id == $tembusans->user_id) selected @endif>{{ $user->name }}</option>
														@endforeach
													</select>  
												</td>
												<td style="width: 80px;border-bottom: 1px solid rgba(34,36,38,.1);">
													@if($key == 0)
														<button class="btn btn-sm btn-success tambah_tembusan" type="button" style="border-radius: 20px;margin-top: 2px;"><i class="fa fa-plus"></i></button>
													@else 
														<button class="btn btn-sm btn-danger hapus_tembusan" type="button" style="border-radius:20px;" data-id={{$key}}><i class="fa fa-remove"></i></button>
													@endif
												</td>
											</tr>
										@endforeach
									@else 
										<tr class="data-container-0" data-id="0">
											<td style="width: 20px;">
												<label style="margin-top: 7px;padding-left: 5px;" class="numboor-0">1</label>
											</td>
											<td style="border-bottom: 1px solid rgba(34,36,38,.1);" class="field">
												<select data-id="0" class="selectpicker form-control show-tick cari" name="details[0][tembusan]" data-style="btn-default" data-live-search="true" data-size="3" title="(Pilih Salah Satu)">
													@foreach(App\Models\Auths\User::where('id', '!=', 1)->get() as $user)
														<option value="{{ $user->id }}" class="cuy">{{ $user->name }}</option>
													@endforeach
												</select>  
											</td>
											<td style="width: 80px;border-bottom: 1px solid rgba(34,36,38,.1);"><button class="btn btn-sm btn-success tambah_tembusan" type="button" style="border-radius: 20px;margin-top: 2px;"><i class="fa fa-plus"></i></button></td>
										</tr>
									@endif
								</tbody>
							</table>
							<div class="form-group col-md-12">
								<div class="text-right">
									<button type="button" class="btn btn-cancel kembali2">Sebelumnya</button>
									<button type="button" class="btn btn-primary save as drafting">Save As Draft</button>
									<button type="button" class="btn btn-simpan save as page">Submit</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
        .table-responsive{
            overflow-x: auto;
            overflow-y: visible !important;
        }

        .table-responsive .dropdown-menu {
	        position: static !important;
	    }
	    .datepicker.dropdown-menu {
		    z-index: 9999 !important;
		}
    </style>
@endpush
@push('scripts')
   <script> 
    	var dateDis = [];
    	var dd = [];
    	var mindate ='';
    	function disabledKoplok(element)
    	{
    		var parent = parseInt(element.getAttribute("data-parent"));
    		var e0 = parseInt(element.getAttribute("data-val"))-1;
    		var e1 = parseInt(element.getAttribute("data-val"));
    		var e2 = parseInt(element.getAttribute("data-val"))+1;
    		var cek = $('.datefak-'+parent+'-'+e0).val();
    		var cek2 = $('.datefak-'+parent+'-'+e1).val();
    		if(!isEmpty(cek2)){
	    		var tgl = cek2.split("-").slice(0)[0];
	    		var bln = cek2.split("-").slice(1)[0];
	    		var thn = cek2.split("-").slice(2)[0];
	    		var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	    		var month = ('0' + (months.indexOf(bln)+1)).slice(-2);
	    		var dates = thn+'-'+month+'-'+tgl;
	    		var dat = new Date(dates);
	    		dat.setDate(dat.getDate() + 1);
    		}else{
    			var dat = minDates;
    		}
    		var retval = getDates(minDates, dat);
    		$('.datefak-'+parent+'-'+e2).prop('readonly', false);
    		$('.datefak-'+parent+'-'+e2).datepicker('destroy').datepicker({
    			orientation: "bottom right",
    			todayHighlight: true,
    			defaultViewDate: {year:$('input[name=tahun]').val(), month:(months.indexOf(bln)), day:tgl},
    			autoclose:true,
    			format: 'dd-M-yyyy',
    			yearRange: '-0:+1',
    			startDate: dat,
    			endDate: last,
    			hideIfNoPrevNext: true,
    			datesDisabled: retval,
    		});
    	}

    	function getDates(minDates, dat){
		  var oneDay = 24*3600*1000;
		  for (var dd=[],ms=minDates*1,last=dat*1;ms<last;ms+=oneDay){
		    dd.push( new Date(ms) );
		  }
		  return dd;
		}

    	$(document).on('click', '.page1', function (e){
    		$('#a-tab').removeClass('active');
    		$('#a-tab').closest('li').removeClass('active');
    		$('#b-tab').addClass('active');
    		$('#b-tab').closest('li').addClass('active');
    		$('#a').removeClass('active');
    		$('#b').addClass('active');
		});

		$(document).on('click', '.kembali1', function (e){
    		$('#a-tab').addClass('active');
    		$('#a-tab').closest('li').addClass('active');
    		$('#b-tab').removeClass('active');
    		$('#b-tab').closest('li').removeClass('active');
    		$('#a').addClass('active');
    		$('#b').removeClass('active');
		});

		$(document).on('click', '.page2', function (e){
    		$('#b-tab').removeClass('active');
    		$('#b-tab').closest('li').removeClass('active');
    		$('#c-tab').addClass('active');
    		$('#c-tab').closest('li').addClass('active');
    		$('#b').removeClass('active');
    		$('#c').addClass('active');
		});

		$(document).on('click', '.kembali2', function (e){
    		$('#b-tab').addClass('active');
    		$('#b-tab').closest('li').addClass('active');
    		$('#c-tab').removeClass('active');
    		$('#c-tab').closest('li').removeClass('active');
    		$('#b').addClass('active');
    		$('#c').removeClass('active');
		});

    	$('#myTab li:first-child a').tab('show')
    	function romawi(number){
        	var map = [1,4,5,9,10,40,50,90,100,400,500,900,1000];  
        	var roma = ["I","IV","V","IX","X","XL","L","XC","C","CD","D","CM","M"];  
        	var hasil='';  

        	for(var i=12; i >=0; i--) {  
        		while(number >= map[i]) {  
        			number = number - map[i];  
        			hasil = hasil + roma[i];  
        		}  
        	}  
		    return hasil;
        }

        var isEmpty = function(data) {
		    if(typeof(data) === 'object'){
		        if(JSON.stringify(data) === '{}' || JSON.stringify(data) === '[]'){
		            return true;
		        }else if(!data){
		            return true;
		        }
		        return false;
		    }else if(typeof(data) === 'string'){
		        if(!data.trim()){
		            return true;
		        }
		        return false;
		    }else if(typeof(data) === 'undefined'){
		        return true;
		    }else{
		        return false;
		    }
		}

        var d = new Date();
    	var full = d.getFullYear();
    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
    	var last = new Date(lastDate);
    	if($('input[name=tahun]').val() > full){
    		var min = minDates;
    	}else{
    		var min = d;
    	}

    	$('.cek-table').each(function(){
            var table_id = $(this).data("id");
	    	var table = $('#table-pelaksanaan-'+table_id);
			var ros = table.find('tbody tr.detail_cek-'+table_id);
	    	$.each(ros, function(key, value){
				var cekval = table.find('.cekcek-'+table_id+'-'+$(this).data("id")).data("val");
				for(i=0; i < 600; ++i) {
					$('.cekcek-'+table_id+'-'+$(this).data("id")).removeClass('datefak-'+table_id+'-'+i);
				}
				$('.cekcek-'+table_id+'-'+$(this).data("id")).addClass('datefak-'+table_id+'-'+key);
				$('input[name="detail['+table_id+'][pelaksanaan]['+$(this).data("id")+'][tanggal]"]').attr("data-val", key)
	            if(key == 0){
	            	$('.datefak-'+table_id+'-0').datepicker('destroy').datepicker({
			    		orientation: "bottom right",
			    		todayHighlight: true,
			    		autoclose:true,
			    		format: 'dd-M-yyyy',
			    		yearRange: '-0:+1',
			    		defaultViewDate: {year:$('input[name=tahun]').val(), month:0, day:1},
			    		startDate: minDates,
			    		endDate: last,
			    		hideIfNoPrevNext: true,
			    	});
	            }else{
	            	var c1= (key-1);
	            	var c2= $('.datefak-'+table_id+'-'+c1).val();
		    		if(!isEmpty(c2)){
			    		var tgl = c2.split("-").slice(0)[0];
			    		var bln = c2.split("-").slice(1)[0];
			    		var thn = c2.split("-").slice(2)[0];
			    		var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
			    		var month = ('0' + (months.indexOf(bln)+1)).slice(-2);
			    		var dates = thn+'-'+month+'-'+tgl;
			    		var dat = new Date(dates);
			    		dat.setDate(dat.getDate() + 1);
		    		}else{
		    			var dat = minDates;
		    		}

		    		var retval = getDates(minDates, dat);
			    	$('.datefak-'+table_id+'-'+key).datepicker('destroy').datepicker({
			    		orientation: "bottom right",
			    		todayHighlight: true,
			    		defaultViewDate: {year:$('input[name=tahun]').val(), month:(months.indexOf(bln)), day:tgl},
			    		autoclose:true,
			    		format: 'dd-M-yyyy',
			    		yearRange: '-0:+1',
			    		startDate: dat,
			    		endDate: last,
			    		hideIfNoPrevNext: true,
			    		datesDisabled: retval,
			    	});
	            }
			});
        });


        $('.waktu').clockpicker({
        	autoclose:true,
        	orientation: "bottom",
        	'default': 'now',
        });

    	$(document).on('click', '.tambah_pelaksanaan', function(e){
    		var parent =$(this).data("parent");
    		var last = parseInt($('input[name=last_pelaksanaan]').val()) + 1;
    		var table = $('#table-pelaksanaan-'+parent);
			var rowCount = table.find('tbody tr.detail_cek-'+parent).length;
			var c = rowCount-1;
			var html = `
						<tr class="detail_cek-`+parent+` data-pelaksanaan-`+parent+`-`+(c+2)+`" data-id="`+(c+2)+`" data-parent="`+parent+`">
							<td class="data_rowspan-`+parent+`-`+(c+2)+` numboor-`+parent+`-`+(c+2)+`" style="text-align: center;width: 100px;" rowspan="">`+romawi(c+2)+`</td>
							<td class="data_rowspan-`+parent+`-`+(c+2)+` field" style="text-align: center;width: 200px;" rowspan="">
								<input type="hidden" name="detail[`+parent+`][pelaksanaan][`+(c+2)+`][tgl_id]" value="`+parent+`">
								<input class="form-control cekcek-`+parent+`-`+(c+2)+` cekdate-`+parent+` datefak-`+parent+`-`+(c+1)+`" id="tanggal" data-parent="`+parent+`" data-val="`+(c+1)+`" name="detail[`+parent+`][pelaksanaan][`+(c+2)+`][tanggal]" placeholder="Tanggal" type="text" onChange="disabledKoplok(this)"/>
							</td>
							<td style="text-align: center;width: 150px;" class="field">
								<input class="form-control waktu" id="waktu" name="detail[`+parent+`][pelaksanaan][`+(c+2)+`][detail][0][mulai]" placeholder="Mulai" type="text"/>
							</td>
							<td style="text-align: center;width: 150px;" class="field">
								<input class="form-control waktu" id="waktu" name="detail[`+parent+`][pelaksanaan][`+(c+2)+`][detail][0][selesai]" placeholder="Selesai" type="text"/>
							</td>
							<td style="text-align: center;width: 800px;" class="field">
								<input class="form-control" id="item" name="detail[`+parent+`][pelaksanaan][`+(c+2)+`][detail][0][item]" placeholder="Item" type="text"/>
							</td>
							<td style="text-align: center;width: 50px;">
								<button class="btn btn-outline-success btn-sm tambah_detail" type="button" data-id="`+(c+2)+`" data-parent="`+parent+`" data-toggle="tooltip" data-placement="left" title="Tambah Item" style="border-radius: 20px;"><i class="fa fa-plus"></i></button>
							</td>
							<td class="data_rowspan-`+(c+2)+`" style="text-align: center;width: 50px;" rowspan="">
								<button class="btn btn-sm btn-danger hapus_pelaksanaan" type="button" data-id="`+(c+2)+`" data-parent="`+parent+`" data-toggle="tooltip" data-placement="left" title="Hapus Pelaksanaan" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
							</td>
							<input type="hidden" name="last_detail_`+parent+`_`+(c+2)+`" value="0">
						</tr>
				`;

				$('.container-pelaksanaan-'+parent).append(html);
		        $('input[name=last_pelaksanaan_'+parent+']').val((c+2))
		        $('.btn').tooltip('enable');
		        var d = new Date();
		    	var full = d.getFullYear();
		    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
		    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
		    	var last = new Date(lastDate);

		    	if($('input[name=tahun]').val() > full){
		    		var min = minDates;
		    	}else{
		    		var min = d;
		    	}

		    	var cek = $('.datefak-'+parent+'-'+c).val();
	    		if(!isEmpty(cek)){
		    		var tgl = cek.split("-").slice(0)[0];
		    		var bln = cek.split("-").slice(1)[0];
		    		var thn = cek.split("-").slice(2)[0];
		    		var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		    		var month = ('0' + (months.indexOf(bln)+1)).slice(-2);
		    		var dates = thn+'-'+month+'-'+tgl;
		    		var dat = new Date(dates);
		    		dat.setDate(dat.getDate() + 1);

		    		var retval = getDates(minDates, dat);
			    	$('.datefak-'+parent+'-'+(c+1)).datepicker('destroy').datepicker({
			    		orientation: "bottom right",
			    		todayHighlight: true,
			    		autoclose:true,
			    		format: 'dd-M-yyyy',
			    		defaultViewDate: {year:$('input[name=tahun]').val(), month:(months.indexOf(bln)), day:tgl},
			    		yearRange: '-0:+1',
			    		startDate: dat,
			    		endDate: last,
			    		hideIfNoPrevNext: true,
			    		datesDisabled: retval,
			    	});
	    		}else{
			    	$('.datefak-'+parent+'-'+(c+1)).datepicker('destroy');
			    	$('.datefak-'+parent+'-'+(c+1)).prop('readonly', true);
	    		}


		    	$('.waktu').clockpicker({
		    		autoclose:true,
		    		'default': 'now',
		    	});
		});
		$(document).on('click', '.hapus_pelaksanaan', function (e){
			var id =$(this).data("id");
			var parent =$(this).data("parent");
			var row = $(this).closest('tr');
			row.remove();

			var rowz = $('tr.detail-pelaksanaan-'+parent+'-'+id).remove();
			var table = $('#table-pelaksanaan-'+parent);
			var rows = table.find('tbody tr.detail_cek-'+parent);
			$.each(rows, function(key, value){
				function romawi(number){
					var map = [1,4,5,9,10,40,50,90,100,400,500,900,1000];  
					var roma = ["I","IV","V","IX","X","XL","L","XC","C","CD","D","CM","M"];  
					var hasil='';  

					for(var i=12; i >=0; i--) {  
						while(number >= map[i]) {  
							number = number - map[i];  
							hasil = hasil + roma[i];  
						}  
					}  
					return hasil;
				}
				console.log('waduk')
				var cekval = table.find('.cekcek-'+$(this).data("parent")+'-'+$(this).data("id")).data("val");
				for(i=0; i < 600; ++i) {
					$('.cekcek-'+$(this).data("parent")+'-'+$(this).data("id")).removeClass('datefak-'+$(this).data("parent")+'-'+i);
				}
				$('.cekcek-'+$(this).data("parent")+'-'+$(this).data("id")).addClass('datefak-'+$(this).data("parent")+'-'+key);
				$('input[name="detail[`+parent+`][pelaksanaan]['+$(this).data("id")+'][tanggal]"]').attr("data-val", key)
				table.find('.numboor-'+$(this).data("parent")+'-'+$(this).data("id")).html(romawi(key+1));

	            if(key == 0){
	            	$('.datefak-'+$(this).data("parent")+'-0').datepicker('destroy').datepicker({
			    		orientation: "bottom right",
			    		todayHighlight: true,
			    		autoclose:true,
			    		format: 'dd-M-yyyy',
			    		yearRange: '-0:+1',
			    		startDate: minDates,
			    		endDate: last,
			    		hideIfNoPrevNext: true,
			    	});
	            }else{
	            	var c1= (key-1);
	            	var c2= $('.datefak-'+$(this).data("parent")+'-'+c1).val();
		    		if(!isEmpty(c2)){
			    		var tgl = c2.split("-").slice(0)[0];
			    		var bln = c2.split("-").slice(1)[0];
			    		var thn = c2.split("-").slice(2)[0];
			    		var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
			    		var month = ('0' + (months.indexOf(bln)+1)).slice(-2);
			    		var dates = thn+'-'+month+'-'+tgl;
			    		var dat = new Date(dates);
			    		dat.setDate(dat.getDate() + 1);
		    		}else{
		    			var dat = minDates;
		    		}

		    		var retval = getDates(minDates, dat);
			    	$('.datefak-'+$(this).data("parent")+'-'+key).datepicker('destroy').datepicker({
			    		orientation: "bottom right",
			    		todayHighlight: true,
			    		autoclose:true,
			    		format: 'dd-M-yyyy',
			    		yearRange: '-0:+1',
			    		startDate: dat,
			    		endDate: last,
			    		hideIfNoPrevNext: true,
			    		datesDisabled: retval,
			    	});
	            }
			});
		});

		$(document).on('click', '.tambah_detail', function(e){
			var parent = $(this).data("parent");
			var id = $(this).data("id");
    		var last1 = parseInt($('input[name=last_pelaksanaan_'+parent+']').val());
    		var last2 = parseInt($('input[name=last_detail_'+parent+'_'+id+']').val()) + 1;
    		var table = $('#table-pelaksanaan-'+parent);
			var row = table.find('tbody tr.detail-pelaksanaan-'+parent+'-'+id).length;
			var c = row-1;
			var html = `
					<tr class="detail-pelaksanaan-`+parent+`-`+id+` detail-`+parent+`-`+id+`-`+(c+2)+`">
						<td style="text-align: center;width: 150px;" class="field">
							<input class="form-control waktu" id="waktu" name="detail[`+parent+`][pelaksanaan][`+id+`][detail][`+last2+`][mulai]" placeholder="Mulai" type="text"/>
						</td>
						<td style="text-align: center;width: 150px;" class="field">
							<input class="form-control waktu" id="waktu" name="detail[`+parent+`][pelaksanaan][`+id+`][detail][`+last2+`][selesai]" placeholder="Selesai" type="text"/>
						</td>
						<td style="text-align: center;width: 800px;" class="field">
							<input class="form-control" id="item" name="detail[`+parent+`][pelaksanaan][`+id+`][detail][`+last2+`][item]" placeholder="Item" type="text"/>
						</td>
						<td style="text-align: center;width: 50px;border-right: 1px solid rgba(34,36,38,.1);">
							<button class="btn btn-outline-danger btn-sm hapus_detail" type="button" data-id="`+id+`" data-detail="`+(c+2)+`" data-parent="`+parent+`" data-toggle="tooltip" data-placement="left" title="Hapus Item" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
						</td>
					</tr>
				`;
				$('.data_rowspan-'+parent+'-'+id).attr('rowspan', parseInt(row+2));

				if(row == 0){
					$(html).insertAfter('.data-pelaksanaan-'+parent+'-'+id).hide().show('slow');
				}else{
					$(html).insertAfter('.detail-'+parent+'-'+id+'-'+parseInt(row)).hide().show('slow');
				}
		        $('input[name=last_detail_'+parent+'_'+id+']').val(last2)
		        $('.btn').tooltip('enable');
		        var d = new Date();
		    	var full = d.getFullYear();
		    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
		    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
		    	var last = new Date(lastDate);
		    	if($('input[name=tahun]').val() > full){
		    		var min = minDates;
		    	}else{
		    		var min = d;
		    	}

		        $('.waktu').clockpicker({
		        	autoclose:true,
		        	'default': 'now',
		        });
		});
		$(document).on('click', '.hapus_detail', function (e){
			var parent = $(this).data("parent");
			var id = $(this).data("id");

	    	var table = $('#table-pelaksanaan-'+parent);
			var rowz = table.find('tbody tr.detail-pelaksanaan-'+parent+'-'+id).length;
			var detail = $(this).data("detail");
			$('.data_rowspan-'+parent+'-'+id).attr('rowspan', parseInt(rowz));
			var row = $('tr.detail-'+parent+'-'+id+'-'+detail).remove();
			row.remove();
		});

		$(document).on('click', '.tambah_tembusan', function(e){
			var rowCount = $('#tembusan > tbody > tr').length;
			var c = rowCount-1;
			var html = `
					<tr class="data-container-`+(c+2)+`" data-id="`+(c+2)+`">
						<td style="width: 20px;">
							<label style="margin-top: 7px;padding-left: 5px;" class="numboor-`+(c+2)+`">`+(c+2)+`</label>
						</td>
						<td style="border-bottom: 1px solid rgba(34,36,38,.1);" class="field">
							<select data-id="`+(c+2)+`" class="selectpicker form-control cari" name="details[`+(c+2)+`][tembusan]" data-style="btn-default" data-live-search="true" data-size="3" title="(Pilih Salah Satu)">
								@foreach(App\Models\Auths\User::where('id', '!=', 1)->get() as $user)
									<option value="{{ $user->id }}" class="cuy">{{ $user->name }}</option>
								@endforeach
							</select>  
						</td>
						<td style="width: 80px;border-bottom: 1px solid rgba(34,36,38,.1);">
						<button class="btn btn-sm btn-danger hapus_tembusan" type="button" style="border-radius:20px;" data-id=`+(c+2)+`><i class="fa fa-remove"></i></button>
						</td>
					</tr>
				`;

			$('.container-tembusan').append(html).children(':last').hide().fadeIn(1000);
		    $('.selectpicker').selectpicker();
		});

		$(document).on('click', '.hapus_tembusan', function (e){
			var row = $(this).closest('tr');
			row.remove();
			var table = $('#tembusan');
			var rows = table.find('tbody tr');

			$.each(rows, function(key, value){
				table.find('.numboor-'+$(this).data("id")).html(key+1);
			});
		});
    </script>
    @yield('js-extra')
@endpush
