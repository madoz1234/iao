@extends('layouts.form')
@section('title', 'Buat Surat Penugasan')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body" style="padding-bottom: 0px;">
		<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
			@csrf
			<div class="loading dimmer padder-v" style="display: none;">
			    <div class="loader"></div>
			</div>
			<input type="hidden" name="id" value="{{ $record->id }}">
			<input type="hidden" name="tahun" value="{{ $record->rencanaaudit->tahun }}">
			<input type="hidden" name="status" value="0">
			<table class="table" style="font-size: 12px;">
				<tbody>
					<tr>
						<td style="width:200px;border: 1px #ffffff;border: 1px #ffffff;">Tahun</td>
						<td style="width:2px;border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;" class="field">
							{{ $record->rencanaaudit->tahun }}
						</td>
					</tr>
					<tr>
						<td style="border: 1px #ffffff;">Rencana Pelaksanaan</td>
						<td style="border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;" class="field">
							{{ $record->rencana }}
						</td>
					</tr>
					<tr>
						<td style="border: 1px #ffffff;">Tipe</td>
						<td style="border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;" class="field">
							@if($record->rencanaaudit->tipe == 0)
								<span class="label label-primary">Audit Reguler</span>
							@else
								<span class="label label-success">Audit Khusus</span>
							@endif
						</td>
					</tr>
					<tr>
						<td style="border: 1px #ffffff;">Kategori</td>
						<td style="border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;" class="field">
								@if($record->tipe_object == 0)
									<span class="label label-success">Business Unit (BU)</span>
								@elseif($record->tipe_object == 1)
									<span class="label label-info">Corporate Office (CO)</span>
								@elseif($record->tipe_object == 2)
									<span class="label label-default">Project</span>
								@else
									<span class="label label-primary">Anak Perusahaan</span>
								@endif
						</td>
					</tr>
					<tr class="detil">
						<td style="border: 1px #ffffff;">Objek Audit</td>
						<td style="border: 1px #ffffff;">:</td>
						<td style="border: 1px #ffffff;">
							{{ object_audit($record->tipe_object, $record->object_id) }}
						</td>
					</tr>
				</tbody>
			</table>
			<div class="form-row">
				<div class="form-group col-md-12">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="a-tab" data-toggle="tab" href="#a" role="tab" aria-controls="home" aria-selected="true">A. TIM</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="b-tab" data-toggle="tab" href="#b" role="tab" aria-controls="messages" aria-selected="false">B. JADWAL</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="a" role="tabpanel" aria-labelledby="a-tab">
							<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
								<tr>
									<td style="width: 10%;border: 1px #ffffff;">Ketua Tim</td>
									<td style="width: 1%;border: 1px #ffffff;">:</td>
									<td scope="col-md-12" colspan="3" style="text-align: left;" class="field">
										<select data-width="20%" class="selectpicker form-control" name="ketua" data-size="3" data-style="btn-default" data-live-search="true" title="(Pilih salah satu)">
											@foreach(App\Models\Auths\User::whereHas('roles', function($z){
									                	$z->where('name', ['auditor']);
									                })->get() as $user)
												<option value="{{ $user->id }}">{{ $user->name }}</option>
											@endforeach
										</select>
									</td>
								</tr>
								<tr>
									<td style="width: 10%;border: 1px #ffffff;">Auditor Operasional</td>
									<td style="width: 1%;border: 1px #ffffff;">:</td>
									<td scope="col-md-12" colspan="3" style="text-align: left;" class="field">
										<div>
											<select data-width="20%" class="selectpicker form-control" name="operasional[]" data-size="3" data-style="btn-default" data-live-search="true" multiple title="(Pilih salah satu atau beberapa)">
												@foreach(App\Models\Auths\User::where('fungsi', 1)->get() as $user)
													<option value="{{ $user->id }}">{{ $user->name }}</option>
												@endforeach
											</select>
										</div>
									</td>
								</tr>
								<tr>
									<td style="width: 10%;border: 1px #ffffff;">Auditor Keuangan</td>
									<td style="width: 1%;border: 1px #ffffff;">:</td>
									<td scope="col-md-12" colspan="3" style="text-align: left;" class="field">
										<div>
											<select data-width="20%" class="selectpicker form-control" name="keuangan[]" data-size="3" data-style="btn-default" data-live-search="true" multiple title="(Pilih salah satu atau beberapa)">
												@foreach(App\Models\Auths\User::where('fungsi', 2)->get() as $user)
													<option value="{{ $user->id }}">{{ $user->name }}</option>
												@endforeach
											</select>
										</div>
									</td>
								</tr>
								<tr>
									<td style="width: 10%;border: 1px #ffffff;">Auditor Sistem</td>
									<td style="width: 1%;border: 1px #ffffff;">:</td>
									<td scope="col-md-12" colspan="3" style="text-align: left;" class="field">
										<div>
											<select data-width="20%" class="selectpicker form-control" name="sistem[]" data-size="3" data-style="btn-default" data-live-search="true" multiple title="(Pilih salah satu atau beberapa)">
												@foreach(App\Models\Auths\User::where('fungsi', 3)->get() as $user)
													<option value="{{ $user->id }}">{{ $user->name }}</option>
												@endforeach
											</select>
										</div>
									</td>
								</tr>
							</table>
							<div class="form-group col-md-12">
								<div class="text-right">
									<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
									<button type="button" class="btn btn-info page1">Selanjutnya</button>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="b" role="tabpanel" aria-labelledby="b-tab">
							<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
								<tr>
									<td scope="col-md-12" colspan="3" style="text-align: left;">
										<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
											<tr style="text-align: center;">
												<td scope="col" style="text-align: left;border: 1px #ffffff;">
													<table id="table-pelaksanaan" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
														<thead>
															<tr>
																<th style="text-align: center;" rowspan="2">Hari</th>
																<th style="text-align: center;" rowspan="2">Tanggal</th>
																<th style="text-align: center; text-align: center;" colspan="2">Waktu</th>
																<th style="text-align: center;border-left: 1px solid rgba(34,36,38,.1);" rowspan="2">Item</th>
																<th style="text-align: center;width: 50px;border-right: 1px solid rgba(34,36,38,.1);border-top: 1px solid rgba(34,36,38,.1);" rowspan="2" colspan="2">
																	<button class="btn btn-sm btn-success tambah_pelaksanaan" data-toggle="tooltip" data-placement="left" title="Tambah Pelaksanaan" type="button" style="border-radius: 20px;"><i class="fa fa-plus"></i></button>
																</th>
															</tr>
															<tr>
																<th style="text-align: center;border-top: 1px solid rgba(34,36,38,.1);">Mulai</th>
																<th style="text-align: center;border-top: 1px solid rgba(34,36,38,.1);">Selesai</th>
															</tr>
														</thead>
														<tbody class="container-pelaksanaan">
															<tr class="detail_cek data-pelaksanaan-0" data-id="0">
																<td class="data_rowspan-0 numboor-0" style="text-align: center;width: 100px;" rowspan="">{{ romawi(1) }}</td>
																<td class="data_rowspan-0 field" style="text-align: center;width: 200px;" rowspan="">
																	<input class="form-control datefak" id="tanggal" name="detail[0][pelaksanaan][0][tanggal]" placeholder="Tanggal" type="text" onChange="disabledKoplok(this)"/>
																</td>
																<td style="text-align: center;width: 150px;" class="field">
																	<input class="form-control waktu" id="waktu" name="detail[0][pelaksanaan][0][detail][0][mulai]" placeholder="Mulai" type="text"/>
																</td>
																<td style="text-align: center;width: 150px;" class="field">
																	<input class="form-control waktu" id="waktu" name="detail[0][pelaksanaan][0][detail][0][selesai]" placeholder="Selesai" type="text"/>
																</td>
																<td style="text-align: center;width: 800px;" class="field">
																	<input class="form-control" id="item" name="detail[0][pelaksanaan][0][detail][0][item]" placeholder="Item" type="text"/>
																</td>
																<td style="text-align: center;width: 50px;">
																	<button class="btn btn-outline-success btn-sm tambah_detail" type="button" data-toggle="tooltip" data-placement="left" data-id="0" title="Tambah Item" style="border-radius: 20px;"><i class="fa fa-plus"></i></button>
																</td>
																<td class="data_rowspan-0" style="text-align: center;width: 50px;border-bottom: 1px solid rgba(34,36,38,.1);" rowspan="">
																</td>
																<input type="hidden" name="last_pelaksanaan" value="0">
																<input type="hidden" name="last_detail_0" value="0">
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<div class="form-group col-md-12">
								<div class="text-right">
									<button type="button" class="btn btn-cancel kembali1">Sebelumnya</button>
									<button type="button" class="btn btn-simpan save as drafting">Simpan</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
	    .datepicker.dropdown-menu {
		    z-index: 9999 !important;
		}
    </style>
@endpush
@push('scripts')
    <script> 
    	var dateDis = [];
    	function disabledKoplok(element)
    	{
    		dateDis = [];
    		$('.datefak').each(function (index, el) {
    			dateDis.push($(el).val());
    		})

    		console.log(dateDis)

    		$('.datefak').datepicker('destroy').datepicker({
    			orientation: "bottom right",
    			todayHighlight: true,
    			autoclose:true,
    			format: 'dd-M-yyyy',
    			yearRange: '-0:+1',
    			startDate: minDates,
    			endDate: last,
    			hideIfNoPrevNext: true,
    			datesDisabled: dateDis
    		});
    	}

    	$(document).on('click', '.page1', function (e){
    		$('#a-tab').removeClass('active');
    		$('#a-tab').closest('li').removeClass('active');
    		$('#b-tab').addClass('active');
    		$('#b-tab').closest('li').addClass('active');
    		$('#a').removeClass('active');
    		$('#b').addClass('active');
		});

		$(document).on('click', '.kembali1', function (e){
    		$('#a-tab').addClass('active');
    		$('#a-tab').closest('li').addClass('active');
    		$('#b-tab').removeClass('active');
    		$('#b-tab').closest('li').removeClass('active');
    		$('#a').addClass('active');
    		$('#b').removeClass('active');
		});

    	$('#myTab li:first-child a').tab('show')
    	function romawi(number){
        	var map = [1,4,5,9,10,40,50,90,100,400,500,900,1000];  
        	var roma = ["I","IV","V","IX","X","XL","L","XC","C","CD","D","CM","M"];  
        	var hasil='';  

        	for(var i=12; i >=0; i--) {  
        		while(number >= map[i]) {  
        			number = number - map[i];  
        			hasil = hasil + roma[i];  
        		}  
        	}  
		    return hasil;
        }

        var d = new Date();
    	var full = d.getFullYear();
    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
    	var last = new Date(lastDate);
    	if($('input[name=tahun]').val() > full){
    		var min = minDates;
    	}else{
    		var min = d;
    	}

    	$('.datefak').datepicker({
    		orientation: "bottom right",
    		todayHighlight: true,
    		autoclose:true,
    		format: 'dd-M-yyyy',
    		yearRange: '-0:+1',
    		startDate: minDates,
    		endDate: last,
    		hideIfNoPrevNext: true,
    	});

        $('.waktu').clockpicker({
        	autoclose:true,
        	orientation: "bottom",
        	'default': 'now',
        });

    	$(document).on('click', '.tambah_pelaksanaan', function(e){
    		var last = parseInt($('input[name=last_pelaksanaan]').val()) + 1;
			var rowCount = $('#table-pelaksanaan > tbody > tr.detail_cek').length;
			var c = rowCount-1;
			var html = `
					<tr class="detail_cek data-pelaksanaan-`+(c+2)+`" data-id=`+(c+2)+`>
						<td class="data_rowspan-`+(c+2)+` numboor-`+(c+2)+`" style="text-align: center;width: 100px;" rowspan="">`+romawi(c+2)+`</td>
						<td class="data_rowspan-`+(c+2)+` field" style="text-align: center;width: 200px;" rowspan="">
							<input class="form-control datefak" id="tanggal" name="detail[0][pelaksanaan][`+(c+2)+`][tanggal]" placeholder="Tanggal" type="text" onChange="disabledKoplok(this)"/>
						</td>
						<td style="text-align: center;width: 150px;" class="field">
							<input class="form-control waktu" id="waktu" name="detail[0][pelaksanaan][`+(c+2)+`][detail][0][mulai]" placeholder="Mulai" type="text"/>
						</td>
						<td style="text-align: center;width: 150px;" class="field">
							<input class="form-control waktu" id="waktu" name="detail[0][pelaksanaan][`+(c+2)+`][detail][0][selesai]" placeholder="Selesai" type="text"/>
						</td>
						<td style="text-align: center;width: 800px;" class="field">
							<input class="form-control" id="item" name="detail[0][pelaksanaan][`+(c+2)+`][detail][0][item]" placeholder="Item" type="text"/>
						</td>
						<td style="text-align: center;width: 50px;">
							<button class="btn btn-outline-success btn-sm tambah_detail" type="button" data-id="`+(c+2)+`" data-toggle="tooltip" data-placement="left" title="Tambah Item" style="border-radius: 20px;"><i class="fa fa-plus"></i></button>
						</td>
						<td class="data_rowspan-`+(c+2)+`" style="text-align: center;width: 50px;" rowspan="">
							<button class="btn btn-sm btn-danger hapus_pelaksanaan" type="button" data-id="`+(c+2)+`" data-toggle="tooltip" data-placement="left" title="Hapus Pelaksanaan" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
						</td>
						<input type="hidden" name="last_detail_`+(c+2)+`" value="0">
					</tr>
				`;

				$('.container-pelaksanaan').append(html);
		        $('input[name=last_pelaksanaan]').val((c+2))
		        $('.btn').tooltip('enable');
		        var d = new Date();
		    	var full = d.getFullYear();
		    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
		    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
		    	var last = new Date(lastDate);
		    	if($('input[name=tahun]').val() > full){
		    		var min = minDates;
		    	}else{
		    		var min = d;
		    	}

		    	$('.datefak').datepicker('destroy').datepicker({
		    		orientation: "bottom right",
		    		todayHighlight: true,
		    		autoclose:true,
		    		format: 'dd-M-yyyy',
		    		yearRange: '-0:+1',
		    		startDate: minDates,
		    		endDate: last,
		    		hideIfNoPrevNext: true,
		    		datesDisabled: dateDis
		    	});

		    	$('.waktu').clockpicker({
		    		autoclose:true,
		    		'default': 'now',
		    	});
		});
		$(document).on('click', '.hapus_pelaksanaan', function (e){
			var id =$(this).data("id");
			var row = $(this).closest('tr');
			row.remove();

			var rowz = $('tr.detail-pelaksanaan-'+id).remove();
			var table = $('#table-pelaksanaan');
			var rows = table.find('tbody tr.detail_cek');

			$.each(rows, function(key, value){
				function romawi(number){
					var map = [1,4,5,9,10,40,50,90,100,400,500,900,1000];  
					var roma = ["I","IV","V","IX","X","XL","L","XC","C","CD","D","CM","M"];  
					var hasil='';  

					for(var i=12; i >=0; i--) {  
						while(number >= map[i]) {  
							number = number - map[i];  
							hasil = hasil + roma[i];  
						}  
					}  
					return hasil;
				}
				table.find('.numboor-'+$(this).data("id")).html(romawi(key+1));
			});
		});

		$(document).on('click', '.tambah_detail', function(e){
			var id = $(this).data("id");
    		var last1 = parseInt($('input[name=last_pelaksanaan]').val());
    		var last2 = parseInt($('input[name=last_detail_'+id+']').val()) + 1;
			var row = $('#table-pelaksanaan > tbody > tr.detail-pelaksanaan-'+id).length;
			var c = row-1;
			var html = `
					<tr class="detail-pelaksanaan-`+id+` detail-`+id+`-`+(c+2)+`">
						<td style="text-align: center;width: 150px;" class="field">
							<input class="form-control waktu" id="waktu" name="detail[0][pelaksanaan][`+id+`][detail][`+last2+`][mulai]" placeholder="Mulai" type="text"/>
						</td>
						<td style="text-align: center;width: 150px;" class="field">
							<input class="form-control waktu" id="waktu" name="detail[0][pelaksanaan][`+id+`][detail][`+last2+`][selesai]" placeholder="Selesai" type="text"/>
						</td>
						<td style="text-align: center;width: 800px;" class="field">
							<input class="form-control" id="item" name="detail[0][pelaksanaan][`+id+`][detail][`+last2+`][item]" placeholder="Item" type="text"/>
						</td>
						<td style="text-align: center;width: 50px;border-right: 1px solid rgba(34,36,38,.1);">
							<button class="btn btn-outline-danger btn-sm hapus_detail" type="button" data-id="`+id+`" data-detail="`+(c+2)+`" data-toggle="tooltip" data-placement="left" title="Hapus Item" style="border-radius:20px;"><i class="fa fa-remove"></i></button>
						</td>
					</tr>
				`;
				$('.data_rowspan-'+id).attr('rowspan', parseInt(row+2));

				if(row == 0){
					$(html).insertAfter('.data-pelaksanaan-'+id).hide().show('slow');
				}else{
					$(html).insertAfter('.detail-'+id+'-'+parseInt(row)).hide().show('slow');
				}
		        $('input[name=last_detail_'+id+']').val(last2)
		        $('.btn').tooltip('enable');
		        var d = new Date();
		    	var full = d.getFullYear();
		    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
		    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
		    	var last = new Date(lastDate);
		    	if($('input[name=tahun]').val() > full){
		    		var min = minDates;
		    	}else{
		    		var min = d;
		    	}

		        $('.waktu').clockpicker({
		        	autoclose:true,
		        	'default': 'now',
		        });
		});
		$(document).on('click', '.hapus_detail', function (e){
			var id = $(this).data("id");
			var rowz = $('#table-pelaksanaan > tbody > tr.detail-pelaksanaan-'+id).length;
			var detail = $(this).data("detail");
			$('.data_rowspan-'+id).attr('rowspan', parseInt(rowz));
			var row = $('tr.detail-'+id+'-'+detail).remove();
			row.remove();
		});
    </script>
    @yield('js-extra')
@endpush
