<form action="{{ route($routes.'.saveFinal', $record->id) }}" method="POST" id="formData" class="was-validated">
    @method('PATCH')
    @csrf
    <div class="loading dimmer padder-v" style="display: none;">
	    <div class="loader"></div>
	</div>
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Upload Final Surat Penugasan Tahun {{ $record->rencanadetail->rencanaaudit->tahun }}</h5>
    </div>
    <div class="modal-body">
    	<input type="hidden" name="id" value="{{ $record->id }}">
    	<input type="hidden" name="tahun" value="{{ $record->rencanadetail->rencanaaudit->tahun }}">
    	<table class="table" style="font-size: 12px;">
			<tbody>
				<tr>
					<td style="width:150px;border: 1px #ffffff;border: 1px #ffffff;">Tahun</td>
					<td style="border: 1px #ffffff;padding-right: 10px;">:</td>
					<td style="border: 1px #ffffff;padding-left: 0px;" class="field">
						@php 
							$tahun=array();
						@endphp
						@foreach($data as $thn)
							@php 
								$tahun[]= $thn->rencanadetail->rencanaaudit->tahun;
							@endphp
						@endforeach
						{{ implode(", ", array_unique($tahun))}}
					</td>
				</tr>
				<tr>
					<td style="border: 1px #ffffff;">Rencana Pelaksanaan</td>
					<td style="border: 1px #ffffff;padding-right: 10px;">:</td>
					<td style="border: 1px #ffffff;padding-left: 0px;" class="field">
						@php 
							$rencana=array();
						@endphp
						@foreach($data as $rnc)
							@php 
								$rencana[]= $rnc->rencanadetail->rencana;
							@endphp
						@endforeach
						{{ implode(", ", $rencana)}}
					</td>
				</tr>
				<tr>
					<td style="border: 1px #ffffff;">Tipe</td>
					<td style="border: 1px #ffffff;padding-right: 10px;">:</td>
					<td style="border: 1px #ffffff;padding-left: 0px;" class="field">
						@php 
							$tipe=array();
						@endphp
						@foreach($data as $tip)
							@php 
							if($tip->rencanadetail->tipe == 0){
								$tipe[]= '<span class="label label-primary">Audit</span>';
							}elseif($tip->rencanadetail->tipe == 1){
								$tipe[]= '<span class="label label-success">Kegiatan Konsultasi</span>';
							}else{
								$tipe[]= '<span class="label label-info">Kegiatan Audit Lain - Lain</span>';
							}
							@endphp
						@endforeach
						{!! implode(" - ", array_unique($tipe))!!}
					</td>
				</tr>
				<tr>
					<td style="border: 1px #ffffff;">Kategori</td>
					<td style="border: 1px #ffffff;padding-right: 10px;">:</td>
					<td style="border: 1px #ffffff;padding-left: 0px;" class="field">
						@php 
							$kategori=array();
						@endphp
						@foreach($data as $kat)
							@php
							if($kat->rencanadetail->tipe == 0) {
								if($kat->rencanadetail->tipe_object == 0){
									$kategori[]= '<span class="label label-success">Business Unit (BU)</span>';
								}elseif($kat->rencanadetail->tipe_object == 1){
									$kategori[]= '<span class="label label-info">Corporate Office (CO)</span>';
								}elseif($kat->rencanadetail->tipe_object == 2){
									$kategori[]= '<span class="label label-default">Project</span>';
								}else{
									$kategori[]= '<span class="label label-primary">Anak Perusahaan</span>';
								}
							}elseif($kat->rencanadetail->tipe == 1) {
								$kategori[]= '<span class="label label-warning">'.$kat->rencanadetail->konsultasi->nama.'</span>';
							}else{
								$kategori[]= '<span class="label label-default">'.$kat->rencanadetail->lain->nama.'</span>';
							}
							@endphp
						@endforeach
						{!! implode(" - ", array_unique($kategori))!!}
					</td>
				</tr>
				<tr class="detil">
					<td style="border: 1px #ffffff;">Objek Audit</td>
					<td style="border: 1px #ffffff;padding-right: 10px;">:</td>
					<td style="border: 1px #ffffff;padding-left: 0px;">
						@php 
							$object=array();
						@endphp
						@foreach($data as $obj)
							@php 
								$object[]= object_audit($obj->rencanadetail->tipe_object, $obj->rencanadetail->object_id);
							@endphp
						@endforeach
						<table>
							<tbody>
								@foreach(array_unique($object) as $koy => $ob)
									@if($ob)
										<tr>
											<td style="font-size: 10px; padding-right: 10px;">{{ $koy+1 }}.</td>
											<td style="text-align: justify;">{{ $ob }}</td>
										</tr>
									@endif
								@endforeach
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
    	<div class="form-group field">
            <label class="control-label">No Surat Penugasan Audit</label>
            <input type="text" name="no_surat" class="form-control" placeholder="No Surat Penugasan Audit" required="" value="{{$record->no_surat}}">
        </div>
        <div class="form-group field">
            <label class="control-label">Tgl Surat</label>
            <input type="text" name="tgl_surat" class="form-control tanggal" placeholder="Tgl Surat" required="">
        </div>
        <div class="form-group field">
            <label class="control-label">Surat Penugasan Audit</label>
            <div class="file-loading">
            	<input id="pdf" name="surat" type="file" class="file form-control" 
        data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf">
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script>

    </script>
    @yield('js-extra')
@endpush