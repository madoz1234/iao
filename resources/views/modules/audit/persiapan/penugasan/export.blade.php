<style>
	table{
		border:silver 3px solid;
	}
	table td{
		border-bottom:silver 3px solid;
		border-right:silver 3px solid;
		padding:0 0 0 5px;
	}
</style>
<table border="1" cellpadding="5" style="border:1em solid black;">
    <thead>
        <tr>
            <th colspan="8" style="text-align: left; font-size: 12px;font-family: 'Tahoma'; font-weight: bold;">{{ $record['tittle'] }}</th>
        </tr>
        <tr>
            <th colspan="8" style="text-align: left; font-size: 12px;font-family: 'Tahoma'; font-weight: bold;">Tanggal Audit : {{ DateToString($record['data']->jadwal->min('tgl')).' - '.DateToString($record['data']->jadwal->max('tgl')) }}</th>
        </tr>
        <tr>
			<th style="width:5px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';" rowspan="2">NO</th>
			<th style="width:300px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';" colspan="2" rowspan="2">RANGKAIAN KEGIATAN AUDIT</th>
			<th style="width:25px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';" rowspan="2">UNIT USAHA</th>
			<th style="width:25px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';" rowspan="2">HARI</th>
			<th style="width:25px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';" rowspan="2">TANGGAL</th>
			<th style="width:20px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';" colspan="2">WAKTU</th>
			<th style="width:80px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';" rowspan="2">KETERANGAN</th>
		</tr>
		<tr>
			<th style="width:20px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';">MULAI</th>
			<th style="width:20px;text-align: center;font-weight: bold;background-color:#dedddd;font-size: 12px;font-family: 'Tahoma';">SELESAI</th>
		</tr>
    </thead>
    <tbody>
    	@php 
    		$i=1;
    	@endphp
    	@foreach($record['data']->jadwal->groupBy('tgl_id') as $a => $cek)
		    <tr>
				<td style="font-weight: bold;background-color: #ececec;font-size: 12px;font-family: 'Tahoma';">{{ $i }}</td>
				<td style="font-weight: bold;background-color: #ececec;font-size: 12px;font-family: 'Tahoma';" colspan="2">{{ object_audit($cek->first()->rencanadetail->tipe_object, $cek->first()->rencanadetail->object_id) }} -	AB : {{ getAb($cek->first()->rencanadetail->tipe_object, $cek->first()->rencanadetail->object_id) }}</td>
				<td style="text-align: center;font-weight: bold;background-color: #ececec;font-size: 12px;font-family: 'Tahoma';">
					@if($cek->first()->rencanadetail->tipe_object == 0)
						Business Unit (BU)
					@elseif($cek->first()->rencanadetail->tipe_object == 1)
						Corporate Office (CO)
					@elseif($cek->first()->rencanadetail->tipe_object == 2)
						{{ getParentBuNama($cek->first()->rencanadetail->object_id) }}
					@else 
						Anak Perusahaan
					@endif
				</td>
				<td style="background-color: #ececec;font-size: 12px;font-family: 'Tahoma';">&nbsp;</td>
				<td style="background-color: #ececec;font-size: 12px;font-family: 'Tahoma';">&nbsp;</td>
				<td style="background-color: #ececec;font-size: 12px;font-family: 'Tahoma';">&nbsp;</td>
				<td style="background-color: #ececec;font-size: 12px;font-family: 'Tahoma';">&nbsp;</td>
				<td style="background-color: #ececec;font-size: 12px;font-family: 'Tahoma';">Auditor :</td>
			</tr>
			@php 
				$j=0;
				$row =0;
				foreach($record['data']->jadwal->where('tgl_id', $cek->first()->tgl_id) as $kiy => $pelaksanaan){
					$row+= $pelaksanaan->detail->count();
				}
			@endphp
			@foreach($record['data']->jadwal->where('tgl_id', $cek->first()->tgl_id) as $kiy => $pelaksanaan)
				<tr>
					<td style="font-size: 12px;font-family: 'Tahoma';">&nbsp;</td>
					<td style="width: 10px;text-align: center;font-size: 12px;font-family: 'Tahoma';" valign="middle" rowspan="{{$pelaksanaan->detail->count()}}">HARI {{ romawi(($j+1)) }}</td>
					<td style="width: 90px;font-size: 12px;font-family: 'Tahoma';">{{ $pelaksanaan->detail->first()->keterangan }}</td>
					<td style="">&nbsp;</td>
					<td style="text-align: center;font-size: 12px;font-family: 'Tahoma';" rowspan="{{$pelaksanaan->detail->count()}}">{{DayOf($pelaksanaan->tgl)}}</td>
					<td style="text-align: center;font-size: 12px;font-family: 'Tahoma';" rowspan="{{$pelaksanaan->detail->count()}}">{{DateToStringWday($pelaksanaan->tgl)}}</td>
					<td style="text-align: center;font-size: 12px;font-family: 'Tahoma';">{{ $pelaksanaan->detail->first()->mulai }}</td>
					<td style="text-align: center;font-size: 12px;font-family: 'Tahoma';">{{ $pelaksanaan->detail->first()->selesai }}</td>
					@if($j==0)
						@php 
							$data_operasional = array();
							foreach($record['data']->anggota->where('fungsi', 1)->where('data_id', $cek->first()->tgl_id) as $operasional) {
								if($record['data']->anggota->where('fungsi', 0)->where('data_id', $cek->first()->tgl_id)->first()->user->inisial == $operasional->user->inisial){

								}else{
								 $data_operasional[] = $operasional->user->inisial;
								}
							}

							$data_keuangan = array();
							foreach($record['data']->anggota->where('fungsi', 2)->where('data_id', $cek->first()->tgl_id) as $keuangan) {
							 	if($record['data']->anggota->where('fungsi', 0)->where('data_id', $cek->first()->tgl_id)->first()->user->inisial == $keuangan->user->inisial){

								}else{
								 $data_keuangan[] = $keuangan->user->inisial;
								}
							}

							$data_sistem = array();
							foreach($record['data']->anggota->where('fungsi', 3)->where('data_id', $cek->first()->tgl_id) as $sistem) {
							 	if($record['data']->anggota->where('fungsi', 0)->where('data_id', $cek->first()->tgl_id)->first()->user->inisial == $sistem->user->inisial){

								}else{
								 $data_sistem[] = $sistem->user->inisial;
								}
							}
							$result = array_unique(array_merge($data_operasional, $data_keuangan, $data_sistem));
							$ketua = $record['data']->anggota->where('fungsi', 0)->where('data_id', $cek->first()->tgl_id)->first()->user->inisial;
							$i=2;
						@endphp
						<td rowspan="{{ $row }}" @if($row < 4) style="height: 80px;font-size: 12px;font-family: 'Tahoma';" @endif>
							@if($record['data']->anggota->where('fungsi', 0)->where('data_id', $cek->first()->tgl_id)->first())
								1. {{ $record['data']->anggota->where('fungsi', 0)->where('data_id', $cek->first()->tgl_id)->first()->user->inisial }} : Ketua @if(in_array($ketua, $result)) merangkap anggota @endif<br>
							@else 
								1. - <br>
							@endif
							@foreach($result as $anggota)
								{{$i}}. {{ $anggota }} : Anggota <br>
								@php 
									$i++;
								@endphp
							@endforeach
						</td>
					@endif
				</tr>
				@php 
					$k=0;
				@endphp
				@foreach($pelaksanaan->detail as $koy => $detail)
					@if($k > 0)
						<tr>
							<td style="font-size: 12px;font-family: 'Tahoma';">&nbsp;</td>
							<td style="font-size: 12px;font-family: 'Tahoma';">{{ $detail->keterangan }}</td>
							<td style="font-size: 12px;font-family: 'Tahoma';"></td>
							<td style="text-align: center;font-size: 12px;font-family: 'Tahoma';">{{ $detail->mulai }}</td>
							<td style="text-align: center;font-size: 12px;font-family: 'Tahoma';">{{ $detail->selesai }}</td>
							<td style="font-size: 12px;font-family: 'Tahoma';">&nbsp;</td>
						</tr>
					@endif
					@php 
						$k++;
					@endphp
				@endforeach
				@php 
		    		$j++;
		    	@endphp
			@endforeach
			@php 
	    		$i++;
	    	@endphp
    	@endforeach
        <tr>
            <th colspan="7" rowspan="8" style="text-align: right; font-size: 12px;font-family: 'Tahoma'; font-weight: bold;">SVP Internal Audit<br style="mso-data-placement:same-cell;"/><br style="mso-data-placement:same-cell;"/><br style="mso-data-placement:same-cell;"/><br style="mso-data-placement:same-cell;"/><br style="mso-data-placement:same-cell;"/><br style="mso-data-placement:same-cell;"/>Pius Sutrisno Riyanto</th>
        </tr>
    </tbody>
</table>