@extends('layouts.list-penugasan')

@section('title', 'Surat Penugasan')

@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection

@section('filters')
<div class="form-group">
    <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
    <label class="control-label sr-only" for="filter-tahun">Tahun</label>
    <input type="text" class="form-control filter-control tahun" name="filter[tahun]" data-post="tahun" placeholder="Tahun">
    <label class="control-label sr-only" for="filter-tipe">Tipe</label>
    <select class="select selectpicker filter-control" name="filter[tipe]" data-post="tipe" data-style="btn-default">
        <option style="font-size: 12px;" value="">Tipe</option>
        <option style="font-size: 12px;" value="1">Audit</option>
        <option style="font-size: 12px;" value="2">Kegiatan Konsultasi</option>
        <option style="font-size: 12px;" value="3">Kegiatan Lain - Lain</option>
    </select>
    <label class="control-label sr-only" for="filter-kategori">Kategori</label>
    <select class="select selectpicker filter-control kategori" name="filter[kategori]" data-post="kategori" data-style="btn-default">
           <option style="font-size: 12px;" value="">Kategori</option>
           <option style="font-size: 12px;" value="1">Business Unit (BU)</option>
           <option style="font-size: 12px;" value="2">Corporate Office (CO)</option>
           <option style="font-size: 12px;" value="3">Project</option>
           <option style="font-size: 12px;" value="4">Anak Perusahaan</option>
    </select>
    <label class="control-label sr-only" for="filter-object_id">Objek Audit</label>
    <select class="select selectpicker filter-control show-tick object" name="filter[object_id]" data-post="object_id" data-size="3" data-style="btn-default" data-live-search="true" title="Objek Audit">
	</select>
</div>
@endsection
@section('tables')
	<ul class="nav nav-tabs" id="myTab" role="tablist" style="margin-bottom: 7px;">
		<li class="nav-item tab-a">
			<a class="nav-link active" id="a-tab" data-toggle="tab" href="#a" role="tab" aria-selected="true">Baru &nbsp;&nbsp;<span class="badge badge-sm down bg-danger pull-right-xs for-isi1" style="display:none;">{{$nbaru}}</span></a>
		</li>
		<li class="nav-item tab-b">
			<a class="nav-link" id="b-tab" data-toggle="tab" href="#b" role="tab" aria-selected="false">On Progress &nbsp;&nbsp;<span class="badge badge-sm down bg-danger pull-right-xs for-isi2" style="display:none;">{{$nprog}}</span></a>
		</li>
		<li class="nav-item tab-c">
			<a class="nav-link" id="c-tab" data-toggle="tab" href="#c" role="tab" aria-selected="false">Historis</a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="a" role="tabpanel" aria-labelledby="a-tab">
			<div class="table-responsive">
				@if(isset($structs['listStruct']))
					<table id="dataTable1" class="table table-bordered m-t-none" style="width: 100%">
						<thead>
							<tr>
								@foreach ($structs['listStruct'] as $struct)
								<th class="text-center v-middle">{{ $struct['label'] }}</th>
								@endforeach
							</tr>
						</thead>
						<tbody>
							@yield('tableBody')
						</tbody>
					</table>
				@endif
			</div>
		</div>
		<div class="tab-pane active" id="b" role="tabpanel" aria-labelledby="b-tab">
			<div class="table-responsive">
				@if(isset($structs['listStruct2']))
					<table id="dataTable2" class="table table-bordered m-t-none" style="width: 100%">
						<thead>
							<tr>
								@foreach ($structs['listStruct2'] as $struct)
								<th class="text-center v-middle">{{ $struct['label'] }}</th>
								@endforeach
							</tr>
						</thead>
						<tbody>
							@yield('tableBody')
						</tbody>
					</table>
				@endif
			</div>
		</div>
		<div class="tab-pane active" id="c" role="tabpanel" aria-labelledby="c-tab">
			<div class="table-responsive">
				@if(isset($structs['listStruct3']))
					<table id="dataTable3" class="table table-bordered m-t-none" style="width: 100%">
						<thead>
							<tr>
								@foreach ($structs['listStruct3'] as $struct)
								<th class="text-center v-middle">{{ $struct['label'] }}</th>
								@endforeach
							</tr>
						</thead>
						<tbody>
							@yield('tableBody')
						</tbody>
					</table>
				@endif
			</div>
		</div>
	</div>
@endsection
@push('styles')
    <style>
        .disabled {
	        pointer-events: none;
	        cursor: default;
	        text-decoration: none;
	        opacity: 0.5;
	    }
    </style>
@endpush
@push('scripts')
    <script> 
    	$('.tahun').datepicker({
            format: "yyyy",
		    viewMode: "years", 
		    minViewMode: "years",
            orientation: "auto",
            autoclose:true
        });

        $('#myTab li:first-child a').tab('show');
    	$(document).on('click', '.tab-a', function (e){
    		$('.buat.button').show();
    		dt1 = $('#dataTable1').DataTable();
    		dt1.draw();
		}); 
		$(document).on('click', '.tab-b', function (e){
			$('.buat.button').hide();
    		dt2 = $('#dataTable2').DataTable();
    		dt2.draw();
		}); 
		$(document).on('click', '.tab-c', function (e){
			$('.buat.button').hide();
    		dt3 = $('#dataTable3').DataTable();
    		dt3.draw();
		}); 
        
    	$('.kategori').on('change', function(){
			$.ajax({
				url: '{{ url('ajax/option/get-kategoris') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					kategori: this.value
				},
			})
			.done(function(response) {
				$('select[name="filter[object_id]"]').html(response);
				$('select[name="filter[object_id]"]').selectpicker("refresh");
			})
			.fail(function() {
				console.log("error");
			});
		});

    	$(document).on('click', '.ubah', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route('kegiatan-audit.persiapan.penugasan.index') !!}/" + idx + '/edit';
            window.location = url;
		});

		// $(document).on('click', '.buat', function (e){
  //   		var idx = $(this).data('id');
  //       	var url = "{!! route('kegiatan-audit.persiapan.penugasan.index') !!}/" + idx + '/buat';
  //           window.location = url;
		// });

		$(document).on('click', '.detil-project', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/detilProject';
            window.location = url;
		});

		$(document).on('click', '.detil-penugasan', function (e){
    		var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/detilPenugasan';
            window.location = url;
		});

		$(document).on('click', '.cetak-audit.button', function(e){
            var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/cetakAudit';
            window.open(url, '_blank');
        });

    	$('#myTab li:first-child a').tab('show');
    	$(document).on('click', '.tab-a', function (e){
    		$('.approve').show();
    		dt1 = $('#dataTable1').DataTable();
    		dt1.draw();
		}); 
		$(document).on('click', '.tab-b', function (e){
			$('.approve').hide();
    		dt2 = $('#dataTable2').DataTable();
    		dt2.draw();
		}); 
		$(document).on('click', '.tab-c', function (e){
			$('.approve').hide();
    		dt3 = $('#dataTable3').DataTable();
    		dt3.draw();
		});

        $(document).on('click', '.approve-save.button', function(e){
        	swal({
					title: "Apakah Anda yakin?",
					text: "Data yang sudah disetujui, tidak dapat diubah!",
					icon: "warning",
					buttons: ['Batal', 'OK'],
					reverseButtons: true,
			}).then((result) => {
				if (result) {
					saveData('formData', function(resp){
						$('#mediumModal').hide();
						$('#mediumModal').find('.loading.dimmer').hide();
		                window.location.reload();
		            });
				}
			})
        });

		$(document).on('click', '.draft.penugasan.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route('kegiatan-audit.persiapan.penugasan.index') !!}/" + idx + '/surat';
            loadModalPreview({
                url: url,
                modal: modal,
            }, function(resp){
                $('#largeModal').find('.loading.dimmer').hide();
                onShow();
            });
        });

        $(document).on('click', '.draft.final.penugasan.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route('kegiatan-audit.persiapan.penugasan.index') !!}/" + idx + '/preview';
            loadModalPreview({
                url: url,
                modal: modal,
            }, function(resp){
                $('#largeModal').find('.loading.dimmer').hide();
                onShow();
            });
        });

	   	$(document).on('click', '.approvesvpdetil.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/approveSvpDetail';
            window.location = url;
        }); 

	   	$(document).on('click', '.approve-svp.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route('kegiatan-audit.persiapan.penugasan.index') !!}/" + idx + '/approveSvp';
        	swal({
					title: "Apakah Anda yakin?",
					text: "Data yang sudah disetujui, tidak dapat diubah!",
					icon: "warning",
					buttons: ['Batal', 'OK'],
					reverseButtons: true,
			}).then((result) => {
				if (result) {
					$.ajax({
						url: url,
						type: 'POST',
						data: {
							id : idx,
							tipe : 1,
							'_token' : '{{ csrf_token() }}'
						}
					})
					.done(function(response) {
						swal({
							icon: 'success',
							title:'Berhasil!',
							text:'Data berhasil disetujui!',
							button: false,
							timer: 1000,
					    }).then((res) => {
					    	dt2 = $('#dataTable2').DataTable();
				    		dt2.draw();
					    	callback()
					    })
					})
					.fail(function(response) {
						console.log(response);
						swal('Data gagal disetujui!', {
							icon: 'error',
					    }).then((res) => {
					    	dt2 = $('#dataTable2').DataTable();
				    		dt2.draw();
					    	callback()
					    })
					})

				}
			})
        });

        $(document).on('click', '.buat.button', function(e){
        	var chkArray = [];
        	dt1 = $('#dataTable1').DataTable();
        	var checkedcollection = dt1.$(".check:checked", { "page": "all" });
        	checkedcollection.each(function() {
				chkArray.push($(this).val());
			});
            if(chkArray.length == 0){
            	swal({
            		type:'error',
					icon: "warning",
					title:'Oopss!',
					text:'Checklist data terlebih dahulu.',
					button: false,
					timer: 1000,
				}).then((result) => { 
				})
            }else{
            	$.ajax({
					url: '{{ url('ajax/option/cek-data') }}',
					type: 'POST',
					data: {
						_token: "{{ csrf_token() }}",
						id: chkArray
					},
				})
				.done(function(response) {
					if(response == 1){
						swal({
		            		type:'error',
							icon: "warning",
							title:'Oopss!',
							text:'Mohon pilih data dengan tahun audit yang sama',
							buttons: 'OK',
						}).then((result) => { 
						})
					}else{
			        	var url = "{!! route('kegiatan-audit.persiapan.penugasan.index') !!}/" + chkArray + '/buat';
		           		window.location = url;
					}
				})
				.fail(function() {
					console.log("error");
				});
            }
        });

        $(document).on('click', '.approvedirutdetail.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/approveDirutDetail';
            window.location = url;
        });

        $(document).on('click', '.upload-dokumen.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route('kegiatan-audit.persiapan.penugasan.index') !!}/" + idx + '/final';
            loadModal({
                url: url,
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                $("#pdf").fileinput({
				    language: 'es',
				    uploadUrl: "/file-upload-batch/2",
				    previewFileType: "pdf",
				    showUpload: false,
				    previewFileIcon: '<i class="fas fa-file"></i>',
				    previewFileIconSettings: {
				        'docx': '<i class="fas fa-file-word text-primary"></i>',
				        'xlsx': '<i class="fas fa-file-excel text-success"></i>',
				        'pptx': '<i class="fas fa-file-powerpoint text-danger"></i>',
				        'pdf': '<i class="fas fa-file-pdf text-danger"></i>',
				        'zip': '<i class="fas fa-file-archive text-muted"></i>',
				    },
				    fileActionSettings: {
					    showRemove: true,
					    showUpload: false, //This remove the upload button
					    showZoom: true,
					    showDrag: true
					},
				    initialPreviewAsData: true,
				    purifyHtml: true,
				    allowedFileTypes: ['pdf'],
			        allowedFileExtensions: ['pdf'],
			        // allowed image size up to 5 MB
			        maxFileSize: 5000,
				    previewFileExtSettings: {
				    	'doc': function(ext) {
				    		return ext.match(/(doc|docx)$/i);
				    	},
				    	'xls': function(ext) {
				    		return ext.match(/(xls|xlsx)$/i);
				    	},
				    	'ppt': function(ext) {
				    		return ext.match(/(ppt|pptx)$/i);
				    	}
				    }
				});
                onShow();
            });
        });
        
        $(document).on('click', '.cetak.button', function(e){
            var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/cetak';
            window.open(url, '_blank');
        });

        $(document).on('click', '.cetak-excel-penugasan.button', function(e){
            var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/cetakExcelPenugasan';
            window.open(url, '_blank');
        });

        $(document).on('click', '.download-rkia.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/downloadAudit';
            window.location = url;
        });
    </script>
    @yield('js-extra')
@endpush