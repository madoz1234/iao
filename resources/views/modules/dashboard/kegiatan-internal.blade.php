<script type="text/javascript">
	$(document).ready(function() {
		var options = {
	          series: [{
	          name: "On Progress",
			  data: [{{$onprogress}}]
	        }, {
	          name: "Completed",
			  data: [{{$close}}]
	        }],
	        chart: {
	          type: 'bar',
	          height: 350,
	          stacked: true,
	          toolbar: {
	            show: false
	          },
	          zoom: {
	            enabled: true
	          }
	        },
	        responsive: [{
	          breakpoint: 480,
	          options: {
	            legend: {
	              position: 'bottom',
	              offsetX: -10,
	              offsetY: 0
	            }
	          }
	        }],
		    grid: {
    			show: false,
    		},
	        plotOptions: {
	          bar: {
	            horizontal: false,
	          },
	        },
	        xaxis: {
	          categories: [{{$tahun}}],
	        },
	        legend: {
	          position: 'top',
	          offsetY: 2
	        },
	        fill: {
	          opacity: 0.9,
			  type: 'solid',
			  pattern: {
			      style: 'verticalLines',
			      width: 6,
			      height: 6,
			      strokeWidth: 2,
			  },
			},dataLabels: {
			  style: {
			    colors: ['#000000']
			  }
			},tooltip: {
			  y: {
			    formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
			      return value
			    }
			  }
			},
			yaxis: {
	          showAlways : false,
		      show: true,
		      labels: {
		          show: false,
		      },
		      lines: {
	            show: false
	          },
		      axisBorder: {
		          show: false,
		      },
		      axisTicks: {
		          show: true,
		      },
		    },
			colors: ['#E1ECFF', '#3F51B5'],
        };

		var chart = new ApexCharts(document.querySelector("#chart-kegiatan"), options);
		chart.render();
	});
</script>
<div id="chart-kegiatan"></div>
