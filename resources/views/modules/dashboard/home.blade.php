@extends('layouts.base')
@include('libs.datatable-dashboard')

@push('css')
    <link rel="stylesheet" href="#" type="text/css" />
    <link rel="stylesheet" href="{{ asset('libs/assets/one-calender/simple-calendar.css') }}" type="text/css" />
@endpush

@push('js')
    <script src="{{ asset('libs/assets/one-calender/jquery.simple-calendar.js') }}"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="https://apexcharts.com/samples/assets/stock-prices.js') }}"></script> --}}
@endpush

@section('title', 'Dashboard')

@section('side-header')
<div style="margin-right: 14px;">
	<span><i class="glyphicon glyphicon-home"></i></span>
</div>
@endsection

@push('styles')
<style>
	.nav-pills > li.active > a,
	.nav-pills > li.active > a:hover,
	.nav-pills > li.active > a:focus {
	  color: #fff;
	  background-color: #448BFF;
	}
	.nav-pills > li > a {
	  border-radius: 0px;
	}
</style>
@endpush

@push('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		var name = <?php echo json_encode($rapat); ?>;
		var text =[];
		for (i = 0; i < name.length; i++) {
		  text.push({
			      startDate: name[i]['startDate'],
			      endDate: name[i]['startDate'],
			      summary: name[i]['summary']
			    },);
		}
		$("#calender").simpleCalendar({
	      fixedStartDay: false,
	      disableEmptyDetails: true,
	      events: text,
	    });

	    $(document).on('click', '.jumlah1', function(e){
	    	var tahun_temuan = $('input[name=tahun_temuan]').val();
			reloadDataTemuan(tahun_temuan)
		});

		$(document).on('click', '.status1', function(e){
		 	var tahun_temuans = $('input[name=tahun_temuans]').val();
			reloadDataTemuans(tahun_temuans)
		});

		$(document).on('click', '.jumlah2', function(e){
	    	var tahun_tl = $('input[name=tahun_tl]').val();
			reloadDataTl(tahun_tl)
		});

		$(document).on('click', '.status2', function(e){
		 	var tahun_tls = $('input[name=tahun_tls]').val();
			reloadDataTls(tahun_tls)
		});

		var tahun = $('input[name=tahuns]').val();
		var tahun_tahap = $('input[name=tahun_tahap]').val();
		reloadData(tahun_tahap)

		var tahun_awal = $('input[name=tahun_awal]').val();
		var tahun_akhir = $('input[name=tahun_akhir]').val();

		reloadDataKegiatan(tahun_awal, tahun_akhir)

		var tahun_temuan = $('input[name=tahun_temuan]').val();
		reloadDataTemuan(tahun_temuan)

		var tahun_temuans = $('input[name=tahun_temuans]').val();
		reloadDataTemuans(tahun_temuans)

		var tahun_tl = $('input[name=tahun_tl]').val();
		reloadDataTl(tahun_tl)

		var tahun_tls = $('input[name=tahun_tls]').val();
		reloadDataTls(tahun_tls)

		$.ajax({
			url: '{{ url('ajax/option/get-alldatadashboard') }}',
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				val: tahun
			},
		})
		.done(function(response) {
			$(".audit_persen").html('');
			$("#auditsss").css('width', 0);
			$(".audit_total").html(response['audit'][0]);
			$(".audit_close").html(response['audit'][1]);

			if(response['audit'][1] > 0){
				var total_audit = Math.round((response['audit'][1] / response['audit'][0]) * 100);
			}else{
				var total_audit = 0;
			}

			$(".audit_persen").html(total_audit+'%');
			$("#auditsss").css('width', total_audit+'%');

			$(".survey_persen").html('');
			$("#survey").css('width', 0);
			$(".survey_total").html(response['survey'][0]);
			$(".survey_close").html(response['survey'][1]);

			if(response['survey'][1] > 0){
				var total_survey = Math.round((response['survey'][1] / response['survey'][0]) * 100);
			}else{
				var total_survey = 0;
			}

			$(".survey_persen").html(total_survey+'%');
			$("#survey").css('width', total_survey+'%');

			$(".konsultasi_persen").html('');
			$("#konsultasi").css('width', 0);
			$(".konsultasi_total").html(response['konsultasi'][0]);
			$(".konsultasi_close").html(response['konsultasi'][1]);

			if(response['konsultasi'][1] > 0){
				var total_konsultasi = Math.round((response['konsultasi'][1] / response['konsultasi'][0]) * 100);
			}else{
				var total_konsultasi = 0;
			}

			$(".konsultasi_persen").html(total_konsultasi+'%');
			$("#konsultasi").css('width', total_konsultasi+'%');

			$(".lain_persen").html('');
			$("#lain").css('width', 0);
			$(".lain_total").html(response['lain'][0]);
			$(".lain_close").html(response['lain'][1]);

			if(response['lain'][1] > 0){
				var total_lain = Math.round((response['lain'][1] / response['lain'][0]) * 100);
			}else{
				var total_lain = 0;
			}

			$(".lain_persen").html(total_lain+'%');
			$("#lain").css('width', total_lain+'%');
		})
		.fail(function() {
			console.log("error");
		});
    });

	$('.tahuns').datepicker({
        format: "yyyy",
	    viewMode: "years",
	    minViewMode: "years",
        orientation: "auto",
        autoclose:true
   	}).on('changeDate', function(e){
        var val = this.value;
        $.ajax({
			url: '{{ url('ajax/option/get-alldatadashboard') }}',
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				val: val
			},
		})
		.done(function(response) {
			$(".audit_persen").html('');
			$("#auditsss").css('width', 0);
			$(".audit_total").html(response['audit'][0]);
			$(".audit_close").html(response['audit'][1]);

			if(response['audit'][1] > 0){
				var total_audit = Math.round((response['audit'][1] / response['audit'][0]) * 100);
			}else{
				var total_audit = 0;
			}

			$(".audit_persen").html(total_audit+'%');
			$("#auditsss").css('width', total_audit+'%');

			$(".survey_persen").html('');
			$("#survey").css('width', 0);
			$(".survey_total").html(response['survey'][0]);
			$(".survey_close").html(response['survey'][1]);

			if(response['survey'][1] > 0){
				var total_survey = Math.round((response['survey'][1] / response['survey'][0]) * 100);
			}else{
				var total_survey = 0;
			}

			$(".survey_persen").html(total_survey+'%');
			$("#survey").css('width', total_survey+'%');

			$(".konsultasi_persen").html('');
			$("#konsultasi").css('width', 0);
			$(".konsultasi_total").html(response['konsultasi'][0]);
			$(".konsultasi_close").html(response['konsultasi'][1]);

			if(response['konsultasi'][1] > 0){
				var total_konsultasi = Math.round((response['konsultasi'][1] / response['konsultasi'][0]) * 100);
			}else{
				var total_konsultasi = 0;
			}

			$(".konsultasi_persen").html(total_konsultasi+'%');
			$("#konsultasi").css('width', total_konsultasi+'%');

			$(".lain_persen").html('');
			$("#lain").css('width', 0);
			$(".lain_total").html(response['lain'][0]);
			$(".lain_close").html(response['lain'][1]);

			if(response['lain'][1] > 0){
				var total_lain = Math.round((response['lain'][1] / response['lain'][0]) * 100);
			}else{
				var total_lain = 0;
			}
			$(".lain_persen").html(total_lain+'%');
			$("#lain").css('width', total_lain+'%');
		})
		.fail(function() {
			console.log("error");
		});
    });

    var reloadData = function(year){
        $.ajax({
			url: '{!! route($routes.'.gridTahap') !!}',
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				val: year
			},
		}).done(function(responses) {
			$('.tahun-tahap').html(year)
			$('.data-tahap').html(responses)
		}).fail(function() {
				console.log("error");
		});
    }

    $('.tgl_awal').datepicker({
        format: "yyyy",
	    viewMode: "years",
	    minViewMode: "years",
        orientation: "auto",
        autoclose:true
    }).on('changeDate', function (selected) {
    	var mins = new Date(selected.date.valueOf());
	    $('.tgl_end_akhir').datepicker('destroy').datepicker({
            format: "yyyy",
		    viewMode: "years",
		    minViewMode: "years",
	        orientation: "auto",
	        autoclose:true,
        }).on('changeDate', function (selected) {
        	var min = mins.getFullYear();
        	var max = (new Date(selected.date.valueOf())).getFullYear();
        	reloadDataKegiatan(min, max)
		});
    	$('.tgl_end_akhir').val('');
        $('.tgl_end_akhir').focus();
	});

	$('.tgl_temuan').datepicker({
        format: "yyyy",
	    viewMode: "years",
	    minViewMode: "years",
        orientation: "auto",
        autoclose:true
    }).on('changeDate', function (selected) {
    	var mins = new Date(selected.date.valueOf());
	    reloadDataTemuan(mins.getFullYear())
	});

	$('.tgl_temuans').datepicker({
        format: "yyyy",
	    viewMode: "years",
	    minViewMode: "years",
        orientation: "auto",
        autoclose:true
    }).on('changeDate', function (selected) {
    	var mins = new Date(selected.date.valueOf());
	    reloadDataTemuans(mins.getFullYear())
	});

	$('.tgl_tl').datepicker({
        format: "yyyy",
	    viewMode: "years",
	    minViewMode: "years",
        orientation: "auto",
        autoclose:true
    }).on('changeDate', function (selected) {
    	var mins = new Date(selected.date.valueOf());
	    reloadDataTl(mins.getFullYear())
	});

	$('.tgl_tls').datepicker({
        format: "yyyy",
	    viewMode: "years",
	    minViewMode: "years",
        orientation: "auto",
        autoclose:true
    }).on('changeDate', function (selected) {
    	var mins = new Date(selected.date.valueOf());
	    reloadDataTls(mins.getFullYear())
	});

    var reloadDataKegiatan = function(year, years){
        $.ajax({
			url: '{!! route($routes.'.gridKegiatan') !!}',
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				min: year,
				max: years
			},
		}).done(function(responses) {
			$('.tahun_awal').html(year)
			$('.tahun_akhir').html(years)
			$('#kegiatan-audit').html(responses)
		}).fail(function() {
				console.log("error");
		});
    }

    var reloadDataTemuan = function(tgl_temuan){
        $.ajax({
			url: '{!! route($routes.'.gridTemuan') !!}',
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				tgl_temuan: tgl_temuan
			},
		}).done(function(responses) {
			$('.tahun_temuan').html(tgl_temuan)
			$('#temuan-audit').html(responses)
		}).fail(function() {
			console.log("error");
		});
    }

    var reloadDataTemuans = function(tgl_temuans){
        $.ajax({
			url: '{!! route($routes.'.gridTemuans') !!}',
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				tgl_temuans: tgl_temuans
			},
		}).done(function(responses) {
			$('.tahun_temuans').html(tgl_temuans)
			$('#temuans-audit').html(responses)
		}).fail(function() {
			console.log("error");
		});
    }

    var reloadDataTl = function(tgl_tl){
        $.ajax({
			url: '{!! route($routes.'.gridTl') !!}',
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				tgl_tl: tgl_tl
			},
		}).done(function(responses) {
			$('.tahun_tl').html(tgl_tl)
			$('#tl-audit').html(responses)
		}).fail(function() {
			console.log("error");
		});
    }

    var reloadDataTls = function(tgl_tls){
        $.ajax({
			url: '{!! route($routes.'.gridTls') !!}',
			type: 'POST',
			data: {
				_token: "{{ csrf_token() }}",
				tgl_tls: tgl_tls
			},
		}).done(function(responses) {
			$('.tahun_tls').html(tgl_tls)
			$('#tls-audit').html(responses)
		}).fail(function() {
			console.log("error");
		});
    }


    $('.tahun_tahap').datepicker({
        format: "yyyy",
	    viewMode: "years",
	    minViewMode: "years",
        orientation: "auto",
        autoclose:true
   	}).on('changeDate', function(e){
        var val = this.value;
        reloadData(val)
    });
	// calender
	var app = {
		settings: {
			container: $('.calendar'),
			calendar: $('.front'),
			days: $('.weeks span'),
			form: $('.back'),
			input: $('.back input'),
			buttons: $('.back button')
		},

		init: function() {
			instance = this;
			settings = this.settings;
			this.bindUIActions();
		},

		swap: function(currentSide, desiredSide) {
			settings.container.toggleClass('flip');

			currentSide.fadeOut(900);
			currentSide.hide();

			desiredSide.show();
		},

		bindUIActions: function() {
			settings.days.on('click', function(){
				instance.swap(settings.calendar, settings.form);
				settings.input.focus();
			});

			settings.buttons.on('click', function(){
				instance.swap(settings.form, settings.calendar);
			});
		}
	}

// chart pies
var options = {
	chart: {
		height: 250,
		type: 'pie',
		events: {
			click: function (event, chartContext, config) {
				console.log("click");
			},
			dataPointSelection: function (event, chartContext, config) {
				console.log("dataPointSelection");
			}
		}
	},
	series: [44, 55, 13],
	labels: ['Sudah', 'Belum', 'Dalam Proses'],
	colors: ['#ff4560', '#feb019', '#00e396'],
}

var charts = new ApexCharts(
	document.querySelector("#chart-pies"),
	options
	);
charts.render();
</script>
@endpush

@section('body')
		<div class="row" style="margin: 0px 0px 20px 0px">
			<div class="col-sm-12 text-right">
				<form id="dataFilters" class="form-inline" role="form">
					<div class="form-group">
						<label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
						<input type="text" class="form-control tahuns" name="tahuns" id="tahuns" data-toggle="years" placeholder="Pilih Tahun" value="{{ \Carbon\Carbon::now()->format('Y') }}">
					</div>
				</form>
			</div>
		</div>

		<div class="row row-sm">
			<div class="col-lg-3 col-md-4 col-sm-6">
        <a href="{{ url('monitoring/monitoring') }}">
				<div class="panel b-a">
					<div class="panel-heading text-left no-border">
						<span class="h4 m-b font-thin">Audit</span>
					</div>
					<div class="wrapper b-b b-light" style="padding-bottom: 0px;padding-top: 0px;">
						<h4 class="m-t-none text-right">
							<span class="text-1x text-lt"><font color="#F54394"><span class="audit_close">10</span></font>/<span class="audit_total">100</span></span>
						</h4>
            <h6 class="m-t-none text-right">
              %Progress
            </h6>
						<div class="m-t-sm">
							<span class="text-1x text-lt audit_persen">10%</span>
							<div class="progress progress-xs">
								<div class="progress-bar" id="auditsss" data-toggle="tooltip" style="width: 10%; background-color: #F54394;"></div>
							</div>
						</div>
					</div>
				</div>
        </a>
			</div>
			<div class="col-lg-3 col-md-4 col-sm-6">
        <a href="{{ url('survey-kegiatan-audit/survey') }}">
  				<div class="panel b-a">
  					<div class="panel-heading text-left no-border">
  						<span class="h4 m-b font-thin">Survey Kepuasan Auditee</span>
  					</div>
  					<div class="wrapper b-b b-light" style="padding-bottom: 0px;padding-top: 0px;">
  						<h4 class="m-t-none text-right">
  							<span class="text-1x text-lt"><font color="#F4C414"><span class="survey_close">10</span></font>/<span class="survey_total">100</span></span>
  						</h4>
              <h6 class="m-t-none text-right">
                %Progress
              </h6>
  						<div class="m-t-sm">
  							<span class="text-1x text-lt survey_persen">62%</span>
  							<div class="progress progress-xs">
  								<div class="progress-bar" id="survey" data-toggle="tooltip" style="width: 62%; background-color: #F4C414;"></div>
  							</div>
  						</div>
  					</div>
  				</div>
        </a>
			</div>
			<div class="col-lg-3 col-md-4 col-sm-6">
        <a href="{{ url('kegiatan-konsultasi/perencanaan') }}">
  				<div class="panel b-a">
  					<div class="panel-heading text-left no-border">
  						<span class="h4 m-b font-thin">Kegiatan Konsultasi</span>
  					</div>
  					<div class="wrapper b-b b-light" style="padding-bottom: 0px;padding-top: 0px;">
  						<h4 class="m-t-none text-right">
  							<span class="text-1x text-lt"><font color="#31C971"><span class="konsultasi_close">10</span></font>/<span class="konsultasi_total">100</span></span>
  						</h4>
              <h6 class="m-t-none text-right">
                %Progress
              </h6>
  						<div class="m-t-sm">
  							<span class="text-1x text-lt konsultasi_persen">70%</span>
  							<div class="progress progress-xs">
  								<div class="progress-bar" id="konsultasi" data-toggle="tooltip" style="width: 70%; background-color: #31C971;"></div>
  							</div>
  						</div>
  					</div>
  				</div>
        </a>
			</div>
			<div class="col-lg-3 col-md-4 col-sm-6">
        <a href="{{ url('kegiatan-lainnya/perencanaan') }}">
  				<div class="panel b-a">
  					<div class="panel-heading text-left no-border">
  						<span class="h4 m-b font-thin">Kegiatan Lainnya</span>
  					</div>
  					<div class="wrapper b-b b-light" style="padding-bottom: 0px;padding-top: 0px;">
  						<h4 class="m-t-none text-right">
  							<span class="text-1x text-lt"><font color="#14BAE4"><span class="lain_close">10</span></font>/<span class="lain_total">100</span></span>
  						</h4>
              <h6 class="m-t-none text-right">
                %Progress
              </h6>
  						<div class="m-t-sm">
  							<span class="text-1x text-lt lain_persen">63%</span>
  							<div class="progress progress-xs">
  								<div class="progress-bar" id="lain" data-toggle="tooltip" style="width: 63%; background-color: #14BAE4;"></div>
  							</div>
  						</div>
  					</div>
  				</div>
        </a>
			</div>
		</div>

		<div class="row row-sm">
			<div class="col-lg-8 col-md-8 col-sm-8">
			{{-- Kegiatan Internal Audit --}}
				<div class="panel panel-default">
					<div class="panel-heading font-bold">Kegiatan Internal Audit</div>
					<div class="panel-body">
						<div class="row" style="margin: 0px 0px 20px 0px">
							<div class="col-sm-12">
								<form id="dataFilters" class="form-inline" role="form">
									<div class="form-group">
										<label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
										<input type="text" class="form-control tgl_awal" name="tahun_awal" data-toggle="years" placeholder="Pilih Tahun" value="{{ \Carbon\Carbon::now()->format('Y') - 10 }}">
										<input type="text" class="form-control tgl_end_akhir" name="tahun_akhir" data-toggle="years" placeholder="Pilih Tahun" value="{{ \Carbon\Carbon::now()->format('Y') }}">
									</div>
								</form>
							</div>
						</div>
						<div class="text-center">
							<h4>Periode Tahun <span class="tahun_awal">{{ \Carbon\Carbon::now()->format('Y') - 10 }}</span> - <span class="tahun_akhir">{{ \Carbon\Carbon::now()->format('Y') }}</span></h4>
						</div>
						<div id="kegiatan-audit">
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4">
				{{-- RAPAT --}}
				<div class="panel panel-default">
	              <div class="panel-heading font-bold">Rapat Terdekat</div>
	              <div class="panel-body" style="margin-bottom: 11px;">
	                <div class="text-right">
	                  <ul class="nav nav-pills nav-sm pull-right" style="border-style: solid;border-radius:5px;color: #448BFF;border-width: 2px;">
	                    <li class="active"><a href="#hari" aria-controls="hari" role="tab" data-toggle="tab">Hari</a></li>
	                    <li ><a href="#bulan" aria-controls="bulan" role="tab" data-toggle="tab">Bulan</a></li>
	                  </ul>
	                </div>
	                <div id="myTabContent" class="tab-content">
	                  <div role="tabpanel" class="tab-pane active" id="hari">
	                  	@if(is_null(getrapat()))
	                  		<div class="col-lg-12">
		                        <p>Jadwal Rapat tidak tersedia</p>
		                    </div>
	                  	@else
		                    <div class="col-lg-12">
		                        <div class="col-lg-4">
		                          <table border="0" cellpadding="0" cellspacing="0" width="90%" style="background-color:#FFFFFF; border:1px solid #CCCCCC;">
		                              <tr>
		                                  <td align="left" valign="top" style="padding:5px;">
		                                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
		                                          <tr>
		                                              <td align="center" valign="top" style="background-color:#F00000; color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:15px; font-weight:bold; padding-top:5px; padding-bottom:5px; text-align:center;">
		                                                  {{ App\Libraries\CoreSn::Bulan(getrapat()->tanggal) }}
		                                              </td>
		                                          </tr>
		                                          <tr>
		                                              <td align="center" valign="top" style="color:#000000; font-family:Helvetica, Arial, sans-serif; font-size:20px; font-weight:bold; line-height:100%; padding-top:20px; padding-bottom:8px; text-align:center;">
		                                                  {{ App\Libraries\CoreSn::Days(getrapat()->tanggal) }}
		                                              </td>
		                                          </tr>
		                                          <tr>
		                                              <td align="center" valign="top" style="color:#c9d1d3; font-family:Helvetica, Arial, sans-serif; font-size:15px; font-weight:bold; text-align:center;padding-top:10px;">
		                                                  {{ App\Libraries\CoreSn::Day(getrapat()->tanggal) }}
		                                              </td>
		                                          </tr>
		                                      </table>
		                                  </td>
		                              </tr>
		                          </table>
		                        </div>
		                        <div class="col-lg-8">
		                          <table border="0" cellpadding="0" cellspacing="0" width="100%" padding-top="10px;">
		                              <tr>
		                                  <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;width: 60px;">Nomor</td>
		                                  <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;">{{ getrapat()->nomor }}</td>
		                              </tr>
		                              <tr>
		                                  <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;width: 60px;">Tempat</td>
		                                  <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;">{{ getrapat()->tempat }}</td>
		                              </tr>
		                              <tr>
		                                  <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;width: 60px;">Jam</td>
		                                  <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;">{{ getrapat()->jam_mulai }} - {{ getrapat()->jam_selesai }}</td>
		                              </tr>
		                          </table>
		                        </div>
		                    </div>
		                    <div class="col-lg-12">
	                          <table border="0" cellpadding="0" cellspacing="0" width="100%" padding-top="10px;">
	                              <tr>
	                                  <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;width: 60px;">Agenda</td>
	                              </tr>
	                              <tr>
	                                  <td style="font-size:13px; line-height:100%; padding-bottom:8px; text-align:left;">
	                                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
	                                      	  @foreach(getrapat()->agendaInternal as $key => $data)
		                                          <tr>
		                                              <td style="font-size:13px; line-height:100%; padding-top:8px; padding-bottom:8px; text-align:left;">{{$key+1}}. {{ $data->agenda }}</td>
		                                          </tr>
	                                          @endforeach
	                                      </table>
	                                  </td>
	                              </tr>
	                          </table>
	                        </div>
	                  	@endif
	                  </div>
	                  <div role="tabpanel" class="tab-pane" id="bulan">
	                    <div class="col-lg-12">
	                        <div id="calender" class="calendar-container" style="padding: 10px;"></div>
	                    </div>
	                  </div>
	                </div>
	              </div>
	            </div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				{{-- TAHAP AUDIT --}}
				<div class="panel panel-default">
					<div class="panel-heading font-bold">Tahap Audit</div>
					<div class="panel-body">
						<div class="row" style="margin: 0px 0px 20px 0px">
							<div class="col-sm-12">
								<form id="dataFilters" class="form-inline" role="form">
									<div class="form-group">
										<label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
										<input type="text" class="form-control tahun_tahap" name="tahun_tahap" id="tahun_tahap" data-toggle="years" placeholder="Pilih Tahun" value="{{ \Carbon\Carbon::now()->format('Y') }}">
									</div>
								</form>
							</div>
						</div>
						<div class="text-center">
							<h4>Periode Tahun <span class="tahun-tahap">{{ \Carbon\Carbon::now()->format('Y') }}</span></h4>
						</div>
						<div class="data-tahap"></div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				{{-- TEMUAN --}}
				<div class="panel panel-default">
	              <div class="panel-heading font-bold">Temuan</div>
	              <div class="panel-body">
	                <div class="text-right">
	                  <ul class="nav nav-pills nav-sm pull-right" style="border-style: solid;border-radius:5px;color: #448BFF;border-width: 2px;">
	                    <li class="active"><a href="#jumlah1" aria-controls="jumlah1" class="jumlah1" role="tab" data-toggle="tab">Jumlah</a></li>
	                    <li><a href="#status1" aria-controls="status1" class="status1" role="tab" data-toggle="tab">Status</a></li>
	                  </ul>
	                </div>
	                <div id="myTabContent" class="tab-content">
	                  <div role="tabpanel" class="tab-pane active" id="jumlah1">
						<form id="dataFilters" class="form-inline" role="form">
							<div class="form-group">
								<label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
								<input type="text" class="form-control tgl_temuan" name="tahun_temuan" id="tahun_temuan" data-toggle="years" placeholder="Pilih Tahun" value="{{ \Carbon\Carbon::now()->format('Y') }}">
							</div>
						</form>
						<div class="panel-body">
							<div id="temuan-audit">
							</div>
						</div>
	                  </div>
	                  <div role="tabpanel" class="tab-pane" id="status1">
                        <form id="dataFilters" width="50%" class="form-inline" role="form">
							<div class="form-group">
								<label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
								<input type="text" class="form-control tgl_temuans" name="tahun_temuans" id="tahun_temuans" data-toggle="years" placeholder="Pilih Tahun" value="{{ \Carbon\Carbon::now()->format('Y') }}">
							</div>
						</form>
	                    <div class="col-lg-12">
							<div id="temuans-audit">
							</div>
	                    </div>
	                  </div>
	                </div>
	              </div>
	            </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				{{-- TINDAK LANJUT --}}
				<div class="panel panel-default">
	              <div class="panel-heading font-bold">Monitoring Tindak Lanjut</div>
	              <div class="panel-body">
	                <div class="text-right">
	                  <ul class="nav nav-pills nav-sm pull-right" style="border-style: solid;border-radius:5px;color: #448BFF;border-width: 2px;">
	                    <li class="active"><a href="#jumlah2" aria-controls="jumlah2" class="jumlah2" role="tab" data-toggle="tab">Jumlah</a></li>
	                    <li><a href="#status2" aria-controls="status2" class="status2" role="tab" data-toggle="tab">Status</a></li>
	                  </ul>
	                </div>
	                <div id="myTabContent" class="tab-content">
	                  <div role="tabpanel" class="tab-pane active" id="jumlah2">
						<form id="dataFilters" class="form-inline" role="form">
							<div class="form-group">
								<label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
								<input type="text" class="form-control tgl_tl" name="tahun_tl" id="tahun_tl" data-toggle="years" placeholder="Pilih Tahun" value="{{ \Carbon\Carbon::now()->format('Y') }}">
							</div>
						</form>
						<div class="panel-body">
							<div id="tl-audit">
							</div>
						</div>
	                  </div>
	                  <div role="tabpanel" class="tab-pane" id="status2">
                        <form id="dataFilters" class="form-inline" role="form">
							<div class="form-group">
								<label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
								<input type="text" class="form-control tgl_tls" name="tahun_tls" id="tahun_tls" data-toggle="years" placeholder="Pilih Tahun" value="{{ \Carbon\Carbon::now()->format('Y') }}">
							</div>
						</form>
	                    <div class="col-lg-12">
							<div id="tls-audit"></div>
	                    </div>
	                  </div>
	                </div>
	              </div>
	            </div>
			</div>
		</div>
	</div>
@endsection
