<script type="text/javascript">
	$(document).ready(function() {
		var options = {
	          series: [{
	          name: "On Progress",
			  data: [{{$onprogress}}]
	        }, {
	          name: "Completed",
			  data: [{{$close}}]
	        }],
	        chart: {
	          type: 'bar',
	          height: 350,
	          stacked: true,
	          toolbar: {
	            show: false
	          },
	          zoom: {
	            enabled: true
	          }
	        },
	        responsive: [{
	          breakpoint: 480,
	          options: {
	            legend: {
	              position: 'bottom',
	              offsetX: -10,
	              offsetY: 0
	            }
	          }
	        }],
	        plotOptions: {
	          bar: {
	            horizontal: false,
	          },
	        },
	        xaxis: {
	          categories: ['RKIA',
						'Surat Penugasan',
						'Permintaan Dokumen',
						'Program Audit',
						'Opening Meeting',
						'KKA',
						'Closing Meeting',
						'LHA'
	          ],
	        },
	        legend: {
	          position: 'top',
	          offsetY: 2
	        },
	        yaxis: {
	          showAlways : false,
		      floating: true,
			  axisTicks: {
			    show: false
			  },
			  axisBorder: {
			    show: false
			  },
			  labels: {
			    offsetX: -100
			  },
		    },
		    dataLabels: {
			  style: {
			    colors: ['#000000']
			  }
			},tooltip: {
			  y: {
			    formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
			      return value
			    }
			  }
			},
			grid: {
			    show: false,
			},
			colors: ['#E3E5E9', '#F54394'],
        };
		var chart_audit_tahap = new ApexCharts(document.querySelector("#chart-tahap"), options);
		chart_audit_tahap.render();
	});
</script>
<div id="chart-tahap"></div>
