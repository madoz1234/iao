<script type="text/javascript">
	$(document).ready(function() {
		app.init();
// chart pie
var options = {
	chart: {
		height: 254,
		type: 'pie',
		events: {
			click: function (event, chartContext, config) {
				console.log("click");
			},
			dataPointSelection: function (event, chartContext, config) {
				console.log("dataPointSelection");
			}
		}
	},
	series: [{{$data}}],
	labels: ['Operasional', 'Keuangan', 'Sistem'],
	colors: ['#3F51B5', '#9C27B0', '#00BCD4'],
}

var chart = new ApexCharts(
	document.querySelector("#chart-tl"),
	options
	);
chart.render();
	});
</script>
<div id="chart-tl"></div>
