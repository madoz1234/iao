<script type="text/javascript">
	$(document).ready(function() {
		app.init();
// chart pie
var options = {
          series: [{
          name: 'Belum',
          data: [{{$belum}}]
        }, {
          name: 'Dalam Proses',
          data: [{{$sedang}}]
        }, {
          name: 'Sudah',
          data: [{{$sudah}}]
        }],
          chart: {
          type: 'bar',
          height: 272,
        },
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '40%',
            endingShape: 'rounded'
          },
        },
        yaxis: {
          showAlways : false,
	      floating: true,
		  axisTicks: {
		    show: false
		  },
		  axisBorder: {
		    show: false
		  },
		  labels: {
		    show: false
		  },
	    },
	    grid: {
			show: false,
		},
        legend: {
          position: 'top',
          offsetY: 2
        },
        stroke: {
          show: true,
          width: 1,
        },
        stroke: {
          show: true,
          width: 1,
        },
        xaxis: {
          categories: ['Operasional', 'Keuangan', 'Sistem'],
        },
        colors: ['#F44336', '#F4C414', '#4CAF50'],
        };

var chart = new ApexCharts(
	document.querySelector("#chart-tls"),
	options
	);
chart.render();
	});
</script>
<div id="chart-tls"></div>
