<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
    @method('PATCH')
    @csrf
    <input type="hidden" name="id" value="{{ $record->id }}">
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Ubah Data RKIA</h5>
    </div>
    <input type="hidden" name="tipe" value="0">
    <div class="modal-body">
    	<div class="form-group field">
            <label class="control-label">Tahun</label>
            <input type="text" name="tahun" class="form-control tahun" placeholder="Tahun" required="" value="{{ $record->tahun }}">
        </div>
        <div class="form-group field">
            <label class="control-label">Revisi</label><br>
            <span class="label label-info" style="font-size: 11px;">{{ ($record->revisi) }}</span>
            <input type="hidden" name="revisi" class="form-control tahun" placeholder="Tahun" required="" value="{{ ($record->revisi) }}">
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>