<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
    @csrf

    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Tambah Data RKIA</h5>
    </div>
    <input type="hidden" name="tipe" value="0">
    <div class="modal-body">
        <div class="form-group field">
            <label class="control-label">Tahun</label>
            <input type="text" name="tahun" class="form-control tahun" placeholder="Tahun" required="">
        </div>
        <div class="form-group field">
            <label class="control-label">Revisi</label><br>
            <span class="label label-info" style="font-size: 11px;">0</span>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>