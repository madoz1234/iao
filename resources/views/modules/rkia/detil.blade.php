@extends('layouts.form')
@section('title', 'RKIA Tahun '.$record->tahun)
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body" style="padding-bottom: 0px;">
		<form action="{{ route($routes.'.saveObject', $record->id) }}" method="POST" id="formData">
			@method('PATCH')
			@csrf
			<input type="hidden" name="id" value="{{ $record->id }}">
			<input type="hidden" name="tahun" value="{{ $record->tahun }}">
			<input type="hidden" name="status" value="0">
			@if($record->ket_dirut)
				<p style="padding-left: 14px;color:red;">Status <b>Ditolak</b> Dirut pada {{DateToStringWday($record->updated_at)}}</p>
				<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
					<tr>
						<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
							<h5 style="font-weight: bold;font-size: 12px;">Keterangan :</h5>
							<p style="text-align: justify;text-justify: inter-word;">{{ $record->ket_dirut }}</p>
						</td>
					</tr>
				</table>
			@endif
			@if($record->detailrencana->count() > 0)
				<div class="panel panel-default data-form">
					@foreach($record->detailrencana as $key => $data)
						<div class="hitung panel-heading data-detil-{{$key}}" data-id="{{$key}}" data-value="{{$data->object_id}}" data-tipe="{{$data->tipe_object}}" style="font-weight: bold;font-size: 13px;text-align: right;">
							<div style="width: 50%;text-align: left;">
								<label style="margin-top: 0px;text-align: left;margin-bottom: 0px;" class="head-numboor-{{$key+1}}">{{$key+1}}.</label>
							</div>
						</div>
						<div class="panel-body data-detil-{{$key}}" style="padding-bottom: 0px;">
							<input type="hidden" name="detail[{{$key}}][detail_id]" value="{{ $data->id }}">
							<input type="hidden" name="exists[]" value="{{$data->id}}">
							<table class="table" style="font-size: 12px;">
								<tbody>
									@if($data->tipe == 1)
										<tr>
											<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">Rencana Kerja</td>
											<td style="width:2px;border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;" class="field">
												@if($data->tipe == 0)
													<span class="label label-success">Kegiatan Audit</span>
												@elseif($data->tipe == 1)
													<span class="label label-info">Kegiatan Konsultasi</span>
												@else 
													<span class="label label-primary">Kegiatan Lainnya</span>
												@endif
											</td>
										</tr>
										<tr>
											<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">Kategori</td>
											<td style="width:2px;border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;" class="field">
												{{ $data->konsultasi->nama }}
											</td>
										</tr>
									@elseif($data->tipe == 2)
										<tr>
											<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">Rencana Kerja</td>
											<td style="width:2px;border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;" class="field">
												@if($data->tipe == 0)
													<span class="label label-success">Kegiatan Audit</span>
												@elseif($data->tipe == 1)
													<span class="label label-info">Kegiatan Konsultasi</span>
												@else 
													<span class="label label-primary">Kegiatan Lainnya</span>
												@endif
											</td>
										</tr>
										<tr>
											<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">Kategori</td>
											<td style="width:2px;border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;" class="field">
												@if($data->lain_id == 1000)
												SPIN
												@elseif($data->lain_id == 1001)
												IACM
												@else
													Counterpart Auditor Eksternal - {{ $data->lain->nama }}
												@endif
											</td>
										</tr>
									@else
										<tr>
											<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">Rencana Kerja</td>
											<td style="width:2px;border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;" class="field">
												@if($data->tipe == 0)
													<span class="label label-success">Kegiatan Audit</span>
												@elseif($data->tipe == 1)
													<span class="label label-info">Kegiatan Konsultasi</span>
												@else 
													<span class="label label-primary">Kegiatan Lainnya</span>
												@endif
											</td>
										</tr>
										<tr>
											<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">Kategori</td>
											<td style="width:2px;border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;" class="field">
												@if($data->tipe_object == 0)
													<span class="label label-success">Business Unit (BU)</span>
												@elseif($data->tipe_object == 1)
													<span class="label label-info">Corporate Office (CO)</span>
												@elseif($data->tipe_object == 2)
													<span class="label label-default">Project</span>
												@else 
													<span class="label label-primary">Anak Perusahaan</span>
												@endif
											</td>
										</tr>
										<tr>
											<td style="border: 1px #ffffff;">Objek Audit</td>
											<td style="border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;" class="field">
												{!! object_audit($data->tipe_object, $data->object_id) !!}
											</td>
										</tr>
									@endif
									<tr>
										<td style="border: 1px #ffffff;">Rencana</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;" class="field">
											{{ $data->rencana }}
										</td>
									</tr>
									<tr>
										<td style="border: 1px #ffffff;">Keterangan</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;" class="field">
											@if($data->keterangan == 0)
												<span class="label label-info">Tidak Dipilih</span>
											@else 
												<span class="label label-primary">Dipilih</span>
											@endif
										</td>
									</tr>
									<tr>
										<td style="border: 1px #ffffff;">Auditor Operasional</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;" class="field">
											@if($data->detailuser->where('tipe', 0))
												@php 	
													$op = array();
												@endphp
												@foreach($data->detailuser->where('tipe', 0) as $user_op)
													@php 	
														$op[]= $user_op->user->name;
													@endphp
												@endforeach
												{{ implode(", ", $op) }}
											@else 
											-
											@endif
										</td>
									</tr>
									<tr>
										<td style="border: 1px #ffffff;">Auditor Keuangan</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;" class="field">
											@if($data->detailuser->where('tipe', 1))
												@php 	
													$keu = array();
												@endphp
												@foreach($data->detailuser->where('tipe', 1) as $user_keu)
													@php 	
														$keu[]= $user_keu->user->name;
													@endphp
												@endforeach
												{{ implode(", ", $keu) }}
											@else 
											-
											@endif
										</td>
									</tr>
									<tr>
										<td style="border: 1px #ffffff;">Auditor Sistem</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;" class="field">
											@if($data->detailuser->where('tipe', 2))
												@php 	
													$sis = array();
												@endphp
												@foreach($data->detailuser->where('tipe', 2) as $user_sis)
													@php 	
														$sis[]= $user_sis->user->name;
													@endphp
												@endforeach
												{{ implode(", ", $sis) }}
											@else 
											-
											@endif
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">No AB</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											{{ getNoAb($data->object_id)}}
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">Project ID</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											{{ getId($data->object_id)}}
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">Project</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											{!! object_audit($data->tipe_object, $data->object_id)!!}
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">Lokasi</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											{{ getlokasi($data->object_id)}}
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">Tgl Mulai</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											{{ DateToString($data->tgl_mulai) }}
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">Tgl Selesai</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											{{ DateToString($data->tgl_selesai) }}
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">Jenis Kontrak</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											@if($data->jenis == 0)
												<span class="label label-info">Kontrak Baru</span>
											@elseif($data->jenis == 1)
												<span class="label label-primary">Sisa Nilai Kontrak</span>
											@else
												<span class="label label-success">Project Selesai</span>
											@endif
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">SNK {{ ($record->tahun-1) }}</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											{{ number_format($data->snk, 0, '.', '.') }}
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">Progress s.d Des {{ ($record->tahun-1) }}</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											{{ number_format($data->progress, 0, '.', '.') }}
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">NK Total</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											{{ number_format($data->nilai_kontrak, 0, '.', '.') }}
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">Progress (Ra)</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											{{ number_format($data->progress_ra, 0, '.', '.') }}
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">Progress (Ri)</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											{{ number_format($data->progress_ri, 0, '.', '.') }}
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">Deviasi Progress</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											{{ number_format($data->deviasi, 0, '.', '.') }}
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">Progress Risk Impact</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											@if($data->risk_impact == 0)
												<span class="label label-default">Sangat Ringan</span>
											@elseif($data->risk_impact == 1)
												<span class="label label-success">Ringan</span>
											@elseif($data->risk_impact == 2)
												<span class="label label-info">Sedang</span>
											@elseif($data->risk_impact == 3)
												<span class="label label-warning">Berat</span>
											@else 
												<span class="label label-danger">Sangat Berat</span>
											@endif
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">MAPP</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											{{ number_format($data->mapp, 0, '.', '.') }}
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">BK / PU</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											{{ number_format($data->bkpu, 0, '.', '.') }}
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">Deviasi BK / PU</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											{{ number_format($data->deviasi_bkpu, 0, '.', '.') }}
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">BK / PU Risk Impact</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											@if($data->bkpu_ri == 0)
												<span class="label label-default">Sangat Ringan</span>
											@elseif($data->bkpu_ri == 1)
												<span class="label label-success">Ringan</span>
											@elseif($data->bkpu_ri == 2)
												<span class="label label-info">Sedang</span>
											@elseif($data->bkpu_ri == 3)
												<span class="label label-warning">Berat</span>
											@else 
												<span class="label label-danger">Sangat Berat</span>
											@endif
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					@endforeach
				</div>
				<input type="hidden" name="last" value="{{$key}}">
			@else 
			@endif
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-left">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
        .table-responsive{
            overflow-x: auto;
            overflow-y: visible !important;
        }

        .table-responsive .dropdown-menu {
	        position: static !important;
	    }
		
	    /*.datepicker-days > table > tbody > tr:hover {
		    background-color: #808080;
		}*/
    </style>
@endpush

@push('scripts')
    <script>
    	$('.hitung').each(function(key, value){
    		var id = $(this).data("id");
    		var val = $(this).data("value");
    		var tipe = $(this).data("tipe");
    		if(tipe == 2){
				$('input[name="detail['+id+'][nama]"]').prop("disabled", false);
				$('.detil-'+id).show();
			}else{
				$('input[name="detail['+id+'][nama]"]').prop("disabled", true);
				$('.detil-'+id).hide();
			}
    		$.ajax({
				url: '{{ url('ajax/option/get-kategori') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					kategori: $('select[name="detail['+id+'][tipe_object]"]').val()
				},
			})
			.done(function(response) {
				$('select[name="detail['+id+'][object_id]"]').html(response);
				$('select[name="detail['+id+'][object_id]"]').children('option[value="'+val+'"]').prop('selected',true);
				$('select[name="detail['+id+'][object_id]"]').selectpicker("refresh");
				if(tipe == 2){
					var nama = $('select[name="detail['+id+'][object_id]"] option:selected').html();
					$('.project-nama-'+id).html(nama);
				}
			})
			.fail(function() {
				console.log("error");
			});
    	});
 
		$('.kategori').on('change', function(){
			var id = $(this).data("id");
			$('.project-nama-'+id).html('-');
			if(this.value == 2){
				$('input[name="detail['+id+'][nama]"]').prop("disabled", false);
				$('.detil-'+id).show();
				$('.object').on('change', function(){
					var id = $(this).data("id");
					var nama = $('select[name="detail['+id+'][object_id]"] option:selected').html();
					$('.project-nama-'+id).html(nama);
				});
			}else{
				$('input[name="detail['+id+'][nama]"]').prop("disabled", true);
				$('.detil-'+id).hide();
			}
			$.ajax({
				url: '{{ url('ajax/option/get-kategori') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					kategori: this.value
				},
			})
			.done(function(response) {
				$('select[name="detail['+id+'][object_id]"]').html(response);
				$('select[name="detail['+id+'][object_id]"]').selectpicker("refresh");
			})
			.fail(function() {
				console.log("error");
			});
		});
		$('.jenis').on('change', function(){
			var id = $(this).data("id");
			if(this.value == 0){
				$('input[name="detail['+id+'][snk]"]').prop("disabled", true);
				$('input[name="detail['+id+'][progress]"]').prop("disabled", true);
			}else{
				$('input[name="detail['+id+'][snk]"]').prop("disabled", false);
				$('input[name="detail['+id+'][progress]"]').prop("disabled", false);
			}
		});
    	$(document).ready(function(){
    		reloadMask();
    		moment.locale('id')
    	});
    </script>
    @yield('js-extra')
@endpush

