<form action="{{ route($routes.'.saveRevisi', $record->id) }}" method="POST" id="formData">
    @method('PATCH')
    @csrf
    <input type="hidden" name="id" value="{{ $record->id }}">
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Buat Revisi</h5>
    </div>
    <div class="modal-body">
    	<div class="form-group field">
            <label class="control-label">Tahun</label></br>
            {{ $record->tahun }}
        </div>
        <div class="form-group field">
            <label class="control-label">Revisi</label><br>
            <span class="label label-info" style="font-size: 11px;">{{ ($record->revisi+1) }}</span>
            <input type="hidden" name="revisi" class="form-control tahun" placeholder="Tahun" required="" value="{{ ($record->revisi+1) }}">
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>