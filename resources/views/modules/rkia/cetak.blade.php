<html>
<head>
<style>
		@font-face {
		    font-family: 'tahoma';
		    src: url({{ storage_path('fonts\tahoma.ttf') }}) format("truetype");
		    font-weight: 400;
		    font-style: normal;
		}

        @page {
            margin: 60px 25px;
            counter-reset: page;
        }

       /* Define now the real margins of every page in the PDF */
       body {
           margin-top: 3cm;
           margin-left: -0.5cm;
           margin-right: -0.5cm;
           margin-bottom: 2cm;
           font-style: normal;
       }

       .ui.table.bordered {
         border: solid black 1px;
         border-collapse: collapse;
         width: 100%;
         font-family : "tahoma", "Verdana", sans-serif;
       }

       .ui.table.bordered td {
         border: solid black .5px;
         border-collapse: collapse;
         /*padding:10px;*/
         padding:5px;
       }

       .ui.table.bordered td.center.aligned {
         text-align : center;
       }

       .page-break {
            page-break-after: always;
       }

       #watermark {
	        position: fixed;
	        font-size: 80px;
		    top: 40%;
		    width: 100%;
		    text-align: center;
		    opacity: .2;
		    transform: rotate(30deg);
		    transform-origin: 50% 50%;
		    z-index: 101;
	    }

       .pad{
       	padding-top: 5px;
       	padding-bottom: 5px;
       }

       /* Define the header rules */
        header {
            position: fixed;
            top: -40px;
            left: 0px;
            right: 0px;
            height: 10px;

            /* Extra personal styles */
            text-align: center;
            /*line-height: 35px;*/

            font-size: 11px;
        }
		
       main {
         position: sticky;
         font-size : 12px;
         padding-top : -1cm;
         margin-top : 2px;
         border : none;
         width: 100%;
       }

       main p {
         margin-left: 5px;
         margin-right: 5px;
       }

       /* Define the footer rules */
       footer {
           position: fixed;
           bottom: -60px;
           left: 0px;
           right: 0px;
           height: 50px;

           /* Extra personal styles */
           text-align: center;
           line-height: 35px;
       }
/*       .page-num:after { 
       	counter-increment: pages; 
       	content: counter(page) " of " counter(pages); 
       }*/
       table.page_content {width: 100%; border: none; padding: 1mm; font-size: 11px;font-family : "tahoma", "Verdana", sans-serif;}
       table.page_header {width: 100%; border: none; padding: 2mm; font-size: 11px;font-family : "tahoma", "Verdana", sans-serif; text-align: left; margin-left: 50px;margin-top: 50px;}
       table.page_content_header {width: 100%; font-size: 11px;border-collapse: collapse;font-family : "tahoma", "Verdana", sans-serif;text-align: center; padding-bottom: 1px;
       	padding-top: 1px;}
       /*.footer .page-number:after {
         content: counter(page);
       }*/
</style>
</head>
<body>
	@if(!is_null($records->parent_id))
		@php 
			$max = getmaxparent($records->parent_id);
		@endphp
		@if($max == $records->revisi)
	    @elseif($max > $records->revisi)
	    	@if(checkParent($records->parent_id, $records->id) >= 3)
				<div id="watermark">
			        DIGANTI
			    </div>
	    	@endif
	    @endif
	@endif
    <header>
    	<table border="1" class="page_content ui table" style="margin-top: 10px;page-break-inside: auto;padding:5px;margin-left: 100px;">
    		<tbody>
    			<tr style="">
    				<td style="text-align: left;border: 1px #ffffff;" colspan="2">
    					<table style="">
    						<tbody>
    							<tr>
    								<td style="width: 100px;padding-left: -10px;"><img src="{{ asset('src/img/logo.png') }}" width="95px" height="80px"></td>
    								<td style="width: 670px;text-align: center;"><h2>RENCANA KERJA INTERNAL AUDIT (RKIA) TAHUN {{ ($records->tahun) }}</h2><h3 style="margin-top:-17px;">PT. WASKITA KARYA (PERSERO) TBK.</h3></td>
    								<td style="width: 190px">
    									<table style="text-align: right;font-size: 11px;" class="page_content ui table bordered">
    										<tbody>
    											<tr>
    												<td style="text-align: center;padding-bottom: 10px;" colspan="2">Form. IA 01</td>
    											</tr>
    											<tr>
    												<td style="text-align: center;padding-bottom: 10px;">Edisi : @if($edisi) {{ $edisi }}  @endif</td>
    												<td style="text-align: center;padding-bottom: 10px;">Revisi : @if($revisi) {{ $revisi }}  @endif</td>
    											</tr>
    										</tbody>
    									</table>
    								</td>
    							</tr>
    						</tbody>
    					</table>
    				</td>
    			</tr>
    		</tbody>
		</table>
  	</header>
	<main style="margin-top: 50px;">
	  	<table style="margin-left: 900px;margin-top: -30px;">
	  		<tr>
	  			<td>
				    <span style="color: black; font-weight: bold; font-style: italic;position: absolute;">Revisi : {{ $records->revisi }}</span>
	  			</td>
	  		</tr>
	  	</table>
		<table  border="1" class="page_content ui table bordered" style="margin-top: -40px;page-break-inside: auto;font-size:10px;">
			<thead>
				<tr>
					<td style="width: 5px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;" rowspan="2">No</td>
					<td style="width: 90px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;" rowspan="2"></td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;" rowspan="2">Lokasi</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;" rowspan="2">NK Total</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;" rowspan="2">SNK {{ ($records->tahun-1) }}</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;" rowspan="2">Progress s.d {{ ($records->tahun-1) }}</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;" rowspan="2">BK/PU</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;" rowspan="2">MAPP</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;" colspan="2">Waktu Pelaksanaan</td>
					<td style="width: 30px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;" colspan="12">Rencana Tahun {{ ($records->tahun) }}</td>
					<td style="width: 30px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;" rowspan="2">TIM AUDITOR</td>
				</tr>
				<tr>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Mulai</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Selesai</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Jan</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Feb</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Mar</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Apr</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Mei</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Jun</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Jul</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Agu</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Sep</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Okt</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Nov</td>
					<td style="width: 15px !important;text-align: center;font-weight: bold;font-size:9px;background-color: #F5F5F5;">Des</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="text-align: center;font-size:9.5px;font-weight: bold;">I</td>
					<td style="font-size:9.5px;width: 90px !important;text-align: justify;font-weight: bold;">KEGIATAN AUDIT</td>
					<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
				</tr>
				<tr>
					<td style="text-align: center;font-size:9.5px;font-weight: bold;">A</td>
					<td style="font-size:9.5px;width: 90px !important;text-align: justify;font-weight: bold;">SISA NILAI KONTRAK {{ ($records->tahun-1) }}</td>
					<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
				</tr>
				@foreach(getNamaObjectBU() as $bu)
					<tr>
						<td style="text-align: center;font-size:9px;font-weight: bold;"></td>
						<td style="font-size:9px;width: 90px !important;text-align: justify;font-weight: bold;">{{ $bu->nama }}</td>
						<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					</tr>
					@php 
						$j=1;
					@endphp
					@foreach($records->detailrencana->where('tipe_object', 2)->where('jenis', 1)->where('keterangan', 1) as $i => $data)
						@if($bu->id == getParentBu($data->object_id))
							@php 
								$pieces = explode(" ", $data->rencana);
								if(count($data->detailuser->where('tipe', 0)) > 0){
									$op = array();
									foreach($data->detailuser->where('tipe', 0) as $user_op){
											$op[]= $user_op->user->inisial;
									}
									$ops = implode(", ", $op);
								}else{
									$ops ='-';
								}
								if(count($data->detailuser->where('tipe', 1)) > 0){
									$keu = array();
									foreach($data->detailuser->where('tipe', 1) as $user_keu){
											$keu[]= $user_keu->user->inisial;
									}
									$keus = implode(", ", $keu);
								}else{
									$keus ='-';
								}

								if(count($data->detailuser->where('tipe', 2)) > 0){
									$si = array();
									foreach($data->detailuser->where('tipe', 2) as $user_si){
											$si[]= $user_si->user->inisial;
									}
									$sis = implode(", ", $si);
								}else{
									$sis ='-';
								}
							@endphp
							<tr>
								<td style="text-align: center;font-size:9px;">{{ $j }}</td>
								<td style="font-size:9px;width: 90px !important;text-align: justify;">{!! object_audit($data->tipe_object, $data->object_id) !!}</td>
								<td style="font-size:9px;width: 15px !important;text-align: justify;">{{ getlokasi($data->object_id)}}</td>
								<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($data->nilai_kontrak, 0, '.', '.') }}</td>
								<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($data->snk, 0, '.', '.') }}</td>
								<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($data->progress, 0, '.', '.') }}</td>
								<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($data->bkpu, 0, '.', '.') }}</td>
								<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($data->mapp, 0, '.', '.') }}</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;">{{ DateToString($data->tgl_mulai) }}</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;">{{ DateToString($data->tgl_selesai) }}</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Januari') background-color:green; color:white;@endif">@if($pieces[3] == 'Januari') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Februari') background-color:green; color:white;@endif">@if($pieces[3] == 'Februari') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Maret') background-color:green; color:white;@endif">@if($pieces[3] == 'Maret') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'April') background-color:green; color:white;@endif">@if($pieces[3] == 'April') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Mei') background-color:green; color:white;@endif">@if($pieces[3] == 'Mei') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Juni') background-color:green; color:white;@endif">@if($pieces[3] == 'Juni') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Juli') background-color:green; color:white;@endif">@if($pieces[3] == 'Juli') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Agustus') background-color:green; color:white;@endif">@if($pieces[3] == 'Agustus') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'September') background-color:green; color:white;@endif">@if($pieces[3] == 'September') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Oktober') background-color:green; color:white;@endif">@if($pieces[3] == 'Oktober') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'November') background-color:green; color:white;@endif">@if($pieces[3] == 'November') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieces[3] == 'Desember') background-color:green; color:white;@endif">@if($pieces[3] == 'Desember') {{ $pieces[2] }} @endif</td>
								<td style="font-size:9px;text-align: center;width: 15px !important;">{{ $ops }}, {{ $keus }}, {{ $sis }}</td>
							</tr>
							@php 
							$j++;
							@endphp
						@else 
						@endif
					@endforeach
				@endforeach
				<tr>
					<td style="text-align: center;font-size:9.5px;font-weight: bold;">B</td>
					<td style="font-size:9.5px;width: 90px !important;text-align: justify;font-weight: bold;">NILAI KONTRAK BARU {{ ($records->tahun-1) }}</td>
					<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
				</tr>
				@php 
					$k=1;
				@endphp
				@foreach($records->detailrencana->where('tipe_object', 2)->where('jenis', 0)->where('keterangan', 1) as $i => $datas)
					@php 
						$piecess = explode(" ", $datas->rencana);
					@endphp
					<tr>
						@if(count($datas->detailuser->where('tipe', 0)) > 0)
							@php 	
								$op = array();
							@endphp
							@foreach($datas->detailuser->where('tipe', 0) as $user_op)
								@php 	
									$op[]= $user_op->user->inisial;
								@endphp
							@endforeach
							@php 
							 $datas1 = implode(", ", $op);
							@endphp
						@else 
							@php 
							 $datas1 = '-';
							@endphp
						@endif

						@if(count($datas->detailuser->where('tipe', 1)) > 0)
							@php 	
								$keu = array();
							@endphp
							@foreach($datas->detailuser->where('tipe', 1) as $user_keu)
								@php 	
									$keu[]= $user_keu->user->inisial;
								@endphp
							@endforeach
							@php 
							 $datas2 = implode(", ", $keu);
							@endphp
						@else 
							@php 
							 $datas2 = '-';
							@endphp
						@endif

						@if(count($datas->detailuser->where('tipe', 2)) > 0)
							@php 	
								$sis = array();
							@endphp
							@foreach($datas->detailuser->where('tipe', 2) as $user_sis)
								@php 	
									$sis[]= $user_sis->user->inisial;
								@endphp
							@endforeach
							@php 
							 $datas3 = implode(", ", $sis);
							@endphp
						@else 
							@php 
							 $datas3 = '-';
							@endphp
						@endif
						<td style="text-align: center;font-size:9px;">{{ $k }}</td>
						<td style="font-size:9px;width: 90px !important;text-align: justify;">{!! object_audit($datas->tipe_object, $datas->object_id) !!}</td>
						<td style="font-size:9px;width: 15px !important;text-align: justify;">{{ getlokasi($datas->object_id)}}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($datas->nilai_kontrak, 0, '.', '.') }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($datas->snk, 0, '.', '.') }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($datas->progress, 0, '.', '.') }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($datas->bkpu, 0, '.', '.') }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($datas->mapp, 0, '.', '.') }}</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;">{{ DateToString($datas->tgl_mulai) }}</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;">{{ DateToString($datas->tgl_selesai) }}</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'Januari') background-color:green; color:white;@endif">@if($piecess[3] == 'Januari') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'Februari') background-color:green; color:white;@endif">@if($piecess[3] == 'Februari') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'Maret') background-color:green; color:white;@endif">@if($piecess[3] == 'Maret') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'April') background-color:green; color:white;@endif">@if($piecess[3] == 'April') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'Mei') background-color:green; color:white;@endif">@if($piecess[3] == 'Mei') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'Juni') background-color:green; color:white;@endif">@if($piecess[3] == 'Juni') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'Juli') background-color:green; color:white;@endif">@if($piecess[3] == 'Juli') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'Agustus') background-color:green; color:white;@endif">@if($piecess[3] == 'Agustus') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'September') background-color:green; color:white;@endif">@if($piecess[3] == 'September') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'Oktober') background-color:green; color:white;@endif">@if($piecess[3] == 'Oktober') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'November') background-color:green; color:white;@endif">@if($piecess[3] == 'November') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecess[3] == 'Desember') background-color:green; color:white;@endif">@if($piecess[3] == 'Desember') {{ $piecess[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;">{{ $datas1 }}, {{ $datas2 }}, {{ $datas3 }}</td>
					</tr>
					@php 
					$k++;
						@endphp
				@endforeach
				<tr>
					<td style="text-align: center;font-size:9.5px;font-weight: bold;">C</td>
					<td style="font-size:9.5px;width: 90px !important;text-align: justify;font-weight: bold;">PROYEK SELESAI</td>
					<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
				</tr>
				@php 
					$l=1;
				@endphp
				@foreach($records->detailrencana->where('tipe_object', 2)->where('jenis', 2)->where('keterangan', 1) as $i => $datax)
					@php 
						$piecex = explode(" ", $datax->rencana);
					@endphp
					<tr>
						@if(count($datax->detailuser->where('tipe', 0)) > 0)
							@php 	
								$op = array();
							@endphp
							@foreach($datax->detailuser->where('tipe', 0) as $user_op)
								@php 	
									$op[]= $user_op->user->inisial;
								@endphp
							@endforeach
							@php 
							 $datax1 = implode(", ", $op);
							@endphp
						@else 
							@php 
							 $datax1 = '-';
							@endphp
						@endif

						@if(count($datax->detailuser->where('tipe', 1)) > 0)
							@php 	
								$keu = array();
							@endphp
							@foreach($datax->detailuser->where('tipe', 1) as $user_keu)
								@php 	
									$keu[]= $user_keu->user->inisial;
								@endphp
							@endforeach
							@php 
							 $datax2 = implode(", ", $keu);
							@endphp
						@else 
							@php 
							 $datax2 = '-';
							@endphp
						@endif

						@if(count($datax->detailuser->where('tipe', 2)) > 0)
							@php 	
								$sis = array();
							@endphp
							@foreach($datax->detailuser->where('tipe', 2) as $user_sis)
								@php 	
									$sis[]= $user_sis->user->inisial;
								@endphp
							@endforeach
							@php 
							 $datax3 = implode(", ", $sis);
							@endphp
						@else 
							@php 
							 $datax3 = '-';
							@endphp
						@endif
						<td style="text-align: center;font-size:9px;">{{ $l }}</td>
						<td style="font-size:9px;width: 90px !important;text-align: justify;">{!! object_audit($datax->tipe_object, $datax->object_id) !!}</td>
						<td style="font-size:9px;width: 15px !important;text-align: justify;">{{ getlokasi($datax->object_id)}}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($datax->nilai_kontrak, 0, '.', '.') }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($datax->snk, 0, '.', '.') }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($datax->progress, 0, '.', '.') }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($datax->bkpu, 0, '.', '.') }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;">{{ number_format($datax->mapp, 0, '.', '.') }}</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;">{{ DateToString($datax->tgl_mulai) }}</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;">{{ DateToString($datax->tgl_selesai) }}</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecex[3] == 'Januari') background-color:green; color:white;@endif">@if($piecex[3] == 'Januari') {{ $piecex[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecex[3] == 'Februari') background-color:green; color:white;@endif">@if($piecex[3] == 'Februari') {{ $piecex[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecex[3] == 'Maret') background-color:green; color:white;@endif">@if($piecex[3] == 'Maret') {{ $piecex[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecex[3] == 'April') background-color:green; color:white;@endif">@if($piecex[3] == 'April') {{ $piecex[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecex[3] == 'Mei') background-color:green; color:white;@endif">@if($piecex[3] == 'Mei') {{ $piecex[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecex[3] == 'Juni') background-color:green; color:white;@endif">@if($piecex[3] == 'Juni') {{ $piecex[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecex[3] == 'Juli') background-color:green; color:white;@endif">@if($piecex[3] == 'Juli') {{ $piecex[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecex[3] == 'Agustus') background-color:green; color:white;@endif">@if($piecex[3] == 'Agustus') {{ $piecex[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecex[3] == 'September') background-color:green; color:white;@endif">@if($piecex[3] == 'September') {{ $piecex[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecex[3] == 'Oktober') background-color:green; color:white;@endif">@if($piecex[3] == 'Oktober') {{ $piecex[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecex[3] == 'November') background-color:green; color:white;@endif">@if($piecex[3] == 'November') {{ $piecex[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecex[3] == 'Desember') background-color:green; color:white;@endif">@if($piecex[3] == 'Desember') {{ $piecex[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;">{{ $datax1 }}, {{ $datax2 }}, {{ $datax3 }}</td>
					</tr>
					@php 
					$l++;
						@endphp
				@endforeach
				<tr>
					<td style="text-align: center;font-size:9.5px;font-weight: bold;">D</td>
					<td style="font-size:9.5px;width: 90px !important;text-align: justify;font-weight: bold;">BUSINESS UNIT (BU)</td>
					<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
				</tr>
				@foreach(getNamaObjectBU() as $keya => $bu)
					@php 
						$rencana1=null;
						$bu_nama = $records->detailrencana->where('tipe_object', 0)->where('object_id', $bu->id)->where('keterangan', 1)->first();
					@endphp
					@if($records->detailrencana->where('tipe_object', 0)->where('object_id', $bu->id)->where('keterangan', 1)->first())
					@php 
						$rencana1 = explode(" ", $records->detailrencana->where('tipe_object', 0)->where('object_id', $bu->id)->where('keterangan', 1)->first()->rencana);
					@endphp
					@endif
					<tr>
						<td style="text-align: center;font-size:9px;font-weight: bold;">{{ $keya+1 }}</td>
						<td style="font-size:9px;width: 90px !important;text-align: justify;font-weight: bold;">{{ $bu->nama }}</td>
						<td style="font-size:9px;width: 15px !important;text-align: justify;">{{ $bu->alamat }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						@if($rencana1)
							@if(count($bu_nama->detailuser->where('tipe', 0)) > 0)
								@php 	
									$op = array();
								@endphp
								@foreach($bu_nama->detailuser->where('tipe', 0) as $user_op)
									@php 	
										$op[]= $user_op->user->inisial;
									@endphp
								@endforeach
								@php 
								 $ops = implode(", ", $op);
								@endphp
							@else 
								@php 
								 $ops = '-';
								@endphp
							@endif

							@if(count($bu_nama->detailuser->where('tipe', 1)) > 0)
								@php 	
									$keu = array();
								@endphp
								@foreach($bu_nama->detailuser->where('tipe', 1) as $user_keu)
									@php 	
										$keu[]= $user_keu->user->inisial;
									@endphp
								@endforeach
								@php 
								 $keus = implode(", ", $keu);
								@endphp
							@else 
								@php 
								 $keus ='-';
								@endphp
							@endif

							@if(count($bu_nama->detailuser->where('tipe', 2)) > 0)
								@php 	
									$sis = array();
								@endphp
								@foreach($bu_nama->detailuser->where('tipe', 2) as $user_sis)
									@php 	
										$sis[]= $user_sis->user->inisial;
									@endphp
								@endforeach
								@php 
								 $siss = implode(", ", $sis);
								@endphp
							@else 
								@php 
								 $siss = '-';
								@endphp
							@endif
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'Januari') background-color:green; color:white;@endif">@if($rencana1[3] == 'Januari') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'Februari') background-color:green; color:white;@endif">@if($rencana1[3] == 'Februari') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'Maret') background-color:green; color:white;@endif">@if($rencana1[3] == 'Maret') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'April') background-color:green; color:white;@endif">@if($rencana1[3] == 'April') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'Mei') background-color:green; color:white;@endif">@if($rencana1[3] == 'Mei') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'Juni') background-color:green; color:white;@endif">@if($rencana1[3] == 'Juni') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'Juli') background-color:green; color:white;@endif">@if($rencana1[3] == 'Juli') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'Agustus') background-color:green; color:white;@endif">@if($rencana1[3] == 'Agustus') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'September') background-color:green; color:white;@endif">@if($rencana1[3] == 'September') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'Oktober') background-color:green; color:white;@endif">@if($rencana1[3] == 'Oktober') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'November') background-color:green; color:white;@endif">@if($rencana1[3] == 'November') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana1[3] == 'Desember') background-color:green; color:white;@endif">@if($rencana1[3] == 'Desember') {{ $rencana1[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;">{{ $ops }}, {{ $keus }}, {{ $siss }}</td>
						@else 
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						@endif
					</tr>
				@endforeach
				<tr>
					<td style="text-align: center;font-size:9.5px;font-weight: bold;">E</td>
					<td style="font-size:9.5px;width: 90px !important;text-align: justify;font-weight: bold;">CORPORATE OFFICE (CO)</td>
					<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
				</tr>
				@foreach(getNamaObjectCO() as $keyb => $co)
					@php 
						$rencana2 = null;
						$co_nama = $records->detailrencana->where('tipe_object', 1)->where('object_id', $co->id)->where('keterangan', 1)->first();
					@endphp
					@if($records->detailrencana->where('tipe_object', 1)->where('object_id', $co->id)->where('keterangan', 1)->first())
						@php 
							$rencana2 = explode(" ", $records->detailrencana->where('tipe_object', 1)->where('object_id', $co->id)->where('keterangan', 1)->first()->rencana);
						@endphp
					@endif
					<tr>
						<td style="text-align: center;font-size:9px;font-weight: bold;">{{ $keyb+1 }}</td>
						<td style="font-size:9px;width: 90px !important;text-align: justify;font-weight: bold;">{{ $co->nama }}</td>
						<td style="font-size:9px;width: 15px !important;text-align: justify;">{{ $co->alamat }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						@if($rencana2)
							@if(count($co_nama->detailuser->where('tipe', 0)) > 0)
								@php 	
									$op = array();
								@endphp
								@foreach($co_nama->detailuser->where('tipe', 0) as $user_op)
									@php 	
										$op[]= $user_op->user->inisial;
									@endphp
								@endforeach
								@php 
								 $co_nama1 = implode(", ", $op);
								@endphp
							@else 
								@php 
								 $co_nama1 = '-';
								@endphp
							@endif

							@if(count($co_nama->detailuser->where('tipe', 1)) > 0)
								@php 	
									$keu = array();
								@endphp
								@foreach($co_nama->detailuser->where('tipe', 1) as $user_keu)
									@php 	
										$keu[]= $user_keu->user->inisial;
									@endphp
								@endforeach
								@php 
								 $co_nama2 = implode(", ", $keu);
								@endphp
							@else 
								@php 
								 $co_nama2 = '-';
								@endphp
							@endif

							@if(count($co_nama->detailuser->where('tipe', 2)) > 0)
								@php 	
									$sis = array();
								@endphp
								@foreach($co_nama->detailuser->where('tipe', 2) as $user_sis)
									@php 	
										$sis[]= $user_sis->user->inisial;
									@endphp
								@endforeach
								@php 
								 $co_nama3 = implode(", ", $sis);
								@endphp
							@else 
								@php 
								 $co_nama3 = '-';
								@endphp
							@endif
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'Januari') background-color:green; color:white;@endif">@if($rencana2[3] == 'Januari') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'Februari') background-color:green; color:white;@endif">@if($rencana2[3] == 'Februari') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'Maret') background-color:green; color:white;@endif">@if($rencana2[3] == 'Maret') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'April') background-color:green; color:white;@endif">@if($rencana2[3] == 'April') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'Mei') background-color:green; color:white;@endif">@if($rencana2[3] == 'Mei') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'Juni') background-color:green; color:white;@endif">@if($rencana2[3] == 'Juni') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'Juli') background-color:green; color:white;@endif">@if($rencana2[3] == 'Juli') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'Agustus') background-color:green; color:white;@endif">@if($rencana2[3] == 'Agustus') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'September') background-color:green; color:white;@endif">@if($rencana2[3] == 'September') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'Oktober') background-color:green; color:white;@endif">@if($rencana2[3] == 'Oktober') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'November') background-color:green; color:white;@endif">@if($rencana2[3] == 'November') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana2[3] == 'Desember') background-color:green; color:white;@endif">@if($rencana2[3] == 'Desember') {{ $rencana2[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;">{{ $co_nama1 }}, {{ $co_nama2 }}, {{ $co_nama3 }}</td>
						@else 
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						@endif
					</tr>
				@endforeach
				<tr>
					<td style="text-align: center;font-size:9.5px;font-weight: bold;">F</td>
					<td style="font-size:9.5px;width: 90px !important;text-align: justify;font-weight: bold;">ANAK PERUSAHAAN</td>
					<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
				</tr>
				@foreach(getNamaObjectAP() as $keyc => $ap)
					@php 
						$rencana3 = null;
						$ap_nama = $records->detailrencana->where('tipe_object', 3)->where('object_id', $ap->id)->first();
					@endphp
					@if($records->detailrencana->where('tipe_object', 3)->where('object_id', $ap->id)->first())
						@php 
							$rencana3 = explode(" ", $records->detailrencana->where('tipe_object', 3)->where('object_id', $ap->id)->first()->rencana);
						@endphp
					@endif
					<tr>
						<td style="text-align: center;font-size:9px;font-weight: bold;">{{ $keyc+1 }}</td>
						<td style="font-size:9px;width: 90px !important;text-align: justify;font-weight: bold;">{{ $ap->nama }}</td>
						<td style="font-size:9px;width: 15px !important;text-align: justify;">{{ $ap->alamat }}</td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						@if($rencana3)
							@if(count($ap_nama->detailuser->where('tipe', 0)) > 0)
								@php 	
									$op = array();
								@endphp
								@foreach($ap_nama->detailuser->where('tipe', 0) as $user_op)
									@php 	
										$op[]= $user_op->user->inisial;
									@endphp
								@endforeach
								@php 
								 $ap_nama1 = implode(", ", $op);
								@endphp
							@else 
								@php 
								 $ap_nama1 = '-';
								@endphp
							@endif

							@if(count($ap_nama->detailuser->where('tipe', 1)) > 0)
								@php 	
									$keu = array();
								@endphp
								@foreach($ap_nama->detailuser->where('tipe', 1) as $user_keu)
									@php 	
										$keu[]= $user_keu->user->inisial;
									@endphp
								@endforeach
								@php 
								 $ap_nama2 = implode(", ", $keu);
								@endphp
							@else 
								@php 
								 $ap_nama2 = '-';
								@endphp
							@endif

							@if(count($ap_nama->detailuser->where('tipe', 2)) > 0)
								@php 	
									$sis = array();
								@endphp
								@foreach($ap_nama->detailuser->where('tipe', 2) as $user_sis)
									@php 	
										$sis[]= $user_sis->user->inisial;
									@endphp
								@endforeach
								@php 
								 $ap_nama3 = implode(", ", $sis);
								@endphp
							@else 
								@php 
								 $ap_nama3 = '-';
								@endphp
							@endif
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'Januari') background-color:green; color:white;@endif">@if($rencana3[3] == 'Januari') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'Februari') background-color:green; color:white;@endif">@if($rencana3[3] == 'Februari') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'Maret') background-color:green; color:white;@endif">@if($rencana3[3] == 'Maret') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'April') background-color:green; color:white;@endif">@if($rencana3[3] == 'April') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'Mei') background-color:green; color:white;@endif">@if($rencana3[3] == 'Mei') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'Juni') background-color:green; color:white;@endif">@if($rencana3[3] == 'Juni') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'Juli') background-color:green; color:white;@endif">@if($rencana3[3] == 'Juli') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'Agustus') background-color:green; color:white;@endif">@if($rencana3[3] == 'Agustus') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'September') background-color:green; color:white;@endif">@if($rencana3[3] == 'September') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'Oktober') background-color:green; color:white;@endif">@if($rencana3[3] == 'Oktober') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'November') background-color:green; color:white;@endif">@if($rencana3[3] == 'November') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;@if($rencana3[3] == 'Desember') background-color:green; color:white;@endif">@if($rencana3[3] == 'Desember') {{ $rencana3[2] }} @endif</td>
							<td style="font-size:9px;text-align: center;width: 15px !important;">{{ $ap_nama1 }}, {{ $ap_nama2 }}, {{ $ap_nama3 }}</td>
						@else 
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
							<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						@endif
					</tr>
				@endforeach
				<tr>
					<td style="text-align: center;font-size:9.5px;font-weight: bold;">II</td>
					<td style="font-size:9.5px;width: 90px !important;text-align: justify;font-weight: bold;">KEGIATAN KONSULTASI</td>
					<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
				</tr>
				@php 
					$y=1;
				@endphp
				@foreach($records->detailrencana->where('tipe', 1)->where('keterangan', 1) as $keyd => $konsultasi)
					@php 
						$pieceszz = explode(" ", $konsultasi->rencana);
					@endphp
					@if(count($konsultasi->detailuser->where('tipe', 0)) > 0)
						@php 	
							$op = array();
						@endphp
						@foreach($konsultasi->detailuser->where('tipe', 0) as $user_op)
							@php 	
								$op[]= $user_op->user->inisial;
							@endphp
						@endforeach
						@php 
						 $konsultasi1 = implode(", ", $op);
						@endphp
					@else 
						@php 
						 $konsultasi1 = '-';
						@endphp
					@endif

					@if(count($konsultasi->detailuser->where('tipe', 1)) > 0)
						@php 	
							$keu = array();
						@endphp
						@foreach($konsultasi->detailuser->where('tipe', 1) as $user_keu)
							@php 	
								$keu[]= $user_keu->user->inisial;
							@endphp
						@endforeach
						@php 
						 $konsultasi2 = implode(", ", $keu);
						@endphp
					@else 
						@php 
						 $konsultasi2 = '-';
						@endphp
					@endif

					@if(count($konsultasi->detailuser->where('tipe', 2)) > 0)
						@php 	
							$sis = array();
						@endphp
						@foreach($konsultasi->detailuser->where('tipe', 2) as $user_sis)
							@php 	
								$sis[]= $user_sis->user->inisial;
							@endphp
						@endforeach
						@php 
						 $konsultasi3 = implode(", ", $sis);
						@endphp
					@else 
						@php 
						 $konsultasi3 = '-';
						@endphp
					@endif
					<tr>
						<td style="text-align: center;font-size:9px;">{{$y}}</td>
						<td style="font-size:9px;width: 90px !important;text-align: justify;">{{ $konsultasi->konsultasi->nama }}</td>
						<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieceszz[3] == 'Januari') background-color:green; color:white;@endif">@if($pieceszz[3] == 'Januari') {{ $pieceszz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieceszz[3] == 'Februari') background-color:green; color:white;@endif">@if($pieceszz[3] == 'Februari') {{ $pieceszz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieceszz[3] == 'Maret') background-color:green; color:white;@endif">@if($pieceszz[3] == 'Maret') {{ $pieceszz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieceszz[3] == 'April') background-color:green; color:white;@endif">@if($pieceszz[3] == 'April') {{ $pieceszz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieceszz[3] == 'Mei') background-color:green; color:white;@endif">@if($pieceszz[3] == 'Mei') {{ $pieceszz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieceszz[3] == 'Juni') background-color:green; color:white;@endif">@if($pieceszz[3] == 'Juni') {{ $pieceszz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieceszz[3] == 'Juli') background-color:green; color:white;@endif">@if($pieceszz[3] == 'Juli') {{ $pieceszz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieceszz[3] == 'Agustus') background-color:green; color:white;@endif">@if($pieceszz[3] == 'Agustus') {{ $pieceszz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieceszz[3] == 'September') background-color:green; color:white;@endif">@if($pieceszz[3] == 'September') {{ $pieceszz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieceszz[3] == 'Oktober') background-color:green; color:white;@endif">@if($pieceszz[3] == 'Oktober') {{ $pieceszz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieceszz[3] == 'November') background-color:green; color:white;@endif">@if($pieceszz[3] == 'November') {{ $pieceszz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($pieceszz[3] == 'Desember') background-color:green; color:white;@endif">@if($pieceszz[3] == 'Desember') {{ $pieceszz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;">{{ $konsultasi1 }}, {{ $konsultasi2 }}, {{ $konsultasi3 }}</td>
					</tr>
					@php 
						$y++;
					@endphp
				@endforeach
				<tr>
					<td style="text-align: center;font-size:9.5px;font-weight: bold;">III</td>
					<td style="font-size:9.5px;width: 90px !important;text-align: justify;font-weight: bold;">KEGIATAN LAINNYA</td>
					<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
					<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
				</tr>
				@php 
					$z=1;
				@endphp
				@foreach($records->detailrencana->where('tipe', 2)->where('keterangan', 1) as $keyd => $lain)
					@php 
						$piecesz = explode(" ", $lain->rencana);
					@endphp
					<tr>
						<td style="text-align: center;font-size:9px;">{{$z}}</td>
						<td style="font-size:9px;width: 90px !important;text-align: justify;">
							@if($lain->lain_id == 1000)
							SPIN
							@elseif($lain->lain_id == 1001)
							IACM
							@else
							Counterpart Auditor Eksternal - {{ $lain->lain->nama }}
							@endif
						</td>
						@if(count($lain->detailuser->where('tipe', 0)) > 0)
							@php 	
								$op = array();
							@endphp
							@foreach($lain->detailuser->where('tipe', 0) as $user_op)
								@php 	
									$op[]= $user_op->user->inisial;
								@endphp
							@endforeach
							@php 
							 $lain1 = implode(", ", $op);
							@endphp
						@else 
							@php 
							 $lain1 = '-';
							@endphp
						@endif

						@if(count($lain->detailuser->where('tipe', 1)) > 0)
							@php 	
								$keu = array();
							@endphp
							@foreach($lain->detailuser->where('tipe', 1) as $user_keu)
								@php 	
									$keu[]= $user_keu->user->inisial;
								@endphp
							@endforeach
							@php 
							 $lain2 = implode(", ", $keu);
							@endphp
						@else 
							@php 
							 $lain2 = '-';
							@endphp
						@endif

						@if(count($lain->detailuser->where('tipe', 2)) > 0)
							@php 	
								$sis = array();
							@endphp
							@foreach($lain->detailuser->where('tipe', 2) as $user_sis)
								@php 	
									$sis[]= $user_sis->user->inisial;
								@endphp
							@endforeach
							@php 
							 $lain3 = implode(", ", $sis);
							@endphp
						@else 
							@php 
							 $lain3 = '-';
							@endphp
						@endif
						<td style="font-size:9px;width: 15px !important;text-justify: inter-word;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: right;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;"></td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecesz[3] == 'Januari') background-color:green; color:white;@endif">@if($piecesz[3] == 'Januari') {{ $piecesz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecesz[3] == 'Februari') background-color:green; color:white;@endif">@if($piecesz[3] == 'Februari') {{ $piecesz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecesz[3] == 'Maret') background-color:green; color:white;@endif">@if($piecesz[3] == 'Maret') {{ $piecesz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecesz[3] == 'April') background-color:green; color:white;@endif">@if($piecesz[3] == 'April') {{ $piecesz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecesz[3] == 'Mei') background-color:green; color:white;@endif">@if($piecesz[3] == 'Mei') {{ $piecesz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecesz[3] == 'Juni') background-color:green; color:white;@endif">@if($piecesz[3] == 'Juni') {{ $piecesz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecesz[3] == 'Juli') background-color:green; color:white;@endif">@if($piecesz[3] == 'Juli') {{ $piecesz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecesz[3] == 'Agustus') background-color:green; color:white;@endif">@if($piecesz[3] == 'Agustus') {{ $piecesz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecesz[3] == 'September') background-color:green; color:white;@endif">@if($piecesz[3] == 'September') {{ $piecesz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecesz[3] == 'Oktober') background-color:green; color:white;@endif">@if($piecesz[3] == 'Oktober') {{ $piecesz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecesz[3] == 'November') background-color:green; color:white;@endif">@if($piecesz[3] == 'November') {{ $piecesz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;@if($piecesz[3] == 'Desember') background-color:green; color:white;@endif">@if($piecesz[3] == 'Desember') {{ $piecesz[2] }} @endif</td>
						<td style="font-size:9px;text-align: center;width: 15px !important;">{{ $lain1 }}, {{ $lain2 }}, {{ $lain3 }}</td>
					</tr>
					@php 
						$z++;
					@endphp
				@endforeach
			</tbody>
		</table>
		@php 
			$data_op= array();
			$data_keu = array();
			$data_sis = array();
			foreach($records->detailrencana->where('keterangan', 1) as $key => $users){
				foreach ($users->detailuser as $key => $value) {
					if($value){
						if($value->tipe == 0){
							$data_op[$value->user->inisial]= $value->user->name;
						}elseif($value->tipe == 1){
							$data_keu[$value->user->inisial] = $value->user->name;
						}else{
							$data_sis[$value->user->inisial] = $value->user->name;
						}
					}
				}
			}
		@endphp
		<table class="page_content_header" style="padding-left: -50px;padding-top: 30px;">
            <tr>
                <td style="text-align: left;padding-left: 100px;font-size: 9px;">
                	Keterangan<br>
                    Rencana Minggu Ke : I, II, III, IV<br>
                    Jumlah Auditor : 3 Tim<br><br>
                    Operational Auditor : <br>
                    @foreach(array_unique($data_op) as $x => $op)
                    	@if($x)
						{{ $x }} : {{ $op }}<br>
						@else 
						- : {{ $op }}<br>
						@endif
                    @endforeach
                    <br>
                    System Auditor : <br>
                    @foreach(array_unique($data_sis) as $y => $sis)
						@if($y)
						{{ $y }} : {{ $sis }}<br>
						@else 
						- : {{ $sis }}<br>
						@endif
                    @endforeach
                    <br>
                    Financial Auditor : <br>
                    @foreach(array_unique($data_keu) as $z => $keu)
						@if($z)
						{{ $z }} : {{ $keu }}<br>
						@else 
						- : {{ $keu }}<br>
						@endif
                    @endforeach
                    <br>
                </td>
                <td style="text-align: center;font-size: 9px;"width="250px;">
                    <b>President Director</b><br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    (I Gusti Ngurah Putra)
                </td>
                <td style="text-align: center;font-size: 9px;">
                    <b>SVP Internal Audit</b><br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    (Pius Sutrisno Riyanto)
                </td>
            </tr>
        </table>
	</main>
</body>
</html>