@extends('layouts.form')
@if($record->revisi > 0)
	@section('title', 'RKIA Tahun '.$record->tahun)
	@section('subtitle', 'Revisi '.$record->revisi)
@else 
	@section('title', 'RKIA Tahun '.$record->tahun)
@endif
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body" style="padding-bottom: 0px;">
		@if($record->ket_svp)
			<p style="padding-left: 14px;color:red;">Status <b>Ditolak</b> SVP pada {{DateToStringWday($record->updated_at)}}</p>
			<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
				<tr>
					<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
						<h5 style="font-weight: bold;font-size: 12px;">Keterangan :</h5>
						<p style="text-align: justify;text-justify: inter-word;">{{ $record->ket_svp }}</p>
					</td>
				</tr>
			</table>
		@elseif($record->ket_dirut)
			<p style="padding-left: 14px;color:red;">Status <b>Ditolak</b> Dirut pada {{DateToStringWday($record->updated_at)}}</p>
			<table id="example" class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
				<tr>
					<td scope="col-md-12" colspan="3" style="text-align: left;border: 1px #ffffff;" class="field">
						<h5 style="font-weight: bold;font-size: 12px;">Keterangan :</h5>
						<p style="text-align: justify;text-justify: inter-word;">{{ $record->ket_dirut }}</p>
					</td>
				</tr>
			</table>
		@else
		@endif
		<form action="{{ route($routes.'.saveObject', $record->id) }}" method="POST" id="formData">
			@method('PATCH')
			@csrf
			<div class="loading dimmer padder-v" style="display: none;">
			    <div class="loader"></div>
			</div>
			<input type="hidden" name="id" value="{{ $record->id }}">
			<input type="hidden" name="tahun" value="{{ $record->tahun }}">
			<input type="hidden" name="status" value="0">
			@if($record->detailrencana->count() > 0)
				<div class="panel panel-default data-form">
					@foreach($record->detailrencana as $key => $data)
						@if($key == 0)
							<div class="hitung panel-heading data-detil-{{$key}}" data-id="{{$key}}" data-value="{{$data->object_id}}" data-tipe="{{$data->tipe_object}}" data-tipes="{{$data->tipe}}" style="font-weight: bold;font-size: 13px;text-align: right;">
								<div style="width: 50%;text-align: left;">
									<label style="margin-top: 7px;" class="head-numboor-{{$key}}">{{$key+1}}.</label>
								</div>
								<div style="text-align: right;">
									<button class="btn btn-sm btn-success tambah_object" type="button" data-id="{{$key}}" style="margin-top: -28px;"><i class="fa fa-plus"></i></button>
								</div>
							</div>
						@else
							<div class="hitung panel-heading data-detil-{{$key}}" data-id="{{$key}}" data-value="{{$data->object_id}}" data-tipe="{{$data->tipe_object}}" data-tipes="{{$data->tipe}}" style="font-weight: bold;font-size: 13px;text-align: right;">
								@if($data->flag == 2)
									<div style="width: 50%;text-align: left;">
										<label style="margin-top: 7px;" class="head-numboor-{{$key}}">{{$key+1}}.</label>
									</div>
								@else 
									<div style="width: 50%;text-align: left;">
										<label style="margin-top: 7px;" class="head-numboor-{{$key}}">{{$key+1}}.</label>
									</div>
									<button class="btn btn-sm btn-danger hapus_object" type="button" data-id="{{$key}}" style="margin-top: -19px;"><i class="fa fa-close"></i></button>
								@endif
							</div>
						@endif
						<div class="panel-body data-detil-{{$key}}" style="padding-bottom: 0px;">
							@if($data->flag == 2)
							@else 
								<input type="hidden" name="detail[{{$key}}][detail_id]" value="{{ $data->id }}">
							@endif
							<input type="hidden" name="exists[]" value="{{$data->id}}">
							<table class="table" style="font-size: 12px;">
								<tbody>
									<tr>
										<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">Rencana Kerja</td>
										<td style="width:2px;border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;" class="field">
											<div>
												@if($data->flag == 2)
													@if($data->tipe == 0)
														Audit
													@elseif($data->tipe == 1)
														Kegiatan Konsultasi
													@else 
														Kegiatan Lainnya
													@endif
												@else
													<select data-width="25%" data-id="{{$key}}" class="pilihan selectpicker col-sm-12 show-tick rencana-kerja" data-size="3" name="detail[{{$key}}][tipe]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
														<option value="0" @if($data->tipe == 0) selected @endif>Kegiatan Audit</option>
														<option value="1" @if($data->tipe == 1) selected @endif>Kegiatan Konsultasi</option>
														<option value="2" @if($data->tipe == 2) selected @endif>Kegiatan Lainnya</option>
													</select> 
												@endif
											</div>
										</td>
									</tr>
									<tr style="display: none;" class="konsultasi-{{$key}}">
										<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">Kategori</td>
										<td style="width:2px;border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;" class="field">
											<div>
												@if($data->flag == 2)
													@if($data->tipe == 1)
														{{ $data->konsultasi->nama }}
													@endif
												@else
													<select data-width="50%" data-id="{{$key}}" class="selectpicker form-control show-tick" data-size="3" name="detail[{{$key}}][konsultasi_id]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
										                @foreach(App\Models\Master\Konsultasi::get() as $konsultasi)
										                    <option value="{{ $konsultasi->id }}" @if($data->tipe == 1) @if($data->konsultasi_id == $konsultasi->id) selected @endif @endif>{{ $konsultasi->nama }}</option>
										                @endforeach
										            </select> 
									            @endif   
											</div>
										</td>
									</tr>
									<tr style="display: none;" class="lain-{{$key}}">
										<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">Kategori</td>
										<td style="width:2px;border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;" class="field">
											<div>
												@if($data->flag == 2)
													@if($data->tipe == 2)
														{{ $data->lain->nama }}
													@endif
												@else
													<select data-width="50%" data-id="{{$key}}" class="selectpicker form-control show-tick" data-size="3" name="detail[{{$key}}][lain]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
										                @foreach(App\Models\Master\Lain::get() as $lain)
										                    <option value="{{ $lain->id }}" @if($data->tipe == 2) @if($data->lain_id == $lain->id) selected @endif @endif>Counterpart Auditor Eksternal - {{ $lain->nama }}</option>
										                @endforeach
										                <option value="1000" @if($data->tipe == 2) @if($data->lain_id == '1000') selected @endif @endif>SPIN</option>
										                <option value="1001" @if($data->tipe == 2) @if($data->lain_id == '1001') selected @endif @endif>IACM</option>
										            </select>  
									            @endif  
											</div>
										</td>
									</tr>
									<tr style="display: none;" class="kepala-{{$key}}">
										<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">Kategori</td>
										<td style="width:2px;border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;" class="field">
											<div>
												@if($data->flag == 2)
													@if($data->tipe == 0)
														@if($data->tipe_object == 0)
															<span class="label label-success">Business Unit (BU)</span>
														@elseif($data->tipe_object == 1)
															<span class="label label-info">Corporate Office (CO)</span>
														@elseif($data->tipe_object == 2)
															<span class="label label-default">Project</span>
														@else 
															<span class="label label-primary">Anak Perusahaan</span>
														@endif
													@endif
												@else
													<select data-width="25%" data-id="{{$key}}" class="pilihan selectpicker col-sm-12 show-tick kategori" data-size="3" name="detail[{{$key}}][tipe_object]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
														<option value="0" @if($data->tipe_object == 0) selected @endif>Business Unit (BU)</option>
														<option value="1" @if($data->tipe_object == 1) selected @endif>Corporate Office (CO)</option>
														<option value="2" @if($data->tipe_object == 2) selected @endif>Project</option>
														<option value="3" @if($data->tipe_object == 3) selected @endif>Anak Perusahaan</option>
													</select>
												@endif 
											</div>
										</td>
									</tr>
									<tr style="display: none;" class="kepala-{{$key}}">
										<td style="border: 1px #ffffff;">Objek Audit</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;" class="field">
											<div>
												@if($data->flag == 2)
													{!! object_audit($data->tipe_object, $data->object_id)!!}
												@else
													<select data-id="{{$key}}" data-width="100%" class="selectpicker show-tick object" name="detail[{{$key}}][object_id]" data-size="3" data-style="btn-default" data-live-search="true">
													</select>
												@endif
											</div>
										</td>
									</tr>
									<tr>
										<td style="border: 1px #ffffff;">Rencana</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;" class="field">
											@if($data->flag == 2)
												{{ $data->rencana }}
											@else 
												<input type="text" style="width: 25%" name="detail[{{$key}}][rencana]" class="form-control rencana" placeholder="Rencana" value="{{ $data->rencana }}">
											@endif
										</td>
									</tr>
									<tr>
										<td style="border: 1px #ffffff;">Keterangan</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;" class="field">
											<div>
												@if($data->flag == 2)
													@if($data->keterangan == 0)
														<span class="label label-info">Tidak Dipilih</span>
													@else 
														<span class="label label-primary">Dipilih</span>
													@endif
												@else
													<select data-width="25%" class="selectpicker show-tick" name="detail[{{$key}}][keterangan]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
														<option value="0" @if($data->keterangan == 0) selected @endif>Tidak Dipilih</option>
														<option value="1" @if($data->keterangan == 1) selected @endif>Dipilih</option>
													</select>    
												@endif 
											</div>
										</td>
									</tr>
									<tr>
										<td style="border: 1px #ffffff;">Auditor Operasional</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;" class="field">
											@if($data->flag == 2)
												@if($data->detailuser->where('tipe', 0))
													@php 	
														$op = array();
													@endphp
													@foreach($data->detailuser->where('tipe', 0) as $user_op)
														@php 	
															$op[]= $user_op->user->name;
														@endphp
													@endforeach
													{{ implode(", ", $op) }}
												@else 
												-
												@endif
											@else 
											@php 
												$user_op  = $data->detailuser->where('tipe', 0)->pluck('user_id')->toArray();
											@endphp
												<div>
													<select data-width="100%" class="selectpicker form-control show-tick" name="detail[{{$key}}][operasional][]" data-size="3" data-style="btn-default" data-live-search="true" title="(Pilih Auditor Operasional)" multiple>
														@foreach(App\Models\Auths\User::where('fungsi', 1)->get() as $user)
															<option value="{{ $user->id }}" @if(in_array($user->id, $user_op, TRUE)) selected @endif>{{ $user->name }}</option>
														@endforeach
													</select>
												</div>
											@endif
										</td>
									</tr>
									<tr>
										<td style="border: 1px #ffffff;">Auditor Keuangan</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;" class="field">
											@if($data->flag == 2)
												@if($data->detailuser->where('tipe', 1))
													@php 	
														$keu = array();
													@endphp
													@foreach($data->detailuser->where('tipe', 1) as $user_keu)
														@php 	
															$keu[]= $user_keu->user->name;
														@endphp
													@endforeach
													{{ implode(", ", $keu) }}
												@else 
												-
												@endif
											@else
											@php 
												$user_keu  = $data->detailuser->where('tipe', 1)->pluck('user_id')->toArray();
											@endphp
												<div>
													<select data-width="100%" class="selectpicker form-control show-tick" name="detail[{{$key}}][keuangan][]" data-size="3" data-style="btn-default" data-live-search="true" title="(Pilih Auditor Keuangan)" multiple>
														@foreach(App\Models\Auths\User::where('fungsi', 2)->get() as $user)
															<option value="{{ $user->id }}" @if(in_array($user->id, $user_keu, TRUE)) selected @endif>{{ $user->name }}</option>
														@endforeach
													</select>
												</div>
											@endif
										</td>
									</tr>
									<tr>
										<td style="border: 1px #ffffff;">Auditor Sistem</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;" class="field">
											@if($data->flag == 2)
												@if($data->detailuser->where('tipe', 2))
													@php 	
														$sis = array();
													@endphp
													@foreach($data->detailuser->where('tipe', 2) as $user_sis)
														@php 	
															$sis[]= $user_sis->user->name;
														@endphp
													@endforeach
													{{ implode(", ", $sis) }}
												@else 
												-
												@endif
											@else
											@php 
												$user_sis  = $data->detailuser->where('tipe', 2)->pluck('user_id')->toArray();
											@endphp
												<div>
													<select data-width="100%" class="selectpicker form-control show-tick" name="detail[{{$key}}][sistem][]" data-size="3" data-style="btn-default" data-live-search="true" title="(Pilih Auditor Sistem)" multiple>
														@foreach(App\Models\Auths\User::where('fungsi', 3)->get() as $user)
															<option value="{{ $user->id }}" @if(in_array($user->id, $user_sis, TRUE)) selected @endif>{{ $user->name }}</option>
														@endforeach
													</select>
												</div>
											@endif
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">No AB</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											@if($data->flag == 2)
												{{ getNoAb($data->object_id) }}
											@else 
												<span class="label label-info project-noAb-{{$key}}" style="font-size: 11px;"></span>
											@endif
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">Project ID</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											@if($data->flag == 2)
												{{ getId($data->object_id)}}
											@else 
												<span class="label label-info project-noId-{{$key}}" style="font-size: 11px;"></span>
											@endif
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">Project</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											@if($data->flag == 2)
												{!! object_audit($data->tipe_object, $data->object_id)!!}
											@else 
												<span class="label label-info project-nama-{{$key}}" style="font-size: 11px;"></span>
											@endif
										</td>
									</tr>
									<tr class="detil-{{$key}}" style="display: none;">
										<td style="border: 1px #ffffff;">Lokasi</td>
										<td style="border: 1px #ffffff;">:</td>
										<td style="border: 1px #ffffff;">
											@if($data->flag == 2)
												{{ getlokasi($data->object_id)}}
											@else 
												<span class="label label-primary project-lokasi-{{$key}}" style="font-size: 11px;"></span>
											@endif
										</td>
									</tr>
									@if($data->flag == 2)
										<tr class="detil-{{$key}}" style="display: none;">
											<td style="border: 1px #ffffff;">Tgl Mulai</td>
											<td style="border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;">
												{{ DateToString($data->tgl_mulai) }}
											</td>
										</tr>
										<tr class="detil-{{$key}}" style="display: none;">
											<td style="border: 1px #ffffff;">Tgl Selesai</td>
											<td style="border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;">
												{{ DateToString($data->tgl_selesai) }}
											</td>
										</tr>
										<tr class="detil-{{$key}}" style="display: none;">
											<td style="border: 1px #ffffff;">Jenis Kontrak</td>
											<td style="border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;">
												@if($data->jenis == 0)
													<span class="label label-info">Kontrak Baru</span>
												@elseif($data->jenis == 1)
													<span class="label label-primary">Sisa Nilai Kontrak</span>
												@else
													<span class="label label-success">Project Selesai</span>
												@endif
											</td>
										</tr>
										<tr class="detil-{{$key}}" style="display: none;">
											<td style="border: 1px #ffffff;">SNK {{ ($record->tahun-1) }}</td>
											<td style="border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;">
												{{ number_format($data->snk, 0, '.', '.') }}
											</td>
										</tr>
										<tr class="detil-{{$key}}" style="display: none;">
											<td style="border: 1px #ffffff;">Progress s.d Des {{ ($record->tahun-1) }}</td>
											<td style="border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;">
												{{ number_format($data->progress, 0, '.', '.') }}
											</td>
										</tr>
										<tr class="detil-{{$key}}" style="display: none;">
											<td style="border: 1px #ffffff;">NK Total</td>
											<td style="border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;">
												{{ number_format($data->nilai_kontrak, 0, '.', '.') }}
											</td>
										</tr>
										<tr class="detil-{{$key}}" style="display: none;">
											<td style="border: 1px #ffffff;">Progress (Ra)</td>
											<td style="border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;">
												{{ number_format($data->progress_ra, 0, '.', '.') }}
											</td>
										</tr>
										<tr class="detil-{{$key}}" style="display: none;">
											<td style="border: 1px #ffffff;">Progress (Ri)</td>
											<td style="border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;">
												{{ number_format($data->progress_ri, 0, '.', '.') }}
											</td>
										</tr>
										<tr class="detil-{{$key}}" style="display: none;">
											<td style="border: 1px #ffffff;">Deviasi Progress</td>
											<td style="border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;">
												{{ number_format($data->deviasi, 0, '.', '.') }}
											</td>
										</tr>
										<tr class="detil-{{$key}}" style="display: none;">
											<td style="border: 1px #ffffff;">Progress Risk Impact</td>
											<td style="border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;">
												@if($data->risk_impact == 0)
													<span class="label label-default">Sangat Ringan</span>
												@elseif($data->risk_impact == 1)
													<span class="label label-success">Ringan</span>
												@elseif($data->risk_impact == 2)
													<span class="label label-info">Sedang</span>
												@elseif($data->risk_impact == 3)
													<span class="label label-warning">Berat</span>
												@else 
													<span class="label label-danger">Sangat Berat</span>
												@endif
											</td>
										</tr>
										<tr class="detil-{{$key}}" style="display: none;">
											<td style="border: 1px #ffffff;">MAPP</td>
											<td style="border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;">
												{{ number_format($data->mapp, 0, '.', '.') }}
											</td>
										</tr>
										<tr class="detil-{{$key}}" style="display: none;">
											<td style="border: 1px #ffffff;">BK / PU</td>
											<td style="border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;">
												{{ number_format($data->bkpu, 0, '.', '.') }}
											</td>
										</tr>
										<tr class="detil-{{$key}}" style="display: none;">
											<td style="border: 1px #ffffff;">Deviasi BK / PU</td>
											<td style="border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;">
												{{ number_format($data->deviasi_bkpu, 0, '.', '.') }}
											</td>
										</tr>
										<tr class="detil-{{$key}}" style="display: none;">
											<td style="border: 1px #ffffff;">BK / PU Risk Impact</td>
											<td style="border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;">
												@if($data->bkpu_ri == 0)
													<span class="label label-default">Sangat Ringan</span>
												@elseif($data->bkpu_ri == 1)
													<span class="label label-success">Ringan</span>
												@elseif($data->bkpu_ri == 2)
													<span class="label label-info">Sedang</span>
												@elseif($data->bkpu_ri == 3)
													<span class="label label-warning">Berat</span>
												@else 
													<span class="label label-danger">Sangat Berat</span>
												@endif
											</td>
										</tr>
									@else
										<tr class="detil-{{$key}}" style="display: none;">
											<td style="border: 1px #ffffff;">Detil</td>
											<td style="border: 1px #ffffff;">:</td>
											<td style="border: 1px #ffffff;">
												<div class="form-row">
												    <div class="form-group col-md-4 field start" style="right: 10px;">
												      <label for="validationCustom01">Tgl Mulai</label>
												      <input type="text" name="detail[{{$key}}][tgl_mulai]" class="form-control tgl_start" placeholder="Tgl Mulai" value="{{ $data->tgl_mulai }}">
												    </div>
												    <div class="form-group col-md-4 field end" style="left: 3px;">
												      <label for="validationCustom02">Tgl Selesai</label>
												      <input type="text" name="detail[{{$key}}][tgl_selesai]" class="form-control tgl_end" placeholder="Tgl Selesai" value="{{ $data->tgl_selesai }}">
												    </div>
												    <div class="form-group col-md-4 field" style="left: 16px;">
													      <label for="validationCustom02">Jenis Kontrak</label>
															<select data-width="100%" data-id="{{$key}}" class="selectpicker form-control show-tick jenis" name="detail[{{$key}}][jenis]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
																<option value="0" @if($data->jenis == 0) selected @endif>Kontrak Baru</option>
																<option value="1" @if($data->jenis == 1) selected @endif>Sisa Nilai Kontrak</option>
																<option value="2" @if($data->jenis == 2) selected @endif>Project Selesai</option>
															</select>     
												    </div>
												</div>
												<div class="form-row">
													<div class="form-group col-md-4 field" style="right: 10px;">
														<label>SNK {{ ($record->tahun-1) }}</label>
														<div class="input-group">
															<span class="input-group-addon" style="font-size: 12px;">Rp.</span>
															<input type="text" class="form-control inputmask" name="detail[{{$key}}][snk]" placeholder="SNK {{ ($record->tahun-1) }}" value="{{ number_format($data->snk, 0, '.', '.') }}">
														</div>
													</div>
													<div class="form-group col-md-4 field" style="left: 3px;">
														<label>Progress s.d Des {{ ($record->tahun-1) }}</label>
														<div class="input-group">
															<input type="text" class="form-control inputmask" name="detail[{{$key}}][progress]" placeholder="Progress s.d Des {{ ($record->tahun-1) }}" value="{{ number_format($data->progress, 0, '.', '.') }}">
															<span class="input-group-addon" style="font-size: 12px;">%</span>
														</div>
													</div>
													<div class="form-group col-md-4 field" style="left: 16px;">
														<label>NK Total</label>
														<div class="input-group">
															<span class="input-group-addon" style="font-size: 10px;">Rp.</span>
															<input type="text" class="form-control inputmask" name="detail[{{$key}}][nilai_kontrak]" placeholder="NK Total" value="{{ number_format($data->nilai_kontrak, 0, '.', '.') }}">
														</div>
													</div>
												</div>
												<div class="form-row">
													<div class="form-group col-md-4 field" style="right: 10px;">
														<label for="inputEmail4">Progress (Ra)</label>
														<div class="input-group">
															<input type="text" class="form-control inputmask" name="detail[{{$key}}][progress_ra]" placeholder="Progress (Ra)" value="{{ number_format($data->progress_ra, 0, '.', '.') }}">
															<span class="input-group-addon" style="font-size: 12px;">%</span>
														</div>
													</div>
													<div class="form-group col-md-4 field" style="left: 3px;">
														<label for="inputEmail4">Progress (Ri)</label>
														<div class="input-group">
															<input type="text" class="form-control inputmask" name="detail[{{$key}}][progress_ri]" placeholder="Progress (Ri)" value="{{ number_format($data->progress_ri, 0, '.', '.') }}">
															<span class="input-group-addon" style="font-size: 12px;">%</span>
														</div>
													</div>
													<div class="form-group col-md-4 field" style="left: 16px;">
														<label for="inputEmail4">Deviasi Progress</label>
														<div class="input-group">
															<input type="text" class="form-control inputmask" name="detail[{{$key}}][deviasi]" placeholder="Deviasi Progress" value="{{ number_format($data->deviasi, 0, '.', '.') }}">
															<span class="input-group-addon" style="font-size: 12px;">%</span>
														</div>
													</div>
												</div>
												<div class="form-row">
													<div class="form-group col-md-4 field" style="right: 10px;">
														<label for="inputEmail4">Progress Risk Impact</label>
														<select class="selectpicker form-control show-tick" name="detail[{{$key}}][risk_impact]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
															<option value="0" @if($data->risk_impact == 0) selected @endif>Sangat Ringan</option>
															<option value="1" @if($data->risk_impact == 1) selected @endif>Ringan</option>
															<option value="2" @if($data->risk_impact == 2) selected @endif>Sedang</option>
															<option value="3" @if($data->risk_impact == 3) selected @endif>Berat</option>
															<option value="4" @if($data->risk_impact == 4) selected @endif>Sangat Berat</option>
														</select> 
													</div>
													<div class="form-group col-md-4 field" style="left: 3px;">
														<label for="inputEmail4">MAPP</label>
														<div class="input-group">
															<input type="text" class="form-control inputmask" name="detail[{{$key}}][mapp]" placeholder="MAPP" value="{{ number_format($data->mapp, 0, '.', '.') }}">
															<span class="input-group-addon" style="font-size: 12px;">%</span>
														</div>
													</div>
													<div class="form-group col-md-4 field" style="left: 16px;">
														<label for="inputEmail4">BK / PU</label>
														<div class="input-group">
															<input type="text" class="form-control inputmask" name="detail[{{$key}}][bkpu]" placeholder="BK / PU" value="{{ number_format($data->bkpu, 0, '.', '.') }}">
															<span class="input-group-addon" style="font-size: 12px;">%</span>
														</div>
													</div>
												</div>
												<div class="form-row">
													<div class="form-group col-md-4 field" style="right: 10px;">
														<label for="inputEmail4">Deviasi BK / PU</label>
														<div class="input-group">
															<input type="text" class="form-control inputmask" name="detail[{{$key}}][deviasi_bkpu]" placeholder="Deviasi BK / PU" value="{{ number_format($data->deviasi_bkpu, 0, '.', '.') }}">
															<span class="input-group-addon" style="font-size: 12px;">%</span>
														</div>
													</div>
													<div class="form-group col-md-4 field" style="left: 3px;">
														<label for="inputEmail4">BK / PU Risk Impact</label>
														<select class="selectpicker form-control show-tick" name="detail[{{$key}}][bkpu_ri]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
															<option value="0" @if($data->bkpu_ri == 0) selected @endif>Sangat Ringan</option>
															<option value="1" @if($data->bkpu_ri == 1) selected @endif>Ringan</option>
															<option value="2" @if($data->bkpu_ri == 2) selected @endif>Sedang</option>
															<option value="3" @if($data->bkpu_ri == 3) selected @endif>Berat</option>
															<option value="4" @if($data->bkpu_ri == 4) selected @endif>Sangat Berat</option>
														</select>
													</div>
												</div>
											</td>
										</tr>
									@endif
								</tbody>
							</table>
						</div>
					@endforeach
				</div>
				<input type="hidden" name="last" value="{{$key}}">
			@else 
				<input type="hidden" name="last" value="0">
				<div class="panel panel-default data-form">
					<div class="panel-heading data-detil-0" data-id="0" style="font-weight: bold;font-size: 13px;text-align: right;">
						<div style="width: 50%;text-align: left;">
							<label style="margin-top: 7px;" class="head-numboor-0">1.</label>
						</div>
						<button class="btn btn-sm btn-success tambah_object"type="button" style="margin-top: -28px;"><i class="fa fa-plus"></i></button>
					</div>
					<div class="panel-body data-detil-0" style="padding-bottom: 0px;">
						<table class="table" style="font-size: 12px;">
							<tbody>
								<tr>
									<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">Rencana Kerja</td>
									<td style="width:2px;border: 1px #ffffff;">:</td>
									<td style="border: 1px #ffffff;" class="field">
										<div>
											<select data-width="25%" data-id="0" class="pilihan selectpicker col-sm-12 show-tick rencana-kerja" data-size="3" name="detail[0][tipe]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
												<option value="0">Kegiatan Audit</option>
												<option value="1">Kegiatan Konsultasi</option>
												<option value="2">Kegiatan Lainnya</option>
											</select> 
										</div>
									</td>
								</tr>
								<tr style="display: none;" class="konsultasi-0">
									<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">Kategori</td>
									<td style="width:2px;border: 1px #ffffff;">:</td>
									<td style="border: 1px #ffffff;" class="field">
										<div>
											<select data-width="50%" data-id="0" class="selectpicker form-control show-tick" data-size="3" name="detail[0][konsultasi_id]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
								                @foreach(App\Models\Master\Konsultasi::get() as $konsultasi)
								                    <option value="{{ $konsultasi->id }}">{{ $konsultasi->nama }}</option>
								                @endforeach
								            </select>    
										</div>
									</td>
								</tr>
								<tr style="display: none;" class="lain-0">
									<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">Kategori</td>
									<td style="width:2px;border: 1px #ffffff;">:</td>
									<td style="border: 1px #ffffff;" class="field">
										<div>
											<select data-width="50%" data-id="0" class="selectpicker form-control show-tick" data-size="3" name="detail[0][lain]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
								                @foreach(App\Models\Master\Lain::get() as $lain)
								                    <option value="{{ $lain->id }}">Counterpart Auditor Eksternal - {{ $lain->nama }}</option>
								                @endforeach
									                <option value="1000">SPIN</option>
									                <option value="1001">IACM</option>
								            </select>    
										</div>
									</td>
								</tr>
								<tr style="display: none;" class="kepala-0">
									<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">Kategori</td>
									<td style="width:2px;border: 1px #ffffff;">:</td>
									<td style="border: 1px #ffffff;" class="field">
										<div>
											<select data-width="25%" data-id="0" class="pilihan selectpicker col-sm-12 show-tick kategori" data-size="3" name="detail[0][tipe_object]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
												<option value="0">Business Unit (BU)</option>
												<option value="1">Corporate Office (CO)</option>
												<option value="2">Project</option>
												<option value="3">Anak Perusahaan</option>
											</select> 
										</div>
									</td>
								</tr>
								<tr style="display: none;" class="kepala-0">
									<td style="border: 1px #ffffff;">Objek Audit</td>
									<td style="border: 1px #ffffff;">:</td>
									<td style="border: 1px #ffffff;" class="field">
										<div>
											<select data-id="0" data-width="100%" class="selectpicker show-tick object" name="detail[0][object_id]" data-size="3" data-style="btn-default" data-live-search="true">
												<option value="">(Pilih Salah Satu)</option>
											</select>
										</div>
									</td>
								</tr>
								<tr>
									<td style="border: 1px #ffffff;">Rencana</td>
									<td style="border: 1px #ffffff;">:</td>
									<td style="border: 1px #ffffff;" class="field">
										<input type="text" style="width: 25%" name="detail[0][rencana]" class="form-control rencana" placeholder="Rencana">
									</td>
								</tr>
								<tr>
									<td style="border: 1px #ffffff;">Keterangan</td>
									<td style="border: 1px #ffffff;">:</td>
									<td style="border: 1px #ffffff;" class="field">
										<div>
											<select data-width="25%" class="selectpicker show-tick" name="detail[0][keterangan]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
												<option value="0">Tidak Dipilih</option>
												<option value="1">Dipilih</option>
											</select>     
										</div>
									</td>
								</tr>
								<tr>
									<td style="border: 1px #ffffff;">Auditor Operasional</td>
									<td style="border: 1px #ffffff;">:</td>
									<td style="border: 1px #ffffff;" class="field">
										<div>
											<select data-width="100%" class="selectpicker form-control" name="detail[0][operasional][]" data-size="3" data-style="btn-default" data-live-search="true" title="(Pilih Auditor Operasional)" multiple>
												@foreach(App\Models\Auths\User::where('fungsi', 1)->get() as $user)
													<option value="{{ $user->id }}">{{ $user->name }}</option>
												@endforeach
											</select>
										</div>
									</td>
								</tr>
								<tr>
									<td style="border: 1px #ffffff;">Auditor Keuangan</td>
									<td style="border: 1px #ffffff;">:</td>
									<td style="border: 1px #ffffff;" class="field">
										<div>
											<select data-width="100%" class="selectpicker form-control" name="detail[0][keuangan][]" data-size="3" data-style="btn-default" data-live-search="true" title="(Pilih Auditor Keuangan)" multiple>
												@foreach(App\Models\Auths\User::where('fungsi', 2)->get() as $user)
													<option value="{{ $user->id }}">{{ $user->name }}</option>
												@endforeach
											</select>
										</div>
									</td>
								</tr>
								<tr>
									<td style="border: 1px #ffffff;">Auditor Sistem</td>
									<td style="border: 1px #ffffff;">:</td>
									<td style="border: 1px #ffffff;" class="field">
										<div>
											<select data-width="100%" class="selectpicker form-control" name="detail[0][sistem][]" data-size="3" data-style="btn-default" data-live-search="true" title="(Pilih Auditor Sistem)" multiple>
												@foreach(App\Models\Auths\User::where('fungsi', 3)->get() as $user)
													<option value="{{ $user->id }}">{{ $user->name }}</option>
												@endforeach
											</select>
										</div>
									</td>
								</tr>
								<tr class="detil-0" style="display: none;">
									<td style="border: 1px #ffffff;">No AB</td>
									<td style="border: 1px #ffffff;">:</td>
									<td style="border: 1px #ffffff;">
										<span class="label label-info project-noAb-0" style="font-size: 11px;"></span>
									</td>
								</tr>
								<tr class="detil-0" style="display: none;">
									<td style="border: 1px #ffffff;">Project ID</td>
									<td style="border: 1px #ffffff;">:</td>
									<td style="border: 1px #ffffff;">
										<span class="label label-info project-noId-0" style="font-size: 11px;"></span>
									</td>
								</tr>
								<tr class="detil-0" style="display: none;">
									<td style="border: 1px #ffffff;">Project</td>
									<td style="border: 1px #ffffff;">:</td>
									<td style="border: 1px #ffffff;">
										<span class="label label-info project-nama-0" style="font-size: 11px;"></span>
									</td>
								</tr>
								<tr class="detil-0" style="display: none;">
									<td style="border: 1px #ffffff;">Lokasi</td>
									<td style="border: 1px #ffffff;">:</td>
									<td style="border: 1px #ffffff;">
										<span class="label label-primary project-lokasi-0" style="font-size: 11px;"></span>
									</td>
								</tr>
								<tr class="detil-0" style="display: none;">
									<td style="border: 1px #ffffff;">Detil</td>
									<td style="border: 1px #ffffff;">:</td>
									<td style="border: 1px #ffffff;">
										<div class="form-row">
										    <div class="form-group col-md-4 field start" style="right: 10px;">
										      <label for="validationCustom01">Tgl Mulai</label>
										      <input type="text" name="detail[0][tgl_mulai]" class="form-control tgl_start" placeholder="Tgl Mulai">
										    </div>
										    <div class="form-group col-md-4 field end" style="left: 3px;">
										      <label for="validationCustom02">Tgl Selesai</label>
										      <input type="text" name="detail[0][tgl_selesai]" class="form-control tgl_end" placeholder="Tgl Selesai">
										    </div>
										    <div class="form-group col-md-4 field" style="left: 16px;">
											      <label for="validationCustom02">Jenis Kontrak</label>
													<select data-width="100%" data-id="0" class="selectpicker form-control show-tick jenis" name="detail[0][jenis]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
														<option value="0">Kontrak Baru</option>
														<option value="1">Sisa Nilai Kontrak</option>
														<option value="2">Project Selesai</option>
													</select>     
										    </div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-4 field" style="right: 10px;">
												<label>SNK {{ ($record->tahun-1) }}</label>
												<div class="input-group">
													<span class="input-group-addon" style="font-size: 10px;">Rp.</span>
													<input type="text" class="form-control inputmask" name="detail[0][snk]" placeholder="SNK {{ ($record->tahun-1) }}">
												</div>
											</div>
											<div class="form-group col-md-4 field" style="left: 3px;">
												<label>Progress s.d Des {{ ($record->tahun-1) }}</label>
												<div class="input-group">
													<input type="text" class="form-control inputmask" name="detail[0][progress]" placeholder="Progress s.d Des {{ ($record->tahun-1) }}">
													<span class="input-group-addon" style="font-size: 12px;">%</span>
												</div>
											</div>
											<div class="form-group col-md-4 field" style="left: 16px;">
												<label>NK Total</label>
												<div class="input-group">
													<span class="input-group-addon" style="font-size: 10px;">Rp.</span>
													<input type="text" class="form-control inputmask" name="detail[0][nilai_kontrak]" placeholder="NK Total">
												</div>
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-4 field" style="right: 10px;">
												<label for="inputEmail4">Progress (Ra)</label>
												<div class="input-group">
													<input type="text" class="form-control inputmask" name="detail[0][progress_ra]" placeholder="Progress (Ra)">
													<span class="input-group-addon" style="font-size: 12px;">%</span>
												</div>
											</div>
											<div class="form-group col-md-4 field" style="left: 3px;">
												<label for="inputEmail4">Progress (Ri)</label>
												<div class="input-group">
													<input type="text" class="form-control inputmask" name="detail[0][progress_ri]" placeholder="Progress (Ri)">
													<span class="input-group-addon" style="font-size: 12px;">%</span>
												</div>
											</div>
											<div class="form-group col-md-4 field" style="left: 16px;">
												<label for="inputEmail4">Deviasi Progress</label>
												<div class="input-group">
													<input type="text" class="form-control inputmask" name="detail[0][deviasi]" placeholder="Deviasi Progress">
													<span class="input-group-addon" style="font-size: 12px;">%</span>
												</div>
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-4 field" style="right: 10px;">
												<label for="inputEmail4">Progress Risk Impact</label>
												<select class="selectpicker form-control show-tick" name="detail[0][risk_impact]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
													<option value="0">Sangat Ringan</option>
													<option value="1">Ringan</option>
													<option value="2">Sedang</option>
													<option value="3">Berat</option>
													<option value="4">Sangat Berat</option>
												</select> 
											</div>
											<div class="form-group col-md-4 field" style="left: 3px;">
												<label for="inputEmail4">MAPP</label>
												<div class="input-group">
													<input type="text" class="form-control inputmask" name="detail[0][mapp]" placeholder="MAPP">
													<span class="input-group-addon" style="font-size: 12px;">%</span>
												</div>
											</div>
											<div class="form-group col-md-4 field" style="left: 16px;">
												<label for="inputEmail4">BK / PU</label>
												<div class="input-group">
													<input type="text" class="form-control inputmask" name="detail[0][bkpu]" placeholder="BK / PU">
													<span class="input-group-addon" style="font-size: 12px;">%</span>
												</div>
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-4 field" style="right: 10px;">
												<label for="inputEmail4">Deviasi BK / PU</label>
												<div class="input-group">
													<input type="text" class="form-control inputmask" name="detail[0][deviasi_bkpu]" placeholder="Deviasi BK / PU">
													<span class="input-group-addon" style="font-size: 12px;">%</span>
												</div>
											</div>
											<div class="form-group col-md-4 field" style="left: 3px;">
												<label for="inputEmail4">BK / PU Risk Impact</label>
												<select class="selectpicker form-control show-tick" name="detail[0][bkpu_ri]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
													<option value="0">Sangat Ringan</option>
													<option value="1">Ringan</option>
													<option value="2">Sedang</option>
													<option value="3">Berat</option>
													<option value="4">Sangat Berat</option>
												</select>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			@endif
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-right">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
						<button type="button" class="btn btn-primary save as drafting">Save As Draft</button>
						<button type="button" class="btn btn-simpan save as page">Submit</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
        .table-responsive{
            overflow-x: auto;
            overflow-y: visible !important;
        }

        .table-responsive .dropdown-menu {
	        position: static !important;
	    }

	    .datepicker-switch {
		  pointer-events: none;
		}

		
	    /*.datepicker-days > table > tbody > tr:hover {
		    background-color: #808080;
		}*/
    </style>
@endpush

@push('scripts')
    <script>
    	$('.hitung').each(function(key, value){
    		var id = $(this).data("id");
    		var val = $(this).data("value");
    		var tipe = $(this).data("tipe");
    		var tipes = $(this).data("tipes");
    		if(tipes == 0){
				$('.kepala-'+id).show();
				$('.konsultasi-'+id).hide();
				$('.lain-'+id).hide();
	    		if(tipe == 2){
					$('input[name="detail['+id+'][nama]"]').prop("disabled", false);
					var jenis = $('select[name="detail['+id+'][jenis]"]').val();
					if(jenis == 0){
						$('input[name="detail['+id+'][snk]"]').prop("disabled", true);
						$('input[name="detail['+id+'][progress]"]').prop("disabled", true);
					}else{
						$('input[name="detail['+id+'][snk]"]').prop("disabled", false);
						$('input[name="detail['+id+'][progress]"]').prop("disabled", false);
					}
					$('.detil-'+id).show();
				}else{
					$('input[name="detail['+id+'][nama]"]').prop("disabled", true);
					$('.detil-'+id).hide();
				}
	    		$.ajax({
					url: '{{ url('ajax/option/get-kategori') }}',
					type: 'POST',
					data: {
						_token: "{{ csrf_token() }}",
						kategori: $('select[name="detail['+id+'][tipe_object]"]').val()
					},
				}).done(function(response) {
					$('select[name="detail['+id+'][object_id]"]').html(response);
					$('select[name="detail['+id+'][object_id]"]').children('option[value="'+val+'"]').prop('selected',true);
					$('select[name="detail['+id+'][object_id]"]').selectpicker("refresh");
					if(tipe == 2){
						var nama = $('select[name="detail['+id+'][object_id]"] option:selected').html();
						$('.project-nama-'+id).html(nama);
						$.ajax({
							url: '{{ url('ajax/option/get-lokasi') }}',
							type: 'POST',
							data: {
								_token: "{{ csrf_token() }}",
								lokasi: val
							},
						})
						.done(function(response) {
							$('.project-lokasi-'+id).html(response);
						})
						.fail(function() {
							console.log("error");
						});

						$.ajax({
							url: '{{ url('ajax/option/get-noAb') }}',
							type: 'POST',
							data: {
								_token: "{{ csrf_token() }}",
								lokasi: val
							},
						})
						.done(function(response) {
							$('.project-noAb-'+id).html(response);
						})
						.fail(function() {
							console.log("error");
						});

						$.ajax({
							url: '{{ url('ajax/option/get-noId') }}',
							type: 'POST',
							data: {
								_token: "{{ csrf_token() }}",
								lokasi: val
							},
						})
						.done(function(response) {
							$('.project-noId-'+id).html(response);
						})
						.fail(function() {
							console.log("error");
						});
					}
				}).fail(function() {
					console.log("error");
				});
			}else if(tipes == 1){
				$('.kepala-'+id).hide();
				$('.konsultasi-'+id).show();
				$('.lain-'+id).hide();
				$('.detil-'+id).hide();
			}else{
				$('.kepala-'+id).hide();
				$('.konsultasi-'+id).hide();
				$('.lain-'+id).show();
				$('.detil-'+id).hide();
			}

    	});

    	$('.object').on('change', function(){
			var id = $(this).data("id");
			var nama = $('select[name="detail['+id+'][object_id]"] option:selected').html();
			$('.project-nama-'+id).html(nama);
			var cek = $(this).val();
			$.ajax({
				url: '{{ url('ajax/option/get-lokasi') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					lokasi: cek
				},
			})
			.done(function(response) {
				$('.project-lokasi-'+id).html(response);
			})
			.fail(function() {
				console.log("error");
			});

			$.ajax({
				url: '{{ url('ajax/option/get-noAb') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					lokasi: cek
				},
			})
			.done(function(response) {
				$('.project-noAb-'+id).html(response);
			})
			.fail(function() {
				console.log("error");
			});

			$.ajax({
				url: '{{ url('ajax/option/get-noId') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					lokasi: cek
				},
			})
			.done(function(response) {
				$('.project-noId-'+id).html(response);
			})
			.fail(function() {
				console.log("error");
			});
		});

		$('.rencana-kerja').on('change', function(){
			var id = $(this).data("id");
			var val = $(this).val();
			if(val == 0){
				$('.kepala-'+id).show();
				$('.konsultasi-'+id).hide();
				$('.lain-'+id).hide();
				$('select[name="detail['+id+'][tipe_object]"]').val('default').selectpicker("refresh");
			}else if(val == 1){
				$('.kepala-'+id).hide();
				$('.konsultasi-'+id).show();
				$('.lain-'+id).hide();
				$('.detil-'+id).hide();
			}else{
				$('.kepala-'+id).hide();
				$('.konsultasi-'+id).hide();
				$('.lain-'+id).show();
				$('.detil-'+id).hide();
			}
		});
 
		$('.kategori').on('change', function(){
			var id = $(this).data("id");
			$('.project-nama-'+id).html('-');
			if(this.value == 2){
				$('input[name="detail['+id+'][nama]"]').prop("disabled", false);
				$('.detil-'+id).show();
				$('.object').on('change', function(){
					var id = $(this).data("id");
					var nama = $('select[name="detail['+id+'][object_id]"] option:selected').html();
					$('.project-nama-'+id).html(nama);
					var cek = $(this).val();
					$.ajax({
						url: '{{ url('ajax/option/get-lokasi') }}',
						type: 'POST',
						data: {
							_token: "{{ csrf_token() }}",
							lokasi: cek
						},
					})
					.done(function(response) {
						$('.lokasi-nama-'+id).html(response);
					})
					.fail(function() {
						console.log("error");
					});
				});
			}else{
				$('input[name="detail['+id+'][nama]"]').prop("disabled", true);
				$('.detil-'+id).hide();
			}
			$.ajax({
				url: '{{ url('ajax/option/get-kategori') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					kategori: this.value
				},
			})
			.done(function(response) {
				$('select[name="detail['+id+'][object_id]"]').html(response);
				$('select[name="detail['+id+'][object_id]"]').selectpicker("refresh");
			})
			.fail(function() {
				console.log("error");
			});
		});
		$('.jenis').on('change', function(){
			var id = $(this).data("id");
			if(this.value == 0){
				$('input[name="detail['+id+'][snk]"]').prop("disabled", true);
				$('input[name="detail['+id+'][progress]"]').prop("disabled", true);
			}else{
				$('input[name="detail['+id+'][snk]"]').prop("disabled", false);
				$('input[name="detail['+id+'][progress]"]').prop("disabled", false);
			}
		});
    	$(document).ready(function(){
    		$(".inputmask").inputmask({
	            radixPoint: ',',
	            groupSeparator: ".",
	            alias: "numeric",
	            autoGroup: true,
	            digits: 2
	        });
    		moment.locale('id');
    	});

    	var d = new Date();
    	var full = d.getFullYear();
    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
    	var last = new Date(lastDate);
    	if($('input[name=tahun]').val() > full){
    		var min = minDates;
    	}else{
    		var min = d;
    	}

    	$('.tgl_start').datepicker({
    		orientation: "top right",
			todayHighlight: true,
			autoclose:true,
    		format: 'dd-M-yy',
    		yearRange: '-0:+1',
    		startDate: minDates,
    		// endDate: last,
			hideIfNoPrevNext: true,
    		defaultViewDate: {
            	year: $('input[name=tahun]').val(),
            }
    	}).on('changeDate', function(date, format, language) {
    			var start = new Date(date.date);
    			console.log(start)
    			var element = $(this).parents('tr').find('div.end').find('input');
    			element.datepicker("destroy");
    			element.datepicker({
    				orientation: "bottom",
		            orientation: "auto",
		            autoclose:true,
		            maxViewMode: 0,
    				startDate: start,
    				// endDate: last,
    				format: 'dd-M-yy',
    				defaultViewDate: {
		            	year: $('input[name=tahun]').val(),
		            }
    			})
    			$(this).parents('tr').find('div.end').find('input').focus();
        });
    	
    	$('.rencana').datepicker({
            orientation: "bottom",
            orientation: "auto",
            autoclose:true,
            maxViewMode: 0,
            startDate: minDates,
            endDate: last,
            format: {
		        toValue: function (date, format, language) {
		        	var d = new Date(date);
			        d.setDate(d.getDate() + 7);
			        return new Date(d);
		        },
		        toDisplay: function (date, format, language) {
		        	var first = moment(date, "MM-DD-YYYY").locale("id");
		        	var weeks = weekcount(first);
		        	return weeks;
		        }
		    },
            todayHighlight: true,
            defaultViewDate: {
            	year: $('input[name=tahun]').val(),
            }
        });

        function weekcount(mJsDate){
        	var str = `Minggu Ke ` + convertToRoman(Math.ceil(mJsDate.date() / 7)) + 
        			  ` `+ mJsDate.format('MMMM') +` `+ mJsDate.format('YYYY');
        	return str;
        }

        function convertToRoman(num) {
		  var roman = {
		    M: 1000,CM: 900,D: 500,CD: 400,C: 100,XC: 90,L: 50,XL: 40,X: 10,IX: 9,V: 5,IV: 4,I: 1
		  };
		  var str = '';
		  for (var i of Object.keys(roman)) {
		    var q = Math.floor(num / roman[i]);
		    num -= q * roman[i];
		    str += i.repeat(q);
		  }
		  return str;
		}

    	$(document).on('click', '.tambah_object', function(e){
    		var last = parseInt($('input[name=last]').val()) + 1;
    		var cek = $('.panel-heading').length;
			var html = `
						<div class="panel-heading data-detil-`+last+`" data-id="`+last+`" style="font-weight: bold;font-size: 13px;text-align: right;">
						<div style="width: 50%;text-align: left;">
							<label style="margin-top: 7px;" class="head-numboor-`+last+`">`+cek+`.</label>
						</div>
						<button class="btn btn-sm btn-danger hapus_object" type="button" data-id="`+last+`" style="margin-top: -19px;"><i class="fa fa-close"></i></button>
						</div>
				       <div class="panel-body data-detil-`+last+`" style="padding-bottom: 0px;">
					<table class="table" style="font-size: 12px;">
						<tbody>
							<tr>
								<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">Rencana Kerja</td>
								<td style="width:2px;border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;" class="field">
									<div>
										<select data-width="25%" data-id="`+last+`" class="pilihan selectpicker col-sm-12 show-tick rencana-kerja" data-size="3" name="detail[`+last+`][tipe]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
											<option value="0">Kegiatan Audit</option>
											<option value="1">Kegiatan Konsultasi</option>
											<option value="2">Kegiatan Lainnya</option>
										</select> 
									</div>
								</td>
							</tr>
							<tr style="display: none;" class="konsultasi-`+last+`">
								<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">Kategori</td>
								<td style="width:2px;border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;" class="field">
									<div>
										<select data-width="50%" data-id="`+last+`" class="selectpicker form-control show-tick" data-size="3" name="detail[`+last+`][konsultasi_id]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
							                @foreach(App\Models\Master\Konsultasi::get() as $konsultasi)
							                    <option value="{{ $konsultasi->id }}">{{ $konsultasi->nama }}</option>
							                @endforeach
							            </select>    
									</div>
								</td>
							</tr>
							<tr style="display: none;" class="lain-`+last+`">
								<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">Kategori</td>
								<td style="width:2px;border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;" class="field">
									<div>
										<select data-width="50%" data-id="`+last+`" class="selectpicker form-control show-tick" data-size="3" name="detail[`+last+`][lain]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
							                 @foreach(App\Models\Master\Lain::get() as $lain)
							                    <option value="{{ $lain->id }}">Counterpart Auditor Eksternal - {{ $lain->nama }}</option>
							                 @endforeach
								                <option value="1000">SPIN</option>
								                <option value="1001">IACM</option>
							            </select>    
									</div>
								</td>
							</tr>
							<tr style="display: none;" class="kepala-`+last+`">
								<td style="width:180px;border: 1px #ffffff;border: 1px #ffffff;">Kategori</td>
								<td style="width:2px;border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;" class="field">
									<div>
										<select data-width="25%" data-id="`+last+`" class="pilihan selectpicker col-sm-12 show-tick kategori" data-size="3" name="detail[`+last+`][tipe_object]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
											<option value="0">Business Unit (BU)</option>
											<option value="1">Corporate Office (CO)</option>
											<option value="2">Project</option>
											<option value="3">Anak Perusahaan</option>
										</select> 
									</div>
								</td>
							</tr>
							<tr style="display: none;" class="kepala-`+last+`">
								<td style="border: 1px #ffffff;">Objek Audit</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;" class="field">
									<div>
										<select data-id="`+last+`" data-width="100%" class="selectpicker show-tick object" name="detail[`+last+`][object_id]" data-size="3" data-style="btn-default" data-live-search="true">
											<option value="">(Pilih Salah Satu)</option>
										</select>
									</div>
								</td>
							</tr>
							<tr>
								<td style="border: 1px #ffffff;">Rencana</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;" class="field">
									<input type="text" style="width: 25%" name="detail[`+last+`][rencana]" class="form-control rencana" placeholder="Rencana">
								</td>
							</tr>
							<tr>
								<td style="border: 1px #ffffff;">Keterangan</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;" class="field">
									<div>
										<select data-width="25%" class="selectpicker show-tick" name="detail[`+last+`][keterangan]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
											<option value="0">Tidak Dipilih</option>
											<option value="1">Dipilih</option>
										</select>     
									</div>
								</td>
							</tr>
							<tr>
								<td style="border: 1px #ffffff;">Auditor Operasional</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;" class="field">
									<div>
										<select data-width="100%" class="selectpicker form-control show-tick" name="detail[`+last+`][operasional][]" data-size="3" data-style="btn-default" data-live-search="true" title="(Pilih Auditor Operasional)" multiple>
											@foreach(App\Models\Auths\User::where('fungsi', 1)->get() as $user)
												<option value="{{ $user->id }}">{{ $user->name }}</option>
											@endforeach
										</select>
									</div>
								</td>
							</tr>
							<tr>
								<td style="border: 1px #ffffff;">Auditor Keuangan</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;" class="field">
									<div>
										<select data-width="100%" class="selectpicker form-control show-tick" name="detail[`+last+`][keuangan][]" data-size="3" data-style="btn-default" data-live-search="true" title="(Pilih Auditor Keuangan)" multiple>
											@foreach(App\Models\Auths\User::where('fungsi', 2)->get() as $user)
												<option value="{{ $user->id }}">{{ $user->name }}</option>
											@endforeach
										</select>
									</div>
								</td>
							</tr>
							<tr>
								<td style="border: 1px #ffffff;">Auditor Sistem</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;" class="field">
									<div>
										<select data-width="100%" class="selectpicker form-control show-tick" name="detail[`+last+`][sistem][]" data-size="3" data-style="btn-default" data-live-search="true" title="(Pilih Auditor Sistem)" multiple>
											@foreach(App\Models\Auths\User::where('fungsi', 3)->get() as $user)
												<option value="{{ $user->id }}">{{ $user->name }}</option>
											@endforeach
										</select>
									</div>
								</td>
							</tr>
							<tr class="detil-`+last+`" style="display: none;">
								<td style="border: 1px #ffffff;">No AB</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									<span class="label label-info project-noAb-`+last+`" style="font-size: 11px;"></span>
								</td>
							</tr>
							<tr class="detil-`+last+`" style="display: none;">
								<td style="border: 1px #ffffff;">Project ID</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									<span class="label label-info project-noId-`+last+`" style="font-size: 11px;"></span>
								</td>
							</tr>
							<tr class="detil-`+last+`" style="display: none;">
								<td style="border: 1px #ffffff;">Project</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									<span class="label label-info project-nama-`+last+`" style="font-size: 11px;"></span>
								</td>
							</tr>
							<tr class="detil-`+last+`" style="display: none;">
								<td style="border: 1px #ffffff;">Lokasi</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									<span class="label label-primary lokasi-nama-`+last+`" style="font-size: 11px;"></span>
								</td>
							</tr>
							<tr class="detil-`+last+`" style="display: none;">
								<td style="border: 1px #ffffff;">Detil</td>
								<td style="border: 1px #ffffff;">:</td>
								<td style="border: 1px #ffffff;">
									<div class="form-row">
									    <div class="form-group col-md-4 field start" style="right: 10px;">
									      <label for="validationCustom01">Tgl Mulai</label>
									      <input type="text" name="detail[`+last+`][tgl_mulai]" class="form-control tgl_start" placeholder="Tgl Mulai">
									    </div>
									    <div class="form-group col-md-4 field end" style="left: 3px;">
									      <label for="validationCustom02">Tgl Selesai</label>
									      <input type="text" name="detail[`+last+`][tgl_selesai]" class="form-control tgl_end" placeholder="Tgl Selesai">
									    </div>
									    <div class="form-group col-md-4 field" style="left: 16px;">
										      <label for="validationCustom02">Jenis Kontrak</label>
												<select data-width="100%" data-id="`+last+`" class="selectpicker form-control show-tick jenis" name="detail[`+last+`][jenis]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
													<option value="0">Kontrak Baru</option>
													<option value="1">Sisa Nilai Kontrak</option>
													<option value="2">Project Selesai</option>
												</select>     
									    </div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-4 field" style="right: 10px;">
											<label>SNK {{ ($record->tahun-1) }}</label>
											<div class="input-group">
												<span class="input-group-addon" style="font-size: 10px;">Rp.</span>
												<input type="text" class="form-control inputmask" name="detail[`+last+`][snk]" placeholder="SNK {{ ($record->tahun-1) }}">
											</div>
										</div>
										<div class="form-group col-md-4 field" style="left: 3px;">
											<label>Progress s.d Des {{ ($record->tahun-1) }}</label>
											<div class="input-group">
												<input type="text" class="form-control inputmask" name="detail[`+last+`][progress]" placeholder="Progress s.d Des {{ ($record->tahun-1) }}">
												<span class="input-group-addon" style="font-size: 12px;">%</span>
											</div>
										</div>
										<div class="form-group col-md-4 field" style="left: 16px;">
											<label>NK Total</label>
											<div class="input-group">
												<span class="input-group-addon" style="font-size: 10px;">Rp.</span>
												<input type="text" class="form-control inputmask" name="detail[`+last+`][nilai_kontrak]" placeholder="NK Total">
											</div>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-4 field" style="right: 10px;">
											<label for="inputEmail4">Progress (Ra)</label>
											<div class="input-group">
												<input type="text" class="form-control inputmask" name="detail[`+last+`][progress_ra]" placeholder="Progress (Ra)">
												<span class="input-group-addon" style="font-size: 12px;">%</span>
											</div>
										</div>
										<div class="form-group col-md-4 field" style="left: 3px;">
											<label for="inputEmail4">Progress (Ri)</label>
											<div class="input-group">
												<input type="text" class="form-control inputmask" name="detail[`+last+`][progress_ri]" placeholder="Progress (Ri)">
												<span class="input-group-addon" style="font-size: 12px;">%</span>
											</div>
										</div>
										<div class="form-group col-md-4 field" style="left: 16px;">
											<label for="inputEmail4">Deviasi Progress</label>
											<div class="input-group">
												<input type="text" class="form-control inputmask" name="detail[`+last+`][deviasi]" placeholder="Deviasi Progress">
												<span class="input-group-addon" style="font-size: 12px;">%</span>
											</div>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-4 field" style="right: 10px;">
											<label for="inputEmail4">Progress Risk Impact</label>
											<select class="selectpicker form-control show-tick" name="detail[`+last+`][risk_impact]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
												<option value="0">Sangat Ringan</option>
												<option value="1">Ringan</option>
												<option value="2">Sedang</option>
												<option value="3">Berat</option>
												<option value="4">Sangat Berat</option>
											</select> 
										</div>
										<div class="form-group col-md-4 field" style="left: 3px;">
											<label for="inputEmail4">MAPP</label>
											<div class="input-group">
												<input type="text" class="form-control inputmask" name="detail[`+last+`][mapp]" placeholder="MAPP">
												<span class="input-group-addon" style="font-size: 12px;">%</span>
											</div>
										</div>
										<div class="form-group col-md-4 field" style="left: 16px;">
											<label for="inputEmail4">BK / PU</label>
											<div class="input-group">
												<input type="text" class="form-control inputmask" name="detail[`+last+`][bkpu]" placeholder="BK / PU">
												<span class="input-group-addon" style="font-size: 12px;">%</span>
											</div>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-4 field" style="right: 10px;">
											<label for="inputEmail4">Deviasi BK / PU</label>
											<div class="input-group">
												<input type="text" class="form-control inputmask" name="detail[`+last+`][deviasi_bkpu]" placeholder="Deviasi BK / PU">
												<span class="input-group-addon" style="font-size: 12px;">%</span>
											</div>
										</div>
										<div class="form-group col-md-4 field" style="left: 3px;">
											<label for="inputEmail4">BK / PU Risk Impact</label>
											<select class="selectpicker form-control show-tick" name="detail[`+last+`][bkpu_ri]" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
												<option value="0">Sangat Ringan</option>
												<option value="1">Ringan</option>
												<option value="2">Sedang</option>
												<option value="3">Berat</option>
												<option value="4">Sangat Berat</option>
											</select>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				`;

				$('.data-form').append(html);
				$('.selectpicker').selectpicker();
				$('.kategori').on('change', function(){
					var id = $(this).data("id");
					$('.project-nama-'+id).html('-');
					if(this.value == 2){
						$('input[name="detail['+id+'][nama]"]').prop("disabled", false);
						$('.detil-'+id).show();
						$('.object').on('change', function(){
							var id = $(this).data("id");
							var nama = $('select[name="detail['+id+'][object_id]"] option:selected').html();
							$('.project-nama-'+id).html(nama);
							var cek = $(this).val();
							$.ajax({
								url: '{{ url('ajax/option/get-lokasi') }}',
								type: 'POST',
								data: {
									_token: "{{ csrf_token() }}",
									lokasi: cek
								},
							})
							.done(function(response) {
								$('.lokasi-nama-'+id).html(response);
							})
							.fail(function() {
								console.log("error");
							});

							$.ajax({
								url: '{{ url('ajax/option/get-noAb') }}',
								type: 'POST',
								data: {
									_token: "{{ csrf_token() }}",
									lokasi: cek
								},
							})
							.done(function(response) {
								$('.project-noAb-'+id).html(response);
							})
							.fail(function() {
								console.log("error");
							});

							$.ajax({
								url: '{{ url('ajax/option/get-noId') }}',
								type: 'POST',
								data: {
									_token: "{{ csrf_token() }}",
									lokasi: cek
								},
							})
							.done(function(response) {
								$('.project-noId-'+id).html(response);
							})
							.fail(function() {
								console.log("error");
							});
						});
					}else{
						$('input[name="detail['+id+'][nama]"]').prop("disabled", true);
						$('.detil-'+id).hide();
					}
					$.ajax({
						url: '{{ url('ajax/option/get-kategori') }}',
						type: 'POST',
						data: {
							_token: "{{ csrf_token() }}",
							kategori: this.value
						},
					})
					.done(function(response) {
						$('select[name="detail['+id+'][object_id]"]').html(response);
						$('select[name="detail['+id+'][object_id]"]').selectpicker("refresh");
					})
					.fail(function() {
						console.log("error");
					});
				});
				$('.jenis').on('change', function(){
					var id = $(this).data("id");
					if(this.value == 0){
						$('input[name="detail['+id+'][snk]"]').prop("disabled", true);
						$('input[name="detail['+id+'][progress]"]').prop("disabled", true);
					}else{
						$('input[name="detail['+id+'][snk]"]').prop("disabled", false);
						$('input[name="detail['+id+'][progress]"]').prop("disabled", false);
					}
				});
		    	$(document).ready(function(){
		    		reloadMask();
		    		moment.locale('id')
		    	});

		  		var d = new Date();
		    	var full = d.getFullYear();
		    	var lastDate = new Date($('input[name=tahun]').val() +'', 11, 31);
		    	var minDates = new Date($('input[name=tahun]').val() +'', 0, 1);
		    	var lastDates = new Date(lastDate);
		    	if($('input[name=tahun]').val() > full){
		    		var min = minDates;
		    	}else{
		    		var min = d;
		    	}

		    	$('.rencana-kerja').on('change', function(){
					var id = $(this).data("id");
					var val = $(this).val();
					if(val == 0){
						$('.kepala-'+id).show();
						$('.konsultasi-'+id).hide();
						$('.lain-'+id).hide();
						console.log(id)
						$('select[name="detail['+id+'][tipe_object]"]').val('default').selectpicker("refresh");
					}else if(val == 1){
						$('.kepala-'+id).hide();
						$('.konsultasi-'+id).show();
						$('.lain-'+id).hide();
						$('.detil-'+id).hide();
					}else{
						$('.kepala-'+id).hide();
						$('.konsultasi-'+id).hide();
						$('.lain-'+id).show();
						$('.detil-'+id).hide();
					}
				});

		    	$('.tgl_start').datepicker({
		    		orientation: "top right",
					todayHighlight: true,
					autoclose:true,
		    		format: 'dd-M-yy',
		    		yearRange: '-0:+1',
		    		startDate: minDates,
		    		// endDate: lastDates,
					hideIfNoPrevNext: true,
		    		defaultViewDate: {
		            	year: $('input[name=tahun]').val(),
		            }
		    	}).on('changeDate', function(date, format, language) {
		    			var start = new Date(date.date);
		    			console.log(start)
		    			var element = $(this).parents('tr').find('div.end').find('input');
		    			element.datepicker("destroy");
		    			element.datepicker({
		    				orientation: "bottom",
				            orientation: "auto",
				            autoclose:true,
				            maxViewMode: 0,
		    				startDate: start,
		    				// endDate: lastDates,
		    				format: 'dd-M-yy',
		    				defaultViewDate: {
				            	year: $('input[name=tahun]').val(),
				            }
		    			})
		    			$(this).parents('tr').find('div.end').find('input').focus();
		        });

		    	$('.rencana').datepicker({
		            orientation: "bottom",
		            autoclose:true,
		            maxViewMode: 0,
		            startDate: minDates,
		            endDate: lastDates,
		            defaultViewDate: {
		            	year: $('input[name=tahun]').val(),
		            },
		            format: {
				        toDisplay: function (date, format, language) {
				        	var first = moment(date, "MM-DD-YYYY").locale("id");
				        	var weeks = weekcount(first);
				        	return weeks;
				        },
				        toValue: function (date, format, language) {
				        	var first = moment(date, "MM-DD-YYYY").locale("id");
				        	var weeks = weekcount(first);
				        	return first;
				        }
				    }
		        });

		        function weekcount(mJsDate){
		        	var str = `Minggu Ke ` + convertToRoman(Math.ceil(mJsDate.date() / 7)) + 
		        			  ` `+ mJsDate.format('MMMM') +` `+ mJsDate.format('YYYY');
		        	return str;
		        }

		        function convertToRoman(num) {
				  var roman = {
				    M: 1000,CM: 900,D: 500,CD: 400,C: 100,XC: 90,L: 50,XL: 40,X: 10,IX: 9,V: 5,IV: 4,I: 1
				  };
				  var str = '';
				  for (var i of Object.keys(roman)) {
				    var q = Math.floor(num / roman[i]);
				    num -= q * roman[i];
				    str += i.repeat(q);
				  }
				  return str;
				}

		        $('input[name=last]').val(last);
		        $(".inputmask").inputmask({
		            radixPoint: ',',
		            groupSeparator: ".",
		            alias: "numeric",
		            autoGroup: true,
		            digits: 2
		        });
				reloadMask();
		});

		$(document).on('click', '.hapus_object', function (e){
			var id = $(this).data("id");
			console.log(id)
			var row = $('.data-detil-'+id).remove();
			$(".inputmask").inputmask({
	            radixPoint: ',',
	            groupSeparator: ".",
	            alias: "numeric",
	            autoGroup: true,
	            digits: 2
	        });
			reloadMask();

			var rows = $('.panel-heading');
			var rowz = $('.panel-heading');
			$.each(rows, function(key, value){
				rowz.find('.head-numboor-'+$(this).data("id")).html(key);
			});
		});
    </script>
    @yield('js-extra')
@endpush

