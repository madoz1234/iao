@php
	$id=1;
@endphp
<form action="{{ route($routes.'.saveUpload', $record->id) }}" method="POST" id="formData">
	@method('PATCH')
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Upload Berkas Final RKIA {{ $record->tahun }}</h5>
    </div>
    <div class="modal-body">
        <div class="form-group">
            <div class="form-group field">
            	<label class="control-label">Berkas Final</label>
            	<div class="file-loading">
            		<input id="pdf" name="final" type="file" class="file form-control"
            		data-show-upload="false" data-show-caption="true" data-msg-placeholder="Pilih {files} yang akan diupload..." accept="application/pdf">
            	</div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script>

    </script>
    @yield('js-extra')
@endpush
