<html>
<head>
<style>
        @page {
            margin: 1cm;
            counter-reset: page;
            font-family: Tahoma, sans-serif;
        }
        *{
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            font-family : Tahoma, sans-serif;
        }

       /* Define now the real margins of every page in the PDF */
       body {
           margin-top: 2.5cm;
           font-style: normal;
           font-family : Tahoma, sans-serif!important;
       }

       .ui.table.bordered {
         border: solid black 1px;
         border-collapse: collapse;
         width: 100%;
         font-family : Tahoma, sans-serif;
       }

       .ui.table.bordered td {
         border: solid black .5px;
         border-collapse: collapse;
         /*padding:10px;*/
         padding:5px;
       }

       .ui.table.bordered td.center.aligned {
         text-align : center;
       }

       .page-break {
            page-break-after: always;
       }

       #watermark {
            position: fixed;
            /** 
                Set a position in the page for your image
                This should center it vertically
            **/
            top: 2.5cm;
            bottom:   10cm;
            left:     10.5cm;

            /** Change image dimensions**/
            width:    8cm;
            height:   8cm;

            /** Your watermark should be behind every content**/
            z-index:  101;
        }

       .pad{
        padding-top: 5px;
        padding-bottom: 5px;
       }

       /* Define the header rules */
        header {
            position: fixed;
            top: 0px;
            left: 0px;
            right: 0px;

            /* Extra personal styles */
            text-align: center;
            /*line-height: 35px;*/

            font-size: 12px;
        }
        
        main {
            position: sticky;
            font-size: 12px;
            border : none;
            width: 100%;
            margin-top: -5px;
        }

        main p {
          margin-top: 5px;
          margin-bottom: 5px;
        }

       /* Define the footer rules */
/*       .page-num:after { 
        counter-increment: pages; 
        content: counter(page) " of " counter(pages); 
       }*/
       table.page_content {width: 100%; border: none; padding: 1mm; font-size: 12px;font-family : Tahoma, sans-serif;}
       table.page_header {width: 100%; border: none; padding: 2mm; font-size: 12px;font-family : Tahoma, sans-serif; text-align: left; margin-left: 50px;margin-top: 50px;}
       table.page_content_header {width: 100%; font-size: 12px;border-collapse: collapse;font-family : Tahoma, sans-serif;text-align: center; padding-bottom: 1px;
        padding-top: 1px;}

        table { page-break-inside: auto }
        tr { page-break-inside: avoid; page-break-after: auto }
        thead { display: table-header-group }
        tfoot { display: table-footer-group }

       /*.footer .page-number:after {
         content: counter(page);
       }*/

       .text-center{
            text-align: center;
        }
        .text-left{
            text-align: left;
        }
        .text-right{
         text-align: right;
        }
        .v-middle{
            vertical-align: middle!important;
        }
        .ui.table.bordered th,
        .ui.table.bordered td {
            vertical-align: top;
        }
</style>
</head>
<body style="font-family: Tahoma, sans-serif;">
    <header>
        <table border="1" class="page_content ui table" style="page-break-inside: auto; padding:5px;">
            <tbody>
                <tr>
                    <td rowspan="2" style="width: 85px;padding-left: -10px; border: none;"><img src="{{ asset('src/img/logo.png') }}" width="85px" height="70px"></td>
                    <td rowspan="2" style="border: none; text-align: left;"><h2>PT. WASKITA KARYA <small>(Persero)</small> Tbk</h2></td>
                    <td style="padding-right: -10px; border: none; width: 140px; vertical-align: bottom;">
                        Form IA 08
                    </td>
                </tr>
                <tr>
                    <td style="padding-right: -20px; border: none; width: 160px; vertical-align: top; text-align: justify;">
                        @if($form_edisi = $record->edisi)
                        {{ 'Edisi: '.$form_edisi->edisi.' Revisi: '.$form_edisi->revisi }}
                        @else
                        Edisi: - Revisi: -
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
    </header>
    <main>
        <table  border="1" class="page_content ui table bordered" style="page-break-inside: auto;">
            <thead>
                <tr>
                    <th colspan="7">
                        <h3 style="margin-top: 5px;">Survey Kepuasan Auditee</h3>
                        <table style="width: 100%;">
                            <tbody>
                                <tr>
                                    <td style="font-weight: normal; padding-top: 0px; padding-bottom: 0px; width: 140px; border: none;">Nama</td>
                                    <td style="font-weight: normal; padding-top: 0px; padding-bottom: 0px; padding-left: 0; padding-right: 0; width: 5px; border: none;">:</td>
                                    <td style="font-weight: normal; padding-top: 0px; padding-bottom: 0px; border: none;">
                                        {{ $record->user->name }}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: normal; padding-top: 0px; padding-bottom: 0px; width: 140px; border: none;">Posisi</td>
                                    <td style="font-weight: normal; padding-top: 0px; padding-bottom: 0px; padding-left: 0; padding-right: 0; width: 5px; border: none;">:</td>
                                    <td style="font-weight: normal; padding-top: 0px; padding-bottom: 0px; border: none;">
                                        {{ $record->user->posisi }}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: normal; padding-top: 0px; padding-bottom: 0px; width: 140px; border: none;">Kategori</td>
                                    <td style="font-weight: normal; padding-top: 0px; padding-bottom: 0px; padding-left: 0; padding-right: 0; width: 5px; border: none;">:</td>
                                    <td style="font-weight: normal; padding-top: 0px; padding-bottom: 0px; border: none;">
                                        @if($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 0)
                                            <span class="label label-success">Business Unit (BU)</span>
                                        @elseif($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 1)
                                            <span class="label label-info">Corporate Office (CO)</span>
                                        @elseif($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 2)
                                            <span class="label label-default">Project</span>
                                        @else 
                                            <span class="label label-primary">Anak Perusahaan</span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: normal; padding-top: 0px; padding-bottom: 0px; width: 140px; border: none;">Objek Audit</td>
                                    <td style="font-weight: normal; padding-top: 0px; padding-bottom: 0px; padding-left: 0; padding-right: 0; width: 5px; border: none;">:</td>
                                    <td style="font-weight: normal; padding-top: 0px; padding-bottom: 0px; border: none;">
                    
                                        {!! object_audit($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->lha->draftkka->program->penugasanaudit->rencanadetail->object_id)!!}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: normal; padding-top: 0px; padding-bottom: 0px; width: 140px; border: none;">Tanggal Pelaksanaan</td>
                                    <td style="font-weight: normal; padding-top: 0px; padding-bottom: 0px; padding-left: 0; padding-right: 0; width: 5px; border: none;">:</td>
                                    <td style="font-weight: normal; padding-top: 0px; padding-bottom: 0px; border: none;">
                                         {{ DateToStringWday($record->lha->draftkka->program->detailpelaksanaan->min('tgl')) }} -  {{DateToStringWday($record->lha->draftkka->program->detailpelaksanaan->max('tgl'))}}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: normal; padding-top: 0px; padding-bottom: 0px; width: 140px; border: none;">Tanggal Aktif</td>
                                    <td style="font-weight: normal; padding-top: 0px; padding-bottom: 0px; padding-left: 0; padding-right: 0; width: 5px; border: none;">:</td>
                                    <td style="font-weight: normal; padding-top: 0px; padding-bottom: 0px; border: none;">
                                         {{ DateToString($record->created_at) }}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: normal; padding-top: 0px; padding-bottom: 0px; width: 140px; border: none;">Tanggal Submit</td>
                                    <td style="font-weight: normal; padding-top: 0px; padding-bottom: 0px; padding-left: 0; padding-right: 0; width: 5px; border: none;">:</td>
                                    <td style="font-weight: normal; padding-top: 0px; padding-bottom: 0px; border: none;">
                                         {{ DateToString($record->updated_at) }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p style="text-align: left; padding-left: 7px;">Isilah pernyataan-pernyataan di bawah ini dengan tanda silang (X)</p>
                    </th>
                </tr>
                <tr>
                    <th class="v-middle text-center" style="vertical-align: middle; width: 30px">No</th>
                    <th class="v-middle text-center" style="vertical-align: middle;">Uraian</th>
                    <th class="v-middle text-center" style="vertical-align: middle; width: 50px">Sangat Tidak Setuju</th>
                    <th class="v-middle text-center" style="vertical-align: middle; width: 50px">Tidak Setuju</th>
                    <th class="v-middle text-center" style="vertical-align: middle; width: 50px">Kurang Setuju</th>
                    <th class="v-middle text-center" style="vertical-align: middle; width: 50px">Setuju</th>
                    <th class="v-middle text-center" style="vertical-align: middle; width: 50px">Sangat Setuju</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($survey->pertanyaan as $key => $val)
                    @php
                        $cek_jawaban = $survey_jawab_detail->where('tanya_id', $val->id)->first();
                        $jawab_detail = ['tanya_id' => '', 'jawaban'=>''];
                        if ($cek_jawaban) {
                            $jawab_detail = [
                                'tanya_id' => $cek_jawaban->tanya_id, 
                                'jawaban'=>$cek_jawaban->jawaban
                            ];
                        }else{
                            continue;
                        }
                    @endphp
                    
                    <tr>
                        <td class="text-center">{{ $loop->iteration.'.' }}</td>
                        <td class="text-left">
                            {{ $val->pertanyaan }}
                        </td>
                        <td class="text-center" style="vertical-align: middle;">
                            @if($jawab_detail['tanya_id']==$val->id && $jawab_detail['jawaban']==1) 
                                <strong>X</strong>
                            @endif
                        </td>
                        <td class="text-center" style="vertical-align: middle;">
                            @if($jawab_detail['tanya_id']==$val->id && $jawab_detail['jawaban']==2) 
                                <strong>X</strong>
                            @endif
                        </td>
                        <td class="text-center" style="vertical-align: middle;">
                            @if($jawab_detail['tanya_id']==$val->id && $jawab_detail['jawaban']==3) 
                                <strong>X</strong>
                            @endif
                        </td>
                        <td class="text-center" style="vertical-align: middle;">
                            @if($jawab_detail['tanya_id']==$val->id && $jawab_detail['jawaban']==4) 
                                <strong>X</strong>
                            @endif
                        </td>
                        <td class="text-center" style="vertical-align: middle;">
                            @if($jawab_detail['tanya_id']==$val->id && $jawab_detail['jawaban']==5) 
                                <strong>X</strong>
                            @endif
                        </td>
                    </tr>
                @empty
                @endforelse
                <tr>
                    <td colspan="7">
                        <strong style="text-decoration: underline;">SARAN</strong>
                        @if ($saran = $survey_jawab_detail->where('tanya_id', null)->where('jenis', 0)->first())
                            <div style="margin-top: 12px;">{{ $saran->jawaban }}</div>
                        @else
                            <div style="margin-top: 12px; height: 40px;"> </div>
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
        
    </main>
</body>
</html>