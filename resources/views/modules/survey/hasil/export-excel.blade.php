@php
    $title = $record['title'];
    $survey = $record['survey'];
    $tahun = $record['tahun'];
@endphp
<table style="table-layout: fixed; border: 1px solid black;">
    <thead>
        <tr>
            <th colspan="7" style="font-size: 16px; text-align: center; vertical-align: top; padding: 0; height: 30">
                {{ $title }}
            </th>
        </tr>
        <tr>
            <th colspan="7" style="text-align: center; vertical-align: top; height: 30">
                Tahun : {{ $tahun }}
                <br>
                Version : {{ $survey->version }}                      
            </th>
        </tr>
        <tr></tr>
        <tr>
            <th style="text-align: center; vertical-align: top; border: 1px solid black; background-color: #ecf3f9;">No</th>
            <th style="text-align: center; vertical-align: top; border: 1px solid black; background-color: #ecf3f9;">Uraian</th>
            <th style="text-align: center; vertical-align: top; border: 1px solid black; background-color: #ecf3f9; word-wrap: break-word;">Sangat Tidak Setuju</th>
            <th style="text-align: center; vertical-align: top; border: 1px solid black; background-color: #ecf3f9; word-wrap: break-word;">Tidak Setuju</th>
            <th style="text-align: center; vertical-align: top; border: 1px solid black; background-color: #ecf3f9; word-wrap: break-word;">Setuju</th>
            <th style="text-align: center; vertical-align: top; border: 1px solid black; background-color: #ecf3f9; word-wrap: break-word;">Kurang Setuju</th>
            <th style="text-align: center; vertical-align: top; border: 1px solid black; background-color: #ecf3f9; word-wrap: break-word;">Sangat Setuju</th>
        </tr>
    </thead>
    <tbody>
        @php
            $no = 1;
            $total_1 = 0;
            $total_2 = 0;
            $total_3 = 0;
            $total_4 = 0;
            $total_5 = 0;
        @endphp
        @forelse ($survey->pertanyaan as $key => $val)
            @php
                $jawaban_1 = 0;
                $jawaban_2 = 0;
                $jawaban_3 = 0;
                $jawaban_4 = 0;
                $jawaban_5 = 0;
                $survey_jawab = $survey->survey_jawab()->whereYear('created_at', '=', $tahun)->get();
                if (count($survey_jawab) > 0) {
                    foreach ($survey_jawab as $key => $value) {
                        if($value->status == 2){
                            $jawaban_1 += $value->survey_jawab_detail->where('tanya_id', $val->id)->where('jawaban', 1)->count();
                            $jawaban_2 += $value->survey_jawab_detail->where('tanya_id', $val->id)->where('jawaban', 2)->count();
                            $jawaban_3 += $value->survey_jawab_detail->where('tanya_id', $val->id)->where('jawaban', 3)->count();
                            $jawaban_4 += $value->survey_jawab_detail->where('tanya_id', $val->id)->where('jawaban', 4)->count();
                            $jawaban_5 += $value->survey_jawab_detail->where('tanya_id', $val->id)->where('jawaban', 5)->count();
                        }
                    }
                    $total_1 += $jawaban_1;
                    $total_2 += $jawaban_2;
                    $total_3 += $jawaban_3;
                    $total_4 += $jawaban_4;
                    $total_5 += $jawaban_5;
                }
            @endphp
            
            <tr>
                <td style="border: 1px solid black; text-align: center; vertical-align: top;">{{ $no++.'.' }}</td>
                <td style="border: 1px solid black; text-align: left; vertical-align: top; word-wrap:break-word;">
                    {{ $val->pertanyaan }}
                </td>
                <td style="border: 1px solid black; text-align: center; vertical-align: middle;">
                    {{ $jawaban_1 }}
                </td>
                <td style="border: 1px solid black; text-align: center; vertical-align: middle;">
                    {{ $jawaban_2 }}
                </td>
                <td style="border: 1px solid black; text-align: center; vertical-align: middle;">
                    {{ $jawaban_3 }}
                </td>
                <td style="border: 1px solid black; text-align: center; vertical-align: middle;">
                    {{ $jawaban_4 }}
                </td>
                <td style="border: 1px solid black; text-align: center; vertical-align: middle;">
                    {{ $jawaban_5 }}
                </td>
            </tr>
        @empty
        @endforelse
        @if($survey != null)
            <tr style="font-weight: bold;">
                <td style="text-align: right; vertical-align: top; border: 1px solid black; background-color: #ecf3f9; border-right: 1px solid #ecf3f9;"></td>
                <td style="text-align: right; vertical-align: top; border: 1px solid black; background-color: #ecf3f9; border-left: 1px solid #ecf3f9;">Total</td>
                <td class="text-center" style="text-align: center; vertical-align: top; border: 1px solid black; background-color: #ecf3f9;">
                    {{ $total_1 }}
                </td>
                <td class="text-center" style="text-align: center; vertical-align: top; border: 1px solid black; background-color: #ecf3f9;">
                    {{ $total_2 }}
                </td>
                <td class="text-center" style="text-align: center; vertical-align: top; border: 1px solid black; background-color: #ecf3f9;">
                    {{ $total_3 }}
                </td>
                <td class="text-center" style="text-align: center; vertical-align: top; border: 1px solid black; background-color: #ecf3f9;">
                    {{ $total_4 }}
                </td>
                <td class="text-center" style="text-align: center; vertical-align: top; border: 1px solid black; background-color: #ecf3f9;">
                    {{ $total_5 }}
                </td>
            </tr>
        @endif
    </tbody>
</table>