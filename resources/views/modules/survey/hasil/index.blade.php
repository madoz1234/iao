@extends('layouts.base')
@section('title', 'Hasil Survey')
@include('libs.actions')
@section('side-header')
   <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color: transparent !important;">
        <?php $i=1; $last=count($breadcrumb);?>
         @foreach ($breadcrumb as $name => $link)
            @if($i++ != $last)
                <li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
         @endforeach
      </ol>
    </nav>
@endsection

@section('body')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-8" style="left: 17px;">
                    <form id="dataFilters" class="form-inline" role="form">
                        <div class="form-group">
                            <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
                            <label class="control-label sr-only" for="filter-tahun">Tahun</label>
                            <input type="text" class="form-control filter-control tahun" name="filter[tahun]" data-post="tahun" placeholder="Tahun">
                            <label class="control-label sr-only" for="filter-kategori">Kategori</label>
                            <select class="select selectpicker filter-control kategori" name="filter[kategori]" data-post="kategori" data-style="btn-default">
                                   <option style="font-size: 12px;" value="">Kategori</option>
                                   <option style="font-size: 12px;" value="1">Business Unit (BU)</option>
                                   <option style="font-size: 12px;" value="2">Corporate Office (CO)</option>
                                   <option style="font-size: 12px;" value="3">Project</option>
                                   <option style="font-size: 12px;" value="4">Anak Perusahaan</option>
                            </select>
                            <label class="control-label sr-only" for="filter-object_id">Objek Audit</label>
                            <select class="select selectpicker filter-control show-tick object" name="filter[object_id]" data-post="object_id" data-size="3" data-style="btn-default" data-live-search="true" title="Objek Audit">
                            </select>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="panel-body" style="padding-top: 0px;padding-bottom: 0px;">
            <ul class="nav nav-tabs" id="myTab" role="tablist" style="margin-bottom: 7px;">
                <li class="nav-item tab-a">
                    <a class="nav-link active" id="a-tab" data-toggle="tab" href="#a" role="tab" aria-selected="true">
                    Objek Audit
                    @if ($badgeTab->object_audit > 0)
                    &nbsp;&nbsp;<span class="badge badge-sm down bg-danger pull-right-xs">{{ $badgeTab->object_audit }}</span>
                    @endif
                </a>
                </li>
                <li class="nav-item tab-b">
                    <a class="nav-link" id="b-tab" data-toggle="tab" href="#b" role="tab" aria-selected="false">Item</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="a" role="tabpanel" aria-labelledby="a-tab">
                    <div class="table-responsive">
                        @if(isset($structs['listStruct']))
                            <table id="dataTable1" class="table table-bordered m-t-none" style="width: 100%">
                                <thead>
                                    <tr>
                                        @foreach ($structs['listStruct'] as $struct)
                                        <th class="text-center v-middle">{{ $struct['label'] }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        @endif
                    </div>

                </div>
                <div class="tab-pane active" id="b" role="tabpanel" aria-labelledby="b-tab">
                    <div class="table-responsive">
                        @if(isset($structs['listStruct2']))
                            <table id="dataTable2" class="table table-striped table-bordered m-t-none" style="width: 100%">
                                <thead>
                                    <tr>
                                        @foreach ($structs['listStruct2'] as $struct)
                                        <th class="text-center v-middle">{{ $struct['label'] }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @yield('tableBody')
                                </tbody>
                            </table>
                        @endif
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}">
@endpush

@push('styles')
    <style>
        .panel .dataTables_wrapper{
            padding-top: 0 !important;
        }
        table.dataTable{
            margin-top: 0 !important;
        }
        table.dataTable tr > td{
            vertical-align: middle;
        }
        .table-striped>tbody>tr:nth-child(odd)>td, .table-striped>tbody>tr:nth-child(odd)>th {
            background-color: #eaf0f5;
        }
    </style>
@endpush

@push('js')
    <script type="text/javascript" src="{{ asset('libs/jquery/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.js') }}"></script>
@endpush

@push('scripts')
    <script>
        var modal = '#mediumModal';
        var onShow = function(){
          $('.tahun').datepicker({
                format: "yyyy",
                viewMode: "years", 
                minViewMode: "years",
                orientation: "auto",
                autoclose:true
           });

           $('.bulan-tahun').datepicker({
                format: "mm-yyyy",
                viewMode: "months", 
                minViewMode: "months",
                orientation: "auto",
                autoclose:true
           });

           $('.tanggal').datepicker({
                format: 'dd-mm-yyyy',
                startDate: '-0d',
                orientation: "auto",
                autoclose:true,
            });

            $('.waktu').clockpicker({
                autoclose:true,
                'default': 'now',
            });
        };
    </script>
    <script>
        var dt = 'null';
        var dt2 = 'null';
        
        $(function() {
            dt = $('#dataTable1').DataTable({
                lengthChange: false,
                filter: false,
                processing: true,
                serverSide: true,
                sorting: [],
                language: {
                    url: "{{ asset('libs/jquery/datatables/media/Indonesian.json') }}"
                },
                ajax: {
                    url: '{!! route($routes.'.grid') !!}',
                    method: 'POST',
                    data: function (d) {
                        d._token = "{{ csrf_token() }}";
                        $('#dataFilters .filter-control').each(function(idx, el) {
                            var name = $(el).data('post');
                            var val = $(el).val();
                            d[name] = val;
                        })
                    },
                    error: function (xhr, error, code)
                    {
                        console.log(xhr);
                        console.log(code);
                    }
                },
                columns: {!! json_encode($structs['listStruct']) !!},
                drawCallback: function() {
                    readMoreItem('list-more1');
                    readMoreItem('list-more2');
                    var api = this.api();

                    api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i, x, y) {
                        cell.innerHTML = parseInt(cell.innerHTML)+i+1;
                    });
                    $("[data-toggle='toggle']").bootstrapToggle('destroy')                 
                    $("[data-toggle='toggle']").bootstrapToggle();
                    
                    $('[data-toggle=tooltip]').tooltip()
                    $('[data-toggle=popover]').popover({
                        trigger: 'hover',
                        placement: 'top',
                        template: '<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-content"></div></div>'
                    });
                    $("[data-toggle='toggle']").bootstrapToggle('destroy')                 
                    $("[data-toggle='toggle']").bootstrapToggle();
                    var cek = api.page.info().recordsTotal;
                    if(cek > 0){
                        $('.for-isi1').show();
                        $('.for-isi1').text(cek);
                    }else{
                        $('.for-isi1').hide();
                    }
                }
            });

            dt2 = $('#dataTable2').DataTable({
                lengthChange: false,
                filter: false,
                processing: true,
                serverSide: true,
                // info: false,
                // paging: false,
                sorting: [],
                language: {
                    url: "{{ asset('libs/jquery/datatables/media/Indonesian.json') }}"
                },
                ajax: {
                    url: '{!! route($routes.'.item') !!}',
                    method: 'POST',
                    data: function (d) {
                        d._token = "{{ csrf_token() }}";
                        $('#dataFilters .filter-control').each(function(idx, el) {
                            var name = $(el).data('post');
                            var val = $(el).val();
                            d[name] = val;
                        })
                    },
                    error: function (responseError, status) {
                        console.log(responseError);
                        return false;
                    }
                },
                columns: {!! json_encode($structs['listStruct2']) !!},
                drawCallback: function() {
                    readMoreItem('list-more2');
                    var api = this.api();

                    api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i, x, y) {
                        cell.innerHTML = parseInt(cell.innerHTML)+i+1;
                    });
                    
                    $('[data-toggle=tooltip]').tooltip()
                    $('[data-toggle=popover]').popover({
                        trigger: 'hover',
                        placement: 'top',
                        template: '<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-content"></div></div>'
                    });
                    $("[data-toggle='toggle']").bootstrapToggle('destroy')                 
                    $("[data-toggle='toggle']").bootstrapToggle();
                }
            });

            $('select[name="filter[page]"]').on('change', function(e) {
                var length = this.value;
                length = (length != '') ? length : 10;
                dt.page.len(length).draw();
                e.preventDefault();
            });

            $('.filter.button').on('click', function(e) {
                dt.draw();
                dt2.draw();
                e.preventDefault();
            });

            $('.filter-control').on('change, keyup, changeDate', function(e) {
                dt.draw();
                dt2.draw();
                e.preventDefault();
            });
            
            $('.filter-control').on('changed.bs.select', function(e) {
                dt.draw();
                dt2.draw();
                e.preventDefault();
            });

            $('.reset.button').on('click', function(e) {
                $('.ui.dropdown').dropdown('clear');
                setTimeout(function() {
                    dt.draw();
                    dt2.draw();
                }, 200);
            });
        });
    </script>
    <script>
        $('.tahun').datepicker({
            format: "yyyy",
            viewMode: "years", 
            minViewMode: "years",
            orientation: "auto",
            autoclose:true
        });

        $('.kategori').on('change', function(){
            $.ajax({
                url: '{{ url('ajax/option/get-kategoris') }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    kategori: this.value
                },
            })
            .done(function(response) {
                $('select[name="filter[object_id]"]').html(response);
                $('select[name="filter[object_id]"]').selectpicker("refresh");
            })
            .fail(function() {
                console.log("error");
            });
        });
        
        $('#myTab li:first-child a').tab('show');
        $(document).on('click', '.tab-a', function (e){
            dt1 = $('#dataTable1').DataTable();
            dt1.draw();
        }); 
        $(document).on('click', '.tab-b', function (e){
            dt2 = $('#dataTable2').DataTable();
            dt2.draw();
        }); 

        $(document).on('click', '.detil-survey.button', function(e){
            var idx = $(this).data('id');
            var url = "{!! route($routes.'.index') !!}/" + idx + '/detil-survey';
            window.location = url;
        });

        $(document).on('click', '.detil-item.button', function(e){
            var idx = $(this).data('id');
            var tahunx = $(this).data('tahun');
            var url = "{!! route($routes.'.index') !!}/" + idx + "/" + tahunx + "/detil-item";
            window.location = url;
        });

        $(document).on('click', '.detil-item-excel.button', function(e){
            var idx = $(this).data('id');
            var tahunx = $(this).data('tahun');
            var url = "{!! route($routes.'.index') !!}/" + idx + "/" + tahunx + "/downloadExcel";
            window.location = url;
        });

        $(document).on('click', '.cetak.button', function(e){
            var idx = $(this).data('id');
            var url = "{!! route($routes.'.index') !!}/" + idx + '/cetak';
            window.open(url, '_blank');
        });
    </script>
@endpush