@extends('layouts.base')
@section('title', 'Detil Item')
@section('side-header')
	<nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color: transparent !important;">
        <?php $i=1; $last=count($breadcrumb);?>
         @foreach ($breadcrumb as $name => $link)
            @if($i++ != $last)
                <li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
         @endforeach
      </ol>
    </nav>
@endsection

@section('body')
    <div class="panel panel-default">
        <div class="panel-body">
	        <div class="table-responsive">
	            @if(isset($structs['listStruct3']))
	            <table class="table table-striped table-bordered" style="width: 100%; font-size: 12px; margin: 3px 0 15px;">
	                <thead>
	                    <tr>
							<th class="v-middle text-center" style="width: 40px!important">No</th>
							<th class="v-middle text-center">Pernyataan</th>
							<th class="v-middle text-center" style="width: 80px!important">Sangat Tidak Setuju</th>
							<th class="v-middle text-center" style="width: 80px!important">Tidak Setuju</th>
							<th class="v-middle text-center" style="width: 80px!important">Kurang Setuju</th>
							<th class="v-middle text-center" style="width: 80px!important">Setuju</th>
							<th class="v-middle text-center" style="width: 80px!important">Sangat Setuju</th>
						</tr>
	                </thead>
	                <tbody>
	                	@php
	                		$no = 1;
	                		$total_1 = 0;
            				$total_2 = 0;
            				$total_3 = 0;
            				$total_4 = 0;
            				$total_5 = 0;
	                	@endphp
		                    @forelse ($records->pertanyaan as $key => $val)
	                			@php
	                				$jawaban_1 = 0;
	                				$jawaban_2 = 0;
	                				$jawaban_3 = 0;
	                				$jawaban_4 = 0;
	                				$jawaban_5 = 0;
                                    $survey_jawab = $records->survey_jawab()->whereYear('created_at', '=', $tahun)->get();
                                    if (count($survey_jawab) > 0) {
    	                				foreach ($survey_jawab as $key => $value) {
    	                					if($value->status == 2){
    	                						$jawaban_1 += $value->survey_jawab_detail->where('tanya_id', $val->id)->where('jawaban', 1)->count();
    	                						$jawaban_2 += $value->survey_jawab_detail->where('tanya_id', $val->id)->where('jawaban', 2)->count();
    	                						$jawaban_3 += $value->survey_jawab_detail->where('tanya_id', $val->id)->where('jawaban', 3)->count();
    	                						$jawaban_4 += $value->survey_jawab_detail->where('tanya_id', $val->id)->where('jawaban', 4)->count();
    	                						$jawaban_5 += $value->survey_jawab_detail->where('tanya_id', $val->id)->where('jawaban', 5)->count();
    	                					}
    	                				}
    			                		$total_1 += $jawaban_1;
    		            				$total_2 += $jawaban_2;
    		            				$total_3 += $jawaban_3;
    		            				$total_4 += $jawaban_4;
    		            				$total_5 += $jawaban_5;
                                    }
	                			@endphp
		                    	<tr>
		                    		<td class="text-center">{{ $no++.'.' }}</td>
		                    		<td style="text-align: left;">
		                    			{{ $val->pertanyaan }}
		                    		</td>
		                    		<td class="text-center">
		                    			{{ $jawaban_1 }}
		                    		</td>
		                    		<td class="text-center">
		                    			{{ $jawaban_2 }}
		                    		</td>
		                    		<td class="text-center">
		                    			{{ $jawaban_3 }}
		                    		</td>
		                    		<td class="text-center">
		                    			{{ $jawaban_4 }}
		                    		</td>
		                    		<td class="text-center">
		                    			{{ $jawaban_5 }}
		                    		</td>
		                    	</tr>
		                    @empty
		                    @endforelse
	                    @if($records != null)
	                    	<tr style="font-weight: bold;">
	                    		<td colspan="2" style="background: #c7def1; text-align: right;">Total</td>
	                    		<td class="text-center" style="background: #c7def1">
	                    			{{ $total_1 }}
	                    		</td>
	                    		<td class="text-center" style="background: #c7def1">
	                    			{{ $total_2 }}
	                    		</td>
	                    		<td class="text-center" style="background: #c7def1">
	                    			{{ $total_3 }}
	                    		</td>
	                    		<td class="text-center" style="background: #c7def1">
	                    			{{ $total_4 }}
	                    		</td>
	                    		<td class="text-center" style="background: #c7def1">
	                    			{{ $total_5 }}
	                    		</td>
	                    	</tr>
	                    @endif
	                </tbody>
	            </table>
	            @endif
	        </div>
	        @if (auth()->user()->hasRole(['auditor','svp-audit','dirut']) && $records)
	        <hr>
	        <h3 class="text-center">Grafik Survey Kepuasan Audit</h3>
	        <div class="row">
	            <div class="col-md-6">
	                <div class="chart-container" style="padding: 20px">
	                    <div class="pie-chart-container">
	                        <canvas id="pie-chart-1"></canvas>
	                    </div>
	                </div>
	            </div>
	            <div class="col-md-6">
	                <div class="chart-container" style="padding: 20px">
	                    <div class="pie-chart-container">
	                        <canvas id="pie-chart-2"></canvas>
	                    </div>
	                </div>
	            </div>
	            <div class="col-md-6">
	                <div class="chart-container" style="padding: 20px">
	                    <div class="pie-chart-container">
	                        <canvas id="pie-chart-3"></canvas>
	                    </div>
	                </div>
	            </div>
	            <div class="col-md-6">
	                <div class="chart-container" style="padding: 20px">
	                    <div class="pie-chart-container">
	                        <canvas id="pie-chart-4"></canvas>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="row" style="display: flex; justify-content: center;">
	            <div class="col-md-6">
	                <div class="chart-container" style="padding: 20px">
	                    <div class="pie-chart-container">
	                        <canvas id="pie-chart-5"></canvas>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <br>
	        @endif
            <div class="form-row">
                <div class="form-group col-md-12">
                    <div class="text-right">
                        <button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
                    </div>
                </div>
            </div>
	    </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}">
@endpush

@push('js')
    <script type="text/javascript" src="{{ asset('libs/jquery/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('libs/assets/chartjs/Chart.min.js') }}"></script>
@endpush

@push('styles')
    <style>
        .table-responsive{
            overflow-x: auto;
            overflow-y: visible !important;
        }

        .table-responsive .dropdown-menu {
	        position: static !important;
	    }
	    .table-striped>tbody>tr:nth-child(odd)>td, .table-striped>tbody>tr:nth-child(odd)>th {
	        background-color: #ecf3f9;
	    }
    </style>
@endpush

@push('scripts')
    <script>
    	createPie(JSON.parse(`<?php echo $data_grafik['chart_data_1']; ?>`), '#pie-chart-1', 'Sangat Tidak Setuju');
    	createPie(JSON.parse(`<?php echo $data_grafik['chart_data_2']; ?>`), '#pie-chart-2', 'Tidak Setuju');
    	createPie(JSON.parse(`<?php echo $data_grafik['chart_data_3']; ?>`), '#pie-chart-3', 'Kurang Setuju');
    	createPie(JSON.parse(`<?php echo $data_grafik['chart_data_4']; ?>`), '#pie-chart-4', 'Setuju');
    	createPie(JSON.parse(`<?php echo $data_grafik['chart_data_5']; ?>`), '#pie-chart-5', 'Sangat Setuju');
        function createPie(cData, cId, cTitle) {
            //get the pie chart canvas
            var cData = cData;
            var ctx = $(cId);

            //pie chart data
            var data = {
                labels: cData.label,
                datasets: [
                    {
                    label: "Kepuasan Audit",
                    data: cData.data,
                    backgroundColor: [
                        '#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6', 
                        '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
                        '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A', 
                        '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
                        '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC', 
                        '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
                        '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680', 
                        '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
                        '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3', 
                        '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'
                    ],
                    borderColor: [
                        '#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6', 
                        '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
                        '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A', 
                        '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
                        '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC', 
                        '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
                        '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680', 
                        '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
                        '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3', 
                        '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'
                    ],
                    borderWidth: [1, 1, 1, 1, 1,1,1]
                    }
                ]
            };

            //options
            var options = {
                responsive: true,
                title: {
                    display: true,
                    position: "top",
                    text: cTitle,
                    fontSize: 18,
                    fontColor: "#111"
                },
                legend: {
                    display: true,
                    position: "left",
                    labels: {
                        fontColor: "#333",
                        fontSize: 14,
                        boxWidth: 25,
                    }
                }
            };

            //create Pie Chart class object
            var chart1 = new Chart(ctx, {
                type: "pie",
                data: data,
                options: options
            });
        }
    </script>
@endpush