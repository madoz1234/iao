@extends('layouts.form')
@section('title', 'Detil Survey')
@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection
@section('body')
<div class="panel panel-default">
	<div class="panel-body" style="padding-bottom: 0px;">
		<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
			@csrf
			<div class="form-row">
				<div class="form-group col-md-12" style="margin-bottom: 0;">
					<table class="table table-bordered m-t-none" style="width: 100%;border: 1px #ffffff;font-size: 12px;">
						<tr>
							<td style="padding-left: 0; width: 140px;border: 1px #ffffff;">Nama</td>
							<td style="padding-left: 0; padding-right: 0; width: 5px; border: 1px #ffffff;">:</td>
							<td style="text-align: left;border: 1px #ffffff;" class="field">
								{{ $record->user->name }}
							</td>
						</tr>
						<tr>
							<td style="padding-left: 0; width: 140px;border: 1px #ffffff;">Posisi</td>
							<td style="padding-left: 0; padding-right: 0; width: 5px; border: 1px #ffffff;">:</td>
							<td style="text-align: left;border: 1px #ffffff;" class="field">
								{{ $record->user->posisi }}
							</td>
						</tr>
						<tr>
							<td style="padding-left: 0; width: 140px;border: 1px #ffffff;">Kategori</td>
							<td style="padding-left: 0; padding-right: 0; width: 5px; border: 1px #ffffff;">:</td>
							<td style="text-align: left;border: 1px #ffffff; vertical-align: middle;" class="field">
								@if($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 0)
									<span class="label label-success">Business Unit (BU)</span>
								@elseif($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 1)
									<span class="label label-info">Corporate Office (CO)</span>
								@elseif($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 2)
									<span class="label label-default">Project</span>
								@else 
									<span class="label label-primary">Anak Perusahaan</span>
								@endif
							</td>
						</tr>
						<tr>
							<td style="padding-left: 0; width: 140px;border: 1px #ffffff;">Objek Audit</td>
							<td style="padding-left: 0; padding-right: 0; width: 5px; border: 1px #ffffff;">:</td>
							<td style="text-align: left;border: 1px #ffffff;" class="field">
								{!! object_audit($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->lha->draftkka->program->penugasanaudit->rencanadetail->object_id)!!}
							</td>
						</tr>
						<tr>
							<td style="padding-left: 0; width: 140px;border: 1px #ffffff;">Tanggal Pelaksanaan</td>
							<td style="padding-left: 0; padding-right: 0; width: 5px; border: 1px #ffffff;">:</td>
							<td style="text-align: left;border: 1px #ffffff;" class="field">
								 {{ DateToStringWday($record->lha->draftkka->program->detailpelaksanaan->min('tgl')) }} -  {{DateToStringWday($record->lha->draftkka->program->detailpelaksanaan->max('tgl'))}}
							</td>
						</tr>
						<tr>
							<td style="padding-left: 0; width: 140px;border: 1px #ffffff;">Tanggal Aktif</td>
							<td style="padding-left: 0; padding-right: 0; width: 5px; border: 1px #ffffff;">:</td>
							<td style="text-align: left;border: 1px #ffffff;" class="field">
								 {{ DateToString($record->created_at) }}
							</td>
						</tr>
						<tr>
							<td style="padding-left: 0; width: 140px;border: 1px #ffffff;">Tanggal Aktif</td>
							<td style="padding-left: 0; padding-right: 0; width: 5px; border: 1px #ffffff;">:</td>
							<td style="text-align: left;border: 1px #ffffff;" class="field">
								 {{ DateToString($record->updated_at) }}
							</td>
						</tr>
					</table>
				</div>
			</div>
			
			<div class="form-row">
				<table class="table table-striped table-bordered" style="width: 100%; font-size: 12px; margin: 3px 0 15px;">
					<thead>
						<tr>
							<th class="v-middle text-center" style="width: 50px">No</th>
							<th class="v-middle text-center">Uraian</th>
							<th class="v-middle text-center" style="width: 80px">Sangat Tidak Setuju</th>
							<th class="v-middle text-center" style="width: 80px">Tidak Setuju</th>
							<th class="v-middle text-center" style="width: 80px">Kurang Setuju</th>
							<th class="v-middle text-center" style="width: 80px">Setuju</th>
							<th class="v-middle text-center" style="width: 80px">Sangat Setuju</th>
						</tr>
					</thead>
					<tbody>
						@forelse ($survey->pertanyaan as $key => $val)
						    @php
						        $cek_jawaban = $survey_jawab_detail->where('tanya_id', $val->id)->first();
						        $jawab_detail = ['tanya_id' => '', 'jawaban'=>''];
						        if ($cek_jawaban) {
						            $jawab_detail = [
						                'tanya_id' => $cek_jawaban->tanya_id, 
						                'jawaban'=>$cek_jawaban->jawaban
						            ];
						        }else{
						            continue;
						        }
						    @endphp
						    
						    <tr>
						        <td class="text-center">{{ $loop->iteration.'.' }}</td>
						        <td class="text-left">
						            {{ $val->pertanyaan }}
						        </td>
						        <td class="text-center v-middle">
						            @if($jawab_detail['tanya_id']==$val->id && $jawab_detail['jawaban']==1) 
						                <strong>X</strong>
						            @endif
						        </td>
						        <td class="text-center v-middle">
						            @if($jawab_detail['tanya_id']==$val->id && $jawab_detail['jawaban']==2) 
						                <strong>X</strong>
						            @endif
						        </td>
						        <td class="text-center v-middle">
						            @if($jawab_detail['tanya_id']==$val->id && $jawab_detail['jawaban']==3) 
						                <strong>X</strong>
						            @endif
						        </td>
						        <td class="text-center v-middle">
						            @if($jawab_detail['tanya_id']==$val->id && $jawab_detail['jawaban']==4) 
						                <strong>X</strong>
						            @endif
						        </td>
						        <td class="text-center v-middle">
						            @if($jawab_detail['tanya_id']==$val->id && $jawab_detail['jawaban']==5) 
						                <strong>X</strong>
						            @endif
						        </td>
						    </tr>
						@empty
						@endforelse
						<tr>
						    <td colspan="7" style="text-align: left;">
						        <strong style="text-decoration: underline;">SARAN</strong>
						        @if ($saran = $survey_jawab_detail->where('tanya_id', null)->where('jenis', 0)->first())
						            <div style="margin-top: 12px">{{ $saran->jawaban }}</div>
						        @else
						            <div style="margin-top: 12px; height: 40px;"> </div>
						        @endif
						    </td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<div class="text-right">
						<button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('styles')
    <style>
        .table-responsive{
            overflow-x: auto;
            overflow-y: visible !important;
        }

        .table-responsive .dropdown-menu {
	        position: static !important;
	    }
	    .table-striped>tbody>tr:nth-child(odd)>td, .table-striped>tbody>tr:nth-child(odd)>th {
	        background-color: #ecf3f9;
	    }

	    div.dropdown-menu.open{
		  max-width: 300px !important;
		  overflow: hidden;
		}
		ul.dropdown-menu.inner{
		  max-width: 300px !important;
		  overflow-y: auto;
		}
		.rating{ cursor: pointer; }

		.checkbox{
			margin-top: 0;
			margin-bottom: 0;
		}

		.checkbox label:after, 
		.radio label:after {
		    content: '';
		    display: table;
		    clear: both;
		}

		.checkbox .cr,
		.radio .cr {
		    position: relative;
		    display: inline-block;
		    border: 1px solid #a9a9a9;
		    border-radius: .25em;
		    width: 1.3em;
		    height: 1.3em;
		    float: left;
		}

		.radio .cr {
		    border-radius: 50%;
		}

		.checkbox .cr .cr-icon,
		.radio .cr .cr-icon {
		    position: absolute;
		    font-size: .8em;
		    line-height: 0;
		    top: 50%;
		    left: 20%;
		}

		.radio .cr .cr-icon {
		    margin-left: 0.04em;
		}

		.checkbox label input[type="checkbox"],
		.radio label input[type="radio"] {
		    display: none;
		}

		.checkbox label input[type="checkbox"] + .cr > .cr-icon,
		.radio label input[type="radio"] + .cr > .cr-icon {
		    transform: scale(1) rotateZ(-20deg);
		    opacity: 0;
		    transition: all .3s ease-in;
		}

		.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
		.radio label input[type="radio"]:checked + .cr > .cr-icon {
		    transform: scale(1) rotateZ(0deg);
		    opacity: 1;
		}

		.checkbox label input[type="checkbox"]:disabled + .cr,
		.radio label input[type="radio"]:disabled + .cr {
		    opacity: .5;
		}
    </style>
@endpush

@push('scripts')
    <script>
    	$('#myTab li:first-child a').tab('show')
    	$('.tanggal').datepicker({
			format: 'dd-mm-yyyy',
			startDate: '-0d',
			orientation: "bottom",
			autoclose:true,
		});
		$(".set-radio").change(function() {
	        var name = $(this).attr('data-name'),
	        	value = $(this).val();
	        $('input[data-name="' + name + '"]').prop('checked', false);
	        $(this).prop('checked', true);
	        $('input[name="' + name + '"]').val(value);
		});
    </script>
    @yield('js-extra')
@endpush

