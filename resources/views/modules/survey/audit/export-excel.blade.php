@php
    $title = $record['title'];
    $record = $record['data'];
@endphp
<table style="table-layout: fixed; border: 1px solid black;">
    <thead>
        <tr>
            <th colspan="7" style="font-size: 16px; text-align: center; vertical-align: top;">
                {{ $title }}
            </th>
        </tr>
        <tr></tr>
        <tr>
            <th colspan="2">Nama</th>
            <th colspan="5">: {{ $record->user->name }}</th>
        </tr>
        <tr>
            <th colspan="2">Posisi</th>
            <th colspan="5">: {{ $record->user->posisi }}</th>
        </tr>
        <tr>
            <th colspan="2">Kategori</th>
            <th colspan="5">: 
                @if($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 0)
                    Business Unit (BU)
                @elseif($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 1)
                    Corporate Office (CO)
                @elseif($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 2)
                    Project
                @else 
                    Anak Perusahaan
                @endif
            </th>
        </tr>
        <tr>
            <th colspan="2">Objek Audit</th>
            <th colspan="5">: 
                {{ object_audit($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->lha->draftkka->program->penugasanaudit->rencanadetail->object_id) }}
            </th>
        </tr>
        <tr>
            <th colspan="2">Tanggal Pelaksanaan</th>
            <th colspan="5">: 
                 {{ DateToString($record->lha->draftkka->program->detailpelaksanaan->first()->tgl).' - '.DateToString($record->lha->draftkka->program->detailpelaksanaan->last()->tgl) }}
            </th>
        </tr>
        <tr>
            <th colspan="2">Tanggal Aktif</th>
            <th colspan="5">: 
                 {{ DateToString($record->created_at) }}
            </th>
        </tr>
        <tr>
            <th colspan="2">Tanggal Submit</th>
            <th colspan="5">: 
                {{ DateToString($record->updated_at) }}
            </th>
        </tr>
        <tr></tr>
        <tr>
            <th colspan="7">Isilah pernyataan-pernyataan di bawah ini dengan tanda silang (X)</th>
        </tr>
        <tr></tr>
        <tr>
            <th style="text-align: center; vertical-align: top; border: 1px solid black; background-color: #ecf3f9;">No</th>
            <th style="text-align: center; vertical-align: top; border: 1px solid black; background-color: #ecf3f9;">Uraian</th>
            <th style="text-align: center; vertical-align: top; border: 1px solid black; background-color: #ecf3f9; word-wrap: break-word;">Sangat Tidak Setuju</th>
            <th style="text-align: center; vertical-align: top; border: 1px solid black; background-color: #ecf3f9; word-wrap: break-word;">Tidak Setuju</th>
            <th style="text-align: center; vertical-align: top; border: 1px solid black; background-color: #ecf3f9; word-wrap: break-word;">Setuju</th>
            <th style="text-align: center; vertical-align: top; border: 1px solid black; background-color: #ecf3f9; word-wrap: break-word;">Kurang Setuju</th>
            <th style="text-align: center; vertical-align: top; border: 1px solid black; background-color: #ecf3f9; word-wrap: break-word;">Sangat Setuju</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($record->survey->pertanyaan as $key => $val)
            @php
                $cek_jawaban = $record->survey_jawab_detail->where('tanya_id', $val->id)->first();
                $jawab_detail = ['tanya_id' => '', 'jawaban'=>''];
                if ($cek_jawaban) {
                    $jawab_detail = [
                        'tanya_id' => $cek_jawaban->tanya_id, 
                        'jawaban'=>$cek_jawaban->jawaban
                    ];
                }else{
                    continue;
                }
            @endphp
            
            <tr>
                <td style="border: 1px solid black; text-align: center; vertical-align: top;">{{ $loop->iteration.'.' }}</td>
                <td style="border: 1px solid black; text-align: left; vertical-align: top; word-wrap:break-word;">
                    {{ $val->pertanyaan }}
                </td>
                <td style="border: 1px solid black; text-align: center; vertical-align: middle;">
                    @if($jawab_detail['tanya_id']==$val->id && $jawab_detail['jawaban']==1) 
                        X
                    @endif
                </td>
                <td style="border: 1px solid black; text-align: center; vertical-align: middle;">
                    @if($jawab_detail['tanya_id']==$val->id && $jawab_detail['jawaban']==2) 
                        X
                    @endif
                </td>
                <td style="border: 1px solid black; text-align: center; vertical-align: middle;">
                    @if($jawab_detail['tanya_id']==$val->id && $jawab_detail['jawaban']==3) 
                        X
                    @endif
                </td>
                <td style="border: 1px solid black; text-align: center; vertical-align: middle;">
                    @if($jawab_detail['tanya_id']==$val->id && $jawab_detail['jawaban']==4) 
                        X
                    @endif
                </td>
                <td style="border: 1px solid black; text-align: center; vertical-align: middle;">
                    @if($jawab_detail['tanya_id']==$val->id && $jawab_detail['jawaban']==5) 
                        X
                    @endif
                </td>
            </tr>
        @empty
        @endforelse
        <tr style="border: 1px solid black; border-bottom: none;">
            <td colspan="7" style="text-decoration: underline;">SARAN :</td>
        </tr>
        @if ($saran = $record->survey_jawab_detail->where('tanya_id', null)->where('jenis', 0)->first())
        <tr style="border: 1px solid black; border-top: none;">
            <td colspan="7" style="text-align: left; vertical-align: top; word-wrap: break-word;">{{ $saran->jawaban }}</td>
        </tr>
        @else
        <tr style="border: 1px solid black; border-top: none;">
            <td colspan="7" style="height: 40px"></td>
        </tr>
        @endif
    </tbody>
</table>