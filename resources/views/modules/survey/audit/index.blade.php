@extends('layouts.list-multi-grid')

@section('title', 'Survey')

@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection

@section('filters')
<div class="form-group">
    <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
    <label class="control-label sr-only" for="filter-tahun">Tahun</label>
    <input type="text" class="form-control filter-control tahun" name="filter[tahun]" data-post="tahun" placeholder="Tahun">
    <label class="control-label sr-only" for="filter-kategori">Kategori</label>
    <select class="select selectpicker filter-control kategori" name="filter[kategori]" data-post="kategori" data-style="btn-default">
           <option style="font-size: 12px;" value="">Kategori</option>
           <option style="font-size: 12px;" value="1">Business Unit (BU)</option>
           <option style="font-size: 12px;" value="2">Corporate Office (CO)</option>
           <option style="font-size: 12px;" value="3">Project</option>
           <option style="font-size: 12px;" value="4">Anak Perusahaan</option>
    </select>
    <label class="control-label sr-only" for="filter-object_id">Objek Audit</label>
    <select class="select selectpicker filter-control show-tick object" name="filter[object_id]" data-post="object_id" data-size="3" data-style="btn-default" data-live-search="true" title="Objek Audit">
	</select>
</div>
@endsection
@section('tables')
	<ul class="nav nav-tabs" id="myTab" role="tablist" style="margin-bottom: 7px;">
		<li class="nav-item tab-a">
			<a class="nav-link active" id="a-tab" data-toggle="tab" href="#a" role="tab" aria-selected="true">
                Baru
                @if ($badgeTab->baru > 0)
                &nbsp;&nbsp;<span class="badge badge-sm down bg-danger pull-right-xs">{{ $badgeTab->baru }}</span>
                @endif
            </a>
		</li>
		<li class="nav-item tab-c">
			<a class="nav-link" id="c-tab" data-toggle="tab" href="#c" role="tab" aria-selected="false">Historis</a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="a" role="tabpanel" aria-labelledby="a-tab">
			<div class="table-responsive">
				@if(isset($structs['listStruct']))
					<table id="dataTable1" class="table table-bordered m-t-none" style="width: 100%">
						<thead>
							<tr>
								@foreach ($structs['listStruct'] as $struct)
								<th class="text-center v-middle">{{ $struct['label'] }}</th>
								@endforeach
							</tr>
						</thead>
						<tbody>
							@yield('tableBody')
						</tbody>
					</table>
				@endif
			</div>
		</div>
		<div class="tab-pane active" id="c" role="tabpanel" aria-labelledby="c-tab">
			<div class="table-responsive">
				@if(isset($structs['listStruct3']))
					<table id="dataTable3" class="table table-bordered m-t-none" style="width: 100%">
						<thead>
							<tr>
								@foreach ($structs['listStruct3'] as $struct)
								<th class="text-center v-middle">{{ $struct['label'] }}</th>
								@endforeach
							</tr>
						</thead>
						<tbody>
							@yield('tableBody')
						</tbody>
					</table>
				@endif
			</div>
		</div>
	</div>
@endsection
@push('styles')
    <style>
        .swal-text {
            text-align: center;
            padding-left: 15px;
            padding-right: 15px;
        }
    </style>
@endpush
@push('scripts')
    <script>
    	$('.tahun').datepicker({
            format: "yyyy",
		    viewMode: "years", 
		    minViewMode: "years",
            orientation: "auto",
            autoclose:true
        });

    	$('.kategori').on('change', function(){
			$.ajax({
				url: '{{ url('ajax/option/get-kategoris') }}',
				type: 'POST',
				data: {
					_token: "{{ csrf_token() }}",
					kategori: this.value
				},
			})
			.done(function(response) {
				$('select[name="filter[object_id]"]').html(response);
				$('select[name="filter[object_id]"]').selectpicker("refresh");
			})
			.fail(function() {
				console.log("error");
			});
		});
		
    	$('#myTab li:first-child a').tab('show');
    	$(document).on('click', '.tab-a', function (e){
    		dt1 = $('#dataTable1').DataTable();
    		dt1.draw();
		}); 
		$(document).on('click', '.tab-b', function (e){
    		dt2 = $('#dataTable2').DataTable();
    		dt2.draw();
		}); 
		$(document).on('click', '.tab-c', function (e){
    		dt3 = $('#dataTable3').DataTable();
    		dt3.draw();
		});

		$(document).on('click', '.programaudit-detil.button', function(e){
        	var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/detil';
            window.location = url;
        });

        $(document).on('click', '.buat.button', function(e){
            var idx = $(this).data('id');
            var time = $(this).data('time');
            var is_countdown = $(this).data('is_countdown');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/buat';
            var show_time = convert_time(time);
            if (is_countdown == 1) {
                var message = "Mohon Bapak/Ibu meluangkan waktu " + convert_time(time) + " untuk mengisi survey berikut ini";
            }else{
                var message = "Bapak/Ibu telah melebihi waktu " + convert_time(time) + ", mohon segera menyelesaikan survey berikut ini";
            }
            swal({
                    title: "Lengkapi Survey",
                    text: message,
                    icon: "warning",
                    buttons: ['Batal', 'OK'],
                    reverseButtons: true
            }).then((result) => {
                if (result) {
                    window.location = url;
                }
            })
        });

        $(document).on('click', '.ubah.button', function(e){
            var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/ubah';
            window.location = url;
        });

        $(document).on('click', '.resume.button', function(e){
            var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/resume';
            window.location = url;
        });

        $(document).on('click', '.cetak.button', function(e){
            var idx = $(this).data('id');
        	var url = "{!! route($routes.'.index') !!}/" + idx + '/cetak';
            window.open(url, '_blank');
        });

        $(document).on('click', '.download-lha.button', function(e){
            var idx = $(this).data('id');
            var url = "{!! route($routes.'.index') !!}/" + idx + '/downloadLHA';
            window.location = url;
        });

        function convert_time(time) {
            var y, jam, menit, detik;
            y     = time % 3600;
            jam   = time / 3600;
            menit = y / 60;
            detik = y % 60;

            var result_jam = '';
            var result_menit = '';
            var result_detik = '';

            if(Math.floor(jam) > 0){
                result_jam = Math.floor(jam) + ' jam ';
            }
            if(Math.floor(menit) > 0){
                result_menit = Math.floor(menit) + ' menit ';
            }
            if(Math.floor(menit) == 0 && Math.floor(detik) > 0){
                result_detik = Math.floor(detik) + ' detik ';
            }
            return result_jam + result_menit + result_detik;
        }

        $(document).on('click', '.download-excel.button', function(e){
            var idx = $(this).data('id');
            var url = "{!! route($routes.'.index') !!}/" + idx + '/downloadExcel';
            window.location = url;
        });
    </script>
    @yield('js-extra')
@endpush