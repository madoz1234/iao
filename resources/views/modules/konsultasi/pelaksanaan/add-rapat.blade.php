@extends('layouts.form')
@section('title', 'Buat Agenda')
@section('side-header')
   <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color: transparent !important;">
        <?php $i=1; $last=count($breadcrumb);?>
         @foreach ($breadcrumb as $name => $link)
            @if($i++ != $last)
                <li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
         @endforeach
      </ol>
    </nav>
@endsection
@section('body')
<form action="{{ route($routes.'.store', $record->id) }}" method="POST" id="formData" autocomplete="off">
@csrf
<input type="hidden" name="id" value="{{ $record->id }}">
<input type="hidden" name="add_rapat" value="true">
<input type="hidden" name="status" value="{{ $record->status }}">
<input type="hidden" name="jenis_rapat" value="{{ $record->jenis_rapat }}">
<div class="panel panel-default">
    <input type="hidden" name="flag">
    <div class="panel-body" style="padding-bottom: 0px;">
        <div class="form-row">
            {{-- kiri --}}
            <div class="form-group col-xs-6" style="padding-right: 14px;">
                <div class="form-group col-xs-12 field" style="padding-right: 14px;">
                    <label for="inputEmail4">Tahun</label>
                    <input type="text" class="form-control" value="{{ $record->rkia->rencanaaudit->tahun }}" readonly>
                </div>
                <div class="form-group col-xs-12 field" style="padding-right: 14px;">
                    <label for="inputEmail4">Rencana Pelaksanaan</label>
                    <input type="text" class="form-control" value="{{ $record->rkia->rencana }}" readonly>
                </div>
                <div class="col-xs-12" style="padding-right: 14px;">
                    <div class="form-row">
                        <div class="form-group col-xs-6 field" style="padding-right: 14px;">
                            <label for="inputEmail4">Tipe</label>
                            <input type="text" class="form-control" value="Kegiatan Konsultasi" readonly>
                        </div>
                        <div class="form-group col-xs-6 field" style="padding-left: 14px;">
                            <label for="inputEmail4">Jenis</label>
                            <input type="text" class="form-control" value="{{ $record->jenis }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group col-xs-12 field" style="padding-right: 14px;">
                    <label for="inputEmail4">Nomor Rapat</label>
                    <input type="text" name="nomor" class="form-control" placeholder="Nomor Rapat">
                    <input type="hidden" class="form-control" name="jumlah_peserta" value="0" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1')" placeholder="Jumlah Peserta">
                </div>
                <div class="form-group col-xs-12 field" style="padding-right: 14px;">
                    <label for="inputEmail4">Tempat</label>
                    <input type="text" name="tempat" class="form-control" placeholder="Tempat">
                </div>
                <div class="col-xs-12" style="padding-right: 14px;">
                    <div class="form-row">
                        <div class="form-group col-xs-6 field" style="padding-right: 14px;">
                            <label for="inputEmail4">Hari</label>
                            <input type="text" name="hari" class="form-control" placeholder="Hari" disabled="" id="hari">
                        </div>
                        <div class="form-group col-xs-6 field" style="padding-left: 14px;">
                            <label for="inputEmail4">Tanggal</label>
                            <div class="input-group" data-placement="left" data-align="top" data-autoclose="true">
                                <input type="text" class="form-control" name="tanggal" placeholder="Tanggal Pelaksanaan" id="tgl_pelaksanaan" value="{{ $record->tanggal }}">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12" style="padding-right: 14px;">
                    <div class="form-row">
                        <div class="form-group col-xs-6 field" style="padding-right: 14px;">
                            <label for="inputEmail4">Jam</label>
                            <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                <input type="text" class="form-control" name="jam_mulai" placeholder="Mulai">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-xs-6 field" style="padding-left: 14px;">
                            <label for="inputEmail4">&nbsp</label>
                            <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                <input type="text" class="form-control" name="jam_selesai" placeholder="Selesai">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- kanan --}}
            <div class="form-group col-xs-6" style="padding-left: 14px;">
                <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                    <label for="inputEmail4">Kategori</label>
                    <input type="text" class="form-control" value="{{ $record->rkia->konsultasi->nama }}" readonly>
                </div>
                <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                    <label for="inputEmail4">Pimpinan Rapat</label>
                    <input type="text" class="form-control" value="{{ $record->pimpinan->name }}" readonly>
                </div>
                <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                    <label for="inputEmail4">Notulen</label>
                    <input type="text" class="form-control" value="{{ $record->notulen->name }}" readonly>
                </div>
                <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                    <table id="example-materi" style="width: 100%;font-size: 12px;">
                        <tbody class="container-materi">
                            <tr class="data-container-materi-1" data-id="1">
                                <td scope="row">
                                    <div class="field">
                                        <label for="inputEmail4">Agenda</label>
                                        <input type="text" name="data[0][agenda]" class="form-control" placeholder="Agenda">
                                    </div>
                                </td>
                                <td style="text-align: center; padding-top: 15px;">
                                    <button class="btn btn-sm btn-success tambah_materi" type="button"><i class="fa fa-plus"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="form-group col-xs-12 field" style="padding-left: 14px;">
                    <label for="inputEmail4">Keterangan Rapat</label>
                    <textarea name="keterangan" rows="3" class="form-control" placeholder="Keterangan Rapat"></textarea>
                </div>
            </div>
        </div>
        @if ($record->jenis_rapat == 1)
            <div class="col-sm-12">
                <h4 class="m-t-lg m-b" style="font-weight: bold;">Peserta Undangan</h4>
                <table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
                    <thead style="background-color: #f9fafb;">
                        <tr>
                            <th scope="col" style="text-align: center; width: 5%;">#</th>
                            <th scope="col" style="text-align: center; width: 20%;">Nama</th>
                            <th scope="col" style="text-align: center; width: 15%;">Email</th>
                            <th scope="col" style="text-align: center; width: 15%;">No Telp/HP</th>
                            <th scope="col" style="text-align: center; width: 15%;">Jabatan</th>
                            <th scope="col" style="text-align: center; width: 15%;">BU/CO</th>
                            <th scope="col" style="text-align: center; width: 40%;">Project</th>
                            <th scope="col" style="text-align: center; width: 5%;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody class="container">
                        <tr class="data-container-1" data-id="1">
                            <td scope="row" style="text-align: center;">
                                <label class="numboor-1">1</label>
                            </td>
                            <td scope="row">
                                <div class="field">
                                    <select class="selectpicker form-control user" 
                                    name="detail[0][user_id]"
                                    data-style="btn-default" 
                                    data-live-search="true" 
                                    title="(Nama)"
                                    data-size="5"
                                    >
                                    {{-- @foreach(App\Models\Auths\User::where('id','<>', 1)->where('tipe', 1)->get() as $user) --}}
                                    @foreach (App\Models\Auths\User::where('id','<>', 1)->where('tipe', 1)->whereNotIn('email', ['', '-'])->whereNotNull('email')->get() as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </td>
                            <td scope="row">
                                <input type="email" name="detail[0][email]" id="email" class="form-control" placeholder="Email" readonly="">
                            </td>
                            <td scope="row">
                                <input type="text" class="form-control" id="hp" placeholder="No Hp" readonly="">
                            </td>
                            <td scope="row">
                                <input type="text" class="form-control" id="jabatan" placeholder="Jabatan" readonly="">
                            </td>
                            <td scope="row">
                                <input type="text" name="detail[0][bu_co]" id="bu_co" class="form-control" placeholder="BU/CO" readonly="">
                            </td>
                            <td scope="row">
                                <select class="selectpicker form-control project" 
                                    name="detail[0][project_id][]"
                                    {{-- name="project_id[]" --}}
                                    {{-- name="project[0][project_id][]" --}}
                                    data-style="btn-default" 
                                    data-live-search="true" 
                                    title="(Pilih Project)"
                                    data-size="5"
                                    id="project"
                                    multiple=""
                                    data-width="500px" 
                                    >
                                    {{-- <option value="0">Tidak ada pilihan</option> --}}
                                </select>
                            </td>
                            <td style="text-align: center;">
                                <button class="btn btn-sm btn-success tambah_peserta_internal" type="button"><i class="fa fa-plus"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        @else
            <div class="col-sm-12">
                {{-- daftar peserta --}}
                <h4 class="m-t-lg m-b" style="font-weight: bold;">Peserta Undangan</h4>
                <table id="example" class="table table-bordered m-t-none" style="width: 100%;font-size: 12px;">
                    <thead style="background-color: #f9fafb;">
                        <tr>
                            <th scope="col" style="text-align: center; width: 5%;">#</th>
                            <th scope="col" style="text-align: center; width: 45%;">Email</th>
                            <th scope="col" style="text-align: center; width: 45%;">Perusahaan</th>
                            <th scope="col" style="text-align: center; width: 5%;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody class="container">
                        <tr class="data-container-1" data-id="1">
                            <td scope="row" style="text-align: center;">
                                <label class="numboor-1">1</label>
                            </td>
                            <td scope="row">
                                <div class="field">
                                    <input type="text" name="detail[0][email]" class="form-control" placeholder="Email">
                                </div>
                            </td>
                            <td scope="row">
                                <div class="field">
                                    <input type="text" name="detail[0][perusahaan]" class="form-control" placeholder="Perusahaan">
                                </div>
                            </td>
                            <td style="text-align: center;">
                                <button class="btn btn-sm btn-success tambah_peserta_eksternal" type="button"><i class="fa fa-plus"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        @endif
        <div class="form-row">
            <div class="form-group col-xs-12">
                <br>
                <div class="text-right">
                    <button type="button" class="btn btn-cancel back" onclick="window.history.back()">Kembali</button>
                    <button type="button" class="btn btn-primary save-as-draft button">Save As Draft</button>
                    <button type="button" class="btn btn-simpan save-as-submit button">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('libs/jquery/jquery-ui-1.12.1/jquery-ui.min.css') }}" type="text/css" />
    <style>
        div.dropdown-menu.open{
          max-width: 500px !important;
          overflow: hidden;
        }
        ul.dropdown-menu.inner{
          max-width: 500px !important;
          overflow-y: auto;
        }
        .table.table-bordered .container tr {
            border: 1px solid #eaeff0;
        }
        .text-error {
            color: #a94442;
        }
        .field .bootstrap-select.has-error .btn.dropdown-toggle {
            border: 1px solid #a94442;
        }
        .field .bootstrap-select.has-error .btn.dropdown-toggle .filter-option{
            color: #a94442;
        }
    </style>
@endpush

@push('js')
    <script src="{{ asset('libs/jquery/jquery-ui-1.12.1/jquery-ui.js') }}"></script>
    <script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
    <script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
    <script> 
        var routes = '{{ route($routes.'.index') }}';
        $(document).ready(function(){
            $('.user').on('change', function(){
                $.ajax({
                    url: '{{ url('ajax/option/get-user') }}',
                    type: 'POST',
                    data: {
                        _token: "{{ csrf_token() }}",
                        user_id: this.value
                    },
                })
                .done(function(response) {
                    $('#email').val(response.email);
                    $('#hp').val(response.phone);
                    $('#jabatan').val(response.jabatan);
                    $('#bu_co').val(response.bu);
                    $('#project').html(response.project);

                    $('.user').selectpicker("refresh");
                    $('.project').selectpicker("refresh");
                })
                .fail(function() {
                    console.log("error");
                });
            })

            $('.project').on('change', function(){
                // clear selected
                var data = $(this).val();
            
                if(jQuery.inArray('0', data) !== -1){
                    $('.project').selectpicker('deselectAll');
                    $('.project').selectpicker('val', '0');
                }
                $.ajax({
                    url: '{{ url('ajax/option/get-project') }}',
                    type: 'POST',
                    data: {
                        _token: "{{ csrf_token() }}",
                        project_id: this.value
                    },
                })
                .done(function(response) {
                    $('input[name=bu_co]').val(response.bu);
                    $('.user').selectpicker("refresh");
                    $('.project').selectpicker("refresh");
                })
                .fail(function() {
                    console.log("error");
                });
            })
        });

        $(document).ready(function(){
            $('#myTabs a').click(function (e) {
                e.preventDefault()
                $(this).tab('show')
            })
            $('#tgl').datepicker({
                format: 'dd-mm-yy',
                onSelect: function(dateText, inst) {
                    var day = dateText.split(",");            
                    alert('day[0]');
                }
            });
            // tanggal
            $( "#tgl_pelaksanaan" ).datepicker({dateFormat: 'dd-mm-yy'});
            local = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum`at', 'Sabtu' ];
            var today = new Date($('#tgl_pelaksanaan').datepicker('getDate')); 
            $('#hari').val(local[today.getDay()]);

            $('#tgl_pelaksanaan').datepicker()
            .on("change", function () {    
                var today = new Date($('#tgl_pelaksanaan').datepicker('getDate'));      
                //alert(local[today.getDay()]);
                $('#hari').val(local[today.getDay()]);
            });
            // waktu
            $('.clockpicker').clockpicker();
            
            
            $('.waktu').clockpicker({
                autoclose:true,
                'default': 'now',
            });
            $("#risalah").fileinput({
                autoReplace: true,
                overwriteInitial: true,
                maxFileCount: 1,
            });
            $("#hadir").fileinput({
                autoReplace: true,
                overwriteInitial: true,
                maxFileCount: 1,
            });
            $('#other').fileinput({
                theme: 'explorer-fas',
            });
        });

        $(document).on('click','.next-tab', function(){
            var next = $('.nav-tabs > .active').next('li');
            if(next.length){
                next.find('a').trigger('click');
            }else{
                $('#myTabs a:first').tab('show');
            }
        });

        $(document).on('click','.previous-tab', function(){
            var prev = $('.nav-tabs > .active').prev('li')
            if(prev.length){
                prev.find('a').trigger('click');
            }else{
                $('#myTabs a:last').tab('show');
            }
        });

        $(document).on('click', '.tambah-materi',function(event) {
            $(".daftar-materi").append(`
                <div class="tambah">               
                    <div class="col-xs-11 field" style="padding-right: 14px;">
                        <label for="inputEmail4">&nbsp</label>
                        <input type="text" name="materi" class="form-control" placeholder="Agenda">
                    </div>
                    <div class="col-xs-1" style="padding-left: 14px; padding-top: 25px;">
                        <label for="inputEmail4">&nbsp</label>
                        <button class="btn btn-sm btn-danger hapus-materi" type="button"><i class="fa fa-close"></i></button>
                    </div>
                </div>
            `);
        });

        $(document).on('click','.hapus-materi',function() {
            $(this).closest(".tambah").remove();
        });
        
        $(document).on('click','.add-line', function(){
            var i = $('.add-line').length;
            var htm = `<tr>
                            <td class="form-row">
                                <div class="form-group col-xs-12 field">
                                    <input type="hidden" name="key_daftar[]" value="`+i+`">
                                    <input type="text" name="nama_peserta[]" class="form-control" placeholder="Nama">
                                </div>
                            </td>
                            <td class="form-row">
                                <div class="form-group col-xs-12 field">
                                    <input type="text" name="jabatan[]" class="form-control" placeholder="Jabatan">
                                </div>
                            </td>
                            <td class=" text-center">
                                <a data-content="Ubah" class="add-line" data-id="1" class="edit button" data-toggle="tooltip" title="" data-original-title="Ubah" style="padding-right: 10px;">
                                    <i class="fa fa-plus text-info"></i>
                                </a>
                                <a data-content="Detil" class="remove-line" data-id="1" class="m-l detil button" data-toggle="tooltip" title="" data-original-title="Detil">
                                    <i class="fa fa-trash text-danger"></i>
                                </a>
                            </td>
                        </tr>`;
            $('.show-daftar').append(htm);
        });

        $(document).on('click','.remove-line', function(){
            $(this).parent().parent().remove();
        });
        
        $(document).on('click','.add-line-risalah', function(){
            var i = $('.add-line-risalah').length;
            var htm = `<tr>
                            <td class="form-row">
                                <div class="form-group col-xs-12 field">
                                    <input type="hidden" name="key_risalah[]" value="`+i+`">
                                    <textarea name="risalah[]" class="form-control" placeholder="Risalah"></textarea>
                                </div>
                            </td>
                            <td class="form-row">
                                <div class="form-group col-xs-12 field">
                                    <input type="text" name="pen_1[]" class="form-control" placeholder="Pen. Jawab">
                                </div>
                            </td>
                            <td class="form-row">
                                <div class="form-group col-xs-12 field">
                                    <textarea name="realisasi[]" class="form-control" placeholder="Realisasi"></textarea>
                                </div>
                            </td>
                            <td class="form-row">
                                <div class="form-group col-xs-12 field">
                                    <input type="text" name="pen_2[]" class="form-control" placeholder="Pen. Jawab">
                                </div>
                            </td>
                            <td class="form-row">
                                <div class="form-group col-xs-12 field">
                                    <textarea name="rencana[]" class="form-control" placeholder="Rencana yang akan datang"></textarea>
                                </div>
                            </td>
                            <td class="form-row">
                                <div class="form-group col-xs-12 field">
                                    <input type="text" name="pen_3[]" class="form-control" placeholder="Pen. Jawab">
                                </div>
                            </td>
                            <td class=" text-center">
                                <a data-content="Ubah" class="add-line" data-id="1" class="edit button" data-toggle="tooltip" title="" data-original-title="Ubah" style="padding-right: 10px;">
                                    <i class="fa fa-plus text-info"></i>
                                </a>
                                <a data-content="Detil" class="remove-line-risalah" data-id="1" class="m-l detil button" data-toggle="tooltip" title="" data-original-title="Detil">
                                    <i class="fa fa-trash text-danger"></i>
                                </a>
                            </td>
                        </tr>`;
            $('.show-risalah').append(htm);
        });

        $(document).on('click','.remove-line-risalah', function(){
            $(this).parent().parent().remove();
        });

        // peserta
        $(document).on('click', '.tambah_peserta_internal', function(e){
            var rowCount = $('#example > tbody > tr').length;
            var c = rowCount-1;
            var html = `
                    <tr class="data-container-`+(c+2)+`" data-id="`+(c+2)+`">
                            <td scope="row" style="text-align: center;">
                                <label class="numboor-`+(c+2)+`">`+(c+2)+`</label>
                            </td>
                            <td scope="row">
                                <div class="field">
                                    <select class="selectpicker form-control user" 
                                    name="detail[`+(c+2)+`][user_id]"
                                    data-style="btn-default" 
                                    data-live-search="true" 
                                    title="(Nama)"
                                    data-size="5"
                                    data-idx="`+c+2+`"
                                    >
                                    @foreach(App\Models\Auths\User::where('id','<>', 1)->where('tipe', 1)->whereNotIn('email', ['', '-'])->whereNotNull('email')->get() as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </td>
                            <td scope="row">
                                <input type="text" name="detail[`+(c+2)+`][email]" id="email`+c+2+`" class="form-control" placeholder="Email" readonly="">
                            </td>
                            <td scope="row">
                                <input type="text" name="detail[`+(c+2)+`][hp]" id="hp`+c+2+`" class="form-control" placeholder="No Hp" readonly="">
                            </td>
                            <td scope="row">
                                <input type="text" name="detail[`+(c+2)+`][jabatan]" id="jabatan`+c+2+`" class="form-control" placeholder="Jabatan" readonly="">
                            </td>
                            <td scope="row">
                                <input type="text" name="detail[`+(c+2)+`][bu_co]" id="bu_co`+c+2+`" class="form-control" placeholder="BU/CO" readonly="">
                            </td>
                            <td scope="row">
                                <select class="selectpicker form-control project"
                                    name="detail[`+(c+2)+`][project_id][]"
                                    data-style="btn-default" 
                                    data-live-search="true" 
                                    title="(Pilih Project)"
                                    data-size="5"
                                    data-idx="`+c+2+`"
                                    id="project`+c+2+`"
                                    multiple=""
                                    data-width="500px"
                                    >
                                </select>
                            </td>
                            <td style="text-align: center;">
                                <button class="btn btn-sm btn-danger hapus_peserta" type="button" data-id=`+(c+2)+`><i class="fa fa-remove"></i></button>
                            </td>
                        </tr>
                `;

                $('.container').append(html);
                $('.selectpicker').selectpicker();

                $('.user').on('change', function(){
                    // console.log('oe');
                    var idx = $(this).data('idx');
                    // alert(idx);
                    $.ajax({
                        url: '{{ url('ajax/option/get-user') }}',
                        type: 'POST',
                        data: {
                            _token: "{{ csrf_token() }}",
                            user_id: this.value
                        },
                    })
                    .done(function(response) {
                        // alert(response);
                        $('#email'+idx).val(response.email);
                        $('#hp'+idx).val(response.phone);
                        $('#jabatan'+idx).val(response.jabatan);
                        $('#bu_co'+idx).val(response.bu);
                        $('#project'+idx).html(response.project);
                        $('.user').selectpicker("refresh");
                        $('.project').selectpicker("refresh");
                    })
                    .fail(function() {
                        console.log("error");
                    });
                })

                $('.project').on('change', function(){
                    var idx = $(this).data('idx');
                    // alert(idx);
                    // clear selected
                    var data = $(this).val();
                
                    if(jQuery.inArray('0', data) !== -1){
                        $('#project'+idx).selectpicker('deselectAll');
                        $('#project'+idx).selectpicker('val', '0');
                    }

                    $.ajax({
                        url: '{{ url('ajax/option/get-project') }}',
                        type: 'POST',
                        data: {
                            _token: "{{ csrf_token() }}",
                            project_id: this.value
                        },
                    })
                    .done(function(response) {
                        $('#bu_co'+idx).val(response.bu);
                        $('.user').selectpicker("refresh");
                        $('.project').selectpicker("refresh");
                    })
                    .fail(function() {
                        console.log("error");
                    });
                })
        });

        $(document).on('click', '.tambah_peserta_eksternal', function(e){
            var rowCount = $('#example > tbody > tr').length;
            var c = rowCount-1;
            var html = `
                    <tr class="data-container-`+(c+2)+`" data-id="`+(c+2)+`">
                            <td scope="row" style="text-align: center;">
                                <label class="numboor-`+(c+2)+`">`+(c+2)+`</label>
                            </td>
                            <td scope="row">
                                <div class="field">
                                    <input type="text" name="detail[`+(c+2)+`][email]" class="form-control" placeholder="Email">
                                </div>
                            </td>
                            <td scope="row">
                                <div class="field">
                                    <input type="text" name="detail[`+(c+2)+`][perusahaan]" class="form-control" placeholder="Perusahaan">
                                </div>
                            </td>
                            <td style="text-align: center;">
                                <button class="btn btn-sm btn-danger hapus_peserta" type="button" data-id=`+(c+2)+`><i class="fa fa-remove"></i></button>
                            </td>
                        </tr>
                `;

                $('.container').append(html);

        });

        $(document).on('click', '.hapus_peserta', function (e){
            var row = $(this).closest('tr');
            row.remove();
            var table = $('#example');
            var rows = table.find('tbody tr');

            $.each(rows, function(key, value){
                console.log($(this).data("id"))
                table.find('.numboor-'+$(this).data("id")).html(key+1);
            });
        });

        $(document).on('click', '.tambah_materi', function(e){
            var rowCount = $('#example-materi > tbody > tr').length;
            var c = rowCount-1;
            var html = `
                    <tr class="data-container-materi-`+(c+2)+`" data-id="`+(c+2)+`">
                        <td scope="row">
                            <div class="field">
                                <label for="inputEmail4">&nbsp;</label>
                                <input type="text" name="data[`+(c+2)+`][agenda]" class="form-control" placeholder="Agenda">
                            </div>
                        </td>
                        <td style="text-align: center;">
                            <button class="btn btn-sm btn-danger hapus_materi" type="button" data-id=`+(c+2)+`><i class="fa fa-remove"></i></button>
                        </td>
                    </tr>
                `;

                $('.container-materi').append(html);
        });

        $(document).on('click', '.hapus_materi', function (e){
            var row = $(this).closest('tr');
            row.remove();
            var table = $('#example-materi');
            var rows = table.find('tbody tr');

            $.each(rows, function(key, value){
                console.log($(this).data("id"))
                table.find('.numboor-'+$(this).data("id")).html(key+1);
            });
        });

        $(document).on('click', '.save-as-draft.button', function(e){
            $('#formData').find('input[name="status"]').val("3");
            customSaveData('formData', function(resp){
                window.location = routes;
            });
        });

        $(document).on('click', '.save-as-submit.button', function(e){
            $('#formData').find('input[name="status"]').val("4");
            swal({
                title: "Apakah Anda yakin?",
                text: "Data yang sudah dikirim, tidak dapat diubah!",
                icon: "warning",
                buttons: ['Batal', 'OK'],
                reverseButtons: true
            }).then((result) => {
                if (result) {
                    customSaveData('formData', function(resp){
                        window.location = routes;
                    });
                }
            })
        });

        function customSaveData(formid, callback) 
        {
            $("#" + formid).ajaxSubmit({
                success: function(resp){
                    swal({
                        icon: "success",
                        title:'Berhasil!',
                        text:'Data berhasil disimpan.',
                        button: false,
                        timer: 2000,
                    });
                    window.location = '{{ route($routes.'.index') }}';
                },
                error: function(resp){
                    var response = resp.responseJSON;
                    if (response.status !== "undefined" && response.status === 'false'){
                        swal('Oops, Maaf!', resp.responseJSON.message, 'error');
                    }
                    $.each(response.errors, function(index, val) {
                        var name = index.split('.')
                                        .reduce((all, item) => {
                                            all += (index == 0 ? item : '[' + item + ']');
                                            return all;
                                        });
                        var fg = $('[name="'+ name +'"], [name="'+ name +'[]"]').closest('.form-group');
                        fg.addClass('has-error');
                        var field = $('[name="'+ name +'"], [name="'+ name +'[]"]').closest('.field');
                        field.addClass('has-error');
                        field.append('<div><small class="control-label error-label font-bold text-error">'+ val +'</small></div>');
                    });

                    var intrv = setInterval(function(){
                        $('.form-group .error-label').slideUp(500, function(e) {
                            $(this).remove();
                            $('.form-group.has-error').removeClass('has-error');
                            clearTimeout(intrv);
                        });
                        $('.field .error-label').slideUp(500, function(e) {
                            $(this).remove();
                            $('.field.has-error').removeClass('has-error');
                            clearTimeout(intrv);
                        });
                    }, 3000)
                }
            });
        }
    </script>
    @yield('js-extra')
@endpush
