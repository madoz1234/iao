@extends('layouts.base')
@section('title', 'Risalah Rapat')
@include('libs.actions')
@section('side-header')
   <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color: transparent !important;">
        <?php $i=1; $last=count($breadcrumb);?>
         @foreach ($breadcrumb as $name => $link)
            @if($i++ != $last)
                <li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
         @endforeach
      </ol>
    </nav>
@endsection

@section('body')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="form-row">
                {{-- kiri --}}
                <div class="form-group col-xs-6" style="padding-right: 14px;">
                    <table class="table-header" style="width: 100%; font-size: 12px;">
                        <tbody>
                            <tr>
                                <td class="table-label">Tahun</td>
                                <td class="titik-dua">:</td>
                                <td>{{ $record->rkia->rencanaaudit->tahun }}</td>
                            </tr>
                            <tr>
                                <td class="table-label">Rencana Pelaksanaan</td>
                                <td class="titik-dua">:</td>
                                <td>{{ $record->rkia->rencana }}</td>
                            </tr>
                            <tr>
                                <td class="table-label">Tipe</td>
                                <td class="titik-dua">:</td>
                                <td>Kegiatan Konsultasi</td>
                            </tr>
                            <tr>
                                <td class="table-label">Kategori</td>
                                <td class="titik-dua">:</td>
                                <td>{{ $record->rkia->konsultasi->nama }}</td>
                            </tr>
                            <tr>
                                <td class="table-label">Jenis</td>
                                <td class="titik-dua">:</td>
                                <td>{{ $record->jenis }}</td>
                            </tr>
                            <tr>
                                <td class="table-label">Pimpinan Rapat</td>
                                <td class="titik-dua">:</td>
                                <td>{{ $record->pimpinan->name }}</td>
                            </tr>
                            <tr>
                                <td class="table-label">Notulen Rapat</td>
                                <td class="titik-dua">:</td>
                                <td>{{ $record->notulen->name }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                {{-- kanan --}}
                <div class="form-group col-xs-6" style="padding-left: 14px;">
                    <table class="table-header" style="width: 100%; font-size: 12px;">
                        <tbody>
                            <tr>
                                <td class="table-label">Nomor Rapat</td>
                                <td class="titik-dua">:</td>
                                <td>{{ $record->rapat->nomor }}</td>
                            </tr>
                            <tr>
                                <td class="table-label">Tempat</td>
                                <td class="titik-dua">:</td>
                                <td>{{ $record->rapat->tempat }}</td>
                            </tr>
                            <tr>
                                <td class="table-label">Hari</td>
                                <td class="titik-dua">:</td>
                                <td>{{ App\Libraries\CoreSn::Day($record->rapat->tanggal) }}</td>
                            </tr>
                            <tr>
                                <td class="table-label">Tanggal</td>
                                <td class="titik-dua">:</td>
                                <td>{{ Carbon::parse($record->rapat->tanggal)->format('d-m-Y') }}</td>
                            </tr>
                            <tr>
                                <td class="table-label">Jam</td>
                                <td class="titik-dua">:</td>
                                <td>{{ $record->rapat->jam_mulai.' s.d '.$record->rapat->jam_selesai }}</td>
                            </tr>
                            <tr>
                                <td class="table-label">Agenda</td>
                                <td class="titik-dua">:</td>
                                <td>
                                    @foreach($record->rapat->agendaInternal as $key => $data)
                                    {!! $key+1 .'. '. $data->agenda !!} <br>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td class="table-label">Keterangan Rapat</td>
                                <td class="titik-dua">:</td>
                                <td>{{ $record->rapat->keterangan }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel-body" style="padding-top: 0px;padding-bottom: 0px;">
            <div class="table-responsive">
                @if(isset($structs['listStruct4']))
                <table id="dataTable" class="table table-bordered m-t-none" style="width: 100%">
                    <thead>
                        <tr>
                            @foreach ($structs['listStruct4'] as $struct)
                                <th class="text-center v-middle">{{ $struct['label'] }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @yield('tableBody')
                    </tbody>
                </table>
                @endif
            </div>
            <div class="form-row">
                <div class="form-group col-xs-12">
                    <br>
                    <div class="text-right">
                        <a href="{{ route($routes.'.index') }}" class="btn btn-cancel back">Kembali</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}">
@endpush

@push('js')
    <script type="text/javascript" src="{{ asset('libs/jquery/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.js') }}"></script>
@endpush

@push('styles')
    <style>
        .panel .dataTables_wrapper{
            padding-top: 0 !important;
        }
        table.dataTable{
            margin-top: 0 !important;
        }
        table.dataTable tr > td{
            vertical-align: middle;
        }
        .table-header tr td {
            vertical-align: top;
        }
    </style>
@endpush

@push('scripts')
    <script>
        var dt = null;
        
        $(function() {
            dt = $('#dataTable').DataTable({
                "scrollX": true,
                lengthChange: false,
                "autoWidth": false,
                filter: false,
                processing: true,
                serverSide: true,
                responsive: true,
                paginate  :false,
                sorting: [],
                language: {
                    url: "{{ asset('libs/jquery/datatables/media/Indonesian.json') }}"
                },
                ajax: {
                    url: '{!! route($routes.'.gridRisalah', $record->id) !!}',
                    method: 'POST',
                    data: function (d) {
                        d._token = "{{ csrf_token() }}";
                        $('#dataFilters .filter-control').each(function(idx, el) {
                            var name = $(el).data('post');
                            var val = $(el).val();
                            d[name] = val;
                        })
                    },
                    error: function (responseError, status) {
                        console.log(responseError);
                        return false;
                    }
                },
                columns: {!! json_encode($structs['listStruct4']) !!},
                drawCallback: function() {
                    onShow();
                    readMoreItem('list-more1');
                    readMoreItem('list-more2');
                    var api = this.api();

                    api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i, x, y) {
                        cell.innerHTML = parseInt(cell.innerHTML)+i+1;
                    });
                    
                    $('[data-toggle=tooltip]').tooltip()
                    $('[data-toggle=popover]').popover({
                        trigger: 'hover',
                        placement: 'top',
                        template: '<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-content"></div></div>'
                    });
                }
            });

            $('select[name="filter[page]"]').on('change', function(e) {
                var length = this.value;
                length = (length != '') ? length : 10;
                dt.page.len(length).draw();
                e.preventDefault();
            });

            $('.filter.button').on('click', function(e) {
                dt.draw();
                e.preventDefault();
            });

            $('.filter-control').on('change, keyup', function(e) {
                dt.draw();
                e.preventDefault();
            });

            $('.tahun').datepicker({
                format: 'yyyy',
                // startDate: '-0d',
                orientation: "auto",
                autoclose:true,
            }).on('changeDate', function(e){
                dt.draw();
                e.preventDefault();
            });

            $('.tanggal').datepicker({
                format: 'dd/mm/yyyy',
                // startDate: '-0d',
                orientation: "auto",
                autoclose:true,
            }).on('changeDate', function(e){
                dt.draw();
                e.preventDefault();
            });

            $('.filter-control').on('changed.bs.select', function(e) {
                dt.draw();
                e.preventDefault();
            });

            $('.reset.button').on('click', function(e) {
                $('.ui.dropdown').dropdown('clear');
                setTimeout(function() {
                    dt.draw();
                }, 200);
            });
        });
    </script>
    <script>
        var routes = '{{ route($routes.'.index') }}';
        var current_url = '{{ url()->current() }}';
        var modal = '#mediumModal';
        var onShow = function(){
            $("#pdf").fileinput({
                language: 'es',
                uploadUrl: "/file-upload-batch/2",
                previewFileType: "pdf",
                showUpload: false,
                previewFileIcon: '<i class="fas fa-file"></i>',
                previewFileIconSettings: {
                    'docx': '<i class="fas fa-file-word text-primary"></i>',
                    'xlsx': '<i class="fas fa-file-excel text-success"></i>',
                    'pptx': '<i class="fas fa-file-powerpoint text-danger"></i>',
                    'pdf': '<i class="fas fa-file-pdf text-danger"></i>',
                    'zip': '<i class="fas fa-file-archive text-muted"></i>',
                },
                fileActionSettings: {
                    showRemove: true,
                    showUpload: false, //This remove the upload button
                    showZoom: true,
                    showDrag: true
                },
                initialPreviewAsData: true,
                purifyHtml: true,
                allowedFileTypes: ['pdf'],
                allowedFileExtensions: ['pdf'],
                // allowed image size up to 5 MB
                maxFileSize: 5000,
                previewFileExtSettings: {
                    'doc': function(ext) {
                        return ext.match(/(doc|docx)$/i);
                    },
                    'xls': function(ext) {
                        return ext.match(/(xls|xlsx)$/i);
                    },
                    'ppt': function(ext) {
                        return ext.match(/(ppt|pptx)$/i);
                    }
                }
            });
        };

        $(document).on('click', '.update-status.button', function(e){
            var me = $(this);
            var url = routes + '/' + $(this).data('id') + '/update-risalah';
            var status = $(this).data('status');
            var risalah_id = $(this).data('risalah');
            swal({
                    title: 'Apakah Anda yakin?',
                    text: 'Data yang sudah dikirim, tidak dapat diubah!',
                    icon: 'warning',
                    buttons: ['Batal', 'OK'],
                    reverseButtons: true
            }).then((result) => {
                if (result) {
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {
                            '_token'    : '{{ csrf_token() }}',
                            'status'    : status,
                            'risalah_id': risalah_id,
                        }
                    })
                    .done(function(response) {
                        swal({
                            type   : 'success',
                            title  : 'Tersimpan!',
                            text   : 'Status berhasil diubah.',
                            icon   : 'success',
                            buttons: false,
                            timer  : 1500
                        });
                        dt = $('#dataTable').DataTable();
                        dt.draw();
                        onShow();
                    })
                    .fail(function(response) {
                        swal('Status gagal diubah!', {
                            icon: 'error',
                        })
                    })
                }
            })
        });

        $(document).on('click', '.upload-file.button', function(e){
            var risalah_id = $(this).data('risalah_id');
            var url = routes + '/' + $(this).data('id') + '/' + risalah_id + '/uploadFile';
            loadModal({
                url: url,
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                onShow();
            });
        });

        $(document).on('click', '.download-file.button', function(e){
            var url = routes + '/' + $(this).data('id') + '/' + $(this).data('risalah_id') + '/downloadFile';
            window.location = url;
        });

        $(document).on('click', '.save-as-upload.button', function(e){
            $('#formData').find('input[name="status"]').val("3");
            customSaveData('formData', function(resp){
                dt = $('#dataTable').DataTable();
                dt.draw();
                onShow();
            });
        });

        function customSaveData(formid, callback) 
        {
            // show loading
            $('#' + formid).find('.loading.dimmer').show();
            // begin submit
            $("#" + formid).ajaxSubmit({
                success: function(resp){
                    swal({
                        icon: "success",
                        title:'Berhasil!',
                        text:'Data berhasil disimpan.',
                        button: false,
                        timer: 1000,
                    }).then((result) => { 
                        $('#' + formid).find('.loading.dimmer').hide();
                        $(modal).modal('hide');
                        callback();
                        // dt1 = $('#dataTable1').DataTable();
                        // dt1.draw();
                        // dt2 = $('#dataTable2').DataTable();
                        // dt2.draw();
                        // dt3 = $('#dataTable3').DataTable();
                        // dt3.draw();
                    })
                },
                error: function(resp){
                    $('#' + formid).find('.loading.dimmer').hide();
                    var response = resp.responseJSON;
                    if (response.status !== "undefined" && response.status === 'false'){
                        swal('Oops, Maaf!', resp.responseJSON.message, 'error');
                    }
                    $.each(response.errors, function(index, val) {
                        var name = index.split('.')
                                        .reduce((all, item) => {
                                            all += (index == 0 ? item : '[' + item + ']');
                                            return all;
                                        });
                        var fg = $('[name="'+ name +'"], [name="'+ name +'[]"]').closest('.form-group');
                        fg.addClass('has-error');
                        var field = $('[name="'+ name +'"], [name="'+ name +'[]"]').closest('.field');
                        field.append('<div><small class="control-label error-label font-bold">'+ val +'</small></div>');
                    });

                    var intrv = setInterval(function(){
                        $('.form-group .error-label').slideUp(500, function(e) {
                            $(this).remove();
                            $('.form-group.has-error').removeClass('has-error');
                            clearTimeout(intrv);
                        });
                    }, 3000)
                }
            });
        }

        function showFiles(dataPreview, dataPreviewConfig, dataCaption) {
            $(this).fileinput({
                autoReplace: true,
                overwriteInitial: true,
                initialPreview: JSON.parse(dataPreview.replace(/&quot;/g,'"')),
                initialPreviewAsData: true,
                initialPreviewConfig: JSON.parse(dataPreviewConfig.replace(/&quot;/g,'"')),
                initialCaption: dataCaption,
                initialPreviewShowDelete: false,
                showRemove: false,
                showClose: false,
                layoutTemplates: {actionDelete: ''}, // disable thumbnail deletion
            });
        }

        $(function() {
            console.log( "ready!" );
            onShow();
            
        })
    </script>
@endpush