<form action="{{ route($routes.'.store') }}" method="POST" id="formData" autocomplete="off">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Tambah Data Kegiatan Konsultasi</h5>
    </div>
    <div class="modal-body">
        <input type="hidden" name="status" value="1">
        <input type="hidden" name="ket_svp">
        <input type="hidden" name="rencana_detail_id" value="{{ $record->id }}">
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <tbody>
                        <tr>
                            <td class="border-none wd-label">Tahun</td>
                            <td class="border-none wd-5 px-0">:</td>
                            <td class="border-none">{{ $record->rencanaaudit->tahun }}</td>
                        </tr>
                        <tr>
                            <td class="border-none wd-label">Rencana Pelaksanaan</td>
                            <td class="border-none wd-5 px-0">:</td>
                            <td class="border-none">{{ $record->rencana }}</td>
                        </tr>
                        <tr>
                            <td class="border-none wd-label">Tipe</td>
                            <td class="border-none wd-5 px-0">:</td>
                            <td class="border-none"><span class="label label-success">Kegiatan Konsultasi</span></td>
                        </tr>
                        <tr>
                            <td class="border-none wd-label">Kategori</td>
                            <td class="border-none wd-5 px-0">:</td>
                            <td class="border-none">{{ $record->kategori }}</td>
                        </tr>
                        <tr class="form-group">
                            <td class="border-none wd-label control-label">Jenis</td>
                            <td class="border-none wd-5 px-0 control-label">:</td>
                            <td class="border-none field">
                                <select name="jenis_rapat" id="jenis_rapat" class="form-control select selectpicker" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
                                    <option value="1">Internal</option>
                                    <option value="2">Eksternal</option>
                                </select>
                            </td>
                        </tr>
                        <tr class="form-group">
                            <td class="border-none wd-label control-label">Pimpinan Rapat</td>
                            <td class="border-none wd-5 px-0 control-label">:</td>
                            <td class="border-none field">
                                <select name="pimpinan_rapat" id="pimpinan_rapat" class="form-control select selectpicker" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr class="form-group">
                            <td class="border-none wd-label control-label">Notulen</td>
                            <td class="border-none wd-5 px-0 control-label">:</td>
                            <td class="border-none field">
                                <select name="notulen_rapat" id="notulen_rapat" class="form-control select selectpicker" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr class="form-group">
                            <td class="border-none wd-label control-label">Tanggal Pelaksanaan</td>
                            <td class="border-none wd-5 px-0 control-label">:</td>
                            <td class="border-none field">
                                <input name="tanggal_rapat" type="text" class="form-control tanggal py-0">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-primary save-as-draft button">Save As Draft</button>
        <button type="button" class="btn btn-simpan save-as-submit button">Submit</button>
    </div>
    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>