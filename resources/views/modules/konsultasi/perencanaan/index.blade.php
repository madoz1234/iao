@extends('layouts.base')
@section('title', 'Perencanaan Kegiatan Konsultasi')
@section('side-header')
   <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color: transparent !important;">
        <?php $i=1; $last=count($breadcrumb);?>
         @foreach ($breadcrumb as $name => $link)
            @if($i++ != $last)
                <li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
         @endforeach
      </ol>
    </nav>
@endsection
@section('body')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-8" style="left: 17px;">
                    <form id="dataFilters" class="form-inline" role="form">
                        <div class="form-group">
                            <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
                            <label class="control-label sr-only" for="filter-tahun">Tahun</label>
                            <input type="text" class="form-control filter-control tahun" name="filter[tahun]" data-post="tahun" placeholder="Tahun">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="panel-body py-0">
            <ul class="nav nav-tabs" id="myTab" role="tablist" style="margin-bottom: 7px;">
                <li class="nav-item tab-a">
                    <a class="nav-link active" id="a-tab" data-toggle="tab" href="#a" role="tab" aria-selected="true">
                        Baru
                        @if ($baru > 0 && (auth()->user()->hasRole(['auditor','SVP - Internal Audit','President Director','Secretary']) || auth()->user()->isUserIa()))
                        &nbsp;&nbsp;<span class="badge badge-sm down bg-danger pull-right-xs">{{ $baru }}</span>
                        @endif
                    </a>
                </li>
                <li class="nav-item tab-b">
                    <a class="nav-link" id="b-tab" data-toggle="tab" href="#b" role="tab" aria-selected="false">
                        On Progress
                        @if ($onProgress > 0 && (auth()->user()->hasRole(['auditor','SVP - Internal Audit','President Director','Secretary']) || auth()->user()->isUserIa()))
                        &nbsp;&nbsp;<span class="badge badge-sm down bg-danger pull-right-xs">{{ $onProgress }}</span>
                        @endif
                    </a>
                </li>
                <li class="nav-item tab-c">
                    <a class="nav-link" id="c-tab" data-toggle="tab" href="#c" role="tab" aria-selected="false">Historis</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="a" role="tabpanel" aria-labelledby="a-tab">
                    <div class="table-responsive">
                        @if(isset($structs['listStruct']))
                        <table id="dataTable1" class="table table-bordered m-t-none" style="width: 100%">
                            <thead>
                                <tr>
                                    @foreach ($structs['listStruct'] as $struct)
                                    <th class="text-center v-middle">{{ $struct['label'] }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @yield('tableBody')
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
                <div class="tab-pane" id="b" role="tabpanel" aria-labelledby="b-tab">
                    <div class="table-responsive">
                        @if(isset($structs['listStruct2']))
                        <table id="dataTable2" class="table table-bordered m-t-none" style="width: 100%">
                            <thead>
                                <tr>
                                    @foreach ($structs['listStruct2'] as $struct)
                                    <th class="text-center v-middle">{{ $struct['label'] }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @yield('tableBody')
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
                <div class="tab-pane" id="c" role="tabpanel" aria-labelledby="c-tab">
                    <div class="table-responsive">
                        @if(isset($structs['listStruct3']))
                        <table id="dataTable3" class="table table-bordered m-t-none" style="width: 100%">
                            <thead>
                                <tr>
                                    @foreach ($structs['listStruct3'] as $struct)
                                    <th class="text-center v-middle">{{ $struct['label'] }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @yield('tableBody')
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('libs.datatable-multigrid')
@include('libs.actions')
@push('styles')
    <style>
        .wd-5 { width: 5px!important; }
        .wd-label { width: 170px!important; }
        .border-none { border: none!important; }
        .v-middle { vertical-align: middle!important; }
        .p-0 { padding: 0!important; }
        .py-0 { 
            padding-top: 0!important; 
            padding-bottom: 0!important; 
        }
        .py-8 { 
            padding-top: 8!important; 
            padding-bottom: 8!important; 
        }
        .px-0 { 
            padding-left: 0!important; 
            padding-right: 0!important; 
        }
        .px-15 { 
            padding-left: 15!important; 
            padding-right: 15!important; 
        }
        .text-red { color: red; }
        .hr-border { border-bottom: 1px solid #e5e5e5; }
    </style>
@endpush
@push('scripts')
    <script>
        var routes = "{!! route($routes.'.index') !!}";
        var modal = "#mediumModal";
        var onShow = function(){
            $('.tahun').datepicker({
                format: "yyyy",
                viewMode: "years", 
                minViewMode: "years",
                orientation: "auto",
                autoclose:true
            });
            $('.tanggal').datepicker({
                format: 'dd-mm-yyyy',
                orientation: "bottom",
                autoclose:true,
            });
            $('.selectpicker').selectpicker();
        };
        onShow();

        $('#myTab li:first-child a').tab('show');

        $(document).on('click', '.tab-a', function (e){
            $('.add.button').show();
            dt1 = $('#dataTable1').DataTable();
            dt1.draw();
        }); 
        
        $(document).on('click', '.tab-b', function (e){
            $('.add.button').hide();
            dt2 = $('#dataTable2').DataTable();
            dt2.draw();
        }); 
        
        $(document).on('click', '.tab-c', function (e){
            $('.add.button').hide();
            dt3 = $('#dataTable3').DataTable();
            dt3.draw();
        });

        $(document).on('click', '.create.button', function(e){
            var url = routes + '/' + $(this).data('id') + '/buat';
            loadModal({
                url: url,
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                onShow();
            });
        });

        $(document).on('click', '.edit.button', function(e){
            var url = routes + '/' + $(this).data('id') + '/edit';
            loadModal({
                url: url,
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                onShow();
            });
        });
        
        $(document).on('click', '.detil.button', function(e){
            var url = routes + '/' + $(this).data('id');
            loadModal({
                url: url,
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                onShow();
            });
        });

        $(document).on('click', '.upload-dokumen.button', function(e){
            var idx = $(this).data('id');
            var url = routes + '/' + $(this).data('id') + '/uploadFile';
            loadModal({
                url: url,
                modal: modal,
            }, function(resp){
                $(modal).find('.loading.dimmer').hide();
                $("#pdf").fileinput({
                    language: 'es',
                    uploadUrl: "/file-upload-batch/2",
                    previewFileType: "pdf",
                    showUpload: false,
                    previewFileIcon: '<i class="fas fa-file"></i>',
                    previewFileIconSettings: {
                        'docx': '<i class="fas fa-file-word text-primary"></i>',
                        'xlsx': '<i class="fas fa-file-excel text-success"></i>',
                        'pptx': '<i class="fas fa-file-powerpoint text-danger"></i>',
                        'pdf': '<i class="fas fa-file-pdf text-danger"></i>',
                        'zip': '<i class="fas fa-file-archive text-muted"></i>',
                    },
                    fileActionSettings: {
                        showRemove: true,
                        showUpload: false, //This remove the upload button
                        showZoom: true,
                        showDrag: true
                    },
                    initialPreviewAsData: true,
                    purifyHtml: true,
                    allowedFileTypes: ['pdf'],
                    allowedFileExtensions: ['pdf'],
                    // allowed image size up to 5 MB
                    maxFileSize: 5000,
                    previewFileExtSettings: {
                        'doc': function(ext) {
                            return ext.match(/(doc|docx)$/i);
                        },
                        'xls': function(ext) {
                            return ext.match(/(xls|xlsx)$/i);
                        },
                        'ppt': function(ext) {
                            return ext.match(/(ppt|pptx)$/i);
                        }
                    }
                });
                onShow();
            });
        });

        $(document).on('click', '.cetak.button', function(e){
            var url = routes + '/' + $(this).data('id') + '/cetak';
            window.open(url, '_blank');
        });

        $(document).on('click', '.download-dokumen.button', function(e){
            var url = routes + '/' + $(this).data('id') + '/download-dokumen';
            window.location = url;
        });

        $(document).on('click', '.save-as-draft.button', function(e){
            $('#formData').find('input[name="status"]').val("1");
            customSaveData('formData', function(resp){
                window.location = routes;
            });
        });

        $(document).on('click', '.save-as-upload.button', function(e){
            $('#formData').find('input[name="status"]').val("3");
            customSaveData('formData', function(resp){
                window.location = routes;
            });
        });

        $(document).on('click', '.save-as-submit.button', function(e){
            $('#formData').find('input[name="status"]').val("2");
            var formDom = "formData";
            if($(this).data("form") !== undefined){
                formDom = $(this).data('form');
            }
            swal({
                    title: "Apakah Anda yakin?",
                    text: "Data yang sudah dikirim, tidak dapat diubah!",
                    icon: "warning",
                    buttons: ['Batal', 'OK'],
                    reverseButtons: true
            }).then((result) => {
                if (result) {
                    customSaveData('formData', function(resp){
                        window.location = routes;
                    });
                }
            })
        });

        $(document).on('click', '.save-as-reject.button', function(e){
            $('#formData').find('input[name="status"]').val("1");
            var formDom = "formData";
            if($(this).data("form") !== undefined){
                formDom = $(this).data('form');
            }
            swal({
                    title: "Apakah Anda yakin?",
                    text: "Data yang sudah dikirim, tidak dapat diubah!",
                    icon: "warning",
                    buttons: ['Batal', 'OK'],
                    reverseButtons: true
            }).then((result) => {
                if (result) {
                    customSaveData('formData', function(resp){
                        window.location = routes;
                    });
                }
            })
        });

        $(document).on('click', '.save-as-approve.button', function(e){
            swal({
                title: "Apakah Anda yakin?",
                text: "Data yang sudah disetujui, tidak dapat diubah!",
                icon: "warning",
                buttons: ['Reject', 'Approve'],
                reverseButtons: true,
            }).then((result) => {
                if(result){
                    var url = routes + '/' + $(this).data('id') + '/approve';
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {
                            '_token' : '{{ csrf_token() }}',
                            'status' : 3,
                            'approve' : true,
                        }
                    })
                    .done(function(response) {
                        swal({
                            icon: 'success',
                            title:'Berhasil!',
                            text:'Data berhasil disetujui!',
                            button: false,
                            timer: 2000,
                        }).then((res) => {
                            window.location = routes;
                        })
                    })
                    .fail(function(response) {
                        swal('Data gagal disetujui!', {
                            icon: 'error',
                        }).then((res) => {
                            window.location = routes;
                        })
                    })
                }else{
                    var url = routes + '/' + $(this).data('id') + '/reject';
                    loadModal({
                        url: url,
                        modal: modal,
                    }, function(resp){
                        $(modal).find('.loading.dimmer').hide();
                        onShow();
                    });
                }
            });
        });

        function customSaveData(formid, callback) 
        {
            // show loading
            $('#' + formid).find('.loading.dimmer').show();
            // begin submit
            $("#" + formid).ajaxSubmit({
                success: function(resp){
                    swal({
                        icon: "success",
                        title:'Berhasil!',
                        text:'Data berhasil disimpan.',
                        button: false,
                        timer: 2000,
                    }).then((result) => { 
                        $('#' + formid).find('.loading.dimmer').hide();
                        $(modal).modal('hide');
                        callback();
                        dt1 = $('#dataTable1').DataTable();
                        dt1.draw();
                        dt2 = $('#dataTable2').DataTable();
                        dt2.draw();
                        dt3 = $('#dataTable3').DataTable();
                        dt3.draw();
                    })
                },
                error: function(resp){
                    $('#' + formid).find('.loading.dimmer').hide();
                    var response = resp.responseJSON;
                    if (response.status !== "undefined" && response.status === 'false'){
                        swal('Oops, Maaf!', resp.responseJSON.message, 'error');
                    }
                    $.each(response.errors, function(index, val) {
                        var name = index.split('.')
                                        .reduce((all, item) => {
                                            all += (index == 0 ? item : '[' + item + ']');
                                            return all;
                                        });
                        var fg = $('[name="'+ name +'"], [name="'+ name +'[]"]').closest('.form-group');
                        fg.addClass('has-error');
                        var field = $('[name="'+ name +'"], [name="'+ name +'[]"]').closest('.field');
                        field.append('<div><small class="control-label error-label font-bold">'+ val +'</small></div>');
                    });

                    var intrv = setInterval(function(){
                        $('.form-group .error-label').slideUp(500, function(e) {
                            $(this).remove();
                            $('.form-group.has-error').removeClass('has-error');
                            clearTimeout(intrv);
                        });
                    }, 3000)
                }
            });
        }
    </script>
@endpush