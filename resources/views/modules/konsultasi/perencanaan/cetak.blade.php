<html>
<head>
<style>
    @font-face {
        font-family: 'tahoma';
        src: url({{ storage_path('fonts\tahoma.ttf') }}) format("truetype");
        font-weight: 400;
        font-style: normal;
    }

    @page {
        margin: 25px;
        counter-reset: page;
        font-family: 'tahoma'!important;
    }

    /* Define now the real margins of every page in the PDF */
    body {
        margin-top: 3cm;
        font-style: 11px;
        font-family: 'tahoma'!important;
    }

    .table {
        width: 100%;
    }

    .table.bordered {
        border: 1px solid black;
        border-collapse: collapse;
    }

    .table.bordered th,
    .table.bordered td{
        border: 1px solid black;
        border-collapse: collapse;
        padding:5px;
        vertical-align: top;
    }

    .table.bordered th{
        text-align: center;
        vertical-align: middle;
    }

    .break-inside-auto {
        page-break-inside: auto;
    }

    .wd-5 { width: 5px!important; }
    .wd-label { width: 170px!important; }
    .border-none { border: none!important; }
    .v-middle { vertical-align: middle!important; }
    .p-0 { padding: 0!important; }
    .py-0 { 
        padding-top: 0!important; 
        padding-bottom: 0!important; 
    }
    .py-8 { 
        padding-top: 8!important; 
        padding-bottom: 8!important; 
    }
    .px-0 { 
        padding-left: 0!important; 
        padding-right: 0!important; 
    }
    .px-15 { 
        padding-left: 15!important; 
        padding-right: 15!important; 
    }

    h1,
    h2,
    h3 {
        font-family: 'tahoma'!important;
    }

    header {
        position: fixed;
        top: 0px;
        left: 0px;
        right: 0px;
        height: 10px;
        text-align: center;
        font-style: 11px;
        font-family: 'tahoma'!important;
    }
        
    main {
        position: sticky;
        padding-top : -1cm;
        margin-top : 2px;
        border : none;
        width: 100%;
        font-style: 11px;
        font-family: 'tahoma'!important;
    }

    footer {
        position: fixed;
        bottom: -60px;
        left: 0px;
        right: 0px;
        height: 50px;
        text-align: center;
        line-height: 35px;
        font-style: 11px;
        font-family: 'tahoma'!important;
    }
</style>
</head>
<body>
    <header>
        <table class="table">
            <tbody>
                <tr>
                    <td style="width: 100px;padding-left: -10px;"><img src="{{ asset('src/img/logo.png') }}" width="95px" height="80px"></td>
                    <td style="text-align: center;">
                        <h2>KEGIATAN KONSULTASI</h2>
                        <h3 style="margin-top:-17px;">PT. WASKITA KARYA (PERSERO) TBK.</h3>
                    </td>
                </tr>
            </tbody>
        </table>
    </header>
    <main style="margin-top: 50px;">
        <table class="table break-inside-auto">
            <tbody>
                <tr>
                    <td class="wd-label">Tahun</td>
                    <td class="wd-5 px-0">:</td>
                    <td class="">{{ $record->rkia->rencanaaudit->tahun }}</td>
                </tr>
                <tr>
                    <td class="wd-label">Rencana Pelaksanaan</td>
                    <td class="wd-5 px-0">:</td>
                    <td class="">{{ $record->rkia->rencana }}</td>
                </tr>
                <tr>
                    <td class="wd-label">Tipe</td>
                    <td class="wd-5 px-0">:</td>
                    <td class=""><span class="label label-success">Kegiatan Konsultasi</span></td>
                </tr>
                <tr>
                    <td class="wd-label">Kategori</td>
                    <td class="wd-5 px-0">:</td>
                    <td class="">{{ $record->rkia->kategori }}</td>
                </tr>
                <tr class="form-group">
                    <td class="wd-label">Jenis</td>
                    <td class="wd-5 px-0">:</td>
                    <td>
                        @if($record->jenis)
                            {{ $record->jenis }}
                        @else
                            {!! '<p style="color: red;">*Belum dipilih</p>' !!}
                        @endif
                    </td>
                </tr>
                <tr class="form-group">
                    <td class="wd-label">Pimpinan Rapat</td>
                    <td class="wd-5 px-0">:</td>
                    <td>
                        @if($record->pimpinan)
                            {{ $record->pimpinan->name }}
                        @else
                            {!! '<p style="color: red;">*Belum dipilih</p>' !!}
                        @endif
                    </td>
                </tr>
                <tr class="form-group">
                    <td class="wd-label">Notulen</td>
                    <td class="wd-5 px-0">:</td>
                    <td>
                        @if($record->notulen)
                            {{ $record->notulen->name }}
                        @else
                            {!! '<p style="color: red;">*Belum dipilih</p>' !!}
                        @endif
                    </td>
                </tr>
                <tr class="form-group">
                    <td class="wd-label">Tanggal Pelaksanaan</td>
                    <td class="wd-5 px-0">:</td>
                    <td>
                        @if($record->tanggal_rapat)
                            {{ $record->tanggal }}
                        @else
                            {!! '<p style="color: red;">*Belum dipilih</p>' !!}
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
    </main>
</body>
</html>