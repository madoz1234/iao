@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}">
@endpush

@push('js')
    <script type="text/javascript" src="{{ asset('libs/jquery/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.js') }}"></script>
@endpush

@push('styles')
    <style>
        .panel .dataTables_wrapper{
            padding-top: 0 !important;
        }
        table.dataTable{
            margin-top: 0 !important;
        }
        table.dataTable tr > td{
            vertical-align: middle;
        }
    </style>
@endpush

@push('scripts')
    <script>
        var dt = 'null';
        var dt2 = 'null';
        var dt3 = 'null';
        
        $(function() {
            dt = $('#dataTable1').DataTable({
                lengthChange: false,
                filter: false,
                processing: true,
                serverSide: true,
                sorting: [],
                language: {
					url: "{{ asset('libs/jquery/datatables/media/Indonesian.json') }}"
				},
                ajax: {
                    url: '{!! route($routes.'.grid') !!}',
                    method: 'POST',
                    data: function (d) {
                        d._token = "{{ csrf_token() }}";
                        $('#dataFilters .filter-control').each(function(idx, el) {
                            var name = $(el).data('post');
                            var val = $(el).val();
                            d[name] = val;
                        })
                    },
                    error: function (xhr, error, code)
		            {
		                console.log(xhr);
		                console.log(code);
		            }
                },
                columns: {!! json_encode($structs['listStruct']) !!},
                drawCallback: function() {
                	readMoreItem('list-more1');
                	readMoreItem('list-more2');
                	readMoreItem('list-more3');
                    var api = this.api();

                    api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i, x, y) {
                        cell.innerHTML = parseInt(cell.innerHTML)+i+1;
                    });
                    $("[data-toggle='toggle']").bootstrapToggle('destroy')                 
				    $("[data-toggle='toggle']").bootstrapToggle();
                    
                    $('[data-toggle=tooltip]').tooltip()
                    $('[data-toggle=popover]').popover({
                        trigger: 'hover',
                        placement: 'top',
                        template: '<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-content"></div></div>'
                    });
                    $("[data-toggle='toggle']").bootstrapToggle('destroy')                 
                    $("[data-toggle='toggle']").bootstrapToggle();
                    var cek = api.page.info().recordsTotal;
		            if(cek > 0){
						$('.for-isi1').show();
						$('.for-isi1').text(cek);
		            }else{
		            	$('.for-isi1').hide();
		            }
                }
            });

            dt2 = $('#dataTable2').DataTable({
                lengthChange: false,
                filter: false,
                processing: true,
                serverSide: true,
                sorting: [],
                language: {
					url: "{{ asset('libs/jquery/datatables/media/Indonesian.json') }}"
				},
                ajax: {
                    url: '{!! route($routes.'.onProgress') !!}',
                    method: 'POST',
                    data: function (d) {
                        d._token = "{{ csrf_token() }}";
                        $('#dataFilters .filter-control').each(function(idx, el) {
                            var name = $(el).data('post');
                            var val = $(el).val();
                            d[name] = val;
                        })
                    },
                    error: function (responseError, status) {
		                console.log(responseError);
		                return false;
		            }
                },
                columns: {!! json_encode($structs['listStruct2']) !!},
                drawCallback: function() {
                	readMoreItem('list-more2');
                	readMoreItem('list-more4');
                    var api = this.api();

                    api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i, x, y) {
                        cell.innerHTML = parseInt(cell.innerHTML)+i+1;
                    });
                    
                    $('[data-toggle=tooltip]').tooltip()
                    $('[data-toggle=popover]').popover({
                        trigger: 'hover',
                        placement: 'top',
                        template: '<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-content"></div></div>'
                    });
                    $("[data-toggle='toggle']").bootstrapToggle('destroy')                 
                    $("[data-toggle='toggle']").bootstrapToggle();
                    var cek2 = api.page.info().recordsTotal;
		            if(cek2 > 0){
						$('.for-isi2').show();
						$('.for-isi2').text(cek2);
		            }else{
		            	$('.for-isi2').hide();
		            }
                }
            });

            dt3 = $('#dataTable3').DataTable({
                lengthChange: false,
                filter: false,
                processing: true,
                serverSide: true,
                sorting: [],
                language: {
					url: "{{ asset('libs/jquery/datatables/media/Indonesian.json') }}"
				},
                ajax: {
                    url: '{!! route($routes.'.historis') !!}',
                    method: 'POST',
                    data: function (d) {
                        d._token = "{{ csrf_token() }}";
                        $('#dataFilters .filter-control').each(function(idx, el) {
                            var name = $(el).data('post');
                            var val = $(el).val();
                            d[name] = val;
                        })
                    },
                    error: function (responseError, status) {
		                console.log(responseError);
		                return false;
		            }
                },
                columns: {!! json_encode($structs['listStruct3']) !!},
                drawCallback: function() {
                	readMoreItem('list-more3');
                    var api = this.api();

                    api.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i, x, y) {
                        cell.innerHTML = parseInt(cell.innerHTML)+i+1;
                    });
                    
                    $('[data-toggle=tooltip]').tooltip()
                    $('[data-toggle=popover]').popover({
                        trigger: 'hover',
                        placement: 'top',
                        template: '<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-content"></div></div>'
                    });
                    $("[data-toggle='toggle']").bootstrapToggle('destroy')                 
                    $("[data-toggle='toggle']").bootstrapToggle();
                }
            });

            $('select[name="filter[page]"]').on('change', function(e) {
                var length = this.value;
                length = (length != '') ? length : 10;
                dt.page.len(length).draw();
                e.preventDefault();
            });

            $('.filter.button').on('click', function(e) {
                dt.draw();
                dt2.draw();
                dt3.draw();
                e.preventDefault();
            });

            $('.filter-control').on('change, keyup, changeDate', function(e) {
                dt.draw();
                dt2.draw();
                dt3.draw();
                e.preventDefault();
            });
            
            $('.filter-control').on('changed.bs.select', function(e) {
                dt.draw();
                dt2.draw();
                dt3.draw();
                e.preventDefault();
            });

            $('.reset.button').on('click', function(e) {
                $('.ui.dropdown').dropdown('clear');
                setTimeout(function() {
                    dt.draw();
                    dt2.draw();
	                dt3.draw();
                }, 200);
            });
        });
    </script>
@endpush