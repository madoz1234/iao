<script type="text/javascript">
function reloadMask() {
  var customInputmask = (function() {
      var config = {
        extendDefaults: {
          showMaskOnHover: false,
          showMaskOnFocus: false
        },
        extendDefinitions: {},
        extendAliases: {
          'numeric': {
            radixPoint: ',',
            groupSeparator: '.',
            autoGroup: true,
            placeholder: '',
            allowMinus: true,
            preValidation: function (buffer, pos, c, isSelection, opts) {
                if (c === ",") {
                    c = ".";
                    if (buffer.indexOf(',') != -1) {
                        buffer[buffer.indexOf(',')] = '.';
                    }
                }
                if ("-" === c || c == opts.negationSymbol.front)
                    return !0 === opts.allowMinus && (opts.isNegative = opts.isNegative === undefined || !opts.isNegative,
                            "" === buffer.join("") || {
                        caret: pos,
                        dopost: !0
                    });
                if (!1 === isSelection && c === opts.radixPoint && opts.digits !== undefined && (isNaN(opts.digits) || parseInt(opts.digits) > 0)) {
                    var radixPos = $.inArray(opts.radixPoint, buffer);
                    if (-1 !== radixPos)
                        return !0 === opts.numericInput ? pos === radixPos : {
                            caret: radixPos + 1
                        };
                }
                return !0;
            },
            digits: 3, 
          },
          'numericMinus': {
            alias: 'numeric',
            radixPoint: ',',
            groupSeparator: '.',
            autoGroup: true,
            placeholder: '',
            allowMinus: true,
            digits: '0', 
          },
          'currency': {
            alias: 'numeric',
            digits: '*',
            digitsOptional: true,
            radixPoint: ',',
            groupSeparator: '.',
            autoGroup: true,
            placeholder: '',
          },
          'euro': {
            alias: 'currency',
            prefix: '',
            suffix: ' €',
            radixPoint: ',',
            groupSeparator: '',
            autoGroup: false,
          },
          'euroComplex': {
            alias: 'currency',
            prefix: '',
            suffix: ' €',
          }
        }
      };

      var init = function() {
        Inputmask.extendDefaults(config.extendDefaults);
        Inputmask.extendDefinitions(config.extendDefinitions);
        Inputmask.extendAliases(config.extendAliases);
        $('[data-inputmask]').inputmask();
      };
      
      return {
        init: init
      };
    }());

    // Initialize app.
    (function() {
      customInputmask.init();
    }());
}
</script>
{{-- @endsection --}}