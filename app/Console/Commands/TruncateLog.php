<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;

use Illuminate\Filesystem\Filesystem;
use Storage;
use Carbon\Carbon;
use DB;
use App\Models\LogDms;

class TruncateLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'truncate:log-dms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Truncate Log DMS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
          LogDms::truncate();
          $this->info('TRUNCATE BERHASIL');
        }catch(\Exception $exception)
        {
            $this->error('GAGAL TRUNCATE');
        }
    }

}
