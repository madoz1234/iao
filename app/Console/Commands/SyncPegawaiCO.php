<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use App\Models\Dms;
use App\Models\Files;
use App\Models\Auths\User;
use App\Models\Master\Lokasi;

use Illuminate\Filesystem\Filesystem;
use Storage;
use Carbon\Carbon;
use Hash;
use App\Models\Master\BU;
use App\Models\Master\CO;
use App\Models\Master\AnakPerusahaan;
use App\Models\Temp\Division;

class SyncPegawaiCO extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:co';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Pegawai';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::where('kategori', 2)->whereNotNull('emp_og_unit')->get();

        $this->info('CO - Pegawai sync dimulai');

        foreach($users as $key => $user)
        {
            $emp_og_unit = $this->getDivision($user->emp_og_unit);

            $co = CO::where('obj_id', $emp_og_unit)->whereNotNull('obj_id')->first();
            if($co)
            {
                $user->kategori_id = $co->id;
                $user->save();
            }

            $this->info($key + 1 .'/'. $users->count());
        }
    }

    public function getDivision($og_unit)
    {
        $check = Division::where('obj_id', $og_unit)->first();
        if($check)
        {
            if($check->obj_level == 'ANAK USAHA')
            {
                return $check->obj_id;
            }else if($check->obj_level == 'DIVISION')
            {
                return $check->obj_id;
            }else if($check->obj_level == 'DEPARTMENT')
            {
                return $this->getProjectDivision($check->parent_id);
            }else if($check->obj_level == 'PROJECT')
            {
                return $this->getProjectDivision($check->parent_id);
            }
        }
    }

    public function getProjectDivision($parent_id)
    {
        $return = 0;

        $check = Division::where('obj_id', $parent_id)->first();
        if($check)
        {
            if($check->obj_level != 'DIVISION')
            {
                $return = $this->getProjectDivision($check->parent_id);
            }else if($check->obj_level == 'DEPARTMENT')
            {
                return $this->getProjectDivision($check->parent_id);
            }else{
                $return = $check->obj_id;
            }
        }

        return $return;
    }

}
