<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;

use Illuminate\Filesystem\Filesystem;
use Storage;
use Carbon\Carbon;

use App\Models\KegiatanAudit\Rencana\RencanaAudit;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit;
use App\Models\Files;
use App\Models\Dms;
use App\Models\LogDms;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit;
use App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka;

class UploadDMS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upload:dms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload DMS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $this->logger('Business Workspace berhasil dibuat / diubah dengan object_id : 1', 1, 1, 0);

        $otdsTicket = $this->otdsLogin();

        $this->info('TIKET OTDS = '. $otdsTicket);

        $this->rkiaUpload($otdsTicket);

        $this->penugasanAuditUpload($otdsTicket);

        $this->dokumenDetailUpload($otdsTicket);

        $this->programAuditUpload($otdsTicket);

        $this->openingMeetingLampiranUpload($otdsTicket);

        $this->openingMeetingHadirUpload($otdsTicket);

        $this->openingMeetingFotoUpload($otdsTicket);

        $this->kkaUpload($otdsTicket);

        $this->draftKkaUpload($otdsTicket);

        $this->lhaUpload($otdsTicket);

        $this->rapatInternalUpload($otdsTicket);
    }

    public function otdsLogin()
    {
      $client = new Client;

      $response = $client->request('POST', 'https://otds.waskita.co.id:8443/otdsws/v1/authentication/credentials', [
          'json' => [
              'user_name' => 'adminIAO',
              'password' => 'Audit123@'
          ],
          'verify' => false,
      ]);

      $ticket = json_decode($response->getBody()->getContents())->ticket;

      return $ticket;
    }

    public function otcsLogin($ticket)
    {
      try {
        $client = new Client;

        $response = $client->request('POST', 'http://dmsinterface.waskita.co.id:8080/xecm/api/v1/auth/otdsticket', [
            'json' => [
                'otds_ticket' => $ticket
            ],
        ]);

        return json_decode($response->getBody()->getContents())->ticket;

      }catch (\Exception $exception){
        $this->info('ERROR LOG : OTCS DOESNT CREATED');

        return $this->otcsLogin($this->otdsLogin());
      }
    }

    public function rkiaUpload($otdsTicket)
    {
      $dms = RencanaAudit::where('dms_update', 1)->get();

      if($dms->count() > 0)
      {
        foreach($dms as $key => $d)
        {
            $this->searchBw($otdsTicket, $d, $d->tahun, 'RKIA', 'False', $d->dms_object_id);

            if(file_exists(public_path().'/storage/'.$d->bukti))
            {
                $data = $this->uploadFile($otdsTicket,
                        $d,
                        'bukti',
                        $d->dms_parent_id,
                        'RKIA.'.pathinfo(storage_path('app/public/'.$d->bukti), PATHINFO_EXTENSION),
                        $d->dms_object_id,
                        $d->bukti);

                $this->info('SUCCESS LOG : RKIA BERHASIL DI UPLOAD');
            }

            $d->dms_update = 0;
            $d->save();

        }
      }
    }

    public function penugasanAuditUpload($otdsTicket)
    {
      $dms = PenugasanAudit::where('dms_update', 1)->get();

      if($dms->count() > 0)
      {
        foreach($dms as $key => $d)
        {
            $this->searchBw($otdsTicket, $d, $d->rencanadetail->rencanaaudit->tahun, 'Penugasan Audit', 'False', $d->dms_object_id);

            if(file_exists(public_path().'/storage/'.$d->bukti))
            {
                $data = $this->uploadFile($otdsTicket,
                        $d,
                        'bukti',
                        $d->dms_parent_id,
                        'Penugasan Audit.'.pathinfo(storage_path('app/public/'.$d->bukti), PATHINFO_EXTENSION),
                        $d->dms_object_id,
                        $d->bukti);

                $this->info('SUCCESS LOG : PENUGASAN AUDIT BERHASIL DI UPLOAD');
              }

                $d->dms_update = 0;
                $d->save();

        }
      }
    }

    public function dokumenDetailUpload($otdsTicket)
    {
      $dms = Files::where('dms_update', 1)->where('target_type', 'dokumen-detail')->get();

      if($dms->count() > 0)
      {
        foreach($dms as $key => $d)
        {
            $this->searchBw($otdsTicket,
                  $d,
                  $d->target->tinjauandokumen->PenugasanAudit->rencanadetail->rencanaaudit->tahun,
                  'Permintaan -'.$d->target->dokumen->judul,
                  'False',
                  $d->dms_object_id);

              if(file_exists(public_path().'/storage/'.$d->url))
              {
                  $data = $this->uploadFile($otdsTicket,
                          $d,
                          'url',
                          $d->dms_parent_id,
                          'Permintaan -'.$d->target->dokumen->judul.'.'.pathinfo(storage_path('app/public/'.$d->url), PATHINFO_EXTENSION),
                          $d->dms_object_id,
                          $d->url);

                  $this->info('SUCCESS LOG : PERMINTAAN -'.$d->target->dokumen->judul.' BERHASIL DI UPLOAD');

              }
              $d->dms_update = 0;
              $d->save();

        }
      }
    }

    public function programAuditUpload($otdsTicket)
    {
      $dms = ProgramAudit::where('dms_update', 1)->get();

      if($dms->count() > 0)
      {
        foreach($dms as $key => $d)
        {

            $this->searchBw($otdsTicket,
                  $d,
                  $d->penugasanaudit->rencanadetail->rencanaaudit->tahun,
                  'Program Audit',
                  'False',
                  $d->dms_object_id);

              if(file_exists(public_path().'/storage/'.$d->lampiran))
              {
                $data = $this->uploadFile($otdsTicket,
                        $d,
                        'lampiran',
                        $d->dms_parent_id,
                        'Program Audit.'.pathinfo(storage_path('app/public/'.$d->lampiran), PATHINFO_EXTENSION),
                        $d->dms_object_id,
                        $d->lampiran);

                      $this->info('SUCCESS LOG : PROGRAM AUDIT BERHASIL DI UPLOAD');
              }

              $d->dms_update = 0;
              $d->save();

        }
      }
    }

    public function openingMeetingLampiranUpload($otdsTicket)
    {
      $dms = OpeningMeeting::where('dms_update', 1)->get();

      if($dms->count() > 0)
      {
        foreach($dms as $key => $d)
        {

            $this->searchBw($otdsTicket,
                  $d,
                  $d->program->penugasanaudit->rencanadetail->rencanaaudit->tahun,
                  'Opening Meeting - Lampiran',
                  'False',
                  $d->dms_object_id);

              if(file_exists(public_path().'/storage/'.$d->lampiran))
              {
                $data = $this->uploadFile($otdsTicket,
                        $d,
                        'lampiran',
                        $d->dms_parent_id,
                        'Opening Meeting - Lampiran.'.pathinfo(storage_path('app/public/'.$d->lampiran), PATHINFO_EXTENSION),
                        $d->dms_object_id,
                        $d->lampiran);

                      $this->info('SUCCESS LOG : OPENING MEETING LAMPIRAN BERHASIL DI UPLOAD');
              }
              $d->dms_update = 0;
              $d->save();

        }
      }
    }

    public function openingMeetingHadirUpload($otdsTicket)
    {
      $dms = OpeningMeeting::where('dms_hadir_update', 1)->get();

      if($dms->count() > 0)
      {
        foreach($dms as $key => $d)
        {

            $this->searchBwHadir($otdsTicket,
                  $d,
                  $d->program->penugasanaudit->rencanadetail->rencanaaudit->tahun,
                  'Opening Meeting - Daftar Hadir',
                  'False',
                  $d->dms_hadir_object_id);

              if(file_exists(public_path().'/storage/'.$d->lampiran))
              {
                $data = $this->uploadFileHadir($otdsTicket,
                        $d,
                        'hadir',
                        $d->dms_parent_id,
                        'Opening Meeting - Daftar Hadir.'.pathinfo(storage_path('app/public/'.$d->hadir), PATHINFO_EXTENSION),
                        $d->dms_object_id,
                        $d->hadir);

                      $this->info('SUCCESS LOG : OPENING MEETING DAFTAR HADIR BERHASIL DI UPLOAD');
              }
              $d->dms_hadir_update = 0;
              $d->save();

        }
      }
    }

    public function openingMeetingFotoUpload($otdsTicket)
    {
      $dms = Files::where('dms_update', 1)->where('target_type', 'opening-meeting')->get();

      if($dms->count() > 0)
      {
        foreach($dms as $key => $d)
        {

            $this->searchBw($otdsTicket,
                  $d,
                  $d->target->program->penugasanaudit->rencanadetail->rencanaaudit->tahun,
                  'Opening Meeting - Foto Meeting',
                  'False',
                  $d->dms_object_id);

              if(file_exists(public_path().'/storage/'.$d->url))
              {
                  $data = $this->uploadFile($otdsTicket,
                          $d,
                          'url',
                          $d->dms_parent_id,
                          'Opening Meeting - Foto Meeting.'.pathinfo(storage_path('app/public/'.$d->url), PATHINFO_EXTENSION),
                          $d->dms_object_id,
                          $d->url);

                  $this->info('SUCCESS LOG : OPENING MEETING FOTO MEETING BERHASIL DI UPLOAD');

              }
              $d->dms_update = 0;
              $d->save();

        }
      }
    }

    public function closingMeetingLampiranUpload($otdsTicket)
    {
      $dms = ClosingMeeting::where('dms_update', 1)->get();

      if($dms->count() > 0)
      {
        foreach($dms as $key => $d)
        {

            $this->searchBw($otdsTicket,
                  $d,
                  $d->opening->program->penugasanaudit->rencanadetail->rencanaaudit->tahun,
                  'Closing Meeting - Lampiran',
                  'False',
                  $d->dms_object_id);

              if(file_exists(public_path().'/storage/'.$d->lampiran))
              {
                  $data = $this->uploadFile($otdsTicket,
                        $d,
                        'lampiran',
                        $d->dms_parent_id,
                        'Closing Meeting - Lampiran.'.pathinfo(storage_path('app/public/'.$d->lampiran), PATHINFO_EXTENSION),
                        $d->dms_object_id,
                        $d->lampiran);

                  $this->info('SUCCESS LOG : OPENING MEETING LAMPIRAN BERHASIL DI UPLOAD');
              }
              $d->dms_update = 0;
              $d->save();

        }
      }
    }

    public function closingMeetingHadirUpload($otdsTicket)
    {
      $dms = ClosingMeeting::where('dms_hadir_update', 1)->get();

      if($dms->count() > 0)
      {
        foreach($dms as $key => $d)
        {

            $this->searchBwHadir($otdsTicket,
                  $d,
                  $d->opening->program->penugasanaudit->rencanadetail->rencanaaudit->tahun,
                  'Closing Meeting - Daftar Hadir',
                  'False',
                  $d->dms_hadir_object_id);

              if(file_exists(public_path().'/storage/'.$d->lampiran))
              {

                $data = $this->uploadFileHadir($otdsTicket,
                        $d,
                        'hadir',
                        $d->dms_parent_id,
                        'Closing Meeting - Daftar Hadir.'.pathinfo(storage_path('app/public/'.$d->hadir), PATHINFO_EXTENSION),
                        $d->dms_object_id,
                        $d->hadir);

                      $this->info('SUCCESS LOG : OPENING MEETING DAFTAR HADIR BERHASIL DI UPLOAD');
              }
              $d->dms_hadir_update = 0;
              $d->save();

        }
      }
    }

    public function closingMeetingFotoUpload($otdsTicket)
    {
      $dms = Files::where('dms_update', 1)->where('target_type', 'closing-meeting')->get();

      if($dms->count() > 0)
      {
        foreach($dms as $key => $d)
        {

            $this->searchBw($otdsTicket,
                  $d,
                  $d->target->opening->program->penugasanaudit->rencanadetail->rencanaaudit->tahun,
                  'Opening Meeting - Foto Meeting',
                  'False',
                  $d->dms_object_id);

              if(file_exists(public_path().'/storage/'.$d->url))
              {
                  $data = $this->uploadFile($otdsTicket,
                        $d,
                        'url',
                        $d->dms_parent_id,
                        'Closing Meeting - Foto Meeting.'.pathinfo(storage_path('app/public/'.$d->url), PATHINFO_EXTENSION),
                        $d->dms_object_id,
                        $d->url);

                  $this->info('SUCCESS LOG : CLOSING MEETING FOTO MEETING BERHASIL DI UPLOAD');

              }
              $d->dms_update = 0;
              $d->save();

        }
      }
    }

    public function kkaUpload($otdsTicket)
    {
      $dms = Files::where('dms_update', 1)->where('target_type', 'kka')->get();

      if($dms->count() > 0)
      {
        foreach($dms as $key => $d)
        {
            $tipe = '';

            if($d->target->tipe == 0)
            {
              $tipe = 'Operasional';
            }else if($d->target->tipe == 1)
            {
              $tipe = 'Keuangan';
            }else if($d->target->tipe == 2)
            {
              $tipe = 'Sistem';
            }

            $this->searchBw($otdsTicket,
                  $d,
                  $d->target->draft->program->penugasanaudit->rencanadetail->rencanaaudit->tahun,
                  'KKA - '.$tipe,
                  'False',
                  $d->dms_object_id);

              if(file_exists(public_path().'/storage/'.$d->url))
              {
                  $data = $this->uploadFile($otdsTicket,
                          $d,
                          'url',
                          $d->dms_parent_id,
                          'KKA - '.$tipe.'.'.pathinfo(storage_path('app/public/'.$d->url), PATHINFO_EXTENSION),
                          $d->dms_object_id,
                          $d->url);

                  $this->info('SUCCESS LOG : KKA '.$tipe.' BERHASIL DI UPLOAD');

              }
              $d->dms_update = 0;
              $d->save();

        }
      }
    }

    public function draftKkaUpload($otdsTicket)
    {
      $dms = DraftKka::where('dms_update', 1)->get();

      if($dms->count() > 0)
      {
        foreach($dms as $key => $d)
        {

            $this->searchBw($otdsTicket,
                  $d,
                  $d->program->penugasanaudit->rencanadetail->rencanaaudit->tahun,
                  'KKA - DRAFT',
                  'False',
                  $d->dms_object_id);

              if(file_exists(public_path().'/storage/'.$d->bukti))
              {
                  $data = $this->uploadFile($otdsTicket,
                          $d,
                          'lampiran',
                          $d->dms_parent_id,
                          'KKA - DRAFT.'.pathinfo(storage_path('app/public/'.$d->lampiran), PATHINFO_EXTENSION),
                          $d->dms_object_id,
                          $d->lampiran);

                  $this->info('SUCCESS LOG : KKA DRAFT BERHASIL DI UPLOAD');
              }

              $d->dms_update = 0;
              $d->save();

        }
      }
    }

    public function lhaUpload($otdsTicket)
    {
      $dms = Files::where('dms_update', 1)->where('target_type', 'lha')->get();

      if($dms->count() > 0)
      {
        foreach($dms as $key => $d)
        {

            $this->searchBw($otdsTicket,
                  $d,
                  $d->target->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun,
                  'LHA',
                  'False',
                  $d->dms_object_id);

              if(file_exists(public_path().'/storage/'.$d->url))
              {
                  $data = $this->uploadFile($otdsTicket,
                          $d,
                          'url',
                          $d->dms_parent_id,
                          'LHA.'.pathinfo(storage_path('app/public/'.$d->url), PATHINFO_EXTENSION),
                          $d->dms_object_id,
                          $d->url);

                    $this->info('SUCCESS LOG : LHA BERHASIL DI UPLOAD');

              }
              $d->dms_update = 0;
              $d->save();

        }
      }
    }

    public function rapatInternalUpload($otdsTicket)
    {
      $dms = Files::where('dms_update', 1)->where('target_type', 'rapat-internal')->get();

      if($dms->count() > 0)
      {
        foreach($dms as $key => $d)
        {

            $tipe = '';
            $jenis = 'Rapat Internal';

            if($d->flag == 'hadir')
            {
                $tipe = 'Daftar Hadir';
            }else{
                $tipe = 'Lain lain';
            }

            if($d->target->status == 2)
            {
                $jenis = 'Rapat Eksternal';
            }

            $this->searchBw($otdsTicket,
                  $d,
                  Carbon::createFromFormat('d-m-Y', $d->target->tanggal)->format('Y'),
                  $jenis.' - '. $tipe,
                  'False',
                  $d->dms_object_id);

              if(file_exists(public_path().'/storage/'.$d->url))
              {
                  $data = $this->uploadFile($otdsTicket,
                          $d,
                          'url',
                          $d->dms_parent_id,
                          $jenis.' - '.$tipe.'.'.pathinfo(storage_path('app/public/'.$d->url), PATHINFO_EXTENSION),
                          $d->dms_object_id,
                          $d->url);

                      $this->info('SUCCESS LOG : '.$jenis.' '.$tipe.' BERHASIL DI UPLOAD');

              }
              $d->dms_update = 0;
              $d->save();

        }
      }
    }

    public function searchBw($otdsTicket, $data, $tahun, $tipe, $is_delete, $object_id = NULL)
    {
        if($object_id == NULL)
        {
            $dms = Dms::first();
            if(!$dms)
            {
                $dms = new Dms;
                $dms->last_id = 0;
                $dms->save();
            }

            $object_id = $dms->last_id + 1;
            $dms->last_id = $object_id;
            $dms->save();
        }

        $ticket = $this->otcsLogin($otdsTicket);

        $this->info('TIKET OTCS = '. $ticket);

        $client = new Client;

        $response = $client->request('PUT', 'http://dmsinterface.waskita.co.id:8080/xecm/api/v1/businessworkspaces', [
          'json' => [
            'external_system_id' => 'XECMInterface',
            'business_info' => [
              [
                'object_id' => $object_id,
                'object_type' => 'Dokumen_IAO',
                'business_properties' => [
                  [
                    'name' => 'ID Dokumen',
                    'values' => [
                      $object_id,
                    ],
                  ],
                  [
                    'name' => 'Tahun',
                    'values' => [
                      $tahun,
                    ],
                  ],
                  [
                    'name' => 'Tipe Dokumen',
                    'values' => [
                      $tipe,
                    ],
                  ],
                  [
                    'name' => 'is_delete',
                    'values' => [
                      $is_delete,
                    ],
                  ],
                ],
              ],
            ],
          ],
          'headers' => [
            'otcsticket' => $ticket
          ],
        ]);

        $bw = json_decode($response->getBody()->getContents())->results[0];
        $this->info('BW : '.$bw);

        if($bw == 0)
        {
            $this->searchBw($otdsTicket, $data, $tahun, $tipe, $is_delete);
            $this->logger('Business Workspace gagal dibuat / diubah dengan object_id : '.$object_id, $object_id, NULL, 1);

        }else{
          $data->dms_parent_id = $bw;
          $data->dms_object_id = $object_id;
          $data->save();
        }

        $this->logger('Business Workspace berhasil dibuat / diubah dengan object_id : '.$object_id, $object_id, NULL, 0);

        return $bw;
    }

    public function uploadFile($otdsTicket, $data, $field, $parent_id, $name, $object_id, $fileurl)
    {
      $rsp = NULL;
      try {
        $ticket = $this->otcsLogin($otdsTicket);
        $this->info('TIKET OTCS = '. $ticket);

        $client = new Client;

        $response = $client->request('POST', 'http://dms.waskita.co.id/otcs/cs.exe/api/v2/nodes', [
                'multipart' => [
                  [
                    'name'     => 'type',
                    'contents' => 144,
                  ],
                  [
                  'name'     => 'parent_id',
                  'contents' => $parent_id,
                  ],
                  [
                  'name'     => 'name',
                  'contents' => $object_id.' '.$name,
                  ],
                  [
                  'name'     => 'file',
                  'contents' => fopen(public_path().'/storage/'.$fileurl, 'r'),
                  ]
                  ],
                  'headers' => [
                    'otcsticket' => $ticket
                    ],
        ]);

        $rsp = json_decode($response->getBody()->getContents());


        $this->info('FILE ID = '. $rsp->results->data->properties->id);

      }catch(\Exception $exception)
      {
          $this->info('GAGAL UPLOAD  ID = '. $object_id);

          $this->logger('File gagal diupload dengan business workspace object_id : '.$object_id, $object_id, $object_id.' '.$name, 1);
      }

      if($rsp == NULL)
      {
          $this->uploadFile($otdsTicket, $data, $field, $parent_id, $name, $object_id + 1, $fileurl);
      }else{
        Storage::disk('public')->delete($data->$field);

        $data->$field = 'http://dmsinterface.waskita.co.id:8080/xecm/api/v1/nodes/'.$rsp->results->data->properties->id.'/content';
        $data->dms_type = 144;
        $data->dms = 1;
        $data->dms_uploaded_date = Carbon::now()->format('Y-m-d H:i:s');
        $data->dms_file_id = $rsp->results->data->properties->id;
        $data->save();

        $this->logger('File berhasil diupload dengan object_id : '.$object_id, $object_id, $object_id.' '.$name, 1);
      }

      return $data;
    }

    public function searchBwHadir($otdsTicket, $data, $tahun, $tipe, $is_delete, $object_id = NULL)
    {
        if($object_id == NULL)
        {
            $dms = Dms::first();
            if(!$dms)
            {
                $dms = new Dms;
                $dms->last_id = 0;
                $dms->save();
            }

            $object_id = $dms->last_id + 1;
            $dms->last_id = $object_id;
            $dms->save();
        }

        $ticket = $this->otcsLogin($otdsTicket);

        $this->info('TIKET OTCS = '. $ticket);

        $client = new Client;

        $response = $client->request('PUT', 'http://dmsinterface.waskita.co.id:8080/xecm/api/v1/businessworkspaces', [
          'json' => [
            'external_system_id' => 'XECMInterface',
            'business_info' => [
              [
                'object_id' => $object_id,
                'object_type' => 'Dokumen_IAO',
                'business_properties' => [
                  [
                    'name' => 'ID Dokumen',
                    'values' => [
                      $object_id,
                    ],
                  ],
                  [
                    'name' => 'Tahun',
                    'values' => [
                      $tahun,
                    ],
                  ],
                  [
                    'name' => 'Tipe Dokumen',
                    'values' => [
                      $tipe,
                    ],
                  ],
                  [
                    'name' => 'is_delete',
                    'values' => [
                      $is_delete,
                    ],
                  ],
                ],
              ],
            ],
          ],
          'headers' => [
            'otcsticket' => $ticket
          ],
        ]);

        $bw = json_decode($response->getBody()->getContents())->results[0];
        $this->info('BW : '.$bw);

        if($bw == 0)
        {
            $this->searchBw($otdsTicket, $data, $tahun, $tipe, $is_delete);
        }else{
          $data->dms_hadir_parent_id = $bw;
          $data->dms_hadir_object_id = $object_id;
          $data->save();
        }

        return $bw;
    }

    public function uploadFileHadir($otdsTicket, $data, $field, $parent_id, $name, $object_id, $fileurl)
    {
      $rsp = NULL;
      try {
        $ticket = $this->otcsLogin($otdsTicket);
        $this->info('TIKET OTCS = '. $ticket);

        $client = new Client;

        $response = $client->request('POST', 'http://dms.waskita.co.id/otcs/cs.exe/api/v2/nodes', [
                'multipart' => [
                  [
                    'name'     => 'type',
                    'contents' => 144,
                  ],
                  [
                  'name'     => 'parent_id',
                  'contents' => $parent_id,
                  ],
                  [
                  'name'     => 'name',
                  'contents' => $object_id.' '.$name,
                  ],
                  [
                  'name'     => 'file',
                  'contents' => fopen(public_path().'/storage/'.$fileurl, 'r'),
                  ]
                  ],
                  'headers' => [
                    'otcsticket' => $ticket
                    ],
        ]);

        $rsp = json_decode($response->getBody()->getContents());
        $this->info('FILE ID = '. $rsp->results->data->properties->id);

      }catch(\Exception $exception)
      {
          $this->info('GAGAL UPLOAD  ID = '. $object_id);
      }

      if($rsp == NULL)
      {
          $this->uploadFile($otdsTicket, $data, $field, $parent_id, $name, $object_id + 1, $fileurl);
      }else{
        Storage::disk('public')->delete($data->$field);

        $data->$field = 'http://dmsinterface.waskita.co.id:8080/xecm/api/v1/nodes/'.$rsp->results->data->properties->id.'/content';
        $data->dms_hadir_type = 144;
        $data->dms_hadir = 1;
        $data->dms_hadir_uploaded_date = Carbon::now()->format('Y-m-d H:i:s');
        $data->dms_hadir_file_id = $rsp->results->data->properties->id;
        $data->save();
      }

      return $data;
    }

    public function logger($message, $bw_id, $file_name = NULL, $error)
    {
        $save = new LogDms;
        $save->message = $message;
        $save->bw_id = $bw_id;
        $save->file_name = $file_name;
        $save->error = $error;

        $save->save();
    }

}
