<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Models\Rapat\RapatInternal;
use Mail;
use Carbon\Carbon;
use App\Libraries\CoreSn;
use PDF;

class RisalahPdfEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $details;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $record = RapatInternal::find($this->details['id']);
            $data = [
                'records' => $record,
                'today' => CoreSn::DateToString(Carbon::now()),
            ];
            $pdf = PDF::loadView('emails.risalah-pdf', $data)->setPaper('a4', 'landscape');
            Mail::send('emails.mail', $data, function($message) use ($data, $pdf){
                $message->to($this->details['to']);
                $message->subject('Risah Rapat');

                 //Attach output from PDF doc, RisalahRapat.pdf is the name of the file attached
                $message->attachData($pdf->output(),'RisalahRapat.pdf');
            });
    }
}
