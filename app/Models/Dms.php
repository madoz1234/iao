<?php

namespace App\Models;

use App\Models\Model;

class Dms extends Model
{
    protected $table 		= 'sys_dms';

    protected $fillable 	= [
        'last_id'
    ];
    protected $appends = [
        'attachment_url',
    ];
}
