<?php
namespace App\Models\KegiatanAudit\TindakLanjut;

use App\Models\Model;
use App\Models\Attachments;
use App\Models\KegiatanAudit\TindakLanjut\RegisterTLDetail;

class RegisterTLDetailLampiran extends Model
{
    /* default */
    protected $table 		= 'trans_register_tl_detail_lampiran';
    protected $fillable 	= ['detail_id','filename','url'];

    /* data ke log */
    protected $log_table    = 'log_trans_register_tl_detail_lampiran';
    protected $log_table_fk = 'ref_id';
    /* relation */

    public function detail(){
        return $this->belongsTo(RegisterTLDetail::class, 'detail_id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
