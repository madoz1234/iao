<?php
namespace App\Models\KegiatanAudit\TindakLanjut;

use App\Models\Model;
use App\Models\Attachments;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka;
use App\Models\KegiatanAudit\TindakLanjut\RegisterTL;
use App\Models\KegiatanAudit\TindakLanjut\RegisterTLDetail;
use App\Models\KegiatanAudit\TindakLanjut\RegisterTLDetailLampiran;

class RegisterTL extends Model
{
    /* default */
    protected $table 		= 'trans_register_tl';
    protected $fillable 	= ['draft_id','status'];

    /* data ke log */
    protected $log_table    = 'log_trans_register_tl';
    protected $log_table_fk = 'ref_id';
    /* relation */

    public function kka(){
        return $this->belongsTo(DraftKka::class, 'draft_id');
    }

    public function filesMorphClass()
    {
        return 'surat-perintah';
    }

    public function detail(){
        return $this->hasMany(RegisterTLDetail::class, 'register_id' , 'id');
    }

    public function attachments()
    {
        return $this->morphMany(Attachments::class, 'target');
    }

    public function files()
    {
        return $this->morphMany(Files::class, 'target');
    }

    public function saveDetail($detail)
    {
    	$tipe = $this->kka->program->penugasanaudit->rencanadetail->tipe_object;
    	if($detail)
    	{
    		if(!empty($detail['operasional'])){
	    		foreach ($detail['operasional'] as $key => $value) {
					if(!empty($value['lampiran'])){
						if(!is_null($value['lampiran'])){
							$register_detail 						= RegisterTLDetail::where('register_id', $this->id)->where('rekomendasi_id', $key)->first();
		    				$register_detail->tl 					= $value['tl'];
		    				if($tipe == 2){
		    					$register_detail->status 				= 1;
		    				}else{
			    				$register_detail->status 				= 2;
		    				}
		    				$detil = $this->detail()->save($register_detail);

		    				$data_lampiran 							= new RegisterTLDetailLampiran;
		    				$path 									= $value['lampiran']->store('tindak-lanjut', 'public');
							$data_lampiran->filename 				= $value['lampiran']->getClientOriginalName();
							$data_lampiran->url 					= $path;
							$cek = $detil->detaillampiran()->save($data_lampiran);
						}
					}
	    		}
    		}

    		if(!empty($detail['keuangan'])){
	    		foreach ($detail['keuangan'] as $koy => $val) {
					if(!empty($val['lampiran'])){
						if(!is_null($val['lampiran'])){
							$register_detail 						= RegisterTLDetail::where('register_id', $this->id)->where('rekomendasi_id', $koy)->first();
		    				$register_detail->tl 					= $val['tl'];
		    				if($tipe == 2){
		    					$register_detail->status 				= 1;
		    				}else{
			    				$register_detail->status 				= 2;
		    				}
		    				$detil = $this->detail()->save($register_detail);

		    				$data_lampiran 							= new RegisterTLDetailLampiran;
		    				$path 									= $val['lampiran']->store('tindak-lanjut', 'public');
							$data_lampiran->filename 				= $val['lampiran']->getClientOriginalName();
							$data_lampiran->url 					= $path;
							$cek = $detil->detaillampiran()->save($data_lampiran);
						}
					}
	    		}
    		}

    		if(!empty($detail['keuangan'])){
	    		foreach ($detail['sistem'] as $kiy => $vil) {
					if(!empty($vil['lampiran'])){
						if(!is_null($vil['lampiran'])){
							$register_detail 						= RegisterTLDetail::where('register_id', $this->id)->where('rekomendasi_id', $kiy)->first();
		    				$register_detail->tl 					= $vil['tl'];
		    				if($tipe == 2){
		    					$register_detail->status 				= 1;
		    				}else{
			    				$register_detail->status 				= 2;
		    				}
		    				$detil = $this->detail()->save($register_detail);

		    				$data_lampiran 							= new RegisterTLDetailLampiran;
		    				$path 									= $vil['lampiran']->store('tindak-lanjut', 'public');
							$data_lampiran->filename 				= $vil['lampiran']->getClientOriginalName();
							$data_lampiran->url 					= $path;
							$cek = $detil->detaillampiran()->save($data_lampiran);
						}
					}
	    		}
    		}	
    	}
    }

    public function updateDetail($detail)
    {
    	$tipe = $this->kka->program->penugasanaudit->rencanadetail->tipe_object;
    	if($detail)
    	{	
    		if(!empty($detail['operasional'])){
	    		foreach ($detail['operasional'] as $a => $value) {
					$register_detail 						= RegisterTLDetail::where('register_id', $this->id)->where('rekomendasi_id', $a)->first();
					if(!empty($value['pilihan'])){
		    			if($value['pilihan'] == 4){
							$register_detail->status 				= 3;
		    			}else{
			    			$register_detail->status 				= 0;
		    			}
						$register_detail->stage 				= $value['pilihan'];
					}
					$detil = $this->detail()->save($register_detail);
	    		}
    		}

    		if(!empty($detail['keuangan'])){
	    		foreach ($detail['keuangan'] as $b => $val) {
					$register_detail 						= RegisterTLDetail::where('register_id', $this->id)->where('rekomendasi_id', $b)->first();
					if(!empty($val['pilihan'])){
		    			if($val['pilihan'] == 4){
							$register_detail->status 				= 3;
		    			}else{
			    			$register_detail->status 				= 0;
		    			}
						$register_detail->stage 				= $val['pilihan'];
					}
					$detil = $this->detail()->save($register_detail);
	    		}
    		}

    		if(!empty($detail['sistem'])){
    			foreach ($detail['sistem'] as $c => $vil) {
					$register_detail 						= RegisterTLDetail::where('register_id', $this->id)->where('rekomendasi_id', $c)->first();
					if(!empty($val['pilihan'])){
		    			if($vil['pilihan'] == 4){
							$register_detail->status 				= 2;
		    			}else{
			    			$register_detail->status 				= 0;
		    			}
						$register_detail->stage 				= $vil['pilihan'];
					}
					$detil = $this->detail()->save($register_detail);
	    		}
    		}

    		$cari = RegisterTLDetail::where('register_id', $this->id)->where('stage', '<', 4)->get();
    		if(count($cari) == 0){
    			$update = RegisterTL::find($this->id);
    			$update->status =2;
    			$update->save();
    		}
    	}
    }

    public function updateDetails($detail)
    {
    	if($detail)
    	{	
    		if(!empty($detail['operasional'])){
	    		foreach ($detail['operasional'] as $a => $value) {
					$register_detail 						= RegisterTLDetail::where('register_id', $this->id)->where('rekomendasi_id', $a)->first();
					if(!empty($value['svp'])){
						$register_detail->status 				= 2;
					}
					$detil = $this->detail()->save($register_detail);
	    		}
    		}

    		if(!empty($detail['keuangan'])){
	    		foreach ($detail['keuangan'] as $b => $val) {
					$register_detail 						= RegisterTLDetail::where('register_id', $this->id)->where('rekomendasi_id', $b)->first();
					if(!empty($val['svp'])){
						$register_detail->status 				= 2;
					}
					$detil = $this->detail()->save($register_detail);
	    		}
    		}

    		if(!empty($detail['sistem'])){
    			foreach ($detail['sistem'] as $c => $vil) {
					$register_detail 						= RegisterTLDetail::where('register_id', $this->id)->where('rekomendasi_id', $c)->first();
					if(!empty($vil['svp'])){
						$register_detail->status 				= 2;
					}
					$detil = $this->detail()->save($register_detail);
	    		}
    		}
    	}
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
