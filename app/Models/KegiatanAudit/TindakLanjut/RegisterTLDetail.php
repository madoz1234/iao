<?php
namespace App\Models\KegiatanAudit\TindakLanjut;

use App\Models\Model;
use App\Models\Attachments;
use App\Models\KegiatanAudit\TindakLanjut\RegisterTL;
use App\Models\KegiatanAudit\TindakLanjut\RegisterTLDetailLampiran;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaDetail;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaDetailRekomendasi;

class RegisterTLDetail extends Model
{
    /* default */
    protected $table 		= 'trans_register_tl_detail';
    protected $fillable 	= ['register_id','kka_detail_id','rekomendasi_id','fungsi','tl','status','stage'];

    /* data ke log */
    protected $log_table    = 'log_trans_register_tl_detail';
    protected $log_table_fk = 'ref_id';
    /* relation */

    public function lha(){
        return $this->belongsTo(LHA::class, 'lha_id');
    }

    public function tl(){
        return $this->belongsTo(RegisterTL::class, 'register_id');
    }

    public function kkadetail(){
        return $this->belongsTo(DraftKkaDetail::class, 'kka_detail_id');
    }

    public function kkadetailrekomendasi(){
        return $this->belongsTo(DraftKkaDetailRekomendasi::class, 'rekomendasi_id');
    }

    public function detaillampiran(){
        return $this->hasMany(RegisterTLDetailLampiran::class, 'detail_id' , 'id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
