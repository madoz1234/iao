<?php
namespace App\Models\KegiatanAudit\Rencana;

use App\Models\Auths\User;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit;
use App\Models\KegiatanAudit\Rencana\RencanaAudit;
use App\Models\KegiatanKonsultasi\KegiatanKonsultasi;
use App\Models\KegiatanLainnya\KegiatanLainnya;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetailUser;
use App\Models\Master\Konsultasi;
use App\Models\Master\Lain;
use App\Models\Model;

class RencanaAuditDetail extends Model
{
    /* default */
    protected $table 		= 'trans_rencana_audit_detail';
    protected $fillable 	= ['rencana_id','konsultasi_id','lain_id','tipe_object','object_id','rencana','keterangan','tgl_mulai','tgl_selesai','jenis','nilai_kontrak','snk','progress','progress_ra','progress_ri','deviasi','risk_impact','mapp','bkpu','deviasi_bkpu','bkpu_ri','status_penugasan','status_konsultasi','status_lain','user_operasional','user_keuangan','user_sistem'];

    /* data ke log */
    protected $log_table    = 'log_trans_rencana_audit_detail';
    protected $log_table_fk = 'ref_id';

    /* relation */
    public function rencanaaudit(){
        return $this->belongsTo(RencanaAudit::class, 'rencana_id');
    }

    public function penugasan(){
       return $this->hasOne(PenugasanAudit::class, 'rencana_id');
    }

    public function konsultasi(){
        return $this->belongsTo(Konsultasi::class, 'konsultasi_id');
    }

    public function lain(){
        return $this->belongsTo(Lain::class, 'lain_id');
    }

    public function detailuser(){
        return $this->hasMany(RencanaAuditDetailUser::class, 'detail_id' , 'id');
    }

    public function kegiatan_konsultasi(){
        return $this->hasOne(KegiatanKonsultasi::class, 'rencana_detail_id');
    }

    public function kegiatan_lainnya(){
        return $this->hasOne(KegiatanLainnya::class, 'rencana_detail_id');
    }

    public function getKategoriAttribute()
    {
        if($this->tipe == 1){
            return $this->konsultasi->nama;
        }elseif($this->tipe == 2){
            if ($this->lain_id == 1000) {
                return 'SPIN';
            }elseif($this->lain_id == 1001) {
                return 'IACM';
            }else{
                return 'Counterpart Auditor Eksternal - '.$this->lain->nama;
            }
        }else{
            if ($this->tipe_object == 0) {
                return 'Business Unit (BU)';
            }elseif($this->tipe_object == 1) {
                return 'Corporate Office (CO)';
            }elseif($this->tipe_object == 2) {
                return 'Project';
            }else{
                return 'Anak Perusahaan';
            }
        }
    }
}
