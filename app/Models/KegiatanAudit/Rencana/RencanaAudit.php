<?php
namespace App\Models\KegiatanAudit\Rencana;

use App\Models\Model;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetailUser;
use App\Models\Master\Edisi;
use App\Models\SessionLog;


class RencanaAudit extends Model
{
    /* default */
    protected $table 		= 'trans_rencana_audit';
    protected $fillable 	= ['tahun','tipe'];

    /* data ke log */
    protected $log_table    = 'log_trans_rencana_audit';
    protected $log_table_fk = 'ref_id';

    /* relation */
    public function detailrencana(){
        return $this->hasMany(RencanaAuditDetail::class, 'rencana_id' , 'id');
    }

    // public function penugasan_latest()
    // {
    //     return $this->hasOne(RencanaKegiatanLelang::class, 'kegiatan_pemasaran_id')->latest();
    // }

    public function saveDetail($detil)
    {
    	if($detil)
    	{
    		$data=array();
    		foreach ($detil as $key => $value) {
    			if(!empty($value['detail_id'])){
    				$hapus = RencanaAuditDetailUser::where('detail_id', $value['detail_id'])
			    									  ->delete();
    			}
    			
    			if($value['tipe'] == 0){
    				if(!empty($value['detail_id'])){
	    				$detail = RencanaAuditDetail::find($value['detail_id']);
		    			$detail->tipe_object 		= $value['tipe_object'];
				        $detail->object_id 			= $value['object_id'];
				        $detail->rencana 			= decimal_for_save($value['rencana']);
				        $detail->keterangan 		= $value['keterangan'];
				        if(!empty($value['tipe_object'])){
				        	if($value['tipe_object'] == 2){
						        $detail->tgl_mulai 			= date('d-m-Y', strtotime($value['tgl_mulai']));
						        $detail->tgl_selesai 		= date('d-m-Y', strtotime($value['tgl_selesai']));
						        $detail->jenis 				= $value['jenis'];
						        if($value['jenis'] == 1 || $value['jenis'] == 2){
							        $detail->snk 			= decimal_for_save($value['snk']);
							        $detail->progress 		= decimal_for_save($value['progress']);
						        }
						        $detail->nilai_kontrak 		= decimal_for_save($value['nilai_kontrak']);
						        $detail->progress_ra 		= decimal_for_save($value['progress_ra']);
						        $detail->progress_ri 		= decimal_for_save($value['progress_ri']);
						        $detail->deviasi 			= decimal_for_save($value['deviasi']);
						        $detail->risk_impact 		= $value['risk_impact'];
						        $detail->mapp 				= decimal_for_save($value['mapp']);
						        $detail->bkpu 				= decimal_for_save($value['bkpu']);
						        $detail->deviasi_bkpu 		= decimal_for_save($value['deviasi_bkpu']);
						        $detail->bkpu_ri 			= $value['bkpu_ri'];
				        	}
				        }
		    			$cek = $this->detailrencana()->save($detail);
		    			if(!empty($value['operasional'])){
			    			foreach ($value['operasional'] as $key => $vall) {
			    				$detail_op = new RencanaAuditDetailUser; 
			    				$detail_op->tipe = 0; 
			    				$detail_op->user_id = $vall; 
				    			$cek->detailuser()->save($detail_op);
			    			}
		    			}

		    			if(!empty($value['keuangan'])){
			    			foreach ($value['keuangan'] as $key => $val) {
			    				$detail_keu = new RencanaAuditDetailUser; 
			    				$detail_keu->tipe = 1; 
			    				$detail_keu->user_id = $val; 
				    			$cek->detailuser()->save($detail_keu);
			    			}
		    			}

		    			if(!empty($value['sistem'])){
			    			foreach ($value['sistem'] as $key => $va) {
			    				$detail_sis = new RencanaAuditDetailUser; 
			    				$detail_sis->tipe = 2; 
			    				$detail_sis->user_id = $va; 
				    			$cek->detailuser()->save($detail_sis);
			    			}
		    			}
	    			}else{
	    				if(!is_null($value['object_id'])){
			    			$detail = new RencanaAuditDetail;
			    			$detail->tipe_object 	= $value['tipe_object'];
					        $detail->object_id 		= $value['object_id'];
					        $detail->rencana 		= decimal_for_save($value['rencana']);
					        if($value['keterangan']){
						        $detail->keterangan 	= $value['keterangan'];
					        }else{
					        	$detail->keterangan 	= 0;
					        }
					        if(!empty($value['tipe_object'])){
						        if($value['tipe_object'] == 2){
							        $detail->tgl_mulai 			= date('d-m-Y', strtotime($value['tgl_mulai']));
							        $detail->tgl_selesai 		= date('d-m-Y', strtotime($value['tgl_selesai']));
							        $detail->jenis 				= $value['jenis'];
							        if($value['jenis'] == 1 || $value['jenis'] == 2){
								        $detail->snk 			= decimal_for_save($value['snk']);
								        $detail->progress 		= decimal_for_save($value['progress']);
							        }
							        $detail->nilai_kontrak 		= decimal_for_save($value['nilai_kontrak']);
							        $detail->progress_ra 		= decimal_for_save($value['progress_ra']);
							        $detail->progress_ri 		= decimal_for_save($value['progress_ri']);
							        $detail->deviasi 			= decimal_for_save($value['deviasi']);
							        $detail->risk_impact 		= $value['risk_impact'];
							        $detail->mapp 				= decimal_for_save($value['mapp']);
							        $detail->bkpu 				= decimal_for_save($value['bkpu']);
							        $detail->deviasi_bkpu 		= decimal_for_save($value['deviasi_bkpu']);
							        $detail->bkpu_ri 			= $value['bkpu_ri'];
					        	}
					        }
			    			$cek = $this->detailrencana()->save($detail);
			    			if(!empty($value['operasional'])){
				    			foreach ($value['operasional'] as $key => $vall) {
				    				$detail_op = new RencanaAuditDetailUser; 
				    				$detail_op->tipe = 0; 
				    				$detail_op->user_id = $vall; 
					    			$cek->detailuser()->save($detail_op);
				    			}
			    			}

			    			if(!empty($value['keuangan'])){
				    			foreach ($value['keuangan'] as $key => $val) {
				    				$detail_keu = new RencanaAuditDetailUser; 
				    				$detail_keu->tipe = 1; 
				    				$detail_keu->user_id = $val; 
					    			$cek->detailuser()->save($detail_keu);
				    			}
			    			}

			    			if(!empty($value['sistem'])){
				    			foreach ($value['sistem'] as $key => $va) {
				    				$detail_sis = new RencanaAuditDetailUser; 
				    				$detail_sis->tipe = 2; 
				    				$detail_sis->user_id = $va; 
					    			$cek->detailuser()->save($detail_sis);
				    			}
			    			}
	    				}
	    			}
    			}elseif($value['tipe'] == 1){
    				if(!empty($value['detail_id'])){
	    				$detail 					= RencanaAuditDetail::find($value['detail_id']);
		    			$detail->tipe 		 		= $value['tipe'];
				        $detail->konsultasi_id 		= $value['konsultasi_id'];
				        $detail->rencana 			= decimal_for_save($value['rencana']);
				        $detail->keterangan 		= $value['keterangan'];
		    			$cek = $this->detailrencana()->save($detail);
		    			if(!empty($value['operasional'])){
			    			foreach ($value['operasional'] as $key => $vall) {
			    				$detail_op = new RencanaAuditDetailUser; 
			    				$detail_op->tipe = 0; 
			    				$detail_op->user_id = $vall; 
				    			$cek->detailuser()->save($detail_op);
			    			}
		    			}

		    			if(!empty($value['keuangan'])){
			    			foreach ($value['keuangan'] as $key => $val) {
			    				$detail_keu = new RencanaAuditDetailUser; 
			    				$detail_keu->tipe = 1; 
			    				$detail_keu->user_id = $val; 
				    			$cek->detailuser()->save($detail_keu);
			    			}
		    			}

		    			if(!empty($value['sistem'])){
			    			foreach ($value['sistem'] as $key => $va) {
			    				$detail_sis = new RencanaAuditDetailUser; 
			    				$detail_sis->tipe = 2; 
			    				$detail_sis->user_id = $va; 
				    			$cek->detailuser()->save($detail_sis);
			    			}
		    			}
	    			}else{
		    			$detail 					= new RencanaAuditDetail;
		    			$detail->tipe 		 		= $value['tipe'];
				        $detail->konsultasi_id 		= $value['konsultasi_id'];
				        $detail->rencana 			= decimal_for_save($value['rencana']);
				        $detail->keterangan 		= $value['keterangan'];
		    			$cek = $this->detailrencana()->save($detail);
		    			if(!empty($value['operasional'])){
			    			foreach ($value['operasional'] as $key => $vall) {
			    				$detail_op = new RencanaAuditDetailUser; 
			    				$detail_op->tipe = 0; 
			    				$detail_op->user_id = $vall; 
				    			$cek->detailuser()->save($detail_op);
			    			}
		    			}

		    			if(!empty($value['keuangan'])){
			    			foreach ($value['keuangan'] as $key => $val) {
			    				$detail_keu = new RencanaAuditDetailUser; 
			    				$detail_keu->tipe = 1; 
			    				$detail_keu->user_id = $val; 
				    			$cek->detailuser()->save($detail_keu);
			    			}
		    			}

		    			if(!empty($value['sistem'])){
			    			foreach ($value['sistem'] as $key => $va) {
			    				$detail_sis = new RencanaAuditDetailUser; 
			    				$detail_sis->tipe = 2; 
			    				$detail_sis->user_id = $va; 
				    			$cek->detailuser()->save($detail_sis);
			    			}
		    			}
	    			}
    			}else{
    				if(!empty($value['detail_id'])){
	    				$detail 					= RencanaAuditDetail::find($value['detail_id']);
		    			$detail->tipe 		 		= $value['tipe'];
				        $detail->lain_id 			= $value['lain'];
				        $detail->rencana 			= decimal_for_save($value['rencana']);
				        $detail->keterangan 		= $value['keterangan'];
		    			$cek = $this->detailrencana()->save($detail);
		    			if(!empty($value['operasional'])){
			    			foreach ($value['operasional'] as $key => $vall) {
			    				$detail_op = new RencanaAuditDetailUser; 
			    				$detail_op->tipe = 0; 
			    				$detail_op->user_id = $vall; 
				    			$cek->detailuser()->save($detail_op);
			    			}
		    			}

		    			if(!empty($value['keuangan'])){
			    			foreach ($value['keuangan'] as $key => $val) {
			    				$detail_keu = new RencanaAuditDetailUser; 
			    				$detail_keu->tipe = 1; 
			    				$detail_keu->user_id = $val; 
				    			$cek->detailuser()->save($detail_keu);
			    			}
		    			}

		    			if(!empty($value['sistem'])){
			    			foreach ($value['sistem'] as $key => $va) {
			    				$detail_sis = new RencanaAuditDetailUser; 
			    				$detail_sis->tipe = 2; 
			    				$detail_sis->user_id = $va; 
				    			$cek->detailuser()->save($detail_sis);
			    			}
		    			}
	    			}else{
		    			$detail 					= new RencanaAuditDetail;
		    			$detail->tipe 		 		= $value['tipe'];
				        $detail->lain_id 			= $value['lain'];
				        $detail->rencana 			= decimal_for_save($value['rencana']);
				        $detail->keterangan 		= $value['keterangan'];
		    			$cek = $this->detailrencana()->save($detail);
		    			if(!empty($value['operasional'])){
			    			foreach ($value['operasional'] as $key => $vall) {
			    				$detail_op = new RencanaAuditDetailUser; 
			    				$detail_op->tipe = 0; 
			    				$detail_op->user_id = $vall; 
				    			$cek->detailuser()->save($detail_op);
			    			}
		    			}
		    			if(!empty($value['keuangan'])){
			    			foreach ($value['keuangan'] as $key => $val) {
			    				$detail_keu = new RencanaAuditDetailUser; 
			    				$detail_keu->tipe = 1; 
			    				$detail_keu->user_id = $val; 
				    			$cek->detailuser()->save($detail_keu);
			    			}
		    			}
		    			if(!empty($value['sistem'])){
			    			foreach ($value['sistem'] as $key => $va) {
			    				$detail_sis = new RencanaAuditDetailUser; 
			    				$detail_sis->tipe = 2; 
			    				$detail_sis->user_id = $va; 
				    			$cek->detailuser()->save($detail_sis);
			    			}
		    			}
	    			}
    			}
    			$edisi = Edisi::where('status', 1)->first();
    			$logs_id = $edisi->logs()->last()->id;
    			$details = RencanaAuditDetail::where('rencana_id', $cek->rencana_id)->get();
    			foreach ($details as $key => $vil) {
    				$cek_details = RencanaAuditDetail::find($vil->id);
    				$cek_details->edisi_id = $logs_id;
	    			$cek_details->save();
    			}
    		}
    		if(!empty($detail)){
	    		$user = auth()->user();
		    	$log = new SessionLog;
			    $log->modul = 'Kegiatan Audit Rencana';
			    $log->sub = 'Audit Reguler';
			    $log->aktivitas = 'Mengubah Data Audit Reguler Tahun '.$detail->rencanaaudit->tahun;
			    $log->user_id = $user->id;
			    $log->save();
    		}
    	}
    }
    	
    // insert code here
}
