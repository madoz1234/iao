<?php
namespace App\Models\KegiatanAudit\Rencana;

use App\Models\Auths\User;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\Model;

class RencanaAuditDetailUser extends Model
{
    /* default */
    protected $table 		= 'trans_rencana_audit_detail_user';
    protected $fillable 	= ['detail_id','user_id','tipe'];

    /* data ke log */
    protected $log_table    = 'log_trans_rencana_audit_detail_user';
    protected $log_table_fk = 'ref_id';

    /* relation */
    public function detail(){
        return $this->belongsTo(RencanaAuditDetail::class, 'rencana_id');
    }
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
