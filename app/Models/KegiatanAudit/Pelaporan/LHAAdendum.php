<?php
namespace App\Models\KegiatanAudit\Pelaporan;

use App\Models\Model;
use App\Models\Attachments;
use App\Models\Files;
use App\Models\KegiatanAudit\Pelaporan\LHA;

class LHAAdendum extends Model
{
    /* default */
    protected $table 		= 'trans_lha_adendum';
    protected $fillable 	= ['lha_id','no_kontrak','nilai','tgl_awal','tgl_akhir'];

    /* data ke log */
    protected $log_table    = 'log_trans_lha_adendum';
    protected $log_table_fk = 'ref_id';
    /* relation */

    public function lha(){
        return $this->belongsTo(LHA::class, 'lha_id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
