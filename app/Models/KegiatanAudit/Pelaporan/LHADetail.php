<?php
namespace App\Models\KegiatanAudit\Pelaporan;

use App\Models\Model;
use App\Models\Attachments;
use App\Models\Files;
use App\Models\KegiatanAudit\Pelaporan\LHA;

class LHADetail extends Model
{
    /* default */
    protected $table 		= 'trans_lha_detail';
    protected $fillable 	= ['lha_id','tipe','keterangan'];

    /* data ke log */
    protected $log_table    = 'log_trans_lha_detail';
    protected $log_table_fk = 'ref_id';
    /* relation */

    public function lha(){
        return $this->belongsTo(LHA::class, 'lha_id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
