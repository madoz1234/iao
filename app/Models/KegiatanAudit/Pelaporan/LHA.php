<?php
namespace App\Models\KegiatanAudit\Pelaporan;

use App\Models\Attachments;
use App\Models\Files;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka;
use App\Models\KegiatanAudit\Pelaporan\LHADetail;
use App\Models\KegiatanAudit\Pelaporan\LHAAdendum;
use App\Models\KegiatanAudit\Pelaporan\LHAKesimpulan;
use App\Models\KegiatanAudit\Pelaporan\LHALingkup;
use App\Models\Model;

class LHA extends Model
{
    /* default */
    protected $table        = 'trans_lha';
    protected $fillable     = ['draft_id','tanggal','memo','status','ket_svp','ket_dirut','pemilik','sumber','sifat','nomor_spmk','nilai_kontrak','tanggal_kontrak','tanggal_akhir_kontrak','waktu_pelaksanaaan','waktu_pemeliharaan','pembayaran','konsultan_perencana','konsultan_pengawas','bkpu','progress','pu','cash','piutang','tagihan','tgl_bkpu','tgl_progress','tgl_pu','tgl_cash','tgl_piutang','tgl_tagihan'];

    /* data ke log */
    protected $log_table    = 'log_trans_lha';
    protected $log_table_fk = 'ref_id';
    /* relation */

    public function draftkka(){
        return $this->belongsTo(DraftKka::class, 'draft_id');
    }

    public function detaillha(){
        return $this->hasMany(LHADetail::class, 'lha_id' , 'id');
    }

    public function detailadendum(){
        return $this->hasMany(LHAAdendum::class, 'lha_id' , 'id');
    }

    public function detailkesimpulan(){
        return $this->hasMany(LHAKesimpulan::class, 'lha_id' , 'id');
    }

    public function detaillingkup(){
        return $this->hasMany(LHALingkup::class, 'lha_id' , 'id');
    }

    public function filesMorphClass()
    {
        return 'lha';
    }

    public function attachments()
    {
        return $this->morphMany(Attachments::class, 'target');
    }

    public function files()
    {
        return $this->morphMany(Files::class, 'target');
    }

    public function saveDetail($detail)
    {
        if($detail)
        {
            foreach ($detail as $key => $value) {
                foreach ($value['data'] as $kiy => $vil) {
                    $lhadetail = new LHADetail;
                    $lhadetail->tipe = $key;
                    $lhadetail->keterangan = $vil['keterangan'];
                    $this->detaillha()->save($lhadetail);
                }
            }
        }
    }

    public function saveKesimpulan($detail)
    {
        if($detail)
        {
            foreach ($detail as $kiy => $vil) {
                $lhakesimpulan = new LHAKesimpulan;
                $lhakesimpulan->kesimpulan = $vil['kesimpulan'];
                $this->detailkesimpulan()->save($lhakesimpulan);
            }
        }
    }

    public function saveAdendum($adendum)
    {
        if($adendum)
        {
            foreach ($adendum as $key => $value) {
                    $lhaadendum 			= new LHAAdendum;
                    $lhaadendum->no_kontrak = $value['no_kontrak'];
                    $lhaadendum->nilai 		= $value['nilai'];
                    $lhaadendum->tgl_awal 	= $value['tgl_awal'];
                    $lhaadendum->tgl_akhir 	= $value['tgl_akhir'];
                    $this->detailadendum()->save($lhaadendum);
            }
        }
    }

    public function saveLingkup($lingkup)
    {
        if($lingkup)
        {
            foreach ($lingkup as $key => $value) {
                    $lhalingkup 			= new LHALingkup;
                    $lhalingkup->uraian 	= $value['uraian'];
                    $lhalingkup->kontrak 	= $value['kontrak'];
                    $lhalingkup->rencana 	= $value['rencana'];
                    $lhalingkup->realisasi 	= $value['realisasi'];
                    $this->detaillingkup()->save($lhalingkup);
            }
        }
    }

    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
