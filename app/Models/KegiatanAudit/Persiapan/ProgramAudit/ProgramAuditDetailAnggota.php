<?php
namespace App\Models\KegiatanAudit\Persiapan\ProgramAudit;

use App\Models\Model;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditDetail;
use App\Models\Auths\User;

class ProgramAuditDetailAnggota extends Model
{
    /* default */
    protected $table 		= 'trans_program_audit_detail_anggota';
    protected $fillable 	= ['program_id','user_id'];

    /* data ke log */
    protected $log_table    = 'log_trans_program_audit_detail_anggota';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function programdetail(){
        return $this->belongsTo(ProgramAuditDetail::class, 'program_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
