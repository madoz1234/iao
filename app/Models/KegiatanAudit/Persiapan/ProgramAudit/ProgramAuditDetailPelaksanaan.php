<?php
namespace App\Models\KegiatanAudit\Persiapan\ProgramAudit;

use App\Models\Model;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditDetail;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditDetailPelaksanaanDetail;

class ProgramAuditDetailPelaksanaan extends Model
{
    /* default */
    protected $table 		= 'trans_program_audit_detail_pel';
    protected $fillable 	= ['program_id','tgl','tgl_id'];

    /* data ke log */
    protected $log_table    = 'log_trans_program_audit_detail_pel';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function programdetail(){
        return $this->belongsTo(ProgramAuditDetail::class, 'program_detail_id');
    }

    public function detailpelaksanaan(){
        return $this->hasMany(ProgramAuditDetailPelaksanaanDetail::class, 'pelaksanaan_detail_id' , 'id');
    }

    public function saveDetail($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
				$data 					= new ProgramAuditDetailPelaksanaanDetail;
	    		$data->mulai 			= $value['mulai'];
	    		$data->selesai 			= $value['selesai'];
	    		$data->keterangan 		= $value['item'];
	            $this->detailpelaksanaan()->save($data);
    		}
    	}
    }
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
