<?php
namespace App\Models\KegiatanAudit\Persiapan\ProgramAudit;

use App\Models\Model;
use App\Models\Master\FokusAudit;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditDetail;

class ProgramAuditDetailKerja extends Model
{
    /* default */
    protected $table 		= 'trans_program_audit_detail_kerja';
    protected $fillable 	= ['program_id','fokus_audit_id','bidang','rencana','realisasi','keterangan'];

    /* data ke log */
    protected $log_table    = 'log_trans_program_audit_detail_kerja';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function programdetail(){
        return $this->belongsTo(ProgramAuditDetail::class, 'program_id');
    }

    public function fokusaudit(){
        return $this->belongsTo(FokusAudit::class, 'fokus_audit_id');
    }
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
