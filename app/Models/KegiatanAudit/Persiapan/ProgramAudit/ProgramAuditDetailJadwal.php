<?php
namespace App\Models\KegiatanAudit\Persiapan\ProgramAudit;

use App\Models\Model;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditDetail;

class ProgramAuditDetailJadwal extends Model
{
    /* default */
    protected $table 		= 'trans_program_audit_detail_jadwal';
    protected $fillable 	= ['program_id','keterangan'];

    /* data ke log */
    protected $log_table    = 'log_trans_program_audit_detail_jadwal';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function programdetail(){
        return $this->belongsTo(ProgramAuditDetail::class, 'program_id');
    }
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
