<?php
namespace App\Models\KegiatanAudit\Persiapan\ProgramAudit;

use App\Models\Model;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditDetailAnggota;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditDetailJadwal;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditDetailKerja;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditDetailPelaksanaan;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditDetailPelaksanaanDetail;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditDetailPenyelesaian;
// use App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumen;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit;
use App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka;
use App\Models\Auths\User;

class ProgramAudit extends Model
{
    /* default */
    protected $table 		= 'trans_program_audit';
    protected $fillable 	= ['penugasan_id','user_id','ruang_lingkup','sasaran'];

    /* data ke log */
    protected $log_table    = 'log_trans_program_audit';
    protected $log_table_fk = 'ref_id';
    /* relation */

    public function penugasanaudit(){
        return $this->belongsTo(PenugasanAudit::class, 'penugasan_id');
    }

    public function opening(){
        return $this->hasOne(OpeningMeeting::class, 'program_id');
    }

    public function kka(){
        return $this->hasOne(DraftKka::class, 'program_id');
    }

    public function detailanggota(){
        return $this->hasMany(ProgramAuditDetailAnggota::class, 'program_id' , 'id');
    }

    public function detailjadwal(){
        return $this->hasMany(ProgramAuditDetailJadwal::class, 'program_id' , 'id');
    }

    public function detailkerja(){
        return $this->hasMany(ProgramAuditDetailKerja::class, 'program_id' , 'id');
    }

    public function detailpelaksanaan(){
        return $this->hasMany(ProgramAuditDetailPelaksanaan::class, 'program_id' , 'id');
    }

    public function detailselesai(){
        return $this->hasMany(ProgramAuditDetailPenyelesaian::class, 'program_id' , 'id');
    }
    
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function saveAnggota($operasional, $keuangan, $sistem, $ketua)
    {	
    	$data_ketua 					= new ProgramAuditDetailAnggota;
		$data_ketua->fungsi 			= 0;
		$data_ketua->user_id 			= $ketua;
        $this->detailanggota()->save($data_ketua);

    	if($operasional)
    	{
    		foreach ($operasional as $key => $value) {
    			if($value){
					$data_operasional 					= new ProgramAuditDetailAnggota;
		    		$data_operasional->fungsi 			= 1;
		    		$data_operasional->user_id 			= $value;
		            $this->detailanggota()->save($data_operasional);
    			}
    		}
    	}

    	if($keuangan)
    	{
    		foreach ($keuangan as $key => $val) {
    			if($val){
					$data_keuangan 					= new ProgramAuditDetailAnggota;
		    		$data_keuangan->fungsi 			= 2;
		    		$data_keuangan->user_id 		= $val;
		            $this->detailanggota()->save($data_keuangan);
    			}
    		}
    	}

    	if($sistem)
    	{
    		foreach ($sistem as $key => $vil) {
    			if($vil){
					$data_sistem 					= new ProgramAuditDetailAnggota;
		    		$data_sistem->fungsi 			= 3;
		    		$data_sistem->user_id 			= $vil;
		            $this->detailanggota()->save($data_sistem);
    			}
    		}
    	}
    }

    public function saveAnggotas($operasional, $keuangan, $sistem)
    {	
    	if($operasional)
    	{
    		foreach ($operasional as $key => $value) {
    			if($value){
					$data_operasional 					= new ProgramAuditDetailAnggota;
		    		$data_operasional->fungsi 			= 1;
		    		$data_operasional->user_id 			= $value;
		            $this->detailanggota()->save($data_operasional);
    			}
    		}
    	}

    	if($keuangan)
    	{
    		foreach ($keuangan as $key => $val) {
    			if($val){
					$data_keuangan 					= new ProgramAuditDetailAnggota;
		    		$data_keuangan->fungsi 			= 2;
		    		$data_keuangan->user_id 		= $val;
		            $this->detailanggota()->save($data_keuangan);
    			}
    		}
    	}

    	if($sistem)
    	{
    		foreach ($sistem as $key => $vil) {
    			if($vil){
					$data_sistem 					= new ProgramAuditDetailAnggota;
		    		$data_sistem->fungsi 			= 3;
		    		$data_sistem->user_id 			= $vil;
		            $this->detailanggota()->save($data_sistem);
    			}
    		}
    	}
    }

    public function saveDetail($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
    			if($key === 1000){
	    			foreach ($value['persiapan'] as $key => $val) {
	    				if($val['persiapan']){
				    		$program_detail_persiapan 						= new ProgramAuditDetailJadwal;
							$program_detail_persiapan->keterangan 			= $val['persiapan'];
							$this->detailjadwal()->save($program_detail_persiapan);
	    				}
	    			}

	    			foreach ($value['penyelesaian'] as $key => $vil) {
		    			if($vil['mulai']){
				    		$program_detail_penyelesaian 						= new ProgramAuditDetailPenyelesaian;
							$program_detail_penyelesaian->tgl_mulai 			= $vil['mulai'];
							$program_detail_penyelesaian->tgl_selesai 			= $vil['selesai'];
							$program_detail_penyelesaian->keterangan 			= $vil['item'];
							$this->detailselesai()->save($program_detail_penyelesaian);
		    			}
		    		}
    			}else{
	    			foreach ($value['pelaksanaan'] as $key => $val_pelaksanaan) {
	    				if($val_pelaksanaan['tanggal']){
				    		$program_detail_pelaksanaan 						= new ProgramAuditDetailPelaksanaan;
				    		$program_detail_pelaksanaan->tgl_id 				= $val_pelaksanaan['tgl_id'];
							$program_detail_pelaksanaan->tgl 					= $val_pelaksanaan['tanggal'];
							$this->detailpelaksanaan()->save($program_detail_pelaksanaan);
							foreach ($val_pelaksanaan['detail'] as $key => $val_detail) {
								if($val_detail['mulai']){
									$data 							= new ProgramAuditDetailPelaksanaanDetail;
						    		$data->pelaksanaan_detail_id 	= $program_detail_pelaksanaan->id;
						    		$data->mulai 					= $val_detail['mulai'];
						    		$data->selesai 					= $val_detail['selesai'];
						    		$data->keterangan 				= $val_detail['item'];
						            $data->save();
								}
				    		}
	    				}
		    		}
    			}
    		}
    	}
    }

    public function saveDetailFokus($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
    			if($key === 1000){
		    		foreach ($value['fokus_audit'] as $key => $fokus) {
						foreach ($fokus['detail'] as $kiy => $vil_fokus) {
							if($vil_fokus['bidang']){
								if($key == 0){
									$bidang = 1;
								}elseif($key == 1){
									$bidang = 2;
								}else{
									$bidang = 3;
								}
								if($vil_fokus['bidang']){
									$program_detail_fokus_audit 						= new ProgramAuditDetailKerja;
				    				$program_detail_fokus_audit->fokus_audit_id 		= $vil_fokus['bidang'];
				    				$program_detail_fokus_audit->bidang 				= $bidang;
				    				if(!empty($vil_fokus['rencana'])){
					    				$program_detail_fokus_audit->rencana 			= $vil_fokus['rencana'];
				    				}
				    				$program_detail_fokus_audit->realisasi 				= $vil_fokus['realisasi'];
				    				if(!empty($vil_fokus['keterangan'])){
					    				$program_detail_fokus_audit->keterangan 		= $vil_fokus['keterangan'];
				    				}
				    				$this->detailkerja()->save($program_detail_fokus_audit);
								}
							}
		    			}
					}
    			}
    		}
    	}
    }

    public function ubahRealisasi($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
    			if($key === 1000){
		    		foreach ($value['fokus_audit'] as $key => $fokus) {
						foreach ($fokus['detail'] as $kiy => $vil_fokus) {
							if(!empty($vil_fokus['id'])){
								$program_detail_fokus_audit 						= ProgramAuditDetailKerja::find($vil_fokus['id']);
								if(!empty($vil_fokus['realisasi'])){
				    				$program_detail_fokus_audit->realisasi 				= $vil_fokus['realisasi'];
								}
			    				$this->detailkerja()->save($program_detail_fokus_audit);
							}
		    			}
					}
    			}
    		}
    	}
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
