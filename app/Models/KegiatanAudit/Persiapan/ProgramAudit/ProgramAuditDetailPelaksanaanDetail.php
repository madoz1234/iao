<?php
namespace App\Models\KegiatanAudit\Persiapan\ProgramAudit;

use App\Models\Model;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditDetailPelaksanaan;

class ProgramAuditDetailPelaksanaanDetail extends Model
{
    /* default */
    protected $table 		= 'trans_program_audit_detail_pel_d';
    protected $fillable 	= ['pelaksanaan_detail_id','mulai','selesai','keterangan'];

    /* data ke log */
    protected $log_table    = 'log_trans_program_audit_detail_pel_d';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function pelaksanaan(){
        return $this->belongsTo(ProgramAuditDetailPelaksanaan::class, 'pelaksanaan_detail_id');
    }
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
