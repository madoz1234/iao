<?php
namespace App\Models\KegiatanAudit\Persiapan\TinjauanDokumen;

use App\Models\Model;
use App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumen;
use App\Models\Master\Dokumen;
use App\Models\Auths\User;

class TinjauanDokumenPenerima extends Model
{
    /* default */
    protected $table 		= 'trans_tinjauan_dokumen_penerima';
    protected $fillable 	= ['tinjauan_id','user_id'];

    /* data ke log */
    protected $log_table    = 'log_trans_tinjauan_dokumen_penerima';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function tinjauandokumen(){
        return $this->belongsTo(TinjauanDokumen::class, 'tinjauan_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
