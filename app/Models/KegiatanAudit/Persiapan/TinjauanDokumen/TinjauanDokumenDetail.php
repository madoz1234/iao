<?php
namespace App\Models\KegiatanAudit\Persiapan\TinjauanDokumen;

use App\Models\Model;
use App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumen;
use App\Models\Master\Dokumen;
use App\Models\Attachments;
use App\Models\Files;

class TinjauanDokumenDetail extends Model
{
    /* default */
    protected $table 		= 'trans_tinjauan_dokumen_detail';
    protected $fillable 	= ['tinjauan_id','dokumen_id','status'];

    /* data ke log */
    protected $log_table    = 'log_trans_tinjauan_dokumen_detail';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function tinjauandokumen(){
        return $this->belongsTo(TinjauanDokumen::class, 'tinjauan_id');
    }

    public function dokumen(){
        return $this->belongsTo(Dokumen::class, 'dokumen_id');
    }

    public function filesMorphClass()
    {
        return 'dokumen-detail';
    }

    public function attachments()
    {
        return $this->morphMany(Attachments::class, 'target');
    }

    public function files()
    {
        return $this->morphMany(Files::class, 'target');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
