<?php
namespace App\Models\KegiatanAudit\Persiapan\TinjauanDokumen;

use App\Models\Model;
use App\Models\Attachments;
use App\Models\Files;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit;
use App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumenEmail;
use App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumenDetail;
use App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumenPenerima;
use App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumenTembusan;
use App\Models\Auths\User;

class TinjauanDokumen extends Model
{
    /* default */
    protected $table 		= 'trans_tinjauan_dokumen';
    protected $fillable 	= ['penugasan_id','nomor','tempat','tanggal','judul','email','dateline','status','group'];

    /* data ke log */
    protected $log_table    = 'log_trans_tinjauan_dokumen';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function penugasanaudit(){
        return $this->belongsTo(PenugasanAudit::class, 'penugasan_id');
    }

    public function programaudit(){
        return $this->hasOne(ProgramAudit::class, 'tinjauan_id');
    }

    public function detaildokumen(){
        return $this->hasMany(TinjauanDokumenDetail::class, 'tinjauan_id' , 'id');
    }

    public function detailemail(){
        return $this->hasMany(TinjauanDokumenEmail::class, 'tinjauan_id' , 'id');
    }

    public function detailpenerima(){
        return $this->hasMany(TinjauanDokumenPenerima::class, 'tinjauan_id' , 'id');
    }

    public function detailtembusan(){
        return $this->hasMany(TinjauanDokumenTembusan::class, 'tinjauan_id' , 'id');
    }

    public function filesMorphClass()
    {
        return 'tinjauan-dokumen';
    }

    public function attachments()
    {
        return $this->morphMany(Attachments::class, 'target');
    }

    public function files()
    {
        return $this->morphMany(Files::class, 'target');
    }


    public function saveDetailDokumen($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
    			if($value['dokumen']){
	    			$dokumen = new TinjauanDokumenDetail;
	    			$dokumen->dokumen_id = $value['dokumen'];
	    			if(!isset($value['status'])){
	    				$dokumen->status = 0;
	    			}else{
		    			$dokumen->status = 1;
	    			}
	    			$this->detaildokumen()->save($dokumen);
    			}
    		}
    	}
    }

    public function saveDetailEmail($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
    			if($value['email']){
	    			$dataemail = new TinjauanDokumenEmail;
	    			$dataemail->email = $value['email'];
	    			$this->detailemail()->save($dataemail);
    			}
    		}
    	}
    }

    public function saveDetailPenerima($details)
    {
    	if($details)
    	{
    		foreach ($details as $key => $value) {
    			if($value['users']){
	    			$cari = User::find($value['users']);
	    			$user = new TinjauanDokumenPenerima;
	    			$user->user_id = $value['users'];
	    			$user->nama = $cari->name;
	    			$user->tipe = 1;
	    			$this->detailpenerima()->save($user);
    			}
    		}
    	}
    }

    public function saveDetailTembusan($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
    			if($value['tembusan']){
	    			$cari = User::find($value['tembusan']);
	    			$tembusan = new TinjauanDokumenTembusan;
	    			$tembusan->user_id 	= $value['tembusan'];
	    			$tembusan->tembusan = $cari->name;
	    			$tembusan->tipe 	= 1;
	    			$this->detailtembusan()->save($tembusan);
    			}
    		}
    	}
    }

    public function uploadDetail($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
    			$dokumen = TinjauanDokumenDetail::find($key);
    			$dokumen->uploadFileDokumen($value);
    		}
    	}
    }

    public function fileOther()
    {
        $show = [];
        foreach ($this->detaildokumen as $key => $value) {
        	$cari =Files::where('target_id', $value->id)->where('target_type', 'dokumen-detail')->first();
        	if($cari){
	            $url = $cari->url;
	            $show[] = asset('storage/'.$url);
        	}
        }
        return json_encode($show);
    }
    
    public function fileOtherJson()
    {
        $arr = [];
        foreach ($this->detaildokumen as $key => $value) {
        	$cari =Files::where('target_id', $value->id)->where('target_type', 'dokumen-detail')->first();
        	if($cari){
	        	$url = $cari->url;
	            $url = asset('storage/'.$url);
	            $type = $cari->type;
	            $filename = $cari->filename;
	            if($type == 'png' OR $type == 'jpg' OR $type == 'jpeg' OR $type == 'gif' OR $type == 'ico' OR $type == 'apng' OR $type == 'pdf'){
	                $arr[]= [
	                    'caption' => $filename,
	                    'downloadUrl' => $url,
	                    'size' => 930321,
	                    'width' => "120px",
	                    'key' => $key+1
	                ];
	            }else{
	                $arr[]= [
	                	'caption' => $filename,
	                    'type' => 'pdf',
	                    'caption' => $filename,
	                    'downloadUrl' => $url,
	                    'size' => 930321,
	                    'width' => "120px",
	                    'key' => $key+1
	                ];
	            }
        	}
        }
        return json_encode($arr);
    }


    public function cekOther()
    {
        $show = [];
        foreach ($this->detaildokumen as $key => $value) {
        	$cari =Files::where('target_id', $value->id)->where('target_type', 'dokumen-detail')->first();
        	if($cari){
	            $url = $cari->url;
	            $show[] = asset('storage/'.$url);
        	}
        }
        return json_encode($show);
    }
    
    public function cekOtherJson()
    {
        $arr = [];
        foreach ($this->detaildokumen as $key => $value) {
        	$cari =Files::where('target_id', $value->id)->where('target_type', 'dokumen-detail')->first();
        	if($cari){
	        	$url = $cari->url;
	            $url = asset('storage/'.$url);
	            $type = $cari->type;
	            $filename = $cari->filename;
	            if($type == 'png' OR $type == 'jpg' OR $type == 'jpeg' OR $type == 'gif' OR $type == 'ico' OR $type == 'apng' OR $type == 'pdf'){
	                $arr[]= [
	                    'caption' => $filename,
	                    'downloadUrl' => $url,
	                    'size' => 930321,
	                    'width' => "120px",
	                    'key' => $key+1
	                ];
	            }else{
	                $arr[]= [
	                	'caption' => $filename,
	                    'type' => 'pdf',
	                    'caption' => $filename,
	                    'downloadUrl' => $url,
	                    'size' => 930321,
	                    'width' => "120px",
	                    'key' => $key+1
	                ];
	            }
        	}
        }
        return json_encode($arr);
    }

    public function cariFile()
    {
        $i=0;
        foreach ($this->detaildokumen as $key => $value) {
        	$cari =Files::where('target_id', $value->id)->where('target_type', 'dokumen-detail')->first();
        	if($cari){
	        	$i+=1;
        	}
        }
        return $i;
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
