<?php
namespace App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit;

use App\Models\Model;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAuditDetailJadwal;

class PenugasanAuditDetailJadwalDetail extends Model
{
    /* default */
    protected $table 		= 'trans_penugasan_audit_detail_jadwal_d';
    protected $fillable 	= ['jadwal_detail_id','mulai','selesai','keterangan'];

    /* data ke log */
    protected $log_table    = 'log_trans_penugasan_audit_detail_jadwal_d';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // public function programauditdetail(){
    //     return $this->belongsTo(ProgramAuditDetail::class, 'program_audit_detail_id');
    // }
    public function detailjadwal(){
        return $this->belongsTo(PenugasanAuditDetailJadwal::class, 'jadwal_detail_id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
