<?php
namespace App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit;

use App\Models\Model;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAuditDetailJadwalDetail;

class PenugasanAuditDetailJadwal extends Model
{
    /* default */
    protected $table 		= 'trans_penugasan_audit_detail_jadwal';
    protected $fillable 	= ['penugasan_id','tgl_id','tgl'];

    /* data ke log */
    protected $log_table    = 'log_trans_penugasan_audit_detail_jadwal';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // public function programauditdetail(){
    //     return $this->belongsTo(ProgramAuditDetail::class, 'program_audit_detail_id');
    // }

    public function penugasan(){
        return $this->belongsTo(PenugasanAudit::class, 'penugasan_id');
    }
    public function detail(){
        return $this->hasMany(PenugasanAuditDetailJadwalDetail::class, 'jadwal_detail_id' , 'id');
    }

    public function rencanadetail(){
        return $this->belongsTo(RencanaAuditDetail::class, 'tgl_id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
