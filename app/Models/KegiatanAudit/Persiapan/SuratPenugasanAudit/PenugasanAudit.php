<?php
namespace App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit;

use App\Models\Model;
use App\Models\Attachments;
use App\Models\Files;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAuditDetailJadwal;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAuditDetailJadwalDetail;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAuditDetailAnggota;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAuditDetailTembusan;
use App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumen;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit;
use App\Models\Auths\User;
use App\Models\Survey\SurveyJawab;

class PenugasanAudit extends Model
{
    /* default */
    protected $table 		= 'trans_penugasan_audit';
    protected $fillable 	= ['rencana_id','no_surat','tgl_surat','status','bukti','filename','ket_svp','user_id','group','status_audit'];

    /* data ke log */
    protected $log_table    = 'log_trans_penugasan_audit';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // public function programauditdetail(){
    //     return $this->belongsTo(ProgramAuditDetail::class, 'program_audit_detail_id');
    // }
    public function tinjauandokumen(){
        return $this->hasOne(TinjauanDokumen::class, 'penugasan_id');
    }

    public function programaudit(){
        return $this->hasOne(ProgramAudit::class, 'penugasan_id');
    }

    public function rencanadetail(){
        return $this->belongsTo(RencanaAuditDetail::class, 'rencana_id');
    }

    public function anggota(){
        return $this->hasMany(PenugasanAuditDetailAnggota::class, 'penugasan_id' , 'id');
    }

    public function jadwal(){
        return $this->hasMany(PenugasanAuditDetailJadwal::class, 'penugasan_id' , 'id');
    }

    public function tembusans(){
        return $this->hasMany(PenugasanAuditDetailTembusan::class, 'penugasan_id' , 'id');
    }

    //SEMENTARA
    public function surveiJawab()
    {
        return $this->hasMany(SurveyJawab::class, 'lha_id', 'id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function filesMorphClass()
    {
        return 'surat-penugasan-audit';
    }

    public function attachments()
    {
        return $this->morphMany(Attachments::class, 'target');
    }

    public function files()
    {
        return $this->morphMany(Files::class, 'target');
    }

    public function fileSurat()
    {
        return asset('storage/'.$this->bukti);
    }

    public function fileFinal()
    {
    	$cari =Files::where('target_id', $this->id)->where('target_type', 'surat-penugasan-audit')->first();
        return asset('storage/'.$cari->url);
    }

    public function fileFinalName()
    {
    	$cari =Files::where('target_id', $this->id)->where('target_type', 'surat-penugasan-audit')->first();
        return $cari->filename;
    }

    public function saveDetail($detail)
    {
    	if($detail){
    		foreach ($detail as $key => $value) {
    			$data_ketua 					= new PenugasanAuditDetailAnggota;
	    		$data_ketua->fungsi 			= 0;
	    		$data_ketua->data_id 			= $key;
	    		$data_ketua->user_id 			= $value['ketua'];
	            $this->anggota()->save($data_ketua);

	            foreach ($value['operasional'] as $kuy => $val_operasional) {
					$data_operasional 					= new PenugasanAuditDetailAnggota;
		    		$data_operasional->fungsi 			= 1;
		    		$data_operasional->data_id 			= $key;
		    		$data_operasional->user_id 			= $val_operasional;
		            $this->anggota()->save($data_operasional);
	    		}

	    		foreach ($value['keuangan'] as $kuy => $val_keuangan) {
					$data_keuangan 					= new PenugasanAuditDetailAnggota;
		    		$data_keuangan->fungsi 			= 2;
		    		$data_keuangan->data_id 		= $key;
		    		$data_keuangan->user_id 		= $val_keuangan;
		            $this->anggota()->save($data_keuangan);
	    		}

	    		foreach ($value['sistem'] as $kuy => $val_sistem) {
					$data_sistem 					= new PenugasanAuditDetailAnggota;
		    		$data_sistem->fungsi 			= 3;
		    		$data_sistem->data_id 			= $key;
		    		$data_sistem->user_id 			= $val_sistem;
		            $this->anggota()->save($data_sistem);
	    		}

	    		foreach ($value['pelaksanaan'] as $kiy => $detil) {
	    			$program_detail_pelaksanaan 						= new PenugasanAuditDetailJadwal;
					$program_detail_pelaksanaan->tgl_id 				= $detil['tgl_id'];
					$program_detail_pelaksanaan->tgl 					= $detil['tanggal'];
					$this->jadwal()->save($program_detail_pelaksanaan);
					foreach ($detil['detail'] as $key => $val_detail) {
						$data 							= new PenugasanAuditDetailJadwalDetail;
			    		$data->jadwal_detail_id 		= $program_detail_pelaksanaan->id;
			    		$data->mulai 					= $val_detail['mulai'];
			    		$data->selesai 					= $val_detail['selesai'];
			    		$data->keterangan 				= $val_detail['item'];
			            $data->save();
		    		}
    			}
    		}
    	}
    }

    public function saveDetailTembusan($details)
    {
    	if($details)
    	{
    		foreach ($details as $key => $val) {
    			if($val['tembusan']){
	    			$data 							= new PenugasanAuditDetailTembusan;
		    		$data->user_id 					= $val['tembusan'];
	    			$this->tembusans()->save($data);
    			}
    		}
    	}
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
