<?php
namespace App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit;

use App\Models\Model;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit;
use App\Models\Auths\User;

class PenugasanAuditDetailTembusan extends Model
{
    /* default */
    protected $table 		= 'trans_penugasan_audit_detail_tembusan';
    protected $fillable 	= ['penugasan_id','user_id'];

    /* data ke log */
    protected $log_table    = 'log_trans_penugasan_audit_detail_tembusan';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // public function programauditdetail(){
    //     return $this->belongsTo(ProgramAuditDetail::class, 'program_audit_detail_id');
    // }
    public function penugasan(){
        return $this->belongsTo(PenugasanAudit::class, 'penugasan_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
