<?php
namespace App\Models\KegiatanAudit\Pelaksanaan\FinalKka;

use App\Models\Model;
use App\Models\KegiatanAudit\Pelaksanaan\ClosingMeeting\ClosingMeeting;
use App\Models\KegiatanAudit\Pelaporan\LHA;

class FinalKka extends Model
{
    /* default */
    protected $table 		= 'trans_final_kka';
    protected $fillable 	= ['close_id','status'];

    /* data ke log */
    protected $log_table    = 'log_trans_final_kka';
    protected $log_table_fk = 'ref_id';
    /* relation */

    public function closing(){
        return $this->belongsTo(ClosingMeeting::class, 'close_id');
    }

    public function lha(){
        return $this->hasOne(LHA::class, 'final_id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
