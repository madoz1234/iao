<?php
namespace App\Models\KegiatanAudit\Pelaksanaan\DraftKka;

use App\Models\Model;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaDetail;
use App\Models\KegiatanAudit\TindakLanjut\RegisterTLDetail;

class DraftKkaDetailRekomendasi extends Model
{
    /* default */
    protected $table 		= 'trans_draft_kka_detail_rekomendasi';
    protected $fillable 	= ['kka_detail_id','rekomendasi'];

    /* data ke log */
    protected $log_table    = 'log_trans_draft_kka_detail_rekomendasi';
    protected $log_table_fk = 'ref_id';
    /* relation */

    public function draftdetail(){
        return $this->belongsTo(DraftKkaDetail::class, 'kka_detail_id');
    }

    public function register(){
        return $this->hasOne(RegisterTLDetail::class, 'rekomendasi_id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
