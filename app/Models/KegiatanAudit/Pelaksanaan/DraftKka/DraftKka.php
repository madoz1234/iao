<?php
namespace App\Models\KegiatanAudit\Pelaksanaan\DraftKka;

use App\Models\Model;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaDetail;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaDetailRekomendasi;
use App\Models\KegiatanAudit\Pelaksanaan\ClosingMeeting\ClosingMeeting;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit;
use App\Models\KegiatanAudit\Pelaporan\LHA;

class DraftKka extends Model
{
    /* default */
    protected $table 		= 'trans_draft_kka';
    protected $fillable 	= ['program_id','status'];

    /* data ke log */
    protected $log_table    = 'log_trans_draft_kka';
    protected $log_table_fk = 'ref_id';
    /* relation */

    public function program(){
        return $this->belongsTo(ProgramAudit::class, 'program_id');
    }

    public function detaildraft(){
        return $this->hasMany(DraftKkaDetail::class, 'draft_id' , 'id');
    }

    public function lha(){
        return $this->hasOne(LHA::class, 'draft_id');
    }

    public function saveDetail($fokus_audit)
    {
    	if($fokus_audit)
    	{
    		foreach ($fokus_audit as $key => $value) {
				foreach ($value['detail'] as $kiy => $val) {
					$daraft_detail = new DraftKkaDetail;
					$daraft_detail->tipe = $val['tipe'];
					$daraft_detail->standarisasi_id = $val['standarisasi_id'];
					$daraft_detail->catatan_kondisi = $val['catatan_kondisi'];
					$daraft_detail->sebab = $val['sebab'];
					$daraft_detail->risiko = $val['risiko'];
					$daraft_detail->kategori_id = $val['kategori_id'];
					$daraft_detail->user_id = $val['user_id'];
					$daraft_detail->tanggapan = $val['tanggapan'];
					if($val['tanggapan'] == 0){
						$daraft_detail->tgl = $val['tanggal'];
					}else{
						$daraft_detail->catatan = $val['catatan'];
					}
					$data = $this->detaildraft()->save($daraft_detail);
					$data->uploadFile($val['lampiran']);
		            $data->files->each(function ($d) {
		                $d->dms_update = 1;
		                $d->save();
		            });
					foreach ($val['detail_rekomen'] as $key => $vil) {
						$daraft_detail_rekomendasi = new DraftKkaDetailRekomendasi;
						$daraft_detail_rekomendasi->rekomendasi = $vil['rekomendasi'];
						$data->rekomendasi()->save($daraft_detail_rekomendasi);
					}
				}
    		}
    	}
    }

    public function updateDetail($fokus_audit, $exist)
    {
    	if($fokus_audit)
    	{
    		foreach ($fokus_audit as $key => $value) {
				foreach ($value['detail'] as $kiy => $val) {
	    			if(!empty($val['data_id'])){
	    				if($exist){
				    		$hapus_rekomendasi = DraftKkaDetailRekomendasi::whereNotIn('id', $exist)
				    													   ->where('kka_detail_id', $val['data_id'])
						                        						   ->delete();
				    	}
	    				$daraft_detail = DraftKkaDetail::find($val['data_id']);
						$daraft_detail->tipe = $val['tipe'];
						$daraft_detail->standarisasi_id = $val['standarisasi_id'];
						$daraft_detail->catatan_kondisi = $val['catatan_kondisi'];
						$daraft_detail->sebab = $val['sebab'];
						$daraft_detail->risiko = $val['risiko'];
						$daraft_detail->kategori_id = $val['kategori_id'];
            			$daraft_detail->user_id = $val['user_id'];
						$daraft_detail->tanggapan = $val['tanggapan'];
						if($val['tanggapan'] == 0){
							$daraft_detail->tgl = $val['tanggal'];
						}else{
							$daraft_detail->catatan = $val['catatan'];
						}
						$data = $this->detaildraft()->save($daraft_detail);
						if($val['lampiran']){
							$data->uploadFile($val['lampiran']);
						}
			            $data->files->each(function ($d) {
			                  $d->dms_update = 1;
			                  $d->save();
			            });
			            foreach($val['detail_rekomen'] as $key => $vil) {
			            	if(!empty($vil['id'])){
			            		$daraft_detail_rekomendasi = DraftKkaDetailRekomendasi::find($vil['id']);
								$daraft_detail_rekomendasi->rekomendasi = $vil['rekomendasi'];
								$data->rekomendasi()->save($daraft_detail_rekomendasi);
			            	}else{
								$daraft_detail_rekomendasi = new DraftKkaDetailRekomendasi;
								$daraft_detail_rekomendasi->rekomendasi = $vil['rekomendasi'];
								$data->rekomendasi()->save($daraft_detail_rekomendasi);
			            	}
						}
	    			}else{
						$daraft_detail = new DraftKkaDetail;
						$daraft_detail->tipe = $val['tipe'];
						$daraft_detail->standarisasi_id = $val['standarisasi_id'];
						$daraft_detail->catatan_kondisi = $val['catatan_kondisi'];
						$daraft_detail->sebab = $val['sebab'];
						$daraft_detail->risiko = $val['risiko'];
						$daraft_detail->kategori_id = $val['kategori_id'];
            			$daraft_detail->user_id = $val['user_id'];
						$daraft_detail->tanggapan = $val['tanggapan'];
						if($val['tanggapan'] == 0){
							$daraft_detail->tgl = $val['tanggal'];
						}else{
							$daraft_detail->catatan = $val['catatan'];
						}
						$data = $this->detaildraft()->save($daraft_detail);
						$data->uploadFile($val['lampiran']);
			            $data->files->each(function ($d) {
			                  $d->dms_update = 1;
			                  $d->save();
			            });
			            foreach($val['detail_rekomen'] as $key => $vil) {
							if(!empty($vil['id'])){
			            		$daraft_detail_rekomendasi = DraftKkaDetailRekomendasi::find($vil['id']);
								$daraft_detail_rekomendasi->rekomendasi = $vil['rekomendasi'];
								$data->rekomendasi()->save($daraft_detail_rekomendasi);
			            	}else{
								$daraft_detail_rekomendasi = new DraftKkaDetailRekomendasi;
								$daraft_detail_rekomendasi->rekomendasi = $vil['rekomendasi'];
								$data->rekomendasi()->save($daraft_detail_rekomendasi);
			            	}
						}
	    			}
				}
    		}
    	}
    }

    public function saveDetails($fokus_audit)
    {
    	if($fokus_audit)
    	{
    		foreach ($fokus_audit as $key => $value) {
				foreach ($value['detail'] as $kiy => $val) {
					$daraft_detail = DraftKkaDetail::find($val['data_id']);
					$daraft_detail->tanggapan = $val['tanggapan'];
					if($val['tanggapan'] == 0){
						$daraft_detail->tgl = $val['tanggal'];
						$daraft_detail->catatan = null;
					}else{
						$daraft_detail->tgl = null;
						$daraft_detail->catatan = $val['catatan'];
					}
					$this->detaildraft()->save($daraft_detail);
				}
    		}
    	}
    }
}
