<?php
namespace App\Models\KegiatanAudit\Pelaksanaan\DraftKka;

use App\Models\Model;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaDetailRekomendasi;
use App\Models\Master\KategoriTemuan;
use App\Models\KegiatanAudit\TindakLanjut\RegisterTLDetail;
// use App\Models\Master\KriteriaTemuanDetail;
use App\Models\Auths\User;
use App\Models\Attachments;
use App\Models\Files;

class DraftKkaDetail extends Model
{
    /* default */
    protected $table 		= 'trans_draft_kka_detail';
    protected $fillable 	= ['draft_id','tipe','kategori_id','kondisi','kriteria','sebab','risiko','user_id','tanggapan','tgl'];

    /* data ke log */
    protected $log_table    = 'log_trans_draft_kka_detail';
    protected $log_table_fk = 'ref_id';
    /* relation */

    public function draft(){
        return $this->belongsTo(DraftKka::class, 'draft_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function rekomendasi(){
        return $this->hasMany(DraftKkaDetailRekomendasi::class, 'kka_detail_id' , 'id');
    }

    public function user_auditor(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function kategori(){
        return $this->belongsTo(KategoriTemuan::class, 'kategori_id');
    }

    // public function kriteriajudul(){
    //     return $this->belongsTo(KriteriaTemuanDetail::class, 'kriteria_id');
    // }

    public function filesMorphClass()
    {
        return 'kka';
    }

    public function attachments()
    {
        return $this->morphMany(Attachments::class, 'target');
    }

    public function files()
    {
        return $this->morphMany(Files::class, 'target');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
