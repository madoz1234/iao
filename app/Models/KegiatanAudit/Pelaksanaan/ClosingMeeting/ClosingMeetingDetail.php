<?php
namespace App\Models\KegiatanAudit\Pelaksanaan\ClosingMeeting;

use App\Models\Model;
use App\Models\KegiatanAudit\Pelaksanaan\ClosingMeeting\ClosingMeeting;
use App\Models\Auths\User;

class ClosingMeetingDetail extends Model
{
    /* default */
    protected $table 		= 'trans_closing_meeting_detail';
    protected $fillable 	= ['closing_id','user_id','nama'];

    /* data ke log */
    protected $log_table    = 'log_trans_closing_meeting_detail';
    protected $log_table_fk = 'ref_id';
    /* relation */

    public function closing(){
        return $this->belongsTo(ClosingMeeting::class, 'closing_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
