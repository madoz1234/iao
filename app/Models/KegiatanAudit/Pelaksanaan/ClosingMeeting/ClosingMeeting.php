<?php
namespace App\Models\KegiatanAudit\Pelaksanaan\ClosingMeeting;

use App\Models\Model;
use App\Models\Attachments;
use App\Models\Files;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka;
use App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting;
use App\Models\KegiatanAudit\Pelaksanaan\ClosingMeeting\ClosingMeetingDetail;

class ClosingMeeting extends Model
{
    /* default */
    protected $table 		= 'trans_closing_meeting';
    protected $fillable 	= ['opening_id','tanggal','tempat','status','mulai','selesai','lampiran','lampiranname','hadir','hadirname'];

    /* data ke log */
    protected $log_table    = 'log_trans_closing_meeting';
    protected $log_table_fk = 'ref_id';
    /* relation */

    public function opening(){
        return $this->belongsTo(OpeningMeeting::class, 'opening_id');
    }

    public function detailanggota(){
        return $this->hasMany(ClosingMeetingDetail::class, 'closing_id' , 'id');
    }

    public function draftkka(){
        return $this->hasOne(DraftKka::class, 'closing_id');
    }

    public function saveDetail($detail, $details)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
    			if(!empty($value['peserta'])){
	    			$opening_detail = new ClosingMeetingDetail;
	    			$opening_detail->user_id = $value['peserta'];
	    			$this->detailanggota()->save($opening_detail);
    			}
    		}

    	}
    	if($details)
    	{
    		foreach ($details as $key => $value) {
    			if(!empty($value['peserta_lainnya'])){
    				if(!is_null($value['peserta_lainnya'])){
		    			$opening_detail = new ClosingMeetingDetail;
		    			$opening_detail->user_id = 1;
		    			$opening_detail->nama = $value['peserta_lainnya'];
		    			$this->detailanggota()->save($opening_detail);
    				}
    			}
    		}
    	}
    }

    public function filesMorphClass()
    {
        return 'closing-meeting';
    }

    public function attachments()
    {
        return $this->morphMany(Attachments::class, 'target');
    }

    public function files()
    {
        return $this->morphMany(Files::class, 'target');
    }

    public function fileLampiran()
    {
    	$show[] = asset('storage/'.$this->lampiran);
        return json_encode($show);
    }
    
    public function fileLampiranJson()
    {
            $url = $this->lampiran;
            $url = asset('storage/'.$url);
            $filename = $this->lampiranname;
            $arr[]= [
                'type' => 'pdf',
                'caption' => $filename,
                'downloadUrl' => $url,
                'size' => 1218822,
                'width' => "120px",
                'key' => 1
            ];
        return json_encode($arr);
    }

    public function fileHadir()
    {
    	$show[] = asset('storage/'.$this->hadir);
        return json_encode($show);
    }
    
    public function fileHadirJson()
    {
            $url = $this->hadir;
            $url = asset('storage/'.$url);
            $filename = $this->hadirname;
            $arr[]= [
                'type' => 'pdf',
                'caption' => $filename,
                'filename' => $filename,
                'downloadUrl' => $url,
                'size' => 1218822,
                'width' => "120px",
                'key' => 1
            ];
        return json_encode($arr);
    }


    public function fileFoto()
    {
    	$show = [];
        foreach ($this->files()->where('target_type','closing-meeting')->get() as $key => $value) {
            $url = $value->url;
            $show[] = asset('storage/'.$url);
        }
        return json_encode($show);
    }
    
    public function fileFotoJson()
    {
        $arr = [];
        foreach ($this->files()->where('target_type','closing-meeting')->get() as $key => $value) {
            $url = $value->url;
            $url = asset('storage/'.$url);
            $type = $value->type;
            $filename = $value->filename;
            if($type == 'png' OR $type == 'jpg' OR $type == 'jpeg' OR $type == 'gif' OR $type == 'ico' OR $type == 'apng'){
                $arr[]= [
                    'caption' => $filename,
                    'downloadUrl' => $url,
                    'size' => 930321,
                    'width' => "120px",
                    'key' => $key+1
                ];
            }else{
                $arr[]= [
                    'type' => $type,
                    'caption' => $filename,
                    'downloadUrl' => $url,
                    'size' => 930321,
                    'width' => "120px",
                    'key' => $key+1
                ];
            }
        }
        return json_encode($arr);
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
