<?php
namespace App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting;

use App\Models\Model;
use App\Models\Attachments;
use App\Models\Files;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit;
use App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting;
use App\Models\KegiatanAudit\Pelaksanaan\ClosingMeeting\ClosingMeeting;
use App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeetingDetail;

class OpeningMeeting extends Model
{
    /* default */
    protected $table 		= 'trans_opening_meeting';
    protected $fillable 	= ['program_id','tanggal','tempat','status','mulai','selesai','lampiran','lampiranname','hadir','hadirname'];

    /* data ke log */
    protected $log_table    = 'log_trans_opening_meeting';
    protected $log_table_fk = 'ref_id';
    /* relation */

    public function closing(){
        return $this->hasOne(ClosingMeeting::class, 'opening_id');
    }

    public function program(){
        return $this->belongsTo(ProgramAudit::class, 'program_id');
    }

    public function detailanggota(){
        return $this->hasMany(OpeningMeetingDetail::class, 'opening_id' , 'id');
    }

    public function saveDetail($detail, $details)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
    			if(!empty($value['peserta'])){
	    			$opening_detail = new OpeningMeetingDetail;
	    			$opening_detail->user_id = $value['peserta'];
	    			$this->detailanggota()->save($opening_detail);
    			}
    		}
    	}

    	if($details)
    	{
    		foreach ($details as $key => $value) {
    			if(!empty($value['peserta_lainnya'])){
    				if(!is_null($value['peserta_lainnya'])){
		    			$opening_detail = new OpeningMeetingDetail;
		    			$opening_detail->user_id = 1;
		    			$opening_detail->nama = $value['peserta_lainnya'];
		    			$this->detailanggota()->save($opening_detail);
    				}
    			}
    		}
    	}
    }

    public function filesMorphClass()
    {
        return 'opening-meeting';
    }

    public function attachments()
    {
        return $this->morphMany(Attachments::class, 'target');
    }

    public function files()
    {
        return $this->morphMany(Files::class, 'target');
    }

    public function fileLampiran()
    {
    	$show[] = asset('storage/'.$this->lampiran);
        return json_encode($show);
    }
    
    public function fileLampiranJson()
    {
            $url = $this->lampiran;
            $url = asset('storage/'.$url);
            $filename = $this->lampiranname;
            $arr[]= [
                'type' => 'pdf',
                'caption' => $filename,
                'downloadUrl' => $url,
                'size' => 1218822,
                'width' => "120px",
                'key' => 1
            ];

            if($this->lampiran){
		        return json_encode($arr);
            }else{
            	return 1;
            }
    }

    public function fileHadir()
    {
    	$show[] = asset('storage/'.$this->hadir);
        return json_encode($show);
    }
    
    public function fileHadirJson()
    {
            $url = $this->hadir;
            $url = asset('storage/'.$url);
            $filename = $this->hadirname;
            $arr[]= [
                'type' => 'pdf',
                'caption' => $filename,
                'filename' => $filename,
                'downloadUrl' => $url,
                'size' => 1218822,
                'width' => "120px",
                'key' => 1
            ];
    	if($this->hadir){
	        return json_encode($arr);
        }else{
        	return 1;
        }
    }


    public function fileFoto()
    {
    	$show = [];
        foreach ($this->files()->where('target_type','opening-meeting')->get() as $key => $value) {
            $url = $value->url;
            $show[] = asset('storage/'.$url);
        }
        return json_encode($show);
    }
    
    public function fileFotoJson()
    {
        $arr = [];
        foreach ($this->files()->where('target_type','opening-meeting')->get() as $key => $value) {
            $url = $value->url;
            $url = asset('storage/'.$url);
            $type = $value->type;
            $filename = $value->filename;
            if($type == 'png' OR $type == 'jpg' OR $type == 'jpeg' OR $type == 'gif' OR $type == 'ico' OR $type == 'apng'){
                $arr[]= [
                    'caption' => $filename,
                    'downloadUrl' => $url,
                    'size' => 930321,
                    'width' => "120px",
                    'key' => $key+1
                ];
            }else{
                $arr[]= [
                    'type' => $type,
                    'caption' => $filename,
                    'downloadUrl' => $url,
                    'size' => 930321,
                    'width' => "120px",
                    'key' => $key+1
                ];
            }
        }
        return json_encode($arr);
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
