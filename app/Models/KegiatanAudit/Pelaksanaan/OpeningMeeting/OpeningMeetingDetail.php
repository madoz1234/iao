<?php
namespace App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting;

use App\Models\Model;
use App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting;
use App\Models\Auths\User;

class OpeningMeetingDetail extends Model
{
    /* default */
    protected $table 		= 'trans_opening_meeting_detail';
    protected $fillable 	= ['opening_id','user_id','nama'];

    /* data ke log */
    protected $log_table    = 'log_trans_opening_meeting_detail';
    protected $log_table_fk = 'ref_id';
    /* relation */

    public function opening(){
        return $this->belongsTo(ProgramAudit::class, 'opening_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
