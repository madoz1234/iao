<?php

namespace App\Models\Rapat;

use App\Models\Model;
use App\Models\Files;
use App\Models\Auths\User;

class AgendaInternal extends Model
{
    /* default */
    protected $table 		= 'trans_rapat_internal_agenda';
    protected $fillable 	= [
                                'rapat_id',
                                'agenda',
                              ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
                    
}
