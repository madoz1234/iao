<?php

namespace App\Models\Rapat;

use App\Models\Model;
use App\Models\Files;

use App\Models\Rapat\UndanganPesertaInternal;
use App\Models\Master\Project;

class PesertaProject extends Model
{
    /* default */
    protected $table 		= 'trans_undangan_internal_project';
    protected $fillable 	= [
                                'undangan_id',
                                'project_id',
                            ]; 

    // public function undanganInternal(){
    //     return $this->hasMany(UndanganPesertaInternal::class, 'undangan_id', 'id');
    // }  
    
    public function undanganInternal()
    {
        return $this->belongsTo(UndanganPesertaInternal::class, 'undangan_id');
    }
}
