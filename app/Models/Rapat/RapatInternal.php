<?php

namespace App\Models\Rapat;

use App\Models\Model;
use App\Models\Files;
use App\Libraries\CoreSn;
use App\Models\Auths\User;
use App\Models\Rapat\UndanganPesertaEksternal;
use App\Models\Rapat\UndanganPesertaInternal;
use App\Models\Rapat\AgendaInternal;
use App\Models\Rapat\Risalah;
use App\Models\Rapat\PesertaProject;
use App\Models\Master\Project;
use App\Models\KegiatanKonsultasi\KegiatanKonsultasi;
use App\Models\KegiatanLainnya\KegiatanLainnya;
use Mail;
use App\Mail\RapatEmail;

class RapatInternal extends Model
{
    /* default */
    protected $table 		= 'trans_rapat_internal';
    protected $fillable 	= [
                        'nomor',
                        'tanggal',
                        'jam_mulai',
                        'jam_selesai',
                        'tempat',
                        'jumlah_peserta',
                        'keterangan',
                        'status',
                        'flag',
                        'status_rapat',
                        'status_kategori',
                    ];

    /* data ke log */
    protected $log_table    = 'log_trans_rapat_internal';
    protected $log_table_fk = 'trans_id';
    protected $appends = ['tanggal_convert'];

    public function getTanggalConvertAttribute()
    {
        $show = CoreSn::DateToString($this->attributes['tanggal']).' Pukul '.$this->attributes['jam_mulai'];

        return $show;
    }

    public function getKategoriAttribute()
    {
        if ($this->konsultasi) {
            return 'Kegiatan Konsultasi';
        }elseif($this->lainnya){
            return 'Kegiatan Lainnya';
        }else{
            return 'Umum';
        }
    }

    public function konsultasi()
    {
        return $this->hasOne(KegiatanKonsultasi::class, 'rapat_id');
    }

    public function lainnya()
    {
        return $this->hasOne(KegiatanLainnya::class, 'rapat_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function filesMorphClass()
    {
        return 'rapat-internal';
    }

    public function files()
    {
        return $this->morphMany(Files::class, 'target');
    }

    public function daftarHadir()
    {
        return $this->hasMany(DaftarHadir::class, 'rapat_id','id');
    }

    public function risalah()
    {
        return $this->hasMany(Risalah::class, 'rapat_id','id');
    }

    public function fileRisalah()
    {
        $url = $this->files()->where('flag','risalah')->pluck('url')->first();
        return asset('storage/'.$url);
    }

    public function fileHadir()
    {
        // $url = null;
        // if( $this->files()->where('flag','hadir')->get()->count() > 0){
        //     $url = asset('storage/'.$this->files()->where('flag','hadir')->pluck('url')->first());
        // }
        // return $url;
        $show = [];
        foreach ($this->files()->where('flag','hadir')->get() as $key => $value) {
            $url = $value->url;
            $show[] = asset('storage/'.$url);
        }

        return json_encode($show);
    }

    public function fileHadirJson()
    {
        $arr = [];
        foreach ($this->files()->where('flag','hadir')->get() as $key => $value) {
            $url = $value->url;
            $url = asset('storage/'.$url);
            $type = $value->type;
            $filename = $value->filename;
            if($type == 'png' OR $type == 'jpg' OR $type == 'jpeg' OR $type == 'gif' OR $type == 'ico' OR $type == 'apng'){
                $arr[]= [
                    'caption' => $filename,
                    'downloadUrl' => $url,
                    'size' => 930321,
                    'width' => "120px",
                    'key' => $key+1
                ];
            }else{
                $arr[]= [
                    'type' => $type,
                    'caption' => $filename,
                    'downloadUrl' => $url,
                    'size' => 930321,
                    'width' => "120px",
                    'key' => $key+1
                ];
            }
        }
        return json_encode($arr);
    }

    public function fileOther()
    {
        $show = [];
        foreach ($this->files()->where('flag','other')->get() as $key => $value) {
            $url = $value->url;
            $show[] = asset('storage/'.$url);
        }

        return json_encode($show);
    }

    public function fileOtherJson()
    {
        $arr = [];
        foreach ($this->files()->where('flag','other')->get() as $key => $value) {
            $url = $value->url;
            $url = asset('storage/'.$url);
            $type = $value->type;
            $filename = $value->filename;
            if($type == 'png' OR $type == 'jpg' OR $type == 'jpeg' OR $type == 'gif' OR $type == 'ico' OR $type == 'apng'){
                $arr[]= [
                    'caption' => $filename,
                    'downloadUrl' => $url,
                    'size' => 930321,
                    'width' => "120px",
                    'key' => $key+1
                ];
            }else{
                $arr[]= [
                    'type' => $type,
                    'caption' => $filename,
                    'downloadUrl' => $url,
                    'size' => 930321,
                    'width' => "120px",
                    'key' => $key+1
                ];
            }
        }
        return json_encode($arr);
    }

    public function saveRisalahFile($files,$slug = null)
    {
        if(isset($slug)){
            foreach($this->files()->where('flag','risalah')->get() as $file)
            {
                if(file_exists(public_path('storage/'.$file->url)))
                {
                    unlink(public_path('storage/'.$file->url));
                }
                $file->delete();
            }
        }
        $data['filename'] = $files->getClientOriginalName();
        $data['url'] = $files->store($this->filesMorphClass(), 'public');
        $data['target_type'] = $this->filesMorphClass();
        $data['target_id'] = $this->id;
        $data['flag'] = 'risalah';
        $data['type'] = $files->getClientOriginalExtension();
        if(isset($taken))
        {
        foreach ($this->taken as $Takenvalue) {
            $data['taken_at'] = $Takenvalue;
        }
        }
        $file = new Files;
        $file->fill($data);
        $file->save();
    }

    public function saveHadirFile($files,$slug = null)
    {
        if(isset($slug)){
            foreach($this->files()->where('flag','hadir')->get() as $file)
            {
                if(file_exists(public_path('storage/'.$file->url)))
                {
                    unlink(public_path('storage/'.$file->url));
                }
                $file->delete();
            }
        }
        foreach ($files as $key => $files) {
            $data['filename'] = $files->getClientOriginalName();
            $data['url'] = $files->store($this->filesMorphClass(), 'public');
            $data['target_type'] = $this->filesMorphClass();
            $data['target_id'] = $this->id;
            $data['flag'] = 'hadir';
            $data['type'] = $files->getClientOriginalExtension();
            $data['dms_update'] = 1;
            if(isset($taken))
                {
                    foreach ($this->taken as $Takenvalue) {
                        $data['taken_at'] = $Takenvalue;
                    }
                }
            $file = new Files;
            $file->fill($data);
            $file->save();
        }
    }

    public function saveOtherFile($req,$slug = null)
    {
        if(isset($slug)){
            foreach($this->files()->where('flag','other')->get() as $file)
            {
                if(file_exists(public_path('storage/'.$file->url)))
                {
                    unlink(public_path('storage/'.$file->url));
                }
                $file->delete();
            }
        }
        foreach ($req as $key => $files) {
            $data['filename'] = $files->getClientOriginalName();
            $data['url'] = $files->store($this->filesMorphClass(), 'public');
            $data['target_type'] = $this->filesMorphClass();
            $data['target_id'] = $this->id;
            $data['flag'] = 'other';
            $data['type'] = $files->getClientOriginalExtension();
            $data['dms_update'] = 1;
            if(isset($taken))
                {
                    foreach ($this->taken as $Takenvalue) {
                        $data['taken_at'] = $Takenvalue;
                    }
                }
            $file = new Files;
            $file->fill($data);
            $file->save();
        }
    }

    public function saveDaftarHadir($req,$slug = null)
    {
        if(isset($slug)){
            foreach($this->daftarHadir as $hadir)
            {
                $hadir->delete();
            }
        }
        if(isset($req->nama_peserta[0]) OR isset($req->email[0]) OR isset($req->nomor_hp[0]) OR isset($req->perusahaan[0])){
            foreach ($req->key_daftar as $key => $hadir) {
                $data['rapat_id']   = $this->id;
                $data['nama']       = $req->nama_peserta[$key];
                $data['email']      = $req->email[$key];
                $data['nomor']      = $req->nomor_hp[$key];
                $data['perusahaan'] = $req->perusahaan[$key];

                $file = new DaftarHadir;
                $file->fill($data);
                $file->save();
            }
        }
    }
    public function saveRisalah($req,$slug = null)
    {
        if(isset($slug)){
            foreach($this->risalah as $risalah)
            {
                $risalah->delete();
            }
        }
        if(isset($req->risalah[0]) OR isset($req->pen_1[0]) OR isset($req->realisasi[0]) OR isset($req->pen_2[0]) OR isset($req->rencana[0]) OR isset($req->pen_3[0])){
            foreach ($req->key_risalah as $key => $risalah) {
                $data['rapat_id'] = $this->id;
                $data['risalah'] = $req->risalah[$key];
                $data['realisasi'] = $req->realisasi[$key];
                if($this->status == 0){
                    $data['pic_1_id'] = $req->pic_1_id[$key];
                    $data['pic_2_id'] = $req->pic_2_id[$key];
                    $data['pic_3_id'] = $req->pic_3_id[$key];
                }else{
                    $data['pen_1'] = $req->pen_1[$key];
                    $data['pen_2'] = $req->pen_2[$key];
                    $data['pen_3'] = $req->pen_3[$key];
                }
                $data['rencana'] = $req->rencana[$key];

                $file = new Risalah;
                $file->fill($data);
                $file->save();
            }
        }
    }

    public function unlinkFiles()
    {
        foreach($this->files as $file)
        {
            if(file_exists(public_path('storage/'.$file->url)))
            {
                unlink(public_path('storage/'.$file->url));
            }
        }
    }

    // undangan peserta
    public function undanganEksternal(){
        return $this->hasMany(UndanganPesertaEksternal::class, 'rapat_id', 'id');
    }
    public function undanganInternal(){
        return $this->hasMany(UndanganPesertaInternal::class, 'rapat_id', 'id');
    }
    public function undanganInternalProject(){
        return $this->hasMany(PesertaProject::class, 'undangan_id', 'id');
    }
    public function agendaInternal(){
        return $this->hasMany(AgendaInternal::class, 'rapat_id', 'id');
    }
    // public function detailProject(){
    //     return $this->belongsToMany(AgendaInternal::class, 'trans_undangan_internal_project', 'undangan_id', 'project_id', 'id');
    // }

    public function saveDetail($detail)
    {
        if($detail)
        {
            foreach ($detail as $key => $value) {
                $data                   = new UndanganPesertaEksternal;
                $data->email            = $value['email'];
                $data->perusahaan       = $value['perusahaan'];
                $this->undanganEksternal()->save($data);
            }
        }
    }

    public function saveAgenda($data)
    {
        if($data)
        {
            foreach ($data as $key => $value) {
                $val                   = new AgendaInternal;
                $val->agenda           = $value['agenda'];
                $this->agendaInternal()->save($val);
            }
        }
    }

    public function updateAgenda($data)
    {
        if($data)
        {
            foreach ($data as $key => $value) {
                if(!empty($value['id'])){
                    $val                   = AgendaInternal::find($value['id']);
                    $val->agenda           = $value['agenda'];
                    $this->agendaInternal()->save($val);
                }else{
                    $val                   = new AgendaInternal;
                    $val->agenda           = $value['agenda'];
                    $this->agendaInternal()->save($val);
                }
            }
        }
    }

    public function updateDetail($detail)
    {
        if($detail)
        {
            foreach ($detail as $key => $value) {
                if(!empty($value['id'])){
                    $data                   = UndanganPesertaEksternal::find($value['id']);
                    $data->email            = $value['email'];
                    $data->perusahaan       = $value['perusahaan'];
                    $this->undanganEksternal()->save($data);
                }else{
                    $data                   = new UndanganPesertaEksternal;
                    $data->email            = $value['email'];
                    $data->perusahaan       = $value['perusahaan'];
                    $this->undanganEksternal()->save($data);
                }
            }
        }
    }

    public function saveDetailInternal($detail)
    {
        // dd($detail);
        if($detail)
        {
            foreach ($detail as $key => $value) {
                $data                   = new UndanganPesertaInternal;
                $data->user_id          = $value['user_id'];
                $data->rapat_id         = $this->id;
                $data->save();
                // dd($data);
                $data->detailProject()->sync($value['project_id']);
            }
        }
    }

    public function saveAgendaInternal($data)
    {
        if($data)
        {
            foreach ($data as $key => $value) {
                $val                   = new AgendaInternal;
                $val->agenda           = $value['agenda'];
                $this->agendaInternal()->save($val);
            }
        }
    }

    public function updateDetailInternal($detail)
    {
        if($detail)
        {
            foreach ($detail as $key => $value) {
                if(!empty($value['id'])){
                    $data                   = UndanganPesertaInternal::find($value['id']);
                    $data->user_id          = $value['user_id'];
                    $data->rapat_id         = $this->id;
                    $data->save();
                    // dd($data);
                    $data->detailProject()->sync($value['project_id']);
                    // $this->undanganInternal()->save($data);
                }else{
                        // dd($value);
                    $data                   = new UndanganPesertaInternal;
                    $data->user_id          = $value['user_id'];
                    $data->rapat_id         = $this->id;
                    // $this->undanganInternal()->save($data);
                    $data->save();
                    // dd($data);
                    $data->detailProject()->sync($value['project_id']);
                }
            }
        }
    }

    public function updateAgendaInternal($data)
    {
        if($data)
        {
            foreach ($data as $key => $value) {
                if(!empty($value['id'])){
                    $val                   = AgendaInternal::find($value['id']);
                    $val->agenda           = $value['agenda'];
                    $this->agendaInternal()->save($val);
                }else{
                    $val                   = new AgendaInternal;
                    $val->agenda           = $value['agenda'];
                    $this->agendaInternal()->save($val);
                }
            }
        }
    }

    public function updateRisalah($detail)
    {
        if($detail)
        {
            foreach ($detail as $key => $value) {
                if(!empty($value['id'])){
                    $data                   = risalah::find($value['id']);
                }else{
                    $data                   = new risalah;
                }
                    $data->risalah          = $value['risalah'];
                    $data->pic_1_id         = $value['pic_1_id'];
                    $data->realisasi        = $value['realisasi'];
                    $data->pic_2_id         = $value['pic_2_id'];
                    $data->rencana          = $value['rencana'];
                    $data->pic_3_id         = $value['pic_3_id'];
                    $this->risalah()->save($data);
            }
        }
    }

    public function updateRisalahEkternal($detail)
    {
        // dd($detail);
        if($detail)
        {
            foreach ($detail as $key => $value) {
                if(!empty($value['id'])){
                    $data                   = risalah::find($value['id']);
                }else{
                    $data                   = new risalah;
                }
                    $data->risalah          = $value['risalah'];
                    $data->pen_1            = $value['pen_1'];
                    $data->realisasi        = $value['realisasi'];
                    $data->pen_2            = $value['pen_2'];
                    $data->rencana          = $value['rencana'];
                    $data->pen_3            = $value['pen_3'];
                    $this->risalah()->save($data);
            }
        }
    }

    // Abdul Hadi
    public function undanganPesertaInternal()
    {
        return $this->belongsToMany(User::class, 'trans_rapat_internal_undangan_peserta', 'rapat_id', 'user_id')->withPivot(['status', 'created_by', 'updated_by'])->withTimestamps();
    }

    public function createOrUpdatePeserta($detail, $jenis)
    {
        $record = [];
        if (strtolower($jenis) == 'internal') {
            foreach($detail as $item){
                $record  =  (!empty($item['id'])) 
                            ? UndanganPesertaInternal::find($item['id']) 
                            : new UndanganPesertaInternal;
                $record->rapat_id = $this->id;
                $record->user_id = $item['user_id'];
                $record->save();

                $project_id = (!empty($item['project_id'])) ? $item['project_id'] : ['project_id' => 0];

                $record->detailProject()->sync($project_id); 
            }
            $this->undanganPesertaInternal()->sync(array_column($detail, 'user_id')); 
        }else{
            foreach($detail as $item){
                $record[] = new UndanganPesertaEksternal([
                                'email' => $item['email'],
                                'perusahaan' => $item['perusahaan'],
                            ]);
            }
            $this->undanganEksternal()->delete();
            $this->undanganEksternal()->saveMany($record);
        }
    }

    public function createOrUpdateAgenda($data)
    {
        $record = [];
        foreach($data as $item){
            $record[] = new AgendaInternal([
                            'agenda' => $item['agenda']
                        ]);
        }

        $this->agendaInternal()->delete();
        $this->agendaInternal()->saveMany($record);
    }

    public function sendMailUndangan($flag, $jenis)
    {
        if ($flag == 1) {
            if (strtolower($jenis) == 'internal') {
                foreach ($this->undanganInternal as $data) {
                    $user = User::find($data->user_id);
                    Mail::to($user['email'])->queue(new RapatEmail($this, $user, 'Konfirmasi Kehadiran Peserta Undangan'));
                    $data->status = 1;
                    $data->save();
                }
            }else{
                foreach ($this->undanganEksternal as $user){
                    Mail::to($user->email)->queue(new RapatEmail($this, $user, 'Konfirmasi Kehadiran Peserta Undangan'));
                    $user->status = 1;
                    $user->save();
                }
            }
        }
    }
}
