<?php

namespace App\Models\Rapat;

use App\Models\Model;
use App\Models\Files;
use App\Models\Auths\User;

class UndanganPesertaEksternal extends Model
{
    /* default */
    protected $table 		= 'trans_rapat_eksternal_undangan_peserta';
    protected $fillable 	= [
                        'rapat_id',
                        'email',
                        'perusahaan',
                        'status',
                    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
                    
}
