<?php

namespace App\Models\Rapat;

use App\Models\Model;
use App\Models\Files;
use App\Models\Auths\User;

class DaftarHadir extends Model
{
    /* default */
    protected $table 		= 'trans_rapat_internal_daftar_hadir';
    protected $fillable 	= [
                        'rapat_id',
                        'nama',
                        'jabatan',
                        'user_id',
                        'email',
                        'nomor',
                        'perusahaan',
                    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
                    
}
