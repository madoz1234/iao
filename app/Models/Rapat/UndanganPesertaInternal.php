<?php

namespace App\Models\Rapat;

use App\Models\Model;
use App\Models\Files;

use App\Models\Rapat\PesertaProject;
use App\Models\Master\Project;
use App\Models\Auths\User;

class UndanganPesertaInternal extends Model
{
    /* default */
    protected $table 		= 'trans_rapat_internal_undangan_peserta';
    protected $fillable 	= [
                        'rapat_id',
                        'user_id',
                        // 'project_id',
                    ];

    public function undanganInternalProject()
    {
        return $this->hasMany(PesertaProject::class, 'undangan_id', 'id');
    }

    public function detailProject(){
        return $this->belongsToMany(Project::class, 'trans_undangan_internal_project', 'undangan_id', 'project_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
                    
}
