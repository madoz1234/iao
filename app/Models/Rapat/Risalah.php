<?php

namespace App\Models\Rapat;

use App\Models\Auths\User;
use App\Models\Files;
use App\Models\Model;
use App\Models\Rapat\RapatInternal;
use ZipArchive;

class Risalah extends Model
{
    /* default */
    protected $table 		= 'trans_rapat_internal_risalah';
    protected $fillable 	= [
                        'rapat_id',
                        'risalah',
                        'pen_1',
                        'realisasi',
                        'pen_2',
                        'rencana',
                        'pen_3',
                        'status',
                        'pic_1_id',
                        'pic_2_id',
                        'pic_3_id',
                    ];

    public function pic_1()
    {
        return $this->belongsTo(User::class, 'pic_1_id');
    }
    public function pic_2()
    {
        return $this->belongsTo(User::class, 'pic_2_id');
    }
    public function pic_3()
    {
        return $this->belongsTo(User::class, 'pic_3_id');
    }

    public function rapat()
    {
        return $this->belongsTo(RapatInternal::class, 'rapat_id');
    }

    public function filesMorphClass()
    {
        return static::class;
    }

    public function files()
    {
        return $this->morphMany(Files::class, 'target');
    }

    /* MUTATOR */
    public function getStrStatusAttribute()
    {
        if ($this->status == 0) {
            $status = 'Open';
        }elseif($this->status == 1) {
            $status = 'On Progress';
        }else{
            $status = 'Closed';
        }
        return $status;
    }

    /* OTHER */
    public function fileRisalah()
    {
        $show = [];
        $files = $this->files()->get();
        foreach ($files as $key => $value) {
            $url = $value->url;
            $show[] = asset('storage/'.$url);
        }

        return json_encode($show);
    }

    public function fileRisalahJson()
    {
        $arr = [];
        foreach ($this->files as $key => $value) {
            $url = $value->url;
            $url = asset('storage/'.$url);
            $type = $value->type;
            $filename = $value->filename;
            if($type == 'png' OR $type == 'jpg' OR $type == 'jpeg' OR $type == 'gif' OR $type == 'ico' OR $type == 'apng'){
                $arr[]= [
                    'caption' => $filename,
                    'downloadUrl' => $url,
                    'size' => 930321,
                    'width' => "120px",
                    'key' => $key+1
                ];
            }else{
                $arr[]= [
                    'type' => $type,
                    'caption' => $filename,
                    'downloadUrl' => $url,
                    'size' => 930321,
                    'width' => "120px",
                    'key' => $key+1
                ];
            }
        }
        return json_encode($arr);
    }

    public function saveRisalahFile($files, $slug = null)
    {
        if(isset($slug)){
            foreach($this->files()->where('flag', $this->status)->get() as $file)
            {
                if(file_exists(public_path('storage/'.$file->url)))
                {
                    unlink(public_path('storage/'.$file->url));
                }
                $file->delete();
            }
        }
        foreach ($files as $key => $files) {
            $data['filename'] = $files->getClientOriginalName();
            $data['url'] = $files->store('risalah-rapat', 'public');
            $data['target_type'] = $this->filesMorphClass();
            $data['target_id'] = $this->id;
            $data['flag'] = $this->status;
            $data['type'] = $files->getClientOriginalExtension();
            $data['dms_update'] = 1;
            if(isset($taken)){
                foreach ($this->taken as $Takenvalue) {
                    $data['taken_at'] = $Takenvalue;
                }
            }
            $file = new Files;
            $file->fill($data);
            $file->save();
        }
    }

    public function zipRisalahFile($title = false)
    {
        $zip      = new ZipArchive;
        $title    = $title ?: 'Risalah Rapat';
        $fileName = '/app/public/'.$title.'.zip';
        if (file_exists(storage_path().'/app/public/'.$title.'.zip')) {
            unlink(storage_path().'/app/public/'.$title.'.zip');
        }
        if ($zip->open(storage_path().$fileName, ZipArchive::CREATE) === TRUE) {
            foreach ($this->files as $value) {
                if(file_exists(storage_path().'/app/public/'.$value->url)) {
                    if($value->flag == 0){
                        $files = storage_path().'/app/public/'.$value->url;
                        $zip->addFile($files,'Open/'.$value->filename);
                    }elseif ($value->flag == 1) {
                        $files = storage_path().'/app/public/'.$value->url;
                        $zip->addFile($files,'On Progress/'.$value->filename);
                    }else{
                        $files = storage_path().'/app/public/'.$value->url;
                        $zip->addFile($files,'Closed/'.$value->filename);
                    }
                }
            }
        }

        $zip->close();

        if(file_exists(storage_path().'/app/public/'.$title.'.zip')){
            return response()->download(storage_path().'/app/public/'.$title.'.zip');
        }
        return abort(404);
    }
}
