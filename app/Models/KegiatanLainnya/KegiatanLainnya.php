<?php

namespace App\Models\KegiatanLainnya;

use App\Models\Auths\User;
use App\Models\Files;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\Model;
use App\Models\Notification;
use App\Models\Rapat\RapatInternal;
use App\Models\Rapat\Risalah;
use App\Models\SessionLog;
use Carbon\Carbon;

class KegiatanLainnya extends Model
{
    /* default */
    protected $table        = 'trans_kegiatan_lainnya';
    protected $fillable     = ['rencana_detail_id', 'jenis_rapat', 'pimpinan_rapat', 'notulen_rapat', 'tanggal_rapat', 'status', 'ket_svp', 'judul_audit', 'tanggal_mulai', 'tanggal_selesai'];

    /* data ke log */
    protected $log_table    = 'log_trans_kegiatan_lainnya';
    protected $log_table_fk = 'ref_id';

    /* RELATION */
    public function rkia()
    {
        return $this->belongsTo(RencanaAuditDetail::class, 'rencana_detail_id');
    }

    public function rapat()
    {
        return $this->belongsTo(RapatInternal::class, 'rapat_id');
    }

    public function pimpinan()
    {
        return $this->belongsTo(User::class, 'pimpinan_rapat');
    }
    
    public function notulen()
    {
        return $this->belongsTo(User::class, 'notulen_rapat');
    }

    /* MUTATOR */
    public function setTanggalRapatAttribute($value)
    {
        $this->attributes['tanggal_rapat'] = (is_null($value)) ? $value : Carbon::createFromFormat("d-m-Y", $value);
    }

    public function setTanggalMulaiAttribute($value)
    {
        $this->attributes['tanggal_mulai'] = (is_null($value)) ? $value : Carbon::createFromFormat("d-m-Y", $value);
    }

    public function setTanggalSelesaiAttribute($value)
    {
        $this->attributes['tanggal_selesai'] = (is_null($value)) ? $value : Carbon::createFromFormat("d-m-Y", $value);
    }

    public function getTanggalAttribute()
    {
        return Carbon::parse($this->tanggal_rapat)->format('d-m-Y');
    }

    public function getMulaiAttribute()
    {
        return Carbon::parse($this->tanggal_mulai)->format('d-m-Y');
    }

    public function getSelesaiAttribute()
    {
        return Carbon::parse($this->tanggal_selesai)->format('d-m-Y');
    }

    public function getJenisAttribute()
    {
        return $this->jenis_rapat == 1 ? 'Internal' : 'Eksternal';
    }

    public function getKategoriAttribute()
    {
        return $this->rkia->kategori;
    }

    public function getAuditorEksternalAttribute()
    {
        if($this->rkia->lain_id == 1000){
            $auditor_eksternal = '-';
        }elseif($this->rkia->lain_id == 1001){
            $auditor_eksternal = '-';
        }else{
            $auditor_eksternal = $this->rkia->lain->nama;
        }
        return $auditor_eksternal;
    }

    /* SAVE DATA */
    public function saveFromRequest($request, $action)
    {
        \DB::beginTransaction();
        try {
            switch ($action) {
                case "perencanaan-store":
                    $this->saveByFill($request);
                    $this->updateRKIA($request->status);
                    if ($request->status == 2) {
                        $this->offNotification(1);
                        $this->addNotification('Waiting Approval');
                        $this->addSessionLog('Waiting Approval');
                    }else{
                        $this->addSessionLog('Menambahkan Draft');
                    }
                break;
                case "perencanaan-update":
                    $this->saveByFill($request);
                    $this->updateRKIA($request->status);
                    if ($request->status == 2) {
                        $this->offNotification(1);
                        $this->addNotification('Waiting Approval');
                        $this->addSessionLog('Waiting Approval');
                    }else{
                        $this->addSessionLog('Mengubah Draft');
                    }
                break;
                case "perencanaan-approve":
                    $this->saveByFill($request);
                    $this->updateRKIA($request->status);
                    $this->offNotification(2);
                    $this->addNotification('Approval', false, [$this->created_by]);
                    $this->addNotification('Approval', route('kegiatan-konsultasi.pelaksanaan.index'), [$this->notulen_rapat]);
                    $this->addSessionLog('Approval');
                break;
                case "perencanaan-reject":
                    $this->saveByFill($request);
                    $this->updateRKIA($request->status);
                    $this->offNotification(2);
                    $this->addNotification('Reject', false, [$this->created_by]);
                    $this->addSessionLog('Reject');
                break;
                case "pelaksanaan-add-rapat":
                    $this->updateRKIA($request->status);
                    $this->addRapat($request);
                    $this->offNotification(3);
                    if ($request->status == 4) {
                        $this->addSessionLog('Menambahkan Agenda Rapat', 'Pelaksanaan');
                    }else{
                        $this->addSessionLog('Menambahkan Draft Agenda Rapat', 'Pelaksanaan');
                    }
                break;
                case "laporan-update-risalah":
                    $this->updateRisalah($request);
                    $arr_status = $this->rapat->risalah->pluck('status')->toArray();
                    if (!in_array(1, $arr_status) && !in_array(2, $arr_status)) {
                        $request->merge(['status' => 4]);
                    }elseif (!in_array(0, $arr_status) && !in_array(1, $arr_status)) {
                        $request->merge(['status' => 6]);
                    }else{
                        $request->merge(['status' => 5]);
                    }
                    $this->saveByFill($request);
                    $this->updateRKIA($request->status);
                    $this->addSessionLog('Mengubah Status Risalah Rapat', 'Laporan');
                break;
                case "laporan-upload-file-risalah":
                    $this->saveRisalahFile($request, true);
                    $this->addSessionLog('Upload Filen Risalah Rapat');
                break;
                default:
                    $this->saveByFill($request);
                    $this->updateRKIA($request->status);
                break;
            }

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();            

            return response()->json([
                'errors'  => [$e->getMessage()],
                'message' => 'Beban server sedang tinggi, silakan ulangi lagi.',
                'data'    => $request->all()
            ], 422);
        }

        return response()->json([
            'status'  => true,
            'message' => 'Data berhasil di simpan',
            'data'    => $this
        ]);
    }

    public function saveByFill($request)
    {
        $this->fill($request->all());
        $this->save();
    }

    public function updateRKIA($status)
    {
        $rkia = RencanaAuditDetail::find($this->rencana_detail_id);
        $rkia->status_lain = $status;
        $rkia->save();
    }

    public function updateRisalah($request)
    {
        $risalah = Risalah::find($request->risalah_id);
        $risalah->status = $request->status;
        $risalah->save();
    }

    public function addRapat($request)
    {
        $status_konsultasi       = $request->status;
        $request                 = $request;
        $request['tanggal']      = Carbon::createFromFormat('d-m-Y',$request->tanggal)->format('d-m-Y');
        $request['status']       = ($request->jenis_rapat == 1) ? 0 : 2;
        $request['flag']         = ($request->status == 3) ? 0 : 1;
        $request['status_rapat'] = 0;

        $record = (is_null($this->rapat_id)) ? new RapatInternal : RapatInternal::find($this->rapat_id);
        $record->fill($request->all());
        $record->save();
        
        $record->createOrUpdateAgenda($request->data);
        $record->createOrUpdatePeserta($request->detail, $this->jenis);
        $record->sendMailUndangan($request->flag, $this->jenis);

        $this->rapat_id        = $record->id;
        $this->status          = $status_konsultasi;
        $this->judul_audit     = $request->judul_audit;
        $this->tanggal_mulai   = $request->tanggal_mulai;
        $this->tanggal_selesai = $request->tanggal_selesai;
        $this->save();
    }

    public function addNotification($type = 'Approval', $url = false, $users = false)
    {
        $url = $url ?: route('kegiatan-lainnya.perencanaan.index');
        $users  = $users ?: User::whereHas('roles', function($role){
                                    $role->where('name', 'svp-audit');
                                })->pluck('id')->toArray();

        foreach ($users as $user_id) {
            $notif             = new Notification;
            $notif->user_id    = $user_id;
            $notif->parent_id  = $this->id;
            $notif->url        = $url;
            $notif->modul      = 'kegiatan-lainnya';
            $notif->stage      = $this->status;
            $notif->keterangan = $type.' Kegiatan Lainnya Kategori '.$this->rkia->kategori;
            $notif->status     = 1;
            $notif->save();
            $notif->sendEmail();
        }
    }

    public function offNotification($stage)
    {
        $update_notif = Notification::where('parent_id', $this->id)
                                    ->where('modul', 'kegiatan-lainnya')
                                    ->where('stage', $stage)
                                    ->get();
        foreach ($update_notif as $notif) {
            $notif->status = 0;
            $notif->save();
        }
    }

    public function sendMailNotification($user_id, $message)
    {
        $user = User::find($user_id);
        Mail::to($user->email)->queue(new NotificationEmail($this, $user, $message));
    }

    public function addSessionLog($aktivitas = 'Menambahkan', $sub = 'Perencanaan')
    {
        $log            = new SessionLog;
        $log->modul     = 'Kegiatan Lainnya';
        $log->sub       = $sub;
        $log->aktivitas = $aktivitas.' Kegiatan Lainnya Kategori '.$this->rkia->kategori;
        $log->user_id   = auth()->id();
        $log->save();
    }

    public function saveRisalahFile($request, $delete = false)
    {
        $files = $this->rapat->risalah->find($request->risalah_id);
        $files->saveRisalahFile($request->files_risalah, $delete);
    }

    /* STATIC FUNCTIONS */
    public static function badgeTab($modul = 'perencanaan')
    {
        if ($modul == 'perencanaan') {
            $baru       = RencanaAuditDetail::where('flag', 1)->where('tipe', 2)->whereIn('status_lain', [0,1])
                                            ->whereHas('rencanaaudit', function($u){
                                                $u->whereIn('status', [4,5,6]);
                                            })->get()->count();
            $onProgress = (new static)::where('status', 2)->get()->count();
        }elseif ($modul == 'pelaksanaan') {
            $baru       = (new static)::where('status', 3)->get()->count();
            $onProgress = (new static)::where('status', 4)->whereHas('rapat', function($rapat){
                                            $rapat->where('status_rapat', 0);
                                        })->get()->count();
        }else{
            $baru       = (new static)::where('status', 4)->whereHas('rapat', function($rapat){
                                            $rapat->where('status_rapat', 1);
                                        })->get()->count();
            $onProgress = (new static)::where('status', 5)->get()->count();
        }
        return json_decode(json_encode([
            'baru'       => $baru,
            'onProgress' => $onProgress,
            'total'      => $baru + $onProgress
        ]));
    }
}
