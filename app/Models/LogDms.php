<?php

namespace App\Models;

use App\Models\Model;

class LogDms extends Model
{
    protected $table 		= 'sys_log_dms';

    protected $fillable 	= [
        'message',
        'bw_id',
        'file_name',
        'error'
    ];
}
