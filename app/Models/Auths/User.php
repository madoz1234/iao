<?php

namespace App\Models\Auths;

use App\Models\Tests\Participation;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Models\Master\ProjectDetailPic;
use App\Models\Master\BU;
use App\Models\Master\CO;
use App\Models\Master\AnakPerusahaan;
use App\Models\Master\BUDetailPic;
use App\Models\Master\AnakPerusahaanDetailPic;
use App\Models\Master\CODetailPic;
use App\Models\Master\VendorDetailPic;
use App\Models\Traits\Utilities;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, HasRoles, Utilities;

    /* JWT */
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    /* End of JWT */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dates = ['last_login'];
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'last_login', 'photo','fungsi','tipe', 'last_activity', 'nip', 'inisial', 'kategori', 'kategori_id', 'posisi', 'kode_divisi', 'nama_divisi', 'no_ktp'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $table = 'sys_users';

    public function tests()
    {
        return $this->hasMany(Participation::class, 'user_id');
    }

    public function averageScore()
    {
        return ($this->tests->count() > 0) ? $this->tests->avg('score') : 0;
    }

    public function testStats()
    {
        return $this->tests()->passed()->count().' / '.$this->tests->count();
    }

    public function canTake($test_id)
    {
        $taken = $this->tests()->whereHas('test', function ($query) use ($test_id) {
            return $query->where('id', $test_id)
                         ->where('trial', 0);
        })->first();

        return is_null($taken);
    }

    public function takeTest($test)
    {
        DB::beginTransaction();

        $test = $this->tests()->create([
            'test_id' => $test->id,
            'start'   => Carbon::now(),
            'status'  => 'ongoing'
        ]);

        if (!$test->generateQuestions()) {
            DB::rollback();
        }

        DB::commit();

        return $test;
    }

    public function bu_pic(){
        return $this->hasMany(BUDetailPic::class, 'pic' , 'id');
    }

    public function project_pic(){
        return $this->hasMany(ProjectDetailPic::class, 'pic' , 'id');
    }

    public function ap_pic(){
        return $this->hasMany(AnakPerusahaanDetailPic::class, 'pic' , 'id');
    }

    public function co_pic(){
        return $this->hasMany(CODetailPic::class, 'pic' , 'id');
    }

    public function vendor_pic(){
        return $this->hasMany(VendorDetailPic::class, 'pic' , 'id');
    }

    public function updateActivity()
    {
        $this->last_activity = Carbon::now()->format('Y-m-d H:i:s');
        $this->save();
    }

    public function bu(){
        return $this->belongsTo(BU::class, 'kategori_id');
    }

    public function co(){
        return $this->belongsTo(CO::class, 'kategori_id');
    }

    public function au(){
        return $this->belongsTo(AnakPerusahaan::class, 'kategori_id');
    }

    public function flushActivity()
    {
        $this->last_activity = NULL;
        $this->save();
    }

    public function setLoginWeb()
    {
        $this->login_web = 1;
        $this->save();
    }

    public function updateLoginWeb()
    {
        $this->login_web = 0;
        $this->save();
    }

    public function checkToken()
    {
        if($this->login_web == 0)
        {
            if($this->access_token != NULL)
            {
                // [TODO] Certificate
            }else{
                $this->flushActivity();
                Auth::logout();
            }
        }
    }

    public function scopeDivisi($query, $kategori='co', $kode='Internal Audit')
    {
        return $query->whereHas($kategori, function ($kgr) use ($kode) {
                                    $kgr->where('kode', $kode);
                                });
    }

    public function isUserIA()
    {
    	if($this->co){
	        if ($this->co->kode == 'Internal Audit') {
	            return true;
	        }
    	}
        return false;
    }

    public function scopeWhereLike($query, $value)
	{
	    return $query->where('posisi', 'like', '%'.$value.'%');
	}

  public function hasPosisi($name)
  {
      if(is_array($name))
      {
          if(in_array($this->posisi))
          {
              return true;
          }
      }else{
          if($this->posisi == $name)
          {
              return true;
          }
      }

      return false;
  }

	public function scopeWhereLikes($query, $value)
	{
	    return $query->where('nama_divisi', 'like', '%'.$value.'%');
	}

	public function hasJabatan($posisi): bool
    {
    	if (is_string($posisi) && false !== strpos($posisi, '|')) {
            $posisi = $this->convertPipeToArray($posisi);
        }

        if (is_string($posisi)) {
            return containz($this->posisi, $posisi);
        }

        if (is_array($posisi)) {
            foreach ($posisi as $posisi) {
                if($this->hasJabatan($posisi)) {
                    return true;
                }
            }

            return false;
        }
    }

    protected function convertPipeToArray(string $pipeString)
    {
        $pipeString = trim($pipeString);

        if (strlen($pipeString) <= 2) {
            return $pipeString;
        }

        $quoteCharacter = substr($pipeString, 0, 1);
        $endCharacter = substr($quoteCharacter, -1, 1);

        if ($quoteCharacter !== $endCharacter) {
            return explode('|', $pipeString);
        }

        if (! in_array($quoteCharacter, ["'", '"'])) {
            return explode('|', $pipeString);
        }

        return explode('|', trim($pipeString, $quoteCharacter));
    }

    public function isPicObjectAudit($object_audit = null)
    {
        if (is_null($object_audit)) {
            switch (true) {
                case $this->bu_pic->count() > 0 :
                case $this->project_pic->count() > 0 :
                case $this->ap_pic->count() > 0 :
                case $this->co_pic->count() > 0 : return true;
                    break;

                default: return false;
                    break;
            }
        }

        switch ($object_audit) {
            case 'bu': return $this->bu_pic->count() > 0;
                break;
            case 'project': return $this->project_pic->count() > 0;
                break;
            case 'ap': return $this->ap_pic->count() > 0;
                break;
            case 'co': return $this->co_pic->count() > 0;
                break;

            default: return false;
                break;
        }
    }
}
