<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\KertasKerja;
use App\Models\Master\SpinPembobotanDetail;

class KertasKerjaDetail extends Model
{
    /* default */
    protected $table 		= 'ref_spin_kertas_kerja_detail';
    protected $fillable 	= ['kertas_kerja_id','pembobotan_detail_id','status'];

    /* data ke log */
    protected $log_table    = 'log_ref_spin_kertas_kerja_detail';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function kertaskerja(){
        return $this->belongsTo(KertasKerja::class, 'kertas_kerja_id');
    }

    public function bobotdetail(){
        return $this->belongsTo(SpinPembobotanDetail::class, 'pembobotan_detail_id');
    }
}
