<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\SpinPembobotanDetail;
use App\Models\Master\SpinPembobotanDetailNilai;
use App\Models\Master\KertasKerja;


class SpinPembobotan extends Model
{
    /* default */
    protected $table 		= 'ref_spin_pembobotan';
    protected $fillable 	= ['version', 'deskripsi', 'status'];

    /* data ke log */
    protected $log_table    = 'log_ref_spin_pembobotan';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function detail(){
        return $this->hasMany(SpinPembobotanDetail::class, 'spin_pembobot_id' , 'id');
    }
    public function detailnilai(){
        return $this->hasMany(SpinPembobotanDetailNilai::class, 'pembobot_id' , 'id');
    }

    public function kertaskerja(){
        return $this->hasOne(KertasKerja::class, 'spin_id');
    }

    public function updateDetail($detail, $detailNilai)
    {
        if($detail)
        {
            foreach ($detail as $key => $value) {
                if(!empty($value['id'])){
                    $data                       = SpinPembobotanDetail::find($value['id']);
                    $data->pof_id               = $value['pof_id'];
                    $this->detail()->save($data);
                }else{
                    $data                       = new SpinPembobotanDetail;
                    $data->pof_id               = $value['pof_id'];
                    $this->detail()->save($data);
                }
            }
        }
    }

    // public function updateDetailNilai($detailNilai)
    // {
    //     if($detailNilai)
    //     {
    //         foreach ($detailNilai as $valuex) {
    //             foreach ($valuex as $key => $value) {
    //                 // dd($value['nilai']);
    //                 if(!empty($value['id'])){
    //                     $data                       = SpinPembobotanDetailNilai::find($value['id']);
    //                     $data->pemenuhan_detail_id  = $value['pemenuhan_detail_id'];
    //                     $data->pembobot_detail_id   = $value['pembobot_detail_id'];
    //                     $data->nilai                = $value['nilai'];
    //                     $this->detailNilai()->save($data);
    //                 }else{
    //                     $data                       = new SpinPembobotanDetailNilai;
    //                     $data->pemenuhan_detail_id  = $value['pemenuhan_detail_id'];
    //                     $data->pembobot_detail_id   = $value['pembobot_detail_id'];
    //                     $data->nilai                = $value['nilai'];
    //                     $this->detailNilai()->save($data);
                        
    //                 }   
    //             }
    //         }   
    //     }
    // }
}