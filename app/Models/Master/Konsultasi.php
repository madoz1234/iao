<?php
namespace App\Models\Master;

use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\Model;

class Konsultasi extends Model
{
    /* default */
    protected $table 		= 'ref_konsultasi';
    protected $fillable 	= ['nama'];

    /* data ke log */
    protected $log_table    = 'log_ref_konsultasi';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function rkia(){
        return $this->hasMany(RencanaAuditDetail::class, 'konsultasi_id' , 'id');
    }
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
