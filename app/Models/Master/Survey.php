<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\SurveyPertanyaan;
use App\Models\Master\SurveyPilgan;
use App\Models\Survey\SurveyJawab;

class Survey extends Model
{
    /* default */
    protected $table 		= 'ref_survei';
    protected $fillable 	= ['version','status','description'];

    /* data ke log */
    protected $log_table    = 'log_ref_survei';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here

    public function pertanyaan(){
        return $this->hasMany(SurveyPertanyaan::class, 'survei_id' , 'id');
    }
	
	public function survey_jawab(){
        return $this->hasMany(SurveyJawab::class, 'survei_id' , 'id');
    }

}
