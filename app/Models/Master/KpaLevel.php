<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\Kpa;
use App\Models\Master\KpaDetil;

class KpaLevel extends Model
{
    /* default */
    protected $table 		= 'ref_kpa_level';
    protected $fillable 	= ['kpa_id','level'];

    /* data ke log */
    protected $log_table    = 'log_ref_kpa_level';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function kpa(){
        return $this->belongsTo(Kpa::class, 'kpa_id');
    }

    public function detail(){
        return $this->hasMany(KpaDetil::class, 'level_id' , 'id');
    }
    // insert code here
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
