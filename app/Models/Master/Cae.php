<?php
namespace App\Models\Master;

use App\Models\Model;

class Cae extends Model
{
    /* default */
    protected $table 		= 'ref_cae';
    protected $fillable 	= ['nama','alamat','no_tlp'];

    /* data ke log */
    protected $log_table    = 'log_ref_cae';
    protected $log_table_fk = 'ref_id';
    /* relation */
    
    // insert code here
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
