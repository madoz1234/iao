<?php
namespace App\Models\Master;

use App\Models\Master\Survey;
use App\Models\Master\SurveyPilgan;
use App\Models\Model;
use App\Models\Survey\SurveyJawabDetail;

class SurveyPertanyaan extends Model
{
    /* default */
    protected $table 		= 'ref_survei_pertanyaan';
    protected $fillable 	= ['survei_id','pertanyaan'];

    /* data ke log */
    protected $log_table    = 'log_ref_survei_pertanyaan';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function survey(){
        return $this->belongsTo(Survey::class, 'survei_id' , 'id');
    }
    public function survey_jawab_detail(){
        return $this->hasMany(SurveyJawabDetail::class, 'tanya_id' , 'id');
    }

    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
