<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\CODetailPic;

class CO extends Model
{
    /* default */
    protected $table 		= 'ref_co';
    protected $fillable 	= ['kode','nama','alamat','no_tlp'];

    /* data ke log */
    protected $log_table    = 'log_ref_co';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function detail_pic(){
        return $this->hasMany(CODetailPic::class, 'co_id' , 'id');
    }
    // insert code here
    /* mutator */
    // insert code here
    public function saveDetail($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
    			$data = new CODetailPic;
    			$data->pic = $value;
    			$this->detail_pic()->save($data);
    		}
    	}
    }

    public function updateDetail($detail, $id)
    {
    	if($detail)
    	{
    		$hapus = CODetailPic::whereNotIn('pic', $detail)
    										 ->where('co_id', $id)
	                        				 ->delete();
    		foreach ($detail as $key => $value) {
    			$cari = CODetailPic::where('co_id', $id)->where('pic', $value)->first();
    			if($cari){
    				$data = CODetailPic::find($cari->id);
	    			$data->pic = $value;
	    			$this->detail_pic()->save($data);
    			}else{
	    			$data = new CODetailPic;
	    			$data->pic = $value;
	    			$this->detail_pic()->save($data);
    			}
    		}
    	}
    }

    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
