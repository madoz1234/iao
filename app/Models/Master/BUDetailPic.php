<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\BU;
use App\Models\Auths\User;

class BUDetailPic extends Model
{
    /* default */
    protected $table 		= 'ref_bu_pic';
    protected $fillable 	= ['bu_id','nama','alamat','no_tlp','pic'];

    /* data ke log */
    protected $log_table    = 'log_ref_bu_pic';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function user(){
        return $this->belongsTo(User::class, 'pic');
    }

    public function bu(){
        return $this->belongsTo(BU::class, 'bu_id');
    }

    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
