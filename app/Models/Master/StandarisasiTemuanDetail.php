<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\StandarisasiTemuan;

class StandarisasiTemuanDetail extends Model
{
    /* default */
    protected $table 		= 'ref_standarisasi_temuan_detail';
    protected $fillable 	= ['standarisasi_id','deskripsi'];

    /* data ke log */
    protected $log_table    = 'log_ref_standarisasi_temuan_detail';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function standarisasi(){
        return $this->belongsTo(StandarisasiTemuan::class, 'standarisasi_id' , 'id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
