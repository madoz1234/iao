<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\AnakPerusahaan;
use App\Models\Auths\User;

class AnakPerusahaanDetailPic extends Model
{
    /* default */
    protected $table 		= 'ref_anak_perusahaan_pic';
    protected $fillable 	= ['ap_id','nama','alamat','no_tlp','pic'];

    /* data ke log */
    protected $log_table    = 'log_ref_anak_perusahaan_pic';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function user(){
        return $this->belongsTo(User::class, 'pic');
    }

    public function ap(){
        return $this->belongsTo(AnakPerusahaan::class, 'ap_id');
    }

    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
