<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\Kpa;
use App\Models\Master\Uraian;

class Elemen extends Model
{
    /* default */
    protected $table 		= 'ref_elemen';
    protected $fillable 	= ['elemen'];

    /* data ke log */
    protected $log_table    = 'log_ref_elemen';
    protected $log_table_fk = 'ref_id';
    /* relation */
    
    public function kpa(){
        return $this->hasMany(Kpa::class, 'elemen_id' , 'id');
    }

    public function uraian(){
        return $this->hasMany(Uraian::class, 'elemen_id' , 'id');
    }
    // insert code here
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
