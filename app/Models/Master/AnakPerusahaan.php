<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\AnakPerusahaanDetailPic;

class AnakPerusahaan extends Model
{
    /* default */
    protected $table 		= 'ref_anak_perusahaan';
    protected $fillable 	= ['kode','nama','alamat','no_tlp'];

    /* data ke log */
    protected $log_table    = 'log_ref_anak_perusahaan';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function detail_pic(){
        return $this->hasMany(AnakPerusahaanDetailPic::class, 'ap_id' , 'id');
    }

    public function saveDetail($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
    			$data = new AnakPerusahaanDetailPic;
    			$data->pic = $value;
    			$this->detail_pic()->save($data);
    		}
    	}
    }

    public function updateDetail($detail, $id)
    {
    	if($detail)
    	{
    		$hapus = AnakPerusahaanDetailPic::whereNotIn('pic', $detail)
    										 ->where('ap_id', $id)
	                        				 ->delete();
    		foreach ($detail as $key => $value) {
    			$cari = AnakPerusahaanDetailPic::where('ap_id', $id)->where('pic', $value)->first();
    			if($cari){
    				$data = AnakPerusahaanDetailPic::find($cari->id);
	    			$data->pic = $value;
	    			$this->detail_pic()->save($data);
    			}else{
	    			$data = new AnakPerusahaanDetailPic;
	    			$data->pic = $value;
	    			$this->detail_pic()->save($data);
    			}
    		}
    	}
    }
    // insert code here
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
