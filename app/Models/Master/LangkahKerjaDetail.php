<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\LangkahKerja;

class LangkahKerjaDetail extends Model
{
    /* default */
    protected $table 		= 'ref_langkah_kerja_detail';
    protected $fillable 	= ['langkah_kerja_id','deskripsi'];

    /* data ke log */
    protected $log_table    = 'log_ref_langkah_kerja_detail';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function langkahkerja(){
        return $this->belongsTo(LangkahKerja::class, 'langkah_kerja_id');
    }
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
