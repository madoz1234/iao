<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\KertasKerjaDetail;
use App\Models\Master\SpinPembobotanDetailNilai;

class KertasKerjaDetailNilai extends Model
{
    /* default */
    protected $table 		= 'ref_spin_kertas_kerja_detail_nilai';
    protected $fillable 	= ['detail_id','detail_nilai_id','status'];

    /* data ke log */
    protected $log_table    = 'log_ref_spin_kertas_kerja_detail_nilai';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function detailkerja(){
        return $this->belongsTo(KertasKerjaDetail::class, 'detail_id');
    }

    public function detailnilai(){
        return $this->belongsTo(SpinPembobotanDetailNilai::class, 'detail_nilai_id');
    }
}
