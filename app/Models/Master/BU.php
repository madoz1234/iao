<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\BUDetailPic;
use App\Models\Master\Project;

class BU extends Model
{
    /* default */
    protected $table 		= 'ref_bu';
    protected $fillable 	= ['kode','nama','alamat','no_tlp'];

    /* data ke log */
    protected $log_table    = 'log_ref_bu';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function detail_pic(){
        return $this->hasMany(BUDetailPic::class, 'bu_id' , 'id');
    }

    public function detailproject(){
        return $this->hasMany(Project::class, 'bu_id' , 'id');
    }
    // insert code here
    /* mutator */
    // insert code here
    public function saveDetail($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
    			$data = new BUDetailPic;
    			$data->pic = $value;
    			$this->detail_pic()->save($data);
    		}
    	}
    }

    public function updateDetail($detail, $id)
    {
    	if($detail)
    	{
    		$hapus = BUDetailPic::whereNotIn('pic', $detail)
    										 ->where('bu_id', $id)
	                        				 ->delete();
    		foreach ($detail as $key => $value) {
    			$cari = BUDetailPic::where('bu_id', $id)->where('pic', $value)->first();
    			if($cari){
    				$data = BUDetailPic::find($cari->id);
	    			$data->pic = $value;
	    			$this->detail_pic()->save($data);
    			}else{
	    			$data = new BUDetailPic;
	    			$data->pic = $value;
	    			$this->detail_pic()->save($data);
    			}
    		}
    	}
    }

    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
