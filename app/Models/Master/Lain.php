<?php
namespace App\Models\Master;

use App\Models\Model;

class Lain extends Model
{
    /* default */
    protected $table 		= 'ref_kegiatan_lain';
    protected $fillable 	= ['nama'];

    /* data ke log */
    protected $log_table    = 'log_ref_kegiatan_lain';
    protected $log_table_fk = 'ref_id';
    /* relation */
    
    // insert code here
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
