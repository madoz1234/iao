<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\SpinPembobotan;
use App\Models\Master\SpinPof;
use App\Models\Master\SpinPembobotanDetailNilai;

class SpinPembobotanDetail extends Model
{
    /* default */
    protected $table 		= 'ref_spin_pembobotan_detail';
    protected $fillable 	= ['pof_id', 'spin_pembobot_id'];

    /* data ke log */
    protected $log_table    = 'log_ref_spin_pembobotan_detail';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function pembobotan(){
        return $this->belongsTo(SpinPembobotan::class, 'spin_pembobot_id');
    }

    public function pof(){
        return $this->belongsTo(SpinPof::class, 'pof_id');
    }

    public function detailnilai(){
        return $this->hasMany(SpinPembobotanDetailNilai::class, 'pembobot_detail_id' , 'id');
    }
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
