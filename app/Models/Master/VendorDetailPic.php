<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\Vendor;
use App\Models\Auths\User;

class VendorDetailPic extends Model
{
    /* default */
    protected $table 		= 'ref_vendor_pic';
    protected $fillable 	= ['vendor_id','nama','alamat','no_tlp','pic'];

    /* data ke log */
    protected $log_table    = 'log_ref_vendor_pic';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function user(){
        return $this->belongsTo(User::class, 'pic');
    }

    public function vendor(){
        return $this->belongsTo(Vendor::class, 'vendor_id');
    }

    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
