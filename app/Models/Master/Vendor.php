<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\VendorDetailPic;

class Vendor extends Model
{
    /* default */
    protected $table 		= 'ref_vendor';
    protected $fillable 	= ['kode','nama','alamat','no_tlp'];

    /* data ke log */
    protected $log_table    = 'log_ref_vendor';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function detail_pic(){
        return $this->hasMany(VendorDetailPic::class, 'vendor_id' , 'id');
    }
    // insert code here
    /* mutator */
    // insert code here
    public function saveDetail($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
    			$data = new VendorDetailPic;
    			$data->pic = $value;
    			$this->detail_pic()->save($data);
    		}
    	}
    }

    public function updateDetail($detail, $id)
    {
    	if($detail)
    	{
    		$hapus = VendorDetailPic::whereNotIn('pic', $detail)
    										 ->where('vendor_id', $id)
	                        				 ->delete();
    		foreach ($detail as $key => $value) {
    			$cari = VendorDetailPic::where('vendor_id', $id)->where('pic', $value)->first();
    			if($cari){
    				$data = VendorDetailPic::find($cari->id);
	    			$data->pic = $value;
	    			$this->detail_pic()->save($data);
    			}else{
	    			$data = new VendorDetailPic;
	    			$data->pic = $value;
	    			$this->detail_pic()->save($data);
    			}
    		}
    	}
    }

    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
