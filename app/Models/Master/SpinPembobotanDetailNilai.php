<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\SpinPembobotan;
use App\Models\Master\SpinPemenuhan;
use App\Models\Master\SpinPemenuhanDetail;
use App\Models\Master\SpinPembobotanDetail;

class SpinPembobotanDetailNilai extends Model
{
    /* default */
    protected $table 		= 'ref_spin_pembobotan_detail_nilai';
    protected $fillable 	= ['pembobot_detail_id', 'pembobot_id', 'pemenuhan_detail_id', 'nilai'];

    /* data ke log */
    protected $log_table    = 'log_ref_spin_pembobotan_detail_nilai';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function pemenuhanDetail(){
        return $this->belongsTo(SpinPemenuhanDetail::class, 'pemenuhan_detail_id', 'id');
    }

    public function pembobotan(){
        return $this->belongsTo(SpinPembobotan::class, 'pembobot_id');
    }

    public function pembobotanDetail(){
        return $this->belongsTo(SpinPembobotanDetail::class, 'pembobot_detail_id');
    }
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
