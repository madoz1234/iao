<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumenDetail;

class Dokumen extends Model
{
    /* default */
    protected $table 		= 'ref_dokumen';
    protected $fillable 	= ['judul','deskripsi'];

    /* data ke log */
    protected $log_table    = 'log_ref_dokumen';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here

    public function detiltinjauan(){
        return $this->hasMany(TinjauanDokumenDetail::class, 'dokumen_id' , 'id');
    }
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
