<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaDetail;

class KategoriTemuan extends Model
{
    /* default */
    protected $table 		= 'ref_kategori_temuan';
    protected $fillable 	= ['status','deskripsi'];

    /* data ke log */
    protected $log_table    = 'log_ref_kategori_temuan';
    protected $log_table_fk = 'ref_id';

    public function kkadetail(){
        return $this->hasMany(DraftKkaDetail::class, 'kategori_id' , 'id');
    }
    /* relation */
    // insert code here
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
