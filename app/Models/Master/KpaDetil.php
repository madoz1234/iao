<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\KpaLevel;
use App\Models\Master\KpaDetilUraian;

class KpaDetil extends Model
{
    /* default */
    protected $table 		= 'ref_kpa_detail';
    protected $fillable 	= ['level_id','kpa','deskripsi','uraian','penjelasan','output'];

    /* data ke log */
    protected $log_table    = 'log_ref_kpa_detail';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function kpalevel(){
        return $this->belongsTo(KpaLevel::class, 'level_id');
    }

    public function detiluraian(){
         return $this->hasMany(KpaDetilUraian::class, 'kpa_detail_id' , 'id');
    }

    public function saveDetail($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
				$data 					= new KpaDetilUraian;
	    		$data->uraian 			= $value['uraian'];
	    		$data->penjelasan 		= $value['penjelasan'];
	    		$data->penjelasan 		= $value['penjelasan'];
	    		$data->output 			= $value['output'];
	            $this->detiluraian()->save($data);
    		}
    	}
    }
    // insert code here
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
