<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\LangkahKerjaDetail;
use App\Models\Master\FokusAudit;

class LangkahKerja extends Model
{
    /* default */
    protected $table 		= 'ref_langkah_kerja';
    protected $fillable 	= ['bidang','fokus_audit_id'];

    /* data ke log */
    protected $log_table    = 'log_ref_langkah_kerja';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function detail(){
        return $this->hasMany(LangkahKerjaDetail::class, 'langkah_kerja_id' , 'id');
    }

    public function fokusaudit(){
        return $this->belongsTo(FokusAudit::class, 'fokus_audit_id');
    }
    /* mutator */
    // insert code here
    public function saveDetail($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
				$data 					= new LangkahKerjaDetail;
	    		$data->deskripsi 		= $value['deskripsi'];
	            $this->detail()->save($data);
    		}
    	}
    }

    public function updateDetail($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
				if(!empty($value['id'])){
    				$data 						= LangkahKerjaDetail::find($value['id']);
		    		$data->deskripsi 		    = $value['deskripsi'];
		            $this->detail()->save($data);
		        }else{
    				$data 						= new LangkahKerjaDetail;
		    		$data->deskripsi 				= $value['deskripsi'];
		            $this->detail()->save($data);
		        }
    		}
    	}
    }

    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
