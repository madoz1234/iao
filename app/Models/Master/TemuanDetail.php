<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\Temuan;

class TemuanDetail extends Model
{
    /* default */
    protected $table 		= 'ref_temuan_detail';
    protected $fillable 	= ['temuan_id','kode','deskripsi'];

    /* data ke log */
    protected $log_table    = 'log_ref_temuan_detail';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function kriteria(){
        return $this->belongsTo(Temuan::class, 'temuan_id');
    }
    /* mutator */
    // insert code here

    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
