<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\SpinPembobotan;

class KertasKerja extends Model
{
    /* default */
    protected $table 		= 'ref_spin_kertas_kerja';
    protected $fillable 	= ['spin_id','kategori','kategori_id','deskripsi'];

    /* data ke log */
    protected $log_table    = 'log_ref_spin_kertas_kerja';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function spin(){
        return $this->belongsTo(SpinPembobotan::class, 'spin_id');
    }
}
