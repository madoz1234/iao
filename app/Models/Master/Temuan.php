<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\StandarisasiTemuan;
use App\Models\Master\TemuanDetail;

class Temuan extends Model
{
    /* default */
    protected $table 		= 'ref_temuan';
    protected $fillable 	= ['standarisasi_id'];

    /* data ke log */
    protected $log_table    = 'log_ref_temuan';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function standarisasi(){
        return $this->belongsTo(StandarisasiTemuan::class, 'standarisasi_id');
    }

    public function detail(){
        return $this->hasMany(TemuanDetail::class, 'temuan_id' , 'id');
    }

    public function saveDetail($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
				$data 					= new TemuanDetail;
	    		$data->kode 			= $value['kode'];
	    		$data->deskripsi 		= $value['deskripsi'];
	            $this->detail()->save($data);
    		}
    	}
    }

    public function updateDetail($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
				if(!empty($value['id'])){
    				$data 						= TemuanDetail::find($value['id']);
		    		$data->kode 		    	= $value['kode'];
		    		$data->deskripsi 		    = $value['deskripsi'];
		            $this->detail()->save($data);
		        }else{
    				$data 						= new TemuanDetail;
		    		$data->kode 				= $value['kode'];
		    		$data->deskripsi 			= $value['deskripsi'];
		            $this->detail()->save($data);
		        }
    		}
    	}
    }
    /* mutator */
    // insert code here

    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
