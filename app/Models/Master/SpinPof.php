<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\SpinPemenuhan;

class SpinPof extends Model
{
    /* default */
    protected $table 		= 'ref_spin_pof';
    protected $fillable 	= ['judul','deskripsi','status'];

    /* data ke log */
    protected $log_table    = 'log_ref_spin_pof';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function pemenuhan(){
        return $this->hasMany(SpinPemenuhan::class, 'pof_id' , 'id');
    }

    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
