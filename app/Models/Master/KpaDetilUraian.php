<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\KpaDetil;

class KpaDetilUraian extends Model
{
    /* default */
    protected $table 		= 'ref_kpa_detail_uraian';
    protected $fillable 	= ['kpa_detail_id','uraian','penjelasan','output'];

    /* data ke log */
    protected $log_table    = 'log_ref_kpa_detail_uraian';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function kpadetil(){
        return $this->belongsTo(KpaDetil::class, 'kpa_detail_id');
    }
    // insert code here
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
