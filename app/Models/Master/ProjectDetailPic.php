<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\Project;
use App\Models\Auths\User;

class ProjectDetailPic extends Model
{
    /* default */
    protected $table 		= 'ref_project_pic';
    protected $fillable 	= ['project_id','nama','alamat','no_tlp','pic'];

    /* data ke log */
    protected $log_table    = 'log_ref_project_pic';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function user(){
        return $this->belongsTo(User::class, 'pic');
    }

    public function project(){
        return $this->belongsTo(Project::class, 'project_id');
    }

    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
