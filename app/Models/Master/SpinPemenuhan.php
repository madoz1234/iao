<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\SpinPof;
use App\Models\Master\SpinPemenuhanDetail;

class SpinPemenuhan extends Model
{
    /* default */
    protected $table        = 'ref_spin_unsur_pemenuhan';
    protected $fillable     = ['pof_id'];

    /* data ke log */
    protected $log_table    = 'log_ref_spin_unsur_pemenuhan';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function pof(){
        return $this->belongsTo(SpinPof::class, 'pof_id');
    }

    public function detail(){
        return $this->hasMany(SpinPemenuhanDetail::class, 'spin_pemenuhan_id' , 'id');
    }


    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
    public function saveDetail($detail)
    {
        if($detail)
        {
            foreach ($detail as $key => $value) {
                $data                   = new SpinPemenuhanDetail;
                $data->deskripsi        = $value['deskripsi'];
                $this->detail()->save($data);
            }
        }
    }

    public function updateDetail($detail)
    {
        if($detail)
        {
            foreach ($detail as $key => $value) {
                if(!empty($value['id'])){
                    $data                       = SpinPemenuhanDetail::find($value['id']);
                    $data->deskripsi            = $value['deskripsi'];
                    $this->detail()->save($data);
                }else{
                    $data                       = new SpinPemenuhanDetail;
                    $data->deskripsi                = $value['deskripsi'];
                    $this->detail()->save($data);
                }
            }
        }
    }
}
