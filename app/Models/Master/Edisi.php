<?php
namespace App\Models\Master;

use App\Models\Model;
use Illuminate\Support\Facades\DB;

class Edisi extends Model
{
    /* default */
    protected $table 		= 'ref_edisi';
    protected $fillable 	= ['edisi','revisi','status'];

    /* data ke log */
    protected $log_table    = 'log_ref_edisi';
    protected $log_table_fk = 'ref_id';


    public function logs()
    {
    	return DB::table($this->log_table)->where($this->log_table_fk, $this->id)->where('status', 1)->get();
    }
    /* relation */
    // insert code here
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
