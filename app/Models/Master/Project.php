<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\ProjectDetailPic;
use App\Models\Master\BU;

class Project extends Model
{
    /* default */
    protected $table 		= 'ref_project';
    protected $fillable 	= ['project_ab','project_id','bu_id','nama','alamat','no_tlp'];

    /* data ke log */
    protected $log_table    = 'log_ref_project';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function detail_pic(){
        return $this->hasMany(ProjectDetailPic::class, 'project_id' , 'id');
    }

    public function bu(){
        return $this->belongsTo(BU::class, 'bu_id');
    }
    // insert code here
    /* mutator */
    // insert code here
    public function saveDetail($detail)
    {
    	if($detail)
    	{
    		foreach ($detail as $key => $value) {
    			$data = new ProjectDetailPic;
    			$data->pic = $value;
    			$this->detail_pic()->save($data);
    		}
    	}
    }

    public function updateDetail($detail, $id)
    {
    	if($detail)
    	{
    		$hapus = ProjectDetailPic::whereNotIn('pic', $detail)
    										 ->where('project_id', $id)
	                        				 ->delete();
    		foreach ($detail as $key => $value) {
    			$cari = ProjectDetailPic::where('project_id', $id)->where('pic', $value)->first();
    			if($cari){
    				$data = ProjectDetailPic::find($cari->id);
	    			$data->pic = $value;
	    			$this->detail_pic()->save($data);
    			}else{
	    			$data = new ProjectDetailPic;
	    			$data->pic = $value;
	    			$this->detail_pic()->save($data);
    			}
    		}
    	}
    }

    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
