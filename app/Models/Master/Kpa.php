<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\Elemen;
use App\Models\Master\Uraian;
use App\Models\Master\KpaLevel;
use App\Models\Master\KpaDetil;

class Kpa extends Model
{
    /* default */
    protected $table 		= 'ref_kpa';
    protected $fillable 	= ['elemen_id'];

    /* data ke log */
    protected $log_table    = 'log_ref_kpa';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function elemen(){
        return $this->belongsTo(Elemen::class, 'elemen_id');
    }

    public function uraian(){
        return $this->hasMany(Uraian::class, 'kpa_id' , 'id');
    }

    public function level(){
        return $this->hasMany(KpaLevel::class, 'kpa_id' , 'id');
    }

    public function saveDetail($detail)
    {
    	if($detail)
    	{
    		$i=1;
    		foreach ($detail as $key => $value) {
				$data 					= new KpaLevel;
	    		$data->level 			= $i;
	            $this->level()->save($data);
	            $i++;
	            foreach ($value['detail'] as $key => $vil) {
	            	$detil = new KpaDetIl;
	            	$detil->level_id 	= $data->id;
	            	$detil->kpa 		= $vil['kpa'];
	            	$detil->deskripsi 	= $vil['deskripsi'];
	            	$detil->save();
	            }
    		}
    	}
    }

    public function updateDetail($detail)
    {
    	if($detail)
    	{
    		$i=1;
    		foreach ($detail as $key => $value) {
    			if(!empty($value['id'])){
	    			$cari = KpaLevel::find($value['id']);
	    			if($cari){
			    		$cari->level 			= $i;
			            $this->level()->save($cari);
			            foreach ($value['detail'] as $key => $vil) {
			            	$detil = new KpaDetil;
			            	$detil->level_id 	= $cari->id;
			            	$detil->kpa 		= $vil['kpa'];
			            	$detil->deskripsi 	= $vil['deskripsi'];
			            	$detil->save();
			            }
	    			}else{
						$data 					= new KpaLevel;
			    		$data->level 			= $i;
			            $this->level()->save($data);
			            foreach ($value['detail'] as $key => $vil) {
			            	$detil = new KpaDetil;
			            	$detil->level_id 	= $data->id;
			            	$detil->kpa 		= $vil['kpa'];
			            	$detil->deskripsi 	= $vil['deskripsi'];
			            	$detil->save();
			            }
	    			}
    			}else{
    				$data 					= new KpaLevel;
		    		$data->level 			= $i;
		            $this->level()->save($data);
		            foreach ($value['detail'] as $key => $vil) {
		            	$detil = new KpaDetil;
		            	$detil->level_id 	= $data->id;
		            	$detil->kpa 		= $vil['kpa'];
		            	$detil->deskripsi 	= $vil['deskripsi'];
		            	$detil->save();
		            }
    			}
    			$i++;
    		}
    	}
    }
    // insert code here
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
