<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\Temuan;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaDetail;

class StandarisasiTemuan extends Model
{
    /* default */
    protected $table 		= 'ref_standarisasi_temuan';
    protected $fillable 	= ['bidang','kode','deskripsi'];

    /* data ke log */
    protected $log_table    = 'log_ref_standarisasi_temuan';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function detail(){
        return $this->hasMany(StandarisasiTemuanDetail::class, 'standarisasi_id' , 'id');
    }

    public function temuan(){
        return $this->hasOne(Temuan::class, 'standarisasi_id');
    }

    public function kkadetail(){
        return $this->hasMany(DraftKkaDetail::class, 'standarisasi_id' , 'id');
    }


    public function saveDetail($detail)
    {
        if($detail)
        {
            foreach ($detail as $key => $value) {
            	if($value['deskripsi']){
	                $data                   = new StandarisasiTemuanDetail;
	                $data->deskripsi        = $value['deskripsi'];
	                $this->detail()->save($data);
            	}
            }
        }
    }

    public function updateDetail($detail)
    {
        if($detail)
        {
            foreach ($detail as $key => $value) {
                if(!empty($value['id'])){
                    $data                       = StandarisasiTemuanDetail::find($value['id']);
                    if($value['deskripsi']){
	                    $data->deskripsi            = $value['deskripsi'];
	                    $this->detail()->save($data);
                    }
                }else{
                    $data                       = new StandarisasiTemuanDetail;
                    if($value['deskripsi']){
	                    $data->deskripsi                = $value['deskripsi'];
	                    $this->detail()->save($data);
                    }
                }
            }
        }
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
