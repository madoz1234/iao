<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\CO;
use App\Models\Auths\User;

class CODetailPic extends Model
{
    /* default */
    protected $table 		= 'ref_co_pic';
    protected $fillable 	= ['co_id','nama','alamat','no_tlp','pic'];

    /* data ke log */
    protected $log_table    = 'log_ref_co_pic';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function user(){
        return $this->belongsTo(User::class, 'pic');
    }

    public function co(){
        return $this->belongsTo(CO::class, 'co_id');
    }

    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* costom function */
    // insert code here    
}
