<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditDetailKerja;

class FokusAudit extends Model
{
    /* default */
    protected $table 		= 'ref_fokus_audit';
    protected $fillable 	= ['bidang','audit'];

    /* data ke log */
    protected $log_table    = 'log_ref_fokus_audit';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function langkahkerja(){
        return $this->hasMany(LangkahKerja::class, 'fokus_audit_id' , 'id');
    }

    public function program(){
        return $this->hasMany(ProgramAuditDetailKerja::class, 'fokus_audit_id' , 'id');
    }


    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
