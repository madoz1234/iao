<?php
namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\SpinPemenuhan;

class SpinPemenuhanDetail extends Model
{
    /* default */
    protected $table 		= 'ref_spin_unsur_pemenuhan_detail';
    protected $fillable 	= ['spin_pemenuhan_id','deskripsi'];

    /* data ke log */
    protected $log_table    = 'log_ref_spin_unsur_pemenuhan_detail';
    protected $log_table_fk = 'ref_id';
    /* relation */
    // insert code here
    public function pemenuhan(){
        return $this->belongsTo(SpinPemenuhan::class, 'spin_pemenuhan_id');
    }
    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
