<?php

namespace App\Models\Temp;

use App\Models\Model;
use App\Models\Files;
use App\Models\Auths\User;

class Division extends Model
{
    /* default */
    protected $table 		= 'temp_division';
    protected $fillable 	= [
                                'obj_id',
                                'obj_level',
                                'parent_id',
                                'short_text'
                              ];

}
