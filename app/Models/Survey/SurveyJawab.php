<?php
namespace App\Models\Survey;

use DB;
use App\Models\Model;
use App\Models\Auths\User;
use App\Models\Master\Edisi;
use App\Models\Notification;
use App\Models\Master\Survey;
use App\Models\Master\SurveyPertanyaan;
use App\Models\KegiatanAudit\Pelaporan\LHA;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit;

class SurveyJawab extends Model
{
    /* default */
    protected $table 		= 'trans_survei_jawab';
    protected $fillable 	= ['lha_id', 'survei_id', 'user_id', 'timer'];

    /* data ke log */
    protected $log_table    = 'log_trans_survei_jawab';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function lha(){
        return $this->belongsTo(LHA::class, 'lha_id' , 'id');
    }
    
    public function survey()
    {
        return $this->belongsTo(Survey::class, 'survei_id', 'id');
    }
    public function user(){
        return $this->belongsTo(User::class, 'user_id' , 'id');
    }
    public function survey_jawab_detail()
    {
        return $this->hasMany(SurveyJawabDetail::class, 'survei_jawab_id', 'id');
    }

    public function edisi()
    {
        return $this->belongsTo(Edisi::class, 'edisi_id', 'id');
    }

    /* scope */
    

    /* mutator */
    public function getSecondsTimerAttribute()
    {
        $time = date_parse($this->timer);
        $seconds = $time['hour'] * 3600 + $time['minute'] * 60 + $time['second'];
        return $seconds;
    }

    public function addNotification($message = false, $url = false, $users = false)
    {
        if ($message === false) {
            $message = 'Anda belum mengisi seluruh pertanyaan. Silakan lengkapi survey Anda!';
        }
        if ($url === false) {
            $url = route('survey-kepuasan-audit.survey.index');
        }
        if ($users === false) {
            $users  = (new static)::where('lha_id', $this->lha_id)->pluck('user_id')->toArray();
        }
        foreach ($users as $user) {
            $notif             = new Notification;
            $notif->user_id    = $user;
            $notif->parent_id  = $this->id;
            $notif->url        = $url;
            $notif->modul      = 'survey-kepuasan-audit';
            $notif->stage      = $this->status;
            $notif->keterangan = $message;
            $notif->status     = 1;
            $notif->save();
            $notif->sendEmail();
        }
    }

    public function offNotification($stage)
    {
        $update_notif = Notification::where('parent_id', $this->id)
                                    ->where('modul', 'survey-kepuasan-audit')
                                    ->where('stage', $stage)
                                    ->get();
        foreach ($update_notif as $notif) {
            $notif->status = 0;
            $notif->save();
        }
    }

    public static function checkSurveyForNotif()
    {
        $surveys = (new static)::whereIn('status', [0,1])->get();
        foreach ($surveys as $survey) {
            $check_notif = Notification::where('parent_id', $survey->id)
                                ->where('modul', 'survey-kepuasan-audit')
                                ->where('stage', $survey->status)
                                ->where('user_id', $survey->user_id)
                                ->whereDate('created_at', '=', date('Y-m-d'))
                                ->first();
            if ($check_notif == null) {
                $survey->offNotification(0);
                $survey->offNotification(1);
                $survey->addNotification(false, false, [$survey->user_id]);
            }
        }
        return true;
    }

    public static function getLhaByObject($user, $all = true)
    {
        $arr_lha = [];
        if ($all === true) {
            $arr_lha += static::where('user_id', $user->id)->pluck('lha_id')->toArray();
        }
        if (count($user->bu_pic) > 0) {
            $bu_id = $user->bu_pic->pluck('bu_id')->toArray();
            $object_id = DB::table('ref_project')->whereIn('bu_id', $bu_id)->pluck('id')->toArray();
            $lha_from_project = static::whereHas('lha', function($lha) use ($object_id){
                                    $lha->whereHas('draftkka', function($draftkka) use ($object_id){
                                        $draftkka->whereHas('program', function($program) use ($object_id){
                                            $program->whereHas('penugasanaudit', function($penugasanaudit) use ($object_id){
                                                $penugasanaudit->whereHas('rencanadetail', function($rencanadetail) use ($object_id){
                                                    $rencanadetail->where('tipe_object', 2)->whereIn('object_id', $object_id);
                                                });
                                            });
                                        });
                                    });
                                });
            $arr_lha += $lha_from_project->pluck('lha_id')->toArray();
        }

        return  $arr_lha;
    }

    public static function badgeTab($modul = 'survey')
    {
        $user = auth()->user();
        $baru = 0;
        $object_audit = 0;
        if ($modul == 'survey') {
            if($user->hasRole(['auditor','svp-audit','dirut'])){
                $baru     = static::whereIn('status', [0,1])->get()->count();
            }else{
                $arr_lha  = static::getLhaByObject($user);
                $baru     = static::whereIn('lha_id', $arr_lha)->whereIn('status', [0,1])->get()->count();
            }
        }else{
            if($user->hasRole(['auditor','svp-audit','dirut'])){
                $object_audit = static::where('status', 2)->get()->count();
            }else{
                // $arr_lha  = static::getLhaByObject($user);
                // $object_audit = static::whereIn('lha_id', $arr_lha)->where('status', 2)->get()->count();
                $object_audit = static::where('id', 0)->get()->count();
            }
        }
        return json_decode(json_encode([
            'baru'       => $baru,
            'object_audit' => $object_audit,
            'total'      => $baru + $object_audit
        ]));
    }
}
