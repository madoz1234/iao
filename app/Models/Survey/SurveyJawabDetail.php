<?php

namespace App\Models\Survey;

use App\Models\Auths\User;
use App\Models\Master\SurveyPertanyaan;
use App\Models\Model;
use App\Models\Survey\SurveyJawab;

class SurveyJawabDetail extends Model
{
    /* default */
    protected $table 		= 'trans_survei_jawab_detail';
    protected $fillable 	= ['survei_jawab_id','tanya_id','jawaban'];

    /* data ke log */
    protected $log_table    = 'log_trans_survei_jawab_detail';
    protected $log_table_fk = 'ref_id';
    /* relation */
    public function survey_jawab(){
        return $this->belongsTo(SurveyJawab::class, 'survei_jawab_id' , 'id');
    }
    public function pertanyaan(){
        return $this->belongsTo(SurveyPertanyaan::class, 'tanya_id' , 'id');
    }
    public function user(){
        return $this->belongsTo(User::class, 'user_id' , 'id');
    }


    /* mutator */
    // insert code here


    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
