<?php

namespace App\Models;

use App\Models\Model;
use App\Models\Auths\User;

class SessionLog extends Model
{
    protected $table 		= 'log_activity';
    protected $fillable 	= [
        'modul',
        'sub',
        'aktivitas',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
