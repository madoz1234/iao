<?php

namespace App\Imports;

use Illuminate\Validation\Rule;
use App\Models\Audit\Rencana\RencanaAudit;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class RencanaAuditPkat implements ToModel, WithStartRow, WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    // private $startRow    = 1;
    // private $currentRow  = 0;
    // private $heading     = null;
    // private $headingRow  = 0;

    public function model(array $row)
    {
        // $this->currentRow++;

        // if($this->currentRow == $this->headingRow){
        //     $this->heading = $row;
        // }
        dd($row);
        if (!isset($row[0])) {
	        return null;
	    }
        return new RencanaAudit([
            'nama' => $row[0],
            'nilai' => $row[1], 
            'progress_ra' => $row[2], 
            'progress_ri' => $row[3], 
            'deviasi' => $row[4], 
            'risk_impact' => $row[5], 
            'mapp' => $row[6], 
            'bkpu' => $row[7], 
            'deviasi_bkpu' => $row[8], 
            'bkpu_ri' => $row[9], 
            'keterangan' => $row[10], 
        ]);
    }

    public function startRow(): int
    {
        return 2;
    }
    
    public function rules(): array
	{
	    return [
	        'nim' => Rule::unique('mahasiswa', 'nim'), // Table name, field in your db
	    ];
	}

	public function customValidationMessages()
	{
	    return [
	        'nim.unique' => 'Custom message',
	    ];
	}
}
