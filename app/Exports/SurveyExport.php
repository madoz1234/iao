<?php
namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromView;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class SurveyExport implements FromView, WithTitle, WithEvents
{
    protected $view;
    protected $data;

    public function __construct($view,$data)
    {
        $this->view = $view;
        $this->data = $data;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getDelegate()->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT);
                $event->sheet->getDelegate()->getPageSetup()->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
                $event->sheet->getDelegate()->getPageMargins()->setTop(0.3);
                $event->sheet->getDelegate()->getPageMargins()->setBottom(0.3);
                $event->sheet->getDelegate()->getPageMargins()->setLeft(0.3);
                $event->sheet->getDelegate()->getPageMargins()->setRight(0.3);
                
                $widths = ['A' => 6, 'B' => 51, 'C' => 8, 'D' => 8, 'E' => 8, 'F' => 8, 'G' => 8];
                foreach ($widths as $k => $v) {
                    $event->sheet->getDelegate()->getColumnDimension($k)->setWidth($v);
                }

            },
        ];
    }

    public function view(): View
    {
        $view = 'layouts.default_export';
        $data = [];
        if($this->view && $this->data){
            $view = $this->view;
            $data = $this->data;
        }
        return view($view, [
            'record' => $data
        ]);
    }

    public function title(): string
    {
        $title = isset($this->data['title'])?$this->data['title']:'Default';
        return $title;
    }
}