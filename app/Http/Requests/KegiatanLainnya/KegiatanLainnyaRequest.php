<?php

namespace App\Http\Requests\KegiatanLainnya;

use Illuminate\Foundation\Http\FormRequest;

class KegiatanLainnyaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if ($this->status == 1 && $this->has('reject')) {
            $rules = [
                'ket_svp' => 'required|min:8'
            ];
        }elseif($this->status == 2) {
            $rules = [
                'jenis_rapat'    => 'required',
                'pimpinan_rapat' => 'required',
                'notulen_rapat'  => 'required',
                'tanggal_rapat'  => 'required',
            ];
        }elseif($this->has('add_rapat')) {
            $rules = [
                'judul_audit'     => 'required|max:255',
                'tanggal_mulai'   => 'required',
                'tanggal_selesai' => 'required',
                
                'nomor'           => 'required|max:100|unique:trans_rapat_internal,nomor,'.$this->get('rapat_id'),
                'tanggal'         => 'required',
                'tempat'          => 'required|max:40',
                'jumlah_peserta'  => 'required',
                'jam_mulai'       => 'required',
                'jam_selesai'     => 'required',
                'data.*.agenda'   => 'required|max:200',
            ];
            if ($this->jenis_rapat == 1) {
                $rules += [
                    'detail.*.user_id'          => 'required|max:30',
                ]; 
            }else{
                $rules += [
                    'detail.*.email'            => 'required|email|max:30',
                    'detail.*.perusahaan'       => 'required|max:40',
                ]; 
            }
        }else{
            $rules = [];
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'jenis_rapat.required'         => 'Jenis tidak boleh kosong',
            'pimpinan_rapat.required'      => 'Pimpinan rapat tidak boleh kosong',
            'notulen_rapat.required'       => 'Notulen tidak boleh kosong',
            'tanggal.required'             => 'Tanggal tidak boleh kosong',
            'ket_svp.required'             => 'Alasan penolakan tidak boleh kosong',
            'ket_svp.min'                  => 'Minimal karakter (8)',
            
            'judul_audit.required'         => 'Judul audit tidak boleh kosong',
            'ket_svp.max'                  => 'Maksimal karakter (255)',
            'tanggal_mulai.required'       => 'Jam mulai tidak boleh kosong',
            'tanggal_selesai.required'     => 'Jam selesai tidak boleh kosong',
            
            'nomor.required'               => 'Nomor rapat tidak boleh kosong',
            'nomor.unique'                 => 'Nomor rapat sudah ada',
            'tanggal.required'             => 'Tanggal rapat tidak boleh kosong',
            'tempat.required'              => 'Lokasi rapat tidak boleh kosong',
            'jumlah_peserta.required'      => 'Jumlah peserta tidak boleh kosong',
            'jam_mulai.required'           => 'Jam mulai tidak boleh kosong',
            'jam_selesai.required'         => 'Jam selesai tidak boleh kosong',
            'daftar_hadir.required'        => 'Tidak boleh kosong',
            'other.*.required'             => 'Tidak boleh kosong',
            'data.*.agenda.required'       => 'Agenda tidak kosong',
            'detail.*.user_id.required'    => 'Nama tidak kosong',
            'detail.*.email.required'      => 'Email tidak boleh kosong',
            'detail.*.email.email'         => 'Bukan alamat email',
            'detail.*.perusahaan.required' => 'Perusahaan tidak boleh kosong',
       ];
    }
}
