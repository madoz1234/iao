<?php

namespace App\Http\Requests\Rapat;

use App\Http\Requests\FormRequest;

class RapatEksternalRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $input = $this->all();
        if($this->get('id')){
            if(!$this->get('type')){
                $return = [
                    'nomor'            			=> 'required|max:100|unique:trans_rapat_internal,nomor,'.$this->get('id'),
                    // 'materi'            		=> 'required|max:100',
                    'tanggal'            		=> 'required',
                    // 'waktu'            			=> 'required',
                    'tempat'            		=> 'required|max:40',
                    'jumlah_peserta'            => 'required',
                    // 'keterangan'            	=> 'required|max:2000',
                    'jam_mulai'                 => 'required',
                    'jam_selesai'               => 'required',
                    'status_kategori'           => 'required',
                    'detail.*.email'            => 'required|email|max:30',
                    'detail.*.perusahaan'       => 'required|max:40',
                    'data.*.agenda'             => 'required|max:200',
                ];
            }
        }else{
            $return = [
                'nomor'            			=> 'required|max:100|unique:trans_rapat_internal,nomor,'.$this->get('id'),
                // 'materi'            		=> 'required|max:100',
                'tanggal'            		=> 'required',
                // 'waktu'            			=> 'required',
                'tempat'            		=> 'required|max:40',
                'jumlah_peserta'            => 'required',
                'jam_mulai'                 => 'required',
                'jam_selesai'               => 'required',
                'status_kategori'           => 'required',
                // 'keterangan'                => 'required|max:2000',
                'detail.*.email'            => 'required|email|max:30',
                'detail.*.perusahaan'       => 'required|max:40',
                'data.*.agenda'             => 'required|max:200',
                // 'daftar_hadir'              => 'required',
                // 'other.*'                   => 'required',
            ];
        }
		return $return;
    }

    public function messages()
    {
    	return [
        	'nomor.required'            	=> 'Nomor Rapat tidak boleh kosong',
            'nomor.unique'                  => 'Nomor Rapat sudah ada',
        	// 'materi.required'            	=> 'Tema / Materi Rapat tidak boleh kosong',
        	'tanggal.required'            	=> 'Tanggal Rapat tidak boleh kosong',
        	// 'waktu.required'            	=> 'Pukul tidak boleh kosong',
        	'tempat.required'               => 'Lokasi Rapat tidak boleh kosong',
        	'jumlah_peserta.required'       => 'Jumlah Peserta tidak boleh kosong',
        	'daftar_hadir.required'         => 'Tidak boleh kosong',
            'other.*.required'              => 'Tidak boleh kosong',
            'detail.*.email.required'       => 'Email Tidak boleh kosong',
        	'detail.*.email.email'          => 'Bukan Alamat Email',
            'detail.*.perusahaan.required'  => 'Perusahaan Tidak boleh kosong',
            'jam_mulai.required'            => 'Jam Mulai tidak boleh kosong',
            'jam_selesai.required'          => 'Jam Selesai tidak boleh kosong',
            'status_kategori.required'      => 'Kategori tidak boleh kosong',
            // 'keterangan.required'           => 'Keterangan Rapat tidak boleh kosong',
            'data.*.agenda.required'        => 'Agenda Tidak kosong',
            // 'detail.*.user_id.email'                  => 'Bukan Alamat Email',
       ];
    }
}
