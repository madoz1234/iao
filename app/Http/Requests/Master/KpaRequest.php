<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class KpaRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'elemen_id'            			=> 'required',
            'data.*.detail.*.kpa'           => 'required',
            'data.*.detail.*.deskripsi'     => 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'elemen_id.required'            			=> 'Elemen tidak boleh kosong',
        	'data.*.detail.*.kpa.required'            	=> 'Deskripsi KPA tidak boleh kosong',
        	'data.*.detail.*.deskripsi.required'        => 'Deskripsi tidak boleh kosong',
       ];
    }
}
