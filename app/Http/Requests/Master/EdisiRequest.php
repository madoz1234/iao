<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class EdisiRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'edisi'            			=> 'required|max:200|unique:ref_edisi,edisi,'.$this->get('id'),
            'revisi'            		=> 'required|max:3',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'edisi.required'              => 'Edisi tidak boleh kosong',
        	'edisi.unique'                => 'Edisi sudah ada',
        	'edisi.max'                	  => 'Maks 200 Karakter',
        	'revisi.max'                  => 'Maks 3 Karakter',
        	'revisi.required'             => 'Revisi tidak boleh kosong',
       ];
    }
}
