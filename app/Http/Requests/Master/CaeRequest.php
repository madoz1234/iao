<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class CaeRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'nama'            			=> 'required|max:200|unique:ref_cae,nama,'.$this->get('id'),
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'nama.required'            		=> 'Nama tidak boleh kosong',
        	'nama.unique'            		=> 'Nama sudah ada',
       ];
    }
}
