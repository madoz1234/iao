<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class SurveyRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'version'            			=> 'required|max:20|unique:ref_survei,version,'.$this->get('id'),
            'detail.*.pertanyaan'           => 'required|distinct',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'version.required'              			=> 'Version tidak boleh kosong',
        	'version.unique'                			=> 'Version sudah ada',
        	'version.max'                				=> 'Maks 20 Karakter',
            'detail.*.pertanyaan.required'              => 'Pertanyaan tidak boleh kosong',
            'detail.*.pertanyaan.distinct'              => 'Pertanyaan tidak boleh sama',
       ];
    }
}
