<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class SpinPemenuhanRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'pof_id'            			=> 'required',
            'detail.*.deskripsi'            => 'required|distinct',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'pof_id.required'               => 'SPIN Point of Focus (PoF) tidak boleh kosong',
            'detail.*.deskripsi.required'   => 'Konten tidak boleh kosong',
            'detail.*.deskripsi.distinct'   => 'Konten tidak boleh sama',
       ];
    }
}