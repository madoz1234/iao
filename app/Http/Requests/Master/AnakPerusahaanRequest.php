<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class AnakPerusahaanRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'nama'            			=> 'required|max:200|unique:ref_anak_perusahaan,nama,'.$this->get('id'),
            'kode'            			=> 'required|max:200|unique:ref_anak_perusahaan,kode,'.$this->get('id'),
            'pic'            			=> 'required',
            'no_tlp'            		=> 'max:30',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'nama.required'            		=> 'Anak Perusahaan tidak boleh kosong',
        	'kode.required'            		=> 'Kode AP tidak boleh kosong',
        	'pic.required'            		=> 'PIC tidak boleh kosong',
        	'pic.unique'            		=> 'PIC sudah ada',
        	'nama.unique'            		=> 'Anak Perusahaan sudah ada',
        	'kode.unique'            		=> 'Kode AP sudah ada',
        	'no_tlp.max'            		=> 'Maks 30 Karakter',
       ];
    }
}
