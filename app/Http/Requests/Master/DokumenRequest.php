<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class DokumenRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'judul'            			=> 'required|max:200|unique:ref_dokumen,judul,'.$this->get('id'),
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'judul.required'              => 'Judul tidak boleh kosong',
        	'judul.unique'                => 'Judul sudah ada',
        	'judul.max'                	  => 'Maks 200 Karakter',
       ];
    }
}
