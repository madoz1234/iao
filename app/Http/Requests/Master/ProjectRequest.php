<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;
use Illuminate\Validation\Rule;

class ProjectRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$id = $this->get('id');
    	$return = [
            'nama'            			=> 'required|max:200|unique:ref_project,nama,'.$this->get('id'),
            'project_ab'            	=> 'required|max:7|unique:ref_project,project_ab,'.$this->get('id'),
            'project_id'            	=> 'required|max:7|unique:ref_project,project_id,'.$this->get('id'),
            'bu_id'            			=> 'required',
            'pic'            			=> 'required',
            // 'pic'            			=> ['required', Rule::unique('ref_project_pic', 'id')->ignore($this->route()->id)->where(function ($query) {
     							// 									$query->where('project_id', $this->request->get('id'));
												// 		}),],
            'no_tlp'            		=> 'max:30',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'nama.required'            		=> 'Project tidak boleh kosong',
        	'project_ab.required'           => 'No AB tidak boleh kosong',
        	'project_id.required'           => 'Project ID tidak boleh kosong',
        	'bu_id.required'            	=> 'Business Unit tidak boleh kosong',
        	'pic.required'            		=> 'PIC tidak boleh kosong',
        	// 'pic.unique'            		=> 'PIC Sudah ada',
        	'nama.unique'            		=> 'Project sudah ada',
        	'project_ab.unique'            	=> 'No AB sudah ada',
        	'project_id.unique'            	=> 'Project ID sudah ada',
        	'project_ab.max'                => 'Maks 7 Karakter',
        	'project_id.max'                => 'Maks 7 Karakter',
        	'no_tlp.max'            		=> 'Maks 30 Karakter',
       ];
    }
}
