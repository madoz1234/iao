<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class FokusAuditRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'bidang'            			=> 'required',
            'audit'            				=> 'required|max:200|unique_with:ref_fokus_audit,bidang,'.$this->get('id'),
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'bidang.required'            	=> 'Bidang tidak boleh kosong',
        	'audit.required'            	=> 'Fokus Audit tidak boleh kosong',
        	'audit.max'            			=> 'Maksimum Karakter 200',
        	'audit.unique_with'            	=> 'Fokus Audit sudah ada',
       ];
    }
}
