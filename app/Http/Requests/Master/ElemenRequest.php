<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class ElemenRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'elemen'            			=> 'required|max:300|unique:ref_elemen,elemen,'.$this->get('id'),
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'elemen.required'            		=> 'Elemen tidak boleh kosong',
        	'elemen.unique'            		    => 'Elemen sudah ada',
        	'elemen.max'            		    => 'Maks 300 Karakter',
       ];
    }
}
