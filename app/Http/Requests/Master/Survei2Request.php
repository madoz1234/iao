<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class Survei2Request extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$data =[];
    	foreach ($input['detail'] as $key => $value) {
    		if($value['tipe'] == 1){
		    	$return = [
		            'version'            				=> 'required|max:200|unique:ref_survei,version,'.$this->get('id'),
		            'description'      			    	=> 'required',
		        ];
		        $return['detail.'.$key.'.tipe'] 		= 'required|in:1,2,3,4';
		        $return['detail.'.$key.'.pertanyaan'] 	= 'required';
		        $return['detail.'.$key.'.sum'] 			= 'required';
    		}elseif($value['tipe'] == 4){
    			$return = [
		            'version'            				=> 'required|max:200|unique:ref_survei,version,'.$this->get('id'),
		            'description'      			    	=> 'required',
		        ];

		        $return['detail.'.$key.'.pertanyaan'] 	= 'required';
		        $return['detail.'.$key.'.tipe'] 		= 'required|in:1,2,3,4';
		        $return['detail.'.$key.'.pilgan.*'] 	= 'required';
    		}else{
    			$return = [
		            'version'            				=> 'required|max:200|unique:ref_survei,version,'.$this->get('id'),
		            'description'      			   	 	=> 'required',
		        ];

		        $return['detail.'.$key.'.pertanyaan'] 	= 'required';
		        $return['detail.'.$key.'.tipe'] 		= 'required|in:1,2,3,4';
    		}
    		$data += $return;
    	}
		return $data;
    }

    public function messages()
    {
    	return [
        	'version.required'            	=> 'Versioning tidak boleh kosong',
        	'version.unique'            	=> 'Versioning sudah ada',
        	'description.required'          => 'Description tidak boleh kosong',
        	'detail.*.pertanyaan.required'  => 'Data Pertanyaan tidak boleh kosong',
        	'detail.*.tipe.required'      	=> 'Data Tipe tidak boleh kosong',
        	'detail.*.sum.required'      	=> 'Data Jumlah Rating tidak boleh kosong',
        	'detail.*.pilgan.*.required'    => 'Data Jawaban tidak boleh kosong',
        	'detail.*.tipe.in' 				=> 'Pilih salah satu',
       ];
    }
}
