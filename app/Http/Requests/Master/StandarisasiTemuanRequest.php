<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class StandarisasiTemuanRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'kode'            				=> 'required|max:200|unique:ref_standarisasi_temuan,kode,'.$this->get('id'),
            'standarisasi'            		=> 'required',
            'bidang'            			=> 'required',
            // 'detail.*.deskripsi'   			=> 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
    		'bidang.required'            	=> 'Bidang tidak boleh kosong',
        	'kode.required'            		=> 'Kode tidak boleh kosong',
        	// 'standarisasi.required'            => 'Standarisasi tidak boleh kosong',
        	'detail.*.deskripsi.required'   => 'Referensi / Kriteria tidak boleh kosong',
        	'kode.unique'            		=> 'Kode sudah ada',
        	'kode.max'            			=> 'Maksimum Karakter 50',
       ];
    }
}
