<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class KertasKerjaRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'spin_id'            		=> 'required|unique:ref_spin_kertas_kerja,spin_id,'.$this->get('id'),
            'kategori'               	=> 'required',
            'kategori_id'    			=> 'required',

        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'spin_id.required'              			=> 'SPIN tidak boleh kosong',
        	'kategori.required'              			=> 'Kategori tidak boleh kosong',
        	'kategori_id.required'              		=> 'Objek Audit tidak boleh kosong',
        	'spin_id.unique'                			=> 'SPIN sudah ada',
       ];
    }
}
