<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class SpinPofRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'judul'            			=> 'required|max:300|unique:ref_spin_pof,judul,'.$this->get('id'),
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'judul.required'              => 'Point of Focus (PoF) tidak boleh kosong',
        	'judul.unique'                => 'Point of Focus (PoF) sudah ada',
        	'judul.max'                	  => 'Maks 300 Karakter',
       ];
    }
}
