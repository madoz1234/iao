<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class TemuanRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'standarisasi_id'            					=> 'required|unique:ref_temuan,standarisasi_id,'.$this->get('id'),
            'detail.*.deskripsi'            				=> 'required|distinct',
            'detail.*.kode'            						=> 'required|distinct',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'kategori_id.required'            				=> 'Standarisasi Temuan tidak boleh kosong',
        	'kategori_id.unique'            				=> 'Standarisasi Temuan sudah ada',
        	'detail.*.kode.required'            			=> 'Kode Temuan tidak boleh kosong',
        	'detail.*.kode.distinct'            			=> 'Kode Temuan tidak boleh sama',
        	'detail.*.deskripsi.required'            		=> 'Deskripsi Temuan tidak boleh kosong',
        	'detail.*.deskripsi.distinct'            		=> 'Deskripsi Temuan tidak boleh sama',
        	'standarisasi_id.required'            			=> 'Standarisasi Temuan tidak boleh kosong',
        	'standarisasi_id.unique'            			=> 'Standarisasi Temuan sudah ada',
       ];
    }
}
