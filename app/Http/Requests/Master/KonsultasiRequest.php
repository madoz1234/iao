<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class KonsultasiRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'nama'            			=> 'required|max:200|unique:ref_konsultasi,nama,'.$this->get('id'),
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'nama.required'            		=> 'Kegiatan Konsultasi tidak boleh kosong',
        	'nama.unique'            		=> 'Kegiatan Konsultasi sudah ada',
       ];
    }
}
