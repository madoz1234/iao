<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class LangkahKerjaRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'bidang'            			=> 'required',
            'fokus'            				=> 'required|unique:ref_langkah_kerja,fokus_audit_id,'.$this->get('id'),
            'detail.*.deskripsi'            => 'required|distinct',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'bidang.required'            	=> 'Bidang tidak boleh kosong',
        	'fokus.required'          		=> 'Fokus Audit tidak boleh kosong',
        	'fokus.unique'          		=> 'Fokus Audit sudah ada',
        	'detail.*.deskripsi.required'   => 'Langkah Kerja tidak boleh kosong',
        	'detail.*.deskripsi.distinct'   => 'Langkah Kerja tidak boleh sama',
       ];
    }
}
