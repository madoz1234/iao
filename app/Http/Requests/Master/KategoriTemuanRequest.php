<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class KategoriTemuanRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'nama'            					=> 'required|unique:ref_kategori_temuan,nama,'.$this->get('id'),
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'nama.required'            		=> 'Kategori Temuan tidak boleh kosong',
        	'nama.unique'            		=> 'Kategori Temuan sudah ada',
       ];
    }
}
