<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class SpinPembobotanRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'version'            			=> 'required|max:20|unique:ref_spin_pembobotan,version,'.$this->get('id'),
            'detail.*.pof_id'               => 'required|distinct',
            'detailNilai.*.data.*.nilai'    => 'required|not_in:0',

        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'version.required'              			=> 'Version tidak boleh kosong',
        	'version.unique'                			=> 'Version sudah ada',
        	'version.max'                				=> 'Maks 20 Karakter',
            'detail.*.pof_id.required'      			=> 'Point of Focus (PoF) tidak boleh kosong',
            'detail.*.pof_id.distinct'      			=> 'Point of Focus (PoF) tidak boleh sama',
            'detailNilai.*.data.*.nilai.required'       => 'Bobot tidak boleh kosong',
            'detailNilai.*.data.*.nilai.not_in'       	=> 'Bobot tidak boleh kosong',

       ];
    }
}
