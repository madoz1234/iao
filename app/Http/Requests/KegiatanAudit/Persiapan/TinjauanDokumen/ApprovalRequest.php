<?php

namespace App\Http\Requests\KegiatanAudit\Persiapan\TinjauanDokumen;

use App\Http\Requests\FormRequest;

class ApprovalRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	if($input['approval'] == 2){
			$return = [
	            'approval'            			=> 'required',
	            'ket_svp'            			=> 'required',
	        ];
    	}else{
    		$return = [
	            'approval'            			=> 'required',
	        ];
    	}
		return $return;
    }

    public function messages()
    {
    	return [
        	'approval.required'            				=> 'Pilihan tidak boleh kosong',
            'ket_svp.required'            				=> 'Alasan Penolakan tidak boleh kosong',
       ];
    }
}
