<?php

namespace App\Http\Requests\KegiatanAudit\Persiapan\TinjauanDokumen;

use App\Http\Requests\FormRequest;

class TinjauanDokumenRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
		$return = [
            'nomor'            				=> 'required',
            'tempat'            			=> 'required',
            'tanggal'            			=> 'required',
            'judul'            				=> 'required',
            'email'            				=> 'required|string|email|max:255',
            'dateline'            			=> 'required',
            'detail.*.dokumen'            	=> 'required|distinct',
            'data_tembusan.*.tembusan'      => 'required|distinct',
            'data_user.*.users'      		=> 'required|distinct',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'nomor.required'            				=> 'Nomor tidak boleh kosong',
            'tempat.required'            				=> 'Tempat tidak boleh kosong',
            'tanggal.required'            				=> 'Tanggal tidak boleh kosong',
            'judul.required'            				=> 'Judul tidak boleh kosong',
            'email.required' 							=> 'Data Email tidak boleh kosong',
	        'email.email' 								=> 'Data Email tidak valid',
	        'email.unique' 								=> 'Data Email sudah ada',
            'dateline.required'     					=> 'Deadline tidak boleh kosong',
            'detail.*.dokumen.required'  				=> 'Dokumen Pendahuluan tidak boleh kosong',
            'detail.*.dokumen.distinct'  				=> 'Dokumen Pendahuluan tidak boleh sama',
            'data_tembusan.*.tembusan.required'  		=> 'Tembusan tidak boleh kosong',
            'data_tembusan.*.tembusan.distinct'  		=> 'Tembusan tidak boleh sama',
            'data_user.*.users.required'  				=> 'Penerima tidak boleh kosong',
            'data_user.*.users.distinct'  				=> 'Penerima tidak boleh sama',
       ];
    }
}
