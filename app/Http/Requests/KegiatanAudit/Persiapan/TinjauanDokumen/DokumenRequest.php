<?php

namespace App\Http\Requests\KegiatanAudit\Persiapan\TinjauanDokumen;

use App\Http\Requests\FormRequest;

class DokumenRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	if(!empty($input['akhir'])){
    		if($input['akhir'] == 3){
    			$return = [];
    		}else{
    			$return = [
		            'detail.*.surat'            			=> 'required',
		        ];
    		}
    	}else{
			$return = [
	            'detail.*.surat'            			=> 'required',
	        ];
    	}
		return $return;
    }

    public function messages()
    {
    	return [
        	'detail.*.surat.required'            	=> 'Dokumen tidak boleh kosong',
       ];
    }
}
