<?php

namespace App\Http\Requests\KegiatanAudit\Persiapan\PenugasanAudit;

use App\Http\Requests\FormRequest;

class PenugasanAuditRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
		$return = [
	        // 'details.*.tembusan'  								=> 'required|distinct',
	    ];
    	foreach($input['detail'] as $key => $value) {
    		if(!empty($value['operasional']) && !empty($value['keuangan']) && !empty($value['sistem'])){
    			$return['detail.'.$key.'.ketua'] 													= 'required';
    			foreach ($value['pelaksanaan'] as $kiy => $val) {
    				$return['detail.'.$key.'.pelaksanaan.'.$kiy.'.tanggal'] 						= 'required';
    				foreach ($val['detail'] as $koy => $vil) {
    					$return['detail.'.$key.'.pelaksanaan.'.$kiy.'.detail.'.$koy.'.mulai'] 		= 'required';
    					$return['detail.'.$key.'.pelaksanaan.'.$kiy.'.detail.'.$koy.'.selesai'] 	= 'required';
    					$return['detail.'.$key.'.pelaksanaan.'.$kiy.'.detail.'.$koy.'.item'] 		= 'required';
    				}
    			}
    		}else{
				$return['detail.'.$key.'.ketua'] 													= 'required';
				$return['detail.'.$key.'.operasional'] 												= 'required';
				$return['detail.'.$key.'.keuangan'] 												= 'required';
				$return['detail.'.$key.'.sistem'] 													= 'required';
    			foreach ($value['pelaksanaan'] as $kiy => $val) {
    				$return['detail.'.$key.'.pelaksanaan.'.$kiy.'.tanggal'] 	= 'required';
    				foreach ($val['detail'] as $koy => $vil) {
    					$return['detail.'.$key.'.pelaksanaan.'.$kiy.'.detail.'.$koy.'.mulai'] 		= 'required';
    					$return['detail.'.$key.'.pelaksanaan.'.$kiy.'.detail.'.$koy.'.selesai'] 	= 'required';
    					$return['detail.'.$key.'.pelaksanaan.'.$kiy.'.detail.'.$koy.'.item'] 		= 'required';
    				}
    			}
    		}
    	}
		return $return;
    }

    public function messages()
    {
    	return [
        	'detail.*.ketua.required'     							=> 'Ketua Tim tidak boleh kosong',
        	'detail.*.operasional.required'     					=> 'Auditor Operasional tidak boleh kosong',
        	'detail.*.keuangan.required'     						=> 'Auditor Keuangan tidak boleh kosong',
        	'detail.*.sistem.required'     							=> 'Auditor Sistem tidak boleh kosong',
        	'detail.*.pelaksanaan.*.tanggal.required'     			=> 'Tanggal Pelaksanaan tidak boleh kosong',
            'detail.*.pelaksanaan.*.detail.*.mulai.required'  		=> 'Waktu Mulai tidak boleh kosong',
            'detail.*.pelaksanaan.*.detail.*.selesai.required'  	=> 'Waktu Selesai tidak boleh kosong',
            'detail.*.pelaksanaan.*.detail.*.item.required'  		=> 'Item tidak boleh kosong',
            // 'details.*.tembusan.required'  							=> 'Tembusan tidak boleh kosong',
            // 'details.*.tembusan.distinct'  							=> 'Tembusan tidak boleh sama',
       ];
    }
}
