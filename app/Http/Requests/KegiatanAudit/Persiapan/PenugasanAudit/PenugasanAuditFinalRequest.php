<?php

namespace App\Http\Requests\KegiatanAudit\Persiapan\PenugasanAudit;

use App\Http\Requests\FormRequest;

class PenugasanAuditFinalRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
		$return = [
            'no_surat'   => 'required',
            'surat'   	 => 'required',
            'tgl_surat'   	 => 'required',

        ];
    	
		return $return;
    }

    public function messages()
    {
    	return [
        	'no_surat.required'     => 'No Surat Penugasan tidak boleh kosong',
        	'surat.required'        => 'Surat Penugasan tidak boleh kosong',
        	'tgl_surat.required'    => 'Tgl Surat tidak boleh kosong',
       ];
    }
}
