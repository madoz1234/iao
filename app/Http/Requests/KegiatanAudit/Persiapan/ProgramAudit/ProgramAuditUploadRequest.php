<?php

namespace App\Http\Requests\KegiatanAudit\Persiapan\ProgramAudit;

use App\Http\Requests\FormRequest;

class ProgramAuditUploadRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
		$return = [
            'lampiran'            				   => 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'lampiran.required'            			=> 'File Scan Program Audit tidak boleh kosong',
       ];
    }
}
