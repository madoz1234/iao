<?php

namespace App\Http\Requests\KegiatanAudit\Persiapan\ProgramAudit;

use App\Http\Requests\FormRequest;

class FokusAuditRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'detail.*.fokus_audit.*.detail.*.bidang'  		=> 'required',
            // 'detail.*.fokus_audit.*.detail.*.rencana'  		=> 'required',
            // 'detail.*.fokus_audit.*.detail.*.realisasi'  	=> 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
            'detail.*.fokus_audit.*.detail.*.bidang.required'  		=> 'Fokus Audit tidak boleh kosong',
            // 'detail.*.fokus_audit.*.detail.*.rencana.required'  	=> 'Rencana tidak boleh kosong',
            // 'detail.*.fokus_audit.*.detail.*.realisasi.required'  	=> 'Realisasi tidak boleh kosong',
       ];
    }
}
