<?php

namespace App\Http\Requests\KegiatanAudit\Persiapan\ProgramAudit;

use App\Http\Requests\FormRequest;

class ProgramAuditRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	if(!empty($input['operasional']) && !empty($input['keuangan']) && !empty($input['sistem'])){
    		$return = [
	            'ruang_lingkup'            						=> 'required',
	            'sasaran'            							=> 'required',
	            'detail.*.persiapan.*.persiapan'     			=> 'required',
	            'detail.*.pelaksanaan.*.tanggal'     			=> 'required',
	            'detail.*.pelaksanaan.*.detail.*.mulai'  		=> 'required',
	            'detail.*.pelaksanaan.*.detail.*.selesai'  		=> 'required',
	            'detail.*.pelaksanaan.*.detail.*.item'  		=> 'required',
	            'detail.*.penyelesaian.*.mulai'  				=> 'required',
	            'detail.*.penyelesaian.*.selesai'  				=> 'required',
	            'detail.*.penyelesaian.*.item'  				=> 'required',
	            // 'detail.*.fokus_audit.*.detail.*.bidang'  		=> 'required',
	            // 'detail.*.fokus_audit.*.detail.*.rencana'  		=> 'required',
	            // 'detail.*.fokus_audit.*.detail.*.realisasi'  	=> 'required',
	        ];
    	}else{
	    	$return = [
	            // 'ketua'            								=> 'required',
	            'operasional'   								=> 'required',
	            'keuangan'   									=> 'required',
	            'sistem'   										=> 'required',
	            'ruang_lingkup'            						=> 'required',
	            'sasaran'            							=> 'required',
	            'detail.*.persiapan.*.persiapan'     			=> 'required',
	            'detail.*.pelaksanaan.*.tanggal'     			=> 'required',
	            'detail.*.pelaksanaan.*.detail.*.mulai'  		=> 'required',
	            'detail.*.pelaksanaan.*.detail.*.selesai'  		=> 'required',
	            'detail.*.pelaksanaan.*.detail.*.item'  		=> 'required',
	            'detail.*.penyelesaian.*.mulai'  				=> 'required',
	            'detail.*.penyelesaian.*.selesai'  				=> 'required',
	            'detail.*.penyelesaian.*.item'  				=> 'required',
	            // 'detail.*.fokus_audit.*.detail.*.bidang'  		=> 'required',
	            // 'detail.*.fokus_audit.*.detail.*.rencana'  		=> 'required',
	            // 'detail.*.fokus_audit.*.detail.*.realisasi'  	=> 'required',
	        ];
    	}
		return $return;
    }

    public function messages()
    {
    	return [
        	// 'detail.*.object_audit.required'            			=> 'Objek Audit tidak boleh kosong',
            // 'detail.*.ketua.required'            					=> 'Ketua Team Auditor tidak boleh kosong',
            // 'detail.*.anggota.required'            					=> 'Anggota Auditor tidak boleh kosong',
            // 'ketua.required'            							=> 'Ketua Tim Auditor tidak boleh kosong',
            'operasional.required'            						=> 'Auditor Operasional tidak boleh kosong',
            'keuangan.required'            							=> 'Auditor Keuangan tidak boleh kosong',
            'sistem.required'            							=> 'Auditor Sistem tidak boleh kosong',
            'ruang_lingkup.required'            					=> 'Ruang Lingkup tidak boleh kosong',
            'sasaran.required'            							=> 'Sasaran tidak boleh kosong',
            'detail.*.persiapan.*.persiapan.required'     			=> 'Persiapan tidak boleh kosong',
            'detail.*.pelaksanaan.*.tanggal.required'     			=> 'Tanggal Pelaksanaan tidak boleh kosong',
            'detail.*.pelaksanaan.*.detail.*.mulai.required'  		=> 'Waktu Mulai tidak boleh kosong',
            'detail.*.pelaksanaan.*.detail.*.selesai.required'  	=> 'Waktu Selesai tidak boleh kosong',
            'detail.*.pelaksanaan.*.detail.*.item.required'  		=> 'Item tidak boleh kosong',
            'detail.*.penyelesaian.*.mulai.required'  				=> 'Waktu Mulai Penyelesaian tidak boleh kosong',
            'detail.*.penyelesaian.*.selesai.required'  			=> 'Waktu Selesai Penyelesaian tidak boleh kosong',
            'detail.*.penyelesaian.*.item.required'  				=> 'Item Penyelesaian tidak boleh kosong',
            // 'detail.*.fokus_audit.*.detail.*.bidang.required'  		=> 'Fokus Audit tidak boleh kosong',
            // 'detail.*.fokus_audit.*.detail.*.rencana.required'  	=> 'Rencana tidak boleh kosong',
            // 'detail.*.fokus_audit.*.detail.*.realisasi.required'  	=> 'Realisasi tidak boleh kosong',
       ];
    }
}
