<?php

namespace App\Http\Requests\KegiatanAudit\Persiapan\ProgramAudit;

use App\Http\Requests\FormRequest;

class UbahTimRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
		if(!empty($input['operasional']) && !empty($input['keuangan']) && !empty($input['sistem'])){
    		$return = [
	            // 'detail.*.fokus_audit.*.detail.*.realisasi'  	=> 'required',
	        ];
    	}else{
	    	$return = [
	            'operasional'   								=> 'required',
	            'keuangan'   									=> 'required',
	            'sistem'   										=> 'required',
	            // 'detail.*.fokus_audit.*.detail.*.realisasi'  	=> 'required',
	        ];
    	}
		return $return;
    }

    public function messages()
    {
    	return [
            'operasional.required'            						=> 'Auditor Operasional tidak boleh kosong',
            'keuangan.required'            							=> 'Auditor Keuangan tidak boleh kosong',
            'sistem.required'            							=> 'Auditor Sistem tidak boleh kosong',
            // 'detail.*.fokus_audit.*.detail.*.realisasi.required'  	=> 'Realisasi tidak boleh kosong',
       ];
    }
}
