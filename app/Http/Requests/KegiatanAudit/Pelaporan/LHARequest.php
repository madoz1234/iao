<?php

namespace App\Http\Requests\KegiatanAudit\Pelaporan;

use App\Http\Requests\FormRequest;
use Illuminate\Validation\Rule;

class LHARequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	if($input['status_proyek'] == 1){
    		$return = [
	            'memo'   					=> 'required|unique:trans_lha,memo,'.$this->get('id'),
	            'tanggal'   				=> 'required',
	            'pemilik'   				=> 'required',
	            'sumber'   					=> 'required',
	            'sifat'   					=> 'required',
	            'nomor_spmk'   				=> 'required',
	            'nilai_kontrak'   			=> 'required',
	            'tanggal_kontrak'   		=> 'required',
	            'tanggal_akhir_kontrak'   	=> 'required',
	            'waktu_pelaksanaaan'   		=> 'required',
	            'waktu_pemeliharaan'   		=> 'required',
	            'pembayaran'   				=> 'required',
	            'konsultan_perencana'   	=> 'required',
	            'konsultan_pengawas'   		=> 'required',
	            'bkpu'   					=> 'required',
	            'progress'   				=> 'required',
	            'pu'   						=> 'required',
	            'cash'   					=> 'required',
	            'piutang'   				=> 'required',
	            'tagihan'   				=> 'required',
	            'tgl_bkpu'   				=> 'required',
	            'tgl_progress'   			=> 'required',
	            'tgl_pu'   					=> 'required',
	            'tgl_cash'   				=> 'required',
	            'tgl_piutang'   			=> 'required',
	            'tgl_tagihan'   			=> 'required',
	            'adendum.*.no_kontrak'   	=> 'required',
	            'adendum.*.nilai'   		=> 'required',
	            'adendum.*.tgl_awal'   		=> 'required',
	            'adendum.*.tgl_akhir'   	=> 'required',
	            'lingkup.*.uraian'   		=> 'required',
	            'lingkup.*.kontrak'   		=> 'required',
	            'lingkup.*.rencana'   		=> 'required',
	            'lingkup.*.realisasi'   	=> 'required',
	        ];
    	}else{
    		$return = [
	            'memo'   					=> 'required|unique:trans_lha,memo,'.$this->get('id'),
	            'tanggal'   				=> 'required',
	        ];
    	}
    	
		return $return;
    }

    public function messages()
    {
    	return [
        	'memo.required'       				=> 'Memo tidak boleh kosong',
        	'tanggal.required'      			=> 'Tanggal tidak boleh kosong',
        	'memo.unique'         				=> 'Memo sudah ada',
        	'pemilik.required'   				=> 'Pemilik Proyek tidak boleh kosong',
            'sumber.required'   				=> 'Sumber Dana tidak boleh kosong',
            'sifat.required'   					=> 'Sifat Kontrak tidak boleh kosong',
            'nomor_spmk.required'   			=> 'Nomor SPMK tidak boleh kosong',
            'nilai_kontrak.required'   			=> 'Nilai Kontrak (include PPN) tidak boleh kosong',
            'tanggal_kontrak.required'   		=> 'Tanggal Kontrak tidak boleh kosong',
            'tanggal_akhir_kontrak.required'   	=> 'Tanggal Akhir Kontrak tidak boleh kosong',
            'waktu_pelaksanaaan.required'   	=> 'Waktu Pelaksanaan tidak boleh kosong',
            'waktu_pemeliharaan.required'   	=> 'Waktu Pemeliharaan tidak boleh kosong',
            'pembayaran.required'   			=> 'Pembayaran tidak boleh kosong',
            'konsultan_perencana.required'   	=> 'Konsultan Perencana tidak boleh kosong',
            'konsultan_pengawas.required'   	=> 'Konsultan Pengawas tidak boleh kosong',
            'bkpu.required'   					=> 'BK/PU tidak boleh kosong',
            'progress.required'   				=> 'Progress tidak boleh kosong',
            'pu.required'   					=> 'PU tidak boleh kosong',
            'cash.required'   					=> 'Cash tidak boleh kosong',
            'piutang.required'   				=> 'Piutang tidak boleh kosong',
            'tagihan.required'   				=> 'Tagihan Bruto tidak boleh kosong',
            'tgl_bkpu.required'   				=> 'Tanggal BK/PU tidak boleh kosong',
            'tgl_progress.required'   			=> 'Tanggal Progress tidak boleh kosong',
            'tgl_pu.required'   				=> 'Tanggal PU tidak boleh kosong',
            'tgl_cash.required'   				=> 'Tanggal Cash tidak boleh kosong',
            'tgl_piutang.required'   			=> 'Tanggal Piutang tidak boleh kosong',
            'tgl_tagihan.required'   			=> 'Tanggal Tagihan Bruto tidak boleh kosong',
            'adendum.*.no_kontrak.required'   	=> 'Nomor Kontrak Adendum tidak boleh kosong',
            'adendum.*.nilai.required'   		=> 'Nilai Kontrak (include PPN) tidak boleh kosong',
            'adendum.*.tgl_awal.required'   	=> 'Tanggal Kontrak Adendum tidak boleh kosong',
            'adendum.*.tgl_akhir.required'   	=> 'Tanggal Akhir Kontrak Adendum tidak boleh kosong',
            'lingkup.*.uraian.required'   		=> 'Uraian tidak boleh kosong',
            'lingkup.*.kontrak.required'   		=> 'Kotrak tidak boleh kosong',
            'lingkup.*.rencana.required'   		=> 'Rencana tidak boleh kosong',
            'lingkup.*.realisasi.required'   	=> 'Realisasi tidak boleh kosong',
       ];
    }
}
