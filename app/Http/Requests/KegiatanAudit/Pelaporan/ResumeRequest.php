<?php

namespace App\Http\Requests\KegiatanAudit\Pelaporan;

use App\Http\Requests\FormRequest;

class ResumeRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'detail.*.data.*.keterangan'      => 'required|max:1500',
            'data.*.kesimpulan'      		  => 'required|max:1500',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'detail.*.data.*.keterangan.required'   => 'Keterangan tidak boleh kosong',
        	'detail.*.data.*.keterangan.max'   		=> 'Max (1500) Karakter',
        	'data.*.kesimpulan.required'   			=> 'Kesimpulan tidak boleh kosong',
        	'data.*.kesimpulan.max'   				=> 'Max (1500) Karakter',
       ];
    }
}
