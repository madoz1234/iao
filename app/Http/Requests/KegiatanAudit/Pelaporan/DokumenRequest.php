<?php

namespace App\Http\Requests\KegiatanAudit\Pelaporan;

use App\Http\Requests\FormRequest;

class DokumenRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'berkas.*.data'            			=> 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'berkas.*.data.required'            => 'Berkas tidak boleh kosong',
       ];
    }
}
