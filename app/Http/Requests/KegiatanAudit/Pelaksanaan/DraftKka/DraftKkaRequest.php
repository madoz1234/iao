<?php

namespace App\Http\Requests\KegiatanAudit\Pelaksanaan\DraftKka;

use App\Http\Requests\FormRequest;
use Illuminate\Validation\Rule;

class DraftKkaRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	if($input['kka'] == 0){
    		$return = [];
    	}else{
			$return = [
	            'fokus_audit.*.detail.*.standarisasi_id'   				=> 'required|distinct',
	            'fokus_audit.*.detail.*.catatan_kondisi'   				=> 'required',
	            'fokus_audit.*.detail.*.sebab'   						=> 'required',
	            'fokus_audit.*.detail.*.risiko'   						=> 'required',
	            'fokus_audit.*.detail.*.tanggapan'   					=> 'required',
	            'fokus_audit.*.detail.*.kategori_id'   					=> 'required',
	            'fokus_audit.*.detail.*.detail_rekomen.*.rekomendasi'   => 'required',
	        ];
	    	foreach ($input['fokus_audit'] as $key => $value) {
	    		foreach ($value['detail'] as $kuy => $vil) {
	    			if($vil['tanggapan'] == 0){
	    				$return['fokus_audit.'.$key.'.detail.'.$kuy.'.tanggal'] 		= 'required';
	    			}else{
	    				$return['fokus_audit.'.$key.'.detail.'.$kuy.'.catatan'] 		= 'required';
	    			}

	    			if(!empty($vil['data_id'])){
	    			}else{
	    				// $return['fokus_audit.'.$key.'.detail.'.$kuy.'.lampiran'] 		= 'required';
	    			}
	    		}
	    	}
    	}
		return $return;
    }

    public function messages()
    {
    	return [
        	'fokus_audit.*.detail.*.standarisasi_id.required'   				=> 'Standarisasi Temuan tidak boleh kosong',
        	'fokus_audit.*.detail.*.standarisasi_id.distinct'   				=> 'Standarisasi Temuan tidak boleh sama',
        	'fokus_audit.*.detail.*.catatan_kondisi.required'   				=> 'Kondisi tidak boleh kosong',
        	'fokus_audit.*.detail.*.sebab.required'        						=> 'Sebab tidak boleh kosong',
        	'fokus_audit.*.detail.*.risiko.required'        					=> 'Risiko tidak boleh kosong',
        	'fokus_audit.*.detail.*.tanggal.required'        					=> 'Tanggal Tindak Lanjut tidak boleh kosong',
        	'fokus_audit.*.detail.*.tanggapan.required'        					=> 'Tanggapan tidak boleh kosong',
        	'fokus_audit.*.detail.*.catatan.required'        					=> 'Catatan Tanggapan Auditee tidak boleh kosong',
        	'fokus_audit.*.detail.*.kategori_id.required'       				=> 'Kategori Temuan tidak boleh kosong',
        	// 'fokus_audit.*.detail.*.lampiran.required'       					=> 'Lampiran tidak boleh kosong',
        	'fokus_audit.*.detail.*.detail_rekomen.*.rekomendasi.required'      => 'Rekomendasi tidak boleh kosong',
       ];
    }
}
