<?php

namespace App\Http\Requests\KegiatanAudit\Pelaksanaan\DraftKka;

use App\Http\Requests\FormRequest;

class DraftKkaUploadRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
		$return = [
            'lampiran'            				   => 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'lampiran.required'            			=> 'Lampiran tidak boleh kosong',
       ];
    }
}
