<?php

namespace App\Http\Requests\KegiatanAudit\Pelaksanaan\DraftKka;

use App\Http\Requests\FormRequest;
use Illuminate\Validation\Rule;

class DraftKkaUbahRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	if($input['kka'] == 0){
    		$return = [];
    	}else{
			$return = [
	            'fokus_audit.*.detail.*.tanggapan'   		=> 'required',
	        ];
	    	foreach ($input['fokus_audit'] as $key => $value) {
	    		foreach ($value['detail'] as $kuy => $vil) {
	    			if($vil['tanggapan'] == 0){
	    				$return['fokus_audit.'.$key.'.detail.'.$kuy.'.tanggal'] 		= 'required';
	    			}else{
	    				$return['fokus_audit.'.$key.'.detail.'.$kuy.'.catatan'] 		= 'required';
	    			}
	    		}
	    	}
    	}
		return $return;
    }

    public function messages()
    {
    	return [
        	'fokus_audit.*.detail.*.tanggal.required'        	=> 'Tanggal Tindak Lanjut tidak boleh kosong',
        	'fokus_audit.*.detail.*.tanggapan.required'        	=> 'Tanggapan tidak boleh kosong',
        	'fokus_audit.*.detail.*.catatan.required'        	=> 'Catatan Tanggapan Auditee tidak boleh kosong',
       ];
    }
}
