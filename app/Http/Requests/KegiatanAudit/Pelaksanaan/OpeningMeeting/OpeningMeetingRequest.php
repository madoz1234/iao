<?php

namespace App\Http\Requests\KegiatanAudit\Pelaksanaan\OpeningMeeting;

use App\Http\Requests\FormRequest;

class OpeningMeetingRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	if($input['tipe'] == 0){
    		if($input['status'] > 0){
				$return = [
		            'tanggal'   			=> 'required',
		            'tempat'   				=> 'required',
		            'mulai'   				=> 'required',
		            'selesai'   			=> 'required',
		            'lampiran'   			=> 'required',
		            'hadir'   				=> 'required',
		            'detail.*.peserta'   	=> 'required|distinct',
		            'foto.*'   				=> 'required',
		        ];
    		}else{
    			$return = [];
    		}
    	}else{
    		if($input['status'] > 0){
				$return = [
		            'tanggal'   			=> 'required',
		            'tempat'   				=> 'required',
		            'mulai'   				=> 'required',
		            'selesai'   			=> 'required',
		            // 'lampiran'   			=> 'required',
		            // 'hadir'   				=> 'required',
		            'detail.*.peserta'   	=> 'required|distinct',
		            // 'foto.*'   				=> 'required',
		        ];
    		}else{
    			$return = [];
    		}
    	}
    	
		return $return;
    }

    public function messages()
    {
    	return [
        	'tanggal.required'        	=> 'Tanggal Opening Meeting tidak boleh kosong',
        	'tempat.required'        	=> 'Tempat tidak boleh kosong',
        	'detail.*.peserta.required' => 'Peserta tidak boleh kosong',
        	'detail.*.peserta.distinct' => 'Peserta tidak boleh sama',
        	'foto.*.required' 			=> 'Foto Meeting tidak boleh kosong',
        	'lampiran.required' 		=> 'Lampiran tidak boleh kosong',
        	'hadir.required' 			=> 'Daftar Hadir tidak boleh kosong',
        	'mulai.required' 			=> 'Waktu Mulai tidak boleh kosong',
        	'selesai.required' 			=> 'Waktu Selesai tidak boleh kosong',
       ];
    }
}
