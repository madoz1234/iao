<?php

namespace App\Http\Requests\KegiatanAudit\TindakLanjut;

use App\Http\Requests\FormRequest;

class SuratPerintahRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
		$return = [
            'tanggal'   			=> 'required',
        ];
    	
		return $return;
    }

    public function messages()
    {
    	return [
        	'tanggal.required'        	=> 'Tanggal Surat Perintah tidak boleh kosong',
       ];
    }
}
