<?php

namespace App\Http\Requests\KegiatanAudit\TindakLanjut;

use App\Http\Requests\FormRequest;

class TLRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
		$return = [
            'data.operasional.*.pilihan'   		=> 'required',
            'data.keuangan.*.pilihan'   		=> 'required',
            'data.sistem.*.pilihan'   			=> 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'data.operasional.*.pilihan.required'      	=> 'Pilih Salah Satu',
        	'data.keuangan.*.pilihan.required'        	=> 'Pilih Salah Satu',
        	'data.sistem.*.pilihan.required'        	=> 'Pilih Salah Satu',
       ];
    }
}
