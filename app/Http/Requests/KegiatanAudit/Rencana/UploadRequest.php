<?php

namespace App\Http\Requests\KegiatanAudit\Rencana;

use App\Http\Requests\FormRequest;

class UploadRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'final'            			=> 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'final.required'            => 'Berkas Final tidak boleh kosong',
       ];
    }
}
