<?php

namespace App\Http\Requests\KegiatanAudit\Rencana;

use App\Http\Requests\FormRequest;

class DetailRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
        if(!empty($input['detail'])){
	    	foreach ($input['detail'] as $key => $value) {
	    		if($value['tipe'] == 1){
	    			$return['detail.'.$key.'.konsultasi_id'] 					= 'required';
	    			$return['detail.'.$key.'.keterangan'] 						= 'required';
	    			$return['detail.'.$key.'.rencana'] 							= 'required';
	    			$return['detail.'.$key.'.operasional'] 						= 'required';
	    			$return['detail.'.$key.'.keuangan'] 						= 'required';
	    			$return['detail.'.$key.'.sistem'] 							= 'required';
	    		}elseif($value['tipe'] == 2){
	    			$return['detail.'.$key.'.lain'] 							= 'required';
	    			$return['detail.'.$key.'.keterangan'] 						= 'required';
	    			$return['detail.'.$key.'.rencana'] 							= 'required';
	    			// $return['detail.'.$key.'.operasional'] 						= 'required';
	    			// $return['detail.'.$key.'.keuangan'] 						= 'required';
	    			// $return['detail.'.$key.'.sistem'] 							= 'required';
	    		}else{
		    		if(!empty($value['tipe_object'])){
			    		if($value['tipe_object'] == 2){
			    			if(!empty($value['jenis'])){
			    				if($value['jenis'] == 1 || $value['jenis'] == 2){
			    					$return['detail.'.$key.'.snk'] 				= 'required';
			    					$return['detail.'.$key.'.progress'] 		= 'required';
			    				}
			    			}
			    			$return['detail.'.$key.'.tgl_mulai'] 				= 'required';
			    			$return['detail.'.$key.'.tgl_selesai'] 				= 'required';
			    			$return['detail.'.$key.'.jenis'] 					= 'required';
			    			$return['detail.'.$key.'.nilai_kontrak'] 			= 'required';
			    			$return['detail.'.$key.'.progress_ra'] 				= 'required';
			    			$return['detail.'.$key.'.progress_ri'] 				= 'required';
			    			$return['detail.'.$key.'.deviasi'] 					= 'required';
			    			$return['detail.'.$key.'.risk_impact'] 				= 'required';
			    			$return['detail.'.$key.'.mapp'] 					= 'required';
			    			$return['detail.'.$key.'.bkpu'] 					= 'required';
			    			$return['detail.'.$key.'.deviasi_bkpu'] 			= 'required';
			    			$return['detail.'.$key.'.bkpu_ri'] 					= 'required';
			    			$return['detail.'.$key.'.object_id'] 				= 'required';
			    			$return['detail.'.$key.'.keterangan'] 				= 'required';
			    			$return['detail.'.$key.'.rencana'] 					= 'required';
			    			// $return['detail.'.$key.'.operasional'] 				= 'required';
			    			// $return['detail.'.$key.'.keuangan'] 				= 'required';
			    			// $return['detail.'.$key.'.sistem'] 					= 'required';
			    		}else{
			    			$return['detail.'.$key.'.tipe'] 					= 'required';
			    			$return['detail.'.$key.'.tipe_object'] 				= 'required';
			    			$return['detail.'.$key.'.object_id'] 				= 'required';
			    			$return['detail.'.$key.'.keterangan'] 				= 'required';
			    			$return['detail.'.$key.'.rencana'] 					= 'required';
			    			// $return['detail.'.$key.'.operasional'] 				= 'required';
			    			// $return['detail.'.$key.'.keuangan'] 				= 'required';
			    			// $return['detail.'.$key.'.sistem'] 					= 'required';
			    		}
		    		}else{
		    			$return['detail.'.$key.'.tipe'] 						= 'required';
		    			$return['detail.'.$key.'.tipe_object'] 					= 'required';
		    			$return['detail.'.$key.'.object_id'] 					= 'required';
		    			$return['detail.'.$key.'.keterangan'] 					= 'required';
		    			$return['detail.'.$key.'.rencana'] 						= 'required';
		    			// $return['detail.'.$key.'.operasional'] 					= 'required';
		    			// $return['detail.'.$key.'.keuangan'] 					= 'required';
		    			// $return['detail.'.$key.'.sistem'] 						= 'required';
		    		}
	    		}
	    	}
        }else{
        	$return =[];
        }
		return $return;
    }

    public function messages()
    {
    	return [
        	'detail.*.tipe.required'     		=> 'Rencana Kerja tidak boleh kosong',
        	'detail.*.tipe_object.required'     => 'Kategori tidak boleh kosong',
        	'detail.*.konsultasi_id.required'     	=> 'Kategori Konsultasi tidak boleh kosong',
        	'detail.*.lain.required'     		=> 'Kategori Kegiatan Lain - Lain tidak boleh kosong',
        	'detail.*.object_id.required'       => 'Objek Audit tidak boleh kosong',
        	'detail.*.keterangan.required'      => 'Keterangan tidak boleh kosong',
        	'detail.*.rencana.required'      	=> 'Rencana tidak boleh kosong',
        	'detail.*.tgl_mulai.required'   	=> 'Tanggal Mulai tidak boleh kosong',
        	'detail.*.tgl_selesai.required'   	=> 'Tanggal Selesai tidak boleh kosong',
        	'detail.*.jenis.required'   		=> 'Jenis Kontrak tidak boleh kosong',
        	'detail.*.nilai_kontrak.required'   => 'Nilai Kontrak tidak boleh kosong',
        	'detail.*.progress_ra.required'     => 'Progress (Ra) tidak boleh kosong',
        	'detail.*.progress_ri.required'     => 'Progress (Ri) tidak boleh kosong',
        	'detail.*.deviasi.required'         => 'Deviasi Progress tidak boleh kosong',
        	'detail.*.risk_impact.required'     => 'Progress Risk Impact tidak boleh kosong',
        	'detail.*.mapp.required'            => 'MAPP tidak boleh kosong',
        	'detail.*.bkpu.required'            => 'BK / PU tidak boleh kosong',
        	'detail.*.deviasi_bkpu.required'    => 'Deviasi BK / PU tidak boleh kosong',
        	'detail.*.bkpu_ri.required'         => 'BK / PU Risk Impact tidak boleh kosong',
        	'detail.*.snk.required'         	=> 'SNK tidak boleh kosong',
        	'detail.*.progress.required'        => 'Progress tidak boleh kosong',
        	'detail.*.operasional.required'     => 'Auditor Operasional tidak boleh kosong',
        	'detail.*.keuangan.required'        => 'Auditor Keuangan tidak boleh kosong',
        	'detail.*.sistem.required'        	=> 'Auditor Sistem tidak boleh kosong',
       ];
    }
}
