<?php

namespace App\Http\Requests\KegiatanAudit\Rencana;

use App\Http\Requests\FormRequest;

class RejectRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'ket_svp'            			=> 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'ket_svp.required'            => 'Alasan Penolakan tidak boleh kosong',
       ];
    }
}
