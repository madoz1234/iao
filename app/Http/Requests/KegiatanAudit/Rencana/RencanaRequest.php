<?php

namespace App\Http\Requests\KegiatanAudit\Rencana;

use App\Http\Requests\FormRequest;

class RencanaRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'tahun'            			=> 'required|unique_with:trans_rencana_audit,tipe,'.$this->get('id'),
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'tahun.required'            => 'Tahun tidak boleh kosong',
        	'tahun.unique_with'         => 'Tahun sudah ada',
       ];
    }
}
