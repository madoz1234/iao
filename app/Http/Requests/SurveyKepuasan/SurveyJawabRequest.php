<?php

namespace App\Http\Requests\SurveyKepuasan;

use Illuminate\Foundation\Http\FormRequest;

class SurveyJawabRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $input = $this->all();
        $data =[];
        if ($input['status'] == 1) {
            foreach ($input['jawaban'] as $key => $value) {
                $return['jawaban.'.$key]   = 'required';
                $data += $return;
            }
        }
        return $data;
    }

    public function messages()
    {
        return [
            'jawaban.*'                     => 'Jawaban tidak boleh kosong',
        ];
    }
}
