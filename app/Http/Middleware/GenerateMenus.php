<?php
namespace App\Http\Middleware;

use Closure;
use Menu;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::make('sideMenu', function ($menu) {
            $menu->add('Dashboard', 'dashboard')
            	 ->data('perms', 'dashboard')
                 ->data('icon', 'fa fa-tachometer')
                 ->active('dashboard/*');

            $menu->add('Monitoring', 'monitoring/monitoring')
            	 ->data('perms', 'monitoring/monitoring')
                 ->data('icon', 'fa fa-desktop')
                 ->active('monitoring/monitoring/*');

            $menu->add('RKIA', 'rkia/rkia')
            	 ->data('perms', 'rkia/rkia')
                 ->data('icon', 'fa fa-calendar')
                 ->active('rkia/rkia/*');

            $menu->add('Kegiatan Audit', 'kegiatan-audit')
                 ->data('icon', 'fa fa-newspaper-o');
            	 $menu->kegiatanAudit->add('Persiapan', 'persiapan');
            	 	  $menu->persiapan->add('Surat Penugasan', 'kegiatan-audit/persiapan/penugasan')
	            	 	   ->active('kegiatan-audit/persiapan/penugasan/*');
	            	  $menu->persiapan->add('Permintaan Dokumen', 'kegiatan-audit/persiapan/tinjauan-dokumen')
	            	 	   ->active('kegiatan-audit/persiapan/tinjauan-dokumen/*');
	            	  $menu->persiapan->add('Program Audit', 'kegiatan-audit/persiapan/program')
	            	 	   ->active('kegiatan-audit/persiapan/program/*');
	             $menu->kegiatanAudit->add('Pelaksanaan', 'pelaksanaan');
            	 	  $menu->pelaksanaan->add('Opening Meeting', 'kegiatan-audit/pelaksanaan/opening-meeting')
	            	 	   ->active('kegiatan-audit/pelaksanaan/opening-meeting/*');
	            	  $menu->pelaksanaan->add('KKA', 'kegiatan-audit/pelaksanaan/draft-kka')
	            	 	   ->active('kegiatan-audit/pelaksanaan/draft-kka/*');
	            	  $menu->pelaksanaan->add('Closing Meeting', 'kegiatan-audit/pelaksanaan/closing-meeting')
	            	 	   ->active('kegiatan-audit/pelaksanaan/closing-meeting/*');
	             $menu->kegiatanAudit->add('Pelaporan');
	                  $menu->pelaporan->add('LHA', 'kegiatan-audit/pelaporan/lha')
	            	 	   ->active('kegiatan-audit/pelaporan/lha/*');
	             $menu->kegiatanAudit->add('Tindak Lanjut', 'tindak-lanjut');
	                  $menu->tindakLanjut->add('Register Tindak Lanjut', 'kegiatan-audit/tindak-lanjut/register')
	            	 	   ->active('kegiatan-audit/tindak-lanjut/register/*');
	            	  $menu->tindakLanjut->add('Monitoring Tindak Lanjut', 'kegiatan-audit/tindak-lanjut/monitoring-tl')
	            	 	   ->active('kegiatan-audit/tindak-lanjut/monitoring-tl/*');
              $menu->kegiatanAudit->add('Survey Kepuasan Auditee', 'survey-kepuasan-audit')
                   ->data('icon', 'fa fa-file-text-o')
                   ->nickname('surveyKepuasanAudit')
                   ->active('survey-kepuasan-audit/*'); 
                   $menu->surveyKepuasanAudit->add('Survey', 'survey-kepuasan-audit/survey/')
                        ->data('perms', 'survey-kepuasan-audit-survey')
                        ->nickname('surveyKepuasanAuditSurvey')
                        ->active('survey-kepuasan-audit/survey/*');
                  $menu->surveyKepuasanAudit->add('Hasil Survey', 'survey-kepuasan-audit/hasil-survey/')
                        ->data('perms', 'survey-kepuasan-audit-hasil-survey')
                        ->nickname('surveyKepuasanAuditHasil')
                        ->active('survey-kepuasan-audit/hasil-survey/*');
            

            $menu->add('Kegiatan Konsultasi')
             	 ->data('icon', 'fa fa-tags')
                 ->nickname('kegiatanKonsultasi')
             	 ->active('kegiatan-konsultasi/*');
                 $menu->kegiatanKonsultasi->add('Perencanaan', 'kegiatan-konsultasi/perencanaan/')
                      ->data('perms', 'kegiatan-konsultasi-perencanaan')
                      ->nickname('kegiatanKonsultasiPerencanaan')
                      ->active('kegiatan-konsultasi/perencanaan/*');
                 $menu->kegiatanKonsultasi->add('Pelaksanaan', 'kegiatan-konsultasi/pelaksanaan/')
                      ->data('perms', 'kegiatan-konsultasi-pelaksanaan')
                      ->nickname('kegiatanKonsultasiPelaksanaan')
                      ->active('kegiatan-konsultasi/pelaksanaan/*');
                 $menu->kegiatanKonsultasi->add('Laporan', 'kegiatan-konsultasi/laporan/')
                      ->data('perms', 'kegiatan-konsultasi-laporan')
                      ->nickname('kegiatanKonsultasiLaporan')
                      ->active('kegiatan-konsultasi/laporan/*');

            $menu->add('Kegiatan Lainnya')
                 ->data('icon', 'fa fa-briefcase')
                 ->nickname('kegiatanLainnya')
                 ->active('kegiatan-lainnya/*');
                 $menu->kegiatanLainnya->add('Perencanaan', 'kegiatan-lainnya/perencanaan/')
                      ->data('perms', 'kegiatan-lainnya-perencanaan')
                      ->nickname('kegiatanLainnyaPerencanaan')
                      ->active('kegiatan-lainnya/perencanaan/*');
                 $menu->kegiatanLainnya->add('Pelaksanaan', 'kegiatan-lainnya/pelaksanaan/')
                      ->data('perms', 'kegiatan-lainnya-pelaksanaan')
                      ->nickname('kegiatanLainnyaPelaksanaan')
                      ->active('kegiatan-lainnya/pelaksanaan/*');
                 $menu->kegiatanLainnya->add('Laporan', 'kegiatan-lainnya/laporan/')
                      ->data('perms', 'kegiatan-lainnya-laporan')
                      ->nickname('kegiatanLainnyaLaporan')
                      ->active('kegiatan-lainnya/laporan/*');

            $menu->add('Rapat', 'rapat')
                 ->data('icon', 'fa fa-gavel');
                 $menu->rapat->add('Internal', 'rapat/internal/')
            	 	  ->data('perms', 'rapat-internal')
            	 	  ->active('rapat/internal/*');
                 $menu->rapat->add('Eksternal', 'rapat/eksternal-real/')
                      ->data('perms', 'rapat-eksternal-real')
                      ->active('rapat/eksternal-real/*');
            $menu->add('Master', 'master')
                  ->data('icon', 'fa fa-book');
	           	 $menu->master->add('Project', 'master/project')
	            	 ->data('perms', 'master-project')
	            	 ->active('master/project/*');
	             $menu->master->add('Business Unit (BU)', 'master/bu')
                 	 ->data('perms', 'master-bu')
	            	 ->active('master/bu/*');
	           	 $menu->master->add('Corporate Office (CO)', 'master/co')
	            	 ->data('perms', 'master-co')
	            	 ->active('master/co/*');
	           	 $menu->master->add('Anak Perusahaan', 'master/anak-perusahaan')
	            	 ->data('perms', 'master-anak-perusahaan')
	            	 ->active('master/anak-perusahaan/*');
	             $menu->master->add('Vendor', 'master/vendor')
	            	 ->data('perms', 'master-vendor')
	            	 ->active('master/vendor/*');
	             $menu->master->add('Kegiatan Konsultasi', 'master/kegiatan-konsultasi')
	            	 ->data('perms', 'master-kegiatan-konsultasi')
	            	 ->active('master/kegiatan-konsultasi/*');
	             $menu->master->add('Auditor Eksternal', 'master/kegiatan-lain')
	            	 ->data('perms', 'master-kegiatan-lain')
	            	 ->active('master/kegiatan-lain/*');
	             $menu->master->add('Dokumen Pendahuluan', 'master/dokumen')
	            	 ->data('perms', 'master-dokumen')
	            	 ->active('master/dokumen/*');
	           	 $menu->master->add('Fokus Audit', 'master/fokus-audit')
	            	 ->data('perms', 'master-fokus-audit')
	            	 ->active('master/fokus-audit/*');
	             $menu->master->add('Langkah Kerja', 'master/langkah-kerja')
	            	 ->data('perms', 'master-langkah-kerja')
	            	 ->active('master/langkah-kerja/*');
	             $menu->master->add('Standardisasi Temuan', 'master/standarisasi-temuan')
	            	 ->data('perms', 'master-standarisasi-temuan')
	            	 ->active('master/standarisasi-temuan/*');
	             $menu->master->add('Temuan', 'master/temuan')
	            	 ->data('perms', 'master-temuan')
	            	 ->active('master/temuan/*');
	             $menu->master->add('Kategori Temuan', 'master/kategori-temuan')
	            	 ->data('perms', 'master-kategori-temuan')
	            	 ->active('master/kategori-temuan/*');
	             $menu->master->add('SPIN');
                	$menu->sPIN->add('Point of Focus (PoF)', 'master/spin-pof/')
                        ->data('perms', 'spin-pof')
                        ->active('master/spin-pof/*');
                	$menu->sPIN->add('Unsur Pemenuhan', 'master/spin-pemenuhan/')
                        ->data('perms', 'spin-pemenuhan')
                        ->active('master/spin-pemenuhan/*');
                	$menu->sPIN->add('Pembobotan', 'master/spin-pembobotan/')
                        ->data('perms', 'spin-pembobotan')
                  		->active('master/spin-pembobotan/*');
                  	$menu->sPIN->add('Kertas Kerja', 'master/spin-kerja/')
                        ->data('perms', 'spin-kerja')
                  		->active('master/spin-kerja/*');
	             $menu->master->add('Kuesioner IACM', 'master/iacm/*');
	                  $menu->kuesionerIACM->add('Elemen', 'master/iacm/elemen/')
	            	 	->data('perms', 'master-iacm-elemen')
	            	 	->active('master/iacm/elemen/*');
	            	  $menu->kuesionerIACM->add('Key Process Area', 'master/iacm/kpa/')
	            	 	->data('perms', 'master-iacm-kpa')
	            	 	->active('master/iacm/kpa/*');
	            	  $menu->kuesionerIACM->add('Uraian', 'master/iacm/uraian/')
	            	 	->data('perms', 'master-iacm-uraian')
	            	 	->active('master/iacm/uraian/*');
                 $menu->master->add('Survey', 'master/survey')
                     ->data('perms', 'master-survey')
                     ->active('master/survey/*');
                 $menu->master->add('Edisi', 'master/edisi')
                     ->data('perms', 'master-edisi')
                     ->active('master/edisi/*');
            $menu->add('Manajemen Pengguna', 'setting')
                 ->data('perms', 'setting')
                 ->data('icon', 'fa fa-users')
                 ->active('setting/*');
        });
        return $next($request);
    }
}
