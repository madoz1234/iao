<?php

namespace App\Http\Controllers\SurveyKepuasan;

use App\Exports\SurveyExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\SurveyKepuasan\SurveyJawabRequest;
use App\Models\Auths\User;
use App\Models\KegiatanAudit\Pelaporan\LHA;
use App\Models\Master\Survey;
use App\Models\SessionLog;
use App\Models\Survey\SurveyJawab;
use App\Models\Survey\SurveyJawabDetail;
use DB;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cookie;
use PDF;
use Yajra\DataTables\Facades\DataTables;

class SurveyKepuasanAuditController extends Controller
{   
    protected $routes = 'survey-kepuasan-audit.survey';

    public function __construct()
    {   
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Kegiatan Audit' => route($this->routes.'.index'), 'Survey Kepuasan Auditee' => route($this->routes.'.index'), 'Survey' => route($this->routes.'.index')]);
        $this->listStructs = [
            'listStruct' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => 'text-center',
                    'width' => '20px',
                ],
                [
                    'data' => 'tahun',
                    'name' => 'tahun',
                    'label' => 'Tahun',
                    'className' => 'text-center',
                    'width' => '100px',
                    'sortable' => true,
                ],
                [
                    'data' => 'rencana',
                    'name' => 'rencana',
                    'label' => 'Waktu Pelaksanaan',
                    'className' => 'text-center',
                    'width' => '300px',
                ],
                [
                    'data' => 'kategori',
                    'name' => 'kategori',
                    'label' => 'Kategori',
                    'className' => 'text-center',
                    'width' => '250px',
                    'sortable' => true,
                ],
                [
                    'data' => 'object_audit',
                    'name' => 'object_audit',
                    'label' => 'Objek Audit',
                    'className' => 'text-center',
                    'width' => '250px',
                    'sortable' => true,
                ],
                [
                    'data' => 'no_ab',
                    'name' => 'no_ab',
                    'label' => 'No AB',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'lha',
                    'name' => 'lha',
                    'label' => 'LHA',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'tgl_aktif',
                    'name' => 'tgl_aktif',
                    'label' => 'Tanggal Aktif',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '250px',
                ],
                [
                    'data' => 'pic',
                    'name' => 'pic',
                    'label' => 'PIC',
                    'className' => 'text-left',
                    'width' => '300px',
                    'sortable' => true,
                ],
                [
                    'data' => 'updated_at',
                    'name' => 'updated_at',
                    'label' => 'Diperbarui Pada',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '300px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '100px',
                    'className' => 'text-center'
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '80px',
                    'className' => 'text-center'
                ],
            ],
            'listStruct2' => [],
            'listStruct3' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => 'text-center',
                    'width' => '20px',
                ],
                [
                    'data' => 'tahun',
                    'name' => 'tahun',
                    'label' => 'Tahun',
                    'className' => 'text-center',
                    'width' => '100px',
                    'sortable' => true,
                ],
                [
                    'data' => 'rencana',
                    'name' => 'rencana',
                    'label' => 'Waktu Pelaksanaan',
                    'className' => 'text-center',
                    'width' => '300px',
                ],
                [
                    'data' => 'kategori',
                    'name' => 'kategori',
                    'label' => 'Kategori',
                    'className' => 'text-center',
                    'width' => '250px',
                    'sortable' => true,
                ],
                [
                    'data' => 'object_audit',
                    'name' => 'object_audit',
                    'label' => 'Objek Audit',
                    'className' => 'text-center',
                    'width' => '250px',
                    'sortable' => true,
                ],
                [
                    'data' => 'no_ab',
                    'name' => 'no_ab',
                    'label' => 'No AB',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'lha',
                    'name' => 'lha',
                    'label' => 'LHA',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'tgl_aktif',
                    'name' => 'tgl_aktif',
                    'label' => 'Tanggal Aktif',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '300px',
                ],
                [
                    'data' => 'pic',
                    'name' => 'pic',
                    'label' => 'PIC',
                    'className' => 'text-left',
                    'width' => '300px',
                    'sortable' => true,
                ],
                [
                    'data' => 'tgl_submit',
                    'name' => 'tgl_submit',
                    'label' => 'Tanggal Submit',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '300px',
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '100px',
                    'className' => 'text-center'
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '80px',
                    'className' => 'text-center'
                ],
            ],
        ];
    }

    public function grid()
    {
        $user = auth()->user();

        if($user->hasRole(['auditor','svp-audit','dirut'])){
            $records = SurveyJawab::whereIn('status', [0,1]);
        }else{
            $arr_lha = SurveyJawab::getLhaByObject($user);
            $records = SurveyJawab::whereIn('lha_id', $arr_lha)->whereIn('status', [0,1]);
        }        

        if(!isset(request()->order[0]['column'])) {
          $records->orderBy('created_at','desc');
        }
        if($tahun = request()->tahun) {
            $records->whereHas('lha', function($lha) use ($tahun){
                $lha->whereHas('draftkka', function($draftkka) use ($tahun){
                    $draftkka->whereHas('program', function($program) use ($tahun){
                        $program->whereHas('penugasanaudit', function($penugasanaudit) use ($tahun){
                            $penugasanaudit->whereHas('rencanadetail', function($rencanadetail) use ($tahun){
                                $rencanadetail->whereHas('rencanaaudit', function($rencanaaudit) use ($tahun){
                                    $rencanaaudit->where('tahun', 'like', '%' . $tahun . '%');
                                });
                            });
                        });
                    });
                });
            });
        }
        if($kategori = request()->kategori) {
            $kategori = $kategori-1;
            $records->whereHas('lha', function($lha) use ($kategori){
                $lha->whereHas('draftkka', function($draftkka) use ($kategori){
                    $draftkka->whereHas('program', function($program) use ($kategori){
                        $program->whereHas('penugasanaudit', function($penugasanaudit) use ($kategori){
                            $penugasanaudit->whereHas('rencanadetail', function($rencanadetail) use ($kategori){
                                $rencanadetail->where('tipe_object', $kategori);
                            });
                        });
                    });
                });
            });
        }
        if($object_id = request()->object_id) {
            $kategori = request()->kategori-1;
            $records->whereHas('lha', function($lha) use ($object_id, $kategori){
                $lha->whereHas('draftkka', function($draftkka) use ($object_id, $kategori){
                    $draftkka->whereHas('program', function($program) use ($object_id, $kategori){
                        $program->whereHas('penugasanaudit', function($penugasanaudit) use ($object_id, $kategori){
                            $penugasanaudit->whereHas('rencanadetail', function($rencanadetail) use ($object_id, $kategori){
                                $rencanadetail->where('tipe_object', $kategori)->where('object_id', $object_id);
                            });
                        });
                    });
                });
            });
        }

        return DataTables::of($records->get())
            ->addColumn('num', function ($record) {
                return request()->start;
            })
            ->addColumn('tahun', function ($record) {
                return $record->lha->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun;
            })
            ->addColumn('rencana', function ($record) {
                return DateToString($record->lha->draftkka->program->detailpelaksanaan->first()->tgl).' - '.DateToString($record->lha->draftkka->program->detailpelaksanaan->last()->tgl);
            })
            ->addColumn('kategori', function ($record) {
                $kategori = '';
                if($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe == 0){
                    if($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 0){
                      $kategori = '<span class="label label-success">Business Unit (BU)</span>';
                    }elseif($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 1){
                      $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
                    }elseif($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 2){
                      $kategori = '<span class="label label-warning">Project</span>';
                    }else{
                      $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
                    }
                }elseif($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe == 1){
                    $kategori = $record->lha->draftkka->program->penugasanaudit->rencanadetail->konsultasi->nama;
                }else{
                    $kategori = $record->lha->draftkka->program->penugasanaudit->rencanadetail->lain->nama;
                }
                return $kategori;
            })
            ->addColumn('object_audit', function ($record) {
                return object_audit($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->lha->draftkka->program->penugasanaudit->rencanadetail->object_id);
            })
            ->addColumn('no_ab', function ($record) {
                return getAb($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->lha->draftkka->program->penugasanaudit->rencanadetail->object_id);
            })
            ->addColumn('lha', function ($record) {
                $buttons = '';
                $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-download text-primary"></i>',
                    'tooltip'   => 'Download',
                    'class'     => 'download-lha button',
                    'id'        => $record->id,
                ]);
               
               return $buttons;
            })
            ->addColumn('tgl_aktif', function ($record) {
                return DateToString($record->created_at);
            })
            ->addColumn('pic', function ($record) {
                $pic = '';
                $pic = $record->user->name;
                
                return $pic;
            })
            ->editColumn('status', function ($record) {
                if($record->status == 0){
                    return '<span class="label label-info">Baru</span>';
                }elseif($record->status == 1){
                    return '<span class="label label-warning">Draft</span>';
                }
            })
            ->addColumn('action', function ($record) use ($user) {
                $buttons = '';
                if($record->user_id == $user->id){
                    $buttons .= $this->makeButton([
                        'type'      => 'edit',
                        'label'     => '<i class="fa fa-edit text-primary"></i>',
                        'tooltip'   => 'Lengkapi Survey',
                        'class'     => 'buat button',
                        'id'        => $record->id,
                        'datas'     => [
                            'time'         => $record->seconds_timer,
                            'is_countdown' => $record->is_countdown,
                        ]
                    ]);
                }
                return $buttons;
            })
            ->editColumn('updated_at', function ($record) {
                if ($record->status == 0) {
                    return '';
                }else{
                    return $record->updated_at->diffForHumans();
                }
            })
            ->rawColumns(['pic','kategori','object_audit','status','lha', 'action'])
            ->make(true);
    }

    public function onPregress(){}
    
    public function historis()
    {
        $user = auth()->user();
        if($user->hasRole(['auditor','svp-audit','dirut'])){
            $records = SurveyJawab::where('status', 2);
        }else{
            $arr_lha = SurveyJawab::getLhaByObject($user);
            $records = SurveyJawab::whereIn('lha_id', $arr_lha)->where('status', 2);
        }

        if(!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at','desc');
        }
        if($tahun = request()->tahun) {
            $records->whereHas('lha', function($lha) use ($tahun){
                $lha->whereHas('draftkka', function($draftkka) use ($tahun){
                    $draftkka->whereHas('program', function($program) use ($tahun){
                        $program->whereHas('penugasanaudit', function($penugasanaudit) use ($tahun){
                            $penugasanaudit->whereHas('rencanadetail', function($rencanadetail) use ($tahun){
                                $rencanadetail->whereHas('rencanaaudit', function($rencanaaudit) use ($tahun){
                                    $rencanaaudit->where('tahun', 'like', '%' . $tahun . '%');
                                });
                            });
                        });
                    });
                });
            });
        }
        if($kategori = request()->kategori) {
            $kategori = $kategori-1;
            $records->whereHas('lha', function($lha) use ($kategori){
                $lha->whereHas('draftkka', function($draftkka) use ($kategori){
                    $draftkka->whereHas('program', function($program) use ($kategori){
                        $program->whereHas('penugasanaudit', function($penugasanaudit) use ($kategori){
                            $penugasanaudit->whereHas('rencanadetail', function($rencanadetail) use ($kategori){
                                $rencanadetail->where('tipe_object', $kategori);
                            });
                        });
                    });
                });
            });
        }
        if($object_id = request()->object_id) {
            $kategori = request()->kategori-1;
            $records->whereHas('lha', function($lha) use ($object_id, $kategori){
                $lha->whereHas('draftkka', function($draftkka) use ($object_id, $kategori){
                    $draftkka->whereHas('program', function($program) use ($object_id, $kategori){
                        $program->whereHas('penugasanaudit', function($penugasanaudit) use ($object_id, $kategori){
                            $penugasanaudit->whereHas('rencanadetail', function($rencanadetail) use ($object_id, $kategori){
                                $rencanadetail->where('tipe_object', $kategori)->where('object_id', $object_id);
                            });
                        });
                    });
                });
            });
        }
        
        return DataTables::of($records->get())
            ->addColumn('num', function ($record) {
                return request()->start;
            })
            ->addColumn('tahun', function ($record) {
                return $record->lha->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun;
            })
            ->addColumn('rencana', function ($record) {
                return DateToString($record->lha->draftkka->program->detailpelaksanaan->first()->tgl).' - '.DateToString($record->lha->draftkka->program->detailpelaksanaan->last()->tgl);
            })
            ->addColumn('kategori', function ($record) {
                $kategori = '';
                if($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe == 0){
                    if($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 0){
                      $kategori = '<span class="label label-success">Business Unit (BU)</span>';
                    }elseif($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 1){
                      $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
                    }elseif($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 2){
                      $kategori = '<span class="label label-warning">Project</span>';
                    }else{
                      $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
                    }
                }elseif($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe == 1){
                    $kategori = $record->lha->draftkka->program->penugasanaudit->rencanadetail->konsultasi->nama;
                }else{
                    $kategori = $record->lha->draftkka->program->penugasanaudit->rencanadetail->lain->nama;
                }
                return $kategori;
            })
            ->addColumn('object_audit', function ($record) {
                return object_audit($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->lha->draftkka->program->penugasanaudit->rencanadetail->object_id);
            })
            ->addColumn('no_ab', function ($record) {
                return getAb($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->lha->draftkka->program->penugasanaudit->rencanadetail->object_id);
            })
            ->addColumn('lha', function ($record) {
                $buttons = '';
                $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-download text-primary"></i>',
                    'tooltip'   => 'Download',
                    'class'     => 'download-lha button',
                    'id'        => $record->id,
                ]);
                return $buttons;
            })
            ->addColumn('tgl_aktif', function ($record) {
                return DateToString($record->created_at);
            })
            ->addColumn('pic', function ($record) {                
                return $record->user->name;
            })
            ->addColumn('tgl_submit', function ($record) {
                return DateToString($record->updated_at);
            })
            ->editColumn('status', function ($record) {
                return '<span class="label label-success">Completed</span>';
            })
            ->addColumn('action', function ($record) use ($user) {
                $buttons = '';
                if ($user->hasRole(['auditor','svp-audit','dirut']) || $user->id == $record->user_id) {
                    $buttons .= $this->makeButton([
                        'type'      => 'edit',
                        'label'     => '<i class="fa fa-print text-info"></i>',
                        'tooltip'   => 'Cetak',
                        'class'     => 'cetak button',
                        'id'        => $record->id,
                    ]);
                    // $buttons .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    // $buttons .= $this->makeButton([
                    //     'type'      => 'edit',
                    //     'label'     => '<i class="fa fa-file-excel-o text-primary"></i>',
                    //     'tooltip'   => 'Download',
                    //     'class'     => 'download-excel button',
                    //     'id'        => $record->id,
                    // ]);
                }
                return $buttons;
            })
            ->rawColumns(['pic','kategori','object_audit','status','lha', 'action'])
            ->make(true);
    }

    public function index()
    {
        return $this->render('modules.survey.audit.index', [
            'mockup' => true,
            'structs' => $this->listStructs,
            'badgeTab' => SurveyJawab::badgeTab('survey')
        ]);
    }

    public function buat(SurveyJawab $id)
    {
        $user = auth()->user();
        if ($user->id == $id->user_id && ($id->status == 0 || $id->status == 1)) {
            $this->pushBreadcrumb(['Lengkapi Survey' => '#']);
            return $this->render('modules.survey.audit.create', [
                'record' => $id,
                'survey' => $id->survey,
                'survey_jawab_detail' => $id->survey_jawab_detail
            ]);
        }
        return abort(404);
    }

    public function store(SurveyJawabRequest $request)
    {
        DB::beginTransaction();
        try {
            for ($i=0; $i < count($request->tanya_id); $i++) { 
                if (isset($request->jawaban[$request->tanya_id[$i]])) {
                    $jawab_detail = SurveyJawabDetail::where('survei_jawab_id', $request->survei_jawab_id)->where('tanya_id', $request->tanya_id[$i])->first();
                    if ($jawab_detail) {
                        $jawab_detail->jawaban = $request->jawaban[$request->tanya_id[$i]];
                        $jawab_detail->save();
                    }else{
                        $jawab_detail                  = new SurveyJawabDetail;
                        $jawab_detail->survei_jawab_id = $request->survei_jawab_id;
                        $jawab_detail->tanya_id        = $request->tanya_id[$i];
                        $jawab_detail->jenis           = 1;
                        $jawab_detail->jawaban         = $request->jawaban[$request->tanya_id[$i]];
                        $jawab_detail->save();                  
                    }
                }
            }
            //Simpan saran dengan tanya_id = NULL dan jenis=0
            $jawab_detail = SurveyJawabDetail::where('survei_jawab_id', $request->survei_jawab_id)->where('tanya_id', null)->first();
            if ($jawab_detail) {
                if (!is_null($request->saran)) {
                    $jawab_detail->jawaban = $request->saran;
                    $jawab_detail->save();      
                }else{
                    $jawab_detail->delete();
                }
            }else{
                if (!is_null($request->saran)) {
                    $jawab_detail                  = new SurveyJawabDetail;
                    $jawab_detail->survei_jawab_id = $request->survei_jawab_id;
                    $jawab_detail->tanya_id        = NULL;
                    $jawab_detail->jenis           = 0;
                    $jawab_detail->jawaban         = $request->saran;
                    $jawab_detail->save();        
                }
            }

            if ($request->status == 1) {
                $record         = SurveyJawab::find($request->survei_jawab_id);
                $record->status = 2;
                $record->timer = $request->timer;
                $record->is_countdown = $request->is_countdown;
                $record->save();
            }else{
                $record         = SurveyJawab::find($request->survei_jawab_id);
                $record->status = 1;
                $record->timer = $request->timer;
                $record->is_countdown = $request->is_countdown;
                $record->save();
            }
            $user = auth()->user();
            $log = new SessionLog;
            $log->modul     = 'Survey Kepuasan';
            $log->aktivitas = 'Membuat survey kepuasan audit.';
            $log->user_id   = $user->id;
            $log->save();
            DB::commit();
            return response([
                'status' => true,
            ]); 
        }catch (\Exception $e) {
            DB::rollback();
            return response([
                'status' => 'error',
                'message' => 'An error occurred!',
                'error' => $e->getMessage(),
            ], 500);
        }
    }

    public function downloadLHA(SurveyJawab $id)
    {
        if ($files = $id->lha->files->last()) {
            if(file_exists(public_path('storage/'.$files->url))){
                return response()->download(public_path('storage/'.$files->url), $files->filename);
            }
        }
        return abort(404);
    }

    public function cetak(SurveyJawab $id)
    {
        $user = auth()->user();
        if(($user->hasRole(['auditor','svp-audit','dirut']) || $user->id == $id->user->id) && $id->status == 2){
            $data = [
                'record' => $id,
                'survey' => $id->survey,
                'survey_jawab_detail' => $id->survey_jawab_detail
            ];
            $pdf = PDF::loadView('modules.survey.audit.cetak', $data);
            return $pdf->stream('Survey Kepuasan Audit '.Carbon::parse($id->updated_at)->format('Y-m-d').'.pdf',array("Attachment" => false));
        }
        return abort(404);
    }

    public function downloadExcel(SurveyJawab $id)
    {
        $user = auth()->user();
        if(($user->hasRole(['auditor','svp-audit','dirut']) || $user->id == $id->user->id) && $id->status == 2){
            $view       = 'modules.survey.audit.export-excel';
            $data_array = [
                'title' => 'Survey Kepuasan Auditee',
                'data' => $id
            ];
            $tittle     ='Survey Kepuasan Auditee';

            return Excel::download(new SurveyExport($view, $data_array), $tittle.'.xlsx');
        }
        return abort(404);
    }

}
