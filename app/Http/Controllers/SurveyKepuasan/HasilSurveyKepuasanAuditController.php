<?php

namespace App\Http\Controllers\SurveyKepuasan;

use App\Exports\SurveyExport;
use App\Http\Controllers\Controller;
use App\Models\Auths\User;
use App\Models\Files;
use App\Models\Master\Survey;
use App\Models\Master\SurveyPertanyaan;
use App\Models\SessionLog;
use App\Models\Survey\SurveyJawab;
use App\Models\Survey\SurveyJawabDetail;
use Carbon\Carbon;
use DB;
use PDF;
use Excel;
use Yajra\DataTables\Facades\DataTables;

class HasilSurveyKepuasanAuditController extends Controller
{   
    protected $routes = 'survey-kepuasan-audit.hasil-survey';

    public function __construct()
    {   
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Survey Kepuasan Auditee' => route('survey-kepuasan-audit.survey.index'), 'Hasil Survey' => route($this->routes.'.index')]);
        $this->listStructs = [
            'listStruct' => [
                    [
                        'data' => 'num',
                        'name' => 'num',
                        'label' => '#',
                        'orderable' => false,
                        'searchable' => false,
                        'className' => 'text-center',
                        'width' => '20px',
                    ],
                    [
                        'data' => 'tahun',
                        'name' => 'tahun',
                        'label' => 'Tahun',
                        'className' => 'text-center',
                        'width' => '200px',
                        'sortable' => true,
                    ],
                    [
                        'data' => 'rencana',
                        'name' => 'rencana',
                        'label' => 'Waktu Pelaksanaan',
                        'className' => 'text-center',
                        'width' => '300px',
                    ],
                    [
                        'data' => 'tipe',
                        'name' => 'tipe',
                        'label' => 'Tipe',
                        'className' => 'text-center',
                        'width' => '200px',
                        'sortable' => true,
                    ],
                    [
                        'data' => 'kategori',
                        'name' => 'kategori',
                        'label' => 'Kategori',
                        'className' => 'text-center',
                        'width' => '250px',
                        'sortable' => true,
                    ],
                    [
                        'data' => 'object_audit',
                        'name' => 'object_audit',
                        'label' => 'Objek Audit',
                        'className' => 'text-center',
                        'width' => '250px',
                        'sortable' => true,
                    ],
                    [
                        'data' => 'no_ab',
                        'name' => 'no_ab',
                        'label' => 'No AB',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'status',
                        'name' => 'status',
                        'label' => 'Status',
                        'searchable' => false,
                        'sortable' => false,
                        'width' => '100px',
                        'className' => 'text-center'
                    ],
                    [
                        'data' => 'score',
                        'name' => 'score',
                        'label' => 'Score',
                        'className' => 'text-right',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'action',
                        'name' => 'action',
                        'label' => 'Aksi',
                        'searchable' => false,
                        'sortable' => false,
                        'width' => '80px',
                        'className' => 'text-center'
                    ],
            ],
            'listStruct2' => [
                    [
                        'data' => 'num',
                        'name' => 'num',
                        'label' => '#',
                        'orderable' => false,
                        'searchable' => false,
                        'className' => 'text-center',
                        'width' => '20px',
                    ],
                    [
                        'data' => 'tahun',
                        'name' => 'tahun',
                        'label' => 'Tahun',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '200px',
                    ],
                    [
                        'data' => 'version',
                        'name' => 'version',
                        'label' => 'Version',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '200px',
                    ],
                    [
                        'data' => 'total',
                        'name' => 'total',
                        'label' => 'Total',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'complete',
                        'name' => 'complete',
                        'label' => 'Complete',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'detil',
                        'name' => 'detil',
                        'label' => 'Detil',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ]
            ],
            'listStruct3' => [
                    [
                        'data' => 'num',
                        'name' => 'num',
                        'label' => '#',
                        'orderable' => false,
                        'searchable' => false,
                        'className' => 'text-center',
                        'width' => '50px',
                    ],
                    [
                        'data' => 'pernyataan',
                        'name' => 'pernyataan',
                        'label' => 'Pernyataan',
                        'className' => 'text-center',
                        'sortable' => true,
                    ],
                    [
                        'data' => 'jawaban_1',
                        'name' => 'jawaban_1',
                        'label' => 'Sangat Tidak Setuju',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'jawaban_2',
                        'name' => 'jawaban_2',
                        'label' => 'Tidak Setuju',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'jawaban_3',
                        'name' => 'jawaban_3',
                        'label' => 'Kurang Setuju',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'jawaban_4',
                        'name' => 'jawaban_4',
                        'label' => 'Setuju',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'jawaban_5',
                        'name' => 'jawaban_5',
                        'label' => 'Sangat Setuju',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
            ],
        ];
    }

    public function grid()
    {
        $user = auth()->user();
        if($user->hasRole(['auditor','svp-audit','dirut'])){
            $records = SurveyJawab::where('status', 2);
        }else{
            $arr_lha = SurveyJawab::getLhaByObject($user, false);
            $records = SurveyJawab::whereIn('lha_id', $arr_lha)->where('status', 2);
            // $records = SurveyJawab::where('id', 0);
        }

        if(!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at','desc');
        }
        if($tahun = request()->tahun) {
            $records->whereHas('lha', function($lha) use ($tahun){
                $lha->whereHas('draftkka', function($draftkka) use ($tahun){
                    $draftkka->whereHas('program', function($program) use ($tahun){
                        $program->whereHas('penugasanaudit', function($penugasanaudit) use ($tahun){
                            $penugasanaudit->whereHas('rencanadetail', function($rencanadetail) use ($tahun){
                                $rencanadetail->whereHas('rencanaaudit', function($rencanaaudit) use ($tahun){
                                    $rencanaaudit->where('tahun', 'like', '%' . $tahun . '%');
                                });
                            });
                        });
                    });
                });
            });
        }
        if($kategori = request()->kategori) {
            $kategori = $kategori-1;
            $records->whereHas('lha', function($lha) use ($kategori){
                $lha->whereHas('draftkka', function($draftkka) use ($kategori){
                    $draftkka->whereHas('program', function($program) use ($kategori){
                        $program->whereHas('penugasanaudit', function($penugasanaudit) use ($kategori){
                            $penugasanaudit->whereHas('rencanadetail', function($rencanadetail) use ($kategori){
                                $rencanadetail->where('tipe_object', $kategori);
                            });
                        });
                    });
                });
            });
        }
        if($object_id = request()->object_id) {
            $kategori = request()->kategori-1;
            $records->whereHas('lha', function($lha) use ($object_id, $kategori){
                $lha->whereHas('draftkka', function($draftkka) use ($object_id, $kategori){
                    $draftkka->whereHas('program', function($program) use ($object_id, $kategori){
                        $program->whereHas('penugasanaudit', function($penugasanaudit) use ($object_id, $kategori){
                            $penugasanaudit->whereHas('rencanadetail', function($rencanadetail) use ($object_id, $kategori){
                                $rencanadetail->where('tipe_object', $kategori)->where('object_id', $object_id);
                            });
                        });
                    });
                });
            });
        }

        return DataTables::of($records->get())
            ->addColumn('num', function ($record) {
               return request()->start;
            })
            ->addColumn('tahun', function ($record) {
                return $record->lha->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun;
            })
            ->addColumn('rencana', function ($record) {
                return DateToString($record->lha->draftkka->program->detailpelaksanaan->first()->tgl).' - '.DateToString($record->lha->draftkka->program->detailpelaksanaan->last()->tgl);
            })
            ->addColumn('tipe', function ($record) {
                $tipe = '';
                if($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe == 0){
                    $tipe = '<span class="label label-primary">Audit</span>';
                }elseif($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe == 1){
                    $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
                }else{
                    $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
                }
                
                return $tipe;
            })
            ->addColumn('kategori', function ($record) {
                $kategori = '';
                if($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe == 0){
                    if($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 0){
                      $kategori = '<span class="label label-success">Business Unit (BU)</span>';
                    }elseif($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 1){
                      $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
                    }elseif($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 2){
                      $kategori = '<span class="label label-warning">Project</span>';
                    }else{
                      $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
                    }
                }elseif($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe == 1){
                    $kategori = $record->lha->draftkka->program->penugasanaudit->rencanadetail->konsultasi->nama;
                }else{
                    $kategori = $record->lha->draftkka->program->penugasanaudit->rencanadetail->lain->nama;
                }
                return $kategori;
            })
            ->addColumn('object_audit', function ($record) {
                return object_audit($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->lha->draftkka->program->penugasanaudit->rencanadetail->object_id);
            })
            ->addColumn('no_ab', function ($record) {
                return getAb($record->lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->lha->draftkka->program->penugasanaudit->rencanadetail->object_id);
            })
            ->editColumn('status', function ($record) {
                if($record->status == 0){
                   return '<span class="label label-success">Baru</span>';
                }elseif($record->status == 1){
                    return '<span class="label label-warning">Draft</span>';
                }else{
                    return '<span class="label label-success">Completed</span>';
                }
            })
            ->editColumn('score', function ($record) {
                $total_soal = count($record->survey_jawab_detail->where('jenis', 1));
                $total_jawab = $record->survey_jawab_detail->where('jenis', 1)->sum('jawaban');
                $average = $total_jawab/$total_soal;
                return number_format($average, 2, '.', ',');
            })
            ->addColumn('action', function ($record) use ($user) {
                $buttons = '';
                $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-eye text-primary"></i>',
                    'tooltip'   => 'Detil Survey',
                    'class'     => 'detil-survey button',
                    'id'        => $record->id,
                ]);
                $buttons .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-print text-info"></i>',
                    'tooltip'   => 'Cetak',
                    'class'     => 'cetak button',
                    'id'        => $record->id,
                ]);
                return $buttons;
            })
           ->rawColumns(['tipe','kategori','object_audit','status','lha', 'action'])
           ->make(true);
    }

    public function item()
    {
        $user = auth()->user();
        if($user->hasRole(['auditor','svp-audit','dirut'])){
            $by_years = SurveyJawab::get()->groupBy(function($date) {
                            return Carbon::parse($date->created_at)->format('Y');
                        });
            $records = [];
            foreach ($by_years as $tahun => $by_year) {
                $by_surveys = $by_year->groupBy('survei_id');
                foreach ($by_surveys as $survei_id => $survey_jawab) {
                    $records[] = [
                        'tahun' => $tahun, 
                        'version' => $survey_jawab->first()->survey->version, 
                        'survei_id' => $survei_id, 
                        'total' => count($survey_jawab), 
                        'completed' => count($survey_jawab->where('status', 2))
                    ];
                }
            }
            $records = json_decode(json_encode($records));
        }else{
            $records = json_decode(json_encode([]));
        }

        return DataTables::of($records)
            ->addColumn('num', function ($record) {
                return request()->start;
            })
            ->addColumn('tahun', function ($record) {
                return $record->tahun;
            })
            ->addColumn('version', function ($record) {
                return $record->version;
            })
            ->addColumn('total', function ($record) {
                return $record->total.' Survey';
            })
            ->addColumn('complete', function ($record) {
                return $record->completed.' Survey';
            })
            ->addColumn('detil', function ($record) {
                $buttons = '';
                $buttons .= $this->makeButton([
                    'type'    => 'edit',
                    'label'   => '<i class="fa fa-eye text-primary"></i>',
                    'tooltip' => 'Detil',
                    'class'   => 'detil-item button',
                    'id'      => $record->survei_id,
                    'datas'   => ['tahun' => $record->tahun],
                ]);
                $buttons .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-file-excel-o text-primary"></i>',
                    'tooltip'   => 'Download',
                    'class'     => 'detil-item-excel button',
                    'id'      => $record->survei_id,
                    'datas'   => ['tahun' => $record->tahun],
                ]);

                return $buttons;
            })
            ->rawColumns(['tahun', 'version', 'detil'])
            ->make(true);
    }

    public function index()
    {
        return $this->render('modules.survey.hasil.index', [
            'mockup' => true,
            'structs' => $this->listStructs,
            'badgeTab' => SurveyJawab::badgeTab('hasil')
        ]);
    }

    public function detilSurvey(SurveyJawab $id)
    {
        $this->pushBreadcrumb(['Detil Survey' => '#']);
        return $this->render('modules.survey.hasil.detil-survey', [
            'record' => $id,
            'survey' => $id->survey,
            'survey_jawab_detail' => $id->survey_jawab_detail
        ]);
    }

    public function detilItem(Survey $survey, $tahun)
    {
        $this->setBreadcrumb(['Survey Kepuasan Auditee' => route('survey-kepuasan-audit.survey.index'), 'Hasil Survey' => route('survey-kepuasan-audit.hasil-survey.index'), 'Detail Item' => '#']);
        return $this->render('modules.survey.hasil.detil-item', [
            'structs' => $this->listStructs,
            'records' => $survey,
            'tahun' => $tahun,
            'data_grafik' => $this->grafik($survey, $tahun)
        ]);
    }

    public function grafik($survey, $tahun)
    {
        $no = 1;
        $data = [];
        if($survey){
            foreach ($survey->pertanyaan as $key => $value) {
                $score[1] = 0;
                $score[2] = 0;
                $score[3] = 0;
                $score[4] = 0;
                $score[5] = 0;
                $survey_jawab = $survey->survey_jawab()->whereYear('created_at', '=', $tahun)->get();
                if (count($survey_jawab) > 0) {
                    foreach($survey_jawab->where('status', 2) as $val){
                        $score[1] = $score[1] + $val->survey_jawab_detail->where('tanya_id', $value->id)->where('jawaban', 1)->count();
                        $score[2] = $score[2] + $val->survey_jawab_detail->where('tanya_id', $value->id)->where('jawaban', 2)->count();
                        $score[3] = $score[3] + $val->survey_jawab_detail->where('tanya_id', $value->id)->where('jawaban', 3)->count();
                        $score[4] = $score[4] + $val->survey_jawab_detail->where('tanya_id', $value->id)->where('jawaban', 4)->count();
                        $score[5] = $score[5] + $val->survey_jawab_detail->where('tanya_id', $value->id)->where('jawaban', 5)->count();
                    }
                    for ($i=1; $i <= 5; $i++) { 
                        $data['chart_'.$i]['label'][] = $no;
                        $data['chart_'.$i]['data'][] = (int) $score[$i];
                    }
                    $no++;
                }
            }
        }else{
            for ($i=1; $i <= 5; $i++) { 
                $data['chart_'.$i]['label'][] = 0;
                $data['chart_'.$i]['data'][] = 0;
            }
        }

        $result = [
            'chart_data_1' => json_encode($data['chart_1']),
            'chart_data_2' => json_encode($data['chart_2']),
            'chart_data_3' => json_encode($data['chart_3']),
            'chart_data_4' => json_encode($data['chart_4']),
            'chart_data_5' => json_encode($data['chart_5']),
        ];
        return $result;
    }

    public function downloadExcel(Survey $survey, $tahun)
    {
        $user = auth()->user();
        if($user->hasRole(['auditor','svp-audit','dirut'])){
            $view       = 'modules.survey.hasil.export-excel';
            $data_array = [
                'title' => 'Detil Item Hasil Survey',
                'survey' => $survey,
                'tahun' => $tahun,
            ];
            $tittle     ='Detil Item Hasil Survey';

            return Excel::download(new SurveyExport($view, $data_array), $tittle.'.xlsx');
        }
        return abort(404);
    }

    public function cetak(SurveyJawab $id)
    {
        $user = auth()->user();
        if(($user->hasRole(['auditor','svp-audit','dirut']) || $user->id == $id->user->id) && $id->status == 2){
            $data = [
                'record' => $id,
                'survey' => $id->survey,
                'survey_jawab_detail' => $id->survey_jawab_detail
            ];
            $pdf = PDF::loadView('modules.survey.audit.cetak', $data);
            return $pdf->stream('Survey Kepuasan Audit '.Carbon::parse($id->updated_at)->format('Y-m-d').'.pdf',array("Attachment" => false));
        }
        return abort(404);
    }
}