<?php

namespace App\Http\Controllers\Rapat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Rapat\RapatEksternalRequest;
use App\Models\Rapat\RapatInternal;
use App\Models\Rapat\UndanganPesertaEksternal;

use App\Models\SessionLog;
use App\Models\Auths\User;
use Mail;
use App\Mail\RapatEmail;

use DB;
use Carbon\Carbon;
use App\Libraries\CoreSn;
use ZipArchive;
use Storage;
use PDF;

class RapatAdvisorController extends Controller
{
    protected $routes = 'rapat.eksternal-advisor';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setTitle('Rapat Advisor');
        $this->setBreadcrumb(['Rapat' => '#', 'Eksternal' => '#', 'Rapat Advisor' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'nomor',
                'name' => 'nomor',
                'label' => 'Nomor Rapat',
                'width' => '100px',
                'className' => 'text-center',
                'sortable' => true,
            ],
            [
                'data' => 'materi',
                'name' => 'materi',
                'label' => 'Tema / Materi Rapat',
                'width' => '300px',
                'sortable' => true,
            ],
            [
                'data' => 'tanggal',
                'name' => 'tanggal',
                'label' => 'Tanggal Pelaksanaan',
                'className' => 'text-center',
                'width' => '90px',
                'sortable' => true,
            ],
            [
                'data' => 'tempat',
                'name' => 'tempat',
                'label' => 'Lokasi Rapat',
                'width' => '100px',
                'sortable' => true,
            ],
            [
                'data' => 'jumlah_peserta',
                'name' => 'jumlah_peserta',
                'label' => 'Jumlah Peserta',
                'width' => '100px',
                'className' => 'text-center',
                'sortable' => true,
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '80px',
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => 'text-center',
                'width' => '100px',
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '116px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $tanggal = '';
        if(request()->tanggal){
            $tanggal = Carbon::createFromFormat('d/m/Y',request()->tanggal)->format('m/d/Y');
        }
        $records = RapatInternal::where('status', 1)->when($name = request()->nomor, function ($q) use ($name) {
            			    $q->where('nomor', 'like', '%'.$name.'%');
                        })->when($tema = request()->tema, function ($q) use ($tema) {
                            $q->where('materi', 'like', '%'.$tema.'%');
                        })->when($tanggal, function ($q) use ($tanggal) {
                            $q->where('tanggal', 'like', '%'.$tanggal.'%');
                        })->when($lokasi = request()->lokasi, function ($q) use ($lokasi) {
                            $q->where('tempat', 'like', '%'.$lokasi.'%');
                        })->orderBy('id','desc')->select('*');

        return DataTables::of($records)
            ->addColumn('num', function ($record) {
                return request()->start;
            })
            ->editColumn('tanggal', function ($record) {
                return Carbon::createFromFormat('m/d/Y',$record->tanggal)->format('d/m/Y').' '.$record->waktu;
            })
            ->editColumn('jumlah_peserta', function ($record) {
                return $record->daftarHadir->count().' Orang';
            })
            // ->editColumn('status', function ($record) {
            //     $shw= '<span class="label label-default">Rapat Belum Dimulai</span>';
            //     if((Carbon::now()->diffInDays($record->tanggal,false) < 0) OR (Carbon::now()->format('Y-m-d') == CoreSn::DateToSql($record->tanggal))){
            //         $jam = Carbon::createFromFormat('m/d/Y H:i',$record->tanggal.' '.$record->waktu);
            //         $jamnow = Carbon::now();
            //         $convert= Carbon::parse($jam->format('H:i:s'))->floatDiffInHours($jamnow->format('H:i:s'), false); 
            //         if($convert > -1){
            //             if($convert > 0){
            //                 $shw= '<span class="label label-danger">Rapat Sedang Dimulai</span>';
            //             }else{
            //                 $shw= '<span class="label label-info">Rapat Akan Segera Dimulai</span>';
            //             }
            //         }else{
            //             $shw= '<span class="label label-default">Rapat Belum Dimulai</span>';
            //         }
            //     }
            //     if(Carbon::now()->diffInDays($record->tanggal,false) < 0){
            //         $shw= '<span class="label label-success">Rapat Telah Selesai</span>';
            //     }
            //     return $shw ;
            // })
            ->editColumn('status', function ($record) {
                if ($record->flag == 0) {
                    $shw= '<span class="label label-warning">Draft</span>';
                } else {
                    $shw= '<span class="label label-default">Rapat Belum Dimulai</span>';
                    if((Carbon::now()->diffInDays($record->tanggal,false) < 0) OR (Carbon::now()->format('Y-m-d') == CoreSn::DateToSql($record->tanggal))){
                        $jam = Carbon::createFromFormat('m/d/Y H:i',$record->tanggal.' '.$record->waktu);
                        $jamnow = Carbon::now();
                        // $diff = abs($jamnow - $jam);
                        $convert= Carbon::parse($jam->format('H:i:s'))->floatDiffInHours($jamnow->format('H:i:s'), false); 
                        if($convert > -1){
                            if($convert > 0){
                                $shw= '<span class="label label-danger">Rapat Sedang Dimulai</span>';
                            }else{
                                $shw= '<span class="label label-info">Rapat Akan Segera Dimulai</span>';
                            }
                        }else{
                            $shw= '<span class="label label-default">Rapat Belum Dimulai</span>';
                        }
                    }
                    if(Carbon::now()->diffInDays($record->tanggal,false) < 0){
                        $shw= '<span class="label label-success">Rapat Telah Selesai</span>';
                    }
                }
                return $shw ;
            })
            ->editColumn('created_at', function ($record) {
                return $record->created_at->diffForHumans();
            })
            // ->addColumn('action', function ($record) {
            //     $buttons = '';
            //     if(Carbon::now()->format('Y-m-d') == CoreSn::DateToSql($record->tanggal)){
            //         $jam = Carbon::createFromFormat('m/d/Y H:i',$record->tanggal.' '.$record->waktu);
            //         $jamnow = Carbon::now();
            //         $convert= Carbon::parse($jam->format('H:i:s'))->floatDiffInHours($jamnow->format('H:i:s'), false); 
            //         if($convert > -1){
            //             $buttons .= $this->makeButton([
            //                 'type' => 'show-modal',
            //                 'class' => 'show-modal button',
            //                 'label' => '<i class="fa fa-qrcode text-info"></i>',
            //                 'tooltip' => 'Lihat QR-Code',
            //                 'id'   => $record->id,
            //             ]);
            //         }
            //     }

            //     $buttons .= $this->makeButton([
            //         'type' => 'show-modal',
            //         'class' => 'm-l re-upload button',
            //         'label' => '<i class="fa fa-check-circle-o text-danger"></i>',
            //         'tooltip' => 'Lengkapi Data Rapat',
            //         'id'   => $record->id,
            //     ]);

            //     $buttons .= $this->makeButton([
            //         'type' => 'url',
            //         'class' => 'm-l url button',
            //         'label' => '<i class="fa fa-download text-info"></i>',
            //         'tooltip' => 'Download Berkas Rapat',
            //         'url' => 'eksternal-advisor/download/'.$record->id,
            //         'id'   => $record->id,
            //     ]);

            //     $buttons .= $this->makeButton([
            //         'class' => 'm-l edit button',
            //         'type' => 'edit',
            //         'id'   => $record->id,
            //     ]);
                
            //     $buttons .= $this->makeButton([
            //         'class' => 'm-l detil button',
            //         'type' => 'detail',
            //         'id'   => $record->id,
            //     ]);

            //     $buttons .= $this->makeButton([
            //         'type' => 'delete',
            //         'id'   => $record->id,
            //     ]);

            //     return $buttons;
            // })
            // 
            ->addColumn('action', function ($record) {
                $buttons = '';
                if ($record->flag == 0) {
                    $buttons .= $this->makeButton([
                        'class' => 'm-l edit button',
                        'type' => 'edit',
                        'id'   => $record->id,
                    ]);

                    $buttons .= $this->makeButton([
                        'type' => 'delete',
                        'id'   => $record->id,
                    ]);
                } else {
                    if(Carbon::now()->format('Y-m-d') == CoreSn::DateToSql($record->tanggal)){
                    $jam = Carbon::createFromFormat('m/d/Y H:i',$record->tanggal.' '.$record->waktu);
                    $jamnow = Carbon::now();
                    $convert= Carbon::parse($jam->format('H:i:s'))->floatDiffInHours($jamnow->format('H:i:s'), false); 
                        if($convert > -1){
                            $buttons .= $this->makeButton([
                                'type' => 'show-modal',
                                'class' => 'show-modal button',
                                'label' => '<i class="fa fa-qrcode text-info"></i>',
                                'tooltip' => 'Lihat QR-Code',
                                'id'   => $record->id,
                            ]);

                            $buttons .= $this->makeButton([
                                'type' => 'show-modal',
                                'class' => 'm-l re-upload button',
                                'label' => '<i class="fa fa-check-circle-o text-danger"></i>',
                                'tooltip' => 'Lengkapi Data Rapat',
                                'id'   => $record->id,
                            ]);
                        }
                    }

                    if(Carbon::now()->diffInDays($record->tanggal,false) < 0){
                        $buttons .= $this->makeButton([
                            'type' => 'show-modal',
                            'class' => 'm-l re-upload button',
                            'label' => '<i class="fa fa-check-circle-o text-danger"></i>',
                            'tooltip' => 'Lengkapi Data Rapat',
                            'id'   => $record->id,
                        ]);
                        // $buttons .= $this->makeButton([
                        //     'type' => 'url',
                        //     'class' => 'm-l url button',
                        //     'label' => '<i class="fa fa-download text-info"></i>',
                        //     'tooltip' => 'Download Berkas Rapat',
                        //     'url' => 'eksternal-advisor/download/'.$record->id,
                        //     'id'   => $record->id,
                        // ]);
                        // dd($record->risalah());
                        if ($record->risalah()->count() == 0 || $record->daftarHadir()->count() == 0) {
                            $buttons .= $this->makeButton([
                                'type' => 'url',
                                'class' => 'm-l',
                                'label' => '<i class="fa fa-download text-grey"></i>',
                                'tooltip' => 'Data Rapat Belum Lengkap',
                            ]);
                        } else {
                            $buttons .= $this->makeButton([
                                'type' => 'url',
                                'class' => 'm-l url button',
                                'label' => '<i class="fa fa-download text-info"></i>',
                                'tooltip' => 'Download Berkas Rapat',
                                'url' => 'eksternal-advisor/download/'.$record->id,
                                'id'   => $record->id,
                            ]);
                        }
                    }
                    $buttons .= $this->makeButton([
                                'class' => 'm-l detil button',
                                'type' => 'detail',
                                'id'   => $record->id,
                            ]);

                }

                return $buttons;
            })
            ->rawColumns(['alamat', 'action','status'])
            ->make(true);
    }

    public function index()
    {
        return $this->render('modules.rapat.eksternal.index');
    }

    public function create()
    {
        $this->setBreadcrumb(['Rapat' => '#', 'Eksternal' => '#', 'Rapat Advisor' => '#', 'Tambah' => '#']);
        return $this->render('modules.rapat.eksternal.create');
    }

    public function store(RapatEksternalRequest $request)
    {
        // dd($request->all());
    	DB::beginTransaction();
    	try {
            $request['tanggal'] = Carbon::createFromFormat('d/m/Y',$request->tanggal)->format('m/d/Y');
            $request['status'] = 1;
    		$record = new RapatInternal;
            $record->fill($request->all());
            $record->save();
	        $record->saveDetail($request->detail);
            if ($request['flag'] == 1) {
                $record = RapatInternal::find($record->id);
                foreach ($record->undanganEksternal as $user) 
                    {
                        Mail::to($user->email)->queue(new RapatEmail($record, $user, 'Konfirmasi Kehadiran Peserta Undangan'));
                        $user->status = 1;
                        $user->save();
                    }  
            }

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function edit($id)
    {
        $this->setBreadcrumb(['Rapat' => '#', 'Eksternal' => '#', 'Rapat Advisor' => '#', 'Ubah' => '#']);
        $record = RapatInternal::find($id);
        return $this->render('modules.rapat.eksternal.edit', ['record' => $record]);
    }
    
    public function show($id)
    {
        $this->setBreadcrumb(['Rapat' => '#', 'Eksternal' => '#', 'Rapat Advisor' => '#', 'Detil' => '#']);
        $record = RapatInternal::find($id);
        return $this->render('modules.rapat.eksternal.detail', ['record' => $record]);
    }
    
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request['tanggal'] = Carbon::createFromFormat('d/m/Y',$request->tanggal)->format('m/d/Y');
            $request['status'] = 1;
        	$record = RapatInternal::find($request->id);
	        $record->fill($request->all());
            $record->save();
            // if(isset($request->type)){
            //     if(isset($request->daftar_hadir)){
            //         $record->saveHadirFile($request->daftar_hadir,'put');
            //     }
            //     if(isset($request->other[0])){
            //         $record->saveOtherFile($request->other,'put');
            //     }
            //     if($record->status != 0){
            //         $record->saveDaftarHadir($request,'put');
            //     }
            //     $record->saveRisalah($request,'put');
            // }
            $record->updateDetail($request->detail);
            if ($request['flag'] == 1) {
                $record = RapatInternal::find($record->id);
                foreach ($record->undanganEksternal as $user) 
                    {
                        Mail::to($user->email)->queue(new RapatEmail($record, $user, 'Konfirmasi Kehadiran Peserta Undangan'));
                        $user->status = 1;
                        $user->save();
                    }  
            }

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }
    
    public function postUpload(Request $request, $id)
    {
        // dd($request->all());
        // dd(empty($request->exists));
        DB::beginTransaction();
        try {
            // if(!empty($request->exists)){
            //     $hapus = Risalah::whereNotIn('id', $request->exists)
            //                       ->delete();
            // }
            // $hapus = Risalah::whereNotIn('id', $request->exists)
            //                                       ->delete();
            $record = RapatInternal::find($request->id);
            $record->fill($request->all());
            $record->save();
            // dd($record);
            $record->updateRisalahEkternal($request->detail);
            if(isset($request->daftar_hadir[0])){
                $record->saveHadirFile($request->daftar_hadir);
            }
            if($record->status != 0){
                    $record->saveDaftarHadir($request,'put');
                }
            if(isset($request->other[0])){
                $record->saveOtherFile($request->other);
            }

            DB::commit();
            return response([
              'status' => true
            ]); 
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy($id)
    {
        $data = RapatInternal::find($id);
        $data->unlinkFiles();
        $data->files()->delete();
        $data->daftarHadir()->delete();
        $data->risalah()->delete();
        $data->delete();
        return response()->json([
            'success' => true
        ]);
    }

    public function showCode($id)
    {
        $record = RapatInternal::find($id);
        $l = route('ex.showing',['id' => md5($id), 'id_real' => base64_encode($id), 'date' => base64_encode(Carbon::now()) ]);
        return $this->render('modules.rapat.eksternal.show-code', [
            'record' => $record,
            'link' => $l,
            'mark' => asset('src/img/logo-mini.png'),
            ]);
    }

    public function reUpload($id)
    {
        $this->setBreadcrumb(['Rapat' => '#', 'Eksternal' => '#', 'Rapat Advisor' => '#', 'Lengkapi' => '#']);
        $record = RapatInternal::find($id);
        return $this->render('modules.rapat.eksternal.re-upload', ['record' => $record]);
    }

    public function download($id)
    {
        $zip = new ZipArchive;
        $record = RapatInternal::find($id);
        $title = 'ADVISOR';
        $fileName = '/app/public/'.str_replace('/','',$record->nomor).'.zip';
        if(file_exists(storage_path().'/app/public/'.str_replace('/','',$record->nomor).'.zip'))
        {
            unlink(storage_path().'/app/public/'.str_replace('/','',$record->nomor).'.zip');
        }
        if(file_exists(storage_path().'/app/public/Daftar Hadir.pdf'))
        {
            unlink(storage_path().'/app/public/Daftar Hadir.pdf');
        }
        if(file_exists(storage_path().'/app/public/Risalah.pdf'))
        {
            unlink(storage_path().'/app/public/Risalah.pdf');
        }
        if ($zip->open(storage_path().$fileName, ZipArchive::CREATE) === TRUE)
        {
            if(isset($record->files) AND $record->files->count() > 0){
                foreach ($record->files as $key => $value) {
                    if(file_exists(storage_path().'/app/public/'.$value->url))
                    {
                        if($value->flag == 'hadir'){
                            $key = $key+1;
                            $files = storage_path().'/app/public/'.$value->url;
                            $zip->addFile($files,'Daftar Hadir/'.$value->filename);
                        }else{
                            $key = $key+1;
                            $files = storage_path().'/app/public/'.$value->url;
                            $zip->addFile($files,'Lampiran/'.$value->filename);
                        }
                    }
                }
            }
        }
        if(isset($record->risalah) AND $record->risalah->count() > 0){
            $pdf = PDF::loadView('modules.rapat.eksternal.risalah-pdf', [
                'record' => $record,
                'today' => CoreSn::DateToString(Carbon::now()),
                'user' => auth()->user(),
                'title' => $title,
            ])->setPaper('a4', 'landscape')->setOptions(
                [
                    'defaultFont' => 'times-roman',
                    'isHtml5ParserEnabled' => true,
                    'isRemoteEnabled' => true,
                    'isPhpEnabled' => true
                ]
            );
            $uri = storage_path().'/app/public/Risalah.pdf';
            file_put_contents($uri, $pdf->output());
            $zip->addFile($uri,'Dokumen/Risalah.pdf');
        }

        if(isset($record->daftarHadir) AND $record->daftarHadir->count() > 0){
            $blnthn = explode(" ", CoreSn::DateToStringWDay($record->tanggal));
            $blnthn = $blnthn[2].' '.$blnthn[3];
            $pdf = PDF::loadView('modules.rapat.eksternal.hadir-pdf', [
                'record' => $record,
                'today' => strtoupper(CoreSn::DateToStringWDay($record->tanggal)),
                'blnthn' => strtoupper($blnthn),
                'user' => auth()->user(),
                'title' => $title,
            ])->setPaper('a4', 'potrait')->setOptions(
                [
                    'defaultFont' => 'times-roman',
                    'isHtml5ParserEnabled' => true,
                    'isRemoteEnabled' => true,
                    'isPhpEnabled' => true
                ]
            );
            $uri = storage_path().'/app/public/Daftar Hadir.pdf';
            file_put_contents($uri, $pdf->output());
            $zip->addFile($uri,'Dokumen/Daftar Hadir.pdf');
        }


        $zip->close();
        
        if(file_exists(storage_path().'/app/public/'.str_replace('/','',$record->nomor).'.zip'))
        {
            return response()->download(storage_path().'/app/public/'.str_replace('/','',$record->nomor).'.zip');
        }
        return abort(404);
    }

    public function hadir($id)
    {
        $record = UndanganPesertaEksternal::find($id);
        $record->status = 2;
        $record->save();
    }

    public function tidakHadir($id)
    {
        $record = UndanganPesertaEksternal::find($id);
        $record->status = 3;
        $record->save();
    }

    public function email($id)
    {
        $record = RapatInternal::find($id);
        foreach ($record->undanganEksternal as $user) 
        {
            Mail::send('partials.emails.mail', ['record' => $record, 'user' => $user],
                function ($message) use ($user) {
                    $message->subject('Konfirmasi Kehadiran Peserta Undangan');
                    $message->to($user['email']);
                });
        }
    }

}

// if (!empty($users)) {
//     foreach ($record->undanganEksternal as $user) {
//         Mail::send('partials.emails.mail', ['record' => $record, 'user' => $user],
//             function ($message) use ($user) {
//                 $message->subject('Konfirmasi Kehadiran Peserta Undangan');
//                 $message->to($user['email']);
//             });
//     }
// }
