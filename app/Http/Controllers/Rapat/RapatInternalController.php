<?php

namespace App\Http\Controllers\Rapat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Rapat\RapatInternalRequest;
use App\Models\Rapat\RapatInternal;
use App\Models\Rapat\UndanganPesertaInternal;
use App\Models\Rapat\Risalah;

use App\Models\SessionLog;
use App\Models\Auths\User;
use Mail;
use App\Mail\RapatEmail;
use App\Jobs\RisalahPdfEmail;

use DB;
use Carbon\Carbon;
use App\Libraries\CoreSn;
use ZipArchive;
use Storage;
use PDF;

class RapatInternalController extends Controller
{
    protected $routes = 'rapat.internal';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setTitle('Rapat Internal');
        $this->setBreadcrumb(['Rapat' => '#', 'Internal' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'nomor',
                'name' => 'nomor',
                'label' => 'Nomor Rapat',
                'width' => '100px',
                'className' => 'text-center',
                'sortable' => true,
            ],
            [
                'data' => 'materi',
                'name' => 'materi',
                'label' => 'Agenda',
                'width' => '300px',
                'sortable' => true,
            ],
            [
                'data' => 'tempat',
                'name' => 'tempat',
                'label' => 'Tempat',
                'width' => '100px',
                'sortable' => true,
            ],
            [
                'data' => 'tanggal',
                'name' => 'tanggal',
                'label' => 'Tanggal',
                'className' => 'text-center',
                'width' => '90px',
                'sortable' => true,
            ],
            [
                'data' => 'jam',
                'name' => 'jam',
                'label' => 'Jam',
                'className' => 'text-center',
                'width' => '90px',
                'sortable' => true,
            ],
            [
                'data' => 'jumlah_peserta',
                'name' => 'jumlah_peserta',
                'label' => 'Jumlah Peserta',
                'width' => '100px',
                'className' => 'text-center',
                'sortable' => true,
            ],
            [
                'data' => 'kategori',
                'name' => 'kategori',
                'label' => 'Kategori',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '80px',
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '80px',
            ],
            [
                'data' => 'created_by',
                'name' => 'created_by',
                'label' => 'Dibuat Oleh',
                'width' => '100px',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Diperbarui Pada',
                'className' => 'text-center',
                'width' => '100px',
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '116px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $tanggal = '';
        if(request()->tanggal){
            $tanggal = Carbon::createFromFormat('d-m-Y',request()->tanggal)->format('d-m-Y');
        }

        $records = RapatInternal::with('agendaInternal')
                        ->where('status', 0)->when($name = request()->nomor, function ($q) use ($name) {
            			    $q->where('nomor', 'like', '%'.$name.'%');
                        })->when($tema = request()->tema, function ($q) use ($tema) {
                            $q->whereHas('agendaInternal', function ($query) use ($tema){
                                $query->where('agenda', 'like', '%'.$tema.'%');
                            });
                        // })->when($tema = request()->tema, function ($q) use ($tema) {
                        //     $q->where('materi', 'like', '%'.$tema.'%');
                        })->when($tanggal, function ($q) use ($tanggal) {
                            $q->where('tanggal', $tanggal);
                        // })->when($date, function ($q) use ($date) {
                        //     $q->where('tanggal', 'like', '%'.$date.'%');
                        })->when($lokasi = request()->lokasi, function ($q) use ($lokasi) {
                            $q->where('tempat', 'like', '%'.$lokasi.'%');
                        })
                        // ->where('created_at')
                        // ->orWhere('updated_at')
                        // ->orderBy('updated_at', 'desc')
                        // // ->orderByRaw("updated_at DESC, created_at DESC")
                        // // ->latest('id')
                        // ->orderBy('updated_at','desc')
                        ->orderBy('created_at','desc')
                        ->select('*');

        // dd($tanggal);
        return DataTables::of($records)
            ->addColumn('num', function ($record) {
                return request()->start;
            })
            ->editColumn('tanggal', function ($record) {
                return CoreSn::DateToStringWDay($record->tanggal);
            })
            ->editColumn('jam', function ($record) {
                return $record->jam_mulai.' - '.$record->jam_selesai;
            })
            ->addColumn('materi', function ($record) {
                  $string ='';
                  $string .='<ul class="list list-more1" data-display="3">';
                  foreach ($record->agendaInternal as $key => $value) {
                      $string .='<li class="item">'.readMoreText($value->agenda, 100).'</li>';
                  }
                  $string .='</ul>';
                    return $string;
            })
            ->editColumn('jumlah_peserta', function ($record) {
                return $record->daftarHadir->count().' Orang';
            })
            ->editColumn('kategori', function ($record) {
                if ($record->status_kategori == 0) {
                    if($record->kategori == 'Kegiatan Konsultasi'){
                        return '<span class="label label-success">Kegiatan Konsultasi</span>';
                    }elseif($record->kategori == 'Kegiatan Lainnya'){
                        return '<span class="label label-info">Kegiatan Lainnya</span>';
                    }else{
                        return '-';
                    }
                }else{
                    if($record->status_kategori == 1){
                        return '<span class="label label-primary">Umum</span>';
                    }elseif($record->status_kategori == 2){
                        return '<span class="label label-primary">Konsultasi</span>';
                    }
                }
            })
            ->editColumn('status', function ($record) {
                if ($record->flag == 0) {
                    $shw= '<span class="label label-warning">Draft</span>';
                } else {
                    $shw= '<span class="label label-default">Rapat Belum Dimulai</span>';
                    if((Carbon::now()->diffInDays($record->tanggal,false) < 0) OR (Carbon::now()->format('d-m-Y') == CoreSn::DateToSql($record->tanggal))){
                        $jam = Carbon::createFromFormat('d-m-Y H:i',$record->tanggal.' '.$record->jam_mulai);
                        $jamnow = Carbon::now();
                        // $diff = abs($jamnow - $jam);
                        $convert= Carbon::parse($jam->format('H:i:s'))->floatDiffInHours($jamnow->format('H:i:s'), false);
                        if($convert > -1){
                            if($convert > 0){
                                $shw= '<span class="label label-danger">Rapat Sedang Dimulai</span>';
                            }else{
                                $shw= '<span class="label label-info">Rapat Akan Segera Dimulai</span>';
                            }
                        }else{
                            $shw= '<span class="label label-default">Rapat Belum Dimulai</span>';
                        }
                    }
                    if(Carbon::now()->diffInDays($record->tanggal,false) < 0){
                        $shw= '<span class="label label-success">Rapat Telah Selesai</span>';
                    }
                }
                return $shw ;
            })
            ->editColumn('created_by', function ($record) {
                   return $record->entryBy();
            })
            ->editColumn('created_at', function ($record) {
                return $record->updated_at ? $record->updated_at->diffForHumans() : $record->created_at->diffForHumans();
            })
            ->addColumn('action', function ($record) {
                $buttons = '';
                if ($record->flag == 0) {
                    if (auth()->user()->id == $record->created_by) {
                        $buttons .= $this->makeButton([
                            'class' => 'm-l edit button',
                            'type' => 'edit',
                            'id'   => $record->id,
                        ]);

                        $buttons .= $this->makeButton([
                            'type' => 'delete',
                            'id'   => $record->id,
                        ]);
                    } else {
                        $buttons .= $this->makeButton([
                            'class' => 'm-l detil button',
                            'type' => 'detail',
                            'id'   => $record->id,
                        ]);
                    }
                } else {
                    if(Carbon::now()->format('d-m-Y') == CoreSn::DateToSql($record->tanggal)){
                    $jam = Carbon::createFromFormat('d-m-Y H:i',$record->tanggal.' '.$record->jam_mulai);
                    $jamnow = Carbon::now();
                    // $diff = abs($jamnow - $jam);
                    $convert= Carbon::parse($jam->format('H:i:s'))->floatDiffInHours($jamnow->format('H:i:s'), false);
                        if($convert > -1){
                            $buttons .= $this->makeButton([
                                'type' => 'show-modal',
                                'class' => 'show-modal button',
                                'label' => '<i class="fa fa-qrcode text-info"></i>',
                                'tooltip' => 'Lihat QR-Code',
                                'id'   => $record->id,
                            ]);

                            if ($record->status_rapat == 0) {
                                $buttons .= $this->makeButton([
                                    'type' => 'show-modal',
                                    'class' => 'm-l re-upload button',
                                    'label' => '<i class="fa fa-check-circle-o text-danger"></i>',
                                    'tooltip' => 'Lengkapi Data Rapat',
                                    'id'   => $record->id,
                                ]);
                            } else {
                                $buttons .= $this->makeButton([
                                    'type' => 'url',
                                    'class' => 'm-l url button',
                                    'label' => '<i class="fa fa-download text-info"></i>',
                                    'tooltip' => 'Download Berkas Rapat',
                                    'url' => 'internal/download/'.$record->id,
                                    'id'   => $record->id,
                                ]);

                                $buttons .= $this->makeButton([
                                    'type'      => 'edit',
                                    'label'     => '<i class="fa fa-print text-primary"></i>',
                                    'tooltip'   => 'Cetak',
                                    'class'     => 'm-l cetak button',
                                    'id'        => $record->id,
                                ]);
                            }
                        }
                    }

                    if(Carbon::now()->diffInDays($record->tanggal,false) < 0){

                        if ($record->status_rapat == 0) {
                        // if ($record->risalah()->count() == 0 || $record->daftarHadir()->count() == 0) {
                            $buttons .= $this->makeButton([
                                'type' => 'show-modal',
                                'class' => 'm-l re-upload button',
                                'label' => '<i class="fa fa-check-circle-o text-danger"></i>',
                                'tooltip' => 'Lengkapi Data Rapat',
                                'id'   => $record->id,
                            ]);

                            $buttons .= $this->makeButton([
                                'type' => 'url',
                                'class' => 'm-l',
                                'label' => '<i class="fa fa-download text-grey"></i>',
                                'tooltip' => 'Data Rapat Belum Lengkap',
                            ]);
                        } else {
                            $buttons .= $this->makeButton([
                                'type' => 'url',
                                'class' => 'm-l url button',
                                'label' => '<i class="fa fa-download text-info"></i>',
                                'tooltip' => 'Download Berkas Rapat',
                                'url' => 'internal/download/'.$record->id,
                                'id'   => $record->id,
                            ]);

                            $buttons .= $this->makeButton([
                                'type'      => 'edit',
                                'label'     => '<i class="fa fa-print text-primary"></i>',
                                'tooltip'   => 'Cetak',
                                'class'     => 'm-l cetak button',
                                'id'        => $record->id,
                            ]);
                        }
                    }
                    $buttons .= $this->makeButton([
                        'class' => 'm-l detil button',
                        'type' => 'detail',
                        'id'   => $record->id,
                    ]);
                }

                return $buttons;
            })
            ->rawColumns(['alamat', 'action', 'materi', 'status', 'kategori'])
            ->make(true);
    }

    public function index()
    {
        return $this->render('modules.rapat.internal.index');
    }

    public function create()
    {
        $this->setBreadcrumb(['Rapat' => '#', 'Internal' => '#', 'Tambah' => '#']);
        return $this->render('modules.rapat.internal.create');
    }

    public function store(RapatInternalRequest $request)
    {
        // dd($request->all());
    	DB::beginTransaction();
    	try {
            $request['tanggal'] = Carbon::createFromFormat('d-m-Y',$request->tanggal)->format('d-m-Y');
            // $request['tanggal'] = $request->tanggal;
    		$record = new RapatInternal;
	        $record->fill($request->all());
            $record->save();
            $record->saveDetailInternal($request->detail);
            $record->saveAgendaInternal($request->data);
            // sync Project
            // $record->detailProject()->sync($request->project_id);

            if ($request->flag == 1) {
                $record = RapatInternal::find($record->id);
                    foreach ($record->undanganInternal as $data)
                        $user = User::find($data->user_id);
                        // dd($user['email']);
                        {
                            Mail::to($user['email'])->queue(new RapatEmail($record, $user, 'Konfirmasi Kehadiran Peserta Undangan'));
                            $data->status = 1;
                            $data->save();
                        }
            }

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function edit($id)
    {
        $this->setBreadcrumb(['Rapat' => '#', 'Internal' => '#', 'Ubah' => '#']);
        $record = RapatInternal::find($id);
        return $this->render('modules.rapat.internal.edit', ['record' => $record]);
    }

    public function show($id)
    {
        $this->setBreadcrumb(['Rapat' => '#', 'Internal' => '#', 'Detil' => '#']);
        $record = RapatInternal::find($id);
        return $this->render('modules.rapat.internal.detail', ['record' => $record]);
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());
        DB::beginTransaction();
        try {
            $request['tanggal'] = Carbon::createFromFormat('d-m-Y',$request->tanggal)->format('d-m-Y');
            // $request['tanggal'] = $request->tanggal;
            if(!empty($request->exists)){
            $hapus = UndanganPesertaInternal::whereNotIn('id', $request->exists)
                    ->where('rapat_id', $request->id)
                    ->delete();
            }

        	$record = RapatInternal::find($request->id);
	        $record->fill($request->all());
            $record->save();
            $record->updateDetailInternal($request->detail);
            $record->updateAgendaInternal($request->data);
            // sync Project
            // $record->detailProject()->sync($request->project_id);

            if ($request->flag == 1) {
                $record = RapatInternal::find($request->id);
                    foreach ($record->undanganInternal as $data)
                        $user = User::find($data->user_id);
                        // dd($user['email']);
                        {
                            Mail::to($user['email'])->queue(new RapatEmail($record, $user, 'Konfirmasi Kehadiran Peserta Undangan'));
                            // Mail::send('partials.emails.mail', ['record' => $record, 'user' => $user],
                            //     function ($message) use ($user) {
                            //         $message->subject('Konfirmasi Kehadiran Peserta Undangan');
                            //         $message->to($user['email']);
                            //     });
                            $data->status = 1;
                            $data->save();
                        }
            }

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function postUpload(Request $request, $id)
    {
        // dd(empty($request->exists));
        DB::beginTransaction();
        try {
            // if(!empty($request->exists)){
            //     $hapus = Risalah::whereNotIn('id', $request->exists)
            //                       ->delete();
            // }
            // $hapus = Risalah::whereNotIn('id', $request->exists)
            //                                       ->delete();
            $record = RapatInternal::find($request->id);
            // $record->status_rapat = 1;
            $record->fill($request->all());
            $record->save();
            $record->updateRisalah($request->detail);
            if(isset($request->daftar_hadir[0])){
                $record->saveHadirFile($request->daftar_hadir);
            }
            if(isset($request->other[0])){
                $record->saveOtherFile($request->other);
            }

            //Sent Mail Risalah
            if ($request->status_rapat == 1) {
                $rapat = RapatInternal::find($id);
                foreach ($rapat->undanganInternal as $data)
                        $user = User::find($data->user_id);
        // dd($user['email']);
                        {
                            RisalahPdfEmail::dispatch([
                                    'id'=> $rapat->id,
                                    'to'=> $user['email'],
                                ]);
                        }
            }

            DB::commit();
            return response([
              'status' => true
            ]);
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy($id)
    {
        $data = RapatInternal::find($id);
        $data->unlinkFiles();
        $data->files()->delete();
        $data->daftarHadir()->delete();
        $data->risalah()->delete();
        $data->delete();
        return response()->json([
            'success' => true
        ]);
    }

    public function showCode($id)
    {
        $record = RapatInternal::find($id);
        // dd($record);
        $l = route('in.showing',['id' => md5($id), 'id_real' => base64_encode($id), 'date' => base64_encode(Carbon::now()) ]);
        // dd($l);
        return $this->render('modules.rapat.internal.show-code', [
            'record' => $record,
            'link' => $l,
            'mark' => asset('src/img/logo-mini.png'),
            ]);
    }

    public function reUpload($id)
    {
        $this->setBreadcrumb(['Rapat' => '#', 'Internal' => '#', 'Lengkapi' => '#']);
        $record = RapatInternal::find($id);
        return $this->render('modules.rapat.internal.re-upload', ['record' => $record]);
    }

    public function download($id)
    {
        $zip = new ZipArchive;
        $record = RapatInternal::find($id);
        $title = 'INTERNAL';
        $fileName = '/app/public/'.str_replace('/','',$record->nomor).'.zip';
        if(file_exists(storage_path().'/app/public/'.str_replace('/','',$record->nomor).'.zip'))
        {
            unlink(storage_path().'/app/public/'.str_replace('/','',$record->nomor).'.zip');
        }
        if(file_exists(storage_path().'/app/public/Daftar Hadir.pdf'))
        {
            unlink(storage_path().'/app/public/Daftar Hadir.pdf');
        }
        if(file_exists(storage_path().'/app/public/Risalah.pdf'))
        {
            unlink(storage_path().'/app/public/Risalah.pdf');
        }
        if ($zip->open(storage_path().$fileName, ZipArchive::CREATE) === TRUE)
        {
            if(isset($record->files) AND $record->files->count() > 0){
                foreach ($record->files as $key => $value) {
                    if(file_exists(storage_path().'/app/public/'.$value->url))
                    {
                        if($value->flag == 'hadir'){
                            $key = $key+1;
                            $files = storage_path().'/app/public/'.$value->url;
                            $zip->addFile($files,'Daftar Hadir/'.$value->filename);
                        }else{
                            $key = $key+1;
                            $files = storage_path().'/app/public/'.$value->url;
                            $zip->addFile($files,'Lampiran/'.$value->filename);
                        }
                    }
                }
            }
        }
        if(isset($record->risalah) AND $record->risalah->count() > 0){
            $pdf = PDF::loadView('modules.rapat.internal.risalah-pdf', [
                'record' => $record,
                'today' => CoreSn::DateToString(Carbon::now()),
                'user' => auth()->user(),
                'title' => $title,
            ])->setPaper('a4', 'landscape')->setOptions(
                [
                    'defaultFont' => 'times-roman',
                    'isHtml5ParserEnabled' => true,
                    'isRemoteEnabled' => true,
                    'isPhpEnabled' => true
                ]
            );
            $uri = storage_path().'/app/public/Risalah.pdf';
            file_put_contents($uri, $pdf->output());
            $zip->addFile($uri,'Dokumen/Risalah.pdf');
        }

        if(isset($record->daftarHadir) AND $record->daftarHadir->count() > 0){
            $blnthn = explode(" ", CoreSn::DateToStringWDay($record->tanggal));
            $blnthn = $blnthn[2].' '.$blnthn[3];
            $pdf = PDF::loadView('modules.rapat.internal.hadir-pdf', [
                'record' => $record,
                'today' => strtoupper(CoreSn::DateToStringWDay($record->tanggal)),
                'blnthn' => strtoupper($blnthn),
                'user' => auth()->user(),
                'title' => $title,
            ])->setPaper('a4', 'potrait')->setOptions(
                [
                    'defaultFont' => 'times-roman',
                    'isHtml5ParserEnabled' => true,
                    'isRemoteEnabled' => true,
                    'isPhpEnabled' => true
                ]
            );
            $uri = storage_path().'/app/public/Daftar Hadir.pdf';
            file_put_contents($uri, $pdf->output());
            $zip->addFile($uri,'Dokumen/Daftar Hadir.pdf');
        }


        $zip->close();

        if(file_exists(storage_path().'/app/public/'.str_replace('/','',$record->nomor).'.zip'))
        {
            return response()->download(storage_path().'/app/public/'.str_replace('/','',$record->nomor).'.zip');
        }
        return abort(404);
    }

    public function hadir($id)
    {
        $record = UndanganPesertaInternal::find($id);
        $record->status = 2;
        $record->save();
    }

    public function tidakHadir($id)
    {
        $record = UndanganPesertaInternal::find($id);
        $record->status = 3;
        $record->save();
    }

    public function cetak(Request $request, $id){
        $record = RapatInternal::find($id);
        $data = [
            'records' => $record,
            'today' => CoreSn::DateToString(Carbon::now()),
        ];
        $pdf = PDF::loadView('modules.rapat.internal.cetak', $data)->setPaper('a4', 'landscape');
        return $pdf->stream('Rapat Internal - '.DateToStringWDay($record->tanggal).'.pdf',array("Attachment" => false));
    }
}
