<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\FokusAuditRequest;
use App\Models\Master\FokusAudit;
use App\Models\SessionLog;

use DB;

class FokusAuditController extends Controller
{
    protected $routes = 'master.fokus-audit';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'Fokus Audit' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'bidang',
                'name' => 'bidang',
                'label' => 'Bidang',
                'className' => 'text-left',
                'width' => '200px',
                'sortable' => true,
            ],
            [
                'data' => 'audit',
                'name' => 'audit',
                'label' => 'Fokus Audit',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Diperbarui Pada',
                'className' => 'text-center',
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = FokusAudit::select('*');

        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($audit = request()->audit) {
            $records->where('audit', 'like', '%' . $audit . '%');
        }
        if ($bidang = request()->bidang) {
            $records->where('bidang', 'like', '%' . $bidang . '%');
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('bidang', function ($record) {
               		if($record->bidang == 1){
               			return 'Operasional';
               		}elseif($record->bidang == 2){
               			return 'Keuangan';
               		}else{
               			return 'Sistem';
               		}
               })
               ->addColumn('audit', function ($record) {
                   return $record->readMoreText('audit', 100);
               })
               ->editColumn('created_by', function ($record) {
                   return $record->creatorName();
               })
               ->editColumn('created_at', function ($record) {
                    if ($record->updated_at) {
	                    return $record->updated_at->diffForHumans();
	                }else{
	                    return $record->created_at->diffForHumans();
	                }
               })
               ->addColumn('action', function ($record) {
                    $buttons = '';
                    if($record->langkahkerja->count() > 0){
			    		if($record->program->count() > 0){
			    			 $buttons = '';
			    		}else{
			    			$buttons .= $this->makeButton([
		                        'type' => 'edit',
		                        'id'   => $record->id,
		                    ]);
		                    $buttons .= $this->makeButton([
		                        'type' => 'delete',
		                        'id'   => $record->id,
		                    ]);
			    		}
			    	}else{
				    	$buttons .= $this->makeButton([
	                        'type' => 'edit',
	                        'id'   => $record->id,
	                    ]);
	                    $buttons .= $this->makeButton([
	                        'type' => 'delete',
	                        'id'   => $record->id,
	                    ]);
			    	}
                    return $buttons;
               })
               ->rawColumns(['audit', 'action'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.fokus-audit.index');
    }

    public function create()
    {
        return $this->render('modules.master.fokus-audit.create');
    }

    public function store(FokusAuditRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record = new FokusAudit;
	        $record->fill($request->all());
	        $record->save();
	        if($request->bidang == 1){
		        $bidang = 'Operasional';
	        }elseif($request->bidang == 2){
		        $bidang = 'Keuangan';
	        }else{
		        $bidang = 'Sistem';
	        }

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Fokus Audit';
		    $log->aktivitas = 'Membuat Fokus Audit dengan Bidang '.$bidang.' dengan nama Audit '.$request->audit;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    // public function show(FokusAudit $user)
    // {
    //     return $user->toJson();
    // }

    public function edit(FokusAudit $FokusAudit)
    {
        return $this->render('modules.master.fokus-audit.edit', ['record' => $FokusAudit]);
    }

    public function update(FokusAuditRequest $request, FokusAudit $FokusAudit)
    {
    	DB::beginTransaction();
        try {
        	$record = FokusAudit::find($request->id);
	        $record->fill($request->all());
	        $record->save();

	        if($record->bidang == 1){
		        $bidang = 'Operasional';
	        }elseif($record->bidang == 2){
		        $bidang = 'Keuangan';
	        }else{
		        $bidang = 'Sistem';
	        }
	    	$user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Fokus Audit';
		    $log->aktivitas = 'Mengubah Fokus Audit dengan Bidang '.$bidang.' dengan nama Audit '.$record->audit;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(FokusAudit $FokusAudit)
    {
    	if($FokusAudit->langkahkerja->count() > 0){
    		if($FokusAudit->program->count() > 0){
    			return response([
	                'status' => true,
	            ],500);
    		}else{
    			if($FokusAudit->bidang == 1){
			        $bidang = 'Operasional';
		        }elseif($FokusAudit->bidang == 2){
			        $bidang = 'Keuangan';
		        }else{
			        $bidang = 'Sistem';
		        }
		    	$user = auth()->user();
		    	$log = new SessionLog;
			    $log->modul = 'Master';
			    $log->sub = 'Fokus Audit';
			    $log->aktivitas = 'Menghapus Fokus Audit dengan Bidang '.$bidang.' dengan nama Audit '.$FokusAudit->audit;
			    $log->user_id = $user->id;
			    $log->save();

		        $FokusAudit->delete();
		        return response()->json([
		            'success' => true
		        ]);
    		}
    	}else{
	    	if($FokusAudit->bidang == 1){
		        $bidang = 'Operasional';
	        }elseif($FokusAudit->bidang == 2){
		        $bidang = 'Keuangan';
	        }else{
		        $bidang = 'Sistem';
	        }
	    	$user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Fokus Audit';
		    $log->aktivitas = 'Menghapus Fokus Audit dengan Bidang '.$bidang.' dengan nama Audit '.$FokusAudit->audit;
		    $log->user_id = $user->id;
		    $log->save();

	        $FokusAudit->delete();
	        return response()->json([
	            'success' => true
	        ]);
    	}
    }
}
