<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\DokumenRequest;
use App\Models\Master\Dokumen;
use App\Models\SessionLog;
use App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumenDetail;

use DB;

class DokumenController extends Controller
{
    protected $routes = 'master.dokumen';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'Dokumen Pendahuluan' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'judul',
                'name' => 'judul',
                'label' => 'Judul',
                'sortable' => true,
                'width' => '200px',
            ],
            [
                'data' => 'deskripsi',
                'name' => 'deskripsi',
                'label' => 'Deskripsi',
                'sortable' => true,
                'width' => '400px',
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Diperbarui Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = Dokumen::select('*');

        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($judul = request()->judul) {
            $records->where('judul', 'like', '%' . $judul . '%');
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('judul', function ($record) {
                   return $record->judul;
               })
               ->addColumn('deskripsi', function ($record) {
                   return $record->readMoreText('deskripsi', 100);
               })
               ->editColumn('created_at', function ($record) {
                    if ($record->updated_at) {
	                    return $record->updated_at->diffForHumans();
	                }else{
	                    return $record->created_at->diffForHumans();
	                }
               })
               ->addColumn('action', function ($record) {
                    $buttons = '';
                    $cari = TinjauanDokumenDetail::where('dokumen_id', $record->id)->get()->count();
                    if($cari > 0){
			    		$buttons = '';
			    	}else{
				    	$buttons .= $this->makeButton([
	                        'type' => 'edit',
	                        'id'   => $record->id,
	                   ]);
	                   $buttons .= $this->makeButton([
	                        'type' => 'delete',
	                        'id'   => $record->id,
	                    ]);
			    	}
                   return $buttons;
               })
               ->rawColumns(['deskripsi', 'action'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.dokumen.index');
    }

    public function create()
    {
        return $this->render('modules.master.dokumen.create');
    }

    public function store(DokumenRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record = new Dokumen;
	        $record->fill($request->all());
	        $record->save();

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Dokumen Pendahuluan';
		    $log->aktivitas = 'Membuat Dokumen Pendahuluan dengan Judul '.$request['judul'];
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    // public function show(d $user)
    // {
    //     return $user->toJson();
    // }

    public function edit(Dokumen $Dokuman)
    {
        return $this->render('modules.master.dokumen.edit', ['record' => $Dokuman]);
    }

    public function update(DokumenRequest $request, Dokumen $Dokuman)
    {
    	DB::beginTransaction();
        try {
        	$record = Dokumen::find($request->id);
        	$name = $record->judul;
	        $record->fill($request->all());
	        $record->save();

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Dokumen Pendahuluan';
		    $log->aktivitas = 'Mengubah Dokumen Pendahuluan dengan Judul '.$name.'</i></b> menjadi '.$request['judul'];
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(Dokumen $Dokuman)
    {
    	if($Dokuman->detiltinjauan->count() > 0){
    		return response([
                'status' => true,
            ],500);
    	}else{
	    	$user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Dokumen Pendahuluan';
		    $log->aktivitas = 'Menghapus Dokumen Pendahuluan dengan Judul '.$Dokuman->judul;
		    $log->user_id = $user->id;
		    $log->save();

	        $Dokuman->delete();
	        return response()->json([
	            'success' => true
	        ]);
    	}
    }
}
