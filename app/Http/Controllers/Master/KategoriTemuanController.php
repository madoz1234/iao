<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\KategoriTemuanRequest;
use App\Models\Master\KategoriTemuan;
use App\Models\SessionLog;

use DB;

class KategoriTemuanController extends Controller
{
    protected $routes = 'master.kategori-temuan';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'Kategori Temuan' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Kategori Temuan',
                'className' => 'text-left',
                'width' => '150px',
                'sortable' => true,
            ],
            [
                'data' => 'deskripsi',
                'name' => 'deskripsi',
                'label' => 'Deskripsi',
                'width' => '200px',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Diperbarui Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = KategoriTemuan::select('*');
        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($name = request()->name) {
            $records->where('nama', 'like', '%' . $name . '%');
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('nama', function ($record) {
               	    return readMoreText($record->nama, 100);
               })
               ->addColumn('deskripsi', function ($record) {
                   return readMoreText($record->deskripsi, 100);
               })
               ->editColumn('created_at', function ($record) {
                    if ($record->updated_at) {
	                    return $record->updated_at->diffForHumans();
	                }else{
	                    return $record->created_at->diffForHumans();
	                }
               })
               ->addColumn('action', function ($record) {
                   $buttons = '';
                    if($record->kkadetail->count() > 0){
						 $buttons = '';
			    	}else{
				    	$buttons .= $this->makeButton([
	                        'type' => 'edit',
	                        'id'   => $record->id,
	                    ]);
	                    $buttons .= $this->makeButton([
	                        'type' => 'delete',
	                        'id'   => $record->id,
	                    ]);
			    	}
                   return $buttons;
               })
               ->rawColumns(['action','status','deskripsi'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.kategori-temuan.index');
    }

    public function create()
    {
        return $this->render('modules.master.kategori-temuan.create');
    }

    public function store(KategoriTemuanRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record = new KategoriTemuan;
	        $record->nama = $request->nama;
	        $record->deskripsi = $request->deskripsi;
	        $record->save();

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Kategori Temuan';
		    $log->aktivitas = 'Membuat Kategori Temuan dengan keterangan '.$request->deskripsi;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    // public function show(KategoriTemuan $user)
    // {
    //     return $user->toJson();
    // }

    public function edit(KategoriTemuan $KategoriTemuan)
    {
        return $this->render('modules.master.kategori-temuan.edit', ['record' => $KategoriTemuan]);
    }

    public function update(KategoriTemuanRequest $request, KategoriTemuan $KategoriTemuan)
    {
    	DB::beginTransaction();
        try {
        	$record = KategoriTemuan::find($request->id);
	        $record->nama = $request->nama;
	        $record->deskripsi = $request->deskripsi;
	        $record->save();

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Kategori Temuan';
		    $log->aktivitas = 'Mengubah Kategori Temuan dengan keterangan '.$record->deskripsi;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(KategoriTemuan $KategoriTemuan)
    {
		if($KategoriTemuan->kkadetail->count() > 0){
			return response([
            	'status' => true,
        	],500);
    	}else{
	    	$user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Kategori Temuan';
		    $log->aktivitas = 'Menghapus Kategori Temuan dengan keterangan '.$KategoriTemuan->kategori;
		    $log->user_id = $user->id;
		    $log->save();
	        $KategoriTemuan->delete();
	        return response()->json([
	            'success' => true
	        ]);
    	}
    }
}
