<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\CORequest;
use App\Models\Master\CO;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\SessionLog;

use DB;

class COController extends Controller
{
    protected $routes = 'master.co';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'Corporate Office (CO)' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'kode',
                'name' => 'kode',
                'label' => 'Kode CO',
                'width' => '200px',
                'sortable' => true,
            ],
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Corporate Office',
                'width' => '250px',
                'sortable' => true,
            ],
            [
                'data' => 'alamat',
                'name' => 'alamat',
                'label' => 'Alamat',
                'sortable' => true,
                'width' => '200px',
            ],
            [
                'data' => 'no_tlp',
                'name' => 'no_tlp',
                'label' => 'No Telepon',
                'width' => '150px',
                'sortable' => true,
                'className' => 'text-left',
            ],
            [
                'data' => 'pic',
                'name' => 'pic',
                'label' => 'PIC',
                'sortable' => true,
                'className' => 'text-left',
                'width' => '200px',
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Diperbarui Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = CO::select('*');

        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($kode = request()->kode) {
            $records->where('kode', 'like', '%' . $kode . '%');
        }
        if ($name = request()->name) {
            $records->where('nama', 'like', '%' . $name . '%');
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('kode', function ($record) {
                   if($record->kode){
	           			return $record->kode;
	           		}else{
	                   return '';
	           		}
               })
               ->addColumn('nama', function ($record) {
                   return $record->nama;
               })
               ->addColumn('alamat', function ($record) {
                   return $record->readMoreText('alamat', 100);
               })
               ->addColumn('no_tlp', function ($record) {
                   if($record->no_tlp){
	           			return $record->no_tlp;
	           		}else{
	                   return '';
	           		}
               })
               ->editColumn('created_at', function ($record) {
                    if ($record->updated_at) {
	                    return $record->updated_at->diffForHumans();
	                }else{
	                    return $record->created_at->diffForHumans();
	                }
               })
               ->addColumn('pic', function ($record) {
                   	$pic ='';
	           		$pic .='<ul class="list list-more1 list-unstyled" style="margin-top:10px;">';
	           		foreach ($record->detail_pic as $key => $value) {
	           				$pic .='<li class="list-group-item" style="padding:0px;border:none;">'.$value->user->name.'</li>';
	           		}
	           		$pic .='</ul>';
	               	return $pic;
               })
               ->addColumn('action', function ($record) {
                    $buttons = '';
                    $cari = RencanaAuditDetail::where('object_id', $record->id)->where('tipe_object', 1)->get()->count();
		    		if($cari > 0){
			        	$buttons = '';
		        	}else{
		        		$buttons .= $this->makeButton([
	                        'type' => 'edit',
	                        'id'   => $record->id,
	                    ]);
	                    $buttons .= $this->makeButton([
	                        'type' => 'delete',
	                        'id'   => $record->id,
	                    ]);
		        	}
                   return $buttons;
               })
               ->rawColumns(['alamat', 'action','pic'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.co.index');
    }

    public function create()
    {
        return $this->render('modules.master.co.create');
    }

    public function store(CORequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record = new CO;
	        $record->fill($request->all());
	        $record->save();
	        $record->saveDetail($request->pic);

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Corporate Office (CO)';
		    $log->aktivitas = 'Membuat Corporate Office (CO) dengan Kode CO '.$request['kode'];
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    // public function show(CO $user)
    // {
    //     return $user->toJson();
    // }

    public function edit(CO $co)
    {
        return $this->render('modules.master.co.edit', ['record' => $co]);
    }

    public function update(CORequest $request, CO $co)
    {
    	DB::beginTransaction();
        try {
        	$record = CO::find($request->id);
        	$name = $record->kode;
	        $record->fill($request->all());
	        $record->save();
	        $record->updateDetail($request->pic, $request->id);

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Corporate Office (CO)';
		    $log->aktivitas = 'Mengubah Corporate Office (CO) dengan Kode CO '.$name.'</i></b> menjadi '.$request['kode'];
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(CO $co)
    {
        if($co){
    		$cari = RencanaAuditDetail::where('object_id', $co->id)->where('tipe_object', 1)->get()->count();
    		if($cari > 0){
	        	return response([
                	'status' => true,
            	],500);
        	}else{
        		$user = auth()->user();
		    	$log = new SessionLog;
			    $log->modul = 'Master';
			    $log->sub = 'Corporate Office (CO)';
			    $log->aktivitas = 'Menghapus Corporate Office (CO) dengan Kode CO '.$co->kode;
			    $log->user_id = $user->id;
			    $log->save();

        		$co->delete();
	        	return response([
	            	'status' => true,
		        ],200);
        	}
    	}else{
    		return response([
                'status' => true,
            ],500);
    	}
    }
}
