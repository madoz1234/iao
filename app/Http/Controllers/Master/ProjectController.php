<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\ProjectRequest;
use App\Models\Master\Project;
use App\Models\Master\BU;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\SessionLog;
use App\Imports\ProjectImport;
use Maatwebsite\Excel\Facades\Excel;

use DB;

class ProjectController extends Controller
{
    protected $routes = 'master.project';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'Project' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'project_ab',
                'name' => 'project_ab',
                'label' => 'No AB',
                'width' => '250px',
                'sortable' => true,
            ],
            [
                'data' => 'project_id',
                'name' => 'project_id',
                'label' => 'Project ID',
                'width' => '250px',
                'sortable' => true,
            ],
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Project',
                'width' => '250px',
                'sortable' => true,
            ],
            [
                'data' => 'bu',
                'name' => 'bu',
                'label' => 'Business Unit',
                'width' => '200px',
                'sortable' => true,
            ],
            [
                'data' => 'alamat',
                'name' => 'alamat',
                'label' => 'Alamat',
                'sortable' => true,
                'width' => '200px',
            ],
            [
                'data' => 'no_tlp',
                'name' => 'no_tlp',
                'label' => 'No Telepon',
                'width' => '150px',
                'sortable' => true,
                'className' => 'text-left',
            ],
            [
                'data' => 'pic',
                'name' => 'pic',
                'label' => 'PIC',
                'sortable' => true,
                'className' => 'text-left',
                'width' => '200px',
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Diperbarui Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = Project::select('*');

       	if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($project_ab = request()->project_ab) {
            $records->where('project_ab', 'like', '%' . $project_ab . '%');
        }
        if ($project_id = request()->project_id) {
            $records->where('project_id', 'like', '%' . $project_id . '%');
        }
        if ($project = request()->project) {
            $records->where('nama', 'like', '%' . $project . '%');
        }
        if ($bu = request()->bu) {
            $records->where('bu_id', 'like', '%' . $bu . '%');
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('project_ab', function ($record) {
	           		if($record->project_ab){
	           			return $record->project_ab;
	           		}else{
	                   return '';
	           		}
               })
               ->addColumn('project_id', function ($record) {
	           		if($record->project_id){
	           			return $record->project_id;
	           		}else{
	                   return '';
	           		}
               })
               ->addColumn('bu', function ($record) {
                   return $record->bu->nama;
               })
               ->addColumn('nama', function ($record) {
                   return $record->nama;
               })
               ->addColumn('alamat', function ($record) {
                   return $record->readMoreText('alamat', 100);
               })
               ->addColumn('no_tlp', function ($record) {
               		return $record->no_tlp;
               })
               ->addColumn('pic', function ($record) {
                    $pic ='';
	           		$pic .='<ul class="list list-more1 list-unstyled" style="margin-top:10px;">';
	           		foreach ($record->detail_pic as $key => $value) {
	           				$pic .='<li class="item">'.$value->user->name.'</li>';
	           		}
	           		$pic .='</ul>';
	               	return $pic;
               })
               ->editColumn('created_at', function ($record) {
                    if ($record->updated_at) {
	                    return $record->updated_at->diffForHumans();
	                }else{
	                    return $record->created_at->diffForHumans();
	                }
               })
               ->addColumn('action', function ($record) {
                    $buttons = '';
		    		$cari = RencanaAuditDetail::where('object_id', $record->id)->where('tipe_object', 2)->get()->count();
		    		if($cari > 0){
		    			$buttons = '';
		    		}else{
	                    $buttons .= $this->makeButton([
	                        'type' => 'edit',
	                        'id'   => $record->id,
	                    ]);
		                $buttons .= $this->makeButton([
		                    'type' => 'delete',
		                    'id'   => $record->id,
		                ]);
		    		}
                    return $buttons;
               })
               ->rawColumns(['alamat', 'action','pic'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.project.index');
    }

    public function create()
    {
        return $this->render('modules.master.project.create');
    }

    public function store(ProjectRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record = new Project;
	        $record->fill($request->all());
	        $record->save();
	        $record->saveDetail($request->pic);

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Project';
		    $log->aktivitas = 'Membuat Project dengan No AB '.$request['project_ab'];
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    // public function show(Project $user)
    // {
    //     return $user->toJson();
    // }

    public function edit(Project $project)
    {
        return $this->render('modules.master.project.edit', ['record' => $project]);
    }

    public function update(ProjectRequest $request, Project $project)
    {
    	DB::beginTransaction();
        try {
        	$record = Project::find($request->id);
        	$name = $record->project_ab;
	        $record->fill($request->all());
	        $record->save();
	        $record->updateDetail($request->pic, $request->id);

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Project';
		    $log->aktivitas = 'Mengubah Project dengan No AB '.$name.' menjadi '.$request['project_ab'];
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function import()
    {
        return $this->render('modules.master.project.import');
    }

    public function importing(Request $request)
    {
      try {
          $excel = Excel::toArray(new ProjectImport, $request->final);
          foreach($excel[0] as $key => $ex)
          {
              if($key != 0)
              {
                  $check = Project::where('project_id', $ex[0])->first();
                  if(!$check)
                  {
                      $check = new Project;
                  }
                  $check->project_id = $ex[0];
                  $check->nama = $ex[1];
                  $check->project_ab = $ex[3];

                  switch($ex[6])
                  {
                      case 'INFRA 1' : $bu = BU::where('nama', 'Infra I Division')->first();
                      break;
                      case 'INFRA 2' : $bu = BU::where('nama', 'Infra II Division')->first();
                      break;
                      case 'INFRA 3' : $bu = BU::where('nama', 'Infra III Division')->first();
                      break;
                      case 'BUILDING' : $bu = BU::where('nama', 'Building Division')->first();
                      break;
                      case 'EPC' : $bu = BU::where('nama', 'EPC Division')->first();
                      break;
                  }

                  if($bu)
                  {
                      $check->bu_id = $bu->id;
                      $check->save();
                  }
              }
          }

      } catch (\Exception $exception) {
          return response()->json([
            'status' => false,
            'message' => $exception->getMessage(),
          ], 500);
      }
      return response()->json([
        'status' => true,
      ]);
    }

    public function destroy(Project $project)
    {
        if($project){
    		$cari = RencanaAuditDetail::where('object_id', $project->id)->where('tipe_object', 2)->get()->count();
    		if($cari > 0){
	        	return response([
                	'status' => true,
            	],500);
        	}else{
        		$user = auth()->user();
		    	$log = new SessionLog;
			    $log->modul = 'Master';
			    $log->sub = 'Project';
			    $log->aktivitas = 'Menghapus Project dengan No AB '.$project->project_ab;
			    $log->user_id = $user->id;
			    $log->save();

        		$project->delete();
	        	return response([
	            	'status' => true,
		        ],200);
        	}
    	}else{
    		return response([
                'status' => true,
            ],500);
    	}
    }
}
