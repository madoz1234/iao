<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\SpinPofRequest;
use App\Models\Master\SpinPof;
use App\Models\SessionLog;

use DB;

class SpinPofController extends Controller
{
    protected $routes = 'master.spin-pof';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'SPIN' => '#', 'Point of Focus (PoF)' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'judul',
                'name' => 'judul',
                'label' => 'Point of Focus (PoF)',
                'sortable' => true,
                'width' => '800px',
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Diperbarui Pada',
                'className' => 'text-center',
                'width' => '100px',
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $user = auth()->user();
        $records = SpinPof::select('*');

        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($judul = request()->judul) {
            $records->where('judul', 'like', '%' . $judul . '%');
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('judul', function ($record) {
                   return readMoreText($record->judul);
               })
               // ->addColumn('deskripsi', function ($record) {
               //     return $record->readMoreText('deskripsi', 100);
               // })
               ->editColumn('created_at', function ($record) {
                    if ($record->updated_at) {
	                    return $record->updated_at->diffForHumans();
	                }else{
	                    return $record->created_at->diffForHumans();
	                }
               })
               ->addColumn('action', function ($record) use ($user) {
                    $buttons = '';
                    if($record->pemenuhan->count() > 0){
			    		 $buttons = '';
			    	}else{
				    	$buttons .= $this->makeButton([
	                        'type' => 'edit',
	                        'id'   => $record->id,
	                    ]);
	                    $buttons .= $this->makeButton([
	                        'type' => 'delete',
	                        'id'   => $record->id,
	                    ]);
			    	}
                   return $buttons;
               })
               ->rawColumns(['judul', 'action'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.pof.index');
    }

    public function create()
    {
        return $this->render('modules.master.pof.create');
    }

    public function store(SpinPofRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record = new SpinPof;
	        $record->fill($request->all());
	        $record->save();

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'SPIN PoF';
		    $log->aktivitas = 'Membuat SPIN PoF dengan Point of Focus (PoF) '.$request->judul;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    // public function show(d $user)
    // {
    //     return $user->toJson();
    // }

    public function edit(SpinPof $SpinPof)
    {
        return $this->render('modules.master.pof.edit', ['record' => $SpinPof]);
    }

    public function update(SpinPofRequest $request, SpinPof $SpinPof)
    {
    	DB::beginTransaction();
        try {
        	$record = SpinPof::find($request->id);
	        $record->fill($request->all());
	        $record->save();

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'SPIN PoF';
		    $log->aktivitas = 'Mengubah SPIN PoF dengan Point of Focus (PoF) '.$record->judul;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(SpinPof $SpinPof)
    {

    	if($SpinPof->pemenuhan->count() > 0){
    		return response([
                'status' => true,
            ],500);
    	}else{
	    	$user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'SPIN PoF';
		    $log->aktivitas = 'Menghapus SPIN PoF dengan Point of Focus (PoF) '.$SpinPof->judul;
		    $log->user_id = $user->id;
		    $log->save();

	        $SpinPof->delete();
	        return response()->json([
	            'success' => true
	        ]);
    	}
    }
}
