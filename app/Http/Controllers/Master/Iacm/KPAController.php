<?php

namespace App\Http\Controllers\Master\Iacm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\KpaRequest;
use App\Models\Master\Kpa;
use App\Models\Master\KpaLevel;
use App\Models\Master\KpaDetil;
use App\Models\SessionLog;

use DB;

class KPAController extends Controller
{
    protected $routes = 'master.iacm.kpa';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'IACM' => '#', 'Key Process Area' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'elemen',
                'name' => 'elemen',
                'label' => 'Elemen',
                'width' => '400px',
                'sortable' => true,
            ],
            [
                'data' => 'kpa',
                'name' => 'kpa',
                'label' => 'Key Process Area',
                'width' => '400px',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => 'text-center',
                'width' => '200px',
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = Kpa::select('*');
        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if($elemen = request()->elemen) {
        	$records->whereHas('elemen', function($u) use ($elemen){
        		$u->where('elemen', 'like', '%' . $elemen . '%');
        	});
        }
        if($kpa = request()->kpa) {
        	$records->whereHas('level', function($u) use ($kpa){
        		$u->whereHas('detail', function($z) use ($kpa){
        			$z->where('kpa', 'like', '%' . $kpa . '%');
        		});
        	});
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('elemen', function ($record) {
                   return readMoreText($record->elemen->elemen, 100);
               })
               ->addColumn('kpa', function ($record) {
               		$pic ='';
	           		$pic .='<ul class="list-group list-more1" data-display="3">';
	           		foreach ($record->level as $key => $value) {
	           				foreach ($value->detail as $key => $val) {
		           				$pic .='<li class="item" style="padding:0px;border:none;">'.readMoreText($val->kpa, 100).'</li>';
	           				}
	           		}
	           		$pic .='</ul>';
	               	return $pic;
               })
               ->editColumn('created_at', function ($record) {
                    if ($record->updated_at) {
	                    return $record->updated_at->diffForHumans();
	                }else{
	                    return $record->created_at->diffForHumans();
	                }
               })
               ->addColumn('action', function ($record) {
                   $buttons = '';

                   $buttons .= $this->makeButton([
                        'type' => 'edit',
                        'id'   => $record->first()->id,
                   ]);
                   $buttons .= $this->makeButton([
                        'type' => 'delete',
                        'id'   => $record->first()->id,
                    ]);

                   return $buttons;
               })
               ->rawColumns(['elemen', 'action','kpa'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.kpa.index');
    }

    public function create()
    {
    	$this->setBreadcrumb(['Master' => '#', 'IACM' => '#', 'Key Process Area' => '#', 'Tambah' => '#']);
        return $this->render('modules.master.kpa.create');
    }

    public function store(KpaRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record = new Kpa;
	        $record->elemen_id 			= $request->elemen_id;
	        $record->save();
	        $record->saveDetail($request->data);

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'IACM Key Process Area';
		    $log->aktivitas = 'Membuat IACM Key Process Area dengan Elemen '.$record->elemen->elemen;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }
    
    public function edit($id)
    {
    	$record = Kpa::find($id);
    	$data = Kpa::where('elemen_id', $record->elemen_id)->get();
        return $this->render('modules.master.kpa.edit', [
        	'record' => $record,
        	'data' => $data,
        ]);
    }

    public function update(KpaRequest $request, Kpa $Kpa)
    {
    	DB::beginTransaction();
        try {
        	$hapus = KpaLevel::whereNotIn('id', $request->exist_data)
						  		->delete();
		    $hapus_detil = KpaDetil::whereIn('level_id', $request->exist_level)
						  			  ->delete();

			$cari = Kpa::find($request->id);
	        $cari->elemen_id 			= $request->elemen_id;
	        $cari->save();
	        $cari->updateDetail($request->data);

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'IACM Key Process Area';
		    $log->aktivitas = 'Mengubah IACM Key Process Area dengan Elemen '.$cari->elemen->elemen;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(Kpa $Kpa)
    {
    	if($Kpa->uraian->count() > 0){
    		return response([
                'status' => true,
            ],500);
    	}else{
	    	$user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'IACM Key Process Area';
		    $log->aktivitas = 'Mengubah IACM Key Process Area dengan Elemen '.$Kpa->elemen->elemen;
		    $log->user_id = $user->id;
		    $log->save();
			    
	        $Kpa->delete();
	        return response()->json([
	            'success' => true
	        ]);
    	}
    }
}
