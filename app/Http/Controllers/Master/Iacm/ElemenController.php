<?php

namespace App\Http\Controllers\Master\Iacm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\ElemenRequest;
use App\Models\Master\Elemen;
use App\Models\SessionLog;

use DB;

class ElemenController extends Controller
{
    protected $routes = 'master.iacm.elemen';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'IACM' => '#', 'Elemen' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'elemen',
                'name' => 'elemen',
                'label' => 'Elemen',
                'width' => '400px',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => 'text-center',
                'width' => '200px',
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = Elemen::select('*');

		if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($elemen = request()->elemen) {
            $records->where('elemen', 'like', '%' . $elemen . '%');
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('elemen', function ($record) {
                   return readMoreText($record->elemen, 120);
               })
               ->editColumn('created_at', function ($record) {
                    if ($record->updated_at) {
	                    return $record->updated_at->diffForHumans();
	                }else{
	                    return $record->created_at->diffForHumans();
	                }
               })
               ->addColumn('action', function ($record) {
                    $buttons = '';
                    if($record->kpa->count() > 0){
			    		$buttons = '';
			    	}else{
				    	$buttons .= $this->makeButton([
	                        'type' => 'edit',
	                        'id'   => $record->id,
	                    ]);
	                    $buttons .= $this->makeButton([
	                        'type' => 'delete',
	                        'id'   => $record->id,
	                    ]);
			    	}

                   return $buttons;
               })
               ->rawColumns(['elemen', 'action'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.elemen.index');
    }

    public function create()
    {
        return $this->render('modules.master.elemen.create');
    }

    public function store(ElemenRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record = new Elemen;
	        $record->fill($request->all());
	        $record->save();

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'IACM Elemen';
		    $log->aktivitas = 'Membuat IACM Elemen dengan Elemen '.$request->elemen;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }
    
    public function edit($id)
    {
    	$record = Elemen::find($id);
        return $this->render('modules.master.elemen.edit', ['record' => $record]);
    }

    public function update(ElemenRequest $request, Elemen $Elemen)
    {
    	DB::beginTransaction();
        try {
        	$record = Elemen::find($request->id);
        	$name = $record->elemen;
	        $record->fill($request->all());
	        $record->save();

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'IACM Elemen';
		     $log->aktivitas = 'Mengubah IACM Elemen dengan Nama Elemen '.$name.' menjadi '.$request->elemen;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy($elemen)
    {	
    	$record = Elemen::find($elemen);
    	if($record->kpa->count() > 0){
    		return response([
                'status' => true,
            ],500);
    	}else{
	    	$user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'IACM Elemen';
		    $log->aktivitas = 'Menghapus IACM Elemen dengan Nama ELemen '.$record->elemen;
		    $log->user_id = $user->id;
		    $log->save();

	        $record->delete();
	        return response()->json([
	            'success' => true
	        ]);
    	}
    }
}
