<?php

namespace App\Http\Controllers\Master\Iacm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Models\Master\Kpa;
use App\Models\Master\KpaLevel;
use App\Models\Master\KpaDetil;
use App\Models\Master\KpaDetilUraian;
use App\Models\SessionLog;

use DB;

class UraianController extends Controller
{
    protected $routes = 'master.iacm.uraian';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'IACM' => '#', 'Uraian' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'elemen',
                'name' => 'elemen',
                'label' => 'Elemen',
                'width' => '400px',
                'sortable' => true,
            ],
            [
                'data' => 'kpa',
                'name' => 'kpa',
                'label' => 'Key Process Area',
                'width' => '400px',
                'sortable' => true,
            ],
            [
                'data' => 'uraian',
                'name' => 'uraian',
                'label' => 'Uraian',
                'width' => '400px',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => 'text-center',
                'width' => '200px',
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = Kpa::select('*');
        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if($kpa = request()->kpa) {
        	$records->whereHas('level', function($u) use ($kpa){
        		$u->whereHas('detail', function($z) use ($kpa){
        			$z->where('kpa', 'like', '%' . $kpa . '%');
        		});
        	});
        }
        if($uraian = request()->uraian) {
        	$records->whereHas('level', function($u) use ($uraian){
        		$u->whereHas('detail', function($z) use ($uraian){
        			$z->where('uraian', 'like', '%' . $uraian . '%')
        			  ->orWhereHas('detiluraian', function($z) use ($uraian){
        			  		$z->where('uraian', 'like', '%' . $uraian . '%');
        			  });
        		});
        	});
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('elemen', function ($record) {
                   return readMoreText($record->elemen->elemen, 100);
               })
               ->addColumn('kpa', function ($record) {
               		$pic ='';
	           		$pic .='<ul style="list-style: none;" class="list-group list-more1" data-display="3">';
	           		foreach ($record->level as $key => $value) {
	           				foreach ($value->detail as $key => $val) {
		           				$pic .='<li class="item" style="padding:0px;border:none;">'.readMoreText($val->kpa, 100).'</li>';
	           				}
	           		}
	           		$pic .='</ul>';
	               	return $pic;
               })
               ->addColumn('uraian', function ($record) {
	               	$angka=0;
               		foreach ($record->level as $key => $value) {
           				foreach ($value->detail as $key => $val) {
           					if(count($val->detiluraian) > 0){
           						foreach ($val->detiluraian as $key => $vil) {
           							if(!is_null($vil->uraian)){
	           							$angka +=1;
           							}
           						}
           					}else{
		           				if(!is_null($val->uraian)){
           							$angka +=1;
       							}
           					}
           				}
	           		}
               		$pic ='';
               		if($angka > 5){
		           		$pic .='<ul style="list-style: none;" class="list-group list-more2" data-display="5">';
               		}else{
               			$pic .='<ul style="list-style: none;" class="list-group" data-display="5">';
               		}
	           		foreach ($record->level as $key => $value) {
           				foreach ($value->detail as $key => $val) {
           					if(count($val->detiluraian) > 0){
           						foreach ($val->detiluraian as $key => $vil) {
           							$pic .='<li class="item" style="padding:0px;border:none;">'.readMoreText($vil->uraian, 100).'</li>';
           						}
           					}else{
		           				$pic .='<li class="item" style="padding:0px;border:none;">'.readMoreText($val->uraian, 100).'</li>';
           					}
           				}
	           		}
	           		$pic .='</ul>';
	               	return $pic;
               })
               ->editColumn('created_at', function ($record) {
                    if ($record->updated_at) {
	                    return $record->updated_at->diffForHumans();
	                }else{
	                    return $record->created_at->diffForHumans();
	                }
               })
               ->addColumn('action', function ($record) {
                   $buttons = '';

                   $buttons .= $this->makeButton([
                        'type' => 'edit',
                        'id'   => $record->id,
                   ]);

                   return $buttons;
               })
               ->rawColumns(['elemen', 'action','kpa','uraian'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.uraian.index');
    }

    public function edit($id)
    {
    	$this->setBreadcrumb(['Master' => '#', 'IACM' => '#', 'Uraian' => '#', 'Ubah' => '#']);
    	$record = Kpa::find($id);
    	$data = Kpa::where('elemen_id', $record->elemen_id)->get();
        return $this->render('modules.master.uraian.edit', [
        	'record' => $record,
        	'data' => $data,
        ]);
    }

    public function update(Request $request, Kpa $Kpa)
    {
    	DB::beginTransaction();
        try {
        	foreach ($request->detail as $key => $value) {
				$cari 				= KpaDetil::find($key);
				$cari->uraian 		= $value['uraian'];
				$cari->penjelasan 	= $value['penjelasan'];
				$cari->output 		= $value['output'];
				$cari->save();
				$hapus_detil = KpaDetilUraian::where('kpa_detail_id', $cari->id)
					  			  			  ->delete();
				if(!empty($value['data'])){
					$cari->saveDetail($value['data']);
				}
    		}
	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(Kpa $Kpa)
    {
        $Kpa->delete();
        return response()->json([
            'success' => true
        ]);
    }
}
