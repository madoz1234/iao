<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\StandarisasiTemuanRequest;
use App\Models\Master\StandarisasiTemuan;
use App\Models\Master\StandarisasiTemuanDetail;
use App\Models\SessionLog;

use DB;

class StandarisasiTemuanController extends Controller
{
    protected $routes = 'master.standarisasi-temuan';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'Standardisasi Temuan' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'bidang',
                'name' => 'bidang',
                'label' => 'Bidang',
                'className' => 'text-left',
                'width' => '200px',
                'sortable' => true,
            ],
            [
                'data' => 'kode',
                'name' => 'kode',
                'label' => 'Kode',
                'width' => '200px',
                'sortable' => true,
            ],
            [
                'data' => 'deskripsi',
                'name' => 'deskripsi',
                'label' => 'Standardisasi Temuan',
                'width' => '200px',
                'sortable' => true,
            ],
            [
                'data' => 'referensi',
                'name' => 'referensi',
                'label' => 'Referensi / Kriteria',
                'sortable' => true,
                'width' => '350px',
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Diperbarui Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = StandarisasiTemuan::select('*');
        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($bidang = request()->bidang) {
            $records->where('bidang', 'like', '%' . $bidang . '%');
        }
        if ($standar = request()->standar) {
            $records->where('deskripsi', 'like', '%' . $standar . '%');
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('bidang', function ($record) {
               		if($record->bidang == 1){
               			return 'Operasional';
               		}elseif($record->bidang == 2){
               			return 'Keuangan';
               		}else{
               			return 'Sistem';
               		}
               })
               ->addColumn('kode', function ($record) {
                   return $record->kode;
               })
               ->addColumn('deskripsi', function ($record) {
                   return $record->deskripsi;
               })
               ->addColumn('referensi', function ($record) {
                    $referensi ='';
	           		$referensi .='<ol class="list list-more1" style="margin-top:10px;" data-display="3">';
	           		foreach ($record->detail as $key => $value) {
	           				$referensi .='<li class="item" style="text-align:justify;">'.readMoreText($value->deskripsi, 100).'</li>';
	           		}
	           		$referensi .='</ol>';
	               	return $referensi;
               })
               ->editColumn('created_at', function ($record) {
                    if ($record->updated_at) {
	                    return $record->updated_at->diffForHumans();
	                }else{
	                    return $record->created_at->diffForHumans();
	                }
               })
               ->addColumn('action', function ($record) {
                   $buttons = '';
                   if($record->detail->count() > 0){
			    		if($record->kkadetail->count() > 0){
			    			$buttons = '';
				    	}else{
					    	$buttons .= $this->makeButton([
		                        'type' => 'edit',
		                        'id'   => $record->id,
		                    ]);
		                    $buttons .= $this->makeButton([
		                        'type' => 'delete',
		                        'id'   => $record->id,
		                    ]);
				    	}
			    	}else{
				    	$buttons .= $this->makeButton([
	                        'type' => 'edit',
	                        'id'   => $record->id,
	                   ]);
	                   $buttons .= $this->makeButton([
	                        'type' => 'delete',
	                        'id'   => $record->id,
	                    ]);
			    	}
                   return $buttons;
               })
               ->rawColumns(['action','referensi'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.standarisasi-temuan.index');
    }

    public function create()
    {
        return $this->render('modules.master.standarisasi-temuan.create');
    }

    public function store(StandarisasiTemuanRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record 			= new StandarisasiTemuan;
	        $record->bidang 	= $request->bidang;
	        $record->kode 		= $request->kode;
	        $record->deskripsi 	= $request->standarisasi;
	        $record->save();
	        $record->saveDetail($request->detail);

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Standardisasi Temuan';
		    $log->aktivitas = 'Membuat Standardisasi Temuan dengan Nama Standardisasi '.$request->standarisasi;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    // public function show(StandarisasiTemuan $user)
    // {
    //     return $user->toJson();
    // }

    public function edit(StandarisasiTemuan $StandarisasiTemuan)
    {
        return $this->render('modules.master.standarisasi-temuan.edit', ['record' => $StandarisasiTemuan]);
    }

    public function update(StandarisasiTemuanRequest $request, StandarisasiTemuan $StandarisasiTemuan)
    {
    	DB::beginTransaction();
        try {
        	$record 			= StandarisasiTemuan::find($request->id);
        	$record->bidang 	= $request->bidang;
	        $record->kode 		= $request->kode;
	        $record->deskripsi 	= $request->standarisasi;
	        $record->save();
	        if(!empty($request->exists)){
		        $hapus = StandarisasiTemuanDetail::where('standarisasi_id', $request->id)
		        									   ->whereNotIn('id', $request->exists)
			    									   ->delete();
	        }
	        $record->updateDetail($request->detail);

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Standardisasi Temuan';
		    $log->aktivitas = 'Mengubah Standardisasi Temuan dengan Nama Standardisasi '.$record->deskripsi;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(StandarisasiTemuan $StandarisasiTemuan)
    {
    	if($StandarisasiTemuan->kriteria->count() > 0){
    		if($StandarisasiTemuan->kkadetail->count() > 0){
    			return response([
                	'status' => true,
            	],500);
	    	}else{
		    	$user = auth()->user();
		    	$log = new SessionLog;
			    $log->modul = 'Master';
			    $log->sub = 'Standardisasi Temuan';
			    $log->aktivitas = 'Menghapus Standardisasi Temuan dengan Nama Standardisasi '.$StandarisasiTemuan->standarisasi;
			    $log->user_id = $user->id;
			    $log->save();
		        $StandarisasiTemuan->delete();
		        return response()->json([
		            'success' => true
		        ]);
	    	}
    	}else{
	    	$user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Standardisasi Temuan';
		    $log->aktivitas = 'Menghapus Standardisasi Temuan dengan Nama Standardisasi '.$StandarisasiTemuan->standarisasi;
		    $log->user_id = $user->id;
		    $log->save();

	        $StandarisasiTemuan->delete();
	        return response()->json([
	            'success' => true
	        ]);
    	}
    }
}
