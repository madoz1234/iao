<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Http\Requests\Master\SurveyRequest;
use App\Models\KegiatanAudit\Pelaporan\LHA;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit;
use App\Models\Master\Survey;
use App\Models\Master\SurveyPertanyaan;
use App\Models\SessionLog;
use App\Models\Survey\SurveyJawab;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

class SurveyController extends Controller
{
    protected $routes = 'master.survey';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'Survey' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'version',
                'name' => 'version',
                'label' => 'Version',
                'sortable' => true,
            ],
            [
                'data' => 'description',
                'name' => 'description',
                'label' => 'Deskripsi',
                'sortable' => true,
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'className' => 'text-center',
                'sortable' => true,
            ],
            [
                'data' => 'updated_at',
                'name' => 'updated_at',
                'label' => 'Diperbarui Pada',
                'className' => 'text-center',
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $user = auth()->user();
        $records = Survey::select('*');

        if(!isset(request()->order[0]['column'])) {
            $records->orderBy('created_at');
        }
        if ($version = request()->version) {
            $records->where('version', 'like', '%' . $version . '%');
        }
        if ($status = request()->status) {
            $records->where('status', $status);
        }
        return DataTables::of($records->get())
            ->addColumn('num', function ($record) {
                return request()->start;
            })
            ->addColumn('version', function ($record) {
                return $record->version;
            })
            ->editColumn('status', function ($record) {
                if ($record->status == 1) {
                    $string = '<span class="label label-success">Aktif</span>';
                } elseif ($record->status == 2) {
                $string = '<span class="label label-danger">Non-Aktif</span>';
                } else {
                    $string = '<span class="label label-warning">Belum Aktif</span>';
                }
                    return $string;
                })
            ->addColumn('description', function ($record) {
                return $record->readMoreText('description', 100);
            })
            ->editColumn('updated_at', function ($record) {
                if ($record->updated_at) {
                    return $record->updated_at->diffForHumans();
                }else{
                    return $record->created_at->diffForHumans();
                }
            })
            ->addColumn('action', function ($record) use ($user) {
                $buttons = '';
                $btn_buat = $this->makeButton([
                    'type'      => 'detail',
                    'label'     => '<i class="fa fa-plus text-primary"></i>',
                    'tooltip'   => 'Buat Survey',
                    'class'     => 'detil button',
                    'id'        => $record->id.'/buat-survey',
                ]);
                $btn_lihat = $this->makeButton([
                    'type'      => 'detail',
                    'label'     => '<i class="fa fa-eye text-primary"></i>',
                    'tooltip'   => 'Lihat Survey',
                    'class'     => 'lihat button',
                    'id'        => $record->id.'/lihat-survey',
                ]);
                $btn_ubah = $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-pencil text-info"></i>',
                    'tooltip'   => 'Ubah',
                    'class'     => 'm-l edit button',
                    'id'        => $record->id,
                ]);
                $btn_hapus = $this->makeButton([
                    'type'      => 'delete',
                    'label'     => '<i class="fa fa-trash text-danger"></i>',
                    'tooltip'   => 'Hapus',
                    'class'     => 'm-l delete button',
                    'id'        => $record->id,
                ]);

                if($record->status == 0){
                    $buttons .= $btn_buat;
                    $buttons .= $btn_ubah;
                    $buttons .= $btn_hapus;
                }else{
                    $buttons .= $btn_lihat;
                    $buttons .= $btn_ubah;
                }
                return $buttons;
            })
            ->rawColumns(['description', 'action', 'status'])
            ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.survey.index');
    }

    public function create()
    {
        return $this->render('modules.master.survey.create');
    }

    public function store(SurveyRequest $request)
    {
        DB::beginTransaction();
        try {
            $record = new Survey;
            if(!isset($request->status)){
                $record->status = 0;
            }else{
                $record->status = 1;
            }
            $record->version = $request->version;
            $record->description = $request->description;
            $record->save();

            $user = auth()->user();
            $log = new SessionLog;
            $log->modul = 'Master';
            $log->sub = 'Survey';
            $log->aktivitas = 'Menambah Survey Versi '.$request->version;
            $log->user_id = $user->id;
            $log->save();

            DB::commit();
            return response([
              'status' => true
            ]); 
        }catch (\Exception $e) {
            DB::rollback();
            return response([
                'status' => 'error',
                'message' => 'An error occurred!',
                'error' => $e->getMessage(),
            ], 500);
        }
    }

    public function buatSurvey(Survey $id)
    {
        return $this->render('modules.master.survey.buat-survey', [
          'record' => $id,
        ]);
    }

    public function lihatSurvey(Survey $id)
    {
        return $this->render('modules.master.survey.detil-survey', [
          'record' => $id,
        ]);
    }

    public function simpanData(SurveyRequest $request)
    {
        DB::beginTransaction();
        try {
            $record = Survey::find($request->id);
            if ($request->status == 1) {
                $survey_active = Survey::where('status', 1)->first();
                if($survey_active){
                	$survey_active->status =2;
                	$survey_active->save();
                	$record->status = 1;
                	$record->save();
                    // return response([
                    //   'status' => 'false',
                    //   'message' => 'Anda harus menonaktifkan versi yang sedang aktif terlebih dahulu',
                    // ], 500);
                }else{
                    $record->status = 1;
                    $record->save();
                    $add_survey_jawab = true;
                }
            }

            if(!empty($request->exists)){
                $hapus = SurveyPertanyaan::where('survei_id', $request->id)->whereNotIn('id', $request->exists)->delete();
            }
            if($request->detail){
                foreach ($request->detail as $key => $value) {
                    if(!empty($value['id'])){
                        $detail = SurveyPertanyaan::find($value['id']);
                    }else{
                        $detail = new SurveyPertanyaan;
                    }
                    $detail->survei_id     = $record->id;
                    $detail->pertanyaan     = $value['pertanyaan'];
                    $detail->save();
                }
            }

            if (isset($add_survey_jawab)) {
                $survey = Survey::find($request->id);
                $lha_selesai = LHA::where('status', 6)->get();
                if (count($lha_selesai) > 0) {
                    foreach ($lha_selesai as $lha) {
                        $pic_object_audit = object_user($lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $lha->draftkka->program->penugasanaudit->rencanadetail->object_id);
                        foreach ($pic_object_audit[0] as $pic_audit) {
                            if(SurveyJawab::where('lha_id', $lha->id)->where('user_id', $pic_audit)->first() == null){
                                $survey_jawab = new SurveyJawab;
                                $survey_jawab->lha_id = $lha->id;
                                $survey_jawab->survei_id = $survey->id;
                                $survey_jawab->user_id = $pic_audit;
                                $survey_jawab->save();
                            }
                        }
                    }
                }
            }

            $user = auth()->user();
            $log = new SessionLog;
            $log->modul = 'Master';
            $log->sub = 'Survey';
            $log->aktivitas = 'Menambah Pertanyaan pada Survey Versi '.$record->version;
            $log->user_id = $user->id;
            $log->save();

            DB::commit();
            return response([
                'status' => true
            ]); 
        }catch (\Exception $e) {
            DB::rollback();
            return response([
                'status' => 'error',
                'message' => 'An error occurred!',
                'error' => $e->getMessage(),
            ], 500);
        }
    }

    public function edit(Survey $Survey)
    {
        return $this->render('modules.master.survey.edit', ['record' => $Survey]);
    }

    public function update(SurveyRequest $request, Survey $Survey)
    {
        DB::beginTransaction();
        try {
            $record = Survey::find($request->id);
            if(!isset($request->status)){
                if(count($record->pertanyaan) > 0){
                    $record->status = 2;
                }else{
                    $record->status = 0;
                }
            }else{
                $survey_active = Survey::where('status', 1)->where('id', '!=', $record->id)->first();
                if ($survey_active) {
                	$survey_active->status =2;
                	$survey_active->save();
                	$record->status = 1;
                	$record->save();
                    // return response([
                    //   'status' => 'false',
                    //   'message' => 'Anda harus menonaktifkan versi yang sedang aktif terlebih dahulu',
                    // ], 500);
                }else{
                    $record->status = 1;
                    $add_survey_jawab = true;
                }
            }
            $record->version = $request->version;
            $record->description = $request->description;
            $record->save();

            if (isset($add_survey_jawab)) {
                $survey = Survey::find($request->id);
                $lha_selesai = LHA::where('status', 6)->get();
                if (count($lha_selesai) > 0) {
                    foreach ($lha_selesai as $lha) {
                        $pic_object_audit = object_user($lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $lha->draftkka->program->penugasanaudit->rencanadetail->object_id);
                        foreach ($pic_object_audit[0] as $pic_audit) {
                            if(SurveyJawab::where('lha_id', $lha->id)->where('user_id', $pic_audit)->first() == null){
                                $survey_jawab = new SurveyJawab;
                                $survey_jawab->lha_id = $lha->id;
                                $survey_jawab->survei_id = $survey->id;
                                $survey_jawab->user_id = $pic_audit;
                                $survey_jawab->save();
                            }
                        }
                    }
                }
            }

            $user = auth()->user();
            $log = new SessionLog;
            $log->modul = 'Master';
            $log->sub = 'Survey';
            $log->aktivitas = 'Mengubah Survey dengan Versi '.$record->version;
            $log->user_id = $user->id;
            $log->save();

            DB::commit();
            return response([
                'status' => true
            ]); 
        }catch (\Exception $e) {
            DB::rollback();
            return response([
                'status' => 'error',
                'message' => 'An error occurred!',
                'error' => $e->getMessage(),
            ], 500);
        }
    }

    public function destroy(Survey $Survey)
    {
        $user = auth()->user();
        $log = new SessionLog;
        $log->modul = 'Master';
        $log->sub = 'Survey';
        $log->aktivitas = 'Menghapus Survey Versi '.$Survey->version;
        $log->user_id = $user->id;
        $log->save();

        $Survey->delete();
        return response()->json([
            'success' => true
        ]);
    }
}
