<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\CaeRequest;
use App\Models\Master\Cae;
use App\Models\SessionLog;

use DB;

class CaeController extends Controller
{
    protected $routes = 'master.cae';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'Anak Perusahaan' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama',
                'width' => '200px',
                'sortable' => true,
            ],
            [
                'data' => 'alamat',
                'name' => 'alamat',
                'label' => 'Alamat',
                'sortable' => true,
            ],
            [
                'data' => 'no_tlp',
                'name' => 'no_tlp',
                'label' => 'No Telepon',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Diperbarui Pada',
                'className' => 'text-center',
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = Cae::when($name = request()->name, function ($q) use ($name) {
            			 $q->where('nama', 'like', '%'.$name.'%');
        			  })->select('*');

        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('nama', function ($record) {
                   return $record->nama;
               })
               ->addColumn('alamat', function ($record) {
                   return $record->readMoreText('alamat', 100);
               })
               ->addColumn('no_tlp', function ($record) {
                   return $record->no_tlp;
               })
               ->editColumn('created_at', function ($record) {
                    if ($record->updated_at) {
	                    return $record->updated_at->diffForHumans();
	                }else{
	                    return $record->created_at->diffForHumans();
	                }
               })
               ->addColumn('action', function ($record) {
                   $buttons = '';

                   $buttons .= $this->makeButton([
                        'type' => 'edit',
                        'id'   => $record->id,
                   ]);
                   $buttons .= $this->makeButton([
                        'type' => 'delete',
                        'id'   => $record->id,
                    ]);

                   return $buttons;
               })
               ->rawColumns(['alamat', 'action'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.cae.index');
    }

    public function create()
    {
        return $this->render('modules.master.cae.create');
    }

    public function store(CaeRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record = new Cae;
	        $record->fill($request->all());
	        $record->save();
	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    // public function show(Cae $user)
    // {
    //     return $user->toJson();
    // }

    public function edit(Cae $Cae)
    {
        return $this->render('modules.master.cae.edit', ['record' => $Cae]);
    }

    public function update(CaeRequest $request, Cae $Cae)
    {
    	DB::beginTransaction();
        try {
        	$record = Cae::find($request->id);
	        $record->fill($request->all());
	        $record->save();
	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(Cae $Cae)
    {
        $Cae->delete();
        return response()->json([
            'success' => true
        ]);
    }
}
