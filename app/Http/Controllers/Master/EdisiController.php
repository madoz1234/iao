<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\EdisiRequest;
use App\Models\Master\Edisi;
use App\Models\SessionLog;

use DB;

class EdisiController extends Controller
{
    protected $routes = 'master.edisi';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'Edisi' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'edisi',
                'name' => 'edisi',
                'label' => 'Edisi',
                'width' => '400px',
                'sortable' => true,
                'className' => 'text-left',
            ],
            [
                'data' => 'revisi',
                'name' => 'revisi',
                'label' => 'Revisi',
                'width' => '150px',
                'sortable' => true,
                'className' => 'text-center',
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'width' => '200px',
                'sortable' => true,
                'className' => 'text-center',
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Diperbarui Pada',
                'width' => '200px',
                'className' => 'text-center',
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = Edisi::select('*');

        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($edisi = request()->edisi) {
            $records->where('edisi', 'like', '%' . $edisi . '%');
        }
        return DataTables::of($records->get())
           ->addColumn('num', function ($record) {
               return request()->start;
           })
           ->addColumn('edisi', function ($record) {
               return $record->edisi;
           })
           ->addColumn('revisi', function ($record) {
               return $record->revisi;
           })
           ->addColumn('status', function ($record) {
               if($record->status == 1){
	               return '<span class="label label-success">Aktif</span>';
	           }else{
	           	   return '<span class="label label-warning">Nonaktif</span>';
	           }
           })
           ->editColumn('created_at', function ($record) {
                if ($record->updated_at) {
                    return $record->updated_at->diffForHumans();
                }else{
                    return $record->created_at->diffForHumans();
                }
           })
           ->addColumn('action', function ($record) {
               $buttons = '';

               $buttons .= $this->makeButton([
                    'type' => 'edit',
                    'id'   => $record->id,
               ]);
               // $buttons .= $this->makeButton([
               //      'type' => 'delete',
               //      'id'   => $record->id,
               //  ]);
               return $buttons;
           })
           ->rawColumns(['alamat', 'action','status'])
           ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.edisi.index');
    }

    public function create()
    {
        return $this->render('modules.master.edisi.create');
    }

    public function store(EdisiRequest $request)
    {
    	DB::beginTransaction();
    	try {
	    	$record = new Edisi;
    		if(!empty($request->status)){
    			$cek = Edisi::where('status', 1)->first();
    			if($cek){
    			    if($cek->id == $request->id){
					}else{
	    			  $cek->status =2;
	                  $cek->save();
					}
    			}
		        $record->edisi 	= $request->edisi;
		        $record->revisi = $request->revisi;
		        $record->status = 1;
    		}else{
		        $record->edisi 	= $request->edisi;
		        $record->revisi = $request->revisi;
		        $record->status = 2;
    		}
		    $record->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    // public function show(Edisi $user)
    // {
    //     return $user->toJson();
    // }

    public function edit(Edisi $Edisi)
    {
        return $this->render('modules.master.edisi.edit', ['record' => $Edisi]);
    }

    public function update(EdisiRequest $request, Edisi $Edisi)
    {
    	DB::beginTransaction();
        try {
        	$record = Edisi::find($request->id);
	        if(!empty($request->status)){
	        	$cek = Edisi::where('status', 1)->first();
    			if($cek){
    				if($cek->id == $request->id){
    				}else{
	    			  $cek->status =2;
	                  $cek->save();
    				}
    			}
		        $record->edisi 	= $request->edisi;
		        $record->revisi = $request->revisi;
		        $record->status = 1;
    		}else{
		        $record->edisi 	= $request->edisi;
		        $record->revisi = $request->revisi;
		        $record->status = 2;
    		}
		    $record->save();
	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(Edisi $Edisi)
    {
        $Edisi->delete();
        return response()->json([
            'success' => true
        ]);
    }
}
