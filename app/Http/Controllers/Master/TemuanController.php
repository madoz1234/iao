<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\TemuanRequest;
use App\Models\Master\Temuan;
use App\Models\Master\TemuanDetail;
use App\Models\SessionLog;

use DB;

class TemuanController extends Controller
{
    protected $routes = 'master.temuan';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'Temuan' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'bidang',
                'name' => 'bidang',
                'label' => 'Bidang',
                'className' => 'text-left',
                'width' => '200px',
                'sortable' => true,
            ],
            [
                'data' => 'kode',
                'name' => 'kode',
                'label' => 'Kode Standardisasi',
                'width' => '100px',
                'sortable' => true,
            ],
            [
                'data' => 'standarisasi',
                'name' => 'standarisasi',
                'label' => 'Standardisasi Temuan',
                'width' => '250px',
                'sortable' => true,
            ],
            [
                'data' => 'kode2',
                'name' => 'kode2',
                'label' => 'Kode Temuan',
                'width' => '400px',
                'sortable' => true,
            ],
            [
                'data' => 'referensi',
                'name' => 'referensi',
                'label' => 'Referensi / Kriteria',
                'width' => '250px',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Diperbarui Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
    	$records = Temuan::select('*');
    	if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($bidang = request()->bidang) {
        	$records->whereHas('standarisasi', function($u) use ($bidang){
	            $u->where('bidang', 'like', '%' . $bidang . '%');
        	});
        }
        if ($standardisasi = request()->standardisasi) {
        	$records->whereHas('standarisasi', function($u) use ($standardisasi){
        		$u->where('deskripsi', 'like', '%' . $standardisasi . '%');
        	});
        }
        if ($temuan = request()->temuan) {
        	$records->whereHas('detail', function($u) use ($temuan){
        		$u->where('deskripsi', 'like', '%' . $temuan . '%');
        	});
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('bidang', function ($record) {
                   if($record->standarisasi->bidang == 1){
               			return 'Operasional';
               		}elseif($record->standarisasi->bidang == 2){
               			return 'Keuangan';
               		}else{
               			return 'Sistem';
               		}
               })
               ->addColumn('kode', function ($record) {
                   return $record->standarisasi->kode;
               })
               ->addColumn('standarisasi', function ($record) {
                    return $record->standarisasi->deskripsi;
               })
               ->addColumn('kode2', function ($record) {
                    $kode ='';
               		$kode .='<ul class="list list-more1 list-unstyled" data-display="3">';
               		$lengths = array_map('strlen', $record->detail->pluck('kode')->toArray());
               		foreach ($record->detail as $key => $value) {
               				$kode .='<li class="item" style="text-align:justify;">
               				<table style="border-bottom:.1em solid #000000;">
								<tbody>
									<tr>
										<td style="text-align:justify;">'.$value->kode.'&nbsp;&nbsp;-&nbsp;&nbsp;</td>
										<td style="text-align:justify;">'.readMoreText($value->deskripsi, 100).'</td>
									</tr>
								</tbody>
							</table></li>';
               		}
               		$kode .='</ul>';
                   	return $kode;
               })
               ->addColumn('referensi', function ($record) {
                   $referensi ='';
	           		$referensi .='<ul class="list list-more2 list-unstyled" data-display="3">';
	           		foreach ($record->standarisasi->detail as $key => $value) {
	           				$referensi .='<li class="item" style="text-align:justify;">'.readMoreText($value->deskripsi, 100).'</li>';
	           		}
	           		$referensi .='</ul>';
	               	return $referensi;
               })
               ->editColumn('created_at', function ($record) {
                    if ($record->updated_at) {
	                    return $record->updated_at->diffForHumans();
	                }else{
	                    return $record->created_at->diffForHumans();
	                }
               })
               ->addColumn('action', function ($record) {
                   $buttons = '';
                    if($record->standarisasi->count() > 0){
			    		if($record->standarisasi->kkadetail->count() > 0){
				    		$buttons = '';
			    		}else{
			    			$buttons = '';
			    		}
			    	}else{
			    		$buttons .= $this->makeButton([
	                        'type' => 'edit',
	                        'id'   => $record->id,
	                    ]);
	                    $buttons .= $this->makeButton([
	                        'type' => 'delete',
	                        'id'   => $record->id,
	                    ]);
			    	}
                   return $buttons;
               })
               ->rawColumns(['action','temuan','kode2','referensi'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.temuan.index');
    }

    public function create()
    {
    	$this->setBreadcrumb(['Master' => '#', ' Temuan' => '#', 'Tambah' => '#']);
        return $this->render('modules.master.temuan.create');
    }

    public function store(TemuanRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record = new Temuan;
	        $record->standarisasi_id = $request->standarisasi_id;
	        $record->save();
	        $record->saveDetail($request->detail);

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Temuan';
		    $log->aktivitas = 'Membuat Temuan dengan Kode Temuan '.$record->kode;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    // public function show(Temuan $user)
    // {
    //     return $user->toJson();
    // }

    public function edit(Temuan $Temuan)
    {
    	$this->setBreadcrumb(['Master' => '#', ' Temuan' => '#', 'Ubah' => '#']);
        return $this->render('modules.master.temuan.edit', ['record' => $Temuan]);
    }

    public function update(TemuanRequest $request, Temuan $Temuan)
    {
    	DB::beginTransaction();
        try {
        	if(!empty($request->exists)){
	        	$hapus = TemuanDetail::whereNotIn('id', $request->exists)
	        							   ->where('temuan_id', $request->id)
	                        			   ->delete();
	        }

        	$record = Temuan::find($request->id);
	        $record->standarisasi_id = $request->standarisasi_id;
	        $record->save();
	        $record->updateDetail($request->detail);

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Temuan';
		    $log->aktivitas = 'Mengubah Temuan';
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(Temuan $Temuan)
    {
    	$cek=0;
    	if($Temuan->standarisasi->count() > 0){
    		if($Temuan->standarisasi->kkadetail->count() > 0){
	    		return response([
	                'status' => true,
	            ],500);
    		}else{
    			return response([
	                'status' => true,
	            ],500);
    		}
    	}else{
    		$user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Temuan';
		    $log->aktivitas = 'Menghapus Temuan dengan Nama Kategori '.$Temuan->kategori->kategori;
		    $log->user_id = $user->id;
		    $log->save();

	        $Temuan->delete();
	        return response()->json([
	            'success' => true
	        ]);
    	}
    }
}
