<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\LangkahKerjaRequest;
use App\Models\Master\LangkahKerja;
use App\Models\Master\LangkahKerjaDetail;
use App\Models\SessionLog;

use DB;

class LangkahKerjaController extends Controller
{
    protected $routes = 'master.langkah-kerja';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'Langkah Kerja' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            [
                'data' => 'bidang',
                'name' => 'bidang',
                'label' => 'Bidang',
                'className' => 'text-left',
                'width' => '120px',
                'sortable' => true,
            ],
            [
                'data' => 'fokus_audit',
                'name' => 'fokus_audit',
                'label' => 'Fokus Audit',
                'sortable' => true,
                'width' => '300px',
            ],
            [
                'data' => 'langkah',
                'name' => 'langkah',
                'label' => 'Langkah Kerja',
                'sortable' => true,
                'width' => '300px',
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Diperbarui Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = LangkahKerja::select('*');

        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($audit = request()->audit) {
            $records->whereHas('fokusaudit', function($z) use ($audit) {
    			 	$z->where('audit', 'like', '%'.$audit.'%');
    			 });
        }
        if ($bidang = request()->bidang) {
            $records->where('bidang', 'like', '%' . $bidang . '%');
        }

        if ($langkah = request()->langkah) {
            $records->whereHas('detail', function($u) use($langkah){
            			$u->where('deskripsi', 'like', '%'.$langkah.'%');
            		});
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('bidang', function ($record) {
               		if($record->bidang == 1){
               			return 'Operasional';
               		}elseif($record->bidang == 2){
               			return 'Keuangan';
               		}else{
               			return 'Sistem';
               		}
               })
               ->addColumn('fokus_audit', function ($record) {
                   return readMoreText($record->fokusaudit->audit, 100);
               })
               ->addColumn('langkah', function ($record) {
               		$langkah ='';
               		$langkah .='<ul class="list list-more1" data-display="3">';
               		foreach ($record->detail as $key => $value) {
               				$langkah .='<li class="item">'.readMoreText($value->deskripsi, 100).'</li>';
               		}
               		$langkah .='</ul>';
                   	return $langkah;
               })
               ->editColumn('created_by', function ($record) {
                   return $record->creatorName();
               })
               ->editColumn('created_at', function ($record) {
                    if ($record->updated_at) {
	                    return $record->updated_at->diffForHumans();
	                }else{
	                    return $record->created_at->diffForHumans();
	                }
               })
               ->addColumn('action', function ($record) {
                   $buttons = '';

                   $buttons .= $this->makeButton([
                    'type' 		=> 'edit',
                    'label' 	=> '<i class="fa fa-edit text-primary"></i>',
                    'tooltip' 	=> 'Ubah',
                    'class' 	=> 'edit button',
                    'id'   		=> $record->id,
               	   ]);

                   $buttons .= $this->makeButton([
                        'type' => 'delete',
                        'id'   => $record->id,
                    ]);

                   return $buttons;
               })
               ->rawColumns(['audit', 'action','fokus_audit','langkah'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.langkah-kerja.index');
    }

    public function create()
    {
    	$this->setBreadcrumb(['Master' => '#', 'Langkah Kerja' => '#', 'Tambah' => '#']);
        return $this->render('modules.master.langkah-kerja.create');
    }

    public function store(LangkahKerjaRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record = new LangkahKerja;
	        $record->bidang      	= $request->bidang;
	        $record->fokus_audit_id = $request->fokus;
	        $record->save();
	        $record->saveDetail($request->detail);

	        if($request->bidang == 1){
		        $bidang = 'Operasional';
	        }elseif($request->bidang == 2){
		        $bidang = 'Keuangan';
	        }else{
		        $bidang = 'Sistem';
	        }

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Langkah Kerja';
		    $log->aktivitas = 'Membuat Langkah Kerja dengan Bidang '.$bidang.' dengan nama Fokus Audit '.$record->fokusaudit->audit;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function edit(LangkahKerja $LangkahKerja)
    {
    	$this->setBreadcrumb(['Master' => '#', 'Langkah Kerja' => '#', 'Ubah' => '#']);
        return $this->render('modules.master.langkah-kerja.edit', ['record' => $LangkahKerja]);
    }

    public function update(LangkahKerjaRequest $request, LangkahKerja $LangkahKerja)
    {
    	DB::beginTransaction();
        try {
        	if(!empty($request->exists)){
	        	$hapus = LangkahKerjaDetail::whereNotIn('id', $request->exists)
	        							   ->where('langkah_kerja_id', $request->id)
	                        			   ->delete();
	        }

        	$record = LangkahKerja::find($request->id);
	        $record->bidang 	 	= $request->bidang;
	        $record->fokus_audit_id = $request->fokus;
	        $record->save();
	        $record->updateDetail($request->detail);

	        if($record->bidang == 1){
		        $bidang = 'Operasional';
	        }elseif($record->bidang == 2){
		        $bidang = 'Keuangan';
	        }else{
		        $bidang = 'Sistem';
	        }

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Langkah Kerja';
		    $log->aktivitas = 'Mengubah Langkah Kerja dengan Bidang '.$bidang.' dengan nama Fokus Audit '.$record->fokusaudit->audit;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(LangkahKerja $LangkahKerja)
    {
    	if($LangkahKerja->bidang == 1){
	        $bidang = 'Operasional';
        }elseif($LangkahKerja->bidang == 2){
	        $bidang = 'Keuangan';
        }else{
	        $bidang = 'Sistem';
        }

        $user = auth()->user();
    	$log = new SessionLog;
	    $log->modul = 'Master';
	    $log->sub = 'Langkah Kerja';
	    $log->aktivitas = 'Menghapus Langkah Kerja dengan Bidang '.$bidang.' dengan nama Fokus Audit '.$LangkahKerja->fokusaudit->audit;
	    $log->user_id = $user->id;
	    $log->save();

        $LangkahKerja->delete();
        return response()->json([
            'success' => true
        ]);
    }
}
