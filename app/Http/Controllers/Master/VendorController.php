<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\VendorRequest;
use App\Models\Master\Vendor;
use App\Models\SessionLog;

use DB;

class VendorController extends Controller
{
    protected $routes = 'master.vendor';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'Vendor' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'kode',
                'name' => 'kode',
                'label' => 'Kode Vendor',
                'width' => '200px',
                'sortable' => true,
            ],
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Vendor',
                'width' => '250px',
                'sortable' => true,
            ],
            [
                'data' => 'alamat',
                'name' => 'alamat',
                'label' => 'Alamat',
                'sortable' => true,
                'width' => '200px',
            ],
            [
                'data' => 'no_tlp',
                'name' => 'no_tlp',
                'label' => 'No Telepon',
                'width' => '150px',
                'sortable' => true,
                'className' => 'text-left',
            ],
            [
                'data' => 'pic',
                'name' => 'pic',
                'label' => 'PIC',
                'sortable' => true,
                'className' => 'text-left',
                'width' => '200px',
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Diperbarui Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = Vendor::select('*');

        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($kode = request()->kode) {
            $records->where('kode', 'like', '%' . $kode . '%');
        }
        if ($name = request()->name) {
            $records->where('nama', 'like', '%' . $name . '%');
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('kode', function ($record) {
                   if($record->kode){
	           			return $record->kode;
	           		}else{
	                   return '';
	           		}
               })
               ->addColumn('nama', function ($record) {
                   return $record->nama;
               })
               ->addColumn('alamat', function ($record) {
                   return $record->readMoreText('alamat', 100);
               })
               ->addColumn('no_tlp', function ($record) {
	           	   return $record->no_tlp;
               })
               ->editColumn('created_at', function ($record) {
                    if ($record->updated_at) {
	                    return $record->updated_at->diffForHumans();
	                }else{
	                    return $record->created_at->diffForHumans();
	                }
               })
               ->addColumn('pic', function ($record) {
                   	$pic ='';
	           		$pic .='<ul class="list-group list-more1" data-display="2">';
	           		foreach ($record->detail_pic as $key => $value) {
	           				$pic .='<li class="list-group-item" style="padding:0px;border:none;">'.$value->user->name.'</li>';
	           		}
	           		$pic .='</ul>';
	               	return $pic;
               })
               ->addColumn('action', function ($record) {
                   $buttons = '';

                   $buttons .= $this->makeButton([
                        'type' => 'edit',
                        'id'   => $record->id,
                   ]);
                   $buttons .= $this->makeButton([
                        'type' => 'delete',
                        'id'   => $record->id,
                    ]);

                   return $buttons;
               })
               ->rawColumns(['alamat', 'action','pic'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.vendor.index');
    }

    public function create()
    {
        return $this->render('modules.master.vendor.create');
    }

    public function store(VendorRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record = new Vendor;
	        $record->fill($request->all());
	        $record->save();
	        $record->saveDetail($request->pic);

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Vendor';
		    $log->aktivitas = 'Membuat Vendor dengan Kode Vendor '.$request['kode'];
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    // public function show(Vendor $user)
    // {
    //     return $user->toJson();
    // }

    public function edit(Vendor $vendor)
    {
        return $this->render('modules.master.vendor.edit', ['record' => $vendor]);
    }

    public function update(VendorRequest $request, Vendor $vendor)
    {
    	DB::beginTransaction();
        try {
        	$record = Vendor::find($request->id);
        	$name = $record->kode;
	        $record->fill($request->all());
	        $record->save();
	        $record->updateDetail($request->pic, $request->id);

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'Vendor';
		    $log->aktivitas = 'Mengubah Vendor dengan Kode Vendor '.$name.' menjadi '.$request['kode'];
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(Vendor $vendor)
    {
    	$user = auth()->user();
    	$log = new SessionLog;
	    $log->modul = 'Master';
	    $log->sub = 'Vendor';
	    $log->aktivitas = 'Menghapus Vendor dengan Kode Vendor '.$vendor->kode;
	    $log->user_id = $user->id;
	    $log->save();

        $vendor->delete();
        return response()->json([
            'success' => true
        ]);
    }
}
