<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\KertasKerjaRequest;
use App\Models\Master\KertasKerja;
use App\Models\Master\KertasKerjaDetail;
use App\Models\Master\KertasKerjaDetailNilai;
use App\Models\Master\SpinPembobotan;
use App\Models\SessionLog;

use DB;

class SpinKerjaController extends Controller
{
    protected $routes = 'master.spin-kerja';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'SPIN' => '#', 'Kertas Kerja' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'spin',
                'name' => 'spin',
                'label' => 'SPIN Version',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'kategori',
                'name' => 'kategori',
                'label' => 'Kategori',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '150px',
            ],
            [
                'data' => 'object',
                'name' => 'object',
                'label' => 'Objek Audit',
                'sortable' => true,
                'width' => '250px',
            ],
            [
                'data' => 'deskripsi',
                'name' => 'deskripsi',
                'label' => 'Deskripsi',
                'sortable' => true,
                'width' => '400px',
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'sortable' => true,
                'className' => 'text-center',
                'width' => '120px',
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Diperbarui Pada',
                'className' => 'text-center',
                'width' => '100px',
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $user = auth()->user();
        $records = KertasKerja::select('*');

        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($judul = request()->judul) {
            $records->where('judul', 'like', '%' . $judul . '%');
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('spin', function ($record) {
                   return $record->spin->version;
               })
               ->addColumn('kategori', function ($record) {
	               	if($record->kategori == 1){
	               		return '<span class="label label-success">Business Unit (BU)</span>';
	               	}else{
	               		return '<span class="label label-info">Corporate Office (CO)</span>';
	               	}
               })
               ->addColumn('object', function ($record) {
	               	if($record->kategori == 1){
	               		return object_audit(0, $record->kategori_id);
	               	}else{
	               		return object_audit(1, $record->kategori_id);
	               	}
               })
               ->addColumn('deskripsi', function ($record) {
                   return $record->readMoreText('deskripsi', 100);
               })
               ->addColumn('status', function ($record) {
                   if ($record->status == 1) {
	                  $string = '<span class="label label-success">Aktif</span>';
	               }elseif ($record->status == 2) {
	                  $string = '<span class="label label-danger">Non-Aktif</span>';
	               }else {
	                  $string = '<span class="label label-warning">Belum Aktif</span>';
	               }
	                 return $string;
               })
               ->editColumn('created_at', function ($record) {
                    if ($record->updated_at) {
	                    return $record->updated_at->diffForHumans();
	                }else{
	                    return $record->created_at->diffForHumans();
	                }
               })
               ->addColumn('action', function ($record) use ($user) {
                   $buttons = '';
                    if($KertasKerja->pemenuhan->count() > 0){
			    		$buttons = '';
			    	}else{
				    	$buttons .= $this->makeButton([
			              'type'      => 'edit',
			              'label'     => '<i class="fa fa-plus text-primary"></i>',
			              'tooltip'   => 'Buat Kertas Kerja',
			              'class'     => 'buat button',
			              'id'        => $record->id,
			            ]);
			            $buttons .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	                    $buttons .= $this->makeButton([
	                        'type' => 'edit',
	                        'id'   => $record->id,
	                    ]);
	                    $buttons .= $this->makeButton([
	                        'type' => 'delete',
	                        'id'   => $record->id,
	                   ]);
			    	}
                   return $buttons;
               })
               ->rawColumns(['action', 'action','kategori','status','object','deskripsi'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.kertas-kerja.index');
    }

    public function create()
    {
        return $this->render('modules.master.kertas-kerja.create');
    }

    public function buat(KertasKerja $id)
    {	
		$this->setBreadcrumb(['Master' => '#', 'SPIN' => '#', 'Kertas Kerja' => '#', 'Buat' => '#']);
        return $this->render('modules.master.kertas-kerja.buat',[
        	'record' => $id,
        ]);
    }

    public function simpanData(Request $request)
    {

      DB::beginTransaction();
      try {
			$kerja = KertasKerja::find($request->id);
			foreach ($request->cek as $key => $detail) {
				$cari = KertasKerjaDetail::where('kertas_kerja_id', $kerja->id)->where('pembobotan_detail_id', $detail['id'])->first();
				$detilkerja = KertasKerjaDetail::find($cari->id);
				if(!empty($detail['status'])){
					$detilkerja->status = 1;
				}else{
					$detilkerja->status = 0;
				}
				$detilkerja->save();
			}

			foreach ($request->detail as $kiy => $value) {
				$cari = KertasKerjaDetail::where('kertas_kerja_id', $kerja->id)->where('pembobotan_detail_id', $kiy)->first();
				foreach($value['data'] as $key => $value) {
					$cari2 = KertasKerjaDetailNilai::where('detail_id', $cari->id)->where('detail_nilai_id', $value['id'])->first();
					$detil_kerja_nilai = KertasKerjaDetailNilai::find($cari2->id);
					if(!empty($value['status'])){
						$detil_kerja_nilai->status = 1;
					}else{
						$detil_kerja_nilai->status = 0;
					}
					$detil_kerja_nilai->save();
				}
			}
	      $user = auth()->user();
	      $log = new SessionLog;
		  $log->modul = 'Master';
		  $log->sub = 'SPIN Kertas Kerja';
		  $log->aktivitas = 'Mengubah SPIN Kertas Kerja';
		  $log->user_id = $user->id;
		  $log->save();

          DB::commit();
          return response([
            'status' => true
          ]); 
      }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function store(KertasKerjaRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$max = KertasKerja::max('version');
    		$record = new KertasKerja;
    		if(!isset($request->status)){
				$record->status = 0;
			}else{
    			$record->status = 1;
			}
			$record->spin_id 		= $request->spin_id;
			$record->kategori 		= $request->kategori;
			$record->kategori_id 	= $request->kategori_id;
			$record->deskripsi 		= $request->deskripsi;
			if($max){
				$record->version 		= $max+1;
			}else{
				$record->version 		= 1;
			}
	        $record->save();

			$spin = SpinPembobotan::find($request->spin_id);
			foreach ($spin->detail as $key => $detail) {
				$detil_kerja = new KertasKerjaDetail;
				$detil_kerja->kertas_kerja_id = $record->id;
				$detil_kerja->pembobotan_detail_id = $detail->id;
				$detil_kerja->status = 0;
				$detil_kerja->save();

				foreach ($detail->detailnilai as $key => $vil) {
					$detil_kerja_nilai = new KertasKerjaDetailNilai;
					$detil_kerja_nilai->detail_id = $detil_kerja->id;
					$detil_kerja_nilai->detail_nilai_id = $vil->id;
					$detil_kerja_nilai->status = 0;
					$detil_kerja_nilai->save();
				}
			}

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'SPIN Kertas Kerja';
		    $log->aktivitas = 'Membuat SPIN Kertas Kerja';
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function edit($id)
    {
    	$record = KertasKerja::find($id);
        return $this->render('modules.master.kertas-kerja.edit', ['record' => $record]);
    }

    public function update(KertasKerjaRequest $request, KertasKerja $KertasKerja)
    {
    	DB::beginTransaction();
        try {
        	$record = KertasKerja::find($request->id);
        	if(!isset($request->status)){
				$record->status = 0;
			}else{
    			$record->status = 1;
			}
	        $record->spin_id 		= $request->spin_id;
			$record->kategori 		= $request->kategori;
			$record->kategori_id 	= $request->kategori_id;
			$record->deskripsi 		= $request->deskripsi;
	        $record->save();

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'SPIN Kertas Kerja';
		    $log->aktivitas = 'Mengubah SPIN Kertas Kerja dengan Point of Focus (PoF) '.$record->judul;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(KertasKerja $KertasKerja)
    {

    	if($KertasKerja->pemenuhan->count() > 0){
    		return response([
                'status' => true,
            ],500);
    	}else{
	    	$user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'SPIN Kertas Kerja';
		    $log->aktivitas = 'Menghapus SPIN Kertas Kerja dengan Point of Focus (PoF) '.$KertasKerja->judul;
		    $log->user_id = $user->id;
		    $log->save();

	        $KertasKerja->delete();
	        return response()->json([
	            'success' => true
	        ]);
    	}
    }
}
