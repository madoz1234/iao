<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\SpinPembobotanRequest;
use App\Models\Master\SpinPembobotan;
use App\Models\Master\SpinPembobotanDetail;
use App\Models\Master\SpinPembobotanDetailNilai;
use App\Models\SessionLog;

use DB;

class SpinPembobotanController extends Controller
{
    protected $routes = 'master.spin-pembobotan';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'SPIN' => '#', 'Pembobotan' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'version',
                'name' => 'version',
                'label' => 'Version',
                'sortable' => true,
            ],
            [
                'data' => 'deskripsi',
                'name' => 'deskripsi',
                'label' => 'Deskripsi',
                'sortable' => true,
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'className' => 'text-center',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Diperbarui Pada',
                'className' => 'text-center',
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $user = auth()->user();
        $records = SpinPembobotan::select('*');

        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($version = request()->version) {
            $records->where('version', 'like', '%' . $version . '%');
        }
        if ($status = request()->status) {
        	if($status == 2){
	            $records->where('status', 1);
        	}else{
        		$records->where('status', 0);
        	}
        }
        return DataTables::of($records->get())
           ->addColumn('num', function ($record) {
               return request()->start;
           })
           ->addColumn('version', function ($record) {
               return $record->version;
           })
           ->editColumn('status', function ($record) {
              if ($record->status == 1) {
                  $string = '<span class="label label-success">Aktif</span>';
              } elseif ($record->status == 2) {
                  $string = '<span class="label label-danger">Non-Aktif</span>';
              } else {
                  $string = '<span class="label label-warning">Belum Aktif</span>';
              }
                 return $string;
             })
           ->addColumn('deskripsi', function ($record) {
               return $record->readMoreText('deskripsi', 100);
           })
           ->editColumn('created_at', function ($record) {
                if ($record->updated_at) {
                    return $record->updated_at->diffForHumans();
                }else{
                    return $record->created_at->diffForHumans();
                }
           })
           ->addColumn('action', function ($record) use ($user) {
            $buttons = '';
            if(count($record->detail) > 0){
            	if($record->status == 0){
            		$buttons .= $this->makeButton([
		              'type'      => 'detail',
		              'label'     => '<i class="fa fa-plus text-primary"></i>',
		              'tooltip'   => 'Buat Pembobotan',
		              'class'     => 'detil button',
		              'id'        => $record->id.'/buat-pembobotan',
		            ]);
            	}else{
	            	$buttons .= $this->makeButton([
		              'type'      => 'detail',
		              'label'     => '<i class="fa fa-eye text-primary"></i>',
		              'tooltip'   => 'Lihat Pembobotan',
		              'class'     => 'lihat button',
		              'id'        => $record->id.'/lihat-pembobotan',
		            ]);
            	}
            }else{
            	$buttons .= $this->makeButton([
	              'type'      => 'detail',
	              'label'     => '<i class="fa fa-plus text-primary"></i>',
	              'tooltip'   => 'Buat Pembobotan',
	              'class'     => 'detil button',
	              'id'        => $record->id.'/buat-pembobotan',
	            ]);
            }
            if($record->status == 0){
	            $buttons .= $this->makeButton([
	              'type'      => 'edit',
	              'label'     => '<i class="fa fa-pencil text-info"></i>',
	              'tooltip'   => 'Ubah',
	              'class'     => 'm-l edit button',
	              'id'        => $record->id,
	            ]);

	            $buttons .= $this->makeButton([
	              'type'      => 'delete',
	              'label'     => '<i class="fa fa-trash text-danger"></i>',
	              'tooltip'   => 'Hapus',
	              'class'     => 'm-l delete button',
	              'id'        => $record->id,
	            ]);
            }else{
            	 $buttons .= $this->makeButton([
	              'type'      => 'edit',
	              'label'     => '<i class="fa fa-pencil text-info"></i>',
	              'tooltip'   => 'Ubah',
	              'class'     => 'm-l edit button',
	              'id'        => $record->id,
	            ]);
            }
            return $buttons;
          })
           ->rawColumns(['deskripsi', 'action', 'status'])
           ->make(true);
}

    public function index()
    {
        return $this->render('modules.master.pembobotan.index');
    }

    public function create()
    {
        return $this->render('modules.master.pembobotan.create');
    }

    public function store(SpinPembobotanRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record = new SpinPembobotan;
    		if(!isset($request->status)){
				$record->status = 0;
			}else{
    			$record->status = 1;
			}
	        $record->version = $request->version;
			$record->deskripsi = $request->deskripsi;
	        $record->save();

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'SPIN Pembobotan';
		    $log->aktivitas = 'Mengubah SPIN Pembobotan dengan Versi '.$request->version;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function buatPembobotan(SpinPembobotan $id)
    {
        return $this->render('modules.master.pembobotan.buat-pembobot', [
          'record' => $id,
        ]);
    }

    public function lihatPembobotan(SpinPembobotan $id)
    {
        return $this->render('modules.master.pembobotan.detil-pembobot', [
          'record' => $id,
        ]);
    }

    public function simpanData(SpinPembobotanRequest $request)
    {
      DB::beginTransaction();
      try {
          $record = SpinPembobotan::find($request->id);
          $record->status   = $request->status;
          $record->save();
	      if(!empty($request->data_exist)){
	    		$hapus 		= SpinPembobotanDetailNilai::where('pembobot_id', $request->id)
	    										  ->whereNotIn('id', $request->data_exist)
		    									  ->delete();
		   		$hapus_spin = SpinPembobotanDetail::whereNotIn('id', $request->exists)
		    									  ->delete();

	      }
          if($request->detail)
          {
              foreach ($request->detail as $key => $value) {
                  if(!empty($value['id'])){
                      $detail                       = SpinPembobotanDetail::find($value['id']);
                  }else{
                      $detail                       = new SpinPembobotanDetail;
                  }
                  $detail->pof_id               = $value['pof_id'];
                  $detail->spin_pembobot_id     = $record->id;
                  $detail->save();
                  if($request->detailNilai)
                  {
                      foreach($request->detailNilai[$key] as $valuex) {
                      	foreach($valuex as $kiy => $detil){
                          if(!empty($detil['id'])){
                              $dataNilai                       = SpinPembobotanDetailNilai::find($detil['id']);
                          }else{
                              $dataNilai                       = new SpinPembobotanDetailNilai;
                          }   
                          $dataNilai->pembobot_id          = $record->id;
                          $dataNilai->pemenuhan_detail_id  = $detil['pemenuhan_detail_id'];
                          $dataNilai->pembobot_detail_id   = $detail->id;
                          $dataNilai->nilai                = decimal_for_save($detil['nilai']);
                          $dataNilai->save();
                      	}
                      }
                  }
              }
          }

          	$user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'SPIN Pembobotan';
		    $log->aktivitas = 'Mengubah SPIN Pembobotan dengan Versi '.$record->version;
		    $log->user_id = $user->id;
		    $log->save();

          DB::commit();
          return response([
            'status' => true
          ]); 
      }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function edit(SpinPembobotan $SpinPembobotan)
    {
        return $this->render('modules.master.pembobotan.edit', ['record' => $SpinPembobotan]);
    }

    public function update(SpinPembobotanRequest $request, SpinPembobotan $SpinPembobotan)
    {
    	DB::beginTransaction();
        try {
        	$record = SpinPembobotan::find($request->id);
        	if(!isset($request->status)){
				if(count($record->detail) > 0){
					$record->status = 2;
				}else{
	    			$record->status = 0;
					$record->deskripsi = $request->deskripsi;
				}
			}else{
				$record->status = 1;
				$record->deskripsi = $request->deskripsi;
			}
			$record->version = $request->version;
	        $record->save();

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'SPIN Pembobotan';
		    $log->aktivitas = 'Mengubah SPIN Pembobotan dengan Versi '.$record->version;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(SpinPembobotan $SpinPembobotan)
    {
    	if($SpinPembobotan->kertaskerja){
    		return response([
            	'status' => true,
        	],500);
    	}else{
	    	$user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'SPIN Pembobotan';
		    $log->aktivitas = 'Menghapus SPIN Pembobotan dengan Versi '.$SpinPembobotan->version;
		    $log->user_id = $user->id;
		    $log->save();

	        $SpinPembobotan->delete();
	        return response()->json([
	            'success' => true
	        ]);
    	}
    }
}
