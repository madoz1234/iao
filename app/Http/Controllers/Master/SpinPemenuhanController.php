<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\SpinPemenuhanRequest;
use App\Models\Master\SpinPof;
use App\Models\Master\SpinPemenuhan;
use App\Models\Master\SpinPemenuhanDetail;
use App\Models\SessionLog;

use DB;

class SpinPemenuhanController extends Controller
{
    protected $routes = 'master.spin-pemenuhan';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'SPIN' => '#', 'Unsur Pemenuhan' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'judul',
                'name' => 'judul',
                'label' => 'Point of Focus (PoF)',
                'width' => '500px',
                'sortable' => true,
            ],
            [
                'data' => 'deskripsi',
                'name' => 'deskripsi',
                'label' => 'Unsur Pemenuhan',
                'width' => '500px',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Diperbarui Pada',
                'className' => 'text-center',
                'width' => '100px',
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '70px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid(Request $request)
    {
        $user = auth()->user();
        $records = SpinPemenuhan::with('detail', 'pof')->select('*');

        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($pof = $request->pof) {
            $records->whereHas('pof', function ($query) use ($pof){
                $query->where('judul', 'like', '%' . $pof . '%');
            });
        }
        if ($detail = $request->deskripsi) {
            $records->whereHas('detail', function ($query) use ($detail){
                $query->where('deskripsi', 'like', '%' . $detail . '%');
            });
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('judul', function ($record) {
                   return $record->pof ? readMoreText($record->pof->judul) : '-';
               })
               ->addColumn('deskripsi', function ($record) {
                  $string ='';
                  $string .='<ul class="list list-more1" data-display="3">';
                  foreach ($record->detail as $key => $value) {
                      $string .='<li class="item">'.readMoreText($value->deskripsi, 100).'</li>';
                  }
                  $string .='</ul>';
                    return $string;
               })
               ->editColumn('created_at', function ($record) {
                    if ($record->updated_at) {
	                    return $record->updated_at->diffForHumans();
	                }else{
	                    return $record->created_at->diffForHumans();
	                }
               })
               ->addColumn('action', function ($record) use ($user) {
                    $buttons = '';
		    		$buttons .= $this->makeButton([
                        'type' => 'edit',
                        'id'   => $record->id,
                    ]);
                    $buttons .= $this->makeButton([
                        'type' => 'delete',
                        'id'   => $record->id,
                    ]);
                   return $buttons;
               })
               ->rawColumns(['deskripsi', 'judul', 'action'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.unsur-pemenuhan.index');
    }

    public function create()
    {
        return $this->render('modules.master.unsur-pemenuhan.create');
    }

    public function store(SpinPemenuhanRequest $request)
    {
      DB::beginTransaction();
      try {
        $record = new SpinPemenuhan;
          $record->pof_id   = $request->pof_id;
          if ($record->save()) {
            $pof = SpinPof::find($record->pof_id);
            $pof->status = 1;
            // dd($record->pof_id);
            $pof->save();
          }

            $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'SPIN Unsur Pemenuhan';
		    $log->aktivitas = 'Membuat SPIN Unsur Pemenuhan dengan Point of Focus (PoF) '.$record->pof->judul;
		    $log->user_id = $user->id;
		    $log->save();
          // $record->save();
          $record->saveDetail($request->detail);
        DB::commit();
          return response([
            'status' => true
          ]); 
      }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    // public function show(d $user)
    // {
    //     return $user->toJson();
    // }

    public function edit(SpinPemenuhan $SpinPemenuhan)
    {
        return $this->render('modules.master.unsur-pemenuhan.edit', ['record' => $SpinPemenuhan]);
    }

    public function update(SpinPemenuhanRequest $request, SpinPemenuhan $SpinPemenuhan)
    {
      DB::beginTransaction();
        try {
          if(!empty($request->exists)){
            $hapus = SpinPemenuhanDetail::whereNotIn('id', $request->exists)
                           ->where('spin_pemenuhan_id', $request->id)
                                   ->delete();
          }

          $record = SpinPemenuhan::find($request->id);
          $record->pof_id   = $request->pof_id;
          // $record->save();
          if ($record->save()) {
            $pof = SpinPof::find($record->pof_id);
            $pof->status = 1;
            $pof->save();
          }

          	$user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'SPIN Unsur Pemenuhan';
		    $log->aktivitas = 'Mengubah SPIN Unsur Pemenuhan dengan Point of Focus (PoF) '.$record->pof->judul;
		    $log->user_id = $user->id;
		    $log->save();

          $record->updateDetail($request->detail);
        DB::commit();
          return response([
            'status' => true
          ]); 
      }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(SpinPemenuhan $SpinPemenuhan)
    {
    	if($SpinPemenuhan->detail->count() > 0){
    		return response([
                'status' => true,
            ],500);
    	}else{
    		$pof = SpinPof::find($SpinPemenuhan->pof_id);
            $pof->status = 0;
            $pof->save();
	    	$user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Master';
		    $log->sub = 'SPIN Unsur Pemenuhan';
		    $log->aktivitas = 'Menghapus SPIN Unsur Pemenuhan dengan Point of Focus (PoF) '.$SpinPemenuhan->pof->judul;
		    $log->user_id = $user->id;
		    $log->save();

	        $SpinPemenuhan->delete();
	        return response()->json([
	            'success' => true
	        ]);
    	}
    }
}
