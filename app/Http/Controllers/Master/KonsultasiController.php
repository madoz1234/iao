<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\KonsultasiRequest;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\Master\Konsultasi;
use App\Models\SessionLog;

use DB;

class KonsultasiController extends Controller
{
    protected $routes = 'master.kegiatan-konsultasi';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'Kegiatan Konsultasi' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Kegiatan Konsultasi',
                'width' => '400px',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Diperbarui Pada',
                'width' => '200px',
                'className' => 'text-center',
                'sortable' => true,
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = Konsultasi::select('*');

        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($name = request()->name) {
            $records->where('nama', 'like', '%' . $name . '%');
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('nama', function ($record) {
                   return $record->nama;
               })
               ->editColumn('created_at', function ($record) {
                    if ($record->updated_at) {
	                    return $record->updated_at->diffForHumans();
	                }else{
	                    return $record->created_at->diffForHumans();
	                }
               })
               ->addColumn('action', function ($record) {
                    $buttons = '';
                    $cari = RencanaAuditDetail::where('konsultasi_id', $record->id)->where('tipe', 1)->get()->count();
					if($cari > 0){
			        	$buttons = '';
			    	}else{
			    		$buttons .= $this->makeButton([
	                        'type' => 'edit',
	                        'id'   => $record->id,
	                   ]);
	                   $buttons .= $this->makeButton([
	                        'type' => 'delete',
	                        'id'   => $record->id,
	                    ]);
			    	}
                   return $buttons;
               })
               ->rawColumns(['alamat', 'action'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.konsultasi.index');
    }

    public function create()
    {
        return $this->render('modules.master.konsultasi.create');
    }

    public function store(KonsultasiRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record = new Konsultasi;
	        $record->fill($request->all());
	        $record->save();
	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    // public function show(Konsultasi $user)
    // {
    //     return $user->toJson();
    // }

    public function edit(Konsultasi $KegiatanKonsultasi)
    {
        return $this->render('modules.master.konsultasi.edit', ['record' => $KegiatanKonsultasi]);
    }

    public function update(KonsultasiRequest $request, Konsultasi $KegiatanKonsultasi)
    {
    	DB::beginTransaction();
        try {
        	$record = Konsultasi::find($request->id);
	        $record->fill($request->all());
	        $record->save();
	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(Konsultasi $KegiatanKonsultasi)
    {
        $cari = RencanaAuditDetail::where('konsultasi_id', $KegiatanKonsultasi->id)->where('tipe', 1)->get()->count();
		if($cari > 0){
        	return response([
            	'status' => true,
        	],500);
    	}else{
    		$KegiatanKonsultasi->delete();
        	return response([
            	'status' => true,
	        ],200);
    	}
    }
}
