<?php

namespace App\Http\Controllers\RKIA;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Models\Master\Edisi;
use App\Http\Requests\KegiatanAudit\Rencana\RencanaRequest;
use App\Http\Requests\KegiatanAudit\Rencana\DetailRequest;
use App\Http\Requests\KegiatanAudit\Rencana\RejectRequest;
use App\Http\Requests\KegiatanAudit\Rencana\UploadRequest;
use App\Models\KegiatanAudit\Rencana\RencanaAudit;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetailUser;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit;
use App\Imports\RencanaAuditPkat;
use Illuminate\Support\Facades\Storage;
use App\Models\SessionLog;
use App\Models\Notification;
use App\Models\Auths\User;
use GuzzleHttp\Client;

use DB;
use Excel;
use Carbon;
use PDF;

class RKIAController extends Controller
{
    protected $routes = 'rkia.rkia';
    protected $rencana;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct(RencanaAudit $rencana)
    {
    	$this->rencana = $rencana;
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['RKIA' => '#']);
        // Header Grid Datatable
        $this->listStructs = [
        	'listStruct' => [
		        	[
		                'data' => 'num',
		                'name' => 'num',
		                'label' => '#',
		                'orderable' => false,
		                'searchable' => false,
		                'className' => 'text-center',
		                'width' => '20px',
		            ],
		            /* --------------------------- */
		            [
		                'data' => 'tahun',
		                'name' => 'tahun',
		                'label' => 'Tahun',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'object_audit',
		                'name' => 'object_audit',
		                'label' => 'Rencana Kerja',
		                'className' => 'text-center',
		                'sortable' => false,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'created_by',
		                'name' => 'created_by',
		                'label' => 'Dibuat Oleh',
		                'className' => 'text-center',
		                'sortable' => false,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'created_at',
		                'name' => 'created_at',
		                'label' => 'Diperbarui Pada',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'revisi',
		                'name' => 'revisi',
		                'label' => 'Revisi',
		                'className' => 'text-center',
		                'sortable' => false,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'status',
		                'name' => 'status',
		                'label' => 'Status',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '100px',
		                'className' => 'text-center'
		            ],
		            [
		                'data' => 'action',
		                'name' => 'action',
		                'label' => 'Aksi',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '80px',
		                'className' => 'text-center'
		            ],
        	],
        	'listStruct2' => [
		        	[
		                'data' => 'num',
		                'name' => 'num',
		                'label' => '#',
		                'orderable' => false,
		                'searchable' => false,
		                'className' => 'text-center',
		                'width' => '20px',
		            ],
		            /* --------------------------- */
		            [
		                'data' => 'tahun',
		                'name' => 'tahun',
		                'label' => 'Tahun',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'object_audit',
		                'name' => 'object_audit',
		                'label' => 'Rencana Kerja',
		                'className' => 'text-center',
		                'sortable' => false,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'created_by',
		                'name' => 'created_by',
		                'label' => 'Dibuat Oleh',
		                'className' => 'text-center',
		                'sortable' => false,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'created_at',
		                'name' => 'created_at',
		                'label' => 'Diperbarui Pada',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'revisi',
		                'name' => 'revisi',
		                'label' => 'Revisi',
		                'className' => 'text-center',
		                'sortable' => false,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'status',
		                'name' => 'status',
		                'label' => 'Status',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '100px',
		                'className' => 'text-center'
		            ],
		            [
		                'data' => 'action',
		                'name' => 'action',
		                'label' => 'Aksi',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '80px',
		                'className' => 'text-center'
		            ],
        	],
        	'listStruct3' => [
		        	[
		                'data' => 'num',
		                'name' => 'num',
		                'label' => '#',
		                'orderable' => false,
		                'searchable' => false,
		                'className' => 'text-center',
		                'width' => '20px',
		            ],
		            /* --------------------------- */
		            [
		                'data' => 'tahun',
		                'name' => 'tahun',
		                'label' => 'Tahun',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'object_audit',
		                'name' => 'object_audit',
		                'label' => 'Rencana Kerja',
		                'className' => 'text-center',
		                'sortable' => false,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'created_by',
		                'name' => 'created_by',
		                'label' => 'Dibuat Oleh',
		                'className' => 'text-center',
		                'sortable' => false,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'created_at',
		                'name' => 'created_at',
		                'label' => 'Diperbarui Pada',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'revisi',
		                'name' => 'revisi',
		                'label' => 'Revisi',
		                'className' => 'text-center',
		                'sortable' => false,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'status',
		                'name' => 'status',
		                'label' => 'Status',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '100px',
		                'className' => 'text-center'
		            ],
		            [
		                'data' => 'action',
		                'name' => 'action',
		                'label' => 'Aksi',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '80px',
		                'className' => 'text-center'
		            ],
        	],
        ];
    }

    public function grid()
    {
    	$user = auth()->user();
    	if($user->hasJabatan(['auditor','SVP - Internal Audit','President Director','Secretary'])){
        	$records = RencanaAudit::where('tipe', 0)
        			  ->where('status', 0)
		        	  ->when($tahun = request()->tahun, function ($q) use ($tahun) {
            			 $q->where('tahun', 'like', '%'.$tahun.'%');
        			  })->select('*');

        	if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at','desc');
	        }
       	}else{
       		$records = RencanaAudit::where('id', 0);
       	}

        return DataTables::of($records->get())
           ->addColumn('num', function ($record) {
               return request()->start;
           })
           ->addColumn('tahun', function ($record) {
               return $record->tahun;
           })
           ->addColumn('revisi', function ($record) {
               return '<span class="label label-info">'.$record->revisi.'</span>';
           })
           ->addColumn('object_audit', function ($record) use ($user) {
               $buttons = '';
               if($user->hasJabatan('auditor')){
	               $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-book text-primary"></i>',
	                    'tooltip' 	=> 'Rencana Kerja',
	                    'class' 	=> 'objectaudit button',
	                    'id'   		=> $record->id,
	               ]);
               	}else{
               		$buttons .= '';
               	}
               return $buttons;
           })
		   ->editColumn('created_by', function ($record) {
               return $record->creatorName();
           })
           ->editColumn('created_at', function ($record) {
           		if($record->detailrencana){
	               return $record->updated_at->diffForHumans();
           		}else{
           			return $record->created_at->diffForHumans();
           		}
           })
           ->editColumn('status', function ($record) {
	           	if($record->status_ditolak == 1){
	               return '<span class="label label-warning">Ditolak</span>';
	           }else{
	           	   return '<span class="label label-default">Draft</span>';
	           }
           })
           ->addColumn('action', function ($record) use ($user) {
                $buttons = '';
                if($record->revisi == 0){
	                if($user->hasJabatan(['auditor'])){
	                	if(count($record->detailrencana) > 0){
	                		if($record->ket_dirut || $record->ket_svp){
	                			$buttons .='';
	                		}else{
		                		$buttons .= $this->makeButton([
				                    'type'  => 'edit',
				                    'class' => 'disabled',
				                    'id'    => $record->id,
				                ]);
	                		}
	                	}else{
	                		if($record->ket_dirut || $record->ket_svp){
	                			$buttons .='';
	                		}else{
					       		$buttons .= $this->makeButton([
				                    'type' => 'edit',
				                    'id'   => $record->id,
				                ]);
	                		}
	                	}
	                	if($record->ket_dirut || $record->ket_svp){
                			$buttons .='';
                		}else{
				       		$buttons .= $this->makeButton([
			                    'type' => 'delete',
			                    'id'   => $record->id,
			                ]);
                		}
	                }else{
	                	$buttons .='';
	                }
                }else{
                	$buttons .='';
                }

                return $buttons;
           })
           ->rawColumns(['action','object_audit','status','revisi'])
           ->make(true);
    }

    public function onProgress()
    {
    	$user = auth()->user();
    	if($user->hasJabatan(['SVP - Internal Audit','President Director','Secretary','auditor'])){
	        $records = RencanaAudit::where('tipe', 0)
	        			  ->whereIn('status', [1,2,3])
			        	  ->when($tahun = request()->tahun, function ($q) use ($tahun) {
	            			 $q->where('tahun', 'like', '%'.$tahun.'%');
	        			  })->select('*');

	        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at','desc');
	        }
		}else{
			$records = RencanaAudit::where('id', 0);
		}

        return DataTables::of($records->get())
           ->addColumn('num', function ($record) {
               return request()->start;
           })
           ->addColumn('tahun', function ($record) {
               return $record->tahun;
           })
           ->addColumn('revisi', function ($record) {
               return '<span class="label label-info">'.$record->revisi.'</span>';
           })
           ->addColumn('object_audit', function ($record) use ($user) {
               $buttons = '';
               $buttons .= $this->makeButton([
                    'type' 		=> 'edit',
                    'label' 	=> '<i class="fa fa-book text-primary"></i>',
                    'tooltip' 	=> 'Rencana Kerja',
                    'class' 	=> 'detil button',
                    'id'   		=> $record->id,
               ]);
               return $buttons;
           })
		   ->editColumn('created_by', function ($record) {
               return $record->creatorName();
           })
           ->editColumn('created_at', function ($record) {
               	if($record->detailrencana){
	               return $record->updated_at->diffForHumans();
           		}else{
           			return $record->created_at->diffForHumans();
           		}
           })
           ->editColumn('status', function ($record) {
           		if($record->status == 1){
           			if($record->ket_dirut){
           				return '<span class="label label-warning">Ditolak</span>';
           			}else{
	               		return '<span class="label label-info">Waiting Approval SVP</span>';
           			}
           		}elseif($record->status == 2){
           			return '<span class="label label-info">Waiting Approval Dirut</span>';
           		}elseif($record->status == 3){
           			return '<span class="label label-info">Waiting Upload</span>';
           		}else{
           			return '<span class="label label-info">Historis</span>';
           		}
           })
           ->addColumn('action', function ($record) use ($user) {
                $buttons = '';
                if($user->hasJabatan(['SVP - Internal Audit'])){
                	if($record->status == 1){
                		$buttons .= $this->makeButtons([
	               			'type' 		=> 'edit',
	               			'label' 	=> '<i class="fa fa-check text-primary"></i>',
	               			'tooltip' 	=> 'Buat Approval',
	               			'class' 	=> 'approve-svp button',
	               			'id'   		=> $record->id,
	               			'detail'   	=> $record->tahun,
	               		]);
	               		$buttons .= '&nbsp;&nbsp;&nbsp;';
                		$buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-print text-primary"></i>',
		                    'tooltip' 	=> 'Cetak',
		                    'class' 	=> 'cetak button',
		                    'id'   		=> $record->id,
		                ]);
                	}else{
                		$buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-print text-primary"></i>',
		                    'tooltip' 	=> 'Cetak',
		                    'class' 	=> 'cetak button',
		                    'id'   		=> $record->id,
		                ]);
                	}
                }elseif($user->hasJabatan(['President Director'])){
                	if($record->status == 2){
                		$buttons .= $this->makeButtons([
	               			'type' 		=> 'edit',
	               			'label' 	=> '<i class="fa fa-check text-primary"></i>',
	               			'tooltip' 	=> 'Buat Approval',
	               			'class' 	=> 'approve-dirut button',
	               			'id'   		=> $record->id,
	               			'detail'   	=> $record->tahun,
	               		]);
                		$buttons .= '&nbsp;&nbsp;&nbsp;';
                		$buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-print text-primary"></i>',
		                    'tooltip' 	=> 'Cetak',
		                    'class' 	=> 'cetak button',
		                    'id'   		=> $record->id,
		                ]);
                	}else{
                		$buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-print text-primary"></i>',
		                    'tooltip' 	=> 'Cetak',
		                    'class' 	=> 'cetak button',
		                    'id'   		=> $record->id,
		                ]);
                	}
                }elseif($user->hasJabatan(['Secretary'])){
                	if($record->status == 3){
                		$buttons .= $this->makeButtons([
	               			'type' 		=> 'edit',
	               			'label' 	=> '<i class="fa fa-upload text-primary"></i>',
	               			'tooltip' 	=> 'Upload Dokumen',
	               			'class' 	=> 'upload-dokumen button',
	               			'id'   		=> $record->id,
	               			'detail'   	=> $record->tahun,
	               		]);
                		$buttons .= '&nbsp;&nbsp;&nbsp;';
                		$buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-print text-primary"></i>',
		                    'tooltip' 	=> 'Cetak',
		                    'class' 	=> 'cetak button',
		                    'id'   		=> $record->id,
		                ]);
                	}else{
                		$buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-print text-primary"></i>',
		                    'tooltip' 	=> 'Cetak',
		                    'class' 	=> 'cetak button',
		                    'id'   		=> $record->id,
		                ]);
                	}
                }else{
                	$buttons = '';
                }
                return $buttons;
           })
           ->rawColumns(['action','object_audit','status','revisi'])
           ->make(true);
    }

    public function historis()
    {
    	$user = auth()->user();
    	if($user->hasJabatan(['SVP - Internal Audit', 'President Director','auditor','Secretary'])){
        	$records = RencanaAudit::where('tipe', 0)
        			  ->whereIn('status_revisi', [1,2])
		        	  ->when($tahun = request()->tahun, function ($q) use ($tahun) {
            			 $q->where('tahun', 'like', '%'.$tahun.'%');
        			  })->select('*');

        	if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at','desc');
	        }
        }else{
        	$records = RencanaAudit::where('id', 0);
        }

        return DataTables::of($records->get())
           ->addColumn('num', function ($record) {
               return request()->start;
           })
           ->addColumn('tahun', function ($record) {
               return $record->tahun;
           })
           ->addColumn('revisi', function ($record) {
           		if($record->revisi > 0){
	               return '<span class="label label-info">'.($record->revisi).'</span>';
	            }else{
	           		return '<span class="label label-info">'.$record->revisi.'</span>';
	            }
           })
           ->addColumn('object_audit', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
                    'type' 		=> 'edit',
                    'label' 	=> '<i class="fa fa-book text-primary"></i>',
                    'tooltip' 	=> 'Rencana Kerja',
                    'class' 	=> 'detil button',
                    'id'   		=> $record->id,
               ]);
               return $buttons;
           })
		   ->editColumn('created_by', function ($record) {
               return $record->creatorName();
           })
           ->editColumn('created_at', function ($record) {
               	if($record->detailrencana){
	               return $record->updated_at->diffForHumans();
           		}else{
           			return $record->created_at->diffForHumans();
           		}
           })
           ->editColumn('status', function ($record) {
               return '<span class="label label-success">Completed</span>';
           })
           ->addColumn('draft', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
                    'type' 		=> 'edit',
                    'label' 	=> '<i class="fa fa-file-pdf-o text-primary"></i>',
                    'tooltip' 	=> 'Draft Final',
                    'class' 	=> 'draft final button',
                    'id'   		=> $record->id,
               ]);

               return $buttons;
           })
           ->addColumn('action', function ($record) use ($user) {
               $buttons = '';
               $buttons .= $this->makeButton([
                    'type' 		=> 'edit',
                    'label' 	=> '<i class="fa fa-print text-primary"></i>',
                    'tooltip' 	=> 'Cetak',
                    'class' 	=> 'cetak button',
                    'id'   		=> $record->id,
               ]);
               $buttons .= '&nbsp;&nbsp;&nbsp;&nbsp;';
               $buttons .= $this->makeButton([
                    'type' 		=> 'edit',
                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
                    'tooltip' 	=> 'Rencana Kerja',
                    'class' 	=> 'download-rkia button',
                    'id'   		=> $record->id,
               ]);
               $buttons .= '&nbsp;&nbsp;&nbsp;&nbsp;';
               if($record->status == 4 && $record->status_revisi == 1){
	               	if($user->hasJabatan('auditor')){
			               $buttons .= $this->makeButton([
			                    'type' 		=> 'edit',
			                    'label' 	=> '<i class="fa fa-retweet text-primary"></i>',
			                    'tooltip' 	=> 'Buat Revisi',
			                    'class' 	=> 'revisi button',
			                    'id'   		=> $record->id,
			               ]);
	               	}else{
	               		$buttons .= '';
	               	}
               }
               return $buttons;
           })
           ->rawColumns(['action','object_audit','status','revisi','draft'])
           ->make(true);
    }

    public function index()
    {
    	$tipe = 0;
    	$user = auth()->user();
    	if($user->hasJabatan(['auditor','SVP - Internal Audit','President Director','Secretary'])){
        	$nbaru = RencanaAudit::where('tipe', 0)
        			  ->where('status', 0)
		        	  ->get()->count();

        	$nprog = RencanaAudit::where('tipe', 0)
        			  ->whereIn('status', [1,2,3])
		        	  ->get()->count();
       	}else{
       		$nbaru = RencanaAudit::where('id', 0)->get()->count();
       		$nprog = RencanaAudit::where('id', 0)->get()->count();
       	}
        return $this->render('modules.rkia.index', [
        	'mockup' => true,
        	'structs' => $this->listStructs,
        	'tipe' => $tipe,
        	'nbaru' => $nbaru,
        	'nprog' => $nprog,
        ]);
    }

    public function create()
    {
    	$this->setBreadcrumb(['RKIA' => '#', 'Rencana Kerja' => '#']);
        return $this->render('modules.rkia.create');
    }

    public function edit($id)
    {
    	$cari = RencanaAudit::find($id);
        return $this->render('modules.rkia.edit', ['record' => $cari]);
    }

    public function revisi(RencanaAudit $id)
    {
        return $this->render('modules.rkia.revisi', ['record' => $id]);
    }

    public function detil(RencanaAudit $id)
    {
        return $this->render('modules.rkia.detil', ['record' => $id]);
    }

    public function reject(RencanaAudit $id)
    {
        return $this->render('modules.rkia.reject', [
        	'record' => $id,
        ]);
    }

    public function reject2(RencanaAudit $id)
    {
        return $this->render('modules.rkia.reject2', [
        	'record' => $id,
        ]);
    }

    public function uploadFinal(RencanaAudit $id)
    {
        return $this->render('modules.rkia.upload', [
        	'record' => $id,
        ]);
    }

    public function download()
    {
    	$file= "template-xls/template.xlsx";
		return response()->download($file, 'Template Rencana Audit.xlsx');
    }

    public function store(RencanaRequest $request)
    {
    	$user = auth()->user();
    	DB::beginTransaction();
    	try {
    		$record 				= new RencanaAudit;
	        $record->tahun 			= $request->tahun;
	        $record->tipe 			= 0;
	        $record->created_by 	= $user->id;
	        $record->status_ditolak = 0;
	        $record->created_at 	= date('Y-m-d H:i:s');
	        $record->save();

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Kegiatan Audit Rencana';
		    $log->sub = 'RKIA';
		    $log->aktivitas = 'Membuat RKIA dengan Tahun '.$request->tahun;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function updateObject(RencanaAudit $id)
    {
    	$this->setBreadcrumb(['RKIA' => '#', 'Rencana Kerja' => '#']);
        return $this->render('modules.rkia.objectaudit', ['record' => $id]);
    }

    public function saveObject(DetailRequest $request)
    {
    	$user = auth()->user();
        DB::beginTransaction();
    	try {
    		if($request->detail){
    			$audit = filter_array($request->detail, '0');
    			$konsul = filter_array($request->detail, '1');
    			$lain = filter_array($request->detail, '2');

    			$cek_edisi = Edisi::where('status', 1)->first();
    			if($cek_edisi){
    			}else{
    				return response()->json([
		                'status' => 'false',
		                'message' => 'Mohon Lengkapi Master Edisi.'
		            ],402);
    			}
    			// if(count($audit) > 0){
    			// 	if(checkunique($audit, ['tipe_object', 'object_id']) == true){
		    	// 	}else{
		    	// 		return response()->json([
			    //             'status' => 'false',
			    //             'message' => 'Ada beberapa Kategori dan Object audit yang sama.'
			    //         ],402);
		    	// 	}
    			// }

    			// if(count($konsul) > 0){
    			// 	if(checkunique($konsul, ['konsultasi_id', 'tipe']) == true){
		    	// 	}else{
		    	// 		return response()->json([
			    //             'status' => 'false',
			    //             'message' => 'Ada beberapa Kategori Konsultasi yang sama.'
			    //         ],402);
		    	// 	}
    			// }

    			// if(count($lain) > 0){
    			// 	if(checkunique($lain, ['lain', 'tipe']) == true){
		    	// 	}else{
		    	// 		return response()->json([
			    //             'status' => 'false',
			    //             'message' => 'Ada beberapa Kategori Kegiatan Lain - Lain yang sama.'
			    //         ],402);
		    	// 	}
    			// }
	    		$record 						= RencanaAudit::find($request->id);
	    		if($request->status == 1){
		    		$record->status 			= 1;
		    		$record->created_at 		= date('Y-m-d H:i:s');
		    		$record->created_by 		= $user->id;
		    		$record->ket_svp 			= null;
		    		$record->ket_dirut 			= null;
		    		$record->status_ditolak 	= 0;

		    		$update_notif = Notification::where('parent_id', $record->id)->where('modul', 'audit-reguler')->where('stage', 0)->get();
			        foreach ($update_notif as $key => $vil) {
			        	$data_notif = Notification::find($vil->id);
				        $data_notif->status =0;
				        $data_notif->save();
			        }
    				$userz = User::whereHas('roles', function($u){
											$u->where('name', 'SVP - Internal Audit');
										})->get();
    				foreach ($userz as $key => $value) {
    					$notif = new Notification;
					    $notif->user_id = $value->id;
					    $notif->parent_id = $record->id;
					    $notif->url = route($this->routes.'.index');
					    $notif->modul = 'audit-reguler';
					    $notif->stage = 1;
					    $notif->keterangan = 'Approval RKIA Tahun '.$record->tahun;
					    $notif->status = 1;
					    $notif->save();
    				}
	    		}else{
	    			$record->status 			= 0;
	    			$record->created_at 		= date('Y-m-d H:i:s');
	    			$record->ket_svp 			= null;
	    			$record->ket_dirut 			= null;
	    			$record->status_ditolak 	= 0;
	    			$record->created_by 		= $user->id;
	    		}
		        $record->save();
		        if(!is_null($request->exists)){
		    		$hapus = RencanaAuditDetail::where('rencana_id', $request->id)
		    										  ->whereNotIn('id', $request->exists)
			    									  ->delete();
		        }

		        $record->saveDetail($request->detail);
    		}else{
	    		return response()->json([
	                'status' => 'false',
	                'message' => 'Mohon inputkan data terlebih dahulu'
	            ],402);
    		}
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function saveUpload(UploadRequest $request)
    {
        DB::beginTransaction();
    	try {
    		$record 						= RencanaAudit::find($request->id);
	    	$path 							= $request->final->store('rencana-audit', 'public');
			$record->filename 				= $request->final->getClientOriginalName();
			$record->bukti 					= $path;
			$record->status 				= 4;

      // [NOTE] adding dms needed
      $record->dms_object_type = 'RKIA';
      $record->dms_update = 1;

			if($record->status_revisi == 0){
				$record->status_revisi = 1;
			}
	        $record->save();

	        $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'audit-reguler')->where('stage', 3)->get();
	        foreach ($update_notif as $key => $vil) {
	        	$data_notif = Notification::find($vil->id);
		        $data_notif->status =0;
		        $data_notif->save();
	        }

			$userz = User::whereHas('roles', function($u){
									$u->where('name', 'auditor');
								})->get();
			foreach ($userz as $key => $value) {
				$notif = new Notification;
			    $notif->user_id = $value->id;
			    $notif->parent_id = $record->id;
			    $notif->url = route($this->routes.'.index');
			    $notif->modul = 'audit-reguler';
			    $notif->stage = 4;
			    $notif->keterangan = 'Surat Penugasan Tahun '.$record->tahun;
			    $notif->status = 1;
			    $notif->save();
			}

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function saveReject(RejectRequest $request)
    {
        DB::beginTransaction();
    	try {
    		$record 						= RencanaAudit::find($request->id);
	    	$record->status 				= 0;
	    	$record->ket_svp 				= $request->ket_svp;
	    	$record->ket_dirut 				= null;
	    	$record->status_ditolak 		= 1;
	        $record->save();

	        $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'audit-reguler')->where('stage', 1)->get();
	        foreach ($update_notif as $key => $vil) {
	        	$data_notif = Notification::find($vil->id);
		        $data_notif->status =0;
		        $data_notif->save();
	        }

	        $userz = User::whereHas('roles', function($u){
									$u->where('name', 'auditor');
								})->get();
			foreach ($userz as $key => $value) {
				$notif = new Notification;
			    $notif->user_id = $value->id;
			    $notif->parent_id = $record->id;
			    $notif->url = route($this->routes.'.index');
			    $notif->modul = 'audit-reguler';
			    $notif->stage = 0;
			    $notif->keterangan = 'Reject SVP RKIA Tahun '.$record->tahun;
			    $notif->status = 1;
			    $notif->save();
			}

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Kegiatan Audit Rencana';
		    $log->sub = 'RKIA';
		    $log->aktivitas = 'Menolak RKIA dengan Tahun '.$record->tahun;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }


    public function saveReject2(RejectRequest $request)
    {
        DB::beginTransaction();
    	try {
    		$record 						= RencanaAudit::find($request->id);
	    	$record->status 				= 0;
	    	$record->ket_dirut 				= $request->ket_svp;
	    	$record->status_ditolak 		= 1;
	        $record->save();

	        $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'audit-reguler')->where('stage', 1)->get();
	        foreach ($update_notif as $key => $vil) {
	        	$data_notif = Notification::find($vil->id);
		        $data_notif->status =0;
		        $data_notif->save();
	        }

	        $userz = User::whereHas('roles', function($u){
									$u->where('name', 'auditor');
								})->get();
			foreach ($userz as $key => $value) {
				$notif = new Notification;
			    $notif->user_id = $value->id;
			    $notif->parent_id = $record->id;
			    $notif->url = route($this->routes.'.index');
			    $notif->modul = 'audit-reguler';
			    $notif->stage = 0;
			    $notif->keterangan = 'Reject SVP RKIA Tahun '.$record->tahun;
			    $notif->status = 1;
			    $notif->save();
			}

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Kegiatan Audit Rencana';
		    $log->sub = 'RKIA';
		    $log->aktivitas = 'Menolak RKIA dengan Tahun '.$record->tahun;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function saveRevisi(Request $request)
    {
    	$user = auth()->user();
        DB::beginTransaction();
    	try {
    		$record 							= RencanaAudit::find($request->id);
	      	$parent 							= $record->id;
		    if(!is_null($record->parent_id)){
		       $parent = $record->parent_id;
		    }

	        $duplicate 							= $record->replicate();
	      	$duplicate->revisi 					= $record->revisi + 1;
	      	$duplicate->status_revisi 			= 0;
	      	$duplicate->status 					= 0;
	      	$duplicate->created_by 				= $user->id;
	      	$duplicate->bukti 					= null;
	      	$duplicate->filename 				= null;
	      	$duplicate->parent_id 				= $parent;
	      	$duplicate->save();

	        $record->status_revisi = 2;
	        $record->parent_id = $parent;
	        $record->save();

	        foreach($record->detailrencana as $row) {
	        	if($row->penugasan){
			        $new = $row->replicate();
			        $new->rencana_id = $duplicate->id;
			        $new->flag = 2;
			        $new->save();
	        	}else{
	        		$new = $row->replicate();
			        $new->rencana_id = $duplicate->id;
			        $new->save();

			        $detail = RencanaAuditDetail::find($row->id);
			        $detail->flag = 2;
			        $detail->save();
	        	}
		        foreach($row->detailuser as $rows){
			        $news = $rows->replicate();
			        $news->detail_id = $new->id;
			        $news->save();
		        }
		     }

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Kegiatan Audit Rencana';
		    $log->sub = 'RKIA';
		    $log->aktivitas = 'Merevisi RKIA dengan Tahun '.$record->tahun;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function approval($id, $tipe)
    {
    	$user = auth()->user();
    	DB::beginTransaction();
        try {
        	$record = RencanaAudit::find($id);
        	$record->ket_dirut = null;
        	if($tipe == 1){
		        $record->status 			= 2;

		        $user = auth()->user();
		    	$log = new SessionLog;
			    $log->modul = 'Kegiatan Audit Rencana';
			    $log->sub = 'RKIA';
			    $log->aktivitas = 'Menyetujui RKIA dengan Tahun '.$record->tahun;
			    $log->user_id = $user->id;
			    $log->save();

			    $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'audit-reguler')->where('stage', 1)->get();
		        foreach ($update_notif as $key => $vil) {
		        	$data_notif = Notification::find($vil->id);
			        $data_notif->status =0;
			        $data_notif->save();
		        }

				$userz = User::whereHas('roles', function($u){
										$u->where('name', 'President Director');
									})->get();
				foreach ($userz as $key => $value) {
					$notif = new Notification;
				    $notif->user_id = $value->id;
				    $notif->parent_id = $record->id;
				    $notif->url = route($this->routes.'.index');
				    $notif->modul = 'audit-reguler';
				    $notif->stage = 2;
				    $notif->keterangan = 'Approval RKIA Tahun '.$record->tahun;
				    $notif->status = 1;
				    $notif->save();
				}

        	}else{
        		$user = auth()->user();
		    	$log = new SessionLog;
			    $log->modul = 'Kegiatan Audit Rencana';
			    $log->sub = 'RKIA';
			    $log->aktivitas = 'Menyetujui RKIA dengan Tahun '.$record->tahun;
			    $log->user_id = $user->id;
			    $log->save();
		        $record->status 			= 3;

		        $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'audit-reguler')->where('stage', 2)->get();
		        foreach ($update_notif as $key => $vil) {
		        	$data_notif = Notification::find($vil->id);
			        $data_notif->status =0;
			        $data_notif->save();
		        }

				$userz = User::whereHas('roles', function($u){
										$u->where('name', 'Secretary');
									})->get();
				foreach ($userz as $key => $value) {
					$notif = new Notification;
				    $notif->user_id = $value->id;
				    $notif->parent_id = $record->id;
				    $notif->url = route($this->routes.'.index');
				    $notif->modul = 'audit-reguler';
				    $notif->stage = 3;
				    $notif->keterangan = 'Upload Dokumen RKIA Tahun '.$record->tahun;
				    $notif->status = 1;
				    $notif->save();
				}
        	}
	        $record->save();
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function update(RencanaRequest $request)
    {
    	$user = auth()->user();
    	DB::beginTransaction();
    	try {
    		$record 				= RencanaAudit::find($request->id);
    		$nama = $record->tahun;
	        $record->tahun 			= $request->tahun;
	        $record->updated_by 	= $user->id;
	        $record->created_by 	= $user->id;
	        $record->save();

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Kegiatan Audit Rencana';
		    $log->sub = 'RKIA';
		    $log->aktivitas = 'Mengubah RKIA dengan Tahun '.$nama.' menjadi '.$request->tahun;
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }

   	}


   	public function preview(RencanaAudit $id)
    {
        $daftar = RencanaAudit::find($id->id);
    	$link =0;
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		$link = asset('/storage/'.$daftar->bukti);
    	}
    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);
    }

    public function destroy($id)
    {
    	$cek = RencanaAudit::find($id);
    	$a =0;
    	foreach ($cek->detailrencana as $key => $value) {
    		if($value->penugasan){
    			$a += 1;
    		}else{
				$a += 0;
    		}
    	}

    	if($a > 0){
    		return response([
            	'status' => true,
        	],500);
    	}else{
	    	$user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Kegiatan Audit Rencana';
		    $log->sub = 'RKIA';
		    $log->aktivitas = 'Menghapus RKIA dengan Tahun '.$cek->tahun;
		    $log->user_id = $user->id;
		    $log->save();

	        $cek->delete();
	        return response()->json([
	            'success' => true
	        ]);
    	}
    }

    public function cetak(Request $request, $id){
    	$record = RencanaAudit::find($id);
    	$edisi_id = $record->detailrencana->first()->edisi_id;
    	$cek_edisi = DB::table('log_ref_edisi')->where('id', $edisi_id)->first();
        $data = [
        	'records' => $record,
        	'edisi' => $cek_edisi->edisi,
        	'revisi' => $cek_edisi->revisi,
        ];
	    $pdf = PDF::loadView('modules.rkia.cetak', $data)->setPaper('a4', 'landscape');
        return $pdf->stream('RKIA.pdf',array("Attachment" => false));
 	}

 	public function downloadAudit($id)
  {
    	$daftar = RencanaAudit::find($id);

      if($daftar->dms == 1)
      {
          $this->downloadDms($daftar->bukti, $daftar->filename);
          $ext = pathinfo($daftar->filename);

          return response()->download(public_path('storage/temp-file/'.md5($filename.auth()->user()->id).'.'.$ext['extension']), $filename)->deleteFileAfterSend(true);
      }else{
      	if(file_exists(public_path('storage/'.$daftar->bukti)))
      	{
      		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
      	}

        return abort('404');
      }
    }

    public function downloadDms($url, $filename)
    {
        $ext = pathinfo($filename);

        $otdsTicket = $this->otdsLogin();
        $otcsTicket = $this->otcsLogin($otdsTicket);

        Storage::disk('public')->put('temp-file/'.md5($filename.auth()->user()->id).'.'.$ext['extension'], $this->showfiledms($otcsTicket, $url));


    }

    public function otdsLogin()
    {

      $client = new Client;

      $response = $client->request('POST', 'https://otds.waskita.co.id:8443/otdsws/v1/authentication/credentials', [
          'json' => [
              'user_name' => 'adminIAO',
              'password' => 'Audit123@'
          ],
          'verify' => false,
      ]);

      $ticket = json_decode($response->getBody()->getContents())->ticket;

      return $ticket;

    }

    public function otcsLogin($ticket)
    {
      try {
        $client = new Client;

        $response = $client->request('POST', 'http://dmsinterface.waskita.co.id:8080/xecm/api/v1/auth/otdsticket', [
            'json' => [
                'otds_ticket' => $ticket
            ],
            'verify' => false,
        ]);

        return json_decode($response->getBody()->getContents())->ticket;

      }catch (\Exception $exception){
        return $this->otcsLogin($this->otdsLogin());
      }

    }

    public function showfiledms($ticket, $url)
    {
      try {
        $client = new Client;

        $response = $client->request('GET', $url, [
            'headers' => [
                'otcsticket' => $ticket
            ],
        ]);

        return $response->getBody()->getContents();

      }catch (\Exception $exception){
        return abort('404');
      }

    }
}
