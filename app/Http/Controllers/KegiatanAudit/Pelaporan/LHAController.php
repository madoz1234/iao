<?php

namespace App\Http\Controllers\KegiatanAudit\Pelaporan;

use App\Models\Auths\User;
use App\Models\Files;
use App\Models\Notification;
use App\Http\Controllers\Controller;
use App\Models\Master\Edisi;
use App\Http\Requests\KegiatanAudit\Pelaporan\DokumenRequest;
use App\Http\Requests\KegiatanAudit\Pelaporan\LHARequest;
use App\Http\Requests\KegiatanAudit\Pelaporan\RejectDirutRequest;
use App\Http\Requests\KegiatanAudit\Pelaporan\RejectSvpRequest;
use App\Http\Requests\KegiatanAudit\Pelaporan\ResumeRequest;
use App\Models\KegiatanAudit\Rencana\RencanaAudit;
use App\Models\KegiatanAudit\Pelaksanaan\ClosingMeeting\ClosingMeeting;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaDetail;
use App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting;
use App\Models\KegiatanAudit\Pelaporan\LHA;
use App\Models\KegiatanAudit\Pelaporan\LHADetail;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit;
use App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumen;
use App\Models\KegiatanAudit\TindakLanjut\SuratPerintah;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\KegiatanAudit\Pelaporan\LHAAdendum;
use App\Models\KegiatanAudit\Pelaporan\LHALingkup;
use App\Models\KegiatanAudit\Pelaporan\LHAKesimpulan;
use App\Models\KegiatanAudit\TindakLanjut\RegisterTL;
use App\Models\KegiatanAudit\TindakLanjut\RegisterTLDetail;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaDetailRekomendasi;
use App\Models\SessionLog;
use App\Models\Master\Survey;
use App\Models\Survey\SurveyJawab;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;
use iio\libmergepdf\Merger;

use DB;
use PDF;
use Yajra\DataTables\Facades\DataTables;

class LHAController extends Controller
{
    protected $routes = 'kegiatan-audit.pelaporan.lha';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Kegiatan Audit' => '#', 'Pelaporan' => '#', 'LHA' => '#']);
        $this->listStructs = [
            'listStruct' => [
                    [
                        'data' => 'num',
                        'name' => 'num',
                        'label' => '#',
                        'orderable' => false,
                        'searchable' => false,
                        'className' => 'text-center',
                        'width' => '20px',
                    ],
                    [
                        'data' => 'tahun',
                        'name' => 'tahun',
                        'label' => 'Tahun',
                        'className' => 'text-center',
                        'width' => '200px',
                        'sortable' => true,
                    ],
                    [
                        'data' => 'rencana',
                        'name' => 'rencana',
                        'label' => 'Waktu Pelaksanaan',
                        'className' => 'text-center',
                        'width' => '300px',
                    ],
                    [
                        'data' => 'tipe',
                        'name' => 'tipe',
                        'label' => 'Tipe',
                        'className' => 'text-center',
                        'width' => '200px',
                        'sortable' => true,
                    ],
                    [
                        'data' => 'kategori',
                        'name' => 'kategori',
                        'label' => 'Kategori',
                        'className' => 'text-center',
                        'width' => '200px',
                        'sortable' => true,
                    ],
                    [
                        'data' => 'object_audit',
                        'name' => 'object_audit',
                        'label' => 'Objek Audit',
                        'className' => 'text-left',
                        'width' => '250px',
                        'sortable' => true,
                    ],
                    [
                        'data' => 'no_ab',
                        'name' => 'no_ab',
                        'label' => 'No AB',
                        'className' => 'text-center',
                        'width' => '150px',
                        'sortable' => true,
                    ],
                    [
                        'data' => 'detil',
                        'name' => 'detil',
                        'label' => 'Rencana Kerja',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'penugasan',
                        'name' => 'penugasan',
                        'label' => 'Surat Penugasan',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'tinjauan',
                        'name' => 'tinjauan',
                        'label' => 'Tinjauan Dokumen',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'detil_program',
                        'name' => 'detil_program',
                        'label' => 'Program Audit',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'detil_draft',
                        'name' => 'detil_draft',
                        'label' => 'KKA',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
		                'data' => 'tim',
		                'name' => 'tim',
		                'label' => 'Tim Auditor',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '180px',
		                'className' => 'text-left'
		            ],
                    [
                        'data' => 'status',
                        'name' => 'status',
                        'label' => 'Status',
                        'searchable' => false,
                        'sortable' => false,
                        'width' => '100px',
                        'className' => 'text-center'
                    ],
                    [
                        'data' => 'action',
                        'name' => 'action',
                        'label' => 'Aksi',
                        'searchable' => false,
                        'sortable' => false,
                        'width' => '80px',
                        'className' => 'text-center'
                    ],
            ],
            'listStruct2' => [
                    [
                        'data' => 'num',
                        'name' => 'num',
                        'label' => '#',
                        'orderable' => false,
                        'searchable' => false,
                        'className' => 'text-center',
                        'width' => '20px',
                    ],
                    [
                        'data' => 'tahun',
                        'name' => 'tahun',
                        'label' => 'Tahun',
                        'className' => 'text-center',
                        'width' => '200px',
                        'sortable' => true,
                    ],
                    [
                        'data' => 'rencana',
                        'name' => 'rencana',
                        'label' => 'Waktu Pelaksanaan',
                        'className' => 'text-center',
                        'width' => '300px',
                    ],
                    [
                        'data' => 'tipe',
                        'name' => 'tipe',
                        'label' => 'Tipe',
                        'className' => 'text-center',
                        'width' => '200px',
                        'sortable' => true,
                    ],
                    [
                        'data' => 'kategori',
                        'name' => 'kategori',
                        'label' => 'Kategori',
                        'className' => 'text-center',
                        'width' => '200px',
                        'sortable' => true,
                    ],
                    [
                        'data' => 'object_audit',
                        'name' => 'object_audit',
                        'label' => 'Objek Audit',
                        'className' => 'text-left',
                        'width' => '250px',
                        'sortable' => true,
                    ],
                    [
                        'data' => 'no_ab',
                        'name' => 'no_ab',
                        'label' => 'No AB',
                        'className' => 'text-center',
                        'width' => '150px',
                        'sortable' => true,
                    ],
                    [
                        'data' => 'detil',
                        'name' => 'detil',
                        'label' => 'Rencana Kerja',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'penugasan',
                        'name' => 'penugasan',
                        'label' => 'Surat Penugasan',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'tinjauan',
                        'name' => 'tinjauan',
                        'label' => 'Tinjauan Dokumen',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'detil_program',
                        'name' => 'detil_program',
                        'label' => 'Program Audit',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'detil_draft',
                        'name' => 'detil_draft',
                        'label' => 'KKA',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'detil_lha',
                        'name' => 'detil_lha',
                        'label' => 'Berkas LHA',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
		                'data' => 'tim',
		                'name' => 'tim',
		                'label' => 'Tim Auditor',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '180px',
		                'className' => 'text-left'
		            ],
                    [
                        'data' => 'status',
                        'name' => 'status',
                        'label' => 'Status',
                        'searchable' => false,
                        'sortable' => false,
                        'width' => '100px',
                        'className' => 'text-center'
                    ],
                    [
                        'data' => 'action',
                        'name' => 'action',
                        'label' => 'Aksi',
                        'searchable' => false,
                        'sortable' => false,
                        'width' => '80px',
                        'className' => 'text-center'
                    ],
            ],
            'listStruct3' => [
                    [
                        'data' => 'num',
                        'name' => 'num',
                        'label' => '#',
                        'orderable' => false,
                        'searchable' => false,
                        'className' => 'text-center',
                        'width' => '20px',
                    ],
                    [
                        'data' => 'tahun',
                        'name' => 'tahun',
                        'label' => 'Tahun',
                        'className' => 'text-center',
                        'width' => '200px',
                        'sortable' => true,
                    ],
                    [
                        'data' => 'rencana',
                        'name' => 'rencana',
                        'label' => 'Waktu Pelaksanaan',
                        'className' => 'text-center',
                        'width' => '300px',
                    ],
                    [
                        'data' => 'tipe',
                        'name' => 'tipe',
                        'label' => 'Tipe',
                        'className' => 'text-center',
                        'width' => '200px',
                        'sortable' => true,
                    ],
                    [
                        'data' => 'kategori',
                        'name' => 'kategori',
                        'label' => 'Kategori',
                        'className' => 'text-center',
                        'width' => '200px',
                        'sortable' => true,
                    ],
                    [
                        'data' => 'object_audit',
                        'name' => 'object_audit',
                        'label' => 'Objek Audit',
                        'className' => 'text-left',
                        'width' => '250px',
                        'sortable' => true,
                    ],
                    [
                        'data' => 'no_ab',
                        'name' => 'no_ab',
                        'label' => 'No AB',
                        'className' => 'text-center',
                        'width' => '150px',
                        'sortable' => true,
                    ],
                    [
                        'data' => 'detil',
                        'name' => 'detil',
                        'label' => 'Rencana Kerja',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'penugasan',
                        'name' => 'penugasan',
                        'label' => 'Surat Penugasan',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'tinjauan',
                        'name' => 'tinjauan',
                        'label' => 'Tinjauan Dokumen',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'detil_program',
                        'name' => 'detil_program',
                        'label' => 'Program Audit',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'detil_draft',
                        'name' => 'detil_draft',
                        'label' => 'KKA',
                        'className' => 'text-center',
                        'sortable' => true,
                        'width' => '100px',
                    ],
                    [
                        'data' => 'status',
                        'name' => 'status',
                        'label' => 'Status',
                        'searchable' => false,
                        'sortable' => false,
                        'width' => '100px',
                        'className' => 'text-center'
                    ],
                    [
                        'data' => 'action',
                        'name' => 'action',
                        'label' => 'Aksi',
                        'searchable' => false,
                        'sortable' => false,
                        'width' => '120px',
                        'className' => 'text-center'
                    ],
            ],
        ];
    }

    public function grid()
    {
        $user = auth()->user();
        if($user->hasJabatan(['SVP - Internal Audit','President Director','Secretary','auditor'])){
            $records = DraftKka::where('status', 5)
                                 ->select('*');

            if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at','desc');
            }
            if($tahun = request()->tahun) {
                $records->whereHas('closing', function($a) use ($tahun){
                    $a->whereHas('opening', function($b) use ($tahun){
                        $b->whereHas('program', function($c) use ($tahun){
                            $c->whereHas('penugasanaudit', function($e) use ($tahun){
                                $e->whereHas('rencanadetail', function($u) use ($tahun){
                                    $u->whereHas('rencanaaudit', function($z) use ($tahun){
                                        $z->where('tahun', 'like', '%' . $tahun . '%');
                                    });
                                });
                            });
                        });
                    });
                });
            }
            if ($tipe = request()->tipe) {
                $records->whereHas('closing', function($a) use ($tipe){
                    $a->whereHas('opening', function($b) use ($tipe){
                        $b->whereHas('program', function($c) use ($tipe){
                            $c->whereHas('penugasanaudit', function($e) use ($tipe){
                                $e->whereHas('rencanadetail', function($u) use ($tipe){
                                    $u->whereHas('rencanaaudit', function($z) use ($tipe){
                                        if($tipe == 1){
                                            $z->where('tipe', 0);
                                        }else{
                                            $z->where('tipe', 1);
                                        }
                                    });
                                });
                            });
                        });
                    });
                });
            }
            if($kategori = request()->kategori) {
                if($kategori == 1){
                    $records->whereHas('closing', function($a){
                        $a->whereHas('opening', function($b){
                            $b->whereHas('program', function($c){
                                $c->whereHas('penugasanaudit', function($e){
                                    $e->whereHas('rencanadetail', function($u){
                                        $u->where('tipe_object', 0);
                                    });
                                });
                            });
                        });
                    });
                }elseif($kategori == 2){
                    $records->whereHas('closing', function($a){
                        $a->whereHas('opening', function($b){
                            $b->whereHas('program', function($c){
                                $c->whereHas('penugasanaudit', function($e){
                                    $e->whereHas('rencanadetail', function($u){
                                        $u->where('tipe_object', 1);
                                    });
                                });
                            });
                        });
                    });
                }elseif($kategori == 3){
                    $records->whereHas('closing', function($a){
                        $a->whereHas('opening', function($b){
                            $b->whereHas('program', function($c){
                                $c->whereHas('penugasanaudit', function($e){
                                    $e->whereHas('rencanadetail', function($u){
                                        $u->where('tipe_object', 2);
                                    });
                                });
                            });
                        });
                    });
                }elseif($kategori == 4){
                    $records->whereHas('closing', function($a){
                        $a->whereHas('opening', function($b){
                            $b->whereHas('program', function($c){
                                $c->whereHas('penugasanaudit', function($e){
                                    $e->whereHas('rencanadetail', function($u){
                                        $u->where('tipe_object', 3);
                                    });
                                });
                            });
                        });
                    });
                }else{
                    $records = $records;
                }
            }

            if($object_id = request()->object_id) {
                if(request()->kategori == 1){
                    $kat = $kategori= 0;
                }elseif(request()->kategori == 2){
                    $kat = $kategori= 1;
                }elseif(request()->kategori == 3){
                    $kat = $kategori= 2;
                }elseif(request()->kategori == 4){
                    $kat = $kategori= 3;
                }else{
                    $kat = null;
                }
                $records->whereHas('closing', function($a) use ($kat, $object_id){
                    $a->whereHas('opening', function($b) use ($kat, $object_id){
                        $b->whereHas('program', function($c) use ($kat, $object_id){
                            $c->whereHas('penugasanaudit', function($e) use ($kat, $object_id){
                                $e->whereHas('rencanadetail', function($u) use ($kat, $object_id){
                                    $u->where('tipe_object', $kat)->where('object_id', $object_id);
                                });
                            });
                        });
                    });
                });
            }
        }else{
            $records = DraftKka::where('id', 0);
        }
        return DataTables::of($records->get())
           ->addColumn('num', function ($record) {
               return request()->start;
           })
           ->addColumn('tahun', function ($record) {
               return $record->program->penugasanaudit->rencanadetail->rencanaaudit->tahun;
           })
           ->addColumn('tipe', function ($record) {
                $tipe = '';
                if($record->program->penugasanaudit->rencanadetail->tipe == 0){
                  $tipe = '<span class="label label-primary">Audit</span>';
                }elseif($record->program->penugasanaudit->rencanadetail->tipe == 1){
                  $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
                }else{
                  $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
                }

                return $tipe;
           })
           ->addColumn('kategori', function ($record) {
                $kategori = '';
                if($record->program->penugasanaudit->rencanadetail->tipe == 0){
                    if($record->program->penugasanaudit->rencanadetail->tipe_object == 0){
                      $kategori = '<span class="label label-success">Business Unit (BU)</span>';
                    }elseif($record->program->penugasanaudit->rencanadetail->tipe_object == 1){
                      $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
                    }elseif($record->program->penugasanaudit->rencanadetail->tipe_object == 2){
                      $kategori = '<span class="label label-warning">Project</span>';
                    }else{
                      $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
                    }
                }elseif($record->program->penugasanaudit->rencanadetail->tipe == 1){
                    $kategori = $record->program->penugasanaudit->rencanadetail->konsultasi->nama;
                }else{
                    $kategori = $record->program->penugasanaudit->rencanadetail->lain->nama;
                }
                return $kategori;
           })
           ->addColumn('object_audit', function ($record) {
               return object_audit($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
           })
           ->addColumn('nama', function ($record) {
                if($record->program->penugasanaudit->rencanadetail->tipe_object == 2){
                    return object_audit($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
                }else{
                    return '';
                }
           })
           ->addColumn('no_ab', function ($record) {
               return getAb($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
           })
           ->addColumn('rencana', function ($record) {
                return DateToString($record->program->detailpelaksanaan->first()->tgl).' - '.DateToString($record->program->detailpelaksanaan->last()->tgl);
           })
           ->addColumn('detil', function ($record) use ($user) {
               $buttons = '';
               if($user->hasJabatan(['SVP - Internal Audit','President Director','auditor'])){
	               $buttons .= $this->makeButton([
	                    'type'      => 'edit',
	                    'label'     => '<i class="fa fa-download text-primary"></i>',
	                    'tooltip'   => 'Rencana Kerja',
	                    'class'     => 'download-rkia button',
	                    'id'        => $record->program->penugasanaudit->rencanadetail->rencanaaudit->id,
	               ]);
               }
               return $buttons;
           })
           ->addColumn('penugasan', function ($record) {
               $buttons = '';
               if($record->program->penugasanaudit->status == 4){
                   $buttons .= $this->makeButton([
                        'type'      => 'edit',
                        'label'     => '<i class="fa fa-download text-primary"></i>',
                        'tooltip'   => 'Surat Penugasan',
                        'class'     => 'download-penugasan button',
                        'id'        => $record->program->penugasanaudit->id,
                   ]);
               }else{
                    $buttons .='';
               }
               return $buttons;
           })
           ->addColumn('tinjauan', function ($record) {
               $buttons = '';
               if($record->program->penugasanaudit->tinjauandokumen){
                   $buttons .= $this->makeButton([
                        'type'      => 'edit',
                        'label'     => '<i class="fa fa-eye text-primary"></i>',
                        'tooltip'   => 'Detil Tinjauan',
                        'class'     => 'detil-tinjauan button',
                        'id'        => $record->program->penugasanaudit->tinjauandokumen->id,
                    ]);
               }

               return $buttons;
           })
           ->addColumn('tim', function ($record) use ($user) {
	           	$data = array();
                $tim ='';
           		$tim .='<ul class="list list-more2 list-unstyled" style="margin-top:10px;" data-display="3">';
           		$tim .='<li class="item">'.$record->program->user->name.'</li>';
           		foreach ($record->program->detailanggota->whereIn('fungsi', [1,2,3]) as $key => $value) {
           				$data[] = $value->user->name;
           		}
           		foreach (array_unique($data) as $key => $value) {
           			$tim .='<li class="item">'.$value.'</li>';
           		}
           		$tim .='</ul>';
               	return $tim;
           })
           ->addColumn('detil_program', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-download text-primary"></i>',
                    'tooltip'   => 'Program Audit',
                    'class'     => 'download-program button',
                    'id'        => $record->program->id,
               ]);
               return $buttons;
           })
           ->addColumn('detil_draft', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-eye text-primary"></i>',
                    'tooltip'   => 'KKA',
                    'class'     => 'detil-draft button',
                    'id'        => $record->id,
                ]);

               return $buttons;
           })
           ->editColumn('status', function ($record) {
               if($record->lha){
                    if($record->lha->ket_svp){
                       return '<span class="label label-warning">Ditolak SVP</span>';
                    }else{
                        return '<span class="label label-default">Draft</span>';
                    }
               }else{
                   return '<span class="label label-default">Draft</span>';
               }
           })
           ->addColumn('action', function ($record) use ($user) {
               $buttons = '';
                   if($user->hasJabatan(['auditor'])){
                   		if($record->program->user->id == $user->id){
	                        if($record->lha){
	                           $buttons .= $this->makeButton([
	                                'type'      => 'edit',
	                                'label'     => '<i class="fa fa-edit text-primary"></i>',
	                                'tooltip'   => 'Ubah LHA',
	                                'class'     => 'ubah button',
	                                'id'        => $record->lha->id,
	                           ]);
	                        }else{
	                           $buttons .= $this->makeButton([
	                                'type'      => 'edit',
	                                'label'     => '<i class="fa fa-plus text-primary"></i>',
	                                'tooltip'   => 'Buat LHA',
	                                'class'     => 'buat button',
	                                'id'        => $record->id,
	                           ]);
	                       }
                   		}else{
                   			$buttons = '';
                   		}
                   }else{
                      $buttons = '';
                   }
               return $buttons;
           })
           ->rawColumns(['tipe','kategori','action','detil','penugasan','tinjauan','object_audit','detil_draft','status','detil_program','tim'])
           ->make(true);
    }

    public function onProgress()
    {
        $user = auth()->user();
        if($user->hasJabatan(['SVP - Internal Audit','President Director','Secretary', 'auditor'])){
            $records = LHA::whereIn('status', [1,2,3,4,5])->select('*');
            if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at','desc');
            }
            if($tahun = request()->tahun) {
                $records->whereHas('draftkka', function($a) use ($tahun){
                    $a->whereHas('closing', function($b) use ($tahun){
                        $b->whereHas('opening', function($c) use ($tahun){
                            $c->whereHas('program', function($d) use ($tahun){
                                $d->whereHas('penugasanaudit', function($f) use ($tahun){
                                    $f->whereHas('rencanadetail', function($u) use ($tahun){
                                        $u->whereHas('rencanaaudit', function($z) use ($tahun){
                                            $z->where('tahun', 'like', '%' . $tahun . '%');
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            }
            if ($tipe = request()->tipe) {
                $records->whereHas('draftkka', function($a) use ($tipe){
                    $a->whereHas('closing', function($b) use ($tipe){
                        $b->whereHas('opening', function($c) use ($tipe){
                            $c->whereHas('program', function($d) use ($tipe){
                                $d->whereHas('penugasanaudit', function($f) use ($tipe){
                                    $f->whereHas('rencanadetail', function($u) use ($tipe){
                                        $u->whereHas('rencanaaudit', function($z) use ($tipe){
                                            if($tipe == 1){
                                                $z->where('tipe', 0);
                                            }else{
                                                $z->where('tipe', 1);
                                            }
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            }
            if($kategori = request()->kategori) {
                if($kategori == 1){
                    $records->whereHas('draftkka', function($a){
                        $a->whereHas('closing', function($b){
                            $b->whereHas('opening', function($c){
                                $c->whereHas('program', function($d){
                                    $d->whereHas('penugasanaudit', function($f){
                                        $f->whereHas('rencanadetail', function($u){
                                            $u->where('tipe_object', 0);
                                        });
                                    });
                                });
                            });
                        });
                    });
                }elseif($kategori == 2){
                    $records->whereHas('draftkka', function($a){
                        $a->whereHas('closing', function($b){
                            $b->whereHas('opening', function($c){
                                $c->whereHas('program', function($d){
                                    $d->whereHas('penugasanaudit', function($f){
                                        $f->whereHas('rencanadetail', function($u){
                                            $u->where('tipe_object', 1);
                                        });
                                    });
                                });
                            });
                        });
                    });
                }elseif($kategori == 3){
                    $records->whereHas('draftkka', function($a){
                        $a->whereHas('closing', function($b){
                            $b->whereHas('opening', function($c){
                                $c->whereHas('program', function($d){
                                    $d->whereHas('penugasanaudit', function($f){
                                        $f->whereHas('rencanadetail', function($u){
                                            $u->where('tipe_object', 2);
                                        });
                                    });
                                });
                            });
                        });
                    });
                }elseif($kategori == 4){
                    $records->whereHas('draftkka', function($a){
                        $a->whereHas('closing', function($b){
                            $b->whereHas('opening', function($c){
                                $c->whereHas('program', function($d){
                                    $d->whereHas('penugasanaudit', function($f){
                                        $f->whereHas('rencanadetail', function($u){
                                            $u->where('tipe_object', 3);
                                        });
                                    });
                                });
                            });
                        });
                    });
                }else{
                    $records = $records;
                }
            }

            if($object_id = request()->object_id) {
                if(request()->kategori == 1){
                    $kat = $kategori= 0;
                }elseif(request()->kategori == 2){
                    $kat = $kategori= 1;
                }elseif(request()->kategori == 3){
                    $kat = $kategori= 2;
                }elseif(request()->kategori == 4){
                    $kat = $kategori= 3;
                }else{
                    $kat = null;
                }

                $records->whereHas('draftkka', function($a) use ($kat, $object_id){
                    $a->whereHas('closing', function($b) use ($kat, $object_id){
                        $b->whereHas('opening', function($c) use ($kat, $object_id){
                            $c->whereHas('program', function($d) use ($kat, $object_id){
                                $d->whereHas('penugasanaudit', function($f) use ($kat, $object_id){
                                    $f->whereHas('rencanadetail', function($u) use ($kat, $object_id){
                                        $u->where('tipe_object', $kat)->where('object_id', $object_id);
                                    });
                                });
                            });
                        });
                    });
                });
            }
        }else{
            $records = LHA::where('id', 0);
        }
        return DataTables::of($records->get())
           ->addColumn('num', function ($record) {
               return request()->start;
           })
           ->addColumn('tahun', function ($record) {
               return $record->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun;
           })
           ->addColumn('tipe', function ($record) {
                $tipe = '';
                if($record->draftkka->program->penugasanaudit->rencanadetail->tipe == 0){
                  $tipe = '<span class="label label-primary">Audit</span>';
                }elseif($record->draftkka->program->penugasanaudit->rencanadetail->tipe == 1){
                  $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
                }else{
                  $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
                }

                return $tipe;
           })
           ->addColumn('kategori', function ($record) {
                $kategori = '';
                if($record->draftkka->program->penugasanaudit->rencanadetail->tipe == 0){
                    if($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 0){
                      $kategori = '<span class="label label-success">Business Unit (BU)</span>';
                    }elseif($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 1){
                      $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
                    }elseif($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 2){
                      $kategori = '<span class="label label-warning">Project</span>';
                    }else{
                      $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
                    }
                }elseif($record->draftkka->program->penugasanaudit->rencanadetail->tipe == 1){
                    $kategori = $record->draftkka->program->penugasanaudit->rencanadetail->konsultasi->nama;
                }else{
                    $kategori = $record->draftkka->program->penugasanaudit->rencanadetail->lain->nama;
                }
                return $kategori;
           })
           ->addColumn('object_audit', function ($record) {
               return object_audit($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id);
           })
           ->addColumn('nama', function ($record) {
                if($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 2){
                    return object_audit($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id);
                }else{
                    return '';
                }
           })
           ->addColumn('tim', function ($record) use ($user) {
	           	$data = array();
                $tim ='';
           		$tim .='<ul class="list list-more2 list-unstyled" style="margin-top:10px;" data-display="3">';
           		$tim .='<li class="item">'.$record->draftkka->program->user->name.'</li>';
           		foreach ($record->draftkka->program->detailanggota->whereIn('fungsi', [1,2,3]) as $key => $value) {
           				$data[] = $value->user->name;
           		}
           		foreach (array_unique($data) as $key => $value) {
           			$tim .='<li class="item">'.$value.'</li>';
           		}
           		$tim .='</ul>';
               	return $tim;
           })
           ->addColumn('no_ab', function ($record) {
               return getAb($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id);
           })
           ->addColumn('rencana', function ($record) {
                return DateToString($record->draftkka->program->detailpelaksanaan->first()->tgl).' - '.DateToString($record->draftkka->program->detailpelaksanaan->last()->tgl);
           })
           ->addColumn('detil', function ($record) use ($user) {
               $buttons = '';
               if($user->hasJabatan(['SVP - Internal Audit','President Director','auditor'])){
	               $buttons .= $this->makeButton([
	                    'type'      => 'edit',
	                    'label'     => '<i class="fa fa-download text-primary"></i>',
	                    'tooltip'   => 'Rencana Kerja',
	                    'class'     => 'download-rkia button',
	                    'id'        => $record->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->id,
	               ]);
               }
               return $buttons;
           })
           ->addColumn('penugasan', function ($record) {
               $buttons = '';
               if($record->draftkka->program->penugasanaudit->status == 4){
                   $buttons .= $this->makeButton([
                        'type'      => 'edit',
                        'label'     => '<i class="fa fa-download text-primary"></i>',
                        'tooltip'   => 'Surat Penugasan',
                        'class'     => 'download-penugasan button',
                        'id'        => $record->draftkka->program->penugasanaudit->id,
                   ]);
               }else{
                    $buttons .='';
               }
               return $buttons;
           })
           ->addColumn('tinjauan', function ($record) {
               $buttons = '';
               if($record->draftkka->program->penugasanaudit->tinjauandokumen){
                   $buttons .= $this->makeButton([
                        'type'      => 'edit',
                        'label'     => '<i class="fa fa-eye text-primary"></i>',
                        'tooltip'   => 'Detil Tinjauan',
                        'class'     => 'detil-tinjauan button',
                        'id'        => $record->draftkka->program->penugasanaudit->tinjauandokumen->id,
                    ]);
               }

               return $buttons;
           })
           ->addColumn('detil_program', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-download text-primary"></i>',
                    'tooltip'   => 'Program Audit',
                    'class'     => 'download-program button',
                    'id'        => $record->draftkka->program->id,
               ]);
               return $buttons;
           })
           ->addColumn('detil_draft', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-eye text-primary"></i>',
                    'tooltip'   => 'KKA',
                    'class'     => 'detil-draft button',
                    'id'        => $record->draftkka->id,
                ]);

               return $buttons;
           })
           ->addColumn('detil_lha', function ($record) {
                $buttons = '';
                $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-eye text-primary"></i>',
                    'tooltip'   => 'LHA',
                    'class'     => 'detil-lha button',
                    'id'        => $record->id,
                ]);
                $buttons .= '&nbsp;';
                if($record->detaillha->isNotEmpty()){
	                $buttons .= $this->makeButton([
	                    'type'      => 'edit',
	                    'label'     => '<i class="fa fa-eye text-primary"></i>',
	                    'tooltip'   => 'Resume',
	                    'class'     => 'detil-resume button',
	                    'id'        => $record->id,
	                ]);
                }
               return $buttons;
           })
           ->editColumn('status', function ($record) {
               if($record->status == 1){
                   return '<span class="label label-info">Waiting Resume</span>';
               }elseif($record->status == 2){
                    return '<span class="label label-info">Menunggu Approval Ketua Tim</span>';
               }elseif($record->status == 3){
                    return '<span class="label label-info">Menunggu Approval SVP IA</span>';
               }elseif($record->status == 4){
                    return '<span class="label label-info">Menunggu Approval Dirut</span>';
               }elseif($record->status == 5){
                    return '<span class="label label-info">Waiting Upload</span>';
               }else{
                    return '<span class="label label-success">Completed</span>';
               }
           })
           ->editColumn('created_at', function ($record) {
               return $record->created_at->diffForHumans();
           })
           ->editColumn('created_by', function ($record) {
               return $record->creatorName();
           })
           ->addColumn('action', function ($record) use ($user) {
               $buttons = '';
               if($record->status == 1){
               		$cek = count($record->draftkka->program->detailanggota->where('user_id', $user->id));
               		if($cek > 0){
	               		if($user->hasJabatan(['auditor'])){
	               			if($record->detaillha->isNotEmpty()){
	               				$buttons .= $this->makeButton([
		                            'type'      => 'edit',
		                            'label'     => '<i class="fa fa-book text-primary"></i>',
		                            'tooltip'   => 'Ubah Resume',
		                            'class'     => 'resume button',
		                            'id'        => $record->id,
		                        ]);
	               			}else{
		                        $buttons .= $this->makeButton([
		                            'type'      => 'edit',
		                            'label'     => '<i class="fa fa-book text-primary"></i>',
		                            'tooltip'   => 'Buat Resume',
		                            'class'     => 'resume button',
		                            'id'        => $record->id,
		                        ]);
	               			}
	                    }else{
	                        $buttons = '';
	                    }
               		}else{
                        $buttons = '';
                    }
                    $buttons .= '&nbsp;&nbsp;&nbsp;&nbsp;';
                    $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-print text-info"></i>',
	                    'tooltip' 	=> 'LHA',
	                    'class' 	=> 'cetak-lha button',
	                    'id'   		=> $record->id,
	               	]);
               }elseif($record->status == 2){
               		if($user->id == $record->draftkka->program->user_id){
                        $buttons .= $this->makeButton([
                            'type'      => 'edit',
                            'label'     => '<i class="fa fa-check text-primary"></i>',
                            'tooltip'   => 'Buat Approval',
                            'class'     => 'approve-tim button',
                            'id'        => $record->id,
                        ]);
                    }else{
                        $buttons = '';
                    }
                    $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-print text-info"></i>',
	                    'tooltip' 	=> 'LHA',
	                    'class' 	=> 'cetak-lha button',
	                    'id'   		=> $record->id,
	               	]);
	               	$buttons .= '&nbsp;&nbsp;&nbsp;&nbsp;';
	                $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-print text-info"></i>',
	                    'tooltip' 	=> 'Resume',
	                    'class' 	=> 'cetak-resume button',
	                    'id'   		=> $record->id,
	               	]);
               }elseif($record->status == 3){
               		if($user->hasJabatan(['SVP - Internal Audit'])){
                        $buttons .= $this->makeButton([
                            'type'      => 'edit',
                            'label'     => '<i class="fa fa-check text-primary"></i>',
                            'tooltip'   => 'Buat Approval',
                            'class'     => 'approve-svp2 button',
                            'id'        => $record->id,
                        ]);
                    }else{
                        $buttons = '';
                    }
                    $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-print text-info"></i>',
	                    'tooltip' 	=> 'LHA',
	                    'class' 	=> 'cetak-lha button',
	                    'id'   		=> $record->id,
	               	]);
	               	$buttons .= '&nbsp;&nbsp;&nbsp;&nbsp;';
	                $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-print text-info"></i>',
	                    'tooltip' 	=> 'Resume',
	                    'class' 	=> 'cetak-resume button',
	                    'id'   		=> $record->id,
	               	]);
               }elseif($record->status == 4){
               		if($user->hasJabatan(['President Director'])){
                        $buttons .= $this->makeButton([
                            'type'      => 'edit',
                            'label'     => '<i class="fa fa-check text-primary"></i>',
                            'tooltip'   => 'Buat Approval',
                            'class'     => 'approve-dirut button',
                            'id'        => $record->id,
                        ]);
                        $buttons .= '&nbsp;&nbsp;&nbsp;';
                        $buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-print text-info"></i>',
		                    'tooltip' 	=> 'Cetak LHA',
		                    'class' 	=> 'cetak-lha button',
		                    'id'   		=> $record->id,
		               	]);
                    }else{
                        $buttons = '';
                    }
	               	$buttons .= '&nbsp;&nbsp;&nbsp;&nbsp;';
	                $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-print text-info"></i>',
	                    'tooltip' 	=> 'Resume',
	                    'class' 	=> 'cetak-resume button',
	                    'id'   		=> $record->id,
	               	]);
	               	$buttons .= '&nbsp;&nbsp;&nbsp;&nbsp;';
	                $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-print text-info"></i>',
	                    'tooltip' 	=> 'Surat Pengantar',
	                    'class' 	=> 'cetak-surat button',
	                    'id'   		=> $record->id,
	               	]);
               }elseif($record->status == 5){
                    if($user->hasJabatan(['Secretary'])){
                        $buttons .= $this->makeButton([
                            'type'      => 'edit',
                            'label'     => '<i class="fa fa-upload text-primary"></i>',
                            'tooltip'   => 'Upload',
                            'class'     => 'upload-dokumen button',
                            'id'        => $record->id,
                        ]);
                    }else{
                    	if($user->hasJabatan(['SVP - Internal Audit','President Director'])){
                    		$buttons .= $this->makeButton([
			                    'type' 		=> 'edit',
			                    'label' 	=> '<i class="fa fa-print text-info"></i>',
			                    'tooltip' 	=> 'Cetak LHA',
			                    'class' 	=> 'cetak-lha button',
			                    'id'   		=> $record->id,
			               	]);
                    	}else{
                    		$buttons = '';
                    	}
                    }
	               	$buttons .= '&nbsp;&nbsp;&nbsp;&nbsp;';
	                $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-print text-info"></i>',
	                    'tooltip' 	=> 'Resume',
	                    'class' 	=> 'cetak-resume button',
	                    'id'   		=> $record->id,
	               	]);
	               	$buttons .= '&nbsp;&nbsp;&nbsp;&nbsp;';
	                $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-print text-info"></i>',
	                    'tooltip' 	=> 'Surat Pengantar',
	                    'class' 	=> 'cetak-surat button',
	                    'id'   		=> $record->id,
	               	]);
               }else{
               		$buttons = '';
               }
               return $buttons;
           })
           ->rawColumns(['tipe', 'action','detil','penugasan','tinjauan','detil_program','object_audit','nama','status','detil_lha','detil_draft','kategori','tim','cetak_data'])
           ->make(true);
    }

    public function historis()
    {
        $user = auth()->user();
        if($user->hasJabatan(['SVP - Internal Audit','President Director','Secretary', 'auditor'])){
            $records = LHA::where('status', 6)->select('*');
            if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at','desc');
            }
            if($tahun = request()->tahun) {
                $records->whereHas('draftkka', function($a) use ($tahun){
                    $a->whereHas('closing', function($b) use ($tahun){
                        $b->whereHas('opening', function($c) use ($tahun){
                            $c->whereHas('program', function($d) use ($tahun){
                                $d->whereHas('penugasanaudit', function($f) use ($tahun){
                                    $f->whereHas('rencanadetail', function($u) use ($tahun){
                                        $u->whereHas('rencanaaudit', function($z) use ($tahun){
                                            $z->where('tahun', 'like', '%' . $tahun . '%');
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            }
            if ($tipe = request()->tipe) {
                $records->whereHas('draftkka', function($a) use ($tipe){
                    $a->whereHas('closing', function($b) use ($tipe){
                        $b->whereHas('opening', function($c) use ($tipe){
                            $c->whereHas('program', function($d) use ($tipe){
                                $d->whereHas('penugasanaudit', function($f) use ($tipe){
                                    $f->whereHas('rencanadetail', function($u) use ($tipe){
                                        $u->whereHas('rencanaaudit', function($z) use ($tipe){
                                            if($tipe == 1){
                                                $z->where('tipe', 0);
                                            }else{
                                                $z->where('tipe', 1);
                                            }
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            }
            if($kategori = request()->kategori) {
                if($kategori == 1){
                    $records->whereHas('draftkka', function($a){
                        $a->whereHas('closing', function($b){
                            $b->whereHas('opening', function($c){
                                $c->whereHas('program', function($d){
                                    $d->whereHas('penugasanaudit', function($f){
                                        $f->whereHas('rencanadetail', function($u){
                                            $u->where('tipe_object', 0);
                                        });
                                    });
                                });
                            });
                        });
                    });
                }elseif($kategori == 2){
                    $records->whereHas('draftkka', function($a){
                        $a->whereHas('closing', function($b){
                            $b->whereHas('opening', function($c){
                                $c->whereHas('program', function($d){
                                    $d->whereHas('penugasanaudit', function($f){
                                        $f->whereHas('rencanadetail', function($u){
                                            $u->where('tipe_object', 1);
                                        });
                                    });
                                });
                            });
                        });
                    });
                }elseif($kategori == 3){
                    $records->whereHas('draftkka', function($a){
                        $a->whereHas('closing', function($b){
                            $b->whereHas('opening', function($c){
                                $c->whereHas('program', function($d){
                                    $d->whereHas('penugasanaudit', function($f){
                                        $f->whereHas('rencanadetail', function($u){
                                            $u->where('tipe_object', 2);
                                        });
                                    });
                                });
                            });
                        });
                    });
                }elseif($kategori == 4){
                    $records->whereHas('draftkka', function($a){
                        $a->whereHas('closing', function($b){
                            $b->whereHas('opening', function($c){
                                $c->whereHas('program', function($d){
                                    $d->whereHas('penugasanaudit', function($f){
                                        $f->whereHas('rencanadetail', function($u){
                                            $u->where('tipe_object', 3);
                                        });
                                    });
                                });
                            });
                        });
                    });
                }else{
                    $records = $records;
                }
            }

            if($object_id = request()->object_id) {
                if(request()->kategori == 1){
                    $kat = $kategori= 0;
                }elseif(request()->kategori == 2){
                    $kat = $kategori= 1;
                }elseif(request()->kategori == 3){
                    $kat = $kategori= 2;
                }elseif(request()->kategori == 4){
                    $kat = $kategori= 3;
                }else{
                    $kat = null;
                }

                $records->whereHas('draftkka', function($a) use ($kat, $object_id){
                    $a->whereHas('closing', function($b) use ($kat, $object_id){
                        $b->whereHas('opening', function($c) use ($kat, $object_id){
                            $c->whereHas('program', function($d) use ($kat, $object_id){
                                $d->whereHas('penugasanaudit', function($f) use ($kat, $object_id){
                                    $f->whereHas('rencanadetail', function($u) use ($kat, $object_id){
                                        $u->where('tipe_object', $kat)->where('object_id', $object_id);
                                    });
                                });
                            });
                        });
                    });
                });
            }
        }else{
            $records = LHA::where('id', 0);
        }
        return DataTables::of($records->get())
           ->addColumn('num', function ($record) {
               return request()->start;
           })
           ->addColumn('tahun', function ($record) {
               return $record->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun;
           })
           ->addColumn('tipe', function ($record) {
                $tipe = '';
                if($record->draftkka->program->penugasanaudit->rencanadetail->tipe == 0){
                  $tipe = '<span class="label label-primary">Audit</span>';
                }elseif($record->draftkka->program->penugasanaudit->rencanadetail->tipe == 1){
                  $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
                }else{
                  $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
                }

                return $tipe;
           })
           ->addColumn('kategori', function ($record) {
                $kategori = '';
                if($record->draftkka->program->penugasanaudit->rencanadetail->tipe == 0){
                    if($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 0){
                      $kategori = '<span class="label label-success">Business Unit (BU)</span>';
                    }elseif($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 1){
                      $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
                    }elseif($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object == 2){
                      $kategori = '<span class="label label-warning">Project</span>';
                    }else{
                      $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
                    }
                }elseif($record->draftkka->program->penugasanaudit->rencanadetail->tipe == 1){
                    $kategori = $record->draftkka->program->penugasanaudit->rencanadetail->konsultasi->nama;
                }else{
                    $kategori = $record->draftkka->program->penugasanaudit->rencanadetail->lain->nama;
                }
                return $kategori;
           })
           ->addColumn('object_audit', function ($record) {
               return object_audit($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id);
           })
           ->addColumn('no_ab', function ($record) {
               return getAb($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id);
           })
           ->addColumn('rencana', function ($record) {
                return DateToString($record->draftkka->program->detailpelaksanaan->first()->tgl).' - '.DateToString($record->draftkka->program->detailpelaksanaan->last()->tgl);
           })
           ->addColumn('detil', function ($record) use ($user) {
               $buttons = '';
               if($user->hasJabatan(['SVP - Internal Audit','President Director','auditor'])){
	               $buttons .= $this->makeButton([
	                    'type'      => 'edit',
	                    'label'     => '<i class="fa fa-download text-primary"></i>',
	                    'tooltip'   => 'Rencana Kerja',
	                    'class'     => 'download-rkia button',
	                    'id'        => $record->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->id,
	               ]);
               }
               return $buttons;
           })
           ->addColumn('penugasan', function ($record) {
               $buttons = '';
               if($record->draftkka->program->penugasanaudit->status == 4){
                   $buttons .= $this->makeButton([
                        'type'      => 'edit',
                        'label'     => '<i class="fa fa-download text-primary"></i>',
                        'tooltip'   => 'Surat Penugasan',
                        'class'     => 'download-penugasan button',
                        'id'        => $record->draftkka->program->penugasanaudit->id,
                   ]);
               }else{
                    $buttons .='';
               }
               return $buttons;
           })
           ->addColumn('tinjauan', function ($record) {
               $buttons = '';
               if($record->draftkka->program->penugasanaudit->tinjauandokumen){
                   $buttons .= $this->makeButton([
                        'type'      => 'edit',
                        'label'     => '<i class="fa fa-eye text-primary"></i>',
                        'tooltip'   => 'Detil Tinjauan',
                        'class'     => 'detil-tinjauan button',
                        'id'        => $record->draftkka->program->penugasanaudit->tinjauandokumen->id,
                    ]);
               }

               return $buttons;
           })
           ->addColumn('detil_program', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-download text-primary"></i>',
                    'tooltip'   => 'Program Audit',
                    'class'     => 'download-program button',
                    'id'        => $record->draftkka->program->id,
               ]);
               return $buttons;
           })
           ->addColumn('detil_draft', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-eye text-primary"></i>',
                    'tooltip'   => 'KKA',
                    'class'     => 'detil-draft button',
                    'id'        => $record->draftkka->id,
                ]);

               return $buttons;
           })
           ->addColumn('detil_lha', function ($record) {
                $buttons = '';
                $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-eye text-primary"></i>',
                    'tooltip'   => 'LHA',
                    'class'     => 'detil-lha button',
                    'id'        => $record->id,
                ]);
                $buttons .= '&nbsp;';
                $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-eye text-primary"></i>',
                    'tooltip'   => 'Resume',
                    'class'     => 'detil-resume button',
                    'id'        => $record->id,
                ]);
               return $buttons;
           })
           ->editColumn('status', function ($record) {
                return '<span class="label label-info">Historis</span>';
           })
           ->editColumn('created_at', function ($record) {
               return $record->created_at->diffForHumans();
           })
           ->editColumn('created_by', function ($record) {
               return $record->creatorName();
           })
           ->addColumn('action', function ($record) use ($user) {
               $buttons = '';
               $buttons .= $this->makeButton([
	                'type' 		=> 'edit',
	                'label' 	=> '<i class="fa fa-print text-info"></i>',
	                'tooltip' 	=> 'LHA',
	                'class' 	=> 'cetak-lha button',
	                'id'   		=> $record->id,
	           	]);
	           	$buttons .= '&nbsp;';
	            $buttons .= $this->makeButton([
	                'type' 		=> 'edit',
	                'label' 	=> '<i class="fa fa-print text-info"></i>',
	                'tooltip' 	=> 'Resume',
	                'class' 	=> 'cetak-resume button',
	                'id'   		=> $record->id,
	           	]);
	           	$buttons .= '&nbsp;';
	            $buttons .= $this->makeButton([
	                'type' 		=> 'edit',
	                'label' 	=> '<i class="fa fa-print text-info"></i>',
	                'tooltip' 	=> 'Surat Pengantar',
	                'class' 	=> 'cetak-surat button',
	                'id'   		=> $record->id,
	           	]);
	           	$buttons .= '&nbsp;';
	            $buttons .= $this->makeButton([
	                'type' 		=> 'edit',
	                'label' 	=> '<i class="fa fa-print text-info"></i>',
	                'tooltip' 	=> 'Surat Disposisi',
	                'class' 	=> 'cetak-disposisi button',
	                'id'   		=> $record->id,
	           	]);
               return $buttons;
           })
           ->rawColumns(['tipe','detil','penugasan','tinjauan','detil_program','object_audit','nama','status','detil_lha','detil_draft','kategori','action'])
           ->make(true);
    }

    public function index()
    {
        $user = auth()->user();
        if($user->hasJabatan(['SVP - Internal Audit','President Director','Secretary', 'auditor'])){
            $nbaru = DraftKka::where('status', 3)->get()->count();

            $nprog = LHA::whereIn('status', [1,2,3,4,5])->get()->count();
        }else{
            $nbaru = DraftKka::where('id', 0)->get()->count();
            $nprog = LHA::where('id', 0)->get()->count();
        }
        return $this->render('modules.audit.pelaporan.lha.index', [
            'mockup' => true,
            'structs' => $this->listStructs,
            'nbaru' => $nbaru,
            'nprog' => $nprog,
        ]);
    }

    public function store(LHARequest $request)
    {
        DB::beginTransaction();
        try {
            $lha                = new LHA;
            if($request->status_proyek == 1){
            	$lha->draft_id  			= $request->draft_id;
	            $lha->tanggal   			= $request->tanggal;
	            $lha->memo      			= $request->memo;
	            $lha->pemilik      			= $request->pemilik;
	            $lha->sumber      			= $request->sumber;
	            $lha->sifat      			= $request->sifat;
	            $lha->nomor_spmk      		= $request->nomor_spmk;
	            $lha->nilai_kontrak      	= $request->nilai_kontrak;
	            $lha->tanggal_kontrak      	= $request->tanggal_kontrak;
	            $lha->tanggal_akhir_kontrak = $request->tanggal_akhir_kontrak;
	            $lha->waktu_pelaksanaaan    = $request->waktu_pelaksanaaan;
	            $lha->waktu_pemeliharaan    = $request->waktu_pemeliharaan;
	            $lha->pembayaran      		= $request->pembayaran;
	            $lha->konsultan_perencana   = $request->konsultan_perencana;
	            $lha->konsultan_pengawas    = $request->konsultan_pengawas;
	            $lha->bkpu      			= $request->bkpu;
	            $lha->progress      		= $request->progress;
	            $lha->pu      				= $request->pu;
	            $lha->cash      			= $request->cash;
	            $lha->piutang      			= $request->piutang;
	            $lha->tagihan      			= $request->tagihan;
	            $lha->tgl_bkpu      		= $request->tgl_bkpu;
	            $lha->tgl_progress      	= $request->tgl_progress;
	            $lha->tgl_pu      			= $request->tgl_pu;
	            $lha->tgl_cash      		= $request->tgl_cash;
	            $lha->tgl_piutang      		= $request->tgl_piutang;
	            $lha->tgl_tagihan      		= $request->tgl_tagihan;
            }else{
	            $lha->draft_id  = $request->draft_id;
	            $lha->tanggal   = $request->tanggal;
	            $lha->memo      = $request->memo;
            }

            if($request->status == 1){
                $lha->status        = 1;
                $final              = DraftKka::find($request->draft_id);
                $final->status      = 6;
                $final->save();
            }else{
                $lha->status        = 0;
            }
            $lha->save();
            $lha->saveAdendum($request->adendum);
            $lha->saveLingkup($request->lingkup);

            $user = auth()->user();
            $log = new SessionLog;
            $log->modul = 'Kegiatan Audit Pelaporan';
            $log->sub = 'LHA';
            $log->aktivitas = 'Membuat LHA Tahun '.$lha->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $lha->draftkka->program->penugasanaudit->rencanadetail->object_id);
            $log->user_id = $user->id;
            $log->save();

            if($request->status == 1){
                $update_notif = Notification::where('parent_id', $request->draft_id)->where('modul', 'draft-kka')->where('stage', 0)->get();
                foreach ($update_notif as $key => $vil) {
                    $data_notif = Notification::find($vil->id);
                    $data_notif->status =0;
                    $data_notif->save();
                }
                $userz = User::whereHas('roles', function($u){
                                $u->where('name', 'svp-audit');
                            })->get();
                foreach ($userz as $key => $vul) {
                    $notif = new Notification;
                    $notif->user_id = $vul->id;
                    $notif->parent_id = $lha->id;
                    $notif->url = route($this->routes.'.index');
                    $notif->modul = 'lha';
                    $notif->stage = 0;
                    $notif->keterangan = 'Approval LHA Tahun '.$lha->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $lha->draftkka->program->penugasanaudit->rencanadetail->object_id);
                    $notif->status = 1;
                    $notif->save();
                }
            }

            DB::commit();
            return response([
              'status' => true
            ]);
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function detil(TinjauanDokumen $id)
    {
        $cari = ProgramAudit::where('tinjauan_id', $id->id)->first();
        $this->setBreadcrumb(['Kegiatan Audit' => '#', 'Pelaporan' => '#', 'LHA' => '#', 'Detil' => '#']);
        return $this->render('modules.audit.pelaporan.lha.detil', [
            'program' => $cari,
            'record' => $id,
        ]);
    }

    public function buat(DraftKka $id)
    {
    	$edisi = Edisi::where('status', 1)->first();
        $this->setBreadcrumb(['Kegiatan Audit' => '#', 'Pelaporan' => '#', 'LHA' => '#', 'Buat' => '#']);
        return $this->render('modules.audit.pelaporan.lha.create', [
            'record' => $id,
            'edisi' => $edisi,
        ]);
    }

    public function ubah(LHA $id)
    {
    	$edisi = Edisi::where('status', 1)->first();
        $this->setBreadcrumb(['Kegiatan Audit' => '#', 'Pelaporan' => '#', 'LHA' => '#', 'Ubah' => '#']);
        return $this->render('modules.audit.pelaporan.lha.edit', [
            'record' => $id,
            'data' => $id,
            'edisi' => $edisi,
        ]);
    }

    public function resume(LHA $id)
    {
    	$edisi = Edisi::where('status', 1)->first();
        $this->setBreadcrumb(['Kegiatan Audit' => '#', 'Pelaporan' => '#', 'LHA' => '#', 'Resume' => '#']);
        return $this->render('modules.audit.pelaporan.lha.resume', [
            'record' => $id,
            'data' => $id,
            'edisi' => $edisi,
        ]);
    }

    public function update(LHARequest $request)
    {
        DB::beginTransaction();
        try {
            $lha                = LHA::find($request->id);
            if(!empty($request->exist_adendum)){
	            if(count($request->exist_adendum) > 0){
		            $hapus_adendum = LHAAdendum::where('lha_id', $request->id)
		                        					->delete();
	            }
            }

            if(!empty($request->exist_lingkup)){
	            if(count($request->exist_lingkup) > 0){
		            $hapus_lingkup = LHALingkup::where('lha_id', $request->id)
		                        					->delete();
	            }
            }

           	if($request->status_proyek == 1){
	            $lha->tanggal   			= $request->tanggal;
	            $lha->memo      			= $request->memo;
	            $lha->pemilik      			= $request->pemilik;
	            $lha->sumber      			= $request->sumber;
	            $lha->sifat      			= $request->sifat;
	            $lha->nomor_spmk      		= $request->nomor_spmk;
	            $lha->nilai_kontrak      	= $request->nilai_kontrak;
	            $lha->tanggal_kontrak      	= $request->tanggal_kontrak;
	            $lha->tanggal_akhir_kontrak = $request->tanggal_akhir_kontrak;
	            $lha->waktu_pelaksanaaan    = $request->waktu_pelaksanaaan;
	            $lha->waktu_pemeliharaan    = $request->waktu_pemeliharaan;
	            $lha->pembayaran      		= $request->pembayaran;
	            $lha->konsultan_perencana   = $request->konsultan_perencana;
	            $lha->konsultan_pengawas    = $request->konsultan_pengawas;
	            $lha->bkpu      			= $request->bkpu;
	            $lha->progress      		= $request->progress;
	            $lha->pu      				= $request->pu;
	            $lha->cash      			= $request->cash;
	            $lha->piutang      			= $request->piutang;
	            $lha->tagihan      			= $request->tagihan;
	            $lha->tgl_bkpu      		= $request->tgl_bkpu;
	            $lha->tgl_progress      	= $request->tgl_progress;
	            $lha->tgl_pu      			= $request->tgl_pu;
	            $lha->tgl_cash      		= $request->tgl_cash;
	            $lha->tgl_piutang      		= $request->tgl_piutang;
	            $lha->tgl_tagihan      		= $request->tgl_tagihan;
            }else{
	            $lha->tanggal   = $request->tanggal;
	            $lha->memo      = $request->memo;
            }

            if($request->status == 1){
                $lha->status        = 1;
                $lha->ket_svp       = null;
                $draft              = DraftKka::find($lha->draft_id);
                $draft->status      = 6;
                $draft->save();
            }else{
                $lha->status            = 0;
            }
            $lha->save();
            $lha->saveAdendum($request->adendum);
            $lha->saveLingkup($request->lingkup);

            $user = auth()->user();
            $log = new SessionLog;
            $log->modul = 'Kegiatan Audit Pelaporan';
            $log->sub = 'LHA';
            $log->aktivitas = 'Mengubah LHA Tahun '.$lha->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $lha->draftkka->program->penugasanaudit->rencanadetail->object_id);
            $log->user_id = $user->id;
            $log->save();

            if($request->status == 1){
                $update_notif = Notification::where('parent_id', $lha->draft_id)->where('modul', 'draft-kka')->where('stage', 0)->get();
                foreach ($update_notif as $key => $vil) {
                    $data_notif = Notification::find($vil->id);
                    $data_notif->status =0;
                    $data_notif->save();
                }
                $userz = User::whereHas('roles', function($u){
                                $u->where('name', 'svp-audit');
                            })->get();
                foreach ($userz as $key => $vul) {
                    $notif = new Notification;
                    $notif->user_id = $vul->id;
                    $notif->parent_id = $lha->id;
                    $notif->url = route($this->routes.'.index');
                    $notif->modul = 'lha';
                    $notif->stage = 0;
                    $notif->keterangan = 'Approval LHA Tahun '.$lha->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($lha->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $lha->draftkka->program->penugasanaudit->rencanadetail->object_id);
                    $notif->status = 1;
                    $notif->save();
                }
            }

            DB::commit();
            return response([
              'status' => true
            ]);
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function approveTim($id)
    {
        DB::beginTransaction();
        try {
            $record = LHA::find($id);
            $record->status = 3;
            $record->save();

            $user = auth()->user();
            $log = new SessionLog;
            $log->modul = 'Kegiatan Audit Pelaporan';
            $log->sub = 'LHA';
            $log->aktivitas = 'Menyetujui LHA Tahun '.$record->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id);
            $log->user_id = $user->id;
            $log->save();

            $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'lha')->where('stage', 0)->get();
            foreach ($update_notif as $key => $vil) {
                $data_notif = Notification::find($vil->id);
                $data_notif->status =0;
                $data_notif->save();
            }
            $userz = User::whereHas('roles', function($u){
                            $u->where('name', 'sekre-ia');
                        })->get();
            foreach ($userz as $key => $vul) {
                $notif = new Notification;
                $notif->user_id = $vul->id;
                $notif->parent_id = $record->id;
                $notif->url = route($this->routes.'.index');
                $notif->modul = 'lha';
                $notif->stage = 1;
                $notif->keterangan = 'Pembuatan Resume LHA Tahun '.$record->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id);
                $notif->status = 1;
                $notif->save();
            }

            DB::commit();
            return response([
              'status' => true
            ]);
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function approveSvp2($id)
    {
        DB::beginTransaction();
        try {
            $record = LHA::find($id);
            $record->status = 4;
            $record->save();

            $user = auth()->user();
            $log = new SessionLog;
            $log->modul = 'Kegiatan Audit Pelaporan';
            $log->sub = 'LHA';
            $log->aktivitas = 'Menyetujui LHA Tahun '.$record->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id);
            $log->user_id = $user->id;
            $log->save();

            $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'lha')->where('stage', 2)->get();
            foreach ($update_notif as $key => $vil) {
                $data_notif = Notification::find($vil->id);
                $data_notif->status =0;
                $data_notif->save();
            }
            $userz = User::whereHas('roles', function($u){
                            $u->where('name', 'sekre-ia');
                        })->get();
            foreach ($userz as $key => $vul) {
                $notif = new Notification;
                $notif->user_id = $vul->id;
                $notif->parent_id = $record->id;
                $notif->url = route($this->routes.'.index');
                $notif->modul = 'lha';
                $notif->stage = 3;
                $notif->keterangan = 'Upload Dokumen LHA Tahun '.$record->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id);
                $notif->status = 1;
                $notif->save();
            }

            DB::commit();
            return response([
              'status' => true
            ]);
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }


    public function approveDirut($id)
    {
        DB::beginTransaction();
        try {
            $record = LHA::find($id);
            $record->status = 5;
            $record->save();
            
            $user = auth()->user();
            $log = new SessionLog;
            $log->modul = 'Kegiatan Audit Pelaporan';
            $log->sub = 'LHA';
            $log->aktivitas = 'Menyetujui LHA Tahun '.$record->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id);
            $log->user_id = $user->id;
            $log->save();

            //SEMENTARA
            //
            //
            $survey = Survey::where('status', 1)->first();
            $lha = LHA::find($id);

            if($survey){
                $pic_object_audit = object_user($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id);
                foreach ($pic_object_audit[0] as $pic_audit) {
                    if (SurveyJawab::where('lha_id', $lha->id)->where('user_id', $pic_audit)->first() == null) {
                        $survey_jawab = new SurveyJawab;
                        $survey_jawab->lha_id = $lha->id;
                        $survey_jawab->survei_id = $survey->id;
                        $survey_jawab->user_id = $pic_audit;
                        $survey_jawab->save();
                    }
                }
            }

            $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'lha')->where('stage', 4)->get();
            foreach ($update_notif as $key => $vil) {
                $data_notif = Notification::find($vil->id);
                $data_notif->status =0;
                $data_notif->save();
            }

            DB::commit();
            return response([
              'status' => true
            ]);
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function reject(lHA $id)
    {
        return $this->render('modules.audit.pelaporan.lha.reject-tim', [
            'record' => $id,
        ]);
    }

    public function reject2(lHA $id)
    {
        return $this->render('modules.audit.pelaporan.lha.reject-svp2', [
            'record' => $id,
        ]);
    }

    public function rejectdirut(lHA $id)
    {
        return $this->render('modules.audit.pelaporan.lha.reject-dirut', [
            'record' => $id,
        ]);
    }

    public function saveReject(RejectSvpRequest $request)
    {
        DB::beginTransaction();
        try {
            $record                         = lHA::find($request->id);
            $record->status                 = 1;
            $record->ket_svp                = $request->ket_svp;
            $record->save();

            $rencana                        = DraftKka::find($record->draft_id);
            $rencana->status                = 4;
            $rencana->save();

            $user = auth()->user();
            $log = new SessionLog;
            $log->modul = 'Kegiatan Audit Pelaporan';
            $log->sub = 'LHA';
            $log->aktivitas = 'Menolak LHA Tahun '.$record->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id);
            $log->user_id = $user->id;
            $log->save();

            $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'lha')->where('stage', 0)->get();
            foreach ($update_notif as $key => $vil) {
                $data_notif = Notification::find($vil->id);
                $data_notif->status =0;
                $data_notif->save();
            }
            $userz = User::whereHas('roles', function($u){
                            $u->where('name', 'auditor');
                        })->get();
            foreach ($userz as $key => $vul) {
                $notif = new Notification;
                $notif->user_id = $vul->id;
                $notif->parent_id = $record->id;
                $notif->url = route($this->routes.'.index');
                $notif->modul = 'lha';
                $notif->stage = 3;
                $notif->keterangan = 'Reject SVP LHA Tahun '.$record->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id);
                $notif->status = 1;
                $notif->save();
            }

            DB::commit();
            return response([
              'status' => true
            ]);
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function saveReject2(RejectSvpRequest $request)
    {
        DB::beginTransaction();
        try {
            $record                         = lHA::find($request->id);
            $record->status                 = 2;
            $record->ket_svp                = $request->ket_svp;
            $record->save();

            $user = auth()->user();
            $log = new SessionLog;
            $log->modul = 'Kegiatan Audit Pelaporan';
            $log->sub = 'LHA';
            $log->aktivitas = 'Menolak LHA Tahun '.$record->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id);
            $log->user_id = $user->id;
            $log->save();

            $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'lha')->where('stage', 2)->get();
            foreach ($update_notif as $key => $vil) {
                $data_notif = Notification::find($vil->id);
                $data_notif->status =0;
                $data_notif->save();
            }
            $userz = User::whereHas('roles', function($u){
                            $u->where('name', 'sekre-ia');
                        })->get();
            foreach ($userz as $key => $vul) {
                $notif = new Notification;
                $notif->user_id = $vul->id;
                $notif->parent_id = $record->id;
                $notif->url = route($this->routes.'.index');
                $notif->modul = 'lha';
                $notif->stage = 1;
                $notif->keterangan = 'Reject SVP Resume LHA Tahun '.$record->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id);
                $notif->status = 1;
                $notif->save();
            }

            DB::commit();
            return response([
              'status' => true
            ]);
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function saveRejectDirut(RejectDirutRequest $request)
    {
        DB::beginTransaction();
        try {
            $record                         = lHA::find($request->id);
            $record->status                 = 3;
            $record->ket_svp                = $request->ket_svp;
            $record->save();

            $user = auth()->user();
            $log = new SessionLog;
            $log->modul = 'Kegiatan Audit Pelaporan';
            $log->sub = 'LHA';
            $log->aktivitas = 'Menolak LHA Tahun '.$record->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id);
            $log->user_id = $user->id;
            $log->save();

            $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'lha')->where('stage', 4)->get();
            foreach ($update_notif as $key => $vil) {
                $data_notif = Notification::find($vil->id);
                $data_notif->status =0;
                $data_notif->save();
            }
            $userz = User::whereHas('roles', function($u){
                            $u->where('name', 'sekre-ia');
                        })->get();
            foreach ($userz as $key => $vul) {
                $notif = new Notification;
                $notif->user_id = $vul->id;
                $notif->parent_id = $record->id;
                $notif->url = route($this->routes.'.index');
                $notif->modul = 'lha';
                $notif->stage = 3;
                $notif->keterangan = 'Reject Dirut Dokumen LHA Tahun '.$record->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id);
                $notif->status = 1;
                $notif->save();
            }

            DB::commit();
            return response([
              'status' => true
            ]);
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function uploadDokumen(LHA $id)
    {
        $this->setBreadcrumb(['Kegiatan Audit' => '#', 'Pelaporan' => '#', 'LHA' => '#', 'Upload Dokumen' => '#']);
        return $this->render('modules.audit.pelaporan.lha.upload-dokumen',[
            'record' => $id,
        ]);
    }

    public function simpanDokumen(DokumenRequest $request)
    {
        DB::beginTransaction();
        try {
            $record             = LHA::find($request->id);
            $record->status     = 6;
            $record->save();
            $record->multipleFileUploads($request->berkas);
            $user = auth()->user();
            $log = new SessionLog;
            $log->modul = 'Kegiatan Audit Pelaporan';
            $log->sub = 'LHA';
            $log->aktivitas = 'Menyelesaikan LHA Tahun '.$record->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id);
            $log->user_id = $user->id;
            $log->save();

            $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'lha')->where('stage', 3)->get();
            foreach ($update_notif as $key => $vil) {
                $data_notif = Notification::find($vil->id);
                $data_notif->status =0;
                $data_notif->save();
            }
            $userz = User::whereHas('roles', function($u){
                            $u->where('name', 'dirut');
                        })->get();
            foreach ($userz as $key => $vul) {
                $notif = new Notification;
                $notif->user_id = $vul->id;
                $notif->parent_id = $record->id;
                $notif->url = route($this->routes.'.index');
                $notif->modul = 'lha';
                $notif->stage = 4;
                $notif->keterangan = 'Approval Dokumen LHA Tahun '.$record->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id);
                $notif->status = 1;
                $notif->save();
            }

            $rencanadetail = RencanaAuditDetail::find($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->id);
            $rencanadetail->status_lha = 1;
            $rencanadetail->save();

            DB::commit();
            return response([
              'status' => true
            ]);
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function simpanResume(ResumeRequest $request)
    {
        DB::beginTransaction();
        try {
            $record             = LHA::find($request->id);

            $hapus = LHADetail::where('lha_id', $request->id)
                                ->delete();
            $hapuss = LHAKesimpulan::where('lha_id', $request->id)
                                ->delete();

            if($request->status > 0){
	            $record->status     = 2;
            }else{
            	$record->status     = 1;
            }
            $record->save();
            $record->saveDetail($request->detail);
            $record->saveKesimpulan($request->data);

            $user = auth()->user();
            $log = new SessionLog;
            $log->modul = 'Kegiatan Audit Pelaporan';
            $log->sub = 'LHA';
            $log->aktivitas = 'Membuat Resume LHA Tahun '.$record->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id);
            $log->user_id = $user->id;
            $log->save();

            $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'lha')->where('stage', 1)->get();
            foreach ($update_notif as $key => $vil) {
                $data_notif = Notification::find($vil->id);
                $data_notif->status =0;
                $data_notif->save();
            }
            $userz = User::whereHas('roles', function($u){
                            $u->where('name', 'svp-audit');
                        })->get();
            foreach ($userz as $key => $vul) {
                $notif = new Notification;
                $notif->user_id = $vul->id;
                $notif->parent_id = $record->id;
                $notif->url = route($this->routes.'.index');
                $notif->modul = 'lha';
                $notif->stage = 2;
                $notif->keterangan = 'Approval Resume LHA Tahun '.$record->draftkka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->draftkka->program->penugasanaudit->rencanadetail->tipe_object, $record->draftkka->program->penugasanaudit->rencanadetail->object_id);
                $notif->status = 1;
                $notif->save();
            }

            DB::commit();
            return response([
              'status' => true
            ]);
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function cetak(Request $request, $id){
        $record = DraftKka::find($id);
        $data = [
            'records' => $record,
        ];
        $pdf = PDF::loadView('modules.audit.pelaporan.lha.cetak', $data);
        return $pdf->stream('LHA '.$record->tanggal.'.pdf',array("Attachment" => false));
    }

    public function cetakLHA(Request $request, $id){
    	$edisi = Edisi::where('status', 1)->first();
        $record = LHA::find($id);
        $data = [
            'records' => $record,
            'edisi' => $edisi,
        ];
        $pdf = PDF::loadView('modules.audit.pelaporan.lha.cetak-lha', $data);
        return $pdf->stream('LHA '.$record->tanggal.'.pdf',array("Attachment" => false));
    }

    public function cetakSuratDisposisi(Request $request, $id){
    	$edisi = Edisi::where('status', 1)->first();
        $record = LHA::find($id);
        $data = [
            'records' => $record,
            'edisi' => $edisi,
        ];
        $pdf = PDF::loadView('modules.audit.pelaporan.lha.cetak-disposisi', $data);
        return $pdf->stream('Surat Disposisi TL '.$record->tanggal.'.pdf',array("Attachment" => false));
    }

    public function cetakSurat(Request $request, $id){
    	$edisi = Edisi::where('status', 1)->first();
        $record = LHA::find($id);
        $data = [
            'records' => $record,
            'edisi' => $edisi,
        ];
        $pdf = PDF::loadView('modules.audit.pelaporan.lha.cetak-surat', $data);
        return $pdf->stream('Surat Pengantar '.$record->tanggal.'.pdf',array("Attachment" => false));
    }

    public function cetakResume(Request $request, $id){
    	$edisi = Edisi::where('status', 1)->first();
        $record = LHA::find($id);
        $data = [
            'records' => $record,
            'edisi' => $edisi,
        ];
        $pdf = PDF::loadView('modules.audit.pelaporan.lha.cetak-resume', $data);
        return $pdf->stream('Resume '.$record->tanggal.'.pdf',array("Attachment" => false));
    }

    public function destroy(ProgramAudit $program)
    {
        $program->delete();
        return response()->json([
            'success' => true
        ]);
    }

    public function detilProject(RencanaAuditDetail $id)
    {
        return $this->render('modules.audit.pelaporan.lha.detil-project', ['record' => $id]);
    }

    public function detilPenugasan(PenugasanAudit $id)
    {
        return $this->render('modules.audit.pelaporan.lha.detil-penugasan', ['record' => $id]);
    }

    public function detilTinjauan(TinjauanDokumen $id)
    {
        if($id->group > 0){
            $cari = TinjauanDokumen::where('group', $id->group)->get();
        }else{
            $cari[]= $id;
        }
        return $this->render('modules.audit.pelaporan.lha.detil-tinjauan', [
            'record' => $id,
            'cek' => $cari,
        ]);
    }

    public function detilProgram(ProgramAudit $id)
    {
        return $this->render('modules.audit.pelaporan.lha.detil-program', ['record' => $id]);
    }

    public function detilDraft(DraftKka $id)
    {
        return $this->render('modules.audit.pelaporan.lha.detil-draft', ['record' => $id]);
    }

    public function detilLha(LHA $id)
    {
    	$edisi = Edisi::where('status', 1)->first();
        return $this->render('modules.audit.pelaporan.lha.detil-lha', [
            'record' => $id,
            'data' => $id,
            'edisi' => $edisi,
        ]);
    }

    public function detilResume(LHA $id)
    {
    	$edisi = Edisi::where('status', 1)->first();
        return $this->render('modules.audit.pelaporan.lha.detil-resume', [
            'record' => $id,
            'data' => $id,
            'edisi' => $edisi,
        ]);
    }

    public function cetakAudit(Request $request, $id){
        $record = RencanaAudit::find($id);
        $data = [
            'records' => $record,
        ];
        $pdf = PDF::loadView('modules.audit.pelaporan.lha.cetak-audit', $data)->setPaper('a4', 'landscape');
        return $pdf->stream('Rencana Audit.pdf',array("Attachment" => false));
    }

    public function cetakPenugasan(Request $request, $id){
        $record = PenugasanAudit::find($id);
        $data = [
            'records' => $record,
        ];

        $merge = new Merger();

        $pdf = PDF::loadView('modules.audit.pelaporan.lha.cetak-penugasan', $data);
        $merge->addRaw($pdf->output());
        $pdf2 = PDF::loadView('modules.audit.pelaporan.lha.cetak-jadwal', $data);
        $pdf2->setPaper('a4', 'landscape');
        $merge->addRaw($pdf2->output());

        return new Response($merge->merge(), 200, array(
            'Content-Type' => 'application/pdf',
            'Content-Disposition' =>  'inline; filename="Penugasan Audit.pdf"',
        ));
    }

    public function downloadAudit($id)
    {
        $daftar = RencanaAudit::find($id);
        if(file_exists(public_path('storage/'.$daftar->bukti)))
        {
            return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
        }
        return abort(404);
    }

    public function downloadPenugasan($id)
    {
        $daftar = PenugasanAudit::find($id);
        if(file_exists(public_path('storage/'.$daftar->bukti)))
        {
            return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
        }
        return abort(404);
    }

    public function downloadProgramAudit($id)
    {
        $daftar = ProgramAudit::find($id);
        if(file_exists(public_path('storage/'.$daftar->lampiran)))
        {
            return response()->download(public_path('storage/'.$daftar->lampiran), $daftar->lampiranname);
        }
        return abort(404);
    }
}
