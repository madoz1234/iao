<?php

namespace App\Http\Controllers\KegiatanAudit\Pelaksanaan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaRequest;

use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit;
use App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumen;
use App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka;
use App\Models\KegiatanAudit\Pelaksanaan\FinalKka\FinalKka;
use App\Models\KegiatanAudit\Pelaksanaan\ClosingMeeting\ClosingMeeting;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaDetail;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\Auths\User;
use App\Models\Files;
use App\Models\SessionLog;
use App\Models\Notification;

use DB;
use PDF;

class FinalKkaController extends Controller
{	
    protected $routes = 'kegiatan-audit.pelaksanaan.final-kka';

    public function __construct()
    {	
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Pelaksanaan Audit' => '#', 'Final Kka' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            [
                'data' => 'tahun',
                'name' => 'tahun',
                'label' => 'Tahun',
                'className' => 'text-center',
                'width' => '150px',
                'sortable' => true,
            ],
            [
                'data' => 'tipe',
                'name' => 'tipe',
                'label' => 'Tipe',
                'className' => 'text-center',
                'width' => '200px',
                'sortable' => true,
            ],
            [
                'data' => 'object_audit',
                'name' => 'object_audit',
                'label' => 'Objek Audit',
                'className' => 'text-center',
                'width' => '200px',
                'sortable' => true,
            ],
            [
                'data' => 'rencana',
                'name' => 'rencana',
                'label' => 'Rencana Pelaksanaan',
                'className' => 'text-center',
                'width' => '200px',
            ],
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama Project',
                'className' => 'text-center',
                'width' => '200px',
                'sortable' => true,
            ],
            [
                'data' => 'detil',
                'name' => 'detil',
                'label' => 'Detil Project',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'detil_penugasan',
                'name' => 'detil_penugasan',
                'label' => 'Detil Penugasan',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'detil_tinjauan',
                'name' => 'detil_tinjauan',
                'label' => 'Detil Tinjauan',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'detil_program',
                'name' => 'detil_program',
                'label' => 'Detil Program',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'detil_draft',
                'name' => 'detil_draft',
                'label' => 'Detil Draft KKA',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'created_by',
                'name' => 'created_by',
                'label' => 'Dibuat Oleh',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '150px',
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '150px',
            ],
            [
	            'data' => 'status',
	            'name' => 'status',
	            'label' => 'Status',
	            'searchable' => false,
	            'sortable' => false,
	            'width' => '100px',
	            'className' => 'text-center'
	        ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ],
        ]);
    }

    public function grid()
    {
    	$user = auth()->user();
    	if($user->hasRole(['kapro'])){
       	 	$records = FinalKka::whereHas('closing.draft.opening.program.tinjauan.penugasanaudit.rencanadetail', function($a){
       	 							$a->where('tipe_object', 2);
       	 						})->select('*');
      	}elseif($user->hasRole(['svp'])){
       	 	$records = FinalKka::whereHas('closing.draft.opening.program.tinjauan.penugasanaudit.rencanadetail', function($a){
       	 							$a->where('tipe_object', '!=', 2);
       	 						})->select('*');
      	}else{
      		$records = FinalKka::where('id', 0);
      	}
        return DataTables::of($records->get())
           ->addColumn('num', function ($record) {
               return request()->start;
           })
           ->addColumn('tahun', function ($record) {
               return $record->closing->draft->opening->program->tinjauan->penugasanaudit->rencanadetail->rencanaaudit->tahun;
           })
           ->addColumn('tipe', function ($record) {
                $tipe = '';
                if($record->closing->draft->opening->program->tinjauan->penugasanaudit->rencanadetail->rencanaaudit->tipe == 0){
               	  $tipe = '<span class="label label-primary">Audit Reguler</span>';
                }else{
               	  $tipe = '<span class="label label-success">Audit Khusus</span>';
                }
               	
                return $tipe;
           })
           ->addColumn('object_audit', function ($record) {
               return object_audit($record->closing->draft->opening->program->tinjauan->penugasanaudit->rencanadetail->tipe_object, $record->closing->draft->opening->program->tinjauan->penugasanaudit->rencanadetail->object_id);
           })
          ->addColumn('detil', function ($record) {
               $buttons = '';
           	   $buttons .= $this->makeButton([
           			'type' 		=> 'edit',
           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
           			'tooltip' 	=> 'Detil',
           			'class' 	=> 'programaudit-detil button',
           			'id'   		=> $record->closing->draft->opening->program->tinjauan->id,
           	   ]);
               return $buttons;
           })
           ->addColumn('rencana', function ($record) {
                   return $record->closing->draft->opening->program->tinjauan->penugasanaudit->rencanadetail->rencana;
           })
           ->addColumn('nama', function ($record) {
           		if($record->closing->draft->opening->program->tinjauan->penugasanaudit->rencanadetail->tipe_object == 2){
               		return object_audit($record->closing->draft->opening->program->tinjauan->penugasanaudit->rencanadetail->tipe_object, $record->closing->draft->opening->program->tinjauan->penugasanaudit->rencanadetail->object_id);
               	}else{
               		return '';
               	}
           })
           ->addColumn('detil', function ($record) {
               $buttons = '';
               if($record->closing->draft->opening->program->tinjauan->penugasanaudit->rencanadetail->tipe_object == 2){
	               $buttons .= $this->makeButton([
	           			'type' 		=> 'edit',
	           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
	           			'tooltip' 	=> 'Detil Project',
	           			'class' 	=> 'detil-project button',
	           			'id'   		=> $record->closing->draft->opening->program->tinjauan->penugasanaudit->rencanadetail->id,
	           		]);
               }else{
               	 $buttons .= '';
               }
               return $buttons;
           })
           ->editColumn('status', function ($record) {
           		if($record->status == 0){
	               return '<span class="label label-info">Menunggu Approval</span>';
	           }else{
	           		return '<span class="label label-success">Disetujui</span>';
	           }
           })
           ->addColumn('detil_penugasan', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
           			'type' 		=> 'edit',
           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
           			'tooltip' 	=> 'Detil Penugasan',
           			'class' 	=> 'detil-penugasan button',
           			'id'   		=> $record->closing->draft->opening->program->tinjauan->penugasanaudit->id,
           		]);
               return $buttons;
           })
           ->addColumn('detil_tinjauan', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
           			'type' 		=> 'edit',
           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
           			'tooltip' 	=> 'Detil Tinjauan',
           			'class' 	=> 'detil-tinjauan button',
           			'id'   		=> $record->closing->draft->opening->program->tinjauan->id,
           		]);
               
               return $buttons;
           })
           ->addColumn('detil_program', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
           			'type' 		=> 'edit',
           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
           			'tooltip' 	=> 'Detil Program Audit',
           			'class' 	=> 'detil-program button',
           			'id'   		=> $record->closing->draft->opening->program->id,
           		]);
               
               return $buttons;
           })
           ->addColumn('detil_draft', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
           			'type' 		=> 'edit',
           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
           			'tooltip' 	=> 'Detil Draft KKA',
           			'class' 	=> 'detil-draft button',
           			'id'   		=> $record->closing->draft->id,
           		]);
               
               return $buttons;
           })
           ->editColumn('created_at', function ($record) {
               return $record->created_at->diffForHumans();
           })
           ->editColumn('created_by', function ($record) {
               return $record->creatorName();
           })
           ->addColumn('action', function ($record) {
               $buttons = '';
               if($record->status == 0){
	               $buttons .= $this->makeButton([
	                    'type' => 'edit',
	                    'class' => 'm-l approve button',
	                    'label' => '<i class="fa fa-check text-info"></i>',
	                    'tooltip' => 'Approval',
	                    'id'   => $record->id,
	               ]);
           		}else{
	               $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-print text-info"></i>',
	                    'tooltip' 	=> 'Cetak',
	                    'class' 	=> 'cetak button',
	                    'id'   		=> $record->id,
	               ]);
               }
               return $buttons;
           })
           ->rawColumns(['tipe', 'action','detil','detil_penugasan','detil_tinjauan','detil_program','detil_project','detil_draft','object_audit','nama','status'])
           ->make(true);
    }

    public function index()
    {
    	return $this->render('modules.audit.pelaksanaan.final-kka.index');
    }

    public function approve(Request $request)
    {
    	DB::beginTransaction();
        try {
        	$record = FinalKka::find($request->id);
        	$record->status =1;
        	$record->save();
	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(ProgramAudit $program)
    {
        $program->delete();
        return response()->json([
            'success' => true
        ]);
    }

    public function detilProject(RencanaAuditDetail $id)
    {
        return $this->render('modules.audit.pelaksanaan.final-kka.detil-project', ['record' => $id]);
    }

    public function detilPenugasan(PenugasanAudit $id)
    {
        return $this->render('modules.audit.pelaksanaan.final-kka.detil-penugasan', ['record' => $id]);
    }

    public function detilTinjauan(TinjauanDokumen $id)
    {
        return $this->render('modules.audit.pelaksanaan.final-kka.detil-tinjauan', ['record' => $id]);
    }

    public function detilProgram(ProgramAudit $id)
    {
        return $this->render('modules.audit.pelaksanaan.final-kka.detil-program', ['record' => $id]);
    }

    public function detilDraft(DraftKka $id)
    {
        return $this->render('modules.audit.pelaksanaan.final-kka.detil-draft', ['record' => $id]);
    }
}
