<?php

namespace App\Http\Controllers\KegiatanAudit\Pelaksanaan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Response;
use iio\libmergepdf\Merger;

use App\Http\Requests\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaRequest;
use App\Http\Requests\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaUbahRequest;
use App\Http\Requests\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaUploadRequest;
use App\Http\Requests\KegiatanAudit\Rencana\RejectRequest;
use App\Models\KegiatanAudit\Rencana\RencanaAudit;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit;
use App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumen;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaDetail;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\KegiatanAudit\TindakLanjut\RegisterTL;
use App\Models\KegiatanAudit\TindakLanjut\RegisterTLDetail;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaDetailRekomendasi;
use App\Models\Auths\User;
use App\Models\Files;
use App\Models\SessionLog;
use App\Models\Notification;

use DB;
use PDF;

class DraftKkaController extends Controller
{
    protected $routes = 'kegiatan-audit.pelaksanaan.draft-kka';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Kegiatan Audit' => '#', 'Pelaksanaan' => '#', 'KKA' => '#']);
        $this->listStructs = [
        	'listStruct' => [
		        	[
		                'data' => 'num',
		                'name' => 'num',
		                'label' => '#',
		                'orderable' => false,
		                'searchable' => false,
		                'className' => 'text-center',
		                'width' => '20px',
		            ],
		            [
		                'data' => 'tahun',
		                'name' => 'tahun',
		                'label' => 'Tahun',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'rencana',
		                'name' => 'rencana',
		                'label' => 'Waktu Pelaksanaan',
		                'className' => 'text-center',
		                'width' => '300px',
		            ],
		            [
		                'data' => 'tipe',
		                'name' => 'tipe',
		                'label' => 'Tipe',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'kategori',
		                'name' => 'kategori',
		                'label' => 'Kategori',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'object_audit',
		                'name' => 'object_audit',
		                'label' => 'Objek Audit',
		                'className' => 'text-left',
		                'width' => '250px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'no_ab',
		                'name' => 'no_ab',
		                'label' => 'No AB',
		                'className' => 'text-center',
		                'width' => '150px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'detil',
		                'name' => 'detil',
		                'label' => 'Rencana Kerja',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'penugasan',
		                'name' => 'penugasan',
		                'label' => 'Surat Penugasan',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'tinjauan',
		                'name' => 'tinjauan',
		                'label' => 'Tinjauan Dokumen',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'detil_program',
		                'name' => 'detil_program',
		                'label' => 'Program Audit',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'tim',
		                'name' => 'tim',
		                'label' => 'Tim Auditor',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '180px',
		                'className' => 'text-left'
		            ],
		            [
		                'data' => 'status',
		                'name' => 'status',
		                'label' => 'Status',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'action',
		                'name' => 'action',
		                'label' => 'Aksi',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '80px',
		                'className' => 'text-center'
		            ],
        	],
        	'listStruct2' => [
		        	[
		                'data' => 'num',
		                'name' => 'num',
		                'label' => '#',
		                'orderable' => false,
		                'searchable' => false,
		                'className' => 'text-center',
		                'width' => '20px',
		            ],
		            [
		                'data' => 'tahun',
		                'name' => 'tahun',
		                'label' => 'Tahun',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'rencana',
		                'name' => 'rencana',
		                'label' => 'Waktu Pelaksanaan',
		                'className' => 'text-center',
		                'width' => '300px',
		            ],
		            [
		                'data' => 'tipe',
		                'name' => 'tipe',
		                'label' => 'Tipe',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'kategori',
		                'name' => 'kategori',
		                'label' => 'Kategori',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'object_audit',
		                'name' => 'object_audit',
		                'label' => 'Objek Audit',
		                'className' => 'text-left',
		                'width' => '250px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'no_ab',
		                'name' => 'no_ab',
		                'label' => 'No AB',
		                'className' => 'text-center',
		                'width' => '150px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'detil',
		                'name' => 'detil',
		                'label' => 'Rencana Kerja',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'penugasan',
		                'name' => 'penugasan',
		                'label' => 'Surat Penugasan',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'tinjauan',
		                'name' => 'tinjauan',
		                'label' => 'Tinjauan Dokumen',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'detil_program',
		                'name' => 'detil_program',
		                'label' => 'Program Audit',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'detil_draft',
		                'name' => 'detil_draft',
		                'label' => 'KKA',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'tim',
		                'name' => 'tim',
		                'label' => 'Tim Auditor',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '180px',
		                'className' => 'text-left'
		            ],
		            [
		                'data' => 'audite',
		                'name' => 'audite',
		                'label' => 'Auditee',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '180px',
		                'className' => 'text-left'
		            ],
		            [
		                'data' => 'status',
		                'name' => 'status',
		                'label' => 'Status',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'action',
		                'name' => 'action',
		                'label' => 'Aksi',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '120px',
		                'className' => 'text-center'
		            ],
        	],
        	'listStruct3' => [
		        	[
		                'data' => 'num',
		                'name' => 'num',
		                'label' => '#',
		                'orderable' => false,
		                'searchable' => false,
		                'className' => 'text-center',
		                'width' => '20px',
		            ],
		            [
		                'data' => 'tahun',
		                'name' => 'tahun',
		                'label' => 'Tahun',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'rencana',
		                'name' => 'rencana',
		                'label' => 'Waktu Pelaksanaan',
		                'className' => 'text-center',
		                'width' => '300px',
		            ],
		            [
		                'data' => 'tipe',
		                'name' => 'tipe',
		                'label' => 'Tipe',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'kategori',
		                'name' => 'kategori',
		                'label' => 'Kategori',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'object_audit',
		                'name' => 'object_audit',
		                'label' => 'Objek Audit',
		                'className' => 'text-left',
		                'width' => '250px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'no_ab',
		                'name' => 'no_ab',
		                'label' => 'No AB',
		                'className' => 'text-center',
		                'width' => '150px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'detil',
		                'name' => 'detil',
		                'label' => 'Rencana Kerja',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'penugasan',
		                'name' => 'penugasan',
		                'label' => 'Surat Penugasan',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'tinjauan',
		                'name' => 'tinjauan',
		                'label' => 'Tinjauan Dokumen',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'detil_program',
		                'name' => 'detil_program',
		                'label' => 'Program Audit',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'detil_draft',
		                'name' => 'detil_draft',
		                'label' => 'KKA',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'status',
		                'name' => 'status',
		                'label' => 'Status',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'action',
		                'name' => 'action',
		                'label' => 'Aksi',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '80px',
		                'className' => 'text-center'
		            ],
        	],
        ];
    }

    public function grid()
    {
    	$user = auth()->user();
    	if($user->hasJabatan(['SVP - Internal Audit','President Director','Secretary'])){
	        $records = ProgramAudit::whereHas('penugasanaudit', function($u){
	        							$u->whereHas('tinjauandokumen', function($a){
	        								$a->where('status', 5);
	        							});
	        						})->where('status_kka', 0)->whereIn('status', [4,5])->select('*');
        	if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at','desc');
	        }

	        if($tahun = request()->tahun) {
	        	$records->whereHas('opening', function($a) use ($tahun){
		        	$a->whereHas('program', function($b) use ($tahun){
			        	$b->whereHas('penugasanaudit', function($d) use ($tahun){
				        	$d->whereHas('rencanadetail', function($u) use ($tahun){
				        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
				        			$z->where('tahun', 'like', '%' . $tahun . '%');
				        		});
				        	});
			        	});
		        	});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('opening', function($a) use ($tipe){
		        	$a->whereHas('program', function($b) use ($tipe){
			        	$b->whereHas('penugasanaudit', function($d) use ($tipe){
				        	$d->whereHas('rencanadetail', function($u) use ($tipe){
				        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
				        			if($tipe == 1){
						        		$z->where('tipe', 0);
					        		}else{
					        			$z->where('tipe', 1);
					        		}
				        		});
				        	});
			        	});
		        	});
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('opening', function($a){
		        		$a->whereHas('program', function($b){
			        		$b->whereHas('penugasanaudit', function($d){
				        		$d->whereHas('rencanadetail', function($u){
					        		$u->where('tipe_object', 0);
					        	});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 2){
	        		$records->whereHas('opening', function($a){
		        		$a->whereHas('program', function($b){
			        		$b->whereHas('penugasanaudit', function($d){
				        		$d->whereHas('rencanadetail', function($u){
					        		$u->where('tipe_object', 1);
					        	});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 3){
	        		$records->whereHas('opening', function($a){
		        		$a->whereHas('program', function($b){
			        		$b->whereHas('penugasanaudit', function($d){
				        		$d->whereHas('rencanadetail', function($u){
					        		$u->where('tipe_object', 2);
					        	});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 4){
	        		$records->whereHas('opening', function($a){
		        		$a->whereHas('program', function($b){
			        		$b->whereHas('penugasanaudit', function($d){
				        		$d->whereHas('rencanadetail', function($u){
					        		$u->where('tipe_object', 3);
					        	});
			        		});
		        		});
	        		});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->whereHas('opening', function($a) use ($kat, $object_id){
		        	$a->whereHas('program', function($b) use ($kat, $object_id){
			        	$b->whereHas('penugasanaudit', function($d) use ($kat, $object_id){
			        		$d->whereHas('rencanadetail', function($u) use ($kat, $object_id){
			        			$u->where('tipe_object', $kat)->where('object_id', $object_id);
			        		});
		        		});
		        	});
	        	});
	        }
        }elseif($user->hasJabatan(['auditor'])){
	        $records = ProgramAudit::whereHas('penugasanaudit', function($u){
	        							$u->whereHas('tinjauandokumen', function($a){
	        								$a->where('status', 5);
	        							});
	        						})->whereHas('detailanggota', function($u) use ($user){
    									$u->where('user_id', $user->id);
    								})
	        						->where('status_kka', 0)
	        						->whereIn('status', [4,5])
	    							->select('*');
        	if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at','desc');
	        }

	        if($tahun = request()->tahun) {
	        	$records->whereHas('opening', function($a) use ($tahun){
		        	$a->whereHas('program', function($b) use ($tahun){
			        	$b->whereHas('penugasanaudit', function($d) use ($tahun){
				        	$d->whereHas('rencanadetail', function($u) use ($tahun){
				        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
				        			$z->where('tahun', 'like', '%' . $tahun . '%');
				        		});
				        	});
			        	});
		        	});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('opening', function($a) use ($tipe){
		        	$a->whereHas('program', function($b) use ($tipe){
			        	$b->whereHas('penugasanaudit', function($d) use ($tipe){
				        	$d->whereHas('rencanadetail', function($u) use ($tipe){
				        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
				        			if($tipe == 1){
						        		$z->where('tipe', 0);
					        		}else{
					        			$z->where('tipe', 1);
					        		}
				        		});
				        	});
			        	});
		        	});
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('opening', function($a){
		        		$a->whereHas('program', function($b){
			        		$b->whereHas('penugasanaudit', function($d){
				        		$d->whereHas('rencanadetail', function($u){
					        		$u->where('tipe_object', 0);
					        	});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 2){
	        		$records->whereHas('opening', function($a){
		        		$a->whereHas('program', function($b){
			        		$b->whereHas('penugasanaudit', function($d){
				        		$d->whereHas('rencanadetail', function($u){
					        		$u->where('tipe_object', 1);
					        	});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 3){
	        		$records->whereHas('opening', function($a){
		        		$a->whereHas('program', function($b){
			        		$b->whereHas('penugasanaudit', function($d){
				        		$d->whereHas('rencanadetail', function($u){
					        		$u->where('tipe_object', 2);
					        	});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 4){
	        		$records->whereHas('opening', function($a){
		        		$a->whereHas('program', function($b){
			        		$b->whereHas('penugasanaudit', function($d){
				        		$d->whereHas('rencanadetail', function($u){
					        		$u->where('tipe_object', 3);
					        	});
			        		});
		        		});
	        		});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->whereHas('opening', function($a) use ($kat, $object_id){
		        	$a->whereHas('program', function($b) use ($kat, $object_id){
			        	$b->whereHas('penugasanaudit', function($d) use ($kat, $object_id){
			        		$d->whereHas('rencanadetail', function($u) use ($kat, $object_id){
			        			$u->where('tipe_object', $kat)->where('object_id', $object_id);
			        		});
		        		});
		        	});
	        	});
	        }
        }else{
        	$records = ProgramAudit::where('id', 0);
        }
        return DataTables::of($records->get())
           ->addColumn('num', function ($record) {
               return request()->start;
           })
           ->addColumn('tahun', function ($record) {
               return $record->penugasanaudit->rencanadetail->rencanaaudit->tahun;
           })
           ->addColumn('tipe', function ($record) {
                $tipe = '';
                if($record->penugasanaudit->rencanadetail->tipe == 0){
               	  $tipe = '<span class="label label-primary">Audit</span>';
                }elseif($record->penugasanaudit->rencanadetail->tipe == 1){
               	  $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
                }else{
                  $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
                }

                return $tipe;
           })
           ->addColumn('kategori', function ($record) {
                $kategori = '';
                if($record->penugasanaudit->rencanadetail->tipe == 0){
	                if($record->penugasanaudit->rencanadetail->tipe_object == 0){
	               	  $kategori = '<span class="label label-success">Business Unit (BU)</span>';
	                }elseif($record->penugasanaudit->rencanadetail->tipe_object == 1){
	               	  $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
	                }elseif($record->penugasanaudit->rencanadetail->tipe_object == 2){
	               	  $kategori = '<span class="label label-warning">Project</span>';
	                }else{
	               	  $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
	                }
                }elseif($record->penugasanaudit->rencanadetail->tipe == 1){
               	  	$kategori = $record->penugasanaudit->rencanadetail->konsultasi->nama;
                }else{
                    $kategori = $record->penugasanaudit->rencanadetail->lain->nama;
                }
                return $kategori;
           })
           ->addColumn('object_audit', function ($record) {
               return object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
           })
           ->addColumn('no_ab', function ($record) {
               return getAb($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
           })
           ->addColumn('rencana', function ($record) {
                return DateToString($record->detailpelaksanaan->first()->tgl).' - '.DateToString($record->detailpelaksanaan->last()->tgl);
           })
           ->addColumn('nama', function ($record) {
           		if($record->penugasanaudit->rencanadetail->tipe_object == 2){
               		return object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
               	}else{
               		return '';
               	}
           })
           ->addColumn('detil', function ($record) use ($user) {
               $buttons = '';
               if($user->hasJabatan(['SVP - Internal Audit','President Director'])){
	               $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
	                    'tooltip' 	=> 'Rencana Kerja',
	                    'class' 	=> 'download-rkia button',
	                    'id'   		=> $record->penugasanaudit->rencanadetail->rencanaaudit->id,
	               ]);
               }else{
               		$buttons .='';
               }
               return $buttons;
           })
           ->addColumn('penugasan', function ($record) {
               $buttons = '';
               if($record->penugasanaudit->status == 4){
	               $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
	                    'tooltip' 	=> 'Surat Penugasan',
	                    'class' 	=> 'download-penugasan button',
	                    'id'   		=> $record->penugasanaudit->id,
	               ]);
               }else{
               	    $buttons .='';
               }
               return $buttons;
           })
           ->addColumn('tinjauan', function ($record) {
               $buttons = '';
               if($record->penugasanaudit->tinjauandokumen){
	               $buttons .= $this->makeButton([
	           			'type' 		=> 'edit',
	           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
	           			'tooltip' 	=> 'Detil Tinjauan',
	           			'class' 	=> 'detil-tinjauan button',
	           			'id'   		=> $record->penugasanaudit->tinjauandokumen->id,
	           		]);
               }

               return $buttons;
           })
           ->addColumn('detil_program', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
                    'type' 		=> 'edit',
                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
                    'tooltip' 	=> 'Program Audit',
                    'class' 	=> 'download-program button',
                    'id'   		=> $record->id,
               ]);
               return $buttons;
           })
           ->editColumn('status', function ($record) {
           		$cek ='';
           		if($record->kka){
		           	if($record->kka->status_operasional < 2){
	       				$cek .= '<span class="label label-warning">Fokus Audit Operasional <i class="fa fa-close" aria-hidden="true"></i></span><br>';
	       			}else{
	           			$cek .= '<span class="label label-success">Fokus Audit Operasional <i class="fa fa-check" aria-hidden="true"></i></span><br>';
	       			}

	       			if($record->kka->status_keuangan < 2){
	       				$cek .= '<span class="label label-warning">Fokus Audit Keuangan <i class="fa fa-close" aria-hidden="true"></i></span><br>';
	       			}else{
	           			$cek .= '<span class="label label-success">Fokus Audit Keuangan <i class="fa fa-check" aria-hidden="true"></i></span><br>';
	       			}

	       			if($record->kka->status_sistem < 2){
	       				$cek .= '<span class="label label-warning">Fokus Audit Sistem <i class="fa fa-close" aria-hidden="true"></i></span><br>';
	       			}else{
	           			$cek .= '<span class="label label-success">Fokus Audit Sistem <i class="fa fa-check" aria-hidden="true"></i></span><br>';
	       			}
	       			return $cek;
	           }else{
	           	   return '<span class="label label-default">Baru</span>';
	           }
           })
           ->addColumn('tim', function ($record) {
	           	$data = array();
                $tim ='';
           		$tim .='<ul class="list list-more1 list-unstyled" style="margin-top:10px;" data-display="3">';
           		$tim .='<li class="item">'.$record->penugasanaudit->anggota->where('fungsi', 0)->where('data_id', $record->penugasanaudit->rencanadetail->id)->first()->user->name.'</li>';
           		foreach ($record->penugasanaudit->anggota->whereIn('fungsi', [1,2,3]) as $key => $value) {
           				$data[] = $value->user->name;
           		}
           		foreach (array_unique($data) as $key => $value) {
           			$tim .='<li class="item">'.$value.'</li>';
           		}
           		$tim .='</ul>';
               	return $tim;
           })
           ->addColumn('action', function ($record) use ($user) {
           	   $buttons = '';
               if($user->hasJabatan('auditor')){
               		$cek = count($record->detailanggota->where('user_id', $user->id));
               		if($cek > 0){
		               	if($user->fungsi > 0){
		               		if($user->fungsi == 1){
			               	   if($record->kka){
				               	   	if($record->kka->status_operasional == 0){
				               	   		$buttons = $this->makeButton([
						                    'type' 		=> 'edit',
						                    'label' 	=> '<i class="fa fa-plus text-primary"></i>',
						                    'tooltip' 	=> 'Buat KKA',
						                    'class' 	=> 'buat button',
						                    'id'   		=> $record->id,
						                ]);
				               	   	}else{
				               	   		if($record->kka->status_operasional == 1){
					               	   		$buttons .= $this->makeButton([
							                    'type' 		=> 'edit',
							                    'label' 	=> '<i class="fa fa-edit text-primary"></i>',
							                    'tooltip' 	=> 'Ubah KKA',
							                    'class' 	=> 'ubah button',
							                    'id'   		=> $record->kka->id,
							                ]);
				               	   		}else{
				               	   			$buttons .= $this->makeButton([
							           			'type' 		=> 'edit',
							           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
							           			'tooltip' 	=> 'Detil Draft KKA',
							           			'class' 	=> 'detil-draft button',
							           			'id'   		=> $record->kka->id,
							           		]);
				               	   		}
				               	   	}
			               	   }else{
					               $buttons = $this->makeButton([
					                    'type' 		=> 'edit',
					                    'label' 	=> '<i class="fa fa-plus text-primary"></i>',
					                    'tooltip' 	=> 'Buat KKA',
					                    'class' 	=> 'buat button',
					                    'id'   		=> $record->id,
					               ]);
			               	   }
		               		}elseif($user->fungsi == 2){
		               			if($record->kka){
			               	   		if($record->kka->status_keuangan == 0){
				               	   		$buttons = $this->makeButton([
						                    'type' 		=> 'edit',
						                    'label' 	=> '<i class="fa fa-plus text-primary"></i>',
						                    'tooltip' 	=> 'Buat KKA',
						                    'class' 	=> 'buat button',
						                    'id'   		=> $record->id,
						               ]);
				               	   	}else{
				               	   		if($record->kka->status_keuangan == 1){
					               	   		$buttons .= $this->makeButton([
							                    'type' 		=> 'edit',
							                    'label' 	=> '<i class="fa fa-edit text-primary"></i>',
							                    'tooltip' 	=> 'Ubah KKA',
							                    'class' 	=> 'ubah button',
							                    'id'   		=> $record->kka->id,
							                ]);
				               	   		}else{
				               	   			$buttons .= $this->makeButton([
							           			'type' 		=> 'edit',
							           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
							           			'tooltip' 	=> 'Detil Draft KKA',
							           			'class' 	=> 'detil-draft button',
							           			'id'   		=> $record->kka->id,
							           		]);
				               	   		}
				               	   	}
			               	   }else{
					               $buttons = $this->makeButton([
					                    'type' 		=> 'edit',
					                    'label' 	=> '<i class="fa fa-plus text-primary"></i>',
					                    'tooltip' 	=> 'Buat KKA',
					                    'class' 	=> 'buat button',
					                    'id'   		=> $record->id,
					               ]);
			               	   }
		               		}else{
		               		   if($record->kka){
			               	   		if($record->kka->status_sistem == 0){
				               	   		$buttons = $this->makeButton([
						                    'type' 		=> 'edit',
						                    'label' 	=> '<i class="fa fa-plus text-primary"></i>',
						                    'tooltip' 	=> 'Buat KKA',
						                    'class' 	=> 'buat button',
						                    'id'   		=> $record->id,
						               ]);
				               	   	}else{
				               	   		if($record->kka->status_sistem == 1){
					               	   		$buttons .= $this->makeButton([
							                    'type' 		=> 'edit',
							                    'label' 	=> '<i class="fa fa-edit text-primary"></i>',
							                    'tooltip' 	=> 'Ubah KKA',
							                    'class' 	=> 'ubah button',
							                    'id'   		=> $record->kka->id,
							                ]);
				               	   		}else{
				               	   			$buttons .= $this->makeButton([
							           			'type' 		=> 'edit',
							           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
							           			'tooltip' 	=> 'Detil Draft KKA',
							           			'class' 	=> 'detil-draft button',
							           			'id'   		=> $record->kka->id,
							           		]);
				               	   		}
				               	   	}
			               	   }else{
					               $buttons = $this->makeButton([
					                    'type' 		=> 'edit',
					                    'label' 	=> '<i class="fa fa-plus text-primary"></i>',
					                    'tooltip' 	=> 'Buat KKA',
					                    'class' 	=> 'buat button',
					                    'id'   		=> $record->id,
					               ]);
			               	   }
		               		}
		               	}else{
		               		$buttons .='';
		               	}
	               }else{
	               		$buttons .='';
	               }
               }else{
               		$buttons .='';
               }
               return $buttons;
           })
           ->rawColumns(['tipe', 'action','detil','penugasan','tinjauan','detil_program','detil_project','kategori','object_audit','status','tim'])
           ->make(true);
    }

    public function onProgress()
    {
    	$user = auth()->user();
        if($user->hasJabatan(['SVP - Internal Audit','President Director','Secretary'])){
	        $records = DraftKka::whereIn('status', [1,2,3,4])->select('*');
        	if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at','desc');
	        }
	        if($tahun = request()->tahun) {
	        	$records->whereHas('closing', function($a) use ($tahun){
		        	$a->whereHas('opening', function($b) use ($tahun){
			        	$b->whereHas('program', function($c) use ($tahun){
				        	$c->whereHas('penugasanaudit', function($e) use ($tahun){
					        	$e->whereHas('rencanadetail', function($u) use ($tahun){
					        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
					        			$z->where('tahun', 'like', '%' . $tahun . '%');
					        		});
					        	});
				        	});
			        	});
		        	});
	        	});
	        }
	        if($tipe = request()->tipe) {
	        	$records->whereHas('closing', function($a) use ($tipe){
		        	$a->whereHas('opening', function($b) use ($tipe){
			        	$b->whereHas('program', function($c) use ($tipe){
				        	$c->whereHas('penugasanaudit', function($e) use ($tipe){
					        	$e->whereHas('rencanadetail', function($u) use ($tipe){
					        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
					        			if($tipe == 1){
							        		$z->where('tipe', 0);
						        		}else{
						        			$z->where('tipe', 1);
						        		}
					        		});
					        	});
				        	});
			        	});
		        	});
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 0);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 2){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 1);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 3){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 2);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 4){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 3);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->whereHas('closing', function($a) use ($kat, $object_id){
		        	$a->whereHas('opening', function($b) use ($kat, $object_id){
			        	$b->whereHas('program', function($c) use ($kat, $object_id){
				        	$c->whereHas('penugasanaudit', function($e) use ($kat, $object_id){
				        		$e->whereHas('rencanadetail', function($u) use ($kat, $object_id){
				        			$u->where('tipe_object', $kat)->where('object_id', $object_id);
				        		});
			        		});
			        	});
		        	});
	        	});
	        }
        }elseif($user->hasJabatan(['auditor'])){
	        $records = DraftKka::whereHas('program', function($a) use ($user){
	        						$a->whereHas('detailanggota', function($u) use ($user){
    									$u->where('user_id', $user->id);
    								});
	        					})
	        					->whereIn('status', [1,2,3,4])->select('*');
        	if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at','desc');
	        }
	        if($tahun = request()->tahun) {
	        	$records->whereHas('closing', function($a) use ($tahun){
		        	$a->whereHas('opening', function($b) use ($tahun){
			        	$b->whereHas('program', function($c) use ($tahun){
				        	$c->whereHas('penugasanaudit', function($e) use ($tahun){
					        	$e->whereHas('rencanadetail', function($u) use ($tahun){
					        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
					        			$z->where('tahun', 'like', '%' . $tahun . '%');
					        		});
					        	});
				        	});
			        	});
		        	});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('closing', function($a) use ($tipe){
		        	$a->whereHas('opening', function($b) use ($tipe){
			        	$b->whereHas('program', function($c) use ($tipe){
				        	$c->whereHas('penugasanaudit', function($e) use ($tipe){
					        	$e->whereHas('rencanadetail', function($u) use ($tipe){
					        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
					        			if($tipe == 1){
							        		$z->where('tipe', 0);
						        		}else{
						        			$z->where('tipe', 1);
						        		}
					        		});
					        	});
				        	});
			        	});
		        	});
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 0);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 2){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 1);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 3){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 2);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 4){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 3);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->whereHas('closing', function($a) use ($kat, $object_id){
		        	$a->whereHas('opening', function($b) use ($kat, $object_id){
			        	$b->whereHas('program', function($c) use ($kat, $object_id){
				        	$c->whereHas('penugasanaudit', function($e) use ($kat, $object_id){
				        		$e->whereHas('rencanadetail', function($u) use ($kat, $object_id){
				        			$u->where('tipe_object', $kat)->where('object_id', $object_id);
				        		});
			        		});
			        	});
		        	});
	        	});
	        }
        }elseif($user->hasJabatan(['SVP','Project'])){
        	$cari = checkuser($user->id);
    		$tipe_object = $cari[0];
    		$object_id = $cari[1];
	        $records = DraftKka::whereHas('program', function($a) use ($tipe_object, $object_id, $user){
    								$a->whereHas('penugasanaudit', function($x) use ($tipe_object, $object_id, $user){
										$x->whereHas('rencanadetail', function($y) use ($tipe_object, $object_id, $user){
											$y->where('tipe_object', $tipe_object)
											  ->whereIn('object_id', $object_id);
										})->orWhere('user_id', $user->id);
									});
	        					})
	        					->whereIn('status', [1,2,3,4])->select('*');
        	if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at','desc');
	        }
	        if($tahun = request()->tahun) {
	        	$records->whereHas('closing', function($a) use ($tahun){
		        	$a->whereHas('opening', function($b) use ($tahun){
			        	$b->whereHas('program', function($c) use ($tahun){
				        	$c->whereHas('penugasanaudit', function($e) use ($tahun){
					        	$e->whereHas('rencanadetail', function($u) use ($tahun){
					        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
					        			$z->where('tahun', 'like', '%' . $tahun . '%');
					        		});
					        	});
				        	});
			        	});
		        	});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('closing', function($a) use ($tipe){
		        	$a->whereHas('opening', function($b) use ($tipe){
			        	$b->whereHas('program', function($c) use ($tipe){
				        	$c->whereHas('penugasanaudit', function($e) use ($tipe){
					        	$e->whereHas('rencanadetail', function($u) use ($tipe){
					        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
					        			if($tipe == 1){
							        		$z->where('tipe', 0);
						        		}else{
						        			$z->where('tipe', 1);
						        		}
					        		});
					        	});
				        	});
			        	});
		        	});
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 0);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 2){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 1);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 3){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 2);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 4){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 3);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->whereHas('closing', function($a) use ($kat, $object_id){
		        	$a->whereHas('opening', function($b) use ($kat, $object_id){
			        	$b->whereHas('program', function($c) use ($kat, $object_id){
				        	$c->whereHas('penugasanaudit', function($e) use ($kat, $object_id){
				        		$e->whereHas('rencanadetail', function($u) use ($kat, $object_id){
				        			$u->where('tipe_object', $kat)->where('object_id', $object_id);
				        		});
			        		});
			        	});
		        	});
	        	});
	        }
        }else{
        	$records = DraftKka::where('id', 0);
        }
        return DataTables::of($records->get())
           ->addColumn('num', function ($record) {
               return request()->start;
           })
           ->addColumn('tahun', function ($record) {
               return $record->program->penugasanaudit->rencanadetail->rencanaaudit->tahun;
           })
           ->addColumn('tipe', function ($record) {
                $tipe = '';
                if($record->program->penugasanaudit->rencanadetail->tipe == 0){
               	  $tipe = '<span class="label label-primary">Audit</span>';
                }elseif($record->program->penugasanaudit->rencanadetail->tipe == 1){
               	  $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
                }else{
                  $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
                }

                return $tipe;
           })
           ->addColumn('kategori', function ($record) {
                $kategori = '';
                if($record->program->penugasanaudit->rencanadetail->tipe == 0){
	                if($record->program->penugasanaudit->rencanadetail->tipe_object == 0){
	               	  $kategori = '<span class="label label-success">Business Unit (BU)</span>';
	                }elseif($record->program->penugasanaudit->rencanadetail->tipe_object == 1){
	               	  $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
	                }elseif($record->program->penugasanaudit->rencanadetail->tipe_object == 2){
	               	  $kategori = '<span class="label label-warning">Project</span>';
	                }else{
	               	  $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
	                }
                }elseif($record->program->penugasanaudit->rencanadetail->tipe == 1){
               	  	$kategori = $record->program->penugasanaudit->rencanadetail->konsultasi->nama;
                }else{
                    $kategori = $record->program->penugasanaudit->rencanadetail->lain->nama;
                }
                return $kategori;
           })
           ->addColumn('object_audit', function ($record) {
               return object_audit($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
           })
           ->addColumn('no_ab', function ($record) {
               return getAb($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
           })
           ->addColumn('rencana', function ($record) {
                return DateToString($record->program->detailpelaksanaan->first()->tgl).' - '.DateToString($record->program->detailpelaksanaan->last()->tgl);
           })
           ->addColumn('tim', function ($record) use ($user) {
	           	$data = array();
                $tim ='';
           		$tim .='<ul class="list list-more2 list-unstyled" style="margin-top:10px;" data-display="3">';
           		$tim .='<li class="item">'.$record->program->user->name.'</li>';
           		foreach ($record->program->detailanggota->whereIn('fungsi', [1,2,3]) as $key => $value) {
           				$data[] = $value->user->name;
           		}
           		foreach (array_unique($data) as $key => $value) {
           			$tim .='<li class="item">'.$value.'</li>';
           		}
           		$tim .='</ul>';
               	return $tim;
           })
           ->addColumn('audite', function ($record){
           		$cek = object_user($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id)[0];
       			$pic ='';
           		$pic .='<ul class="list list-more4 list-unstyled" style="margin-top:10px;" data-display="3">';
           		foreach ($record->program->penugasanaudit->tinjauandokumen->detailpenerima as $key => $value) {
	           			if(in_array($value->user->id, $cek)){
	           				$pic .='<li class="item" style="padding:0px;border:none;">'.$value->user->name.'</li>';
	           			}
           		}
           		$pic .='</ul>';
               	return $pic;
           })
           ->addColumn('nama', function ($record) {
           		if($record->program->penugasanaudit->rencanadetail->tipe_object == 2){
               		return object_audit($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
               	}else{
               		return '';
               	}
           })
           ->addColumn('detil', function ($record) use ($user) {
               $buttons = '';
               if($user->hasJabatan(['SVP - Internal Audit','President Director'])){
	               $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
	                    'tooltip' 	=> 'Rencana Kerja',
	                    'class' 	=> 'download-rkia button',
	                    'id'   		=> $record->program->penugasanaudit->rencanadetail->rencanaaudit->id,
	               ]);
               }else{
               		$buttons .='';
               }
               return $buttons;
           })
           ->addColumn('penugasan', function ($record) {
               $buttons = '';
               if($record->program->penugasanaudit->status == 4){
	               $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
	                    'tooltip' 	=> 'Surat Penugasan',
	                    'class' 	=> 'download-penugasan button',
	                    'id'   		=> $record->program->penugasanaudit->id,
	               ]);
               }else{
               	    $buttons .='';
               }
               return $buttons;
           })
           ->addColumn('tinjauan', function ($record) {
               $buttons = '';
               if($record->program->penugasanaudit->tinjauandokumen){
	               $buttons .= $this->makeButton([
	           			'type' 		=> 'edit',
	           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
	           			'tooltip' 	=> 'Detil Tinjauan',
	           			'class' 	=> 'detil-tinjauan button',
	           			'id'   		=> $record->program->penugasanaudit->tinjauandokumen->id,
	           		]);
               }

               return $buttons;
           })
           ->addColumn('detil_program', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
                    'type' 		=> 'edit',
                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
                    'tooltip' 	=> 'Program Audit',
                    'class' 	=> 'download-program button',
                    'id'   		=> $record->program->id,
               ]);
               return $buttons;
           })
           ->addColumn('detil_draft', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
           			'type' 		=> 'edit',
           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
           			'tooltip' 	=> 'Detil Draft KKA',
           			'class' 	=> 'detil-draft button',
           			'id'   		=> $record->id,
           		]);

               return $buttons;
           })
           ->editColumn('status', function ($record) {
           		$cek = '';
	           	if($record->status == 1){
	           		return '<span class="label label-warning">Waiting Approval Ketua Tim</span>';
	           	}elseif($record->status == 2){
	           		return '<span class="label label-warning">Waiting Approval Auditee</span>';
	           	}elseif($record->status == 3){
	           		if($record->status_operasional < 1){
	       				$cek .= '<span class="label label-warning">Update Fokus Audit Operasional <i class="fa fa-close" aria-hidden="true"></i></span><br>';
	       			}else{
	           			$cek .= '<span class="label label-success">Update Fokus Audit Operasional <i class="fa fa-check" aria-hidden="true"></i></span><br>';
	       			}

	       			if($record->status_keuangan < 1){
	       				$cek .= '<span class="label label-warning">Update Fokus Audit Keuangan <i class="fa fa-close" aria-hidden="true"></i></span><br>';
	       			}else{
	           			$cek .= '<span class="label label-success">Update Fokus Audit Keuangan <i class="fa fa-check" aria-hidden="true"></i></span><br>';
	       			}

	       			if($record->status_sistem < 1){
	       				$cek .= '<span class="label label-warning">Update Fokus Audit Sistem <i class="fa fa-close" aria-hidden="true"></i></span><br>';
	       			}else{
	           			$cek .= '<span class="label label-success">Update Fokus Audit Sistem <i class="fa fa-check" aria-hidden="true"></i></span><br>';
	       			}
	       			return $cek;
	           	}else{
	               return '<span class="label label-warning">Waiting Upload</span>';
	           	}
           })
           ->addColumn('action', function ($record) use ($user) {
               $buttons = '';
               if($record->status == 1){
               	   if($record->program->user->id == $user->id){
		               $buttons .= $this->makeButton([
		                    'type' => 'edit',
		                    'class' => 'm-l approval button',
		                    'label' => '<i class="fa fa-check text-info"></i>',
		                    'tooltip' => 'Buat Approval',
		                    'id'   => $record->id,
		               ]);
			       }else{
			       		$buttons = '';
			       }
               }elseif($record->status == 2){
               		$count = count($record->program->penugasanaudit->tinjauandokumen->detailpenerima->where('user_id', $user->id));
	               if($count > 0){
		                $buttons .= $this->makeButton([
		                    'type' => 'edit',
		                    'class' => 'm-l ubah-persetujuan button',
		                    'label' => '<i class="fa fa-edit text-info"></i>',
		                    'tooltip' => 'Buat Approval',
		                    'id'   => $record->id,
		                ]);
			       }else{
			       		$buttons .= '';
			       }
               }elseif($record->status == 3){
               		$cek = count($record->program->detailanggota->where('user_id', $user->id));
               		if($cek > 0){
               			if($user->fungsi > 0){
		               		if($user->fungsi == 1){
			               	   	if($record->status_operasional == 0){
			               	   		$buttons = $this->makeButton([
					                    'type' 		=> 'edit',
					                    'label' 	=> '<i class="fa fa-edit text-primary"></i>',
					                    'tooltip' 	=> 'Ubah KKA',
					                    'class' 	=> 'ubah-kka button',
					                    'id'   		=> $record->id,
					                ]);
			               	   	}else{
		               	   			$buttons .= $this->makeButton([
					           			'type' 		=> 'edit',
					           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
					           			'tooltip' 	=> 'Detil Draft KKA',
					           			'class' 	=> 'detil-draft button',
					           			'id'   		=> $record->id,
					           		]);
			               	   	}
		               		}elseif($user->fungsi == 2){
		               	   		if($record->status_keuangan == 0){
			               	   		$buttons = $this->makeButton([
					                    'type' 		=> 'edit',
					                    'label' 	=> '<i class="fa fa-edit text-primary"></i>',
					                    'tooltip' 	=> 'Ubah KKA',
					                    'class' 	=> 'ubah-kka button',
					                    'id'   		=> $record->id,
					               ]);
			               	   	}else{
		               	   			$buttons .= $this->makeButton([
					           			'type' 		=> 'edit',
					           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
					           			'tooltip' 	=> 'Detil Draft KKA',
					           			'class' 	=> 'detil-draft button',
					           			'id'   		=> $record->id,
					           		]);
			               	   	}
		               		}else{
		               	   		if($record->status_sistem == 0){
			               	   		$buttons = $this->makeButton([
					                    'type' 		=> 'edit',
					                    'label' 	=> '<i class="fa fa-edit text-primary"></i>',
					                    'tooltip' 	=> 'Ubah KKA',
					                    'class' 	=> 'ubah-kka button',
					                    'id'   		=> $record->id,
					               ]);
			               	   	}else{
		               	   			$buttons .= $this->makeButton([
					           			'type' 		=> 'edit',
					           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
					           			'tooltip' 	=> 'Detil Draft KKA',
					           			'class' 	=> 'detil-draft button',
					           			'id'   		=> $record->id,
					           		]);
			               	   	}
		               		}
		               	}else{
		               		$buttons .='';
		               	}
	                }else{
	               		$buttons .='';
	                }
               }elseif($record->status == 4){
               	 	if($user->hasJabatan(['Secretary'])){
	                	$buttons .= $this->makeButton([
	               			'type' 		=> 'edit',
	               			'label' 	=> '<i class="fa fa-upload text-primary"></i>',
	               			'tooltip' 	=> 'Upload Dokumen',
	               			'class' 	=> 'upload-dokumen button',
	               			'id'   		=> $record->id,
	               		]);
	                }else{
	                	$buttons = '';
	                }
               }else{
               	 $buttons .= '';
               }

               return $buttons;
           })
           ->rawColumns(['tipe', 'action','detil','detil_draft','penugasan','tinjauan','detil_program','detil_project','kategori','object_audit','status','tim','audite'])
           ->make(true);
    }

    public function historis()
    {
        $user = auth()->user();
        if($user->hasJabatan(['SVP - Internal Audit','President Director','Secretary'])){
	        $records = DraftKka::whereIn('status', [5,6])->select('*');
        	if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at','desc');
	        }
	        if($tahun = request()->tahun) {
	        	$records->whereHas('closing', function($a) use ($tahun){
		        	$a->whereHas('opening', function($b) use ($tahun){
			        	$b->whereHas('program', function($c) use ($tahun){
				        	$c->whereHas('penugasanaudit', function($e) use ($tahun){
					        	$e->whereHas('rencanadetail', function($u) use ($tahun){
					        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
					        			$z->where('tahun', 'like', '%' . $tahun . '%');
					        		});
					        	});
				        	});
			        	});
		        	});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('closing', function($a) use ($tipe){
		        	$a->whereHas('opening', function($b) use ($tipe){
			        	$b->whereHas('program', function($c) use ($tipe){
				        	$c->whereHas('penugasanaudit', function($e) use ($tipe){
					        	$e->whereHas('rencanadetail', function($u) use ($tipe){
					        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
					        			if($tipe == 1){
							        		$z->where('tipe', 0);
						        		}else{
						        			$z->where('tipe', 1);
						        		}
					        		});
					        	});
				        	});
			        	});
		        	});
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 0);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 2){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 1);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 3){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 2);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 4){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 3);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->whereHas('closing', function($a) use ($kat, $object_id){
		        	$a->whereHas('opening', function($b) use ($kat, $object_id){
			        	$b->whereHas('program', function($c) use ($kat, $object_id){
				        	$c->whereHas('penugasanaudit', function($e) use ($kat, $object_id){
				        		$e->whereHas('rencanadetail', function($u) use ($kat, $object_id){
				        			$u->where('tipe_object', $kat)->where('object_id', $object_id);
				        		});
			        		});
			        	});
		        	});
	        	});
	        }
        }elseif($user->hasJabatan(['auditor'])){
	        $records = DraftKka::whereHas('program', function($a) use ($user){
	        						$a->whereHas('detailanggota', function($u) use ($user){
    									$u->where('user_id', $user->id);
    								})->orWhere('user_id', $user->id);
	        					})
	        					->whereIn('status', [5,6])->select('*');
        	if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at','desc');
	        }
	        if($tahun = request()->tahun) {
	        	$records->whereHas('closing', function($a) use ($tahun){
		        	$a->whereHas('opening', function($b) use ($tahun){
			        	$b->whereHas('program', function($c) use ($tahun){
				        	$c->whereHas('penugasanaudit', function($e) use ($tahun){
					        	$e->whereHas('rencanadetail', function($u) use ($tahun){
					        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
					        			$z->where('tahun', 'like', '%' . $tahun . '%');
					        		});
					        	});
				        	});
			        	});
		        	});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('closing', function($a) use ($tipe){
		        	$a->whereHas('opening', function($b) use ($tipe){
			        	$b->whereHas('program', function($c) use ($tipe){
				        	$c->whereHas('penugasanaudit', function($e) use ($tipe){
					        	$e->whereHas('rencanadetail', function($u) use ($tipe){
					        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
					        			if($tipe == 1){
							        		$z->where('tipe', 0);
						        		}else{
						        			$z->where('tipe', 1);
						        		}
					        		});
					        	});
				        	});
			        	});
		        	});
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 0);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 2){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 1);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 3){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 2);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 4){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 3);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->whereHas('closing', function($a) use ($kat, $object_id){
		        	$a->whereHas('opening', function($b) use ($kat, $object_id){
			        	$b->whereHas('program', function($c) use ($kat, $object_id){
				        	$c->whereHas('penugasanaudit', function($e) use ($kat, $object_id){
				        		$e->whereHas('rencanadetail', function($u) use ($kat, $object_id){
				        			$u->where('tipe_object', $kat)->where('object_id', $object_id);
				        		});
			        		});
			        	});
		        	});
	        	});
	        }
        }elseif($user->hasJabatan(['SVP','Project'])){
	        $records = DraftKka::whereHas('detaildraft', function($a) use ($user){
    								$a->where('user_id', $user->id);
	        					})
	        					->whereIn('status', [5,6])->select('*');
        	if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at','desc');
	        }
	        if($tahun = request()->tahun) {
	        	$records->whereHas('closing', function($a) use ($tahun){
		        	$a->whereHas('opening', function($b) use ($tahun){
			        	$b->whereHas('program', function($c) use ($tahun){
				        	$c->whereHas('penugasanaudit', function($e) use ($tahun){
					        	$e->whereHas('rencanadetail', function($u) use ($tahun){
					        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
					        			$z->where('tahun', 'like', '%' . $tahun . '%');
					        		});
					        	});
				        	});
			        	});
		        	});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('closing', function($a) use ($tipe){
		        	$a->whereHas('opening', function($b) use ($tipe){
			        	$b->whereHas('program', function($c) use ($tipe){
				        	$c->whereHas('penugasanaudit', function($e) use ($tipe){
					        	$e->whereHas('rencanadetail', function($u) use ($tipe){
					        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
					        			if($tipe == 1){
							        		$z->where('tipe', 0);
						        		}else{
						        			$z->where('tipe', 1);
						        		}
					        		});
					        	});
				        	});
			        	});
		        	});
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 0);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 2){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 1);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 3){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 2);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}elseif($kategori == 4){
	        		$records->whereHas('closing', function($a){
		        		$a->whereHas('opening', function($b){
			        		$b->whereHas('program', function($c){
				        		$c->whereHas('penugasanaudit', function($e){
					        		$e->whereHas('rencanadetail', function($u){
						        		$u->where('tipe_object', 3);
						        	});
				        		});
			        		});
		        		});
	        		});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->whereHas('closing', function($a) use ($kat, $object_id){
		        	$a->whereHas('opening', function($b) use ($kat, $object_id){
			        	$b->whereHas('program', function($c) use ($kat, $object_id){
				        	$c->whereHas('penugasanaudit', function($e) use ($kat, $object_id){
				        		$e->whereHas('rencanadetail', function($u) use ($kat, $object_id){
				        			$u->where('tipe_object', $kat)->where('object_id', $object_id);
				        		});
			        		});
			        	});
		        	});
	        	});
	        }
        }else{
        	$records = DraftKka::where('id', 0);
        }
        return DataTables::of($records->get())
           ->addColumn('num', function ($record) {
               return request()->start;
           })
           ->addColumn('tahun', function ($record) {
               return $record->program->penugasanaudit->rencanadetail->rencanaaudit->tahun;
           })
           ->addColumn('tipe', function ($record) {
                $tipe = '';
                if($record->program->penugasanaudit->rencanadetail->tipe == 0){
               	  $tipe = '<span class="label label-primary">Audit</span>';
                }elseif($record->program->penugasanaudit->rencanadetail->tipe == 1){
               	  $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
                }else{
                  $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
                }

                return $tipe;
           })
           ->addColumn('kategori', function ($record) {
                $kategori = '';
                if($record->program->penugasanaudit->rencanadetail->tipe == 0){
	                if($record->program->penugasanaudit->rencanadetail->tipe_object == 0){
	               	  $kategori = '<span class="label label-success">Business Unit (BU)</span>';
	                }elseif($record->program->penugasanaudit->rencanadetail->tipe_object == 1){
	               	  $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
	                }elseif($record->program->penugasanaudit->rencanadetail->tipe_object == 2){
	               	  $kategori = '<span class="label label-warning">Project</span>';
	                }else{
	               	  $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
	                }
                }elseif($record->program->penugasanaudit->rencanadetail->tipe == 1){
               	  	$kategori = $record->program->penugasanaudit->rencanadetail->konsultasi->nama;
                }else{
                    $kategori = $record->program->penugasanaudit->rencanadetail->lain->nama;
                }
                return $kategori;
           })
           ->addColumn('object_audit', function ($record) {
               return object_audit($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
           })
           ->addColumn('no_ab', function ($record) {
               return getAb($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
           })
           ->addColumn('rencana', function ($record) {
                return DateToString($record->program->detailpelaksanaan->first()->tgl).' - '.DateToString($record->program->detailpelaksanaan->last()->tgl);
           })
           ->addColumn('nama', function ($record) {
           		if($record->program->penugasanaudit->rencanadetail->tipe_object == 2){
               		return object_audit($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
               	}else{
               		return '';
               	}
           })
           ->addColumn('detil', function ($record) use ($user) {
               $buttons = '';
               if($user->hasJabatan(['SVP - Internal Audit','President Director'])){
	               $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
	                    'tooltip' 	=> 'Rencana Kerja',
	                    'class' 	=> 'download-rkia button',
	                    'id'   		=> $record->program->penugasanaudit->rencanadetail->rencanaaudit->id,
	               ]);
               }else{
               		$buttons .='';
               }
               return $buttons;
           })
           ->addColumn('penugasan', function ($record) {
               $buttons = '';
               if($record->program->penugasanaudit->status == 4){
	               $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
	                    'tooltip' 	=> 'Surat Penugasan',
	                    'class' 	=> 'download-penugasan button',
	                    'id'   		=> $record->program->penugasanaudit->id,
	               ]);
               }else{
               	    $buttons .='';
               }
               return $buttons;
           })
           ->addColumn('tinjauan', function ($record) {
               $buttons = '';
               if($record->program->penugasanaudit->tinjauandokumen){
	               $buttons .= $this->makeButton([
	           			'type' 		=> 'edit',
	           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
	           			'tooltip' 	=> 'Detil Tinjauan',
	           			'class' 	=> 'detil-tinjauan button',
	           			'id'   		=> $record->program->penugasanaudit->tinjauandokumen->id,
	           		]);
               }

               return $buttons;
           })
           ->addColumn('detil_program', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
                    'type' 		=> 'edit',
                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
                    'tooltip' 	=> 'Program Audit',
                    'class' 	=> 'download-program button',
                    'id'   		=> $record->program->id,
               ]);
               return $buttons;
           })
           ->addColumn('detil_draft', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
           			'type' 		=> 'edit',
           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
           			'tooltip' 	=> 'Detil Final KKA',
           			'class' 	=> 'detil-draft button',
           			'id'   		=> $record->id,
           		]);

               return $buttons;
           })
           ->addColumn('action', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
                    'type' 		=> 'edit',
                    'label' 	=> '<i class="fa fa-print text-info"></i>',
                    'tooltip' 	=> 'Cetak',
                    'class' 	=> 'cetak-kka button',
                    'id'   		=> $record->id,
               ]);

               return $buttons;
           })
           ->editColumn('status', function ($record) {
               return '<span class="label label-success">Completed</span>';
           })
           ->rawColumns(['tipe', 'action','detil','detil_draft','penugasan','tinjauan','detil_program','detil_project','kategori','object_audit','status'])
           ->make(true);
    }

    public function index()
    {
    	$user = auth()->user();
    	if($user->hasJabatan(['SVP - Internal Audit','President Director','Secretary'])){
        	$nbaru = ProgramAudit::whereHas('penugasanaudit', function($u){
	        							$u->whereHas('tinjauandokumen', function($a){
	        								$a->where('status', 5);
	        							});
	        						})->where('status_kka', 0)->whereIn('status', [4,5])->get()->count();
        	$nprog = DraftKka::whereIn('status', [1,2,3,4])->get()->count();
       	}elseif($user->hasJabatan(['auditor'])){
        	$nbaru = ProgramAudit::whereHas('penugasanaudit', function($u){
	        							$u->whereHas('tinjauandokumen', function($a){
	        								$a->where('status', 5);
	        							});
	        						})->whereHas('detailanggota', function($u) use ($user){
    									$u->where('user_id', $user->id);
    								})
	        						->where('status_kka', 0)->whereIn('status', [4,5])->get()->count();
        	$nprog = DraftKka::whereHas('program', function($a) use ($user){
	        						$a->whereHas('detailanggota', function($u) use ($user){
    									$u->where('user_id', $user->id);
    								});
	        					})
	        					->whereIn('status', [1,2,3,4])->get()->count();
       	}elseif($user->hasJabatan(['SVP','Project'])){
       		$cari = checkuser($user->id);
    		$tipe_object = $cari[0];
    		$object_id = $cari[1];
        	$nbaru = ProgramAudit::where('id', 0)->get()->count();
        	$nprog = DraftKka::whereHas('program', function($a) use ($tipe_object, $object_id, $user){
    								$a->whereHas('penugasanaudit', function($x) use ($tipe_object, $object_id, $user){
										$x->whereHas('rencanadetail', function($y) use ($tipe_object, $object_id, $user){
											$y->where('tipe_object', $tipe_object)
											  ->whereIn('object_id', $object_id);
										})->orWhere('user_id', $user->id);
									});
	        					})
	        					->whereIn('status', [1,2,3,4])->get()->count();
       	}else{
       		$nbaru = ProgramAudit::where('id', 0)->get()->count();
       		$nprog = DraftKka::where('id', 0)->get()->count();
       	}
        return $this->render('modules.audit.pelaksanaan.draft-kka.index', [
        	'mockup' => true,
        	'structs' => $this->listStructs,
        	'nbaru' => $nbaru,
        	'nprog' => $nprog,
        ]);
    }

    public function store(DraftKkaRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$cek_rekomendasi =[];
    		if($request->status > 0){
    			if(!empty($request->id)){
		        	$draft 				= DraftKka::find($request->id);
		        	$draft->program_id 	= $request->program_id;
		    		if($request->tipe == 1){
		    			$draft->status_operasional = 2;
		    			if($draft->status_keuangan == 2 && $draft->status_sistem == 2){
		    				$draft->status 			= 1;
		    			}
		    		}elseif($request->tipe == 2){
		    			$draft->status_keuangan = 2;
		    			if($draft->status_operasional == 2 && $draft->status_sistem == 2){
		    				$draft->status 			= 1;
		    			}
		    		}else{
		    			$draft->status_sistem = 2;
		    			if($draft->status_operasional == 2 && $draft->status_keuangan == 2){
		    				$draft->status 			= 1;
		    			}
		    		}
		    		if($draft->status == 1){
			    		$programaudit = ProgramAudit::find($request->program_id);
			    		$programaudit->status_kka = 1;
			    		$programaudit->save();
		    		}
		    		$draft->save();
		        	$draft->updateDetail($request->fokus_audit, $cek_rekomendasi);
    			}else{
    				$program = ProgramAudit::find($request->program_id);
    				$tipes = $program->penugasanaudit->rencanadetail->tipe_object;

    				$draft 				= new DraftKka;
		        	$draft->program_id 	= $request->program_id;
    				if($tipes == 2){
    					$bu = findbupic($program->penugasanaudit->rencanadetail->object_id);
    					$draft->parent_svp 			= $bu;
    				}else{
    					$draft->parent_svp 			= 0;
    				}
		    		$draft->status 			= 0;
		    		if($request->tipe == 1){
		    			$draft->status_operasional = 2;
		    		}elseif($request->tipe == 2){
		    			$draft->status_keuangan = 2;
		    		}else{
		    			$draft->status_sistem = 2;
		    		}

		        	$draft->save();
		        	$draft->saveDetail($request->fokus_audit);
    			}

	    		$user = auth()->user();
		    	$log = new SessionLog;
			    $log->modul = 'Kegiatan Audit Pelaksanaan';
			    $log->sub = 'KKA';
			    $log->aktivitas = 'Membuat KKA Tahun '.$draft->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($draft->program->penugasanaudit->rencanadetail->tipe_object, $draft->program->penugasanaudit->rencanadetail->object_id);
			    $log->user_id = $user->id;
			    $log->save();


	        	$update_notif = Notification::where('parent_id', $request->program_id)->where('modul', 'closing-meeting')->where('stage', 1)->get();
		        foreach ($update_notif as $key => $vil) {
		        	$data_notif = Notification::find($vil->id);
			        $data_notif->status =0;
			        $data_notif->save();
		        }

				$notif = new Notification;
			    $notif->user_id = $draft->program->user_id;
			    $notif->parent_id = $draft->id;
			    $notif->url = route($this->routes.'.index');
			    $notif->modul = 'draft-kka';
			    $notif->stage = 0;
			    $notif->keterangan = 'KKA Tahun '.$draft->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($draft->program->penugasanaudit->rencanadetail->tipe_object, $draft->program->penugasanaudit->rencanadetail->object_id);
			    $notif->status = 1;
			    $notif->save();
    		}else{
    			if(!empty($request->id)){
    				$draft 				= DraftKka::find($request->id);
		        	$draft->program_id 	= $request->program_id;
		        	if($request->tipe == 1){
		    			$draft->status_operasional = 1;
		    		}elseif($request->tipe == 2){
		    			$draft->status_keuangan = 1;
		    		}else{
		    			$draft->status_sistem = 1;
		    		}
	    			$draft->status 		= 0;
	    			$draft->save();
		        	$draft->updateDetail($request->fokus_audit, $cek_rekomendasi);
    			}else{
	    			$program = ProgramAudit::find($request->program_id);
    				$tipes = $program->penugasanaudit->rencanadetail->tipe_object;

    				$draft 				= new DraftKka;
		        	$draft->program_id 	= $request->program_id;
    				if($tipes == 2){
    					$bu = findbupic($program->penugasanaudit->rencanadetail->object_id);
    					$draft->parent_svp 			= $bu;
    				}else{
    					$draft->parent_svp 			= 0;
    				}
		        	if($request->tipe == 1){
		    			$draft->status_operasional = 1;
		    		}elseif($request->tipe == 2){
		    			$draft->status_keuangan = 1;
		    		}else{
		    			$draft->status_sistem = 1;
		    		}
	    			$draft->status 		= 0;
	    			$draft->save();
		        	$draft->saveDetail($request->fokus_audit);
    			}
    		}

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function detil(TinjauanDokumen $id)
    {
    	$cari = ProgramAudit::where('tinjauan_id', $id->id)->first();
    	$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Pelaksanaan' => '#', 'KKA' => '#', 'Detil' => '#']);
        return $this->render('modules.audit.pelaksanaan.draft-kka.detil', [
        	'program' => $cari,
        	'record' => $id,
        ]);
    }

    public function buat(ProgramAudit $id)
    {
    	$user = auth()->user();
    	$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Pelaksanaan' => '#', 'KKA' => '#', 'Buat' => '#']);
        return $this->render('modules.audit.pelaksanaan.draft-kka.create', [
        	'record' => $id,
        	'user' => $user->fungsi,
        ]);
    }

    public function ubah(DraftKka $id)
    {
    	$user = auth()->user();
    	$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Pelaksanaan' => '#', 'KKA' => '#', 'Ubah' => '#']);
        return $this->render('modules.audit.pelaksanaan.draft-kka.edit', [
        	'record' => $id,
        	'user' => $user->fungsi,
        ]);
    }

    public function ubahKKA(DraftKka $id)
    {
    	$user = auth()->user();
    	$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Pelaksanaan' => '#', 'KKA' => '#', 'Ubah Draft KKA' => '#']);
        return $this->render('modules.audit.pelaksanaan.draft-kka.edit-kka', [
        	'record' => $id,
        	'user' => $user->fungsi,
        ]);
    }

    public function ubahPersetujuan(DraftKka $id)
    {
    	$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Pelaksanaan' => '#', 'KKA' => '#', 'Ubah Tanggapan Auditee' => '#']);
        return $this->render('modules.audit.pelaksanaan.draft-kka.edit-persetujuan', [
        	'record' => $id,
        ]);
    }

    public function update(DraftKkaRequest $request)
    {
    	DB::beginTransaction();
        try {
        	$record = DraftKka::find($request->id);
        	if(!is_null($request->exists_operasional)){
		        $hapus_operasional = DraftKkaDetail::whereNotIn('id', $request->exists_operasional)
		        									->where('draft_id', $request->id)
		        									->where('tipe', 0)
		                        					->delete();

		        if(count($request->exists_operasional) > 0){
			        $cari = Files::whereIn('target_id', $request->exists_operasional)->where('target_type', 'kka')->get();
			        // if($cari){
			        // 	foreach ($cari as $key => $value) {
			        // 		unlink(public_path('storage/'.$value->url));
				    	  // 	$value->delete();
			        // 	}
			        // }
		        }
        	}
        	if(!is_null($request->exists_keuangan)){
		        $hapus_keuangan = DraftKkaDetail::whereNotIn('id', $request->exists_keuangan)
		        									->where('draft_id', $request->id)
		        									->where('tipe', 1)
		                        					->delete();

		        if(count($request->exists_keuangan) > 0){
			        $cari = Files::whereIn('target_id', $request->exists_keuangan)->where('target_type', 'kka')->get();
			        // if($cari){
			        // 	foreach ($cari as $key => $value) {
			        // 		unlink(public_path('storage/'.$value->url));
				    	  // 	$value->delete();
			        // 	}
			        // }
		        }
        	}

        	if(!is_null($request->exists_sistem)){
		        $hapus_sistem = DraftKkaDetail::whereNotIn('id', $request->exists_sistem)
		        									->where('draft_id', $request->id)
		        									->where('tipe', 2)
		                        					->delete();

		        if(count($request->exists_sistem) > 0){
			        $cari = Files::whereIn('target_id', $request->exists_sistem)->where('target_type', 'kka')->get();
			        // if($cari){
			        // 	foreach ($cari as $key => $value) {
			        // 		unlink(public_path('storage/'.$value->url));
				    	  // 	$value->delete();
			        // 	}
			        // }
		        }
        	}
        	if($request->status == 1){
        		if($request->tipe == 1){
	    			$record->status_operasional = 2;
	    			if($record->status_keuangan == 2 && $record->status_sistem == 2){
	    				$record->status = 1;
	    			}
		        	$record->ket_ketua = null;
		        	$record->save();
	    			$cek_rekomendasi = $request->exist_rekomendasi_operasional;
	    		}elseif($request->tipe == 2){
	    			$record->status_keuangan = 2;
	    			if($record->status_operasional == 2 && $record->status_sistem == 2){
	    				$record->status = 1;
	    			}
	    			$record->ket_ketua = null;
		        	$record->save();
	    			$cek_rekomendasi = $request->exist_rekomendasi_keuangan;
	    		}else{
	    			$record->status_sistem = 2;
	    			if($record->status_operasional == 2 && $record->status_keuangan == 2){
			    		$record->status = 1;
	    			}
	    			$record->ket_ketua = null;
		        	$record->save();
	    			$cek_rekomendasi = $request->exist_rekomendasi_sistem;
	    		}
	    		
	    		if($record->status == 1){
		        	$programaudit = ProgramAudit::find($record->program_id);
		    		$programaudit->status_kka = 1;
		    		$programaudit->save();
	    		}

	        	$record->updateDetail($request->fokus_audit, $cek_rekomendasi);

	        	$user = auth()->user();
		    	$log = new SessionLog;
			    $log->modul = 'Kegiatan Audit Pelaksanaan';
			    $log->sub = 'KKA';
			    $log->aktivitas = 'Mengubah KKA Tahun '.$record->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
			    $log->user_id = $user->id;
			    $log->save();
			    $update_notif = Notification::where('parent_id', $record->program->id)->where('modul', 'programaudit')->where('stage', 1)->get();
		        foreach ($update_notif as $key => $vil) {
		        	$data_notif = Notification::find($vil->id);
			        $data_notif->status =0;
			        $data_notif->save();
		        }

				$notif = new Notification;
			    $notif->user_id = $record->program->user_id;
			    $notif->parent_id = $record->id;
			    $notif->url = route($this->routes.'.index');
			    $notif->modul = 'draft-kka';
			    $notif->stage = 0;
			    $notif->keterangan = 'KKA Tahun '.$record->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
			    $notif->status = 1;
			    $notif->save();
    		}else{
    			if($request->tipe == 1){
		    		$cek_rekomendasi = $request->exist_rekomendasi_operasional;
	    			$record->status_operasional = 1;
	    		}elseif($request->tipe == 2){
	    			$record->status_keuangan = 1;
		    		$cek_rekomendasi = $request->exist_rekomendasi_keuangan;
	    		}else{
	    			$record->status_sistem = 1;
		    		$cek_rekomendasi = $request->exist_rekomendasi_sistem;
	    		}
    			$record->status 			= 0;
	        	$record->save();
	        	$record->updateDetail($request->fokus_audit, $cek_rekomendasi);
	    	}

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function saveUbah(DraftKkaUbahRequest $request)
    {
    	DB::beginTransaction();
        try {
        	$record = DraftKka::find($request->id);
        	$record->status = 3;
        	$record->status_operasional = 0;
        	$record->status_keuangan = 0;
        	$record->status_sistem = 0;
        	$record->save();
        	$record->saveDetails($request->fokus_audit);
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function reject(DraftKka $id)
    {
        return $this->render('modules.audit.pelaksanaan.draft-kka.reject', [
        	'record' => $id,
        ]);
    }

    public function saveReject(RejectRequest $request)
    {
        DB::beginTransaction();
    	try {
    		$record 						= DraftKka::find($request->id);
	    	$record->status 				= 0;
	    	$record->ket_ketua 				= $request->ket_svp;
	    	$record->status_operasional 	= 1;
	    	$record->status_keuangan 		= 1;
	    	$record->status_sistem			= 1;
	        $record->save();

	        $programaudit = ProgramAudit::find($record->program_id);
	        $programaudit->status_kka =0;
	        $programaudit->save();
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function saveUbahKKA(Request $request)
    {
    	DB::beginTransaction();
        try {
        	if($request->fokus_audit){
	        	foreach ($request->fokus_audit as $key => $value) {
	        		foreach ($value['detail'] as $key => $vil) {
	        			if(!empty($vil['lampirans'])){
	        				if(!is_null($vil['lampirans'])){
					        	$record = DraftKkaDetail::find($vil['data_id']);
					        	if($lampiran = $vil['lampirans']){
					        		$path_lampiran = $lampiran->store('uploads/programaudit', 'public');
					        		$record->baname = $lampiran->getClientOriginalName();
					            	$record->ba = $path_lampiran;
					            }
					            $record->save();
	        				}
	        			}
	    				$draft = DraftKka::find($request->id);
	        			if($vil['tipe'] == 0){
	        				$draft->status_operasional =1;
	        			}elseif($vil['tipe'] == 1){
	        				$draft->status_keuangan =1;
	        			}else{
	        				$draft->status_sistem =1;
	        			}
	        			$draft->save();

	        			if($draft->status_operasional == 1 && $draft->status_keuangan == 1 && $draft->status_sistem == 1){
	        				$drafts = DraftKka::find($request->id);
	        				$drafts->status = 4;
	        				$drafts->save();
	        			}
		        	}
	        	}
        	}else{
        		$draft = DraftKka::find($request->id);
    			if($request->tipe == 0){
    				$draft->status_operasional =1;
    			}elseif($request->tipe == 1){
    				$draft->status_keuangan =1;
    			}else{
    				$draft->status_sistem =1;
    			}
    			$draft->save();

    			if($draft->status_operasional == 1 && $draft->status_keuangan == 1 && $draft->status_sistem == 1){
    				$drafts = DraftKka::find($request->id);
    				$drafts->status = 4;
    				$drafts->save();
    			}
        	}
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function submit(Request $request)
    {
    	DB::beginTransaction();
        try {
        	$record = DraftKka::find($request->id);
        	$record->status =2;
        	$record->save();

        	$user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Kegiatan Audit Pelaksanaan';
		    $log->sub = 'KKA';
		    $log->aktivitas = 'Menyelesaikan KKA Tahun '.$record->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
		    $log->user_id = $user->id;
		    $log->save();

		    $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'draft-kka')->where('stage', 0)->get();
	        foreach ($update_notif as $key => $vil) {
	        	$data_notif = Notification::find($vil->id);
		        $data_notif->status =0;
		        $data_notif->save();
	        }

			$userz = User::whereHas('roles', function($u){
							$u->where('name', 'auditor');
						})->get();
			foreach ($userz as $key => $vul) {
				$notif = new Notification;
			    $notif->user_id = $vul->id;
			    $notif->parent_id = $record->id;
			    $notif->url = route($this->routes.'.index');
			    $notif->modul = 'draft-kka';
			    $notif->stage = 0;
			    $notif->keterangan = 'Pembuatan LHA Tahun '.$record->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
			    $notif->status = 1;
			    $notif->save();
			}

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function approval(Request $request)
    {
    	DB::beginTransaction();
        try {
        	$record = DraftKka::find($request->id);
        	$record->status = 2;
        	$record->save();

        	$user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Kegiatan Audit Pelaksanaan';
		    $log->sub = 'KKA';
		    $log->aktivitas = 'Menyelesaikan KKA Tahun '.$record->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
		    $log->user_id = $user->id;
		    $log->save();

		    $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'draft-kka')->where('stage', 0)->get();
	        foreach ($update_notif as $key => $vil) {
	        	$data_notif = Notification::find($vil->id);
		        $data_notif->status =0;
		        $data_notif->save();
	        }

			$userz = User::whereHas('roles', function($u){
							$u->where('name', 'auditor');
						})->get();
			foreach ($userz as $key => $vul) {
				$notif = new Notification;
			    $notif->user_id = $vul->id;
			    $notif->parent_id = $record->id;
			    $notif->url = route($this->routes.'.index');
			    $notif->modul = 'draft-kka';
			    $notif->stage = 0;
			    $notif->keterangan = 'Pembuatan LHA Tahun '.$record->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
			    $notif->status = 1;
			    $notif->save();
			}

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function cetak(Request $request, $id){
    	$record = DraftKka::find($id);
        $data = [
        	'records' => $record,
        ];
	    $pdf = PDF::loadView('modules.audit.pelaksanaan.draft-kka.cetak', $data);
        return $pdf->stream('Draft KKA '.$record->tanggal.'.pdf',array("Attachment" => false));
 	}

    public function destroy(ProgramAudit $program)
    {
        $program->delete();
        return response()->json([
            'success' => true
        ]);
    }

    public function detilProject(RencanaAuditDetail $id)
    {
        return $this->render('modules.audit.pelaksanaan.draft-kka.detil-project', ['record' => $id]);
    }

    public function detilPenugasan(PenugasanAudit $id)
    {
        return $this->render('modules.audit.pelaksanaan.draft-kka.detil-penugasan', ['record' => $id]);
    }

    public function detilTinjauan(TinjauanDokumen $id)
    {
    	if($id->group > 0){
	    	$cari = TinjauanDokumen::where('group', $id->group)->get();
    	}else{
    		$cari[]= $id;
    	}
        return $this->render('modules.audit.pelaksanaan.draft-kka.detil-tinjauan', [
        	'record' => $id,
        	'cek' => $cari,
        ]);
    }

    public function detilProgram(ProgramAudit $id)
    {
        return $this->render('modules.audit.pelaksanaan.draft-kka.detil-program', ['record' => $id]);
    }

    public function detilDraft(DraftKka $id)
    {
        return $this->render('modules.audit.pelaksanaan.draft-kka.detil-draft', ['record' => $id]);
    }

    public function cetakAudit(Request $request, $id){
    	$record = RencanaAudit::find($id);
        $data = [
        	'records' => $record,
        ];
	    $pdf = PDF::loadView('modules.audit.pelaksanaan.draft-kka.cetak-audit', $data)->setPaper('a4', 'landscape');
        return $pdf->stream('Rencana Audit.pdf',array("Attachment" => false));
 	}

 	public function cetakDraft(Request $request, $id){
    	$record = DraftKka::find($id);
        $data = [
        	'records' => $record,
        ];
	    $pdf = PDF::loadView('modules.audit.pelaksanaan.draft-kka.cetak-kka', $data)->setPaper('a4', 'landscape');
        return $pdf->stream('Draft KKA.pdf',array("Attachment" => false));
 	}

 	public function cetakPenugasan(Request $request, $id){
    	$record = PenugasanAudit::find($id);
        $data = [
        	'records' => $record,
        ];

        $merge = new Merger();

	    $pdf = PDF::loadView('modules.audit.pelaksanaan.draft-kka.cetak-penugasan', $data);
	    $merge->addRaw($pdf->output());
	    $pdf2 = PDF::loadView('modules.audit.pelaksanaan.draft-kka.cetak-jadwal', $data);
	    $pdf2->setPaper('a4', 'landscape');
	    $merge->addRaw($pdf2->output());

        return new Response($merge->merge(), 200, array(
            'Content-Type' => 'application/pdf',
            'Content-Disposition' =>  'inline; filename="Penugasan Audit.pdf"',
        ));
    }

    public function downloadAudit($id)
    {
    	$daftar = RencanaAudit::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return abort(404);
    }

    public function downloadPenugasan($id)
    {
    	$daftar = PenugasanAudit::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return abort(404);
    }

    public function downloadProgramAudit($id)
    {
    	$daftar = ProgramAudit::find($id);
    	if(file_exists(public_path('storage/'.$daftar->lampiran)))
    	{
    		return response()->download(public_path('storage/'.$daftar->lampiran), $daftar->lampiranname);
    	}
    	return abort(404);
    }

    public function simpanDokumen(DokumenRequest $request)
    {
    	$user = auth()->user();
    	DB::beginTransaction();
        try {
        	$record 			= TinjauanDokumen::find($request->id);
        	$record->status 	= 3;
	        $record->save();
        	$record->uploadDetail($request->detail);

        	$update_notif = Notification::where('parent_id', $record->id)->where('modul', 'permintaan-dokumen')->where('stage', 1)->get();
	        foreach ($update_notif as $key => $vil) {
	        	$data_notif = Notification::find($vil->id);
		        $data_notif->status =0;
		        $data_notif->save();
	        }

			$userz = User::whereHas('roles', function($u){
									$u->where('name', 'auditor');
								})->get();
			foreach ($userz as $key => $vul) {
				$notif = new Notification;
			    $notif->user_id = $vul->id;
			    $notif->parent_id = $record->id;
			    $notif->url = route($this->routes.'.index');
			    $notif->modul = 'permintaan-dokumen';
			    $notif->stage = 2;
			    $notif->keterangan = 'Approval Dokumen Tahun '.$record->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
			    $notif->status = 1;
			    $notif->save();
			}
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function upload(DraftKka $id)
    {
        return $this->render('modules.audit.pelaksanaan.draft-kka.upload', [
        	'record' => $id,
        ]);
    }

    public function saveUpload(DraftKkaUploadRequest $request)
    {
        DB::beginTransaction();
    	try {
    		$record 						= DraftKka::find($request->id);
    		if($lampiran = $request->lampiran){
        		$path_lampiran = $lampiran->store('uploads/programaudit', 'public');
        		$record->lampiranname = $lampiran->getClientOriginalName();
        		$record->lampiran = $path_lampiran;
            }

            $register = new RegisterTL;
            $register->draft_id = $record->id;
            $register->status = 0;
            $register->save();

            foreach ($record->detaildraft as $key => $value) {
            	foreach ($value->rekomendasi as $key => $val) {
	            	$detail_register = new RegisterTLDetail;
	            	$detail_register->register_id = $register->id;
	            	$detail_register->kka_detail_id = $value->id;
	            	$detail_register->rekomendasi_id = $val->id;
	            	$detail_register->fungsi = $value->tipe;
	            	$detail_register->save();
            	}
            }

        	$record->dms_update = 1;
    		$record->status = 5;
    		$record->save();
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }
}
