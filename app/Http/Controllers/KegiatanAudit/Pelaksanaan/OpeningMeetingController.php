<?php

namespace App\Http\Controllers\KegiatanAudit\Pelaksanaan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Response;
use iio\libmergepdf\Merger;

use App\Http\Requests\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeetingRequest;
use App\Models\KegiatanAudit\Rencana\RencanaAudit;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit;
use App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumen;
use App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting;
use App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeetingDetail;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditDetail;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\Auths\User;
use App\Models\Files;
use App\Models\SessionLog;
use App\Models\Notification;

use DB;
use PDF;

class OpeningMeetingController extends Controller
{
    protected $routes = 'kegiatan-audit.pelaksanaan.opening-meeting';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Kegiatan Audit' => '#', 'Pelaksanaan' => '#', 'Opening Meeting' => '#']);
        $this->listStructs = [
        	'listStruct' => [
		        	[
		                'data' => 'num',
		                'name' => 'num',
		                'label' => '#',
		                'orderable' => false,
		                'searchable' => false,
		                'className' => 'text-center',
		                'width' => '20px',
		            ],
		            [
		                'data' => 'tahun',
		                'name' => 'tahun',
		                'label' => 'Tahun',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'rencana',
		                'name' => 'rencana',
		                'label' => 'Waktu Pelaksanaan',
		                'className' => 'text-center',
		                'width' => '300px',
		            ],
		            [
		                'data' => 'tipe',
		                'name' => 'tipe',
		                'label' => 'Tipe',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'kategori',
		                'name' => 'kategori',
		                'label' => 'Kategori',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'object_audit',
		                'name' => 'object_audit',
		                'label' => 'Objek Audit',
		                'className' => 'text-left',
		                'width' => '250px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'no_ab',
		                'name' => 'no_ab',
		                'label' => 'No AB',
		                'className' => 'text-center',
		                'width' => '150px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'detil',
		                'name' => 'detil',
		                'label' => 'Rencana Kerja',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'penugasan',
		                'name' => 'penugasan',
		                'label' => 'Surat Penugasan',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'tinjauan',
		                'name' => 'tinjauan',
		                'label' => 'Tinjauan Dokumen',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'detil_program',
		                'name' => 'detil_program',
		                'label' => 'Program Audit',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'status',
		                'name' => 'status',
		                'label' => 'Status',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'pic',
		                'name' => 'pic',
		                'label' => 'Tim Auditor',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '150px',
		            ],
		            [
		                'data' => 'action',
		                'name' => 'action',
		                'label' => 'Aksi',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '80px',
		                'className' => 'text-center'
		            ],
        	],
        	'listStruct2' => [
		        	[
		                'data' => 'num',
		                'name' => 'num',
		                'label' => '#',
		                'orderable' => false,
		                'searchable' => false,
		                'className' => 'text-center',
		                'width' => '20px',
		            ],
		            [
		                'data' => 'tahun',
		                'name' => 'tahun',
		                'label' => 'Tahun',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'rencana',
		                'name' => 'rencana',
		                'label' => 'Waktu Pelaksanaan',
		                'className' => 'text-center',
		                'width' => '300px',
		            ],
		            [
		                'data' => 'tipe',
		                'name' => 'tipe',
		                'label' => 'Tipe',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'kategori',
		                'name' => 'kategori',
		                'label' => 'Kategori',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'object_audit',
		                'name' => 'object_audit',
		                'label' => 'Objek Audit',
		                'className' => 'text-left',
		                'width' => '250px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'no_ab',
		                'name' => 'no_ab',
		                'label' => 'No AB',
		                'className' => 'text-center',
		                'width' => '150px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'detil',
		                'name' => 'detil',
		                'label' => 'Rencana Kerja',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'penugasan',
		                'name' => 'penugasan',
		                'label' => 'Surat Penugasan',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'tinjauan',
		                'name' => 'tinjauan',
		                'label' => 'Tinjauan Dokumen',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'detil_program',
		                'name' => 'detil_program',
		                'label' => 'Program Audit',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'pic',
		                'name' => 'pic',
		                'label' => 'Tim Auditor',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '150px',
		            ],
		            [
		                'data' => 'status',
		                'name' => 'status',
		                'label' => 'Status',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '100px',
		                'className' => 'text-center'
		            ],
		            [
		                'data' => 'action',
		                'name' => 'action',
		                'label' => 'Aksi',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '80px',
		                'className' => 'text-center'
		            ],
        	],
        	'listStruct3' => [
		        	[
		                'data' => 'num',
		                'name' => 'num',
		                'label' => '#',
		                'orderable' => false,
		                'searchable' => false,
		                'className' => 'text-center',
		                'width' => '20px',
		            ],
		            [
		                'data' => 'tahun',
		                'name' => 'tahun',
		                'label' => 'Tahun',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'rencana',
		                'name' => 'rencana',
		                'label' => 'Waktu Pelaksanaan',
		                'className' => 'text-center',
		                'width' => '300px',
		            ],
		            [
		                'data' => 'tipe',
		                'name' => 'tipe',
		                'label' => 'Tipe',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'kategori',
		                'name' => 'kategori',
		                'label' => 'Kategori',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'object_audit',
		                'name' => 'object_audit',
		                'label' => 'Objek Audit',
		                'className' => 'text-left',
		                'width' => '250px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'no_ab',
		                'name' => 'no_ab',
		                'label' => 'No AB',
		                'className' => 'text-center',
		                'width' => '150px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'detil',
		                'name' => 'detil',
		                'label' => 'Rencana Kerja',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'penugasan',
		                'name' => 'penugasan',
		                'label' => 'Surat Penugasan',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'tinjauan',
		                'name' => 'tinjauan',
		                'label' => 'Tinjauan Dokumen',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'detil_program',
		                'name' => 'detil_program',
		                'label' => 'Program Audit',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'status',
		                'name' => 'status',
		                'label' => 'Status',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '100px',
		                'className' => 'text-center'
		            ],
		            [
		                'data' => 'action',
		                'name' => 'action',
		                'label' => 'Aksi',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '80px',
		                'className' => 'text-center'
		            ],
        	],
        ];
    }

    public function grid()
    {
    	$user = auth()->user();
    	if($user->hasJabatan(['SVP','Project'])){
	        $records = ProgramAudit::whereHas('detailanggota', function($u) use ($user){
    									$u->where('user_id', $user->id);
    								})->whereHas('penugasanaudit', function($u){
	        							$u->whereHas('tinjauandokumen', function($a){
	        								$a->where('status', 5);
	        							});
	        						})
	        						->where('status_kka','!=', 10)
	        						->doesntHave('opening')->whereIn('status', [4,5])
	        						->select('*');
	       	if(!isset(request()->order[0]['column'])) {
	              $records->orderBy('created_at','desc');
	        }
	        if($tahun = request()->tahun) {
	        	$records->whereHas('penugasanaudit', function($b) use ($tahun){
		        	$b->whereHas('rencanadetail', function($u) use ($tahun){
		        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
		        			$z->where('tahun', 'like', '%' . $tahun . '%');
		        		});
		        	});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('penugasanaudit', function($b) use ($tipe){
		        	$b->whereHas('rencanadetail', function($u) use ($tipe){
		        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
		        			if($tipe == 1){
				        		$z->where('tipe', 0);
			        		}else{
			        			$z->where('tipe', 1);
			        		}
		        		});
		        	});
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('penugasanaudit', function($b){
		        		$b->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 0);
			        	});
	        		});
	        	}elseif($kategori == 2){
	        		$records->whereHas('penugasanaudit', function($b){
		        		$b->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 1);
			        	});
	        		});
	        	}elseif($kategori == 3){
	        		$records->whereHas('penugasanaudit', function($b){
		        		$b->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 2);
			        	});
	        		});
	        	}elseif($kategori == 4){
	        		$records->whereHas('penugasanaudit', function($b){
		        		$b->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 3);
			        	});
	        		});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->whereHas('penugasanaudit', function($b) use ($kat, $object_id){
	        		$b->whereHas('rencanadetail', function($u) use ($kat, $object_id){
	        			$u->where('tipe_object', $kat)->where('object_id', $object_id);
	        		});
        		});
	        }
       	}elseif($user->hasJabatan(['SVP - Internal Audit','President Director','auditor'])){
	        $records = ProgramAudit::doesntHave('opening')->whereIn('status', [4,5])
	        						->select('*');
	       	if(!isset(request()->order[0]['column'])) {
	              $records->orderBy('created_at','desc');
	        }
	        if($tahun = request()->tahun) {
	        	$records->whereHas('penugasanaudit', function($b) use ($tahun){
		        	$b->whereHas('rencanadetail', function($u) use ($tahun){
		        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
		        			$z->where('tahun', 'like', '%' . $tahun . '%');
		        		});
		        	});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('penugasanaudit', function($b) use ($tipe){
		        	$b->whereHas('rencanadetail', function($u) use ($tipe){
		        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
		        			if($tipe == 1){
				        		$z->where('tipe', 0);
			        		}else{
			        			$z->where('tipe', 1);
			        		}
		        		});
		        	});
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('penugasanaudit', function($b){
		        		$b->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 0);
			        	});
	        		});
	        	}elseif($kategori == 2){
	        		$records->whereHas('penugasanaudit', function($b){
		        		$b->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 1);
			        	});
	        		});
	        	}elseif($kategori == 3){
	        		$records->whereHas('penugasanaudit', function($b){
		        		$b->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 2);
			        	});
	        		});
	        	}elseif($kategori == 4){
	        		$records->whereHas('penugasanaudit', function($b){
		        		$b->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 3);
			        	});
	        		});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->whereHas('penugasanaudit', function($b) use ($kat, $object_id){
	        		$b->whereHas('rencanadetail', function($u) use ($kat, $object_id){
	        			$u->where('tipe_object', $kat)->where('object_id', $object_id);
	        		});
        		});
	        }
       	}else{
       		$records = ProgramAudit::where('id', 0);
       	}

        return DataTables::of($records->get())
           ->addColumn('num', function ($record) {
               return request()->start;
           })
           ->addColumn('tahun', function ($record) {
               return $record->penugasanaudit->rencanadetail->rencanaaudit->tahun;
           })
           ->addColumn('tipe', function ($record) {
                $tipe = '';
                if($record->penugasanaudit->rencanadetail->tipe == 0){
               	  $tipe = '<span class="label label-primary">Audit</span>';
                }elseif($record->penugasanaudit->rencanadetail->tipe == 1){
               	  $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
                }else{
                  $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
                }

                return $tipe;
           })
           ->addColumn('kategori', function ($record) {
                $kategori = '';
                if($record->penugasanaudit->rencanadetail->tipe == 0){
	                if($record->penugasanaudit->rencanadetail->tipe_object == 0){
	               	  $kategori = '<span class="label label-success">Business Unit (BU)</span>';
	                }elseif($record->penugasanaudit->rencanadetail->tipe_object == 1){
	               	  $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
	                }elseif($record->penugasanaudit->rencanadetail->tipe_object == 2){
	               	  $kategori = '<span class="label label-warning">Project</span>';
	                }else{
	               	  $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
	                }
                }elseif($record->penugasanaudit->rencanadetail->tipe == 1){
               	  	$kategori = $record->penugasanaudit->rencanadetail->konsultasi->nama;
                }else{
                    $kategori = $record->penugasanaudit->rencanadetail->lain->nama;
                }
                return $kategori;
           })
           ->addColumn('object_audit', function ($record) {
               return object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
           })
           ->addColumn('no_ab', function ($record) {
               return getAb($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
           })
           ->addColumn('rencana', function ($record) {
                return DateToString($record->detailpelaksanaan->first()->tgl).' - '.DateToString($record->detailpelaksanaan->last()->tgl);
           })
           ->addColumn('nama', function ($record) {
           		if($record->penugasanaudit->rencanadetail->tipe_object == 2){
               		return object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
               	}else{
               		return '';
               	}
           })
           ->addColumn('pic', function ($record) {
                $data = array();
                $tim ='';
           		$tim .='<ul class="list list-more1 list-unstyled" style="margin-top:10px;" data-display="3">';
           		$tim .='<li class="item">'.$record->user->name.'</li>';
           		foreach ($record->detailanggota->whereIn('fungsi', [1,2,3]) as $key => $value) {
           				$data[] = $value->user->name;
           		}
           		foreach (array_unique($data) as $key => $value) {
           			$tim .='<li class="item">'.$value.'</li>';
           		}
           		$tim .='</ul>';
               	return $tim;
           })
           ->addColumn('detil', function ($record) use ($user) {
               $buttons = '';
               if($user->hasJabatan(['SVP - Internal Audit','President Director'])){
	               $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
	                    'tooltip' 	=> 'Rencana Kerja',
	                    'class' 	=> 'download-rkia button',
	                    'id'   		=> $record->penugasanaudit->rencanadetail->rencanaaudit->id,
	               ]);
               }else{
               		$buttons .='';
               }
               return $buttons;
           })
           ->addColumn('penugasan', function ($record) {
               $buttons = '';
               if($record->penugasanaudit->status == 4){
	               $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
	                    'tooltip' 	=> 'Surat Penugasan',
	                    'class' 	=> 'download-penugasan button',
	                    'id'   		=> $record->penugasanaudit->id,
	               ]);
               }else{
               	    $buttons .='';
               }
               return $buttons;
           })
           ->addColumn('tinjauan', function ($record) {
               $buttons = '';
               if($record->penugasanaudit->tinjauandokumen){
	               $buttons .= $this->makeButton([
	           			'type' 		=> 'edit',
	           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
	           			'tooltip' 	=> 'Detil Tinjauan',
	           			'class' 	=> 'detil-tinjauan button',
	       				'id'   		=> $record->penugasanaudit->tinjauandokumen->id,
	           		]);
               }

               return $buttons;
           })
           ->addColumn('detil_program', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
                    'type' 		=> 'edit',
                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
                    'tooltip' 	=> 'Program Audit',
                    'class' 	=> 'download-program button',
                    'id'   		=> $record->id,
               ]);
               return $buttons;
           })
           ->editColumn('status', function ($record) {
               return '<span class="label label-default">Baru</span>';
           })
           ->editColumn('created_by', function ($record) {
               return $record->creatorName();
           })
           ->addColumn('action', function ($record) use ($user) {
               $buttons = '';
               if($user->hasJabatan('auditor')){
               		if($record->user_id == $user->id){
		               $buttons = $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i style="color:white;" class="fa fa-plus text-primary"></i>',
		                    'tooltip' 	=> 'Buat Opening Meeting',
		                    'class' 	=> 'btn btn-sm btn-info buat button',
		                    'id'   		=> $record->id,
		               ]);
               		}else{
               			$buttons ='';
               		}
               }else{
               		$buttons ='';
               }

               return $buttons;
           })
           ->rawColumns(['tipe', 'action','detil','penugasan','tinjauan','detil_program','detil_project','kategori','object_audit','status','pic'])
           ->make(true);
    }

    public function onProgress()
    {
    	$user = auth()->user();
    	if($user->hasJabatan(['SVP - Internal Audit','President Director','auditor'])){
	        $records = OpeningMeeting::where('status', 1)->select('*');
	       	if(!isset(request()->order[0]['column'])) {
	              $records->orderBy('created_at','desc');
	        }
	        if($tahun = request()->tahun) {
	        	$records->whereHas('program', function($a) use ($tahun){
		        	$a->whereHas('penugasanaudit', function($c) use ($tahun){
			        	$c->whereHas('rencanadetail', function($u) use ($tahun){
			        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
			        			$z->where('tahun', 'like', '%' . $tahun . '%');
			        		});
			        	});
		        	});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('program', function($a) use ($tipe){
		        	$a->whereHas('penugasanaudit', function($c) use ($tipe){
			        	$c->whereHas('rencanadetail', function($u) use ($tipe){
			        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
			        			if($tipe == 1){
					        		$z->where('tipe', 0);
				        		}else{
				        			$z->where('tipe', 1);
				        		}
			        		});
			        	});
		        	});
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('program', function($a){
		        		$a->whereHas('penugasanaudit', function($c){
			        		$c->whereHas('rencanadetail', function($u){
				        		$u->where('tipe_object', 0);
				        	});
		        		});
	        		});
	        	}elseif($kategori == 2){
	        		$records->whereHas('program', function($a){
		        		$a->whereHas('penugasanaudit', function($c){
			        		$c->whereHas('rencanadetail', function($u){
				        		$u->where('tipe_object', 1);
				        	});
		        		});
	        		});
	        	}elseif($kategori == 3){
	        		$records->whereHas('program', function($a){
		        		$a->whereHas('penugasanaudit', function($c){
			        		$c->whereHas('rencanadetail', function($u){
				        		$u->where('tipe_object', 2);
				        	});
		        		});
	        		});
	        	}elseif($kategori == 4){
	        		$records->whereHas('program', function($a){
		        		$a->whereHas('penugasanaudit', function($c){
			        		$c->whereHas('rencanadetail', function($u){
				        		$u->where('tipe_object', 3);
				        	});
		        		});
	        		});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->whereHas('program', function($a) use ($kat, $object_id){
		        	$a->whereHas('penugasanaudit', function($c) use ($kat, $object_id){
		        		$c->whereHas('rencanadetail', function($u) use ($kat, $object_id){
		        			$u->where('tipe_object', $kat)->where('object_id', $object_id);
		        		});
	        		});
	        	});
	        }
       	}elseif($user->hasJabatan(['SVP','Project'])){
       		$records = OpeningMeeting::whereHas('detailanggota', function($u) use ($user){
	        							$u->where('user_id', $user->id);
	        						})
	        						->orWhereHas('program', function($u) use ($user){
	        							$u->whereHas('detailanggota', function($a) use ($user){
	        								$a->where('user_id', $user->id);
	        							})->where('user_id', $user->id);
	        						})
	        						->where('status', 1)
	        						->select('*');
       	}else{
       		$records = ProgramAudit::where('id', 0);
       	}
        return DataTables::of($records->get())
           ->addColumn('num', function ($record) {
               return request()->start;
           })
           ->addColumn('tahun', function ($record) {
               return $record->program->penugasanaudit->rencanadetail->rencanaaudit->tahun;
           })
           ->addColumn('tipe', function ($record) {
                $tipe = '';
                if($record->program->penugasanaudit->rencanadetail->tipe == 0){
               	  $tipe = '<span class="label label-primary">Audit</span>';
                }elseif($record->program->penugasanaudit->rencanadetail->tipe == 1){
               	  $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
                }else{
                  $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
                }

                return $tipe;
           })
           ->addColumn('kategori', function ($record) {
                $kategori = '';
                if($record->program->penugasanaudit->rencanadetail->tipe == 0){
	                if($record->program->penugasanaudit->rencanadetail->tipe_object == 0){
	               	  $kategori = '<span class="label label-success">Business Unit (BU)</span>';
	                }elseif($record->program->penugasanaudit->rencanadetail->tipe_object == 1){
	               	  $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
	                }elseif($record->program->penugasanaudit->rencanadetail->tipe_object == 2){
	               	  $kategori = '<span class="label label-warning">Project</span>';
	                }else{
	               	  $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
	                }
                }elseif($record->program->penugasanaudit->rencanadetail->tipe == 1){
               	  	$kategori = $record->program->penugasanaudit->rencanadetail->konsultasi->nama;
                }else{
                    $kategori = $record->program->penugasanaudit->rencanadetail->lain->nama;
                }
                return $kategori;
           })
           ->addColumn('object_audit', function ($record) {
               return object_audit($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
           })
           ->addColumn('no_ab', function ($record) {
               return getAb($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
           })
           ->addColumn('rencana', function ($record) {
           		return DateToString($record->program->detailpelaksanaan->first()->tgl).' - '.DateToString($record->program->detailpelaksanaan->last()->tgl);
           })
           ->addColumn('nama', function ($record) {
           		if($record->program->penugasanaudit->rencanadetail->tipe_object == 2){
               		return object_audit($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
               	}else{
               		return '';
               	}
           })
           ->addColumn('detil', function ($record) use($user) {
               $buttons = '';
               if($user->hasJabatan(['SVP - Internal Audit','President Director'])){
	               $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
	                    'tooltip' 	=> 'Rencana Kerja',
	                    'class' 	=> 'download-rkia button',
	                    'id'   		=> $record->program->penugasanaudit->rencanadetail->rencanaaudit->id,
	               ]);
               }else{
               		$buttons .='';
               }
               return $buttons;
           })
           ->addColumn('penugasan', function ($record) {
               $buttons = '';
               if($record->program->penugasanaudit->status == 4){
	               $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
	                    'tooltip' 	=> 'Surat Penugasan',
	                    'class' 	=> 'download-penugasan button',
	                    'id'   		=> $record->program->penugasanaudit->id,
	               ]);
               }else{
               	    $buttons .='';
               }
               return $buttons;
           })
           ->addColumn('tinjauan', function ($record) {
               $buttons = '';
               if($record->program->penugasanaudit->tinjauandokumen){
	               $buttons .= $this->makeButton([
	           			'type' 		=> 'edit',
	           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
	           			'tooltip' 	=> 'Detil Tinjauan',
	           			'class' 	=> 'detil-tinjauan button',
	       				'id'   		=> $record->program->penugasanaudit->tinjauandokumen->id,
	           		]);
               }

               return $buttons;
           })
           ->addColumn('detil_program', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
                    'type' 		=> 'edit',
                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
                    'tooltip' 	=> 'Program Audit',
                    'class' 	=> 'download-program button',
                    'id'   		=> $record->program->id,
               ]);
               return $buttons;
           })
           ->editColumn('status', function ($record) {
	           return '<span class="label label-default">Draft</span>';
	       })
	       ->addColumn('pic', function ($record) {
                $data = array();
                $tim ='';
           		$tim .='<ul class="list list-more2 list-unstyled" style="margin-top:10px;" data-display="3">';
           		$tim .='<li class="item">'.$record->program->user->name.'</li>';
           		foreach ($record->program->detailanggota->whereIn('fungsi', [1,2,3]) as $key => $value) {
           				$data[] = $value->user->name;
           		}
           		foreach (array_unique($data) as $key => $value) {
           			$tim .='<li class="item">'.$value.'</li>';
           		}
           		$tim .='</ul>';
               	return $tim;
           })
           ->addColumn('action', function ($record) use ($user) {
               $buttons = '';
               if($user->hasJabatan('auditor')){
               		if($record->program->user_id == $user->id){
		               $buttons = $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-edit text-primary"></i>',
		                    'tooltip' 	=> 'Ubah Opening Meeting',
		                    'class' 	=> 'ubah button',
		                    'id'   		=> $record->id,
		               ]);
               		}else{
               			$buttons ='';
               		}
               }else{
               		$buttons = $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-eye text-primary"></i>',
	                    'tooltip' 	=> 'Detil',
	                    'class' 	=> 'detil-opening button',
	                    'id'   		=> $record->id,
	                ]);
               }

               return $buttons;
           })
           ->rawColumns(['detil','tipe','penugasan','tinjauan','detil_program','detil_project','kategori','object_audit','nama','action','status','pic'])
           ->make(true);
    }

    public function historis()
    {
        $user = auth()->user();
    	if($user->hasJabatan(['SVP - Internal Audit','President Director','auditor'])){
	        $records = OpeningMeeting::where('status', 2)
	        						->select('*');
	        if(!isset(request()->order[0]['column'])) {
	              $records->orderBy('created_at','desc');
	        }
	        if($tahun = request()->tahun) {
	        	$records->whereHas('program', function($a) use ($tahun){
		        	$a->whereHas('penugasanaudit', function($c) use ($tahun){
			        	$c->whereHas('rencanadetail', function($u) use ($tahun){
			        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
			        			$z->where('tahun', 'like', '%' . $tahun . '%');
			        		});
			        	});
		        	});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('program', function($a) use ($tipe){
		        	$a->whereHas('penugasanaudit', function($c) use ($tipe){
			        	$c->whereHas('rencanadetail', function($u) use ($tipe){
			        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
			        			if($tipe == 1){
					        		$z->where('tipe', 0);
				        		}else{
				        			$z->where('tipe', 1);
				        		}
			        		});
			        	});
		        	});
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('program', function($a){
		        		$a->whereHas('penugasanaudit', function($c){
			        		$c->whereHas('rencanadetail', function($u){
				        		$u->where('tipe_object', 0);
				        	});
		        		});
	        		});
	        	}elseif($kategori == 2){
	        		$records->whereHas('program', function($a){
		        		$a->whereHas('penugasanaudit', function($c){
			        		$c->whereHas('rencanadetail', function($u){
				        		$u->where('tipe_object', 1);
				        	});
		        		});
	        		});
	        	}elseif($kategori == 3){
	        		$records->whereHas('program', function($a){
		        		$a->whereHas('penugasanaudit', function($c){
			        		$c->whereHas('rencanadetail', function($u){
				        		$u->where('tipe_object', 2);
				        	});
		        		});
	        		});
	        	}elseif($kategori == 4){
	        		$records->whereHas('program', function($a){
		        		$a->whereHas('penugasanaudit', function($c){
			        		$c->whereHas('rencanadetail', function($u){
				        		$u->where('tipe_object', 3);
				        	});
		        		});
	        		});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->whereHas('program', function($a) use ($kat, $object_id){
		        	$a->whereHas('penugasanaudit', function($c) use ($kat, $object_id){
		        		$c->whereHas('rencanadetail', function($u) use ($kat, $object_id){
		        			$u->where('tipe_object', $kat)->where('object_id', $object_id);
		        		});
	        		});
	        	});
	        }
       	}elseif($user->hasJabatan(['SVP','Project'])){
       		$records = OpeningMeeting::whereHas('detailanggota', function($u) use ($user){
        							$u->where('user_id', $user->id);
        						})
        						->orWhereHas('program', function($u) use ($user){
        							$u->whereHas('detailanggota', function($a) use ($user){
        								$a->where('user_id', $user->id);
        							})->where('user_id', $user->id);
        						})
        						->where('status', 2)
        						->select('*');
       	}else{
       		$records = OpeningMeeting::where('id', 0);
       	}

        return DataTables::of($records->get())
           ->addColumn('num', function ($record) {
               return request()->start;
           })
           ->addColumn('tahun', function ($record) {
               return $record->program->penugasanaudit->rencanadetail->rencanaaudit->tahun;
           })
           ->addColumn('tipe', function ($record) {
                $tipe = '';
                if($record->program->penugasanaudit->rencanadetail->tipe == 0){
               	  $tipe = '<span class="label label-primary">Audit</span>';
                }elseif($record->program->penugasanaudit->rencanadetail->tipe == 1){
               	  $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
                }else{
                  $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
                }

                return $tipe;
           })
           ->addColumn('kategori', function ($record) {
                $kategori = '';
                if($record->program->penugasanaudit->rencanadetail->tipe == 0){
	                if($record->program->penugasanaudit->rencanadetail->tipe_object == 0){
	               	  $kategori = '<span class="label label-success">Business Unit (BU)</span>';
	                }elseif($record->program->penugasanaudit->rencanadetail->tipe_object == 1){
	               	  $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
	                }elseif($record->program->penugasanaudit->rencanadetail->tipe_object == 2){
	               	  $kategori = '<span class="label label-warning">Project</span>';
	                }else{
	               	  $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
	                }
                }elseif($record->program->penugasanaudit->rencanadetail->tipe == 1){
               	  	$kategori = $record->program->penugasanaudit->rencanadetail->konsultasi->nama;
                }else{
                    $kategori = $record->program->penugasanaudit->rencanadetail->lain->nama;
                }
                return $kategori;
           })
           ->addColumn('object_audit', function ($record) {
               return object_audit($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
           })
           ->addColumn('no_ab', function ($record) {
               return getAb($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
           })
           ->addColumn('rencana', function ($record) {
                return DateToString($record->program->detailpelaksanaan->first()->tgl).' - '.DateToString($record->program->detailpelaksanaan->last()->tgl);
           })
           ->addColumn('nama', function ($record) {
           		if($record->program->penugasanaudit->rencanadetail->tipe_object == 2){
               		return object_audit($record->program->penugasanaudit->rencanadetail->tipe_object, $record->program->penugasanaudit->rencanadetail->object_id);
               	}else{
               		return '';
               	}
           })
           ->addColumn('detil', function ($record) use($user) {
               $buttons = '';
               if($user->hasJabatan(['SVP - Internal Audit','President Director'])){
	               $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
	                    'tooltip' 	=> 'Rencana Kerja',
	                    'class' 	=> 'download-rkia button',
	                    'id'   		=> $record->program->penugasanaudit->rencanadetail->rencanaaudit->id,
	               ]);
               }else{
               		$buttons .='';
               }
               return $buttons;
           })
           ->addColumn('penugasan', function ($record) {
               $buttons = '';
               if($record->program->penugasanaudit->status == 4){
	               $buttons .= $this->makeButton([
	                    'type' 		=> 'edit',
	                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
	                    'tooltip' 	=> 'Surat Penugasan',
	                    'class' 	=> 'download-penugasan button',
	                    'id'   		=> $record->program->penugasanaudit->id,
	               ]);
               }else{
               	    $buttons .='';
               }
               return $buttons;
           })
           ->addColumn('tinjauan', function ($record) {
               $buttons = '';
               if($record->program->penugasanaudit->tinjauandokumen){
	               $buttons .= $this->makeButton([
	           			'type' 		=> 'edit',
	           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
	           			'tooltip' 	=> 'Detil Tinjauan',
	           			'class' 	=> 'detil-tinjauan button',
	       				'id'   		=> $record->program->penugasanaudit->tinjauandokumen->id,
	           		]);
               }

               return $buttons;
           })
           ->addColumn('detil_program', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
                    'type' 		=> 'edit',
                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
                    'tooltip' 	=> 'Program Audit',
                    'class' 	=> 'download-program button',
                    'id'   		=> $record->program->id,
               ]);
               return $buttons;
           })
           ->editColumn('status', function ($record) {
	           return '<span class="label label-success">Completed</span>';
	       })
           ->addColumn('action', function ($record) use ($user) {
               $buttons = '';
               $buttons = $this->makeButton([
                    'type' 		=> 'edit',
                    'label' 	=> '<i class="fa fa-eye text-primary"></i>',
                    'tooltip' 	=> 'Detil',
                    'class' 	=> 'detil-opening button',
                    'id'   		=> $record->id,
               ]);
               return $buttons;
           })
           ->rawColumns(['tipe','penugasan','tinjauan','detil_program','detil','kategori','object_audit','nama','status','action'])
           ->make(true);
    }

    public function index()
    {
    	$user = auth()->user();
    	if($user->hasJabatan(['SVP - Internal Audit','President Director','auditor'])){
        	$nbaru = ProgramAudit::whereHas('penugasanaudit', function($u){
	        							$u->whereHas('tinjauandokumen', function($a){
	        								$a->where('status', 5);
	        							});
	        						})->doesntHave('opening')->whereIn('status', [4,5])->get()->count();
        	$nprog = OpeningMeeting::where('status', 1)->get()->count();
       	}elseif($user->hasJabatan(['SVP','Project'])){
       		$nbaru = ProgramAudit::whereHas('penugasanaudit', function($u){
	        							$u->whereHas('tinjauandokumen', function($a){
	        								$a->where('status', 5);
	        							});
	        						})->whereHas('detailanggota', function($u) use ($user){
    									$u->where('user_id', $user->id);
    								})->doesntHave('opening')->whereIn('status', [4,5])->get()->count();
        	$nprog = OpeningMeeting::whereHas('detailanggota', function($u) use ($user){
	        							$u->where('user_id', $user->id);
	        						})
	        						->orWhereHas('program', function($u) use ($user){
	        							$u->whereHas('detailanggota', function($a) use ($user){
	        								$a->where('user_id', $user->id);
	        							})->where('user_id', $user->id);
	        						})
	        						->where('status', 1)->get()->count();
       	}else{
       		$nbaru = ProgramAudit::where('id', 0)->get()->count();
       		$nprog = OpeningMeeting::where('id', 0)->get()->count();
       	}
        return $this->render('modules.audit.pelaksanaan.opening-meeting.index', [
        	'mockup' => true,
        	'structs' => $this->listStructs,
        	'nbaru' => $nbaru,
        	'nprog' => $nprog,
        ]);
    }

    public function store(OpeningMeetingRequest $request)
    {
    	DB::beginTransaction();
    	try {
        	$opening 				= new OpeningMeeting;
        	$opening->program_id 	= $request->program_id;
        	$opening->tanggal 		= $request->tanggal;
	        $opening->tempat  		= $request->tempat;
        	$opening->mulai  		= $request->mulai;
        	$opening->selesai  		= $request->selesai;
        	if($request->status == 0){
    			$opening->status 		 	= 1;
    		}else{
    			$opening->status 		 	= 2;
    		}
        	if($lampiran = $request->lampiran){
        		$path_lampiran = $lampiran->store('uploads/lampiran', 'public');
        		$opening->lampiranname = $lampiran->getClientOriginalName();
        		$opening->lampiran = $path_lampiran;
            	$opening->dms_update = 1;
            }
            if($hadir = $request->hadir){
        		$path_hadir = $hadir->store('uploads/daftarhadir', 'public');
        		$opening->hadirname = $hadir->getClientOriginalName();
        		$opening->hadir = $path_hadir;
            	$opening->dms_hadir_update = 1;
            }

            $programaudit = ProgramAudit::find($request->program_id);
    		$programaudit->status = 5;
    		$programaudit->save();

        	$opening->save();
        	$opening->saveDetail($request->detail, $request->details);
        	$opening->multipleFileUpload($request->foto);

        	$user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Kegiatan Audit Pelaksanaan';
		    $log->sub = 'Opening Meeting';
		    $log->aktivitas = 'Membuat Opening Meeting Tahun '.$opening->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($opening->program->penugasanaudit->rencanadetail->tipe_object, $opening->program->penugasanaudit->rencanadetail->object_id);
		    $log->user_id = $user->id;
		    $log->save();

		    $update_notif = Notification::where('parent_id', $opening->program->id)->where('modul', 'program-audit')->where('stage', 1)->get();
	        foreach ($update_notif as $key => $vil) {
	        	$data_notif = Notification::find($vil->id);
		        $data_notif->status =0;
		        $data_notif->save();
	        }

			$notif = new Notification;
		    $notif->user_id = $opening->program->user_id;
		    $notif->parent_id = $opening->id;
		    $notif->url = route($this->routes.'.index');
		    $notif->modul = 'opening-meeting';
		    $notif->stage = 0;
		    $notif->keterangan = 'Opening Meeting Tahun '.$opening->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($opening->program->penugasanaudit->rencanadetail->tipe_object, $opening->program->penugasanaudit->rencanadetail->object_id);
		    $notif->status = 1;
		    $notif->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function update(OpeningMeetingRequest $request)
    {
    	DB::beginTransaction();
    	try {
        	$opening 				= OpeningMeeting::find($request->id);
        	$opening->tanggal 		= $request->tanggal;
	        $opening->tempat  		= $request->tempat;
        	$opening->mulai  		= $request->mulai;
        	$opening->selesai  		= $request->selesai;
        	if($request->status == 0){
    			$opening->status 		 	= 1;
    		}else{
    			$opening->status 		 	= 2;
    		}
        	if($lampiran = $request->lampiran){
        		$path_lampiran = $lampiran->store('uploads/lampiran', 'public');
        		$opening->lampiranname = $lampiran->getClientOriginalName();
            $opening->lampiran = $path_lampiran;
        		$opening->dms_update = 1;
            }
            if($hadir = $request->hadir){
        		$path_hadir = $hadir->store('uploads/daftarhadir', 'public');
        		$opening->hadirname = $hadir->getClientOriginalName();
            $opening->dms_hadir_update = 1;
        		$opening->hadir = $path_hadir;
            }
        	$opening->save();
        	$hapus_anggota 		= OpeningMeetingDetail::where('opening_id', $request->id)
	                        							->delete();
        	$opening->saveDetail($request->detail, $request->details);
        	$opening->multipleFileUpload($request->foto);

        	$user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Kegiatan Audit Pelaksanaan';
		    $log->sub = 'Opening Meeting';
		    $log->aktivitas = 'Mengubah Opening Meeting Tahun '.$opening->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($opening->program->penugasanaudit->rencanadetail->tipe_object, $opening->program->penugasanaudit->rencanadetail->object_id);
		    $log->user_id = $user->id;
		    $log->save();

		    if($request->status ==1){
		    	$update_notif = Notification::where('parent_id', $opening->id)->where('modul', 'opening-meeting')->where('stage', 0)->get();
		        foreach ($update_notif as $key => $vil) {
		        	$data_notif = Notification::find($vil->id);
			        $data_notif->status =0;
			        $data_notif->save();
		        }

				$notif = new Notification;
			    $notif->user_id = $opening->program->user_id;
			    $notif->parent_id = $opening->id;
			    $notif->url = route($this->routes.'.index');
			    $notif->modul = 'opening-meeting';
			    $notif->stage = 1;
			    $notif->keterangan = 'Closing Meeting Tahun '.$opening->program->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($opening->program->penugasanaudit->rencanadetail->tipe_object, $opening->program->penugasanaudit->rencanadetail->object_id);
			    $notif->status = 1;
			    $notif->save();
		    }

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function detil(TinjauanDokumen $id)
    {
    	$cari = ProgramAudit::where('tinjauan_id', $id->id)->first();
    	$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Pelaksanaan' => '#', 'Opening Meeting' => '#', 'Detil' => '#']);
        return $this->render('modules.audit.pelaksanaan.opening-meeting.detil', [
        	'program' => $cari,
        	'record' => $id,
        ]);
    }

    public function buat(ProgramAudit $id)
    {
    	$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Pelaksanaan' => '#', 'Opening Meeting' => '#', 'Buat' => '#']);
    	$penugasan = PenugasanAudit::where('id', $id->penugasanaudit->id)->get();
    	foreach ($penugasan as $key => $value) {
    		$kategori = $value->rencanadetail->tipe_object;
    		$object = $value->rencanadetail->object_id;
    		$user = object_user($kategori, $object);
    		$user_id[] =$user[0];
    	}
    	$unique = array();
		foreach ($user_id as $piece) {
		    $unique = array_merge($unique, $piece);
		}
		$unique = array_unique($unique);
        return $this->render('modules.audit.pelaksanaan.opening-meeting.create', [
        	'record' => $id,
        	'user_id' => $unique
        ]);
    }

    public function ubah(OpeningMeeting $id)
    {
    	$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Pelaksanaan' => '#', 'Opening Meeting' => '#', 'Ubah' => '#']);
        return $this->render('modules.audit.pelaksanaan.opening-meeting.edit', [
        	'record' => $id,
        ]);
    }

    public function detilOpening(OpeningMeeting $id)
    {
    	$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Pelaksanaan' => '#', 'Opening Meeting' => '#', 'Detil' => '#']);
        return $this->render('modules.audit.pelaksanaan.opening-meeting.detil-opening', [
        	'record' => $id,
        ]);
    }

    public function detilProject(RencanaAuditDetail $id)
    {
        return $this->render('modules.audit.pelaksanaan.opening-meeting.detil-project', ['record' => $id]);
    }

    public function detilPenugasan(PenugasanAudit $id)
    {
        return $this->render('modules.audit.pelaksanaan.opening-meeting.detil-penugasan', ['record' => $id]);
    }

    public function detilTinjauan(TinjauanDokumen $id)
    {
    	if($id->group > 0){
	    	$cari = TinjauanDokumen::where('group', $id->group)->get();
    	}else{
    		$cari[]= $id;
    	}
        return $this->render('modules.audit.pelaksanaan.opening-meeting.detil-tinjauan', [
        	'record' => $id,
        	'cek' => $cari,
        ]);
    }

    public function detilProgram(ProgramAudit $id)
    {
        return $this->render('modules.audit.pelaksanaan.opening-meeting.detil-program', ['record' => $id]);
    }

    public function destroy(ProgramAudit $program)
    {
        $program->delete();
        return response()->json([
            'success' => true
        ]);
    }

    public function cetakAudit(Request $request, $id){
    	$record = RencanaAudit::find($id);
        $data = [
        	'records' => $record,
        ];
	    $pdf = PDF::loadView('modules.audit.pelaksanaan.opening-meeting.cetak-audit', $data)->setPaper('a4', 'landscape');
        return $pdf->stream('Rencana Audit.pdf',array("Attachment" => false));
 	}

 	public function cetakPenugasan(Request $request, $id){
    	$record = PenugasanAudit::find($id);
        $data = [
        	'records' => $record,
        ];

        $merge = new Merger();

	    $pdf = PDF::loadView('modules.audit.pelaksanaan.opening-meeting.cetak-penugasan', $data);
	    $merge->addRaw($pdf->output());
	    $pdf2 = PDF::loadView('modules.audit.pelaksanaan.opening-meeting.cetak-jadwal', $data);
	    $pdf2->setPaper('a4', 'landscape');
	    $merge->addRaw($pdf2->output());

        return new Response($merge->merge(), 200, array(
            'Content-Type' => 'application/pdf',
            'Content-Disposition' =>  'inline; filename="Penugasan Audit.pdf"',
        ));
    }

    public function downloadAudit($id)
    {
    	$daftar = RencanaAudit::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return abort(404);
    }

    public function downloadPenugasan($id)
    {
    	$daftar = PenugasanAudit::find($id);
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}
    	return abort(404);
    }

    public function downloadProgramAudit($id)
    {
    	$daftar = ProgramAudit::find($id);
    	if(file_exists(public_path('storage/'.$daftar->lampiran)))
    	{
    		return response()->download(public_path('storage/'.$daftar->lampiran), $daftar->lampiranname);
    	}
    	return abort(404);
    }
}
