<?php

namespace App\Http\Controllers\KegiatanAudit\Persiapan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;
use Yajra\DataTables\Facades\DataTables;
use iio\libmergepdf\Merger;

use App\Http\Requests\KegiatanAudit\Persiapan\PenugasanAudit\PenugasanAuditRequest;
use App\Http\Requests\KegiatanAudit\Persiapan\PenugasanAudit\PenugasanAuditFinalRequest;
use App\Http\Requests\KegiatanAudit\Persiapan\PenugasanAudit\RejectRequest;

use App\Models\RencanaAudit\Pkat;
use App\Models\KegiatanAudit\Rencana\RencanaAudit;
use App\Models\RencanaAudit\Program;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAuditDetailAnggota;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAuditDetailTembusan;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAuditDetailJadwal;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAuditDetailJadwalDetail;
use App\Models\Auths\User;
use App\Models\Files;
use App\Models\SessionLog;
use App\Models\Notification;
use App\Exports\GenerateExport;
use GuzzleHttp\Client;

use DB;
use PDF;
use Excel;
use Storage;

class PenugasanAuditController extends Controller
{
    protected $routes = 'kegiatan-audit.persiapan.penugasan';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Kegiatan Audit' => '#', 'Persiapan' => '#', 'Surat Penugasan' => '#']);
        $this->listStructs = [
        	'listStruct' => [
		        	[
		                'data' => 'num',
		                'name' => 'num',
		                'label' => '#',
		                'orderable' => false,
		                'searchable' => false,
		                'className' => 'text-center',
		                'width' => '20px',
		            ],
		            [
		                'data' => 'tahun',
		                'name' => 'tahun',
		                'label' => 'Tahun',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'rencana',
		                'name' => 'rencana',
		                'label' => 'Rencana Pelaksanaan',
		                // 'className' => 'text-center',
		                'width' => '300px',
		            ],
		            [
		                'data' => 'tipe',
		                'name' => 'tipe',
		                'label' => 'Tipe',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'kategori',
		                'name' => 'kategori',
		                'label' => 'Kategori',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'object_audit',
		                'name' => 'object_audit',
		                'label' => 'Objek Audit',
		                'width' => '250px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'no_ab',
		                'name' => 'no_ab',
		                'label' => 'No AB',
		                // 'className' => 'text-center',
		                'width' => '150px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'detil',
		                'name' => 'detil',
		                'label' => 'Rencana Kerja',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'status',
		                'name' => 'status',
		                'label' => 'Status',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '100px',
		                'className' => 'text-center'
		            ],
              //   	[
		            //     'data' => 'action',
		            //     'name' => 'action',
		            //     'label' => 'Aksi',
		            //     'searchable' => false,
		            //     'sortable' => false,
		            //     'width' => '80px',
		            //     'className' => 'text-center'
		            // ],
		            [
	                    'data' => 'checklist',
	                    'name' => 'checklist',
	                    'label' => 'Checklist',
	                    'searchable' => false,
	                    'sortable' => false,
	                    'className' => "text-center",
	                    'width' => '80px',
                	],
        	],
        	'listStruct2' => [
		        	[
		                'data' => 'num',
		                'name' => 'num',
		                'label' => '#',
		                'orderable' => false,
		                'searchable' => false,
		                'className' => 'text-center',
		                'width' => '20px',
		            ],
		            [
		                'data' => 'tahun',
		                'name' => 'tahun',
		                'label' => 'Tahun',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'rencana',
		                'name' => 'rencana',
		                'label' => 'Rencana Pelaksanaan',
		                // 'className' => 'text-center',
		                'width' => '300px',
		            ],
		            [
		                'data' => 'tipe',
		                'name' => 'tipe',
		                'label' => 'Tipe',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'kategori',
		                'name' => 'kategori',
		                'label' => 'Kategori',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'object_audit',
		                'name' => 'object_audit',
		                'label' => 'Objek Audit',
		                'width' => '250px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'no_ab',
		                'name' => 'no_ab',
		                'label' => 'No AB',
		                // 'className' => 'text-center',
		                'width' => '150px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'detil',
		                'name' => 'detil',
		                'label' => 'Rencana Kerja',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'status',
		                'name' => 'status',
		                'label' => 'Status',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '100px',
		                'className' => 'text-center'
		            ],
                	[
		                'data' => 'action',
		                'name' => 'action',
		                'label' => 'Aksi',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '80px',
		                'className' => 'text-center'
		            ],
        	],
        	'listStruct3' => [
		        	[
		                'data' => 'num',
		                'name' => 'num',
		                'label' => '#',
		                'orderable' => false,
		                'searchable' => false,
		                'className' => 'text-center',
		                'width' => '20px',
		            ],
		            [
		                'data' => 'tahun',
		                'name' => 'tahun',
		                'label' => 'Tahun',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'rencana',
		                'name' => 'rencana',
		                'label' => 'Rencana Pelaksanaan',
		                // 'className' => 'text-center',
		                'width' => '300px',
		            ],
		            [
		                'data' => 'tipe',
		                'name' => 'tipe',
		                'label' => 'Tipe',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'kategori',
		                'name' => 'kategori',
		                'label' => 'Kategori',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'object_audit',
		                'name' => 'object_audit',
		                'label' => 'Objek Audit',
		                'width' => '250px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'no_ab',
		                'name' => 'no_ab',
		                'label' => 'No AB',
		                // 'className' => 'text-left',
		                'width' => '150px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'detil',
		                'name' => 'detil',
		                'label' => 'Rencana Kerja',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'detil_penugasan',
		                'name' => 'detil_penugasan',
		                'label' => 'Surat Penugasan',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'status',
		                'name' => 'status',
		                'label' => 'Status',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '100px',
		                'className' => 'text-center'
		            ],
		            [
		                'data' => 'action',
		                'name' => 'action',
		                'label' => 'Aksi',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '80px',
		                'className' => 'text-center'
		            ],
        	],
        ];
    }

    public function grid()
    {
    	$user = auth()->user();
    	if($user->hasJabatan(['SVP - Internal Audit','President Director','Secretary','auditor'])){
	        $records = RencanaAuditDetail::whereHas('rencanaaudit', function($u){
	        							   		$u->whereNotNull('bukti');
	        							   })
	        							   ->where('status_penugasan', 0)
	        							   ->where('flag', 1)
	        							   ->where('tipe', 0)
	        							   ->where('keterangan', 1)
	        							   ->select('*');
	     	if(!isset(request()->order[0]['column'])) {
	              $records->orderBy('created_at','desc');
	        }
	        if ($tahun = request()->tahun) {
	        	$records->whereHas('rencanaaudit', function($u) use ($tahun) {
	        		$u->where('tahun', 'like', '%' . $tahun . '%');
	        	});
	        }
	        if($tipe = request()->tipe) {
        		if($tipe == 1){
	        		$records->where('tipe', 0);
        		}elseif($tipe == 2){
        			$records->where('tipe', 1);
        		}else{
        			$records->where('tipe', 2);
        		}
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
		        	$records->where('tipe_object', 0);
	        	}elseif($kategori == 2){
	        		$records->where('tipe_object', 1);
	        	}elseif($kategori == 3){
	        		$records->where('tipe_object', 2);
	        	}elseif($kategori == 4){
	        		$records->where('tipe_object', 3);
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->where('tipe_object', $kat)->where('object_id', $object_id);
	        }
       	}else{
       		$records = RencanaAuditDetail::where('id', 0);
       	}

        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('tahun', function ($record) {
                   return $record->rencanaaudit->tahun;
               })
               ->addColumn('tipe', function ($record) {
                    $tipe = '';
	                if($record->tipe == 0){
	               	  $tipe = '<span class="label label-primary">Audit</span>';
	                }elseif($record->tipe == 1){
	               	  $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
	                }else{
	                  $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
	                }

	                return $tipe;
               })
               ->addColumn('kategori', function ($record) {
                    $kategori = '';
                    if($record->tipe == 0){
		                if($record->tipe_object == 0){
		               	  $kategori = '<span class="label label-success">Business Unit (BU)</span>';
		                }elseif($record->tipe_object == 1){
		               	  $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
		                }elseif($record->tipe_object == 2){
		               	  $kategori = '<span class="label label-warning">Project</span>';
		                }else{
		               	  $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
		                }
	                }elseif($record->tipe == 1){
	               	  	$kategori = $record->konsultasi->nama;
	                }else{
	                    $kategori = $record->lain->nama;
	                }
	                return $kategori;
               })
               ->addColumn('object_audit', function ($record) {
                   return object_audit($record->tipe_object, $record->object_id);
               })
               ->addColumn('rencana', function ($record) {
                   return $record->rencana;
               })
               ->addColumn('nama', function ($record) {
               		if($record->tipe_object == 2){
	               		return object_audit($record->tipe_object, $record->object_id);
	               	}else{
	               		return '';
	               	}
               })
               ->addColumn('detil', function ($record) use ($user){
	               $buttons = '';
	               if($user->hasJabatan(['SVP - Internal Audit','President Director','auditor'])){
		               $buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
		                    'tooltip' 	=> 'Rencana Kerja',
		                    'class' 	=> 'download-rkia button',
		                    'id'   		=> $record->rencanaaudit->id,
		               ]);
		           }
	               
	               return $buttons;
	           })
	           ->addColumn('no_ab', function ($record) {
	               return getAb($record->tipe_object, $record->object_id);
	           })
               ->addColumn('nilai', function ($record) {
               		return number_format($record->nilai, 0, '.', '.');
           	   })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->editColumn('created_by', function ($record) {
                   return $record->creatorName();
               })
               ->editColumn('status', function ($record) {
               		if($record->penugasan){
               			if($record->penugasan->ket_svp){
	               			return '<span class="label label-warning">Ditolak</span>';
               			}else{
	               			return '<span class="label label-default">Draft</span>';
               			}
               		}else{
		               return '<span class="label label-default">Draft</span>';
               		}
	           })
               ->editColumn('checklist', function($record) use ($user){
               		if($user->hasRole(['auditor'])){
		               	if($record->penugasan){
		               		$checklist = $this->makeButton([
			           			'type' 		=> 'edit',
			           			'label' 	=> '<i class="fa fa-edit text-primary"></i>',
			           			'tooltip' 	=> 'Ubah Program',
			           			'class' 	=> 'ubah button',
			           			'id'   		=> $record->penugasan->id,
			           		]);
		               	}else{
			                $checklist = '<input type="checkbox" class="view check" name="check[]" data-id="'.$record->id.'" value="'.$record->id.'" data-toggle="toggle" data-size="mini" data-on="Ya" data-off="Tidak" data-style="ios">';
		               	}
		            }else{
		            	$checklist = '';
		            }
	                return $checklist;
	            })
               ->rawColumns(['tipe', 'action','kategori','checklist','detil','status','object_audit','nama'])
               ->make(true);
    }

    public function onProgress()
    {
    	$user = auth()->user();
    	if($user->hasJabatan(['SVP - Internal Audit','President Director','Secretary','auditor'])){
    		$records = PenugasanAudit::where('status', '!=', 0)
    								  ->where('status', '!=', 4)
    								  ->select('*');
  			if(!isset(request()->order[0]['column'])) {
	              $records->orderBy('created_at','desc');
	        }
	        if ($tahun = request()->tahun) {
	        	$records->whereHas('rencanadetail', function($a) use ($tahun) {
		        	$a->whereHas('rencanaaudit', function($u) use ($tahun) {
		        		$u->where('tahun', 'like', '%' . $tahun . '%');
		        	});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('rencanadetail', function($a) use ($tipe) {
		        	if($tipe == 1){
		        		$a->where('tipe', 0);
	        		}elseif($tipe == 2){
	        			$a->where('tipe', 1);
	        		}else{
	        			$a->where('tipe', 2);
	        		}
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('rencanadetail', function($a){
			        	$a->where('tipe_object', 0);
		        	});
	        	}elseif($kategori == 2){
	        		$records->whereHas('rencanadetail', function($a){
			        	$a->where('tipe_object', 1);
		        	});
	        	}elseif($kategori == 3){
	        		$records->whereHas('rencanadetail', function($a){
			        	$a->where('tipe_object', 2);
		        	});
	        	}elseif($kategori == 4){
	        		$records->whereHas('rencanadetail', function($a){
			        	$a->where('tipe_object', 3);
		        	});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->whereHas('rencanadetail', function($a) use ($kat, $object_id){
		        	$a->where('tipe_object', $kat)->where('object_id', $object_id);
	        	});
	        }
       	}else{
       		$records = PenugasanAudit::whereHas('tembusans', function($u) use ($user){
       									$u->where('user_id', $user->id);
       								  })
       								  ->where('status', '!=', 0)
    								  ->where('status', '!=', 4)
    								  ->select('*');
       	}
        return DataTables::of($records->get())
           ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('tahun', function ($record) {
                   return $record->rencanadetail->rencanaaudit->tahun;
               })
               ->addColumn('tipe', function ($record) {
                    $tipe = '';
	                if($record->rencanadetail->tipe == 0){
	               	  $tipe = '<span class="label label-primary">Audit</span>';
	                }elseif($record->rencanadetail->tipe == 1){
	               	  $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
	                }else{
	                  $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
	                }

	                return $tipe;
               })
               ->addColumn('kategori', function ($record) {
                    $kategori = '';
                    if($record->rencanadetail->tipe == 0){
		                if($record->rencanadetail->tipe_object == 0){
		               	  $kategori = '<span class="label label-success">Business Unit (BU)</span>';
		                }elseif($record->rencanadetail->tipe_object == 1){
		               	  $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
		                }elseif($record->rencanadetail->tipe_object == 2){
		               	  $kategori = '<span class="label label-warning">Project</span>';
		                }else{
		               	  $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
		                }
	                }elseif($record->rencanadetail->tipe == 1){
	               	  	$kategori = $record->rencanadetail->konsultasi->nama;
	                }else{
	                    $kategori = $record->rencanadetail->lain->nama;
	                }
	                return $kategori;
               })
               ->addColumn('no_ab', function ($record) {
	               return getAb($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
	           })
               ->addColumn('object_audit', function ($record) {
                   return object_audit($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
               })
               ->addColumn('rencana', function ($record) {
                   return $record->rencanadetail->rencana;
               })
               ->editColumn('status', function ($record) {
	           		if($record->status == 1){
	           			if($record->ket_svp){
	           				return '<span class="label label-warning">Ditolak</span>';
	           			}else{
			               	return '<span class="label label-info">Waiting Approval SVP</span>';
	           			}
	           		}elseif($record->status == 2){
	           			return '<span class="label label-info">Waiting Approval Dirut</span>';
	           		}elseif($record->status == 3){
	           			return '<span class="label label-info">Waiting Upload</span>';
	           		}else{
	           			return '<span class="label label-info">Historis</span>';
	           		}
	           })
               ->addColumn('nama', function ($record) {
               		if($record->rencanadetail->tipe_object == 2){
	               		return object_audit($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
	               	}else{
	               		return '';
	               	}
               })
               ->addColumn('detil', function ($record) use ($user){
	               $buttons = '';
	               if($user->hasJabatan(['SVP - Internal Audit','President Director','auditor'])){
		               $buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
		                    'tooltip' 	=> 'Rencana Kerja',
		                    'class' 	=> 'download-rkia button',
		                    'id'   		=> $record->rencanadetail->rencanaaudit->id,
		               ]);
		           }
	               return $buttons;
	           })
               ->addColumn('nilai', function ($record) {
               		return number_format($record->nilai, 0, '.', '.');
           	   })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->editColumn('created_by', function ($record) {
                   return $record->creatorName();
               })
               ->editColumn('checklist', function($record){
	               	if($record->penugasan){
		                $checklist = '<input type="checkbox" class="view check" name="check[]" data-id="'.$record->id.'" value="'.$record->id.'" data-toggle="toggle" data-size="mini" data-on="Ya" data-off="Tidak" data-style="ios">';
	               	}else{
	               		$checklist = '';
	               	}
	                return $checklist;
	            })
                ->editColumn('action', function($record) use ($user){
	               	$buttons = '';
	                if($record->status == 1){
	                	if($user->hasJabatan(['SVP - Internal Audit'])){
		                	$buttons .= $this->makeButton([
		               			'type' 		=> 'edit',
		               			'label' 	=> '<i class="fa fa-check text-primary"></i>',
		               			'tooltip' 	=> 'Buat Approval',
		               			'class' 	=> 'approvesvpdetil button',
		               			'id'   		=> $record->id,
		               		]);
		                }elseif($user->hasRole(['auditor'])){
		                	$buttons .= $this->makeButton([
			           			'type' 		=> 'edit',
			           			'label' 	=> '<i class="fa fa-print text-primary"></i>',
			           			'tooltip' 	=> 'Cetak',
			           			'class' 	=> 'cetak button',
			           			'id'   		=> $record->id,
			           		]);
		                }else{
		                	$buttons = '';
		                }
	                }elseif($record->status == 2){
	                	if($record->group > 0){
	                		$cek = PenugasanAudit::where('group', $record->group)->where('status', '<', 2)->get()->count();
		                	if($user->hasJabatan(['President Director'])){
		                		if($cek > 0){
		                			$buttons .= $this->makeButton([
				               			'type' 		=> 'edit',
				               			'label' 	=> '<i class="fa fa-check text-primary"></i>',
				               			'tooltip' 	=> 'Buat Approval',
				               			'class' 	=> 'approvedirutdetail button disabled',
				               			'id'   		=> $record->id,
				               		]);
				               		$buttons .='&nbsp;&nbsp;&nbsp;&nbsp;';
				               		$buttons .= $this->makeButton([
					           			'type' 		=> 'edit',
					           			'label' 	=> '<i class="fa fa-print text-primary"></i>',
					           			'tooltip' 	=> 'Cetak',
					           			'class' 	=> 'cetak button',
					           			'id'   		=> $record->id,
					           		]);
		                		}else{
				                	$buttons .= $this->makeButton([
				               			'type' 		=> 'edit',
				               			'label' 	=> '<i class="fa fa-check text-primary"></i>',
				               			'tooltip' 	=> 'Buat Approval',
				               			'class' 	=> 'approvedirutdetail button',
				               			'id'   		=> $record->id,
				               		]);
				               		$buttons .='&nbsp;&nbsp;&nbsp;&nbsp;';
				               		$buttons .= $this->makeButton([
					           			'type' 		=> 'edit',
					           			'label' 	=> '<i class="fa fa-print text-primary"></i>',
					           			'tooltip' 	=> 'Cetak',
					           			'class' 	=> 'cetak button',
					           			'id'   		=> $record->id,
					           		]);
		                		}
			                }elseif($user->hasRole(['svp-audit']) || $user->hasRole(['auditor'])){
			                	$buttons .= $this->makeButton([
				           			'type' 		=> 'edit',
				           			'label' 	=> '<i class="fa fa-print text-primary"></i>',
				           			'tooltip' 	=> 'Cetak',
				           			'class' 	=> 'cetak button',
				           			'id'   		=> $record->id,
				           		]);
			                }else{
			                	$buttons = '';
			                }
	                	}else{
	                		if($user->hasJabatan(['President Director'])){
	                			$buttons .= $this->makeButton([
			               			'type' 		=> 'edit',
			               			'label' 	=> '<i class="fa fa-check text-primary"></i>',
			               			'tooltip' 	=> 'Buat Approval',
			               			'class' 	=> 'approvedirutdetail button',
			               			'id'   		=> $record->id,
			               		]);
			               		$buttons .='&nbsp;&nbsp;&nbsp;&nbsp;';
			               		$buttons .= $this->makeButton([
				           			'type' 		=> 'edit',
				           			'label' 	=> '<i class="fa fa-print text-primary"></i>',
				           			'tooltip' 	=> 'Cetak',
				           			'class' 	=> 'cetak button',
				           			'id'   		=> $record->id,
				           		]);
	                		}elseif($user->hasJabatan(['SVP - Internal Audit']) || $user->hasJabatan(['auditor'])){
	                			$buttons .= $this->makeButton([
				           			'type' 		=> 'edit',
				           			'label' 	=> '<i class="fa fa-print text-primary"></i>',
				           			'tooltip' 	=> 'Cetak',
				           			'class' 	=> 'cetak button',
				           			'id'   		=> $record->id,
				           		]);
	                		}else{
			                	$buttons = '';
	                		}
		                }
	                }elseif($record->status == 3){
	                	if($record->group > 0){
	                		$cek = PenugasanAudit::where('group', $record->group)->where('status', '<', 3)->get()->count();
		                	if($user->hasJabatan(['Secretary'])){
		                		if($cek > 0){
		                			$buttons .= $this->makeButton([
				               			'type' 		=> 'edit',
				               			'label' 	=> '<i class="fa fa-upload text-primary"></i>',
				               			'tooltip' 	=> 'Upload Dokumen',
				               			'class' 	=> 'upload-dokumen button disabled',
				               			'id'   		=> $record->id,
				               		]);
		                		}else{
				                	$buttons .= $this->makeButton([
				               			'type' 		=> 'edit',
				               			'label' 	=> '<i class="fa fa-upload text-primary"></i>',
				               			'tooltip' 	=> 'Upload Dokumen',
				               			'class' 	=> 'upload-dokumen button',
				               			'id'   		=> $record->id,
				               		]);
		                		}
			                }elseif($user->hasJabatan(['President Director']) || $user->hasJabatan(['SVP - Internal Audit'])){
			                	$buttons .= $this->makeButton([
				           			'type' 		=> 'edit',
				           			'label' 	=> '<i class="fa fa-print text-primary"></i>',
				           			'tooltip' 	=> 'Cetak',
				           			'class' 	=> 'cetak button',
				           			'id'   		=> $record->id,
				           		]);
			                }else{
				                $buttons = '';
			                }
	                	}else{
		                	if($user->hasJabatan(['Secretary'])){
			                	$buttons .= $this->makeButton([
			               			'type' 		=> 'edit',
			               			'label' 	=> '<i class="fa fa-upload text-primary"></i>',
			               			'tooltip' 	=> 'Upload Dokumen',
			               			'class' 	=> 'upload-dokumen button',
			               			'id'   		=> $record->id,
			               		]);
			                }elseif($user->hasJabatan(['President Director']) || $user->hasJabatan(['SVP - Internal Audit']) || $user->hasJabatan(['auditor'])){
			                	$buttons .= $this->makeButton([
				           			'type' 		=> 'edit',
				           			'label' 	=> '<i class="fa fa-print text-primary"></i>',
				           			'tooltip' 	=> 'Cetak',
				           			'class' 	=> 'cetak button',
				           			'id'   		=> $record->id,
				           		]);
			                }else{
				                $buttons = '';
			                }
		                }
	                }else{
	                	$buttons = '';
	                }
	                return $buttons;
	            })
               ->rawColumns(['object_audit','tipe', 'action','kategori','checklist','detil','status','object_audit','nama'])
               ->make(true);
    }

    public function historis()
    {
        $user = auth()->user();
    	if($user->hasJabatan(['SVP - Internal Audit','President Director','Secretary','auditor'])){
    		$records = PenugasanAudit::whereIn('status', [4,5,6])
    								  ->select('*');
    		if(!isset(request()->order[0]['column'])) {
	              $records->orderBy('created_at','desc');
	        }
	        if ($tahun = request()->tahun) {
	        	$records->whereHas('rencanadetail', function($a) use ($tahun) {
		        	$a->whereHas('rencanaaudit', function($u) use ($tahun) {
		        		$u->where('tahun', 'like', '%' . $tahun . '%');
		        	});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('rencanadetail', function($a) use ($tipe) {
		        	if($tipe == 1){
		        		$a->where('tipe', 0);
	        		}elseif($tipe == 2){
	        			$a->where('tipe', 1);
	        		}else{
	        			$a->where('tipe', 2);
	        		}
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('rencanadetail', function($a){
			        	$a->where('tipe_object', 0);
		        	});
	        	}elseif($kategori == 2){
	        		$records->whereHas('rencanadetail', function($a){
			        	$a->where('tipe_object', 1);
		        	});
	        	}elseif($kategori == 3){
	        		$records->whereHas('rencanadetail', function($a){
			        	$a->where('tipe_object', 2);
		        	});
	        	}elseif($kategori == 4){
	        		$records->whereHas('rencanadetail', function($a){
			        	$a->where('tipe_object', 3);
		        	});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->whereHas('rencanadetail', function($a) use ($kat, $object_id){
		        	$a->where('tipe_object', $kat)->where('object_id', $object_id);
	        	});
	        }
       	}else{
       		$records = PenugasanAudit::whereHas('tembusans', function($u) use ($user){
       									$u->where('user_id', $user->id);
       								  })
       								  ->whereIn('status', [4,5,6])
    								  ->select('*');
       		$records = PenugasanAudit::where('id', 0);
       	}
        return DataTables::of($records->get())
           	   ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('tahun', function ($record) {
                   return $record->rencanadetail->rencanaaudit->tahun;
               })
               ->addColumn('tipe', function ($record) {
                    $tipe = '';
	                if($record->rencanadetail->tipe == 0){
	               	  $tipe = '<span class="label label-primary">Audit</span>';
	                }elseif($record->rencanadetail->tipe == 1){
	               	  $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
	                }else{
	                  $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
	                }

	                return $tipe;
               })
               ->addColumn('kategori', function ($record) {
                    $kategori = '';
                    if($record->rencanadetail->tipe == 0){
		                if($record->rencanadetail->tipe_object == 0){
		               	  $kategori = '<span class="label label-success">Business Unit (BU)</span>';
		                }elseif($record->rencanadetail->tipe_object == 1){
		               	  $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
		                }elseif($record->rencanadetail->tipe_object == 2){
		               	  $kategori = '<span class="label label-warning">Project</span>';
		                }else{
		               	  $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
		                }
	                }elseif($record->rencanadetail->tipe == 1){
	               	  	$kategori = $record->rencanadetail->konsultasi->nama;
	                }else{
	                    $kategori = $record->rencanadetail->lain->nama;
	                }
	                return $kategori;
               })
               ->addColumn('no_ab', function ($record) {
	               return getAb($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
	           })
               ->addColumn('object_audit', function ($record) {
                   return object_audit($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
               })
               ->addColumn('rencana', function ($record) {
                   return $record->rencanadetail->rencana;
               })
               ->addColumn('nama', function ($record) {
               		if($record->rencanadetail->tipe_object == 2){
	               		return object_audit($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
	               	}else{
	               		return '';
	               	}
               })
               ->addColumn('detil', function ($record) use ($user){
	               $buttons = '';
	               if($user->hasJabatan(['SVP - Internal Audit','President Director','auditor'])){
		               $buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
		                    'tooltip' 	=> 'Rencana Kerja',
		                    'class' 	=> 'download-rkia button',
		                    'id'   		=> $record->rencanadetail->rencanaaudit->id,
		               ]);
	               }
	               return $buttons;
	           })
	           ->addColumn('detil_penugasan', function ($record) {
	               $buttons = '';
	               $buttons .= $this->makeButton([
	           			'type' 		=> 'edit',
	           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
	           			'tooltip' 	=> 'Detil Penugasan Audit',
	           			'class' 	=> 'detil-penugasan button',
	           			'id'   		=> $record->id,
	           		]);
	               return $buttons;
	           })
	           ->editColumn('status', function ($record) {
	               return '<span class="label label-success">Completed</span>';
	           })
               ->addColumn('nilai', function ($record) {
               		return number_format($record->nilai, 0, '.', '.');
           	   })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->editColumn('created_by', function ($record) {
                   return $record->creatorName();
               })
               ->editColumn('action', function($record){
               	    $checklist = '';
	                $checklist .= $this->makeButton([
	           			'type' 		=> 'edit',
	           			'label' 	=> '<i class="fa fa-print text-primary"></i>',
	           			'tooltip' 	=> 'Cetak',
	           			'class' 	=> 'cetak button',
	           			'id'   		=> $record->id,
	           		]);
	                $checklist .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	           		$checklist .= $this->makeButton([
	           			'type' 		=> 'edit',
	           			'label' 	=> '<i class="fa fa-file-excel-o text-primary"></i>',
	           			'tooltip' 	=> 'Jadwal',
	           			'class' 	=> 'cetak-excel-penugasan button',
	           			// 'class' 	=> 'cetak-excel button',
	           			'id'   		=> $record->id,
	           		]);
	                return $checklist;
	            })
               ->rawColumns(['object_audit','tipe','kategori','detil','status','detil_penugasan','object_audit','nama','action'])
               ->make(true);
    }

    public function index()
    {
    	$user = auth()->user();
    	if($user->hasJabatan(['SVP - Internal Audit','President Director','Secretary','auditor'])){
        	$nbaru = RencanaAuditDetail::whereHas('rencanaaudit', function($u){
	        							   		$u->whereNotNull('bukti');
	        							   })
	        							   ->where('status_penugasan', 0)
	        							   ->where('flag', 1)
	        							   ->where('tipe', 0)
		        	  					   ->get()->count();

        	$nprog = PenugasanAudit::where('status', '!=', 0)
    								  ->where('status', '!=', 4)
		        	  				  ->get()->count();
       	}else{
       		$nbaru = RencanaAuditDetail::where('id', 0)->get()->count();
       		$nprog = PenugasanAudit::whereHas('tembusans', function($u) use ($user){
       									$u->where('user_id', $user->id);
       								  })
       								  ->where('status', '!=', 0)
    								  ->where('status', '!=', 4)->get()->count();
       	}
        return $this->render('modules.audit.persiapan.penugasan.index', [
        	'mockup' => true,
        	'structs' => $this->listStructs,
        	'nbaru' => $nbaru,
        	'nprog' => $nprog,
        ]);
    }

    public function reject(PenugasanAudit $id)
    {
        return $this->render('modules.audit.persiapan.penugasan.reject', [
        	'record' => $id,
        ]);
    }

    public function rejectDirut(PenugasanAudit $id)
    {
        return $this->render('modules.audit.persiapan.penugasan.rejectdirut', [
        	'record' => $id,
        ]);
    }

    public function saveReject(RejectRequest $request)
    {
        DB::beginTransaction();
    	try {
    		$record 						= PenugasanAudit::find($request->id);
    		if($record->group > 0){
        		$cek = PenugasanAudit::where('group', $record->group)->get();
        		foreach ($cek as $key => $value) {
        			$data = PenugasanAudit::find($value->id);
        			$data->status 				= 0;
			    	$data->ket_svp 				= $request->ket_svp;
			        $data->save();

			        $rencana 						= RencanaAuditDetail::find($data->rencana_id);
			    	$rencana->status_penugasan 		= 0;
			        $rencana->save();

			        $user = auth()->user();
			    	$log = new SessionLog;
				    $log->modul = 'Kegiatan Audit Persiapan';
				    $log->sub = 'Surat Penugasan';
				    $log->aktivitas = 'Menolak Surat Penugasan Tahun '.$rencana->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($rencana->tipe_object, $rencana->object_id);
				    $log->user_id = $user->id;
				    $log->save();

				    $update_notif = Notification::where('parent_id', $data->id)->where('modul', 'penugasan-audit')->where('stage', 0)->get();
			        foreach ($update_notif as $key => $vil) {
			        	$data_notif = Notification::find($vil->id);
				        $data_notif->status =0;
				        $data_notif->save();
			        }

					$userz = User::whereHas('roles', function($u){
											$u->where('name', 'auditor');
										})->get();
					foreach ($userz as $key => $vul) {
						$notif = new Notification;
					    $notif->user_id = $vul->id;
					    $notif->parent_id = $data->rencanadetail->rencanaaudit->id;
					    $notif->url = route($this->routes.'.index');
					    $notif->modul = 'audit-reguler';
					    $notif->stage = 4;
					    $notif->keterangan = 'Reject SVP Penugasan Audit Tahun '.$data->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($data->rencanadetail->tipe_object, $data->rencanadetail->object_id);
					    $notif->status = 1;
					    $notif->save();
					}
        		}
        	}else{
		    	$record->status 				= 0;
		    	$record->ket_svp 				= $request->ket_svp;
		        $record->save();

		        $rencana 						= RencanaAuditDetail::find($record->rencana_id);
		    	$rencana->status_penugasan 		= 0;
		        $rencana->save();

		        $user = auth()->user();
		    	$log = new SessionLog;
			    $log->modul = 'Kegiatan Audit Persiapan';
			    $log->sub = 'Surat Penugasan';
			    $log->aktivitas = 'Menolak Surat Penugasan Tahun '.$rencana->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($rencana->tipe_object, $rencana->object_id);
			    $log->user_id = $user->id;
			    $log->save();

			    $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'penugasan-audit')->where('stage', 0)->get();
		        foreach ($update_notif as $key => $vil) {
		        	$data_notif = Notification::find($vil->id);
			        $data_notif->status =0;
			        $data_notif->save();
		        }

				$userz = User::whereHas('roles', function($u){
										$u->where('name', 'auditor');
									})->get();
				foreach ($userz as $key => $vul) {
					$notif = new Notification;
				    $notif->user_id = $vul->id;
				    $notif->parent_id = $record->rencanadetail->rencanaaudit->id;
				    $notif->url = route($this->routes.'.index');
				    $notif->modul = 'audit-reguler';
				    $notif->stage = 4;
				    $notif->keterangan = 'Reject SVP Penugasan Audit Tahun '.$record->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
				    $notif->status = 1;
				    $notif->save();
				}
        	}

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function saveRejectDirut(RejectRequest $request)
    {
        DB::beginTransaction();
    	try {
    		$record 						= PenugasanAudit::find($request->id);
    		if($record->group > 0){
        		$cek = PenugasanAudit::where('group', $record->group)->get();
        		foreach ($cek as $key => $value) {
        			$data = PenugasanAudit::find($value->id);
        			$data->status 				= 0;
			    	$data->ket_svp 				= $request->ket_svp;
			        $data->save();

			        $rencana 						= RencanaAuditDetail::find($data->rencana_id);
			    	$rencana->status_penugasan 		= 0;
			        $rencana->save();

			        $user = auth()->user();
			    	$log = new SessionLog;
				    $log->modul = 'Kegiatan Audit Persiapan';
				    $log->sub = 'Surat Penugasan';
				    $log->aktivitas = 'Menolak Surat Penugasan Tahun '.$data->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($data->rencanadetail->tipe_object, $data->rencanadetail->object_id);
				    $log->user_id = $user->id;
				    $log->save();

				    $update_notif = Notification::where('parent_id', $data->id)->where('modul', 'penugasan-audit')->where('stage', 2)->get();
			        foreach ($update_notif as $key => $vil) {
			        	$data_notif = Notification::find($vil->id);
				        $data_notif->status =0;
				        $data_notif->save();
			        }

					$userz = User::whereHas('roles', function($u){
											$u->where('name', 'svp-audit');
										})->get();
					foreach ($userz as $key => $vul) {
						$notif = new Notification;
					    $notif->user_id = $vul->id;
					    $notif->parent_id = $data->id;
					    $notif->url = route($this->routes.'.index');
					    $notif->modul = 'penugasan-audit';
					    $notif->stage = 1;
					    $notif->keterangan = 'Reject Dirut Penugasan Audit Tahun '.$data->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($data->rencanadetail->tipe_object, $data->rencanadetail->object_id);
					    $notif->status = 1;
					    $notif->save();
					}
        		}
        	}else{
		    	$record->status 				= 0;
		    	$record->ket_svp 				= $request->ket_svp;
		        $record->save();

		        $rencana 						= RencanaAuditDetail::find($record->rencana_id);
		    	$rencana->status_penugasan 		= 0;
		        $rencana->save();

		        $user = auth()->user();
		    	$log = new SessionLog;
			    $log->modul = 'Kegiatan Audit Persiapan';
			    $log->sub = 'Surat Penugasan';
			    $log->aktivitas = 'Menolak Surat Penugasan Tahun '.$record->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
			    $log->user_id = $user->id;
			    $log->save();

			    $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'penugasan-audit')->where('stage', 2)->get();
		        foreach ($update_notif as $key => $vil) {
		        	$data_notif = Notification::find($vil->id);
			        $data_notif->status =0;
			        $data_notif->save();
		        }

				$userz = User::whereHas('roles', function($u){
										$u->where('name', 'svp-audit');
									})->get();
				foreach ($userz as $key => $vul) {
					$notif = new Notification;
				    $notif->user_id = $vul->id;
				    $notif->parent_id = $record->id;
				    $notif->url = route($this->routes.'.index');
				    $notif->modul = 'penugasan-audit';
				    $notif->stage = 1;
				    $notif->keterangan = 'Reject Dirut Penugasan Audit Tahun '.$record->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
				    $notif->status = 1;
				    $notif->save();
				}
        	}
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function approve($id)
    {
    	$data_id = explode(',', $id);
    	$data = PenugasanAudit::whereIn('rencana_id', $data_id)->get();
    	return $this->render('modules.audit.persiapan.penugasan.approval', [
        	'record' => $data,
        	'id' => $data_id
        ]);
    }

    public function approveSvp(Request $request)
    {
    	DB::beginTransaction();
        try {
        	$record = PenugasanAudit::find($request->id);
        	if($record->group > 0){
        		$cek = PenugasanAudit::where('group', $record->group)->get();
        		foreach ($cek as $key => $value) {
        			$data = PenugasanAudit::find($value->id);
			        $data->status 			= 2;
			        $user = auth()->user();
			    	$log = new SessionLog;
				    $log->modul = 'Kegiatan Audit Persiapan';
				    $log->sub = 'Surat Penugasan';
				    $log->aktivitas = 'Menyetujui Surat Penugasan Tahun '.$data->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($data->rencanadetail->tipe_object, $data->rencanadetail->object_id);
				    $log->user_id = $user->id;
				    $log->save();

				    $update_notif = Notification::where('parent_id', $data->id)->where('modul', 'penugasan-audit')->where('stage', 1)->get();
			        foreach ($update_notif as $key => $vil) {
			        	$data_notif = Notification::find($vil->id);
				        $data_notif->status =0;
				        $data_notif->save();
			        }

					$userz = User::whereHas('roles', function($u){
											$u->where('name', 'sekre-dirut');
										})->get();
					foreach ($userz as $key => $vul) {
						$notif = new Notification;
					    $notif->user_id = $vul->id;
					    $notif->parent_id = $data->id;
					    $notif->url = route($this->routes.'.index');
					    $notif->modul = 'penugasan-audit';
					    $notif->stage = 3;
					    $notif->keterangan = 'Upload File Penugasan Audit Tahun '.$data->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($data->rencanadetail->tipe_object, $data->rencanadetail->object_id);
					    $notif->status = 1;
					    $notif->save();
					}
					$data->save();
        		}
        	}else{
        		$user = auth()->user();
		    	$log = new SessionLog;
			    $log->modul = 'Kegiatan Audit Persiapan';
			    $log->sub = 'Surat Penugasan';
			    $log->aktivitas = 'Menyetujui Surat Penugasan Tahun '.$record->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
			    $log->user_id = $user->id;
			    $log->save();
		        $record->status = 2;

		        $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'penugasan-audit')->where('stage', 1)->get();
		        foreach ($update_notif as $key => $vil) {
		        	$data_notif = Notification::find($vil->id);
			        $data_notif->status =0;
			        $data_notif->save();
		        }

				$userz = User::whereHas('roles', function($u){
										$u->where('name', 'dirut');
									})->get();
				foreach ($userz as $key => $vul) {
					$notif = new Notification;
				    $notif->user_id = $vul->id;
				    $notif->parent_id = $record->id;
				    $notif->url = route($this->routes.'.index');
				    $notif->modul = 'penugasan-audit';
				    $notif->stage = 2;
				    $notif->keterangan = 'Approval Penugasan Audit Tahun '.$record->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
				    $notif->status = 1;
				    $notif->save();
				}
        	}
	        $record->save();
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function approveDirut(Request $request)
    {
    	$user = auth()->user();
    	DB::beginTransaction();
        try {
        	$record = PenugasanAudit::find($request->id);
        	if($record->group > 0){
        		$cek = PenugasanAudit::where('group', $record->group)->get();
        		foreach ($cek as $key => $value) {
        			$data = PenugasanAudit::find($value->id);
        			$data->status 			= 3;
				    $user = auth()->user();
			    	$log = new SessionLog;
				    $log->modul = 'Kegiatan Audit Persiapan';
				    $log->sub = 'Surat Penugasan';
				    $log->aktivitas = 'Menyetujui Surat Penugasan Tahun '.$data->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($data->rencanadetail->tipe_object, $data->rencanadetail->object_id);
				    $log->user_id = $user->id;
				    $log->save();

			        $data->save();

			        $update_notif = Notification::where('parent_id', $data->id)->where('modul', 'penugasan-audit')->where('stage', 2)->get();
			        foreach ($update_notif as $key => $vil) {
			        	$data_notif = Notification::find($vil->id);
				        $data_notif->status =0;
				        $data_notif->save();
			        }

					$userz = User::whereHas('roles', function($u){
											$u->where('name', 'sekre-dirut');
										})->get();
					foreach ($userz as $key => $vul) {
						$notif = new Notification;
					    $notif->user_id = $vul->id;
					    $notif->parent_id = $data->id;
					    $notif->url = route($this->routes.'.index');
					    $notif->modul = 'penugasan-audit';
					    $notif->stage = 3;
					    $notif->keterangan = 'Upload File Penugasan Audit Tahun '.$data->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($data->rencanadetail->tipe_object, $data->rencanadetail->object_id);
					    $notif->status = 1;
					    $notif->save();
					}
        		}
        	}else{
			    $record->status 			= 3;
			    $user = auth()->user();
		    	$log = new SessionLog;
			    $log->modul = 'Kegiatan Audit Persiapan';
			    $log->sub = 'Surat Penugasan';
			    $log->aktivitas = 'Menyetujui Surat Penugasan Tahun '.$record->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
			    $log->user_id = $user->id;
			    $log->save();

		        $record->save();

		        $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'penugasan-audit')->where('stage', 2)->get();
		        foreach ($update_notif as $key => $vil) {
		        	$data_notif = Notification::find($vil->id);
			        $data_notif->status =0;
			        $data_notif->save();
		        }

				$userz = User::whereHas('roles', function($u){
										$u->where('name', 'sekre-dirut');
									})->get();
				foreach ($userz as $key => $vul) {
					$notif = new Notification;
				    $notif->user_id = $vul->id;
				    $notif->parent_id = $record->id;
				    $notif->url = route($this->routes.'.index');
				    $notif->modul = 'penugasan-audit';
				    $notif->stage = 3;
				    $notif->keterangan = 'Upload File Penugasan Audit Tahun '.$record->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
				    $notif->status = 1;
				    $notif->save();
				}
        	}
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function saveApproval(Request $request)
    {
    	DB::beginTransaction();
    	try {
	    	$maxValue = PenugasanAudit::max('group');
    		$record = PenugasanAudit::whereIn('rencana_id', $request->id)->get();
    		foreach ($record as $key => $value) {
    			$data = PenugasanAudit::find($value->id);
	        	$data->status 		= 1;
	        	$data->ket_svp 		= null;
	        	if(count($record) >= 2){
		        	$data->group  = ($maxValue+1);
	        	}else{
	        		if($data->group > 0){
	        		}else{
		        		$data->group  = 0;
	        		}
	        	}
		        $data->save();

		        $rencanadetail = RencanaAuditDetail::find($data->rencana_id);
		        $rencanadetail->status_penugasan =1;
		        $rencanadetail->save();

		        $user = auth()->user();
		    	$log = new SessionLog;
			    $log->modul = 'Kegiatan Audit Persiapan';
			    $log->sub = 'Surat Penugasan';
			    $log->aktivitas = 'Membuat Surat Penugasan Tahun '.$data->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($data->rencanadetail->tipe_object, $data->rencanadetail->object_id);
			    $log->user_id = $user->id;
			    $log->save();

			    $update_notif = Notification::where('parent_id', $data->rencanadetail->rencanaaudit->id)->where('modul', 'audit-reguler')->where('stage', 4)->get();
		        foreach ($update_notif as $key => $vil) {
		        	$data_notif = Notification::find($vil->id);
			        $data_notif->status =0;
			        $data_notif->save();
		        }

				$userz = User::whereHas('roles', function($u){
										$u->where('name', 'svp-audit');
									})->get();
				foreach ($userz as $key => $vul) {
					$notif = new Notification;
				    $notif->user_id = $vul->id;
				    $notif->parent_id = $value->id;
				    $notif->url = route($this->routes.'.index');
				    $notif->modul = 'penugasan-audit';
				    $notif->stage = 0;
				    $notif->keterangan = 'Approval Penugasan Audit Tahun '.$data->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($data->rencanadetail->tipe_object, $data->rencanadetail->object_id);
				    $notif->status = 1;
				    $notif->save();
				}
    		}
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function store(PenugasanAuditRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$maxValue = PenugasanAudit::max('group');
			if(count($request->id) > 1){
	        	$groups  = ($maxValue+1);
        	}else{
	        	$groups  = 0;
        	}
    		foreach($request->id as $key => $data_id) {
    			$rencana = RencanaAuditDetail::find($data_id);
    			$tipes = $rencana->tipe_object;
	    		$record = new PenugasanAudit;
		        $record->rencana_id 		= $data_id;
				if($tipes == 2){
					$bu = findbupic($rencana->object_id);
					$record->user_id 			= $bu;
				}else{
					$record->user_id 			= 1;
				}
		        $record->group 				= $groups;
		        if($request->status == 0){
		        	$record->status 		= 0;
		        }else{
		        	$record->status 		= 1;
		        	$record->ket_svp 		= null;
		        	$rencanadetail = RencanaAuditDetail::find($data_id);
			        $rencanadetail->status_penugasan =1;
			        $rencanadetail->save();
		        }
		        $record->save();
		        $record->saveDetail($request->detail);
		        if(count($request->details) > 0){
			        $record->saveDetailTembusan($request->details);
		        }

		        $user = auth()->user();
		    	$log = new SessionLog;
			    $log->modul = 'Kegiatan Audit Persiapan';
			    $log->sub = 'Surat Penugasan';
			    $log->aktivitas = 'Membuat Surat Penugasan Tahun '.$record->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
			    $log->user_id = $user->id;
			    $log->save();
    		}
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function create()
    {
        return $this->render('modules.audit.persiapan.penugasan.create');
    }

    public function uploadFinal(PenugasanAudit $id)
    {
    	if($id->group > 0){
	    	$cari = PenugasanAudit::where('group', $id->group)->get();
    	}else{
    		$cari = collect([$id]);
    	}

        return $this->render('modules.audit.persiapan.penugasan.final', [
        	'record' => $id,
        	'data' => $cari,
        ]);
    }

    public function approveSvpDetail(PenugasanAudit $id)
    {
    	if($id->group > 0){
	    	$cari = PenugasanAudit::where('group', $id->group)->get();
    	}else{
    		$cari = collect([$id]);
    	}
        return $this->render('modules.audit.persiapan.penugasan.approvesvpdetil', [
        	'record' => $id,
        	'data' => $cari,
        ]);
    }

    public function approveDirutDetail(PenugasanAudit $id)
    {
    	if($id->group > 0){
	    	$cari = PenugasanAudit::where('group', $id->group)->get();
    	}else{
    		$cari = collect([$id]);
    	}
        return $this->render('modules.audit.persiapan.penugasan.approvedirutdetil', [
        	'record' => $id,
        	'data' => $cari,
        ]);
    }

    public function buat($id)
    {
    	$data_id = explode(',', $id);
    	$data = RencanaAuditDetail::whereIn('id', $data_id)->get();
    	$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Persiapan' => '#', 'Surat Penugasan' => '#', 'Buat' => '#']);
        return $this->render('modules.audit.persiapan.penugasan.buat',[
        	'record' => $data,
        	'data' => $data_id,
        ]);
    }

    public function detilProject(RencanaAuditDetail $id)
    {
        return $this->render('modules.audit.persiapan.penugasan.detil', ['record' => $id]);
    }

    public function detilPenugasan(PenugasanAudit $id)
    {
    	if($id->group > 0){
	    	$cari = PenugasanAudit::where('group', $id->group)->get();
    	}else{
    		$cari = collect([$id]);
    	}
        return $this->render('modules.audit.persiapan.penugasan.detil-penugasan', [
        	'record' => $id,
        	'data' => $cari,
        ]);
    }

    public function detil(Program $id)
    {
    	$cari = ProgramAuditDetail::where('pkat_detail_id', $id->id)->first();
    	$operasional = FokusAudit::where('bidang', 1)->get();
    	$keuangan = FokusAudit::where('bidang', 2)->get();
    	$sistem = FokusAudit::where('bidang', 3)->get();
    	$user = User::where('id','!=',1)->get();
    	$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Persiapan' => '#', 'Surat Penugasan' => '#', 'Buat' => '#']);
        return $this->render('modules.audit.persiapan.penugasan.detil', [
        	'record' => $id,
        	'data' 	 => $cari,
        	'operasional' => $operasional,
        	'keuangan' => $keuangan,
        	'sistem' => $sistem,
        	'user' => $user,

        ]);
    }

    public function update(PenugasanAuditRequest $request)
    {
    	DB::beginTransaction();
        try {
        	$record = PenugasanAudit::find($request->id);
        	if($record->group > 0){
        		$cek = PenugasanAudit::where('group', $record->group)->get();
        		foreach ($cek as $key => $value) {
        			$data = PenugasanAudit::find($value->id);
	        		if($request->status == 0){
			        	$data->status 		= 0;
			        }else{
			        	$data->status 		= 1;
			        	$data->ket_svp 		= null;
			        	$rencanadetail = RencanaAuditDetail::find($data->rencana_id);
				        $rencanadetail->status_penugasan =1;
				        $rencanadetail->save();
			        }
			        $data->save();
			        $cari = PenugasanAuditDetailJadwal::where('penugasan_id', $value->id)->pluck('id');
			        $hapus_detil = PenugasanAuditDetailJadwalDetail::whereIn('jadwal_detail_id', $cari->toArray())
			                        					 		->delete();

			        $hapus_jadwal = PenugasanAuditDetailJadwal::where('penugasan_id', $value->id)
			                        					 		->delete();

		        	$hapus = PenugasanAuditDetailAnggota::where('penugasan_id', $value->id)
			                        					 		->delete();
			        $hapus_tembusan = PenugasanAuditDetailTembusan::where('penugasan_id', $value->id)
			                        					 		 ->delete();

			        $data->saveDetail($request->detail);
			        if(count($request->details) > 0){
				        $data->saveDetailTembusan($request->details);
			        }
			        $user = auth()->user();
			    	$log = new SessionLog;
				    $log->modul = 'Kegiatan Audit Persiapan';
				    $log->sub = 'Surat Penugasan';
				    $log->aktivitas = 'Mengubah Surat Penugasan Tahun '.$data->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($data->rencanadetail->tipe_object, $data->rencanadetail->object_id);
				    $log->user_id = $user->id;
				    $log->save();
        		}
        	}else{
		        if($request->status == 0){
		        	$record->status 		= 0;
		        }else{
		        	$record->status 		= 1;
		        	$record->ket_svp 		= null;
		        	$rencanadetail = RencanaAuditDetail::find($record->rencana_id);
			        $rencanadetail->status_penugasan =1;
			        $rencanadetail->save();
		        }

		        $record->save();
		        $cari = PenugasanAuditDetailJadwal::where('penugasan_id', $request->id)->pluck('id');

		        $hapus_detil = PenugasanAuditDetailJadwalDetail::whereIn('jadwal_detail_id', $cari->toArray())
		                        					 		->delete();

		        $hapus_jadwal = PenugasanAuditDetailJadwal::where('penugasan_id', $request->id)
		                        					 		->delete();

	        	$hapus = PenugasanAuditDetailAnggota::where('penugasan_id', $request->id)
		                        					 		->delete();
		        $hapus_tembusan = PenugasanAuditDetailTembusan::where('penugasan_id', $request->id)
			                        					 		 ->delete();

		        $record->saveDetail($request->detail);
			    $record->saveDetailTembusan($request->details);

		        $user = auth()->user();
		    	$log = new SessionLog;
			    $log->modul = 'Kegiatan Audit Persiapan';
			    $log->sub = 'Surat Penugasan';
			    $log->aktivitas = 'Mengubah Surat Penugasan Tahun '.$record->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
			    $log->user_id = $user->id;
			    $log->save();
        	}
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function updateSurat(Request $request)
    {
    	DB::beginTransaction();
        try {
        	$cari = PenugasanAudit::where('program_id', $request->id)->first();
        	if($cari){
	        	$record 				= PenugasanAudit::find($cari->id);
	        	$penugasan->realisasi 	= $request->realisasi;
		        $record->save();
        	}else{
        		$penugasan 				= new PenugasanAudit;
        		$penugasan->program_id 	= $request->id;
        		$penugasan->realisasi 	= $request->realisasi;
        		$penugasan->save();
        	}

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function surat(PenugasanAudit $id)
    {
    	$daftar = PenugasanAudit::find($id);
    	$link =0;
    	if(file_exists(public_path('storage/'.$id->bukti)))
    	{
    		$link = asset('/storage/'.$id->bukti);
    	}
    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);
    }

    public function edit(PenugasanAudit $id)
    {
    	if($id->group > 0){
	    	$cari = PenugasanAudit::where('group', $id->group)->get();
    	}else{
    		$cari[0]=$id;
    	}

    	$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Persiapan' => '#', 'Surat Penugasan' => '#', 'Ubah' => '#']);
        return $this->render('modules.audit.persiapan.penugasan.edit', [
        	'record' => $id,
        	'data' => $cari,
        ]);
    }

    public function preview(PenugasanAudit $id)
    {
        $check = Files::where('target_id', $id->id)->where('target_type', 'surat-penugasan-audit')->get();
        $files = [];
        $link =0;
        if($check->count() > 0)
        {
            $rename = [];
            foreach($check as $c)
            {
                if(file_exists(public_path('storage/'.$c->url)))
                {
                    $newname = '';
                    $splitname = explode("/",$c->url);
                    for ($i=0; $i < count($splitname) - 1 ; $i++) {
                        $newname .= $splitname[$i].'/';
                    }
                    $files[] = public_path('storage/'.$newname.$c->filename);
                    $rename[] = [
                        'old' => $c->url,
                        'new' => $c->filename,
                    ];
                }
            }
            $link =asset('/storage/'.$rename[0]['old']);
            return $this->render('pdf', [
	        	'mockup' => true,
	        	'link' => $link,
	        ]);
        }
    }

    public function saveFinal(PenugasanAuditFinalRequest $request)
    {
        DB::beginTransaction();
    	try {
    		$record 								= PenugasanAudit::find($request->id);
    		if($record->group > 0){
        		$cek = PenugasanAudit::where('group', $record->group)->get();
        		foreach ($cek as $key => $value) {
        			$data = PenugasanAudit::find($value->id);
        			$data->no_surat 				= $request->no_surat;
					$data->tgl_surat 				= $request->tgl_surat;
			    	$path 							= $request->surat->store('rencana-audit', 'public');
					$data->filename 				= $request->surat->getClientOriginalName();
					$data->bukti 					= $path;
					$data->status 					= 4;

          // [NOTE] adding dms needed
          $data->dms_object_type = 'Penugasan Audit';
          $data->dms_update = 1;

			        $data->save();

			        $update_notif = Notification::where('parent_id', $data->id)->where('modul', 'penugasan-audit')->where('stage', 3)->get();
			        foreach ($update_notif as $key => $vil) {
			        	$data_notif = Notification::find($vil->id);
				        $data_notif->status =0;
				        $data_notif->save();
			        }

					$userz = User::whereHas('roles', function($u){
											$u->where('name', 'auditor');
										})->get();
					foreach ($userz as $key => $vul) {
						$notif = new Notification;
					    $notif->user_id = $vul->id;
					    $notif->parent_id = $data->id;
					    $notif->url = route($this->routes.'.index');
					    $notif->modul = 'penugasan-audit';
					    $notif->stage = 4;
					    $notif->keterangan = 'Permintaan Dokumen Tahun '.$data->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($data->rencanadetail->tipe_object, $data->rencanadetail->object_id);
					    $notif->status = 1;
					    $notif->save();
					}
        		}
        	}else{
				$record->no_surat 				= $request->no_surat;
				$record->tgl_surat 				= $request->tgl_surat;
		    	$path 							= $request->surat->store('rencana-audit', 'public');
				$record->filename 				= $request->surat->getClientOriginalName();
				$record->bukti 					= $path;
				$record->status 				= 4;
		        $record->save();

		        $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'penugasan-audit')->where('stage', 3)->get();
		        foreach ($update_notif as $key => $vil) {
		        	$data_notif = Notification::find($vil->id);
			        $data_notif->status =0;
			        $data_notif->save();
		        }

				$userz = User::whereHas('roles', function($u){
										$u->where('name', 'auditor');
									})->get();
				foreach ($userz as $key => $vul) {
					$notif = new Notification;
				    $notif->user_id = $vul->id;
				    $notif->parent_id = $record->id;
				    $notif->url = route($this->routes.'.index');
				    $notif->modul = 'penugasan-audit';
				    $notif->stage = 4;
				    $notif->keterangan = 'Permintaan Dokumen Tahun '.$record->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
				    $notif->status = 1;
				    $notif->save();
				}
        	}
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(ProgramAudit $program)
    {
        $program->delete();
        return response()->json([
            'success' => true
        ]);
    }

    public function cetak(Request $request, $id){
    	$record = PenugasanAudit::find($id);
        $data = [
        	'records' => $record,
        ];
        // $merge = new Merger();

	    $pdf = PDF::loadView('modules.audit.persiapan.penugasan.cetak', $data);
	    // $merge->addRaw($pdf->output());
	    // $pdf2 = PDF::loadView('modules.audit.persiapan.penugasan.cetak-jadwal', $data);
	    // $pdf2->setPaper('a4', 'landscape');
	    // $merge->addRaw($pdf2->output());

	    return $pdf->stream('Rencana Audit.pdf',array("Attachment" => false));
        // return new Response($merge->merge(), 200, array(
        //     'Content-Type' => 'application/pdf',
        //     'Content-Disposition' =>  'inline; filename="Penugasan Audit.pdf"',
        // ));
 	}

 	public function cetakAudit(Request $request, $id){
    	$record = RencanaAudit::find($id);
        $data = [
        	'records' => $record,
        ];
	    $pdf = PDF::loadView('modules.audit.persiapan.penugasan.cetak-audit', $data)->setPaper('a4', 'landscape');
        return $pdf->stream('Rencana Audit.pdf',array("Attachment" => false));
 	}

 	public function downloadAudit($id)
  {
  	$daftar = RencanaAudit::find($id);

    if($daftar->dms == 1)
    {
        $this->downloadDms($daftar->bukti, $daftar->filename);
        $ext = pathinfo($daftar->filename);

        return response()->download(public_path('storage/temp-file/'.md5($daftar->filename.auth()->user()->id).'.'.$ext['extension']), $daftar->filename)->deleteFileAfterSend(true);
    }else{
    	if(file_exists(public_path('storage/'.$daftar->bukti)))
    	{
    		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
    	}

      return abort('404');
    }
  }

    public function cetakExcelPenugasan(Request $request, $id)
    {
    	$record 	= PenugasanAudit::find($id);
        $tittle     = 'Jadwal Audit Internal';
        $tgl     	= 'Jadwal Audit Internal';
        $view       = "modules.audit.persiapan.penugasan.export";
        $data_array = [
            'tittle'   => $tittle,
            'tgl'      => DateToStringWday($record->tgl_surat),
            'data'     => $record,
        ];

        return Excel::download(new GenerateExport($view, $data_array), $tittle.'.xls');
    }

    public function downloadDms($url, $filename)
    {
        $ext = pathinfo($filename);

        $otdsTicket = $this->otdsLogin();
        $otcsTicket = $this->otcsLogin($otdsTicket);

        Storage::disk('public')->put('temp-file/'.md5($filename.auth()->user()->id).'.'.$ext['extension'], $this->showfiledms($otcsTicket, $url));
    }

    public function otdsLogin()
    {

      $client = new Client;

      $response = $client->request('POST', 'https://otds.waskita.co.id:8443/otdsws/v1/authentication/credentials', [
          'json' => [
              'user_name' => 'adminIAO',
              'password' => 'Audit123@'
          ],
          'verify' => false,
      ]);

      $ticket = json_decode($response->getBody()->getContents())->ticket;

      return $ticket;

    }

    public function otcsLogin($ticket)
    {
      try {
        $client = new Client;

        $response = $client->request('POST', 'http://dmsinterface.waskita.co.id:8080/xecm/api/v1/auth/otdsticket', [
            'json' => [
                'otds_ticket' => $ticket
            ],
            'verify' => false,
        ]);

        return json_decode($response->getBody()->getContents())->ticket;

      }catch (\Exception $exception){
        return $this->otcsLogin($this->otdsLogin());
      }

    }

    public function showfiledms($ticket, $url)
    {
      // try {
        $client = new Client;

        $response = $client->request('GET', $url, [
            'headers' => [
                'otcsticket' => $ticket
            ],
        ]);

        return $response->getBody()->getContents();

      // }catch (\Exception $exception){
      //   return abort('404');
      // }

    }
}
