<?php

namespace App\Http\Controllers\KegiatanAudit\Persiapan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Response;
use iio\libmergepdf\Merger;

use App\Http\Requests\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumenRequest;
use App\Http\Requests\KegiatanAudit\Persiapan\TinjauanDokumen\ApprovalRequest;
use App\Http\Requests\KegiatanAudit\Persiapan\TinjauanDokumen\DokumenRequest;
use App\Http\Requests\KegiatanAudit\Rencana\RejectRequest;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\KegiatanAudit\Rencana\RencanaAudit;
use App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumen;
use App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumenDetail;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit;
use App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumenPenerima;
use App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumenTembusan;
use App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumenEmail;
use App\Models\Auths\User;
use App\Models\Files;
use App\Models\FlowData;
use App\Models\SessionLog;
use App\Models\Notification;
use GuzzleHttp\Client;


use DB;
use PDF;

class TinjauanDokumenController extends Controller
{
    protected $routes = 'kegiatan-audit.persiapan.tinjauan-dokumen';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Kegiatan Audit' => '#', 'Persiapan' => '#', 'Permintaan Dokumen' => '#']);
        $this->listStructs = [
        	'listStruct' => [
		        	[
		                'data' => 'num',
		                'name' => 'num',
		                'label' => '#',
		                'orderable' => false,
		                'searchable' => false,
		                'className' => 'text-center',
		                'width' => '20px',
		            ],
		            [
		                'data' => 'tahun',
		                'name' => 'tahun',
		                'label' => 'Tahun',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'rencana',
		                'name' => 'rencana',
		                'label' => 'Rencana Pelaksanaan',
		                // 'className' => 'text-center',
		                'width' => '300px',
		            ],
		            [
		                'data' => 'tipe',
		                'name' => 'tipe',
		                'label' => 'Tipe',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'kategori',
		                'name' => 'kategori',
		                'label' => 'Kategori',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'object_audit',
		                'name' => 'object_audit',
		                'label' => 'Objek Audit',
		                'className' => 'text-left',
		                'width' => '250px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'no_ab',
		                'name' => 'no_ab',
		                'label' => 'No AB',
		                // 'className' => 'text-center',
		                'width' => '150px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'detil_program',
		                'name' => 'detil_program',
		                'label' => 'Rencana Kerja',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'penugasan',
		                'name' => 'penugasan',
		                'label' => 'Surat Penugasan',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'status',
		                'name' => 'status',
		                'label' => 'Status',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '100px',
		                'className' => 'text-center'
		            ],
		            [
	                    'data' => 'checklist',
	                    'name' => 'checklist',
	                    'label' => 'Checklist',
	                    'searchable' => false,
	                    'sortable' => false,
	                    'className' => "text-center",
	                    'width' => '100px',
                	],
        	],
        	'listStruct2' => [
		        	[
		                'data' => 'num',
		                'name' => 'num',
		                'label' => '#',
		                'orderable' => false,
		                'searchable' => false,
		                'className' => 'text-center',
		                'width' => '20px',
		            ],
		            [
		                'data' => 'tahun',
		                'name' => 'tahun',
		                'label' => 'Tahun',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'rencana',
		                'name' => 'rencana',
		                'label' => 'Rencana Pelaksanaan',
		                // 'className' => 'text-center',
		                'width' => '300px',
		            ],
		            [
		                'data' => 'tipe',
		                'name' => 'tipe',
		                'label' => 'Tipe',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'kategori',
		                'name' => 'kategori',
		                'label' => 'Kategori',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'object_audit',
		                'name' => 'object_audit',
		                'label' => 'Objek Audit',
		                'className' => 'text-left',
		                'width' => '250px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'no_ab',
		                'name' => 'no_ab',
		                'label' => 'No AB',
		                // 'className' => 'text-center',
		                'width' => '150px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'detil_program',
		                'name' => 'detil_program',
		                'label' => 'Rencana Kerja',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'penugasan',
		                'name' => 'penugasan',
		                'label' => 'Surat Penugasan',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'detil_tinjauan',
		                'name' => 'detil_tinjauan',
		                'label' => 'Tinjauan Dokumen',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'pic',
		                'name' => 'pic',
		                'label' => 'PIC',
		                'sortable' => true,
		                'width' => '250px',
		            ],
		            [
		                'data' => 'status',
		                'name' => 'status',
		                'label' => 'Status',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '100px',
		                'className' => 'text-center'
		            ],
                	[
		                'data' => 'action',
		                'name' => 'action',
		                'label' => 'Aksi',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '80px',
		                'className' => 'text-center'
		            ],
        	],
        	'listStruct3' => [
		        	[
		                'data' => 'num',
		                'name' => 'num',
		                'label' => '#',
		                'orderable' => false,
		                'searchable' => false,
		                'className' => 'text-center',
		                'width' => '20px',
		            ],
		            [
		                'data' => 'tahun',
		                'name' => 'tahun',
		                'label' => 'Tahun',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'rencana',
		                'name' => 'rencana',
		                'label' => 'Rencana Pelaksanaan',
		                // 'className' => 'text-center',
		                'width' => '300px',
		            ],
		            [
		                'data' => 'tipe',
		                'name' => 'tipe',
		                'label' => 'Tipe',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'kategori',
		                'name' => 'kategori',
		                'label' => 'Kategori',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'object_audit',
		                'name' => 'object_audit',
		                'label' => 'Objek Audit',
		                'className' => 'text-left',
		                'width' => '250px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'no_ab',
		                'name' => 'no_ab',
		                'label' => 'No AB',
		                // 'className' => 'text-center',
		                'width' => '150px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'detil_program',
		                'name' => 'detil_program',
		                'label' => 'Rencana Kerja',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'penugasan',
		                'name' => 'penugasan',
		                'label' => 'Surat Penugasan',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'detil_tinjauan',
		                'name' => 'detil_tinjauan',
		                'label' => 'Tinjauan Dokumen',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'status',
		                'name' => 'status',
		                'label' => 'Status',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '100px',
		                'className' => 'text-center'
		            ],
		            [
		                'data' => 'action',
		                'name' => 'action',
		                'label' => 'Aksi',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '80px',
		                'className' => 'text-center'
		            ],
        	],
        ];
    }

    public function grid()
    {
    	$user = auth()->user();
    	if($user->hasJabatan(['SVP - Internal Audit','President Director','auditor'])){
       	 	$records = PenugasanAudit::where('status_tinjauan', 0)->whereIn('status', [1,2,3,4])
        						       ->select('*');

     		if(!isset(request()->order[0]['column'])) {
	              $records->orderBy('created_at','desc');
	        }
	        if ($tahun = request()->tahun) {
	        	$records->whereHas('rencanadetail', function($u) use ($tahun){
	        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
	        			$z->where('tahun', 'like', '%' . $tahun . '%');
	        		});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('rencanadetail', function($u) use ($tipe){
	        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
	        			if($tipe == 1){
			        		$z->where('tipe', 0);
		        		}else{
		        			$z->where('tipe', 1);
		        		}
	        		});
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 0);
		        	});
	        	}elseif($kategori == 2){
	        		$records->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 1);
		        	});
	        	}elseif($kategori == 3){
	        		$records->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 2);
		        	});
	        	}elseif($kategori == 4){
	        		$records->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 3);
		        	});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->whereHas('rencanadetail', function($u) use ($kat, $object_id){
	        		$u->where('tipe_object', $kat)->where('object_id', $object_id);
	        	});
	        }
      	}elseif($user->hasJabatan(['SVP','Project'])){
      		$cari = checkuser($user->id);
    		$tipe_object = $cari[0];
    		$object_id = $cari[1];
      		$records = PenugasanAudit::where('status_tinjauan', 0)->whereIn('status', [1,2,3,4])
      									->whereHas('rencanadetail', function($y) use ($tipe_object, $object_id, $user){
											$y->where('tipe_object', $tipe_object)
											  ->whereIn('object_id', $object_id);
										})->orWhere('user_id', $user->id)
        						        ->select('*');

     		if(!isset(request()->order[0]['column'])) {
	              $records->orderBy('created_at','desc');
	        }
	        if ($tahun = request()->tahun) {
	        	$records->whereHas('rencanadetail', function($u) use ($tahun){
	        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
	        			$z->where('tahun', 'like', '%' . $tahun . '%');
	        		});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('rencanadetail', function($u) use ($tipe){
	        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
	        			if($tipe == 1){
			        		$z->where('tipe', 0);
		        		}else{
		        			$z->where('tipe', 1);
		        		}
	        		});
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 0);
		        	});
	        	}elseif($kategori == 2){
	        		$records->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 1);
		        	});
	        	}elseif($kategori == 3){
	        		$records->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 2);
		        	});
	        	}elseif($kategori == 4){
	        		$records->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 3);
		        	});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->whereHas('rencanadetail', function($u) use ($kat, $object_id){
	        		$u->where('tipe_object', $kat)->where('object_id', $object_id);
	        	});
	        }
      	}else{
      		$records = PenugasanAudit::where('id', 0);
      	}

        return DataTables::of($records->get())
        	   ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('tahun', function ($record) {
                   return $record->rencanadetail->rencanaaudit->tahun;
               })
               ->addColumn('tipe', function ($record) {
                    $tipe = '';
	                if($record->rencanadetail->tipe == 0){
	               	  $tipe = '<span class="label label-primary">Audit</span>';
	                }elseif($record->rencanadetail->tipe == 1){
	               	  $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
	                }else{
	                  $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
	                }

	                return $tipe;
               })
               ->addColumn('kategori', function ($record) {
                    $kategori = '';
                    if($record->rencanadetail->tipe == 0){
		                if($record->rencanadetail->tipe_object == 0){
		               	  $kategori = '<span class="label label-success">Business Unit (BU)</span>';
		                }elseif($record->rencanadetail->tipe_object == 1){
		               	  $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
		                }elseif($record->rencanadetail->tipe_object == 2){
		               	  $kategori = '<span class="label label-warning">Project</span>';
		                }else{
		               	  $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
		                }
	                }elseif($record->rencanadetail->tipe == 1){
	               	  	$kategori = $record->rencanadetail->konsultasi->nama;
	                }else{
	                    $kategori = $record->rencanadetail->lain->nama;
	                }
	                return $kategori;
               })
               ->addColumn('object_audit', function ($record) {
                   return object_audit($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
               })
               ->addColumn('rencana', function ($record) {
                   return $record->rencanadetail->rencana;
               })
               ->addColumn('no_ab', function ($record) {
	               return getAb($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
	           })
               ->addColumn('nama', function ($record) {
               		if($record->rencanadetail->tipe_object == 2){
	               		return object_audit($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
	               	}else{
	               		return '';
	               	}
               })
               ->editColumn('status', function ($record) {
	               if($record->tinjauandokumen){
	               	  if($record->tinjauandokumen->ket_svp){
	               	  	return '<span class="label label-warning">Ditolak</span>';
	               	  }else{
	               	  	return '<span class="label label-info">Draft</span>';
	               	  }
	               }else{
		               return '<span class="label label-default">Baru</span>';
	               }
	           })
	           ->addColumn('detil_program', function ($record) use ($user) {
	               $buttons = '';
	               if($user->hasJabatan(['SVP - Internal Audit','President Director','auditor'])){
		               $buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
		                    'tooltip' 	=> 'Rencana Kerja',
		                    'class' 	=> 'download-rkia button',
		                    'id'   		=> $record->rencanadetail->rencanaaudit->id,
		               ]);
	               }else{
	               	 $buttons ='';
	               }
	               return $buttons;
	           })
	           ->addColumn('penugasan', function ($record) {
	               $buttons = '';
	               if($record->status == 4){
		               $buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
		                    'tooltip' 	=> 'Surat Penugasan',
		                    'class' 	=> 'download-penugasan button',
		                    'id'   		=> $record->id,
		               ]);
	               }else{
	               	    $buttons .='';
	               }
	               return $buttons;
	           })
               ->addColumn('nilai', function ($record) {
               		return number_format($record->nilai, 0, '.', '.');
           	   })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->editColumn('created_by', function ($record) {
                   return $record->creatorName();
               })
               ->editColumn('checklist', function($record) use($user){
               		if($user->hasRole(['auditor'])){
	               		if($record->tinjauandokumen){
	               			$checklist= $this->makeButton([
				           			'type' 		=> 'edit',
				           			'label' 	=> '<i class="fa fa-edit text-primary"></i>',
				           			'tooltip' 	=> 'Ubah',
				           			'class' 	=> 'ubah button',
				           			'id'   		=> $record->tinjauandokumen->id,
				           			]);
	               		}else{
			                $checklist = '<input type="checkbox" class="view check" name="check[]" data-id="'.$record->id.'" value="'.$record->id.'" data-toggle="toggle" data-size="mini" data-on="Ya" data-off="Tidak" data-style="ios">';
	               		}
		            }else{
		            	$checklist = '';
		            }
	                return $checklist;
	            })
               ->rawColumns(['object_audit','tipe','kategori','detil','detil_program','status','object_audit','nama','checklist','penugasan'])
               ->make(true);
    }

    public function onProgress()
    {
    	$user = auth()->user();
    	if($user->hasJabatan(['SVP - Internal Audit','President Director','auditor'])){
    		$records = TinjauanDokumen::whereIn('status', [1,2,3,4])->select('*');
    		if(!isset(request()->order[0]['column'])) {
	              $records->orderBy('created_at','desc');
	        }
	        if($tahun = request()->tahun) {
	        	$records->whereHas('penugasanaudit', function($b) use ($tahun){
		        	$b->whereHas('rencanadetail', function($u) use ($tahun){
		        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
		        			$z->where('tahun', 'like', '%' . $tahun . '%');
		        		});
		        	});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('penugasanaudit', function($a) use ($tipe){
		        	$a->whereHas('rencanadetail', function($u) use ($tipe){
		        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
		        			if($tipe == 1){
				        		$z->where('tipe', 0);
			        		}else{
			        			$z->where('tipe', 1);
			        		}
		        		});
		        	});
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('penugasanaudit', function($a){
		        		$a->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 0);
			        	});
	        		});
	        	}elseif($kategori == 2){
	        		$records->whereHas('penugasanaudit', function($a){
		        		$a->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 1);
			        	});
	        		});
	        	}elseif($kategori == 3){
	        		$records->whereHas('penugasanaudit', function($a){
		        		$a->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 2);
			        	});
	        		});
	        	}elseif($kategori == 4){
	        		$records->whereHas('penugasanaudit', function($a){
		        		$a->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 3);
			        	});
	        		});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->whereHas('penugasanaudit', function($a) use ($kat, $object_id){
	        		$a->whereHas('rencanadetail', function($u) use ($kat, $object_id){
	        			$u->where('tipe_object', $kat)->where('object_id', $object_id);
	        		});
        		});
	        }
    	}elseif($user->hasJabatan(['SVP','Project'])){
    		$cari = checkuser($user->id);
    		$tipe_object = $cari[0];
    		$object_id = $cari[1];
    		$records = TinjauanDokumen::whereHas('penugasanaudit', function($x) use ($tipe_object, $object_id, $user){
    										$x->whereHas('rencanadetail', function($y) use ($tipe_object, $object_id, $user){
    											$y->where('tipe_object', $tipe_object)
    											  ->whereIn('object_id', $object_id);
    										})->orWhere('user_id', $user->id);
    									})
    									->whereIn('status', [2,3,4])
    									->select('*');
    		if(!isset(request()->order[0]['column'])) {
	              $records->orderBy('created_at','desc');
	        }
	        if($tahun = request()->tahun) {
	        	$records->whereHas('penugasanaudit', function($b) use ($tahun){
		        	$b->whereHas('rencanadetail', function($u) use ($tahun){
		        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
		        			$z->where('tahun', 'like', '%' . $tahun . '%');
		        		});
		        	});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('penugasanaudit', function($a) use ($tipe){
		        	$a->whereHas('rencanadetail', function($u) use ($tipe){
		        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
		        			if($tipe == 1){
				        		$z->where('tipe', 0);
			        		}else{
			        			$z->where('tipe', 1);
			        		}
		        		});
		        	});
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('penugasanaudit', function($a){
		        		$a->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 0);
			        	});
	        		});
	        	}elseif($kategori == 2){
	        		$records->whereHas('penugasanaudit', function($a){
		        		$a->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 1);
			        	});
	        		});
	        	}elseif($kategori == 3){
	        		$records->whereHas('penugasanaudit', function($a){
		        		$a->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 2);
			        	});
	        		});
	        	}elseif($kategori == 4){
	        		$records->whereHas('penugasanaudit', function($a){
		        		$a->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 3);
			        	});
	        		});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->whereHas('penugasanaudit', function($a) use ($kat, $object_id){
	        		$a->whereHas('rencanadetail', function($u) use ($kat, $object_id){
	        			$u->where('tipe_object', $kat)->where('object_id', $object_id);
	        		});
        		});
	        }
    	}else{
    		$records = TinjauanDokumen::whereHas('detailtembusan', function($x) use ($user){
    										$x->Where('user_id', $user->id);
    									})
    									->whereIn('status', [2,3,4])
    									->select('*');
    		if(!isset(request()->order[0]['column'])) {
	              $records->orderBy('created_at','desc');
	        }
	        if($tahun = request()->tahun) {
	        	$records->whereHas('penugasanaudit', function($b) use ($tahun){
		        	$b->whereHas('rencanadetail', function($u) use ($tahun){
		        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
		        			$z->where('tahun', 'like', '%' . $tahun . '%');
		        		});
		        	});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('penugasanaudit', function($a) use ($tipe){
		        	$a->whereHas('rencanadetail', function($u) use ($tipe){
		        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
		        			if($tipe == 1){
				        		$z->where('tipe', 0);
			        		}else{
			        			$z->where('tipe', 1);
			        		}
		        		});
		        	});
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('penugasanaudit', function($a){
		        		$a->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 0);
			        	});
	        		});
	        	}elseif($kategori == 2){
	        		$records->whereHas('penugasanaudit', function($a){
		        		$a->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 1);
			        	});
	        		});
	        	}elseif($kategori == 3){
	        		$records->whereHas('penugasanaudit', function($a){
		        		$a->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 2);
			        	});
	        		});
	        	}elseif($kategori == 4){
	        		$records->whereHas('penugasanaudit', function($a){
		        		$a->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 3);
			        	});
	        		});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->whereHas('penugasanaudit', function($a) use ($kat, $object_id){
	        		$a->whereHas('rencanadetail', function($u) use ($kat, $object_id){
	        			$u->where('tipe_object', $kat)->where('object_id', $object_id);
	        		});
        		});
	        }
    	}
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('tahun', function ($record) {
                   return $record->penugasanaudit->rencanadetail->rencanaaudit->tahun;
               })
               ->addColumn('tipe', function ($record) {
                    $tipe = '';
	                if($record->penugasanaudit->rencanadetail->tipe == 0){
	               	  $tipe = '<span class="label label-primary">Audit</span>';
	                }elseif($record->penugasanaudit->rencanadetail->tipe == 1){
	               	  $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
	                }else{
	                  $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
	                }

	                return $tipe;
               })
               ->addColumn('kategori', function ($record) {
                    $kategori = '';
                    if($record->penugasanaudit->rencanadetail->tipe == 0){
		                if($record->penugasanaudit->rencanadetail->tipe_object == 0){
		               	  $kategori = '<span class="label label-success">Business Unit (BU)</span>';
		                }elseif($record->penugasanaudit->rencanadetail->tipe_object == 1){
		               	  $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
		                }elseif($record->penugasanaudit->rencanadetail->tipe_object == 2){
		               	  $kategori = '<span class="label label-warning">Project</span>';
		                }else{
		               	  $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
		                }
	                }elseif($record->penugasanaudit->rencanadetail->tipe == 1){
	               	  	$kategori = $record->penugasanaudit->rencanadetail->konsultasi->nama;
	                }else{
	                    $kategori = $record->penugasanaudit->rencanadetail->lain->nama;
	                }
	                return $kategori;
               })
               ->addColumn('object_audit', function ($record) {
                   return object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
               })
               ->addColumn('rencana', function ($record) {
                   return $record->penugasanaudit->rencanadetail->rencana;
               })
               ->addColumn('nama', function ($record) {
               		if($record->penugasanaudit->rencanadetail->tipe_object == 2){
	               		return object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
	               	}else{
	               		return '';
	               	}
               })
               ->addColumn('detil_program', function ($record) use ($user) {
	               $buttons = '';
	               if($user->hasJabatan(['SVP - Internal Audit','President Director','auditor'])){
		               $buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
		                    'tooltip' 	=> 'Rencana Kerja',
		                    'class' 	=> 'download-rkia button',
		                    'id'   		=> $record->penugasanaudit->rencanadetail->rencanaaudit->id,
		               ]);
	               }else{
	               		$buttons='';
	               }
	               return $buttons;
	           })
	           ->addColumn('penugasan', function ($record) {
	               $buttons = '';
	               if($record->penugasanaudit->status == 4){
		               $buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
		                    'tooltip' 	=> 'Surat Penugasan',
		                    'class' 	=> 'download-penugasan button',
		                    'id'   		=> $record->penugasanaudit->id,
		               ]);
	               }else{
	               	    $buttons .='';
	               }
	               return $buttons;
	           })
	           ->addColumn('detil_tinjauan', function ($record) {
	               $buttons = '';
	               $buttons .= $this->makeButton([
	           			'type' 		=> 'edit',
	           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
	           			'tooltip' 	=> 'Tinjauan Dokumen',
	           			'class' 	=> 'detil-tinjauan button',
	           			'id'   		=> $record->id,
	           		]);

	               return $buttons;
	           })
	           ->addColumn('no_ab', function ($record) {
	               return getAb($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
	           })
	           ->addColumn('pic', function ($record) {
               		$cek = object_user($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id)[0];
           			$pic ='';
	           		$pic .='<ul class="list list-more4 list-unstyled" style="margin-top:10px;" data-display="3">';
	           		foreach ($record->detailpenerima as $key => $value) {
		           			if(in_array($value->user->id, $cek)){
		           				$pic .='<li class="item" style="padding:0px;border:none;">'.$value->user->name.'</li>';
		           			}
	           		}
	           		$pic .='</ul>';
	               	return $pic;
	           })
               ->addColumn('nilai', function ($record) {
               		return number_format($record->nilai, 0, '.', '.');
           	   })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->editColumn('created_by', function ($record) {
                   return $record->creatorName();
               })
               ->editColumn('status', function ($record) {
	           		if($record->status == 1){
		               	return '<span class="label label-info">Waiting Approval SVP</span>';
	           		}elseif($record->status == 2){
	           			return '<span class="label label-info">Waiting Upload</span>';
	           		}elseif($record->status == 3){
	           			return '<span class="label label-info">Waiting Reupload</span>';
	           		}elseif($record->status == 4){
	           			return '<span class="label label-info">Waiting Approval Auditor</span>';
	           		}else{
	           			return '<span class="label label-info">Completed</span>';
	           		}
	           })
               ->editColumn('action', function($record) use ($user){
	               	$buttons ='';
	               	if($record->status == 1){
		               	if($user->hasJabatan(['SVP - Internal Audit'])){
		               		if($record->group > 0){
		               			$cek = TinjauanDokumen::where('group', $record->group)->where('status', '<', 1)->get()->count();
		               			if($cek > 0){
				                	$buttons .= $this->makeButton([
				               			'type' 		=> 'edit',
				               			'label' 	=> '<i class="fa fa-check text-primary"></i>',
				               			'tooltip' 	=> 'Buat Approval',
				               			'class' 	=> 'approve-svp button disabled',
				               			'id'   		=> $record->id,
				               		]);
		               			}else{
		               				$buttons .= $this->makeButton([
				               			'type' 		=> 'edit',
				               			'label' 	=> '<i class="fa fa-check text-primary"></i>',
				               			'tooltip' 	=> 'Buat Approval',
				               			'class' 	=> 'approve-svp button',
				               			'id'   		=> $record->id,
				               		]);
		               			}
		               		}else{
		               			$buttons .= $this->makeButton([
			               			'type' 		=> 'edit',
			               			'label' 	=> '<i class="fa fa-check text-primary"></i>',
			               			'tooltip' 	=> 'Buat Approval',
			               			'class' 	=> 'approve-svp button',
			               			'id'   		=> $record->id,
			               		]);
		               		}
		                	$buttons .= '&nbsp;';
		               		$buttons .= $this->makeButton([
			                    'type' 		=> 'edit',
			                    'label' 	=> '<i class="fa fa-print text-primary"></i>',
			                    'tooltip' 	=> 'Cetak',
			                    'class' 	=> 'cetak button',
			                    'id'   		=> $record->id,
			               ]);
		                }else{
		                	$buttons .= '';
		                }

	               	}elseif($record->status == 2){
	               		if($record->detailpenerima->where('user_id', $user->id)->count() > 0){
	               			if($record->group > 0){
		               			$cek = TinjauanDokumen::where('group', $record->group)->where('status', '<', 2)->get()->count();
		               			if($cek > 0){
		               				$buttons .= $this->makeButton([
				               			'type' 		=> 'edit',
				               			'label' 	=> '<i class="fa fa-upload text-primary"></i>',
				               			'tooltip' 	=> 'Upload',
				               			'class' 	=> 'upload-dokumen button disabled',
				               			'id'   		=> $record->id,
				               		]);
		               			}else{
		               				$buttons .= $this->makeButton([
				               			'type' 		=> 'edit',
				               			'label' 	=> '<i class="fa fa-upload text-primary"></i>',
				               			'tooltip' 	=> 'Upload',
				               			'class' 	=> 'upload-dokumen button',
				               			'id'   		=> $record->id,
				               		]);
		               			}
		               		}else{
		               			$buttons .= $this->makeButton([
			               			'type' 		=> 'edit',
			               			'label' 	=> '<i class="fa fa-upload text-primary"></i>',
			               			'tooltip' 	=> 'Upload',
			               			'class' 	=> 'upload-dokumen button',
			               			'id'   		=> $record->id,
			               		]);
		               		}
	               			$buttons .= '&nbsp;';
		               		$buttons .= $this->makeButton([
			                    'type' 		=> 'edit',
			                    'label' 	=> '<i class="fa fa-print text-primary"></i>',
			                    'tooltip' 	=> 'Cetak',
			                    'class' 	=> 'cetak button',
			                    'id'   		=> $record->id,
			               ]);
	               		}else{
	               			$buttons .= '';
	               		}
	               	}elseif($record->status == 3){
	               		if($record->detailpenerima->where('user_id', $user->id)->count() > 0){
	               			if($record->group > 0){
		               			$cek = TinjauanDokumen::where('group', $record->group)->where('status', '<', 3)->get()->count();
		               			if($cek > 0){
		               				$buttons .= $this->makeButton([
				               			'type' 		=> 'edit',
				               			'label' 	=> '<i class="fa fa-upload text-primary"></i>',
				               			'tooltip' 	=> 'Upload',
				               			'class' 	=> 'upload-dokumen button',
				               			'id'   		=> $record->id,
				               		]);
		               			}else{
		               				$buttons .= $this->makeButton([
				               			'type' 		=> 'edit',
				               			'label' 	=> '<i class="fa fa-upload text-primary"></i>',
				               			'tooltip' 	=> 'Upload',
				               			'class' 	=> 'upload-dokumen button',
				               			'id'   		=> $record->id,
				               		]);
		               			}
		               		}else{
		               			$buttons .= $this->makeButton([
			               			'type' 		=> 'edit',
			               			'label' 	=> '<i class="fa fa-upload text-primary"></i>',
			               			'tooltip' 	=> 'Upload',
			               			'class' 	=> 'upload-dokumen button',
			               			'id'   		=> $record->id,
			               		]);
		               		}
	               			$buttons .= '&nbsp;';
		               		$buttons .= $this->makeButton([
			                    'type' 		=> 'edit',
			                    'label' 	=> '<i class="fa fa-print text-primary"></i>',
			                    'tooltip' 	=> 'Cetak',
			                    'class' 	=> 'cetak button',
			                    'id'   		=> $record->id,
			               ]);
	               		}else{
	               			$buttons .= '';
	               		}
	               	}elseif($record->status == 4){
	               		if($user->hasRole(['auditor'])){
		               		$buttons .= '<input type="checkbox" class="view checks" name="checks[]" data-id="'.$record->id.'" value="'.$record->id.'" data-toggle="toggle" data-size="mini" data-on="Ya" data-off="Tidak" data-style="ios">';
		                }else{
		                	$buttons .= '';
		                }
	               	}else{

	               	}
	                return $buttons;
	            })
               ->rawColumns(['object_audit','tipe','kategori','detil','action','detil_program','status','detil_tinjauan','object_audit','nama','pic','penugasan'])
               ->make(true);
    }

    public function historis()
    {
    	$user = auth()->user();
    	if($user->hasJabatan(['SVP - Internal Audit','President Director','auditor'])){
        	$records = TinjauanDokumen::where('status', 5)->select('*');
	    }elseif($user->hasJabatan(['SVP','Project'])){
	    	$cari = checkuser($user->id);
    		$tipe_object = $cari[0];
    		$object_id = $cari[1];
    		$records = TinjauanDokumen::whereHas('penugasanaudit', function($x) use ($tipe_object, $object_id){
    										$x->whereHas('rencanadetail', function($y) use ($tipe_object, $object_id){
    											$y->where('tipe_object', $tipe_object)
    											  ->whereIn('object_id', $object_id);
    										});
    									})
    									->where('status', 5)
    									->select('*');
	    }else{
	    	$records = TinjauanDokumen::where('id', 0);
	    }
        if(!isset(request()->order[0]['column'])) {
	          $records->orderBy('created_at','desc');
	    }
	    if($tahun = request()->tahun) {
	    	$records->whereHas('penugasanaudit', function($b) use ($tahun){
	        	$b->whereHas('rencanadetail', function($u) use ($tahun){
	        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
	        			$z->where('tahun', 'like', '%' . $tahun . '%');
	        		});
	        	});
	    	});
	    }
	    if ($tipe = request()->tipe) {
	    	$records->whereHas('penugasanaudit', function($a) use ($tipe){
	        	$a->whereHas('rencanadetail', function($u) use ($tipe){
	        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
	        			if($tipe == 1){
			        		$z->where('tipe', 0);
		        		}else{
		        			$z->where('tipe', 1);
		        		}
	        		});
	        	});
	    	});
	    }
	    if($kategori = request()->kategori) {
	    	if($kategori == 1){
	    		$records->whereHas('penugasanaudit', function($a){
	        		$a->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 0);
		        	});
	    		});
	    	}elseif($kategori == 2){
	    		$records->whereHas('penugasanaudit', function($a){
	        		$a->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 1);
		        	});
	    		});
	    	}elseif($kategori == 3){
	    		$records->whereHas('penugasanaudit', function($a){
	        		$a->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 2);
		        	});
	    		});
	    	}elseif($kategori == 4){
	    		$records->whereHas('penugasanaudit', function($a){
	        		$a->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 3);
		        	});
	    		});
	    	}else{
	    		$records = $records;
	    	}
	    }

	    if($object_id = request()->object_id) {
	    	if(request()->kategori == 1){
	        	$kat = $kategori= 0;
	    	}elseif(request()->kategori == 2){
	    		$kat = $kategori= 1;
	    	}elseif(request()->kategori == 3){
	    		$kat = $kategori= 2;
	    	}elseif(request()->kategori == 4){
	    		$kat = $kategori= 3;
	    	}else{
	    		$kat = null;
	    	}
	    	$records->whereHas('penugasanaudit', function($a) use ($kat, $object_id){
	    		$a->whereHas('rencanadetail', function($u) use ($kat, $object_id){
	    			$u->where('tipe_object', $kat)->where('object_id', $object_id);
	    		});
			});
	    }

        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('tahun', function ($record) {
                   return $record->penugasanaudit->rencanadetail->rencanaaudit->tahun;
               })
               ->addColumn('tipe', function ($record) {
                    $tipe = '';
	                if($record->penugasanaudit->rencanadetail->tipe == 0){
	               	  $tipe = '<span class="label label-primary">Audit</span>';
	                }elseif($record->penugasanaudit->rencanadetail->tipe == 1){
	               	  $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
	                }else{
	                  $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
	                }

	                return $tipe;
               })
               ->addColumn('kategori', function ($record) {
                    $kategori = '';
                    if($record->penugasanaudit->rencanadetail->tipe == 0){
		                if($record->penugasanaudit->rencanadetail->tipe_object == 0){
		               	  $kategori = '<span class="label label-success">Business Unit (BU)</span>';
		                }elseif($record->penugasanaudit->rencanadetail->tipe_object == 1){
		               	  $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
		                }elseif($record->penugasanaudit->rencanadetail->tipe_object == 2){
		               	  $kategori = '<span class="label label-warning">Project</span>';
		                }else{
		               	  $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
		                }
	                }elseif($record->penugasanaudit->rencanadetail->tipe == 1){
	               	  	$kategori = $record->penugasanaudit->rencanadetail->konsultasi->nama;
	                }else{
	                    $kategori = $record->penugasanaudit->rencanadetail->lain->nama;
	                }
	                return $kategori;
               })
               ->addColumn('object_audit', function ($record) {
                   return object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
               })
               ->addColumn('rencana', function ($record) {
                   return $record->penugasanaudit->rencanadetail->rencana;
               })
               ->addColumn('nama', function ($record) {
               		if($record->penugasanaudit->rencanadetail->tipe_object == 2){
	               		return object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
	               	}else{
	               		return '';
	               	}
               })
               ->addColumn('no_ab', function ($record) {
	               return getAb($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
	           })
	           ->addColumn('detil_program', function ($record) use ($user) {
	               $buttons = '';
	               if($user->hasJabatan(['SVP - Internal Audit','President Director','auditor'])){
		               $buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
		                    'tooltip' 	=> 'Rencana Kerja',
		                    'class' 	=> 'download-rkia button',
		                    'id'   		=> $record->penugasanaudit->rencanadetail->rencanaaudit->id,
		               ]);
	               }else{
	               		$buttons='';
	               }
	               return $buttons;
	           })
	           ->addColumn('penugasan', function ($record) {
	               $buttons = '';
	               if($record->penugasanaudit->status == 4){
		               $buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
		                    'tooltip' 	=> 'Surat Penugasan',
		                    'class' 	=> 'download-penugasan button',
		                    'id'   		=> $record->penugasanaudit->id,
		               ]);
	               }else{
	               	    $buttons .='';
	               }
	               return $buttons;
	           })
	           ->addColumn('detil_tinjauan', function ($record) {
	               $buttons = '';
	               $buttons .= $this->makeButton([
	           			'type' 		=> 'edit',
	           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
	           			'tooltip' 	=> 'Tinjauan Dokumen',
	           			'class' 	=> 'detil-tinjauan button',
	           			'id'   		=> $record->id,
	           		]);

	               return $buttons;
	           })
               ->addColumn('nilai', function ($record) {
               		return number_format($record->nilai, 0, '.', '.');
           	   })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->editColumn('created_by', function ($record) {
                   return $record->creatorName();
               })
               ->editColumn('status', function ($record) {
	               return '<span class="label label-success">Completed</span>';
	           })
	           ->addColumn('action', function ($record) {
	               $buttons = '';
	               $buttons .= $this->makeButton([
	           			'type' 		=> 'edit',
	           			'label' 	=> '<i class="fa fa-print text-primary"></i>',
	           			'tooltip' 	=> 'Cetak',
	           			'class' 	=> 'cetak button',
	           			'id'   		=> $record->id,
	           		]);

	               return $buttons;
	           })
               ->rawColumns(['object_audit','tipe','kategori','detil','status','detil_program','detil_tinjauan','action','object_audit','nama','penugasan'])
               ->make(true);
    }

    public function index()
    {
    	$user = auth()->user();
    	if($user->hasJabatan(['SVP - Internal Audit','President Director','auditor'])){
        	$nbaru = PenugasanAudit::where('status_tinjauan', 0)->get()->count();
        	$nprog = TinjauanDokumen::whereIn('status', [1,2,3,4])->get()->count();
       	}elseif($user->hasJabatan(['SVP','Project'])){
	    	$cari 			= checkuser($user->id);
			$tipe_object 	= $cari[0];
			$object_id 		= $cari[1];
       		$nbaru = PenugasanAudit::where('status_tinjauan', 0)->whereIn('status', [1,2,3,4])
      									->whereHas('rencanadetail', function($y) use ($tipe_object, $object_id, $user){
											$y->where('tipe_object', $tipe_object)
											  ->whereIn('object_id', $object_id);
										})->orWhere('user_id', $user->id)->get()->count();
       		$nprog = TinjauanDokumen::whereHas('penugasanaudit', function($x) use ($tipe_object, $object_id, $user){
    										$x->whereHas('rencanadetail', function($y) use ($tipe_object, $object_id, $user){
    											$y->where('tipe_object', $tipe_object)
    											  ->whereIn('object_id', $object_id);
    										})->orWhere('user_id', $user->id);
    									})
    									->whereIn('status', [2,3,4])
    									->get()->count();
       	}else{
       		$nbaru = PenugasanAudit::where('id', 0)->get()->count();
       		$nprog = TinjauanDokumen::where('id', 0)->get()->count();
       	}
        return $this->render('modules.audit.persiapan.tinjauan-dokumen.index', [
        	'mockup' => true,
        	'structs' => $this->listStructs,
        	'nbaru' => $nbaru,
        	'nprog' => $nprog,
        ]);
    }

    public function store(TinjauanDokumenRequest $request)
    {
    	$user = auth()->user();
    	DB::beginTransaction();
    	try {
    		$maxValue = TinjauanDokumen::max('group');
    		foreach ($request->surat_penugasan_id as $key => $data) {
    			$cari = PenugasanAudit::find($data);
    			if($request->status == 0){
	    			$cari->status_tinjauan = 0;
	    			$status = 0;
    			}else{
    				$cari->status_tinjauan = 1;
    				$status = 1;
    			}
	    		$record = new TinjauanDokumen;
    			$cari->save();
	    		$record->penugasan_id = $data;
	    		$record->nomor 				= $request->nomor;
	    		$record->tempat 			= $request->tempat;
	    		$record->tanggal 			= $request->tanggal;
	    		$record->judul 				= $request->judul;
	    		$record->email 				= $request->email;
	    		$record->dateline 			= $request->dateline;
	    		$record->status 			= $status;
		        if(count($request->surat_penugasan_id) >= 2){
		        	$record->group  = ($maxValue+1);
	        	}else{
		        	$record->group  = 0;
	        	}
		        $record->save();

		        if($request->data_email){
			        $record->saveDetailEmail($request->data_email);
		        }

		        if($request->data_user){
			        $record->saveDetailPenerima($request->data_user);
		        }else{
		        	if($request->status == 0){
		        	}else{
			        	return response()->json([
			                'status' => 'false',
			                'message' => 'Mohon Inputkan Penerima (Internal)'
			            ],402);
		        	}
		        }

		        if($request->data_tembusan){
			        $record->saveDetailTembusan($request->data_tembusan);
		        }else{
		        	// if($request->status == 0){
		        	// }else{
			        // 	return response()->json([
		         //        	'status' => 'false',
		         //        	'message' => 'Mohon Inputkan Tembusan'
			        //     ],402);
		        	// }
		        }
		        $record->saveDetailDokumen($request->detail);

		        if($request->status == 1){
			        $update_notif = Notification::where('parent_id', $data)->where('modul', 'penugasan-audit')->where('stage', 4)->get();
			        foreach ($update_notif as $key => $vil) {
			        	$data_notif = Notification::find($vil->id);
				        $data_notif->status =0;
				        $data_notif->save();
			        }

					$userz = User::whereHas('roles', function($u){
											$u->where('name', 'svp-audit');
										})->get();
					foreach ($userz as $key => $vul) {
						$notif = new Notification;
					    $notif->user_id = $vul->id;
					    $notif->parent_id = $record->id;
					    $notif->url = route($this->routes.'.index');
					    $notif->modul = 'permintaan-dokumen';
					    $notif->stage = 0;
					    $notif->keterangan = 'Approval Permintaan Dokumen Tahun '.$record->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
					    $notif->status = 1;
					    $notif->save();
					}
    			}

		        $user = auth()->user();
		    	$log = new SessionLog;
			    $log->modul = 'Kegiatan Audit Persiapan';
			    $log->sub = 'Permintaan Dokumen';
			    $log->aktivitas = 'Membuat Permintaan Dokumen Tahun '.$record->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
			    $log->user_id = $user->id;
			    $log->save();

    		}
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function simpanData(TinjauanDokumenRequest $request)
    {
    	$user = auth()->user();
    	DB::beginTransaction();
    	try {
	    		$record = TinjauanDokumen::find($request->id);
	    		$record->nomor 				= $request->nomor;
	    		$record->tempat 			= $request->tempat;
	    		$record->tanggal 			= $request->tanggal;
	    		$record->judul 				= $request->judul;
	    		$record->email 				= $request->email;
	    		$record->dateline 			= $request->dateline;
	    		$record->status 			= 1;
		        $record->save();

		        $hapus_tembusan = TinjauanDokumenTembusan::where('tinjauan_id', $request->id)
	                        				->delete();
	            $hapus_dokumen  = TinjauanDokumenDetail::where('tinjauan_id', $request->id)
	                        				->delete();
	            $hapus_penerima = TinjauanDokumenPenerima::where('tinjauan_id', $request->id)
	                        				->delete();
	            $hapus_email = TinjauanDokumenEmail::where('tinjauan_id', $request->id)
	                        				->delete();
		        if($request->data_email){
			        $record->saveDetailEmail($request->data_email);
		        }

		        if($request->data_user){
			        $record->saveDetailPenerima($request->data_user);
		        }else{
		        	return response()->json([
		                'status' => 'false',
		                'message' => 'Mohon Inputkan Penerima (Internal)'
		            ],402);
		        }

		        if($request->data_tembusan){
			        $record->saveDetailTembusan($request->data_tembusan);
		        }else{
		        	// return response()->json([
		         //        'status' => 'false',
		         //        'message' => 'Mohon Inputkan Tembusan'
		         //    ],402);
		        }
		        $record->saveDetailDokumen($request->detail);
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function create()
    {
        return $this->render('modules.audit.persiapan.tinjauan-dokumen.create');
    }

    public function buat($id)
    {
    	$data_id = explode(',',$id);
    	$user_nama = [];
    	$user_id = [];
    	$penugasan = PenugasanAudit::whereIn('id', $data_id)->get();
    	foreach ($penugasan as $key => $value) {
    		$kategori = $value->rencanadetail->tipe_object;
    		$object = $value->rencanadetail->object_id;
    		$user = object_user($kategori, $object);
    		$user_id[] =$user[0];
    	}
    	$unique = array();
		foreach ($user_id as $piece) {
		    $unique = array_merge($unique, $piece);
		}
		$unique = array_unique($unique);
		$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Persiapan' => '#', 'Permintaan Dokumen' => '#', 'Buat' => '#']);
        return $this->render('modules.audit.persiapan.tinjauan-dokumen.create',[
        	'record' => $id,
        	'user_id' => $unique,
        	'data' =>$data_id,
        	'records' =>$penugasan,
        ]);
    }

    public function reject(RencanaAudit $id)
    {
        return $this->render('modules.audit.persiapan.tinjauan-dokumen.reject', [
        	'record' => $id,
        ]);
    }

    public function ubahData(TinjauanDokumen $id)
    {
    	if($id->group > 0){
    		$cek = TinjauanDokumen::where('group', $id->group)->pluck('penugasan_id')->toArray();
    	}else{
    		$cek[]= $id->penugasan_id;
    	}
    	$user_nama = [];
    	$user_id = [];
    	$penugasan = PenugasanAudit::whereIn('id', $cek)->get();
    	foreach ($penugasan as $key => $value) {
    		$kategori = $value->rencanadetail->tipe_object;
    		$object = $value->rencanadetail->object_id;
    		$user = object_user($kategori, $object);
    		$user_id[] =$user[0];
    	}
    	$unique = array();
		foreach ($user_id as $piece) {
		    $unique = array_merge($unique, $piece);
		}
		$unique = array_unique($unique);
		$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Persiapan' => '#', 'Permintaan Dokumen' => '#', 'Ubah' => '#']);
        return $this->render('modules.audit.persiapan.tinjauan-dokumen.ubah', [
        	'record' => $id,
        	'user_id' => $unique,
        	'records' =>$penugasan,
        ]);
    }

    public function uploadDokumen($id)
    {
    	$data = TinjauanDokumen::find($id);
    	$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Persiapan' => '#', 'Permintaan Dokumen' => '#', 'Upload Dokumen' => '#']);
        return $this->render('modules.audit.persiapan.tinjauan-dokumen.upload-dokumen',[
        	'record' => $data,
        ]);
    }

    public function saveReject(RejectRequest $request)
    {
        DB::beginTransaction();
    	try {
    		$records = TinjauanDokumen::find($request->id);
    		$records->status = 0;
    		$records->ket_svp = $request->ket_svp;
    		$records->save();

    		$penugasan = PenugasanAudit::find($records->penugasan_id);
    		$penugasan->status_tinjauan = 0;
    		$penugasan->save();
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function approveSvp($id)
    {
    	DB::beginTransaction();
        try {
        	$record = TinjauanDokumen::find($id);
        	if($record->group > 0){
        		$cek = TinjauanDokumen::where('group', $record->group)->get();
        		foreach ($cek as $key => $value) {
        			$data = TinjauanDokumen::find($value->id);
        			$data->status 			= 2;
			        $user = auth()->user();
			    	$log = new SessionLog;
				    $log->modul = 'Kegiatan Audit Persiapan';
				    $log->sub = 'Permintaan Dokumen';
				    $log->aktivitas = 'Menyetujui Permintaan Dokumen Tahun '.$data->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($data->penugasanaudit->rencanadetail->tipe_object, $data->penugasanaudit->rencanadetail->object_id);
				    $log->user_id = $user->id;
				    $log->save();

			        $data->save();

			        $update_notif = Notification::where('parent_id', $data->id)->where('modul', 'permintaan-dokumen')->where('stage', 0)->get();
			        foreach ($update_notif as $key => $vil) {
			        	$data_notif = Notification::find($vil->id);
				        $data_notif->status =0;
				        $data_notif->save();
			        }

			        foreach ($data->detailpenerima as $key => $vul) {
						$notif = new Notification;
					    $notif->user_id = $vul->user_id;
					    $notif->parent_id = $data->id;
					    $notif->url = route($this->routes.'.index');
					    $notif->modul = 'permintaan-dokumen';
					    $notif->stage = 1;
					    $notif->keterangan = 'Upload Permintaan Dokumen Tahun '.$data->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($data->penugasanaudit->rencanadetail->tipe_object, $data->penugasanaudit->rencanadetail->object_id);
					    $notif->status = 1;
					    $notif->save();
			        }
        		}
        	}else{
		        $record->status 			= 2;

		        $user = auth()->user();
		    	$log = new SessionLog;
			    $log->modul = 'Kegiatan Audit Persiapan';
			    $log->sub = 'Permintaan Dokumen';
			    $log->aktivitas = 'Menyetujui Permintaan Dokumen Tahun '.$record->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
			    $log->user_id = $user->id;
			    $log->save();

		        $record->save();

		        $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'permintaan-dokumen')->where('stage', 0)->get();
		        foreach ($update_notif as $key => $vil) {
		        	$data_notif = Notification::find($vil->id);
			        $data_notif->status =0;
			        $data_notif->save();
		        }

		        foreach ($record->detailpenerima as $key => $vul) {
					$notif = new Notification;
				    $notif->user_id = $vul->user_id;
				    $notif->parent_id = $record->id;
				    $notif->url = route($this->routes.'.index');
				    $notif->modul = 'permintaan-dokumen';
				    $notif->stage = 1;
				    $notif->keterangan = 'Upload Permintaan Dokumen Tahun '.$record->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
				    $notif->status = 1;
				    $notif->save();
		        }
        	}

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function kirim($id)
    {
    	$user = auth()->user();
    	DB::beginTransaction();
        try {
        	$record = TinjauanDokumen::find($id);
	        $record->status 			= 3;
	        $record->save();
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function approval($id)
    {
        return $this->render('modules.audit.persiapan.tinjauan-dokumen.approval',[
        	'record' => $id,
        ]);
    }

    public function uploadFinal(PenugasanAudit $id)
    {
        return $this->render('modules.audit.persiapan.tinjauan-dokumen.final', [
        	'record' => $id,
        ]);
    }

    public function detilProject(RencanaAuditDetail $id)
    {
    	$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Persiapan' => '#', 'Permintaan Dokumen' => '#', 'Detil Audit' => '#']);
        return $this->render('modules.audit.persiapan.tinjauan-dokumen.detil-project', ['record' => $id]);
    }

    public function detilProgram(PenugasanAudit $id)
    {
    	$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Persiapan' => '#', 'Permintaan Dokumen' => '#', 'Detil Penugasan Audit' => '#']);
        return $this->render('modules.audit.persiapan.tinjauan-dokumen.detil-penugasan', ['record' => $id]);
    }

    public function detilTinjauan(TinjauanDokumen $id)
    {
    	if($id->group > 0){
	    	$cari = TinjauanDokumen::where('group', $id->group)->get();
    	}else{
    		$cari[]= $id;
    	}
    	$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Persiapan' => '#', 'Permintaan Dokumen' => '#', 'Tinjauan Dokumen' => '#']);
        return $this->render('modules.audit.persiapan.tinjauan-dokumen.detil-tinjauan', [
        	'record' => $id,
        	'cek' => $cari,
        ]);
    }

    // public function detil(TinjauanDokumen $id)
    // {
    // 	$this->setBreadcrumb(['Audit' => '#', 'Persiapan' => '#', 'Permintaan Dokumen' => '#', 'Detil' => '#']);
    //     return $this->render('modules.audit.persiapan.tinjauan-dokumen.detil', [
    //     	'record' => $id,
    //     ]);
    // }

    public function draftSurat(PenugasanAudit $id)
    {
    	$daftar = PenugasanAudit::find($id);
    	$link =0;
    	if(file_exists(public_path('storage/'.$id->bukti)))
    	{
    		$link = asset('/storage/'.$id->bukti);
    	}
    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);
    }

    public function draftFinal(PenugasanAudit $id)
    {
        $check = Files::where('target_id', $id->id)->where('target_type', 'surat-penugasan-audit')->get();
        $files = [];
        $link =0;
        if($check->count() > 0)
        {
            $rename = [];
            foreach($check as $c)
            {
                if(file_exists(public_path('storage/'.$c->url)))
                {
                    $newname = '';
                    $splitname = explode("/",$c->url);
                    for ($i=0; $i < count($splitname) - 1 ; $i++) {
                        $newname .= $splitname[$i].'/';
                    }
                    $files[] = public_path('storage/'.$newname.$c->filename);
                    $rename[] = [
                        'old' => $c->url,
                        'new' => $c->filename,
                    ];
                }
            }
            $link =asset('/storage/'.$rename[0]['old']);
            return $this->render('pdf', [
	        	'mockup' => true,
	        	'link' => $link,
	        ]);
        }
    }

    public function dokumenPenugasan(TinjauanDokumenDetail $id)
    {
        $check = Files::where('target_id', $id->id)->where('target_type', 'dokumen-detail')->get();
        $files = [];
        $link =0;
        if($check->count() > 0)
        {
            $rename = [];
            foreach($check as $c)
            {
                if(file_exists(public_path('storage/'.$c->url)))
                {
                    $newname = '';
                    $splitname = explode("/",$c->url);
                    for ($i=0; $i < count($splitname) - 1 ; $i++) {
                        $newname .= $splitname[$i].'/';
                    }
                    $files[] = public_path('storage/'.$newname.$c->filename);
                    $rename[] = [
                        'old' => $c->url,
                        'new' => $c->filename,
                    ];
                }
            }
            $link =asset('/storage/'.$rename[0]['old']);
            return $this->render('pdf', [
	        	'mockup' => true,
	        	'link' => $link,
	        ]);
        }
    }

    public function update(TinjauanDokumenRequest $request)
    {
    	DB::beginTransaction();
        try {
    		$record = TinjauanDokumen::find($request->id);
        	if($record->group > 0){
        		$cek = TinjauanDokumen::where('group', $record->group)->get();
        		foreach ($cek as $key => $value) {
        			$data = TinjauanDokumen::find($value->id);
		        	$cari = PenugasanAudit::find($data->penugasan_id);
					if($request->status == 0){
		    			$cari->status_tinjauan = 0;
		    			$status = 0;
					}else{
						$cari->status_tinjauan = 1;
						$status = 1;
					}
        			$data->penugasan_id 		= $data->penugasan_id;
		    		$data->nomor 				= $request->nomor;
		    		$data->tempat 				= $request->tempat;
		    		$data->tanggal 				= $request->tanggal;
		    		$data->judul 				= $request->judul;
		    		$data->email 				= $request->email;
		    		$data->dateline 			= $request->dateline;
		    		$data->status 				= $status;
		    		$data->ket_svp 				= null;
			        $data->save();


					$cari->save();

			        $hapus_dokumen = TinjauanDokumenDetail::where('tinjauan_id', $value->id)
			                        				->delete();
			        $hapus_penerima = TinjauanDokumenPenerima::where('tinjauan_id', $value->id)
			                        				->delete();
			        $hapus_tembusan = TinjauanDokumenTembusan::where('tinjauan_id', $value->id)
			                        				->delete();
			        $hapus_email= TinjauanDokumenEmail::where('tinjauan_id', $value->id)
			                        				->delete();

			        if($request->data_email){
				        $data->saveDetailEmail($request->data_email);
			        }

			        if($request->data_user){
				        $data->saveDetailPenerima($request->data_user);
			        }else{
			        	if($request->status == 0){
			        	}else{
				        	return response()->json([
				                'status' => 'false',
				                'message' => 'Mohon Inputkan Penerima (Internal)'
				            ],402);
			        	}
			        }

			        if($request->data_tembusan){
				        $data->saveDetailTembusan($request->data_tembusan);
			        }else{
			        	// if($request->status == 0){
			        	// }else{
			        	// 	return response()->json([
			         //        	'status' => 'false',
			         //        	'message' => 'Mohon Inputkan Tembusan'
			         //    	],402);
			        	// }
			        }
			        $data->saveDetailDokumen($request->detail);

			        $user = auth()->user();
			    	$log = new SessionLog;
				    $log->modul = 'Kegiatan Audit Persiapan';
				    $log->sub = 'Permintaan Dokumen';
				    $log->aktivitas = 'Mengubah Permintaan Dokumen Tahun '.$data->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($data->penugasanaudit->rencanadetail->tipe_object, $data->penugasanaudit->rencanadetail->object_id);
				    $log->user_id = $user->id;
				    $log->save();
        		}
        	}else{
	        	$cari = PenugasanAudit::find($request->penugasan_id);
				if($request->status == 0){
	    			$cari->status_tinjauan = 0;
	    			$status = 0;
				}else{
					$cari->status_tinjauan = 1;
					$status = 1;
				}
	    		$record->penugasan_id 		= $request->penugasan_id;
	    		$record->nomor 				= $request->nomor;
	    		$record->tempat 			= $request->tempat;
	    		$record->tanggal 			= $request->tanggal;
	    		$record->judul 				= $request->judul;
	    		$record->email 				= $request->email;
	    		$record->dateline 			= $request->dateline;
	    		$record->status 			= $status;
	    		$record->ket_svp 				= null;
		        $record->save();

				$cari->save();

		        $hapus_dokumen = TinjauanDokumenDetail::where('tinjauan_id', $request->id)
		                        				->delete();
		        $hapus_penerima = TinjauanDokumenPenerima::where('tinjauan_id', $request->id)
		                        				->delete();
		        $hapus_tembusan = TinjauanDokumenTembusan::where('tinjauan_id', $request->id)
		                        				->delete();
		        $hapus_email= TinjauanDokumenEmail::where('tinjauan_id', $request->id)
		                        				->delete();

		        if($request->data_email){
			        $record->saveDetailEmail($request->data_email);
		        }

		        if($request->data_user){
			        $record->saveDetailPenerima($request->data_user);
		        }else{
		        	return response()->json([
		                'status' => 'false',
		                'message' => 'Mohon Inputkan Penerima (Internal)'
		            ],402);
		        }

		        if($request->data_tembusan){
			        $record->saveDetailTembusan($request->data_tembusan);
		        }else{
		        	// return response()->json([
		         //        'status' => 'false',
		         //        'message' => 'Mohon Inputkan Tembusan'
		         //    ],402);
		        }
		        $record->saveDetailDokumen($request->detail);

		        $user = auth()->user();
		    	$log = new SessionLog;
			    $log->modul = 'Kegiatan Audit Persiapan';
			    $log->sub = 'Permintaan Dokumen';
			    $log->aktivitas = 'Mengubah Permintaan Dokumen Tahun '.$record->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
			    $log->user_id = $user->id;
			    $log->save();
        	}

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function saveApproval(ApprovalRequest $request)
    {
    	$data_id = explode(',', $request->data);
    	DB::beginTransaction();
        try {
        	foreach ($data_id as $key => $value) {
        		if($request->approval == 2){
        			$record = TinjauanDokumen::find($value);
        			$record->ket_svp = $request->ket_svp;
	        		$record->status = 3;
        			$record->save();

        			$user = auth()->user();
			    	$log = new SessionLog;
				    $log->modul = 'Kegiatan Audit Persiapan';
				    $log->sub = 'Permintaan Dokumen';
				    $log->aktivitas = 'Menolak Permintaan Dokumen Tahun '.$record->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
				    $log->user_id = $user->id;
				    $log->save();

				    $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'permintaan-dokumen')->where('stage', 2)->get();
			        foreach ($update_notif as $key => $vil) {
			        	$data_notif = Notification::find($vil->id);
				        $data_notif->status =0;
				        $data_notif->save();
			        }

					foreach ($record->detailpenerima as $key => $vel) {
						$notif = new Notification;
					    $notif->user_id = $vel->user_id;
					    $notif->parent_id = $record->id;
					    $notif->url = route($this->routes.'.index');
					    $notif->modul = 'permintaan-dokumen';
					    $notif->stage = 1;
					    $notif->keterangan = 'Auditor Menolak Dokumen Tahun '.$record->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
					    $notif->status = 1;
					    $notif->save();
					}

        		}else{
        			$record = TinjauanDokumen::find($value);
        			$record->status = 5;
        			$record->save();

        			$user = auth()->user();
			    	$log = new SessionLog;
				    $log->modul = 'Kegiatan Audit Persiapan';
				    $log->sub = 'Permintaan Dokumen';
				    $log->aktivitas = 'Menyetujui Permintaan Dokumen Tahun '.$record->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
				    $log->user_id = $user->id;
				    $log->save();

				    $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'permintaan-dokumen')->where('stage', 2)->get();
			        foreach ($update_notif as $key => $vil) {
			        	$data_notif = Notification::find($vil->id);
				        $data_notif->status =0;
				        $data_notif->save();
			        }

					$userz = User::whereHas('roles', function($u){
									$u->where('name', 'auditor');
								})->get();
					foreach ($userz as $key => $vul) {
						$notif = new Notification;
					    $notif->user_id = $vul->id;
					    $notif->parent_id = $record->id;
					    $notif->url = route($this->routes.'.index');
					    $notif->modul = 'permintaan-dokumen';
					    $notif->stage = 3;
					    $notif->keterangan = 'Pembuatan Rencana Kerja Tahun '.$record->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
					    $notif->status = 1;
					    $notif->save();
					}
        		}
        	}
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function simpanDokumen(DokumenRequest $request)
    {
    	$user = auth()->user();
    	DB::beginTransaction();
        try {
        	$record 			= TinjauanDokumen::find($request->id);
        	if($request->status == 0){
	        	$record->status 	= 3;
        	}else{
        		$record->status 	= 4;
        	}
	        $record->save();
        	$record->uploadDetail($request->detail);

        	$update_notif = Notification::where('parent_id', $record->id)->where('modul', 'permintaan-dokumen')->where('stage', 1)->get();
	        foreach ($update_notif as $key => $vil) {
	        	$data_notif = Notification::find($vil->id);
		        $data_notif->status =0;
		        $data_notif->save();
	        }

			$userz = User::whereHas('roles', function($u){
									$u->where('name', 'auditor');
								})->get();
			foreach ($userz as $key => $vul) {
				$notif = new Notification;
			    $notif->user_id = $vul->id;
			    $notif->parent_id = $record->id;
			    $notif->url = route($this->routes.'.index');
			    $notif->modul = 'permintaan-dokumen';
			    $notif->stage = 2;
			    $notif->keterangan = 'Approval Dokumen Tahun '.$record->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
			    $notif->status = 1;
			    $notif->save();
			}
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function surat(PenugasanAudit $id)
    {
    	$daftar = PenugasanAudit::find($id);
    	$link =0;
    	if(file_exists(public_path('storage/'.$id->bukti)))
    	{
    		$link = asset('/storage/'.$id->bukti);
    	}
    	return $this->render('pdf', [
        	'mockup' => true,
        	'link' => $link,
        ]);
    }

    public function preview(PenugasanAudit $id)
    {
        $check = Files::where('target_id', $id->id)->where('target_type', 'tinjauan-dokumen')->get();
        $files = [];
        $link =0;
        if($check->count() > 0)
        {
            $rename = [];
            foreach($check as $c)
            {
                if(file_exists(public_path('storage/'.$c->url)))
                {
                    $newname = '';
                    $splitname = explode("/",$c->url);
                    for ($i=0; $i < count($splitname) - 1 ; $i++) {
                        $newname .= $splitname[$i].'/';
                    }
                    $files[] = public_path('storage/'.$newname.$c->filename);
                    $rename[] = [
                        'old' => $c->url,
                        'new' => $c->filename,
                    ];
                }
            }
            $link =asset('/storage/'.$rename[0]['old']);
            return $this->render('pdf', [
	        	'mockup' => true,
	        	'link' => $link,
	        ]);
        }
    }

    public function getFile($id)
    {
        $check = Files::where('target_id', $id)->where('target_type', 'dokumen-detail')->get();
        $files = [];
        $link =0;
        if($check->count() > 0)
        {
            $rename = [];
            foreach($check as $c)
            {
                if(file_exists(public_path('storage/'.$c->url)))
                {
                    $newname = '';
                    $splitname = explode("/",$c->url);
                    for ($i=0; $i < count($splitname) - 1 ; $i++) {
                        $newname .= $splitname[$i].'/';
                    }
                    $files[] = public_path('storage/'.$newname.$c->filename);
                    $rename[] = [
                        'old' => $c->url,
                        'new' => $c->filename,
                    ];
                }
            }
            $link =asset('/storage/'.$rename[0]['old']);
            return $link;
        }
    }

    public function destroy(ProgramAudit $program)
    {
        $program->delete();
        return response()->json([
            'success' => true
        ]);
    }

    public function cetak(Request $request, $id){

    	$record = TinjauanDokumen::find($id);
    	if($record->group > 0){
	    	$cari = TinjauanDokumen::where('group', $record->group)->get();
    	}else{
    		$cari[]= $record;
    	}
        $data = [
        	'records' => $record,
        	'cek' => $cari,
        ];
	    $pdf = PDF::loadView('modules.audit.persiapan.tinjauan-dokumen.cetak', $data);
        return $pdf->stream('Permintaan Dokumen.pdf',array("Attachment" => false));
 	}

 	public function cetakAudit(Request $request, $id){
    	$record = RencanaAudit::find($id);
        $data = [
        	'records' => $record,
        ];
	    $pdf = PDF::loadView('modules.audit.persiapan.tinjauan-dokumen.cetak-audit', $data)->setPaper('a4', 'landscape');
        return $pdf->stream('Audit Reguler.pdf',array("Attachment" => false));
 	}

 	public function cetakPenugasan(Request $request, $id){
    	$record = PenugasanAudit::find($id);
        $data = [
        	'records' => $record,
        ];

        $merge = new Merger();

	    $pdf = PDF::loadView('modules.audit.persiapan.tinjauan-dokumen.cetak-penugasan', $data);
	    $merge->addRaw($pdf->output());
	    $pdf2 = PDF::loadView('modules.audit.persiapan.tinjauan-dokumen.cetak-jadwal', $data);
	    $pdf2->setPaper('a4', 'landscape');
	    $merge->addRaw($pdf2->output());

        return new Response($merge->merge(), 200, array(
            'Content-Type' => 'application/pdf',
            'Content-Disposition' =>  'inline; filename="Penugasan Audit.pdf"',
        ));
 	}

 	public function downloadAudit($id)
  {
    	$daftar = RencanaAudit::find($id);

      if($daftar->dms == 1)
      {
          $this->downloadDms($daftar->bukti, $daftar->filename);
          $ext = pathinfo($daftar->filename);

          return response()->download(public_path('storage/temp-file/'.md5($filename.auth()->user()->id).'.'.$ext['extension']), $filename)->deleteFileAfterSend(true);
      }else{
      	if(file_exists(public_path('storage/'.$daftar->bukti)))
      	{
      		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
      	}
      }
  }

    public function downloadPenugasan($id)
    {
    	$daftar = PenugasanAudit::find($id);

      if($daftar->dms == 1)
      {
          $this->downloadDms($daftar->bukti, $daftar->filename);
          $ext = pathinfo($daftar->filename);

          return response()->download(public_path('storage/temp-file/'.md5($filename.auth()->user()->id).'.'.$ext['extension']), $filename)->deleteFileAfterSend(true);
      }else{
      	if(file_exists(public_path('storage/'.$daftar->bukti)))
      	{
      		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
      	}
      }
    }

    public function downloadDms($url, $filename)
    {
        $ext = pathinfo($filename);

        $otdsTicket = $this->otdsLogin();
        $otcsTicket = $this->otcsLogin($otdsTicket);

        Storage::disk('public')->put('temp-file/'.md5($filename.auth()->user()->id).'.'.$ext['extension'], $this->showfiledms($otcsTicket, $url));


    }

    public function otdsLogin()
    {

      $client = new Client;

      $response = $client->request('POST', 'https://otds.waskita.co.id:8443/otdsws/v1/authentication/credentials', [
          'json' => [
              'user_name' => 'adminIAO',
              'password' => 'Audit123@'
          ],
          'verify' => false,
      ]);

      $ticket = json_decode($response->getBody()->getContents())->ticket;

      return $ticket;

    }

    public function otcsLogin($ticket)
    {
      try {
        $client = new Client;

        $response = $client->request('POST', 'http://dmsinterface.waskita.co.id:8080/xecm/api/v1/auth/otdsticket', [
            'json' => [
                'otds_ticket' => $ticket
            ],
            'verify' => false,
        ]);

        return json_decode($response->getBody()->getContents())->ticket;

      }catch (\Exception $exception){
        return $this->otcsLogin($this->otdsLogin());
      }

    }

    public function showfiledms($ticket, $url)
    {
      try {
        $client = new Client;

        $response = $client->request('GET', $url, [
            'headers' => [
                'otcsticket' => $ticket
            ],
        ]);

        return $response->getBody()->getContents();

      }catch (\Exception $exception){
        return abort('404');
      }

    }
}
