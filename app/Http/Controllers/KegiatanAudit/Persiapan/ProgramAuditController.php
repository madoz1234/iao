<?php

namespace App\Http\Controllers\KegiatanAudit\Persiapan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditRequest;
use App\Http\Requests\KegiatanAudit\Persiapan\ProgramAudit\FokusAuditRequest;
use App\Http\Requests\KegiatanAudit\Persiapan\ProgramAudit\UbahTimRequest;
use App\Http\Requests\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditUploadRequest;
use App\Http\Requests\KegiatanAudit\Rencana\RejectRequest;
use Illuminate\Http\Response;
use iio\libmergepdf\Merger;

use App\Models\KegiatanAudit\Rencana\RencanaAudit;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditDetail;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditDetailAnggota;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditDetailJadwal;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditDetailKerja;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditDetailPelaksanaan;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAuditDetailPenyelesaian;
use App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumen;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\Auths\User;
use App\Models\SessionLog;
use App\Models\Notification;
use App\Exports\GenerateExport;
use GuzzleHttp\Client;

use DB;
use PDF;
use Excel;

class ProgramAuditController extends Controller
{
    protected $routes = 'kegiatan-audit.persiapan.program';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Kegiatan Audit' => '#', 'Persiapan' => '#', 'Program Audit' => '#']);
        $this->listStructs = [
        	'listStruct' => [
		        	[
		                'data' => 'num',
		                'name' => 'num',
		                'label' => '#',
		                'orderable' => false,
		                'searchable' => false,
		                'className' => 'text-center',
		                'width' => '20px',
		            ],
		            [
		                'data' => 'tahun',
		                'name' => 'tahun',
		                'label' => 'Tahun',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'rencana',
		                'name' => 'rencana',
		                'label' => 'Rencana Pelaksanaan',
		                // 'className' => 'text-center',
		                'width' => '250px',
		            ],
		            [
		                'data' => 'tipe',
		                'name' => 'tipe',
		                'label' => 'Tipe',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'kategori',
		                'name' => 'kategori',
		                'label' => 'Kategori',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'object_audit',
		                'name' => 'object_audit',
		                'label' => 'Objek Audit',
		                'className' => 'text-left',
		                'width' => '250px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'no_ab',
		                'name' => 'no_ab',
		                'label' => 'No AB',
		                // 'className' => 'text-center',
		                'width' => '150px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'detil',
		                'name' => 'detil',
		                'label' => 'Rencana Kerja',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'penugasan',
		                'name' => 'penugasan',
		                'label' => 'Surat Penugasan',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'tinjauan',
		                'name' => 'tinjauan',
		                'label' => 'Tinjauan Dokumen',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'tim',
		                'name' => 'tim',
		                'label' => 'Tim Auditor',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '180px',
		                'className' => 'text-left'
		            ],
		            [
		                'data' => 'status',
		                'name' => 'status',
		                'label' => 'Status',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '100px',
		                'className' => 'text-center'
		            ],
                	[
		                'data' => 'action',
		                'name' => 'action',
		                'label' => 'Aksi',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '80px',
		                'className' => 'text-center'
		            ],
        	],
        	'listStruct2' => [
		        	[
		                'data' => 'num',
		                'name' => 'num',
		                'label' => '#',
		                'orderable' => false,
		                'searchable' => false,
		                'className' => 'text-center',
		                'width' => '20px',
		            ],
		            [
		                'data' => 'tahun',
		                'name' => 'tahun',
		                'label' => 'Tahun',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'rencana',
		                'name' => 'rencana',
		                'label' => 'Rencana Pelaksanaan',
		                // 'className' => 'text-center',
		                'width' => '250px',
		            ],
		            [
		                'data' => 'tipe',
		                'name' => 'tipe',
		                'label' => 'Tipe',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'kategori',
		                'name' => 'kategori',
		                'label' => 'Kategori',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'object_audit',
		                'name' => 'object_audit',
		                'label' => 'Objek Audit',
		                'className' => 'text-left',
		                'width' => '250px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'no_ab',
		                'name' => 'no_ab',
		                'label' => 'No AB',
		                // 'className' => 'text-center',
		                'width' => '150px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'detil',
		                'name' => 'detil',
		                'label' => 'Rencana Kerja',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'penugasan',
		                'name' => 'penugasan',
		                'label' => 'Surat Penugasan',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'tinjauan',
		                'name' => 'tinjauan',
		                'label' => 'Tinjauan Dokumen',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'detil_program',
		                'name' => 'detil_program',
		                'label' => 'Program Audit',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'tim',
		                'name' => 'tim',
		                'label' => 'Tim Auditor',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '180px',
		                'className' => 'text-left'
		            ],
		            [
		                'data' => 'status',
		                'name' => 'status',
		                'label' => 'Status',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '100px',
		                'className' => 'text-center'
		            ],
                	[
		                'data' => 'action',
		                'name' => 'action',
		                'label' => 'Aksi',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '80px',
		                'className' => 'text-center'
		            ],
        	],
        	'listStruct3' => [
		        	[
		                'data' => 'num',
		                'name' => 'num',
		                'label' => '#',
		                'orderable' => false,
		                'searchable' => false,
		                'className' => 'text-center',
		                'width' => '20px',
		            ],
		            [
		                'data' => 'tahun',
		                'name' => 'tahun',
		                'label' => 'Tahun',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'rencana',
		                'name' => 'rencana',
		                'label' => 'Rencana Pelaksanaan',
		                // 'className' => 'text-center',
		                'width' => '250px',
		            ],
		            [
		                'data' => 'tipe',
		                'name' => 'tipe',
		                'label' => 'Tipe',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'kategori',
		                'name' => 'kategori',
		                'label' => 'Kategori',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'object_audit',
		                'name' => 'object_audit',
		                'label' => 'Objek Audit',
		                'className' => 'text-left',
		                'width' => '250px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'no_ab',
		                'name' => 'no_ab',
		                'label' => 'No AB',
		                // 'className' => 'text-center',
		                'width' => '150px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'detil',
		                'name' => 'detil',
		                'label' => 'Rencana Kerja',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'penugasan',
		                'name' => 'penugasan',
		                'label' => 'Surat Penugasan',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'tinjauan',
		                'name' => 'tinjauan',
		                'label' => 'Tinjauan Dokumen',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'detil_program',
		                'name' => 'detil_program',
		                'label' => 'Program Audit',
		                'className' => 'text-center',
		                'sortable' => true,
		                'width' => '100px',
		            ],
		            [
		                'data' => 'tim',
		                'name' => 'tim',
		                'label' => 'Tim Auditor',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '180px',
		                'className' => 'text-left'
		            ],
		            [
		                'data' => 'status',
		                'name' => 'status',
		                'label' => 'Status',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '100px',
		                'className' => 'text-center'
		            ],
		            [
		                'data' => 'action',
		                'name' => 'action',
		                'label' => 'Aksi',
		                'searchable' => false,
		                'sortable' => false,
		                'width' => '80px',
		                'className' => 'text-center'
		            ],
        	],
        ];
    }

    public function grid()
    {
    	$user = auth()->user();
    	if($user->hasJabatan(['SVP - Internal Audit','President Director','Secretary'])){
       	 	$records = PenugasanAudit::where('status_audit', 0)->whereIn('status', [1,2,3,4])
        						       ->select('*');

     		if(!isset(request()->order[0]['column'])) {
	              $records->orderBy('created_at','desc');
	        }
	        if($tahun = request()->tahun) {
	        	$records->whereHas('rencanadetail', function($u) use ($tahun){
	        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
	        			$z->where('tahun', 'like', '%' . $tahun . '%');
	        		});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('rencanadetail', function($u) use ($tipe){
	        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
	        			if($tipe == 1){
			        		$z->where('tipe', 0);
		        		}else{
		        			$z->where('tipe', 1);
		        		}
	        		});
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 0);
		        	});
	        	}elseif($kategori == 2){
	        		$records->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 1);
		        	});
	        	}elseif($kategori == 3){
	        		$records->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 2);
		        	});
	        	}elseif($kategori == 4){
	        		$records->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 3);
		        	});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
        		$records->whereHas('rencanadetail', function($u) use ($kat, $object_id){
        			$u->where('tipe_object', $kat)->where('object_id', $object_id);
        		});
	        }
      	}elseif($user->hasRole(['auditor'])){
       	 	$records = PenugasanAudit::where('status_audit', 0)
       	 							   ->whereIn('status', [1,2,3,4])
        						       ->select('*');

     		if(!isset(request()->order[0]['column'])) {
	              $records->orderBy('created_at','desc');
	        }
	        if($tahun = request()->tahun) {
	        	$records->whereHas('rencanadetail', function($u) use ($tahun){
	        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
	        			$z->where('tahun', 'like', '%' . $tahun . '%');
	        		});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('rencanadetail', function($u) use ($tipe){
	        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
	        			if($tipe == 1){
			        		$z->where('tipe', 0);
		        		}else{
		        			$z->where('tipe', 1);
		        		}
	        		});
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 0);
		        	});
	        	}elseif($kategori == 2){
	        		$records->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 1);
		        	});
	        	}elseif($kategori == 3){
	        		$records->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 2);
		        	});
	        	}elseif($kategori == 4){
	        		$records->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 3);
		        	});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
        		$records->whereHas('rencanadetail', function($u) use ($kat, $object_id){
        			$u->where('tipe_object', $kat)->where('object_id', $object_id);
        		});
	        }
      	}else{
      		$records = PenugasanAudit::where('id', 0);
      	}

        return DataTables::of($records->get())
        	   ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('tahun', function ($record) {
                   return $record->rencanadetail->rencanaaudit->tahun;
               })
               ->addColumn('tipe', function ($record) {
                    $tipe = '';
	                if($record->rencanadetail->tipe == 0){
	               	  $tipe = '<span class="label label-primary">Audit</span>';
	                }elseif($record->rencanadetail->tipe == 1){
	               	  $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
	                }else{
	                  $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
	                }

	                return $tipe;
               })
               ->addColumn('kategori', function ($record) {
                    $kategori = '';
                    if($record->rencanadetail->tipe == 0){
		                if($record->rencanadetail->tipe_object == 0){
		               	  $kategori = '<span class="label label-success">Business Unit (BU)</span>';
		                }elseif($record->rencanadetail->tipe_object == 1){
		               	  $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
		                }elseif($record->rencanadetail->tipe_object == 2){
		               	  $kategori = '<span class="label label-warning">Project</span>';
		                }else{
		               	  $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
		                }
	                }elseif($record->rencanadetail->tipe == 1){
	               	  	$kategori = $record->rencanadetail->konsultasi->nama;
	                }else{
	                    $kategori = $record->rencanadetail->lain->nama;
	                }
	                return $kategori;
               })
               ->addColumn('object_audit', function ($record) {
                   return object_audit($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
               })
               ->addColumn('no_ab', function ($record) {
	               return getAb($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
	           })
               ->addColumn('rencana', function ($record) {
                   return $record->rencanadetail->rencana;
               })
               ->addColumn('nama', function ($record) {
               		if($record->rencanadetail->tipe_object == 2){
	               		return object_audit($record->rencanadetail->tipe_object, $record->rencanadetail->object_id);
	               	}else{
	               		return '';
	               	}
               })
               ->editColumn('status', function ($record) {
	               	if($record->programaudit){
	               		if($record->programaudit->ket_svp){
	               			return '<span class="label label-warning">Ditolak</span>';
	               		}else{
		               		return '<span class="label label-info">Draft</span>';
	               		}
	               	}else{
		               return '<span class="label label-default">Baru</span>';
	               	}
	           })
               ->addColumn('detil', function ($record) use ($user) {
	               $buttons = '';
	               if($user->hasJabatan(['SVP - Internal Audit','President Director','auditor'])){
		               $buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
		                    'tooltip' 	=> 'Rencana Kerja',
		                    'class' 	=> 'download-rkia button',
		                    'id'   		=> $record->rencanadetail->rencanaaudit->id,
		               ]);
	               }else{
	               		$buttons='';
	               }
	               return $buttons;
	           })
	           ->addColumn('penugasan', function ($record) {
	               $buttons = '';
	               if($record->status == 4){
		               $buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
		                    'tooltip' 	=> 'Surat Penugasan',
		                    'class' 	=> 'download-penugasan button',
		                    'id'   		=> $record->id,
		               ]);
	               }else{
	               	    $buttons .='';
	               }
	               return $buttons;
	           })
	           ->addColumn('tim', function ($record) {
		           	$data = array();
	                $tim ='';
	           		$tim .='<ul class="list list-more1 list-unstyled" style="margin-top:10px;" data-display="3">';
	           		$tim .='<li class="item">'.$record->anggota->where('fungsi', 0)->where('data_id', $record->rencanadetail->id)->first()->user->name.'</li>';
	           		foreach ($record->anggota->whereIn('fungsi', [1,2,3]) as $key => $value) {
	           				$data[] = $value->user->name;
	           		}
	           		foreach (array_unique($data) as $key => $value) {
	           			$tim .='<li class="item">'.$value.'</li>';
	           		}
	           		$tim .='</ul>';
	               	return $tim;
	           })
	           ->addColumn('tinjauan', function ($record) {
	               $buttons = '';
	               if($record->tinjauandokumen){
		               $buttons .= $this->makeButton([
		           			'type' 		=> 'edit',
		           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
		           			'tooltip' 	=> 'Detil Tinjauan',
		           			'class' 	=> 'detil-tinjauan button',
		           			'id'   		=> $record->tinjauandokumen->id,
		           		]);
	               }else{
	               	 $buttons = '';
	               }
	               return $buttons;
	           })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->editColumn('created_by', function ($record) {
                   return $record->creatorName();
               })
               ->editColumn('action', function($record) use ($user){
               		if($user->hasRole(['auditor'])){
		               	if($record->programaudit){
		               		if($record->anggota->where('fungsi', 0)->first()->user_id == $user->id){
				                $checklist = $this->makeButton([
				           			'type' 		=> 'edit',
				           			'label' 	=> '<i class="fa fa-edit text-primary"></i>',
				           			'tooltip' 	=> 'Ubah Program Audit',
				           			'class' 	=> 'ubah button',
				           			'id'   		=> $record->programaudit->id,
				           		]);
		               		}else{
		               			$checklist = '';
		               		}
		               	}else{
		               		if($record->anggota->where('fungsi', 0)->first()->user_id == $user->id){
			               		$checklist = $this->makeButton([
				           			'type' 		=> 'edit',
				           			'label' 	=> '<i class="fa fa-plus text-primary"></i>',
				           			'tooltip' 	=> 'Buat Program Audit',
				           			'class' 	=> 'programaudit button',
				           			'id'   		=> $record->id,
				           		]);
		               		}else{
		               			$checklist = '';
		               		}
		               	}
		            }else{
		            	$checklist = '';
		            }
	                return $checklist;
	            })
               ->rawColumns(['object_audit','tipe','kategori','detil','action','penugasan','status','tinjauan','object_audit','nama','tim'])
               ->make(true);
    }

    public function onProgress()
    {
    	$user = auth()->user();
    	if($user->hasJabatan(['SVP - Internal Audit','President Director','Secretary'])){
    		$records = ProgramAudit::whereIn('status', [1,2,3])->select('*');
    		if(!isset(request()->order[0]['column'])) {
	              $records->orderBy('created_at','desc');
	        }
	        if($tahun = request()->tahun) {
	        	$records->whereHas('penugasanaudit', function($b) use ($tahun){
		        	$b->whereHas('rencanadetail', function($u) use ($tahun){
		        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
		        			$z->where('tahun', 'like', '%' . $tahun . '%');
		        		});
		        	});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('penugasanaudit', function($b) use ($tipe){
		        	$b->whereHas('rencanadetail', function($u) use ($tipe){
		        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
		        			if($tipe == 1){
				        		$z->where('tipe', 0);
			        		}else{
			        			$z->where('tipe', 1);
			        		}
		        		});
		        	});
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('penugasanaudit', function($b){
		        		$b->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 0);
			        	});
	        		});
	        	}elseif($kategori == 2){
	        		$records->whereHas('penugasanaudit', function($b){
		        		$b->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 1);
			        	});
	        		});
	        	}elseif($kategori == 3){
	        		$records->whereHas('penugasanaudit', function($b){
		        		$b->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 2);
			        	});
	        		});
	        	}elseif($kategori == 4){
	        		$records->whereHas('penugasanaudit', function($b){
		        		$b->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 3);
			        	});
	        		});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->whereHas('penugasanaudit', function($b) use ($kat, $object_id){
	        		$b->whereHas('rencanadetail', function($u) use ($kat, $object_id){
	        			$u->where('tipe_object', $kat)->where('object_id', $object_id);
	        		});
        		});
	        }
    	}elseif($user->hasRole(['auditor'])){
    		$records = ProgramAudit::whereIn('status', [1,2,3])
    								->whereHas('detailanggota', function($u) use ($user){
    									$u->where('user_id', $user->id);
    								})
    								->select('*');
    		if(!isset(request()->order[0]['column'])) {
	              $records->orderBy('created_at','desc');
	        }
	        if($tahun = request()->tahun) {
	        	$records->whereHas('penugasanaudit', function($b) use ($tahun){
		        	$b->whereHas('rencanadetail', function($u) use ($tahun){
		        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
		        			$z->where('tahun', 'like', '%' . $tahun . '%');
		        		});
		        	});
	        	});
	        }
	        if ($tipe = request()->tipe) {
	        	$records->whereHas('penugasanaudit', function($b) use ($tipe){
		        	$b->whereHas('rencanadetail', function($u) use ($tipe){
		        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
		        			if($tipe == 1){
				        		$z->where('tipe', 0);
			        		}else{
			        			$z->where('tipe', 1);
			        		}
		        		});
		        	});
	        	});
	        }
	        if($kategori = request()->kategori) {
	        	if($kategori == 1){
	        		$records->whereHas('penugasanaudit', function($b){
		        		$b->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 0);
			        	});
	        		});
	        	}elseif($kategori == 2){
	        		$records->whereHas('penugasanaudit', function($b){
		        		$b->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 1);
			        	});
	        		});
	        	}elseif($kategori == 3){
	        		$records->whereHas('penugasanaudit', function($b){
		        		$b->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 2);
			        	});
	        		});
	        	}elseif($kategori == 4){
	        		$records->whereHas('penugasanaudit', function($b){
		        		$b->whereHas('rencanadetail', function($u){
			        		$u->where('tipe_object', 3);
			        	});
	        		});
	        	}else{
	        		$records = $records;
	        	}
	        }

	        if($object_id = request()->object_id) {
	        	if(request()->kategori == 1){
		        	$kat = $kategori= 0;
	        	}elseif(request()->kategori == 2){
	        		$kat = $kategori= 1;
	        	}elseif(request()->kategori == 3){
	        		$kat = $kategori= 2;
	        	}elseif(request()->kategori == 4){
	        		$kat = $kategori= 3;
	        	}else{
	        		$kat = null;
	        	}
	        	$records->whereHas('penugasanaudit', function($b) use ($kat, $object_id){
	        		$b->whereHas('rencanadetail', function($u) use ($kat, $object_id){
	        			$u->where('tipe_object', $kat)->where('object_id', $object_id);
	        		});
        		});
	        }
    	}else{
    		$records = ProgramAudit::where('id', 0);
    	}

        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('tahun', function ($record) {
                   return $record->penugasanaudit->rencanadetail->rencanaaudit->tahun;
               })
               ->addColumn('tipe', function ($record) {
                    $tipe = '';
	                if($record->penugasanaudit->rencanadetail->tipe == 0){
	               	  $tipe = '<span class="label label-primary">Audit</span>';
	                }elseif($record->penugasanaudit->rencanadetail->tipe == 1){
	               	  $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
	                }else{
	                  $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
	                }

	                return $tipe;
               })
               ->addColumn('kategori', function ($record) {
                    $kategori = '';
                    if($record->penugasanaudit->rencanadetail->tipe == 0){
		                if($record->penugasanaudit->rencanadetail->tipe_object == 0){
		               	  $kategori = '<span class="label label-success">Business Unit (BU)</span>';
		                }elseif($record->penugasanaudit->rencanadetail->tipe_object == 1){
		               	  $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
		                }elseif($record->penugasanaudit->rencanadetail->tipe_object == 2){
		               	  $kategori = '<span class="label label-warning">Project</span>';
		                }else{
		               	  $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
		                }
	                }elseif($record->penugasanaudit->rencanadetail->tipe == 1){
	               	  	$kategori = $record->penugasanaudit->rencanadetail->konsultasi->nama;
	                }else{
	                    $kategori = $record->penugasanaudit->rencanadetail->lain->nama;
	                }
	                return $kategori;
               })
               ->addColumn('object_audit', function ($record) {
                   return object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
               })
               ->addColumn('rencana', function ($record) {
                   return $record->penugasanaudit->rencanadetail->rencana;
               })
               ->addColumn('no_ab', function ($record) {
	               return getAb($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
	           })
               ->addColumn('nama', function ($record) {
               		if($record->penugasanaudit->rencanadetail->tipe_object == 2){
	               		return object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
	               	}else{
	               		return '';
	               	}
               })
               ->editColumn('status', function ($record) {
	               	$cek='';
               		if($record->status == 1){
               			if($record->status_operasional < 2){
               				$cek .= '<span class="label label-warning">Fokus Audit Operasional <i class="fa fa-close" aria-hidden="true"></i></span><br>';
               			}else{
	               			$cek .= '<span class="label label-success">Fokus Audit Operasional <i class="fa fa-check" aria-hidden="true"></i></span><br>';
               			}

               			if($record->status_keuangan < 2){
               				$cek .= '<span class="label label-warning">Fokus Audit Keuangan <i class="fa fa-close" aria-hidden="true"></i></span><br>';
               			}else{
	               			$cek .= '<span class="label label-success">Fokus Audit Keuangan <i class="fa fa-check" aria-hidden="true"></i></span><br>';
               			}

               			if($record->status_sistem < 2){
               				$cek .= '<span class="label label-warning">Fokus Audit Sistem <i class="fa fa-close" aria-hidden="true"></i></span><br>';
               			}else{
	               			$cek .= '<span class="label label-success">Fokus Audit Sistem <i class="fa fa-check" aria-hidden="true"></i></span><br>';
               			}
               			return $cek;
               		}elseif($record->status == 2){
		               	return '<span class="label label-info">Waiting Approval SVP</span>';
	           		}elseif($record->status == 3){
	           			return '<span class="label label-info">Waiting Upload</span>';
	           		}else{
	           			return '<span class="label label-info">Completed</span>';
	           		}
	           })
               ->addColumn('detil', function ($record) use ($user) {
	               $buttons = '';
	               if($user->hasJabatan(['SVP - Internal Audit','President Director','auditor'])){
		               $buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
		                    'tooltip' 	=> 'Rencana Kerja',
		                    'class' 	=> 'download-rkia button',
		                    'id'   		=> $record->penugasanaudit->rencanadetail->rencanaaudit->id,
		               ]);
	               }else{
	               		$buttons='';
	               }
	               return $buttons;
	           })
	           ->addColumn('penugasan', function ($record) {
	               $buttons = '';
	               if($record->penugasanaudit->status == 4){
		               $buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
		                    'tooltip' 	=> 'Surat Penugasan',
		                    'class' 	=> 'download-penugasan button',
		                    'id'   		=> $record->penugasanaudit->id,
		               ]);
	               }else{
	               	    $buttons .='';
	               }
	               return $buttons;
	           })
	           ->addColumn('tinjauan', function ($record) {
	               $buttons = '';
	               if($record->penugasanaudit->tinjauandokumen){
		               $buttons .= $this->makeButton([
		           			'type' 		=> 'edit',
		           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
		           			'tooltip' 	=> 'Detil Tinjauan',
		           			'class' 	=> 'detil-tinjauan button',
		           			'id'   		=> $record->penugasanaudit->tinjauandokumen->id,
		           		]);
	               }

	               return $buttons;
	           })
	           ->addColumn('detil_program', function ($record) {
	               $buttons = '';
	               $buttons .= $this->makeButton([
	           			'type' 		=> 'edit',
	           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
	           			'tooltip' 	=> 'Detil Program Audit',
	           			'class' 	=> 'detil-program button',
	           			'id'   		=> $record->id,
	           		]);

	               return $buttons;
	           })
	           ->addColumn('tim', function ($record) {
		           	$data = array();
	                $tim ='';
	           		$tim .='<ul class="list list-more2 list-unstyled" style="margin-top:10px;" data-display="3">';
	           		$tim .='<li class="item">'.$record->penugasanaudit->anggota->where('fungsi', 0)->where('data_id', $record->penugasanaudit->rencanadetail->id)->first()->user->name.'</li>';
	           		foreach ($record->detailanggota->whereIn('fungsi', [1,2,3]) as $key => $value) {
	           				$data[] = $value->user->name;
	           		}
	           		foreach (array_unique($data) as $key => $value) {
	           			$tim .='<li class="item">'.$value.'</li>';
	           		}
	           		$tim .='</ul>';
	               	return $tim;
	           })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->editColumn('created_by', function ($record) {
                   return $record->creatorName();
               })
               ->editColumn('action', function($record) use ($user){
               		if($record->status == 1){
               			if($user->fungsi == 1){
               				if($record->status_operasional == 0){
		               			$checklist = $this->makeButton([
				                    'type' 		=> 'edit',
				                    'label' 	=> '<i class="fa fa-plus text-primary"></i>',
				                    'tooltip' 	=> 'Buat Fokus Audit',
				                    'class' 	=> 'buat-fokus button',
				                    'id'   		=> $record->id,
				                ]);
               				}elseif($record->status_operasional == 1){
               					$checklist = $this->makeButton([
				                    'type' 		=> 'edit',
				                    'label' 	=> '<i class="fa fa-edit text-primary"></i>',
				                    'tooltip' 	=> 'Ubah Fokus Audit',
				                    'class' 	=> 'ubah-fokus button',
				                    'id'   		=> $record->id,
				                ]);
               				}else{
               					$checklist = '';
               				}
               			}elseif($user->fungsi == 2){
               				if($record->status_keuangan == 0){
		               			$checklist = $this->makeButton([
				                    'type' 		=> 'edit',
				                    'label' 	=> '<i class="fa fa-plus text-primary"></i>',
				                    'tooltip' 	=> 'Buat Fokus Audit',
				                    'class' 	=> 'buat-fokus button',
				                    'id'   		=> $record->id,
				                ]);
               				}elseif($record->status_keuangan == 1){
               					$checklist = $this->makeButton([
				                    'type' 		=> 'edit',
				                    'label' 	=> '<i class="fa fa-edit text-primary"></i>',
				                    'tooltip' 	=> 'Ubah Fokus Audit',
				                    'class' 	=> 'ubah-fokus button',
				                    'id'   		=> $record->id,
				                ]);
               				}else{
               					$checklist = '';
               				}
               			}elseif($user->fungsi == 3){
               				if($record->status_sistem == 0){
		               			$checklist = $this->makeButton([
				                    'type' 		=> 'edit',
				                    'label' 	=> '<i class="fa fa-plus text-primary"></i>',
				                    'tooltip' 	=> 'Buat Fokus Audit',
				                    'class' 	=> 'buat-fokus button',
				                    'id'   		=> $record->id,
				                ]);
               				}elseif($record->status_sistem == 1){
               					$checklist = $this->makeButton([
				                    'type' 		=> 'edit',
				                    'label' 	=> '<i class="fa fa-edit text-primary"></i>',
				                    'tooltip' 	=> 'Ubah Fokus Audit',
				                    'class' 	=> 'ubah-fokus button',
				                    'id'   		=> $record->id,
				                ]);
               				}else{
               					$checklist = '';
               				}
               			}else{
               				$checklist = '';
               			}
               		}elseif($record->status == 2){
	               		if($user->hasJabatan(['SVP - Internal Audit'])){
			                $checklist = $this->makeButton([
			           			'type' 		=> 'edit',
		               			'label' 	=> '<i class="fa fa-check text-primary"></i>',
		               			'tooltip' 	=> 'Buat Approval',
		               			'class' 	=> 'approve-svp button',
		               			'id'   		=> $record->id,
			           		]);
			            }else{
			            	$checklist = '';
			            }
               		}elseif($record->status == 3){
               			if($user->hasJabatan(['Secretary'])){
			                $checklist = $this->makeButton([
			           			'type' 		=> 'edit',
		               			'label' 	=> '<i class="fa fa-upload text-primary"></i>',
		               			'tooltip' 	=> 'Upload',
		               			'class' 	=> 'upload button',
		               			'id'   		=> $record->id,
			           		]);
			            }else{
			            	$checklist = '';
			            }
               		}else{
               			$checklist = '';
               		}
	                return $checklist;
	            })
               ->rawColumns(['object_audit','tipe','kategori','detil','action','penugasan','status','tinjauan','detil_program','object_audit','nama','tim'])
               ->make(true);
    }

    public function historis()
    {
    	$user = auth()->user();
    	if($user->hasJabatan(['SVP - Internal Audit','President Director','Secretary'])){
        	$records = ProgramAudit::whereIn('status', [4,5])->select('*');
    	}elseif($user->hasRole(['auditor'])){
    		$records = ProgramAudit::whereIn('status', [4,5])->select('*');
    	}else{
    		$records = ProgramAudit::where('id', 0);
    	}
        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at','desc');
        }
        if($tahun = request()->tahun) {
        	$records->whereHas('penugasanaudit', function($b) use ($tahun){
	        	$b->whereHas('rencanadetail', function($u) use ($tahun){
	        		$u->whereHas('rencanaaudit', function($z) use ($tahun){
	        			$z->where('tahun', 'like', '%' . $tahun . '%');
	        		});
	        	});
        	});
        }
        if ($tipe = request()->tipe) {
        	$records->whereHas('penugasanaudit', function($b) use ($tipe){
	        	$b->whereHas('rencanadetail', function($u) use ($tipe){
	        		$u->whereHas('rencanaaudit', function($z) use ($tipe){
	        			if($tipe == 1){
			        		$z->where('tipe', 0);
		        		}else{
		        			$z->where('tipe', 1);
		        		}
	        		});
	        	});
        	});
        }
        if($kategori = request()->kategori) {
        	if($kategori == 1){
        		$records->whereHas('penugasanaudit', function($b){
	        		$b->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 0);
		        	});
        		});
        	}elseif($kategori == 2){
        		$records->whereHas('penugasanaudit', function($b){
	        		$b->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 1);
		        	});
        		});
        	}elseif($kategori == 3){
        		$records->whereHas('penugasanaudit', function($b){
	        		$b->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 2);
		        	});
        		});
        	}elseif($kategori == 4){
        		$records->whereHas('penugasanaudit', function($b){
	        		$b->whereHas('rencanadetail', function($u){
		        		$u->where('tipe_object', 3);
		        	});
        		});
        	}else{
        		$records = $records;
        	}
        }

        if($object_id = request()->object_id) {
        	if(request()->kategori == 1){
	        	$kat = $kategori= 0;
        	}elseif(request()->kategori == 2){
        		$kat = $kategori= 1;
        	}elseif(request()->kategori == 3){
        		$kat = $kategori= 2;
        	}elseif(request()->kategori == 4){
        		$kat = $kategori= 3;
        	}else{
        		$kat = null;
        	}
        	$records->whereHas('penugasanaudit', function($b) use ($kat, $object_id){
        		$b->whereHas('rencanadetail', function($u) use ($kat, $object_id){
        			$u->where('tipe_object', $kat)->where('object_id', $object_id);
        		});
    		});
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('tahun', function ($record) {
                   return $record->penugasanaudit->rencanadetail->rencanaaudit->tahun;
               })
               ->addColumn('tipe', function ($record) {
                    $tipe = '';
	                if($record->penugasanaudit->rencanadetail->tipe == 0){
	               	  $tipe = '<span class="label label-primary">Audit</span>';
	                }elseif($record->penugasanaudit->rencanadetail->tipe == 1){
	               	  $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
	                }else{
	                  $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
	                }

	                return $tipe;
               })
               ->addColumn('kategori', function ($record) {
                    $kategori = '';
                    if($record->penugasanaudit->rencanadetail->tipe == 0){
		                if($record->penugasanaudit->rencanadetail->tipe_object == 0){
		               	  $kategori = '<span class="label label-success">Business Unit (BU)</span>';
		                }elseif($record->penugasanaudit->rencanadetail->tipe_object == 1){
		               	  $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
		                }elseif($record->penugasanaudit->rencanadetail->tipe_object == 2){
		               	  $kategori = '<span class="label label-warning">Project</span>';
		                }else{
		               	  $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
		                }
	                }elseif($record->penugasanaudit->rencanadetail->tipe == 1){
	               	  	$kategori = $record->penugasanaudit->rencanadetail->konsultasi->nama;
	                }else{
	                    $kategori = $record->penugasanaudit->rencanadetail->lain->nama;
	                }
	                return $kategori;
               })
               ->addColumn('object_audit', function ($record) {
                   return object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
               })
               ->addColumn('rencana', function ($record) {
                   return $record->penugasanaudit->rencanadetail->rencana;
               })
               ->addColumn('no_ab', function ($record) {
	               return getAb($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
	           })
               ->addColumn('nama', function ($record) {
               		if($record->penugasanaudit->rencanadetail->tipe_object == 2){
	               		return object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
	               	}else{
	               		return '';
	               	}
               })
               ->addColumn('detil', function ($record) use ($user) {
	               $buttons = '';
	               if($user->hasJabatan(['SVP - Internal Audit','President Director','auditor'])){
		               $buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
		                    'tooltip' 	=> 'Rencana Kerja',
		                    'class' 	=> 'download-rkia button',
		                    'id'   		=> $record->penugasanaudit->rencanadetail->rencanaaudit->id,
		               ]);
	               }else{
	               		$buttons='';
	               }
	               return $buttons;
	           })
	           ->addColumn('penugasan', function ($record) {
	               $buttons = '';
	               if($record->penugasanaudit->status == 4){
		               $buttons .= $this->makeButton([
		                    'type' 		=> 'edit',
		                    'label' 	=> '<i class="fa fa-download text-primary"></i>',
		                    'tooltip' 	=> 'Surat Penugasan',
		                    'class' 	=> 'download-penugasan button',
		                    'id'   		=> $record->penugasanaudit->id,
		               ]);
	               }else{
	               	    $buttons .='';
	               }
	               return $buttons;
	           })
	           ->addColumn('tinjauan', function ($record) {
	               $buttons = '';
	               if($record->penugasanaudit->tinjauandokumen){
		               $buttons .= $this->makeButton([
		           			'type' 		=> 'edit',
		           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
		           			'tooltip' 	=> 'Detil Tinjauan',
		           			'class' 	=> 'detil-tinjauan button',
		           			'id'   		=> $record->penugasanaudit->tinjauandokumen->id,
		           		]);
	               }

	               return $buttons;
	           })
	           ->addColumn('detil_program', function ($record) {
	               $buttons = '';
	               $buttons .= $this->makeButton([
	           			'type' 		=> 'edit',
	           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
	           			'tooltip' 	=> 'Detil Program Audit',
	           			'class' 	=> 'detil-program button',
	           			'id'   		=> $record->id,
	           		]);

	               return $buttons;
	           })
	           ->addColumn('tim', function ($record) {
		           	$data = array();
	                $tim ='';
	           		$tim .='<ul class="list list-more3 list-unstyled" style="margin-top:10px;" data-display="3">';
	           		$tim .='<li class="item">'.$record->penugasanaudit->anggota->where('fungsi', 0)->where('data_id', $record->penugasanaudit->rencanadetail->id)->first()->user->name.'</li>';
	           		foreach ($record->detailanggota->whereIn('fungsi', [1,2,3]) as $key => $value) {
	           				$data[] = $value->user->name;
	           		}
	           		foreach (array_unique($data) as $key => $value) {
	           			$tim .='<li class="item">'.$value.'</li>';
	           		}
	           		$tim .='</ul>';
	               	return $tim;
	           })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->editColumn('created_by', function ($record) {
                   return $record->creatorName();
               })
               ->editColumn('status', function ($record) {
               		$op = count($record->detailkerja->where('bidang', 1)->where('fokus_audit_id', 1000));
               		$keu = count($record->detailkerja->where('bidang', 2)->where('fokus_audit_id', 1000));
               		$sis = count($record->detailkerja->where('bidang', 3)->where('fokus_audit_id', 1000));
               		$count = $op+$keu+$sis;
               		if($count < 3){
		               return '<span class="label label-success">Completed</span>';
               		}else{
               		   return '<span class="label label-danger">Stage Close</span>';
               		}
	           })
	           ->addColumn('action', function ($record) use ($user) {
	                $buttons = '';
	           		$buttons .= $this->makeButton([
	           			'type' 		=> 'edit',
	           			'label' 	=> '<i class="fa fa-file-excel-o text-primary"></i>',
	           			'tooltip' 	=> 'Jadwal Audit',
	           			'class' 	=> 'jadwal-audit button',
	           			'id'   		=> $record->id,
	           		]);
	           		$buttons .= '&nbsp;&nbsp;&nbsp;&nbsp;';
	           		if($record->opening){
		                $buttons .= $this->makeButton([
		           			'type' 		=> 'edit',
		           			'label' 	=> '<i class="fa fa-edit text-primary"></i>',
		           			'tooltip' 	=> 'Ubah Tim & Waktu Realisasi Audit',
		           			'class' 	=> 'ubah-tim button disabled',
		           			'id'   		=> $record->id,
		           		]);
	           		}else{
	           			if($record->user_id == $user->id){
		           			$buttons .= $this->makeButton([
			           			'type' 		=> 'edit',
			           			'label' 	=> '<i class="fa fa-edit text-primary"></i>',
			           			'tooltip' 	=> 'Ubah Tim',
			           			'class' 	=> 'ubah-tim-ketua button',
			           			'id'   		=> $record->id,
			           		]);
	           			}elseif($record->detailanggota->where('user_id', $user->id)->first()){
	           				$buttons .= $this->makeButton([
			           			'type' 		=> 'edit',
			           			'label' 	=> '<i class="fa fa-edit text-primary"></i>',
			           			'tooltip' 	=> 'Ubah Waktu Realisasi Audit',
			           			'class' 	=> 'ubah-tim-anggota button',
			           			'id'   		=> $record->id,
			           		]);
	           			}else{
	           				$buttons .= $this->makeButton([
			           			'type' 		=> 'edit',
			           			'label' 	=> '<i class="fa fa-edit text-primary"></i>',
			           			'tooltip' 	=> 'Ubah Tim & Waktu Realisasi Audit',
			           			'class' 	=> 'ubah-tim button disabled',
			           			'id'   		=> $record->id,
			           		]);
	           			}
	           		}

	               return $buttons;
	           })
               ->rawColumns(['object_audit','tipe','kategori','detil','status','penugasan','tinjauan','action','detil_program','object_audit','nama','tim'])
               ->make(true);
    }

    public function index()
    {
    	$user = auth()->user();
    	if($user->hasJabatan(['SVP - Internal Audit','President Director','Secretary'])){
    		$nbaru = PenugasanAudit::where('status_audit', 0)->whereIn('status', [1,2,3,4])
        						       ->get()->count();
    		$nprog = ProgramAudit::whereIn('status', [1,2,3])->get()->count();
    	}elseif($user->hasRole(['auditor'])){
    		$nbaru = PenugasanAudit::where('status_audit', 0)->whereIn('status', [1,2,3,4])
        						       ->get()->count();
    		$nprog = ProgramAudit::whereIn('status', [1,2,3])->get()->count();
    	}else{
    		$nbaru = PenugasanAudit::where('id', 0)->get()->count();
       		$nprog = ProgramAudit::where('id', 0)->get()->count();
    	}
    	return $this->render('modules.audit.persiapan.program-audit.index', [
        	'mockup' => true,
        	'structs' => $this->listStructs,
        	'nbaru' => $nbaru,
        	'nprog' => $nprog,
        ]);
    }

    public function buat(PenugasanAudit $id)
    {
    	$this->setBreadcrumb(['Persiapan Audit' => '#', 'Program Audit' => '#', 'Buat' => '#']);
        return $this->render('modules.audit.persiapan.program-audit.buat', [
        	'record' => $id,
        ]);
    }

    public function buatFokus(ProgramAudit $id)
    {
    	$user = auth()->user();
    	$this->setBreadcrumb(['Persiapan Audit' => '#', 'Program Audit' => '#', 'Buat Fokus Audit' => '#']);
        return $this->render('modules.audit.persiapan.program-audit.buat-fokus', [
        	'record' => $id,
        	'user' => $user->fungsi,
        ]);
    }

    public function ubah(ProgramAudit $id)
    {
    	$this->setBreadcrumb(['Persiapan Audit' => '#', 'Program Audit' => '#', 'Ubah' => '#']);
        return $this->render('modules.audit.persiapan.program-audit.ubah', [
        	'record' => $id,
        ]);
    }

    public function ubahFokus(ProgramAudit $id)
    {
    	$user = auth()->user();
    	$this->setBreadcrumb(['Persiapan Audit' => '#', 'Program Audit' => '#', 'Ubah Fokus Audit' => '#']);
        return $this->render('modules.audit.persiapan.program-audit.buat-fokus', [
        	'record' => $id,
        	'user' => $user->fungsi,
        ]);
    }

    public function detil(TinjauanDokumen $id)
    {
    	$cari = ProgramAudit::where('penugasan_id', $id->id)->first();
    	$this->setBreadcrumb(['Persiapan Audit' => '#', 'Program Audit' => '#', 'Detil' => '#']);
        return $this->render('modules.audit.persiapan.program-audit.detil', [
        	'record' => $cari,
        ]);
    }

    public function store(ProgramAuditRequest $request)
    {
    	$user = auth()->user();
    	DB::beginTransaction();
    	try {
    		$programaudit = new ProgramAudit;
    		$programaudit->penugasan_id   = $request->penugasan_id;
    		$programaudit->ruang_lingkup = $request->ruang_lingkup;
    		$programaudit->sasaran 		 = $request->sasaran;
    		$programaudit->user_id 		 = $user->id;
    		$cari = PenugasanAudit::find($request->penugasan_id);
    		if($request->status == 0){
    			$programaudit->status 		 	= 0;
	    		$cari->status_audit 			= 0;
    		}else{
    			$programaudit->status 		 	= 1;
	    		$cari->status_audit 			= 1;
    		}
    		$cari->save();
    		$programaudit->save();
	        $programaudit->saveDetail($request->detail);
	        $programaudit->saveAnggota($request->operasional, $request->keuangan, $request->sistem, $request->ketua);


	        if($request->status == 1){
	        	$update_notif = Notification::where('parent_id', $request->penugasan_id)->where('modul', 'permintaan-dokumen')->where('stage', 3)->get();
		        foreach ($update_notif as $key => $vil) {
		        	$data_notif = Notification::find($vil->id);
			        $data_notif->status =0;
			        $data_notif->save();
		        }

				$userz = User::whereHas('roles', function($u){
								$u->where('name', 'svp-audit');
							})->get();
				foreach ($userz as $key => $vul) {
					$notif = new Notification;
				    $notif->user_id = $vul->id;
				    $notif->parent_id = $programaudit->id;
				    $notif->url = route($this->routes.'.index');
				    $notif->modul = 'program-audit';
				    $notif->stage = 0;
				    $notif->keterangan = 'Approval Program Audit Tahun '.$programaudit->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($programaudit->penugasanaudit->rencanadetail->tipe_object, $programaudit->penugasanaudit->rencanadetail->object_id);
				    $notif->status = 1;
				    $notif->save();
				}
	        }

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Kegiatan Audit Persiapan';
		    $log->sub = 'Program Audit';
		    $log->aktivitas = 'Membuat Program Audit Tahun '.$programaudit->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($programaudit->penugasanaudit->rencanadetail->tipe_object, $programaudit->penugasanaudit->rencanadetail->object_id);
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function update(ProgramAuditRequest $request)
    {
    	$user = auth()->user();
    	DB::beginTransaction();
    	try {
    		$programaudit = ProgramAudit::find($request->id);
    		$programaudit->penugasan_id   = $request->penugasan_id;
    		$programaudit->ruang_lingkup = $request->ruang_lingkup;
    		$programaudit->sasaran 		 = $request->sasaran;
    		$programaudit->user_id 		 = $user->id;
    		$cari = PenugasanAudit::find($request->penugasan_id);
    		if($request->status == 0){
    			$programaudit->status 		 	= 0;
	    		$cari->status_audit 			= 0;
    		}else{
    			$programaudit->status 		 	= 1;
	    		$cari->status_audit 			= 1;
    		}
    		$cari->save();
    		$programaudit->save();

    		$hapus_anggota 		= ProgramAuditDetailAnggota::where('program_id', $request->id)
	                        							->delete();
	        $hapus_jadwal 		= ProgramAuditDetailJadwal::where('program_id', $request->id)
	                        							->delete();
	        // $hapus_kerja 		= ProgramAuditDetailKerja::where('program_id', $request->id)
	        //                 							->delete();
	        $hapus_selesai 		= ProgramAuditDetailPenyelesaian::where('program_id', $request->id)
	                        							->delete();
	        $hapus_pelaksanaan 	= ProgramAuditDetailPelaksanaan::where('program_id', $request->id)
	                        							->delete();

	        $programaudit->saveDetail($request->detail);
	        $programaudit->saveAnggotas($request->operasional, $request->keuangan, $request->sistem);

	        $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Kegiatan Audit Persiapan';
		    $log->sub = 'Program Audit';
		    $log->aktivitas = 'Menyetujui Program Audit Tahun '.$programaudit->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($programaudit->penugasanaudit->rencanadetail->tipe_object, $programaudit->penugasanaudit->rencanadetail->object_id);
		    $log->user_id = $user->id;
		    $log->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function updateFokus(FokusAuditRequest $request)
    {
    	$user = auth()->user();
    	DB::beginTransaction();
    	try {
    		$programaudit = ProgramAudit::find($request->id);
    		if($request->status > 0){
	    		if($request->tipe == 1){
	    			$hapus_kerja 		= ProgramAuditDetailKerja::where('program_id', $request->id)
	    														   ->where('bidang', 1)
	                        									   ->delete();
	    			$programaudit->status_operasional = 2;
	    			if($programaudit->status_keuangan == 2 && $programaudit->status_sistem == 2){
	    				$programaudit->status 			= 2;
	    				$programaudit->ket_svp 			= null;
	    			}
	    		}elseif($request->tipe == 2){
	    			$hapus_kerja 		= ProgramAuditDetailKerja::where('program_id', $request->id)
	    														   ->where('bidang', 2)
	                        									   ->delete();
	    			$programaudit->status_keuangan = 2;
	    			if($programaudit->status_operasional == 2 && $programaudit->status_sistem == 2){
	    				$programaudit->status 			= 2;
	    				$programaudit->ket_svp 			= null;
	    			}
	    		}else{
	    			$hapus_kerja 		= ProgramAuditDetailKerja::where('program_id', $request->id)
	    														   ->where('bidang', 3)
	                        									   ->delete();

	    			$programaudit->status_sistem = 2;
	    			if($programaudit->status_operasional == 2 && $programaudit->status_keuangan == 2){
	    				$programaudit->status 			= 2;
			    		$programaudit->ket_svp 			= null;
	    			}
	    		}
	    		$programaudit->save();
	    		$programaudit->saveDetailFokus($request->detail);
    		}else{
    			if($request->tipe == 1){
    				$hapus_kerja 		= ProgramAuditDetailKerja::where('program_id', $request->id)
	    														   ->where('bidang', 1)
	                        									   ->delete();

	    			$programaudit->status_operasional = 1;
	    		}elseif($request->tipe == 2){
	    			$hapus_kerja 		= ProgramAuditDetailKerja::where('program_id', $request->id)
	    														   ->where('bidang', 2)
	                        									   ->delete();
	    			$programaudit->status_keuangan = 1;
	    		}else{
	    			$hapus_kerja 		= ProgramAuditDetailKerja::where('program_id', $request->id)
	    														   ->where('bidang', 3)
	                        									   ->delete();

	    			$programaudit->status_sistem = 1;
	    		}
	    		$programaudit->save();
		        $programaudit->saveDetailFokus($request->detail);
    		}
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function approveSvp($id)
    {
    	DB::beginTransaction();
        try {
        	$record = ProgramAudit::find($id);
		    $record->status 			= 3;

		    $user = auth()->user();
	    	$log = new SessionLog;
		    $log->modul = 'Kegiatan Audit Persiapan';
		    $log->sub = 'Program Audit';
		    $log->aktivitas = 'Menyetujui Program Audit Tahun '.$record->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
		    $log->user_id = $user->id;
		    $log->save();

	        $record->save();

	        $update_notif = Notification::where('parent_id', $record->id)->where('modul', 'program-audit')->where('stage', 0)->get();
	        foreach ($update_notif as $key => $vil) {
	        	$data_notif = Notification::find($vil->id);
		        $data_notif->status =0;
		        $data_notif->save();
	        }

			$notif = new Notification;
		    $notif->user_id = $record->user_id;
		    $notif->parent_id = $record->id;
		    $notif->url = route($this->routes.'.index');
		    $notif->modul = 'program-audit';
		    $notif->stage = 1;
		    $notif->keterangan = 'Opening Meeting Tahun '.$record->penugasanaudit->rencanadetail->rencanaaudit->tahun.' dengan Objek Audit '.object_audit($record->penugasanaudit->rencanadetail->tipe_object, $record->penugasanaudit->rencanadetail->object_id);
		    $notif->status = 1;
		    $notif->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function detilProject(RencanaAuditDetail $id)
    {
        return $this->render('modules.audit.persiapan.program-audit.detil-project', ['record' => $id]);
    }

    public function detilPenugasan(PenugasanAudit $id)
    {
        return $this->render('modules.audit.persiapan.program-audit.detil-penugasan', ['record' => $id]);
    }

    public function detilTinjauan(TinjauanDokumen $id)
    {
    	$cari[]= $id;
        return $this->render('modules.audit.persiapan.program-audit.detil-tinjauan', [
        	'record' => $id,
        	'cek' => $cari,
        ]);
    }

    public function detilProgram(ProgramAudit $id)
    {
        return $this->render('modules.audit.persiapan.program-audit.detil', ['record' => $id]);
    }

    public function reject(ProgramAudit $id)
    {
        return $this->render('modules.audit.persiapan.program-audit.reject', [
        	'record' => $id,
        ]);
    }

    public function saveReject(RejectRequest $request)
    {
        DB::beginTransaction();
    	try {
    		$records = ProgramAudit::find($request->id);
    		$records->status = 0;
    		$records->ket_svp = $request->ket_svp;
    		$records->status_operasional = 1;
    		$records->status_keuangan = 1;
    		$records->status_sistem = 1;
    		$records->save();

    		$penugasan = PenugasanAudit::find($records->penugasan_id);
    		$penugasan->status_audit = 0;
    		$penugasan->save();
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(ProgramAudit $program)
    {
        $program->delete();
        return response()->json([
            'success' => true
        ]);
    }

    public function cetakAudit(Request $request, $id){
    	$record = RencanaAudit::find($id);
        $data = [
        	'records' => $record,
        ];
	    $pdf = PDF::loadView('modules.audit.persiapan.program-audit.cetak-audit', $data)->setPaper('a4', 'landscape');
        return $pdf->stream('Rencana Audit.pdf',array("Attachment" => false));
 	}

 	public function cetakPenugasan(Request $request, $id){
    	$record = PenugasanAudit::find($id);
        $data = [
        	'records' => $record,
        ];

        $merge = new Merger();

	    $pdf = PDF::loadView('modules.audit.persiapan.program-audit.cetak-penugasan', $data);
	    $merge->addRaw($pdf->output());
	    $pdf2 = PDF::loadView('modules.audit.persiapan.program-audit.cetak-jadwal', $data);
	    $pdf2->setPaper('a4', 'landscape');
	    $merge->addRaw($pdf2->output());

        return new Response($merge->merge(), 200, array(
            'Content-Type' => 'application/pdf',
            'Content-Disposition' =>  'inline; filename="Penugasan Audit.pdf"',
        ));
 	}

 	public function downloadAudit($id)
  {
    	$daftar = RencanaAudit::find($id);

      if($daftar->dms == 1)
      {
          $this->downloadDms($daftar->bukti, $daftar->filename);
          $ext = pathinfo($daftar->filename);

          return response()->download(public_path('storage/temp-file/'.md5($filename.auth()->user()->id).'.'.$ext['extension']), $filename)->deleteFileAfterSend(true);
      }else{
      	if(file_exists(public_path('storage/'.$daftar->bukti)))
      	{
      		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
      	}

        return abort('404');
      }
  }

    public function downloadPenugasan($id)
    {
    	$daftar = PenugasanAudit::find($id);

      if($daftar->dms == 1)
      {
          $this->downloadDms($daftar->bukti, $daftar->filename);
          $ext = pathinfo($daftar->filename);

          return response()->download(public_path('storage/temp-file/'.md5($filename.auth()->user()->id).'.'.$ext['extension']), $filename)->deleteFileAfterSend(true);
      }else{
      	if(file_exists(public_path('storage/'.$daftar->bukti)))
      	{
      		return response()->download(public_path('storage/'.$daftar->bukti), $daftar->filename);
      	}

        return abort('404');
      }
    }

    public function cetak(Request $request, $id){
    	$record = ProgramAudit::find($id);
        $data = [
        	'records' => $record,
        ];

	    $pdf = PDF::loadView('modules.audit.persiapan.program-audit.cetak', $data);
	    // $merge->addRaw($pdf->output());
	    // $pdf2 = PDF::loadView('modules.audit.persiapan.program-audit.cetak-jadwal-audit', $data);
	    // $pdf2->setPaper('a4', 'landscape');
	    // $merge->addRaw($pdf2->output());

	    return $pdf->stream('Program Audit.pdf',array("Attachment" => false));
        // return new Response($merge->merge(), 200, array(
        //     'Content-Type' => 'application/pdf',
        //     'Content-Disposition' =>  'inline; filename="Program Audit.pdf"',
        // ));
 	}

 	public function upload(ProgramAudit $id)
    {
        return $this->render('modules.audit.persiapan.program-audit.upload', [
        	'record' => $id,
        ]);
    }

    public function ubahTim(ProgramAudit $id)
    {
    	$this->setBreadcrumb(['Persiapan Audit' => '#', 'Program Audit' => '#', 'Ubah Tim' => '#']);
        return $this->render('modules.audit.persiapan.program-audit.ubah-tim', [
        	'record' => $id,
        ]);
    }

    public function ubahRealisasi(ProgramAudit $id)
    {
    	$user = auth()->user();
    	$this->setBreadcrumb(['Persiapan Audit' => '#', 'Program Audit' => '#', 'Ubah Waktu Realisasi Audit' => '#']);
        return $this->render('modules.audit.persiapan.program-audit.ubah-realisasi', [
        	'record' => $id,
        	'user' => $user->fungsi,
        ]);
    }

    public function saveUpload(ProgramAuditUploadRequest $request)
    {
        DB::beginTransaction();
    	try {
    		$record 						= ProgramAudit::find($request->id);
    		$op = count($record->detailkerja->where('bidang', 1)->where('fokus_audit_id', 1000));
       		$keu = count($record->detailkerja->where('bidang', 2)->where('fokus_audit_id', 1000));
       		$sis = count($record->detailkerja->where('bidang', 3)->where('fokus_audit_id', 1000));
       		$count = $op+$keu+$sis;
       		if($count >= 3){
       			$record->status_kka = 10;
       		}
    		if($lampiran = $request->lampiran){
        		$path_lampiran = $lampiran->store('uploads/programaudit', 'public');
        		$record->lampiranname = $lampiran->getClientOriginalName();
            $record->lampiran = $path_lampiran;
          }
        $record->dms_update = 1;
    		$record->status = 4;
    		$record->save();
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function saveUbahTim(UbahTimRequest $request)
    {
        DB::beginTransaction();
    	try {
    		$programaudit = ProgramAudit::find($request->id);
    		$programaudit->save();

    		$hapus_anggota 		= ProgramAuditDetailAnggota::where('program_id', $programaudit->id)
	                        							->delete();
	        $programaudit->saveAnggota($request->operasional, $request->keuangan, $request->sistem);
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function saveUbahRealisasi(Request $request)
    {
        DB::beginTransaction();
    	try {
    		$programaudit = ProgramAudit::find($request->id);
    		$programaudit->save();
	        $programaudit->ubahRealisasi($request->detail);
	    	DB::commit();
	        return response([
	          'status' => true
	        ]);
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function jadwalAudit(Request $request, $id)
    {
    	$record 	= ProgramAudit::find($id);
        $tittle     = 'Jadwal Audit';
        $tgl     	= 'Jadwal Audit';
        $view       = "modules.audit.persiapan.program-audit.export";
        $data_array = [
            'tittle'   => $tittle,
            'data'     => $record,
        ];

        return Excel::download(new GenerateExport($view, $data_array), $tittle.'.xls');
    }

    public function downloadDms($url, $filename)
    {
        $ext = pathinfo($filename);

        $otdsTicket = $this->otdsLogin();
        $otcsTicket = $this->otcsLogin($otdsTicket);

        Storage::disk('public')->put('temp-file/'.md5($filename.auth()->user()->id).'.'.$ext['extension'], $this->showfiledms($otcsTicket, $url));


    }

    public function otdsLogin()
    {

      $client = new Client;

      $response = $client->request('POST', 'https://otds.waskita.co.id:8443/otdsws/v1/authentication/credentials', [
          'json' => [
              'user_name' => 'adminIAO',
              'password' => 'Audit123@'
          ],
          'verify' => false,
      ]);

      $ticket = json_decode($response->getBody()->getContents())->ticket;

      return $ticket;

    }

    public function otcsLogin($ticket)
    {
      try {
        $client = new Client;

        $response = $client->request('POST', 'http://dmsinterface.waskita.co.id:8080/xecm/api/v1/auth/otdsticket', [
            'json' => [
                'otds_ticket' => $ticket
            ],
            'verify' => false,
        ]);

        return json_decode($response->getBody()->getContents())->ticket;

      }catch (\Exception $exception){
        return $this->otcsLogin($this->otdsLogin());
      }

    }

    public function showfiledms($ticket, $url)
    {
      try {
        $client = new Client;

        $response = $client->request('GET', $url, [
            'headers' => [
                'otcsticket' => $ticket
            ],
        ]);

        return $response->getBody()->getContents();

      }catch (\Exception $exception){
        return abort('404');
      }

    }
}
