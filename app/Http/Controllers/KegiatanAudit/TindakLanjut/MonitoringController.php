<?php

namespace App\Http\Controllers\KegiatanAudit\TindakLanjut;

use App\Models\Auths\User;
use App\Models\Files;
use App\Models\Notification;
use App\Http\Controllers\Controller;
use App\Http\Requests\KegiatanAudit\TindakLanjut\TLRequest;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka;
use App\Models\KegiatanAudit\Pelaporan\LHA;
use App\Models\KegiatanAudit\TindakLanjut\RegisterTL;
use App\Models\KegiatanAudit\TindakLanjut\RegisterTLDetail;
use App\Models\KegiatanAudit\TindakLanjut\RegisterTLDetailLampiran;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaDetail;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaDetailRekomendasi;
use App\Models\SessionLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;
use iio\libmergepdf\Merger;
use App\Libraries\CoreSn;


use Carbon;
use Storage;
use DB;
use PDF;
use Madzipper;
use Yajra\DataTables\Facades\DataTables;

class MonitoringController extends Controller
{	
    protected $routes = 'kegiatan-audit.tindak-lanjut.monitoring-tl';

    public function __construct()
    {	
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Kegiatan Audit' => '#', 'Tindak Lanjut' => '#', 'Monitoring Tindak Lanjut' => '#']);
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            [
                'data' => 'tahun',
                'name' => 'tahun',
                'label' => 'Tahun',
                'className' => 'text-center',
                'width' => '200px',
                'sortable' => true,
            ],
            [
                'data' => 'rencana',
                'name' => 'rencana',
                'label' => 'Waktu Pelaksanaan',
                'className' => 'text-center',
                'width' => '300px',
            ],
            [
                'data' => 'tipe',
                'name' => 'tipe',
                'label' => 'Tipe',
                'className' => 'text-center',
                'width' => '200px',
                'sortable' => true,
            ],
            [
                'data' => 'kategori',
                'name' => 'kategori',
                'label' => 'Kategori',
                'className' => 'text-center',
                'width' => '200px',
                'sortable' => true,
            ],
            [
                'data' => 'object_audit',
                'name' => 'object_audit',
                'label' => 'Objek Audit',
                'className' => 'text-left',
                'width' => '250px',
                'sortable' => true,
            ],
            [
                'data' => 'no_ab',
                'name' => 'no_ab',
                'label' => 'No AB',
                'className' => 'text-center',
                'width' => '150px',
                'sortable' => true,
            ],
            [
                'data' => 'kka',
                'name' => 'kka',
                'label' => 'KKA',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'surat',
                'name' => 'surat',
                'label' => 'Surat Disposisi Tindak Lanjut',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'tim',
                'name' => 'tim',
                'label' => 'Tim Auditor',
                'searchable' => false,
                'sortable' => false,
                'width' => '180px',
                'className' => 'text-left'
            ],
            [
                'data' => 'pic',
                'name' => 'pic',
                'label' => 'Auditee',
                'searchable' => false,
                'sortable' => false,
                'width' => '180px',
                'className' => 'text-left'
            ],
            [
                'data' => 'tl',
                'name' => 'tl',
                'label' => 'Tindak Lanjut',
                'className' => 'text-left',
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '150px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ],
        ]);
    }

    public function grid()
    {
    	$user = auth()->user();
    	if($user->hasJabatan(['auditor'])){
	    	$records = RegisterTL::whereHas('kka', function($b) use ($user){
    								$b->whereHas('program', function($c) use ($user){
		        						$c->whereHas('detailanggota', function($u) use ($user){
	    									$u->where('user_id', $user->id);
	    								});
		        					})->whereHas('detaildraft', function($u){
		        						$u->whereHas('rekomendasi', function($a){
		        							$a->where('status_monitoring', 1);
		        						});
		        					});
	    						})
	    						->select('*');
	    }elseif($user->hasJabatan(['SVP','Project'])){
	    	$records = RegisterTL::whereHas('kka', function($b) use ($user){
    									$b->whereHas('detaildraft', function($c) use ($user){
	    									$c->where('user_id', $user->id)
	    									->whereHas('rekomendasi', function($a){
			        							$a->where('status_monitoring', 1);
			        						});
		        						})->orWhere('parent_svp', $user->id);
	    						   })
	    						   ->select('*');
	    }else{
	    	$records = RegisterTL::where('id', 0)->select('*');
	    }
    	if(!isset(request()->order[0]['column'])) {
          $records->orderBy('created_at','desc');
        }
        return DataTables::of($records->get())
           ->addColumn('num', function ($record) {
               return request()->start;
           })
           ->addColumn('tahun', function ($record) {
               return $record->kka->program->penugasanaudit->rencanadetail->rencanaaudit->tahun;
           })
           ->addColumn('rencana', function ($record) {
                return DateToString($record->kka->program->detailpelaksanaan->first()->tgl).' - '.DateToString($record->kka->program->detailpelaksanaan->last()->tgl);
           })
           ->addColumn('tipe', function ($record) {
                $tipe = '';
                if($record->kka->program->penugasanaudit->rencanadetail->tipe == 0){
                  $tipe = '<span class="label label-primary">Audit</span>';
                }elseif($record->kka->program->penugasanaudit->rencanadetail->tipe == 1){
                  $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
                }else{
                  $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
                }

                return $tipe;
           })
           ->addColumn('kategori', function ($record) {
                $kategori = '';
                if($record->kka->program->penugasanaudit->rencanadetail->tipe == 0){
                    if($record->kka->program->penugasanaudit->rencanadetail->tipe_object == 0){
                      $kategori = '<span class="label label-success">Business Unit (BU)</span>';
                    }elseif($record->kka->program->penugasanaudit->rencanadetail->tipe_object == 1){
                      $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
                    }elseif($record->kka->program->penugasanaudit->rencanadetail->tipe_object == 2){
                      $kategori = '<span class="label label-warning">Projek</span>';
                    }else{
                      $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
                    }
                }elseif($record->kka->program->penugasanaudit->rencanadetail->tipe == 1){
                    $kategori = $record->kka->program->penugasanaudit->rencanadetail->konsultasi->nama;
                }else{
                    $kategori = $record->kka->program->penugasanaudit->rencanadetail->lain->nama;
                }
                return $kategori;
           })
           ->addColumn('object_audit', function ($record) {
               return object_audit($record->kka->program->penugasanaudit->rencanadetail->tipe_object, $record->kka->program->penugasanaudit->rencanadetail->object_id);
           })
           ->addColumn('no_ab', function ($record) {
               return getAb($record->kka->program->penugasanaudit->rencanadetail->tipe_object, $record->kka->program->penugasanaudit->rencanadetail->object_id);
           })
           ->addColumn('kka', function ($record) {
               $buttons = '';
               $buttons .= $this->makeButton([
                    'type' 		=> 'edit',
                    'label' 	=> '<i class="fa fa-print text-info"></i>',
                    'tooltip' 	=> 'Cetak',
                    'class' 	=> 'cetak-kka button',
                    'id'   		=> $record->kka->id,
               ]);

               return $buttons;
           })
           ->addColumn('tim', function ($record) use ($user) {
	           	$data = array();
                $tim ='';
           		$tim .='<ul class="list list-more2 list-unstyled" style="margin-top:10px;" data-display="3">';
           		$tim .='<li class="item">'.$record->kka->program->user->name.'</li>';
           		foreach ($record->kka->program->detailanggota->whereIn('fungsi', [1,2,3]) as $key => $value) {
           				$data[] = $value->user->name;
           		}
           		foreach (array_unique($data) as $key => $value) {
           			$tim .='<li class="item">'.$value.'</li>';
           		}
           		$tim .='</ul>';
               	return $tim;
           })
           ->addColumn('pic', function ($record) use ($user) {
               	return $record->kka->detaildraft->first()->user->name;
           })
           ->addColumn('surat', function ($record) {
                $buttons = '';
                if($record->lha){
		            $buttons .= $this->makeButton([
		                'type' 		=> 'edit',
		                'label' 	=> '<i class="fa fa-print text-info"></i>',
		                'tooltip' 	=> 'Surat Disposisi',
		                'class' 	=> 'cetak-disposisi button',
		                'id'   		=> $record->lha->id,
		           	]);
		        }else{
		        	$buttons .= '';
		        }

               return $buttons;
           })
           ->addColumn('tl', function ($record) {
                $tipe = '';
                $belum = $record->detail->where('status_monitoring', 1)->whereIn('stage', [0,1,2])->count();
                $sedang = $record->detail->where('status_monitoring', 1)->where('stage', 3)->count();
                $selesai = $record->detail->where('status_monitoring', 1)->where('stage', 4)->count();
                  $tipe .= '<span class="label label-primary">Belum <span class="badge badge-sm down bg-danger pull-right-xs">'.$belum.'</span></span><br>';
                  $tipe .= '<span class="label label-info">Dalam Proses <span class="badge badge-sm down bg-danger pull-right-xs">'.$sedang.'</span></span><br>';
                  $tipe .= '<span class="label label-success">Sudah <span class="badge badge-sm down bg-danger pull-right-xs">'.$selesai.'</span></span><br>';

                return $tipe;
           })
           ->addColumn('status', function ($record) {
               if($record->status == 0){
                    return '<span class="label label-default">Draft</span>';
               }elseif($record->status == 1){
                    return '<span class="label label-info">On Progress</span>';
               }else{
                   return '<span class="label label-success">Completed</span>';
               }
           })
           ->addColumn('action', function ($record) use ($user){
                $buttons = '';
	   			if($user->hasJabatan(['SVP'])){
	   				if($record->kka->detaildraft->first()->user_id == $user->id){
		               if($record->status == 0){
			               	$buttons .= $this->makeButton([
			                    'type'      => 'edit',
			                    'label'     => '<i class="fa fa-plus text-primary"></i>',
			                    'tooltip'   => 'Buat Tindak Lanjut',
			                    'class'     => 'buat button',
			                    'id'        => $record->id,
			               ]);
		               }elseif($record->status == 1){
			               $buttons .= $this->makeButton([
			                    'type'      => 'edit',
			                    'label'     => '<i class="fa fa-edit text-primary"></i>',
			                    'tooltip'   => 'Ubah Tindak Lanjut',
			                    'class'     => 'ubah button',
			                    'id'        => $record->id,
			               ]);
		               }else{
			               $buttons .= $this->makeButton([
			                    'type'      => 'edit',
			                    'label'     => '<i class="fa fa-eye text-primary"></i>',
			                    'tooltip'   => 'Detil Tindak Lanjut',
			                    'class'     => 'detil-tl button',
			                    'id'        => $record->id,
			               ]);
		               }
	           		}else{
	           			if($record->status == 1){
		           	   		$buttons .= $this->makeButton([
			                    'type'      => 'edit',
			                    'label'     => '<i class="fa fa-edit text-primary"></i>',
			                    'tooltip'   => 'Approval',
			                    'class'     => 'udata-svp button',
			                    'id'        => $record->id,
			                ]);
	           			}else{
	           				$buttons .= $this->makeButton([
			                    'type'      => 'edit',
			                    'label'     => '<i class="fa fa-eye text-primary"></i>',
			                    'tooltip'   => 'Detil Tindak Lanjut',
			                    'class'     => 'detil-svp button',
			                    'id'        => $record->id,
			               ]);
	           			}
	           		}
	   			}elseif($user->hasJabatan(['Project'])){
   				   if($record->status == 0){
		               	$buttons .= $this->makeButton([
		                    'type'      => 'edit',
		                    'label'     => '<i class="fa fa-plus text-primary"></i>',
		                    'tooltip'   => 'Buat Tindak Lanjut',
		                    'class'     => 'buat button',
		                    'id'        => $record->id,
		               ]);
	               }elseif($record->status == 1){
		               $buttons .= $this->makeButton([
		                    'type'      => 'edit',
		                    'label'     => '<i class="fa fa-edit text-primary"></i>',
		                    'tooltip'   => 'Ubah Tindak Lanjut',
		                    'class'     => 'ubah button',
		                    'id'        => $record->id,
		               ]);
	               }else{
		               $buttons .= $this->makeButton([
		                    'type'      => 'edit',
		                    'label'     => '<i class="fa fa-eye text-primary"></i>',
		                    'tooltip'   => 'Detil Tindak Lanjut',
		                    'class'     => 'detil-tl button',
		                    'id'        => $record->id,
		               ]);
	               }
	   			}else{
	   				if($record->status == 0 || $record->status == 1){
	           	   		$buttons .= $this->makeButton([
		                    'type'      => 'edit',
		                    'label'     => '<i class="fa fa-edit text-primary"></i>',
		                    'tooltip'   => 'Data Tindak Lanjut',
		                    'class'     => 'udata button',
		                    'id'        => $record->id,
		                ]);
           			}else{
           				$buttons .= $this->makeButton([
		                    'type'      => 'edit',
		                    'label'     => '<i class="fa fa-eye text-primary"></i>',
		                    'tooltip'   => 'Detil Tindak Lanjut',
		                    'class'     => 'detil-tl button',
		                    'id'        => $record->id,
		               ]);
           			}
	   			}
               return $buttons;
           })
           ->rawColumns(['action','status','tl','surat','kka','no_ab','kka','object_audit','kategori','tipe','rencana','tahun','tim'])
           ->make(true);
    }

    public function index()
    {
        return $this->render('modules.audit.tindak-lanjut.monitoring.index');
    }

    public function buat(RegisterTL $id)
    {
    	$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Tindak Lanjut' => '#', 'Register Tindak Lanjut' => '#', 'Buat' => '#']);
        return $this->render('modules.audit.tindak-lanjut.monitoring.buat', [
            'record' => $id,
        ]);
    }

    public function ubah(RegisterTL $id)
    {
    	$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Tindak Lanjut' => '#', 'Register Tindak Lanjut' => '#', 'Ubah' => '#']);
        return $this->render('modules.audit.tindak-lanjut.monitoring.ubah', [
            'record' => $id,
        ]);
    }

    public function udata(RegisterTL $id)
    {
    	$user = auth()->user();
    	$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Tindak Lanjut' => '#', 'Register Tindak Lanjut' => '#', 'Tindak Lanjut' => '#']);
        return $this->render('modules.audit.tindak-lanjut.monitoring.approve', [
            'record' => $id,
            'user' => $user->fungsi,
        ]);
    }

    public function udataSvp(RegisterTL $id)
    {
    	$user = auth()->user();
    	$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Tindak Lanjut' => '#', 'Register Tindak Lanjut' => '#', 'Tindak Lanjut' => '#']);
        return $this->render('modules.audit.tindak-lanjut.monitoring.approve-svp', [
            'record' => $id,
        ]);
    }

    public function detil(RegisterTL $id)
    {
    	$user = auth()->user();
    	$this->setBreadcrumb(['Kegiatan Audit' => '#', 'Tindak Lanjut' => '#', 'Register Tindak Lanjut' => '#', 'Detil Tindak Lanjut' => '#']);
        return $this->render('modules.audit.tindak-lanjut.monitoring.detil', [
            'record' => $id,
        ]);
    }

    public function edit(SuratPerintah $SuratPerintah)
    {
        return $this->render('modules.audit.tindak-lanjut.monitoring.edit', ['record' => $SuratPerintah]);
    }

    public function store(Request $request)
    {
    	DB::beginTransaction();
    	try {
    		$record 			= RegisterTL::find($request->register_id);
    		$record->status 	= 1;
    		$record->save();
    		$record->saveDetail($request->data);
	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function update(Request $request)
    {
    	DB::beginTransaction();
    	try {
    		$record 			= RegisterTL::find($request->id);
    		$record->status 	= 1;
    		$record->save();
    		$record->saveDetail($request->data);
	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function ubahData(Request $request)
    {
    	DB::beginTransaction();
    	try {
    		$record 			= RegisterTL::find($request->id);
    		$record->status 	= 1;
    		$record->save();
    		$record->updateDetail($request->data);
	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function ubahDataApprove(Request $request)
    {
    	DB::beginTransaction();
    	try {
    		$record 			= RegisterTL::find($request->id);
    		$record->status 	= 1;
    		$record->save();
    		$record->updateDetails($request->data);
	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function cetakSuratDisposisi(Request $request, $id){
        $record = LHA::find($id);
        $data = [
            'records' => $record,
        ];
        $pdf = PDF::loadView('modules.audit.tindak-lanjut.monitoring.cetak-disposisi', $data);
        return $pdf->stream('Surat Disposisi TL '.$record->tanggal.'.pdf',array("Attachment" => false));
    }

    public function cetakKka(Request $request, $id){
    	$record = DraftKka::find($id);
        $data = [
        	'records' => $record,
        ];
	    $pdf = PDF::loadView('modules.audit.tindak-lanjut.monitoring.cetak-kka', $data)->setPaper('a4', 'landscape');
        return $pdf->stream('Draft KKA.pdf',array("Attachment" => false));
 	}

 	public function monitoring()
    {
        DB::beginTransaction();
    	try {
    		$kkadetailrekomendasi = DraftKkaDetailRekomendasi::get();
    		foreach ($kkadetailrekomendasi as $key => $value) {
    			if($value->register->stage < 4){
    				if($value->draftdetail->tgl){
    					$now = new Carbon(Carbon::now());
						$dates = new Carbon($value->draftdetail->tgl);
						$are_different = $now->gt($dates);
    					if($are_different){
    						$rekomen = DraftKkaDetailRekomendasi::find($value->id);
    						$register = RegisterTLDetail::find($rekomen->register->id);
    						$register->status_monitoring =1;
    						$register->save();
    						$rekomen->status_monitoring =1;
    						$rekomen->save();
    					}
    				}else{
    					$cek = $value->draftdetail->draft->lha;
    					if($cek){
    						if($cek->tanggal){
    							$now = new Carbon(Carbon::now());
								$dates = new Carbon($cek->tanggal);
								$are_different = $now->gt($dates);
    							if($are_different){
		    						$rekomen = DraftKkaDetailRekomendasi::find($value->id);
		    						$register = RegisterTLDetail::find($rekomen->register->id);
		    						$register->status_monitoring =1;
		    						$register->save();
		    						$rekomen->status_monitoring =2;
		    						$rekomen->save();
		    					}
    						}
    					}
    				}
    			}
    		}


	        DB::commit();
        }catch (\Exception $e) {
	      DB::rollback();
	      return response([
	        'status' => 'error',
	        'message' => $e,
	        'error' => $e->getMessage(),
	      ], 500);
	    }
        return response([
            'status' => true,
        ]);
    }
}
