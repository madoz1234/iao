<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use App\Models\Auths\User;
use Carbon\Carbon;
use Illuminate\Validation\ValidationException;

class AbsensiController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as performLogout;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'dashboard/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('guest')->except('logout');
    // }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        auth()->user()->flushActivity();
        auth()->user()->updateLoginWeb();

        $this->performLogout($request);
        return redirect('/absensi');
    }

    public function showLoginForm()
    {
        return view('auth.absensi');
    }

    public function loginIdProo()
    {
      return view('auth.login_idproo_absensi');
    }

  public function callback()
  {
      if (!isset($_SESSION)) {
        session_start();
      }

      try {
        $oidc = new OpenIDConnectClient("https://login.waskita.co.id:6443/", // [TODO] ganti IP
        "162dba77-9407-444d-ac31-24fc7f1aeb85",
        "693b40cb-2422-4f9f-8adb-92f591addced");

        $scopesDefine = array('openid' => 'log in using your identity',
        'api.auth' => 'access your info while not being logged in',
        'email' => 'read your email address',
        'profile' => 'read your basic profile info',
        'user.role' => 'read sign in user role',
        'user.read' => 'read sign in user profile',
        'user.readAll' => 'read all users full profile',
        'unit.readAll' => 'read all organization unit',
        'unit.read' => 'read detail organization unit'
      );
      $redirect_url = "http://10.10.1.10/digimaster/public/auth"; // [TODO] ganti IP

      $scopes = array_keys($scopesDefine);
      $oidc->addScope($scopes);
      $oidc->setRedirectURL($redirect_url);
      $response = array('code');
      $oidc->setResponseTypes($response);
      $oidc->authenticate();

      $refreshToken = $oidc->getRefreshToken();
      $accessToken = $oidc->getAccessToken();
      $infoUser = $oidc->requestUserInfo();

      if($infoUser)
      {
        $check = User::where('email', $infoUser->email)->first();
        if($check)
        {
          Auth::loginUsingId($check->id);
          auth()->user()->access_token = $accessToken;
          auth()->user()->save();

          return redirect()->route('home.index');
        }
      }

      return abort('403');
    }catch (\Exception $exception){
      return route('idproo.login');
    }
  }

  /**
   * The user has been authenticated.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  mixed  $user
   * @return mixed
   */
  protected function authenticated(Request $request, $user)
  {
      $user->setLoginWeb();
  }

    // protected function attemptLogin(Request $request)
    // {
    //     // dd('dor');
    //     if (\Auth::validate($request->only(['email', 'password']))) {
    //        $user = User::whereEmail($request->email)->first();
    //        if($user->last_activity != NULL && !Carbon::parse($user->last_activity)->addHours(2)->isPast())
    //        {
    //            throw ValidationException::withMessages([
    //                $this->username() => [trans('auth.login')],
    //            ]);
    //        }
    //     }

    //    return $this->guard()->attempt(
    //        $this->credentials($request), $request->filled('remember')
    //    );
    // }
}
