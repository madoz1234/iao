<?php

namespace App\Http\Controllers\Monitoring;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Models\Master\Edisi;
use App\Http\Requests\KegiatanAudit\Rencana\RencanaRequest;
use App\Http\Requests\KegiatanAudit\Rencana\DetailRequest;
use App\Http\Requests\KegiatanAudit\Rencana\RejectRequest;
use App\Http\Requests\KegiatanAudit\Rencana\UploadRequest;
use App\Models\KegiatanAudit\Rencana\RencanaAudit;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit;
use App\Imports\RencanaAuditPkat;
use Illuminate\Support\Facades\Storage;
use App\Models\SessionLog;
use App\Models\Notification;
use App\Models\Auths\User;
use GuzzleHttp\Client;

use DB;
use Excel;
use Carbon;
use PDF;

class MonitoringController extends Controller
{
    protected $routes = 'monitoring.monitoring';
    protected $rencana;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct(RencanaAudit $rencana)
    {
    	$this->rencana = $rencana;
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Monitoring' => '#']);
    	 $this->setTableStruct([
	        	[
		                'data' => 'num',
		                'name' => 'num',
		                'label' => '#',
		                'orderable' => false,
		                'searchable' => false,
		                'className' => 'text-center',
		                'width' => '20px',
		            ],
		            [
		                'data' => 'tahun',
		                'name' => 'tahun',
		                'label' => 'Tahun',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'rencana',
		                'name' => 'rencana',
		                'label' => 'Rencana Pelaksanaan',
		                'className' => 'text-center',
		                'width' => '300px',
		            ],
		            [
		                'data' => 'tipe',
		                'name' => 'tipe',
		                'label' => 'Tipe',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'kategori',
		                'name' => 'kategori',
		                'label' => 'Kategori',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'object_audit',
		                'name' => 'object_audit',
		                'label' => 'Objek Audit',
		                'className' => 'text-left',
		                'width' => '250px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'no_ab',
		                'name' => 'no_ab',
		                'label' => 'No AB',
		                'className' => 'text-left',
		                'width' => '150px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'stage_awal',
		                'name' => 'stage_awal',
		                'label' => 'Stage Saat Ini',
                    'className' => 'text-left',
		                'width' => '250px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'stage_akhir',
		                'name' => 'stage_akhir',
		                'label' => 'Stage Selanjutnya',
                    'className' => 'text-left',
		                'width' => '250px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'riwayat',
		                'name' => 'riwayat',
		                'label' => 'Aksi',
		                'className' => 'text-center',
		                'width' => '80px',
		                'sortable' => true,
		            ],
    	]);
    }

    public function grid()
    {
        $records = RencanaAuditDetail::where('flag', 1)->where('tipe', 0)->select('*');
        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($tahun = request()->tahun) {
        	$records->whereHas('rencanaaudit', function($u) use ($tahun) {
        		$u->where('tahun', 'like', '%' . $tahun . '%');
        	});
        }
        if($kategori = request()->kategori) {
        	if($kategori == 1){
	        	$records->where('tipe_object', 0);
        	}elseif($kategori == 2){
        		$records->where('tipe_object', 1);
        	}elseif($kategori == 3){
        		$records->where('tipe_object', 2);
        	}elseif($kategori == 4){
        		$records->where('tipe_object', 3);
        	}else{
        		$records = $records;
        	}
        }

        if($object_id = request()->object_id) {
        	if(request()->kategori == 1){
	        	$kat = $kategori= 0;
        	}elseif(request()->kategori == 2){
        		$kat = $kategori= 1;
        	}elseif(request()->kategori == 3){
        		$kat = $kategori= 2;
        	}elseif(request()->kategori == 4){
        		$kat = $kategori= 3;
        	}else{
        		$kat = null;
        	}
        	$records->where('tipe_object', $kat)->where('object_id', $object_id);
        }
        return DataTables::of($records->get())
           ->addColumn('num', function ($record) {
               return request()->start;
           })
           ->addColumn('tahun', function ($record) {
               return $record->rencanaaudit->tahun;
           })
           ->addColumn('tipe', function ($record) {
                $tipe = '';
                if($record->tipe == 0){
               	  $tipe = '<span class="label label-primary">Audit</span>';
                }elseif($record->tipe == 1){
               	  $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
                }else{
                  $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
                }
               	
                return $tipe;
           })
           ->addColumn('object_audit', function ($record) {
               return object_audit($record->tipe_object, $record->object_id);
           })
           ->addColumn('no_ab', function ($record) {
               return getAb($record->tipe_object, $record->object_id);
           })
           ->addColumn('kategori', function ($record) {
                $kategori = '';
                if($record->tipe == 0){
	                if($record->tipe_object == 0){
	               	  $kategori = '<span class="label label-success">Business Unit (BU)</span>';
	                }elseif($record->tipe_object == 1){
	               	  $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
	                }elseif($record->tipe_object == 2){
	               	  $kategori = '<span class="label label-warning">Project</span>';
	                }else{
	               	  $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
	                }
                }elseif($record->tipe == 1){
               	  	$kategori = $record->konsultasi->nama;
                }else{
                    $kategori = $record->lain->nama;
                }
                return $kategori;
           })
           ->addColumn('rencana', function ($record) {
               return $record->rencana;
           })
           ->addColumn('stage_awal', function ($record) {
           		if($record->status_penugasan == 1){
           			if($record->penugasan){
           				if($record->penugasan->status_audit == 0 && $record->penugasan->status_tinjauan == 0){
           					return 'Tinjauan Dokumen dan Program Audit';
           				}else{
           					if($record->penugasan->status_audit == 1){
           						if($record->penugasan->programaudit){
           							if($record->penugasan->programaudit->status < 3){
           								return 'Program Audit';
           							}else{
           								if($record->penugasan->programaudit->kka){
           									if($record->penugasan->programaudit->kka->status < 4){
           										return 'KKA';
           									}else{
           										if($record->penugasan->programaudit->kka->lha){
           											if($record->penugasan->programaudit->kka->lha->status < 6){
           												return 'LHA';
           											}else{
           												return 'Tindak Lanjut';
           											}
           										}else{
           											return 'KKA';
           										}
           									}
           								}else{
	           								return 'KKA dan Opening Meeting';
           								}
           							}
           						}else{
	           						return 'Program Audit';
           						}
           					}else{
           						return 'Tinjauan Dokumen dan Program Audit';
           					}
           				}
           			}else{
	           		   return 'Surat Penugasan';
           			}
           		}else{
	               return 'RKIA';
           		}
           })
           ->addColumn('stage_akhir', function ($record) {
               if($record->status_penugasan == 1){
           			if($record->penugasan){
           				if($record->penugasan->status_audit == 0 && $record->penugasan->status_tinjauan == 0){
           					return 'KKA';
           				}else{
           					if($record->penugasan->status_audit == 1){
           						if($record->penugasan->programaudit){
           							if($record->penugasan->programaudit->status < 3){
           								return 'KKA / Opening Meeting';
           							}else{
           								if($record->penugasan->programaudit->kka){
           									if($record->penugasan->programaudit->kka->status < 4){
           										return 'LHA';
           									}else{
           										if($record->penugasan->programaudit->kka->lha){
           											if($record->penugasan->programaudit->kka->lha->status < 6){
           												return 'Tindak Lanjut';
           											}else{
           												return 'Close';
           											}
           										}else{
           											return 'LHA';
           										}
           									}
           								}else{
	           								return 'LHA / Closing Meeting';
           								}
           							}
           						}else{
	           						return 'KKA / Opening Meeting';
           						}
           					}else{
           						return 'KKA';
           					}
           				}
           			}else{
	           		   return 'Tinjauan Dokumen / Program Audit';
           			}
           		}else{
	               return 'Surat Penugasan';
           		}
           })
           ->editColumn('riwayat', function ($record){
                $buttons = '';
                $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-eye text-primary"></i>',
                    'tooltip'   => 'Riwayat',
                    'class'     => 'riwayat button',
                    'id'        => $record->id,
                ]);
                $buttons .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-link text-primary"></i>',
                    'tooltip'   => 'Link',
                    'class'     => 'link button',
                    'id'        => $record->id,
                ]);
                return $buttons;
            })
           ->rawColumns(['tipe','object_audit','kategori','rencana','riwayat'])
           ->make(true);
    }

    public function index()
    {
    	return $this->render('modules.monitoring.index');
    }
}
