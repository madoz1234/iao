<?php

namespace App\Http\Controllers\KegiatanLainnya;

use App\Http\Controllers\Controller;
use App\Http\Requests\KegiatanLainnya\KegiatanLainnyaRequest;
use App\Models\Auths\User;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\KegiatanLainnya\KegiatanLainnya;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use PDF;
use Yajra\DataTables\DataTables;

class LainnyaPerencanaanController extends Controller
{
    protected $routes = 'kegiatan-lainnya.perencanaan';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setTitle('Kegiatan Lainnya Perencanaan');
        $this->setBreadcrumb(['Kegiatan Lainnya' => route($this->routes.'.index'), 'Perencanaan' => route($this->routes.'.index')]);
        // Header Grid Datatable
        $this->listStructs = [
            'listStruct' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => 'text-center',
                    'width' => '20px',
                ],
                /* --------------------------- */
                [
                    'data' => 'tahun',
                    'name' => 'tahun',
                    'label' => 'Tahun',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'rencana',
                    'name' => 'rencana',
                    'label' => 'Rencana Pelaksanaan',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'tipe',
                    'name' => 'tipe',
                    'label' => 'Tipe',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'kategori',
                    'name' => 'kategori',
                    'label' => 'Kategori',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '200px'
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '100px',
                    'className' => 'text-center'
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '80px',
                    'className' => 'text-center'
                ],
            ],
            'listStruct2' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => 'text-center',
                    'width' => '20px',
                ],
                /* --------------------------- */
                [
                    'data' => 'tahun',
                    'name' => 'tahun',
                    'label' => 'Tahun',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'rencana',
                    'name' => 'rencana',
                    'label' => 'Rencana Pelaksanaan',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'tipe',
                    'name' => 'tipe',
                    'label' => 'Tipe',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'kategori',
                    'name' => 'kategori',
                    'label' => 'Kategori',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '200px'
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '100px',
                    'className' => 'text-center'
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '80px',
                    'className' => 'text-center'
                ],
            ],
            'listStruct3' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => 'text-center',
                    'width' => '20px',
                ],
                /* --------------------------- */
                [
                    'data' => 'tahun',
                    'name' => 'tahun',
                    'label' => 'Tahun',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'rencana',
                    'name' => 'rencana',
                    'label' => 'Rencana Pelaksanaan',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'tipe',
                    'name' => 'tipe',
                    'label' => 'Tipe',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'kategori',
                    'name' => 'kategori',
                    'label' => 'Kategori',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '200px'
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '100px',
                    'className' => 'text-center'
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '80px',
                    'className' => 'text-center'
                ],
            ],
        ];
    }

    public function grid()
    {
        $user = auth()->user();
        if($user->hasRole(['auditor','SVP - Internal Audit','President Director','Secretary']) || $user->isUserIa()){
            $records = RencanaAuditDetail::with('kegiatan_lainnya')
                                        ->where('flag', 1)
                                        ->where('tipe', 2)
                                        ->whereIn('status_lain', [0,1])
                                        ->select('*')
                                        ->whereHas('rencanaaudit', function($u){
                                            $u->whereIn('status', [4,5,6]);
                                        });
            if(!isset(request()->order[0]['column'])) {
                  $records->orderBy('created_at','desc');
            }
            if ($tahun = request()->tahun) {
                $records->whereHas('rencanaaudit', function($u) use ($tahun) {
                    $u->where('tahun', 'like', '%' . $tahun . '%');
                });
            }
        }else{
            $records = RencanaAuditDetail::where('id', 0);
        }

        return DataTables::of($records->get())
            ->editColumn('num', function ($record) {
                return request()->start;
            })
            ->editColumn('tahun', function ($record) { 
                return $record->rencanaaudit->tahun;
            })
            ->editColumn('rencana', function ($record) { 
                return $record->rencana;
            })
            ->editColumn('tipe', function ($record) { 
                return '<span class="label label-success">Kegiatan Lainnya</span>';
            })
            ->editColumn('kategori', function ($record) { 
                return $record->kategori;
            })
            ->editColumn('status', function ($record) {
                if($record->kegiatan_lainnya){
                    if($record->kegiatan_lainnya->ket_svp){
                        return '<span class="label label-warning">Ditolak SVP</span>';
                    }else{
                        return '<span class="label label-default">Draft</span>';
                    }
                }else{
                    return '<span class="label label-info">Baru</span>';
                }
            })
            ->editColumn('action', function ($record) use ($user) {
                $buttons = '';

                if (is_null($record->kegiatan_lainnya)) {
                    if($user->hasRole(['auditor'])){
                        $buttons .= $this->makeButton([
                            'type'      => 'edit',
                            'label'     => '<i class="fa fa-plus text-default"></i>',
                            'tooltip'   => 'Buat Kegiatan Lainnya',
                            'class'     => 'create button',
                            'id'        => $record->id,
                        ]);
                    }
                }else{
                    if ($record->kegiatan_lainnya->status == 1) {
                        if($user->hasRole(['auditor'])){
                            $buttons .= $this->makeButton([
                                'type'      => 'edit',
                                'label'     => '<i class="fa fa-pencil text-info"></i>',
                                'tooltip'   => 'Ubah',
                                'class'     => 'edit button',
                                'id'        => $record->kegiatan_lainnya->id,
                            ]);
                        }
                        $buttons .= $this->makeButton([
                            'type'      => 'edit',
                            'label'     => '<i class="fa fa-eye text-primary"></i>',
                            'tooltip'   => 'Detil',
                            'class'     => 'm-l detil button',
                            'id'        => $record->kegiatan_lainnya->id,
                        ]);
                    }
                }
                return $buttons;
            })
            ->rawColumns(['tipe', 'status', 'action'])
            ->make(true);
    }

    public function onProgress()
    {
        $user = auth()->user();
        if($user->hasRole(['auditor','SVP - Internal Audit','President Director','Secretary']) || $user->isUserIa()){
            $records = KegiatanLainnya::with('rkia')
                                        ->where('status', 2)
                                        ->select('*');
            if(!isset(request()->order[0]['column'])) {
                  $records->orderBy('created_at','desc');
            }
            if ($tahun = request()->tahun) {
                $records->whereHas('rkia', function($rkia) use ($tahun) {
                    $rkia->whereHas('rencanaaudit', function($u) use ($tahun) {
                        $u->where('tahun', 'like', '%' . $tahun . '%');
                    });
                });
            }
        }else{
            $records = KegiatanLainnya::where('id', 0);
        }

        return DataTables::of($records->get())
            ->editColumn('num', function ($record) {
                return request()->start;
            })
            ->editColumn('tahun', function ($record) { 
                return $record->rkia->rencanaaudit->tahun;
            })
            ->editColumn('rencana', function ($record) { 
                return $record->rkia->rencana;
            })
            ->editColumn('tipe', function ($record) { 
                return '<span class="label label-success">Kegiatan Lainnya</span>';
            })
            ->editColumn('kategori', function ($record) { 
                return $record->rkia->kategori;
            })
            ->editColumn('status', function ($record) {
                return '<span class="label label-info">Waiting Approval SVP</span>';            
            })
            ->editColumn('action', function ($record) use ($user) {
                $buttons = '';
                if($user->hasRole(['SVP - Internal Audit'])){
                    $buttons .= $this->makeButton([
                        'type'      => 'edit',
                        'label'     => '<i class="fa fa-check text-primary"></i>',
                        'tooltip'   => 'Buat Approval',
                        'class'     => 'save-as-approve button',
                        'id'        => $record->id,
                    ]);
                }
                $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-eye text-primary"></i>',
                    'tooltip'   => 'Detil',
                    'class'     => 'm-l detil button',
                    'id'        => $record->id,
                ]);

                return $buttons;
            })
            ->rawColumns(['tipe', 'status', 'action'])
            ->make(true);
    }

    public function historis()
    {
        $user = auth()->user();
        if($user->hasRole(['auditor','SVP - Internal Audit','President Director','Secretary']) || $user->isUserIa()){
            $records = KegiatanLainnya::with('rkia')
                                        ->whereIn('status', [3,4])
                                        ->select('*');
            if(!isset(request()->order[0]['column'])) {
                  $records->orderBy('created_at','desc');
            }
            if ($tahun = request()->tahun) {
                $records->whereHas('rkia', function($rkia) use ($tahun) {
                    $rkia->whereHas('rencanaaudit', function($u) use ($tahun) {
                        $u->where('tahun', 'like', '%' . $tahun . '%');
                    });
                });
            }
        }else{
            $records = KegiatanLainnya::where('id', 0);
        }

        return DataTables::of($records->get())
            ->editColumn('num', function ($record) {
                return request()->start;
            })
            ->editColumn('tahun', function ($record) { 
                return $record->rkia->rencanaaudit->tahun;
            })
            ->editColumn('rencana', function ($record) { 
                return $record->rkia->rencana;
            })
            ->editColumn('tipe', function ($record) { 
                return '<span class="label label-success">Kegiatan Lainnya</span>';
            })
            ->editColumn('kategori', function ($record) { 
                return $record->rkia->kategori;
            })
            ->editColumn('status', function ($record) {
                return '<span class="label label-success">Completed</span>';            
            })
            ->editColumn('action', function ($record) use ($user) {
                $buttons = '';
                $buttons .= $this->makeButton([
                'type'    => 'edit',
                'label'   => '<i class="fa fa-print text-primary"></i>',
                'tooltip' => 'Cetak',
                'class'   => 'cetak button',
                'id'      => $record->id,
                ]);
                return $buttons;
            })
            ->rawColumns(['tipe', 'status', 'action'])
            ->make(true);
    }

    public function index()
    {
        $badgeTab = KegiatanLainnya::badgeTab('perencanaan');
        return $this->render('modules.lainnya.perencanaan.index', [
            'structs'    => $this->listStructs,
            'baru'       => $badgeTab->baru,
            'onProgress' => $badgeTab->onProgress
        ]);
    }

    public function create(RencanaAuditDetail $rkia)
    {
        $this->pushBreadcrumb(['Buat Kegiatan Lainnya' => route($this->routes.'.create', $rkia->id)]);
        $users = User::divisi()->get();
        return $this->render('modules.lainnya.perencanaan.create', ['record' => $rkia, 'users' => $users]);
    }

    public function store(KegiatanLainnyaRequest $request)
    {
        $perencanaan = new KegiatanLainnya;
        return $perencanaan->saveFromRequest($request, 'perencanaan-store');
    }

    public function edit(KegiatanLainnya $perencanaan)
    {
        $users = User::divisi()->get();
        return $this->render('modules.lainnya.perencanaan.edit', [
          'record' => $perencanaan,
          'users' => $users,
        ]);
    }

    public function update(KegiatanLainnyaRequest $request, KegiatanLainnya $perencanaan)
    {
        return $perencanaan->saveFromRequest($request, 'perencanaan-update');
    }

    public function show(KegiatanLainnya $perencanaan)
    {
        return $this->render('modules.lainnya.perencanaan.detail', [
          'record' => $perencanaan,
        ]);
    }

    public function destroy(KegiatanLainnya $perencanaan)
    {
        $lainnya->delete();
        return response()->json([
            'success' => true
        ]);
    }

    public function approve(KegiatanLainnyaRequest $request, KegiatanLainnya $perencanaan)
    {
        return $perencanaan->saveFromRequest($request, 'perencanaan-approve');
    }

    public function formReject(KegiatanLainnya $perencanaan)
    {
        return $this->render('modules.lainnya.perencanaan.reject', [
            'record' => $perencanaan,
        ]);
    }

    public function reject(KegiatanLainnyaRequest $request, KegiatanLainnya $perencanaan)
    {
        return $perencanaan->saveFromRequest($request, 'perencanaan-reject');
    }

    public function cetak(KegiatanLainnya $perencanaan)
    {
        $user = auth()->user();
        if($user->hasRole(['auditor','SVP - Internal Audit','President Director','Secretary']) || $user->isUserIa()){
            $data = [
                'record' => $perencanaan
            ];
            $pdf = PDF::loadView('modules.lainnya.perencanaan.cetak', $data)->setPaper('a4', 'portrait');
            return $pdf->stream('Kegiatan Lainnya Perencanaan '.Carbon::parse($perencanaan->updated_at)->format('Y-m-d').'.pdf',array("Attachment" => false));
        }
        return abort(404);
    }

}
