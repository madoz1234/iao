<?php

namespace App\Http\Controllers\KegiatanLainnya;

use App\Http\Controllers\Controller;
use App\Http\Requests\KegiatanLainnya\KegiatanLainnyaRequest;
use App\Libraries\CoreSn;
use App\Models\Auths\User;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\KegiatanLainnya\KegiatanLainnya;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use PDF;
use Yajra\DataTables\DataTables;

class LainnyaPelaksanaanController extends Controller
{
    protected $routes = 'kegiatan-lainnya.pelaksanaan';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setTitle('Kegiatan Lainnya Pelaksanaan');
        $this->setBreadcrumb(['Kegiatan Lainnya' => route($this->routes.'.index'), 'Pelaksanaan' => route($this->routes.'.index')]);
        // Header Grid Datatable
        $this->listStructs = [
            'listStruct' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => 'text-center',
                    'width' => '20px',
                ],
                /* --------------------------- */
                [
                    'data' => 'tahun',
                    'name' => 'tahun',
                    'label' => 'Tahun',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'rencana',
                    'name' => 'rencana',
                    'label' => 'Rencana Pelaksanaan',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'tipe',
                    'name' => 'tipe',
                    'label' => 'Tipe',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'jenis',
                    'name' => 'jenis',
                    'label' => 'Jenis',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'kategori',
                    'name' => 'kategori',
                    'label' => 'Kategori',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '200px'
                ],
                [
                    'data' => 'notulen',
                    'name' => 'notulen',
                    'label' => 'Notulen',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '150px'
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '100px',
                    'className' => 'text-center'
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '80px',
                    'className' => 'text-center'
                ],
            ],
            'listStruct2' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => 'text-center',
                    'width' => '20px',
                ],
                /* --------------------------- */
                [
                    'data' => 'tahun',
                    'name' => 'tahun',
                    'label' => 'Tahun',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'rencana',
                    'name' => 'rencana',
                    'label' => 'Rencana Pelaksanaan',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'tipe',
                    'name' => 'tipe',
                    'label' => 'Tipe',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'jenis',
                    'name' => 'jenis',
                    'label' => 'Jenis',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'kategori',
                    'name' => 'kategori',
                    'label' => 'Kategori',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '200px'
                ],
                [
                    'data' => 'notulen',
                    'name' => 'notulen',
                    'label' => 'Notulen',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '150px'
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '100px',
                    'className' => 'text-center'
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '80px',
                    'className' => 'text-center'
                ],
            ],
            'listStruct3' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => 'text-center',
                    'width' => '20px',
                ],
                /* --------------------------- */
                [
                    'data' => 'tahun',
                    'name' => 'tahun',
                    'label' => 'Tahun',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'rencana',
                    'name' => 'rencana',
                    'label' => 'Rencana Pelaksanaan',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'tipe',
                    'name' => 'tipe',
                    'label' => 'Tipe',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'jenis',
                    'name' => 'jenis',
                    'label' => 'Jenis',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'kategori',
                    'name' => 'kategori',
                    'label' => 'Kategori',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '200px'
                ],
                [
                    'data' => 'notulen',
                    'name' => 'notulen',
                    'label' => 'Notulen',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '150px'
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '100px',
                    'className' => 'text-center'
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '80px',
                    'className' => 'text-center'
                ],
            ],
        ];
    }

    public function grid()
    {
        $user = auth()->user();
        if($user->hasRole(['auditor','SVP - Internal Audit','President Director','Secretary']) || $user->isUserIa()){
            $records = KegiatanLainnya::with(['rkia', 'notulen'])
                                        ->where('status', 3)
                                        ->select('*');
            if(!isset(request()->order[0]['column'])) {
                  $records->orderBy('created_at','desc');
            }
            if ($tahun = request()->tahun) {
                $records->whereHas('rkia', function($rkia) use ($tahun) {
                    $rkia->whereHas('rencanaaudit', function($u) use ($tahun) {
                        $u->where('tahun', 'like', '%' . $tahun . '%');
                    });
                });
            }
        }else{
            $records = KegiatanLainnya::where('id', 0);
        }

        return DataTables::of($records->get())
            ->editColumn('num', function ($record) {
                return request()->start;
            })
            ->editColumn('tahun', function ($record) { 
                return $record->rkia->rencanaaudit->tahun;
            })
            ->editColumn('rencana', function ($record) { 
                return $record->rkia->rencana;
            })
            ->editColumn('tipe', function ($record) { 
                return '<span class="label label-success">Kegiatan Lainnya</span>';
            })
            ->editColumn('jenis', function ($record) { 
                if($record->jenis_rapat == 1){
                    return '<span class="label label-primary">Internal</span>'; 
                }else{
                    return '<span class="label label-warning">Eksternal</span>';
                }
            })
            ->editColumn('kategori', function ($record) { 
                return $record->kategori;
            })
            ->editColumn('notulen', function ($record) { 
                return $record->notulen->name;
            })
            ->editColumn('status', function ($record) {
                if (is_null($record->rapat)) {
                    return '<span class="label label-info">Baru</span>'; 
                }else{
                    return '<span class="label label-default">Draft</span>'; 
                }
            })
            ->editColumn('action', function ($record) use ($user) {
                $buttons = '';
                if (is_null($record->rapat)) {
                    if($user->id == $record->notulen_rapat ){
                        $buttons .= $this->makeButton([
                            'type'      => 'edit',
                            'label'     => '<i class="fa fa-plus text-default"></i>',
                            'tooltip'   => 'Buat Agenda',
                            'class'     => 'create button',
                            'id'        => $record->id,
                        ]);
                    }
                }else{
                    if($user->id == $record->notulen_rapat ){
                        $buttons .= $this->makeButton([
                            'type'      => 'edit',
                            'tooltip'   => 'Ubah Agenda',
                            'class'     => 'edit button',
                            'id'        => $record->id,
                        ]);
                    }
                }
                return $buttons;
            })
            ->rawColumns(['tipe', 'jenis', 'status', 'action'])
            ->make(true);
    }

    public function onProgress()
    {
        $user = auth()->user();
        if($user->hasRole(['auditor','SVP - Internal Audit','President Director','Secretary']) || $user->isUserIa()){
            $records = KegiatanLainnya::with(['rkia', 'notulen'])
                                        ->where('status', 4)
                                        ->select('*')
                                        ->whereHas('rapat', function($rapat){
                                            $rapat->where('status_rapat', 0);
                                        });
            if(!isset(request()->order[0]['column'])) {
                  $records->orderBy('created_at','desc');
            }
            if ($tahun = request()->tahun) {
                $records->whereHas('rkia', function($rkia) use ($tahun) {
                    $rkia->whereHas('rencanaaudit', function($u) use ($tahun) {
                        $u->where('tahun', 'like', '%' . $tahun . '%');
                    });
                });
            }
        }else{
            $records = KegiatanLainnya::where('id', 0);
        }

        return DataTables::of($records)
            ->editColumn('num', function ($record) {
                return request()->start;
            })
            ->editColumn('tahun', function ($record) { 
                return $record->rkia->rencanaaudit->tahun;
            })
            ->editColumn('rencana', function ($record) { 
                return $record->rkia->rencana;
            })
            ->editColumn('tipe', function ($record) { 
                return '<span class="label label-success">Kegiatan Lainnya</span>';
            })
            ->editColumn('jenis', function ($record) { 
                if($record->jenis_rapat == 1){
                    return '<span class="label label-primary">Internal</span>'; 
                }else{
                    return '<span class="label label-warning">Eksternal</span>';
                }
            })
            ->editColumn('kategori', function ($record) { 
                return $record->kategori;
            })
            ->editColumn('notulen', function ($record) { 
                return $record->notulen->name;
            })
            ->editColumn('status', function ($record) {
                if ($record->rapat->flag == 0) {
                    $shw= '<span class="label label-warning">Draft</span>';
                } else {
                    $shw= '<span class="label label-default">Rapat Belum Dimulai</span>';
                    if((Carbon::now()->diffInDays($record->rapat->tanggal,false) < 0) OR (Carbon::now()->format('d-m-Y') == CoreSn::DateToSql($record->rapat->tanggal))){
                        $jam = Carbon::createFromFormat('d-m-Y H:i',$record->rapat->tanggal.' '.$record->rapat->jam_mulai);
                        $jamnow = Carbon::now();
                        // $diff = abs($jamnow - $jam);
                        $convert= Carbon::parse($jam->format('H:i:s'))->floatDiffInHours($jamnow->format('H:i:s'), false); 
                        if($convert > -1){
                            if($convert > 0){
                                $shw= '<span class="label label-danger">Rapat Sedang Dimulai</span>';
                            }else{
                                $shw= '<span class="label label-info">Rapat Akan Segera Dimulai</span>';
                            }
                        }else{
                            $shw= '<span class="label label-default">Rapat Belum Dimulai</span>';
                        }
                    }
                    if(Carbon::now()->diffInDays($record->rapat->tanggal,false) < 0){
                        $shw= '<span class="label label-success">Rapat Telah Selesai</span>';
                    }
                }
                return $shw ;      
            })
            ->editColumn('action', function ($record) use ($user) {
                $buttons = '';
                $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-eye text-primary"></i>',
                    'tooltip'   => 'Detil',
                    'class'     => 'detil button',
                    'id'        => $record->id,
                ]);
                return $buttons;
            })
           ->rawColumns(['tipe', 'jenis', 'status', 'action'])
           ->make(true);
    }

    public function historis()
    {
        $user = auth()->user();
        if($user->hasRole(['auditor','SVP - Internal Audit','President Director','Secretary']) || $user->isUserIa()){
            $records = KegiatanLainnya::with(['rkia', 'notulen'])
                                        ->where('status', 4)
                                        ->select('*')
                                        ->whereHas('rapat', function($rapat){
                                            $rapat->where('status_rapat', 1);
                                        });
            if(!isset(request()->order[0]['column'])) {
                  $records->orderBy('created_at','desc');
            }
            if ($tahun = request()->tahun) {
                $records->whereHas('rkia', function($rkia) use ($tahun) {
                    $rkia->whereHas('rencanaaudit', function($u) use ($tahun) {
                        $u->where('tahun', 'like', '%' . $tahun . '%');
                    });
                });
            }
        }else{
            $records = KegiatanLainnya::where('id', 0);
        }

        return DataTables::of($records->get())
            ->editColumn('num', function ($record) {
                return request()->start;
            })
            ->editColumn('tahun', function ($record) { 
                return $record->rkia->rencanaaudit->tahun;
            })
            ->editColumn('rencana', function ($record) { 
                return $record->rkia->rencana;
            })
            ->editColumn('tipe', function ($record) { 
                return '<span class="label label-success">Kegiatan Lainnya</span>';
            })
            ->editColumn('jenis', function ($record) { 
                if($record->jenis_rapat == 1){
                    return '<span class="label label-primary">Internal</span>'; 
                }else{
                    return '<span class="label label-warning">Eksternal</span>';
                }
            })
            ->editColumn('kategori', function ($record) { 
                return $record->kategori;
            })
            ->editColumn('notulen', function ($record) { 
                return $record->notulen->name;
            })
            ->editColumn('status', function ($record) {
                return '<span class="label label-success">Completed</span>';            
            })
            ->editColumn('action', function ($record) use ($user) {
                $buttons = '';
                $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-eye text-primary"></i>',
                    'tooltip'   => 'Detil',
                    'class'     => 'detil button',
                    'id'        => $record->id,
                ]);
                return $buttons;
            })
           ->rawColumns(['tipe', 'jenis', 'status', 'action'])
           ->make(true);
    }

    public function index()
    {
        $badgeTab = KegiatanLainnya::badgeTab('pelaksanaan');
        return $this->render('modules.lainnya.pelaksanaan.index', [
            'structs'    => $this->listStructs,
            'baru'       => $badgeTab->baru,
            'onProgress' => $badgeTab->onProgress
        ]);
    }

    public function create(KegiatanLainnya $pelaksanaan)
    {
        $this->pushBreadcrumb(['Buat Agenda' => route($this->routes.'.create', $pelaksanaan->id)]);
        return $this->render('modules.lainnya.pelaksanaan.add-rapat', ['record' => $pelaksanaan]);
    }

    public function store(KegiatanLainnyaRequest $request, KegiatanLainnya $pelaksanaan)
    {
        return $pelaksanaan->saveFromRequest($request, 'pelaksanaan-add-rapat');
    }

    public function edit(KegiatanLainnya $pelaksanaan)
    {
        $this->pushBreadcrumb(['Ubah Agenda' => route($this->routes.'.create', $pelaksanaan->id)]);
        return $this->render('modules.lainnya.pelaksanaan.edit-rapat', ['record' => $pelaksanaan]);
    }

    public function update(KegiatanLainnyaRequest $request, KegiatanLainnya $pelaksanaan)
    {
       return $pelaksanaan->saveFromRequest($request, 'pelaksanaan-add-rapat');
    }

    public function show(KegiatanLainnya $pelaksanaan)
    {
        $this->pushBreadcrumb(['Detil' => route($this->routes.'.edit', $pelaksanaan->id)]);
        if ($pelaksanaan->jenis_rapat == 1) {
            return $this->render('modules.lainnya.pelaksanaan.detail-internal', ['record' => $pelaksanaan]);
        }else{
            return $this->render('modules.lainnya.pelaksanaan.detail-eksternal', ['record' => $pelaksanaan]);
        }
    }

    public function destroy(KegiatanLainnya $pelaksanaan)
    {
        $lainnya->delete();
        return response()->json([
            'success' => true
        ]);
    }

    public function cetak(KegiatanLainnya $pelaksanaan)
    {
        $user = auth()->user();
            if($user->hasRole(['auditor','SVP - Internal Audit','President Director','Secretary']) || $user->isUserIa()){
                $data = [
                'records' => $pelaksanaan,
                'today' => CoreSn::DateToString(Carbon::now()),
            ];
            if ($pelaksanaan->jenis_rapat == 1) {
                $pdf = PDF::loadView('modules.lainnya.pelaksanaan.cetak-internal', $data)->setPaper('a4', 'landscape');
            }else{
                $pdf = PDF::loadView('modules.lainnya.pelaksanaan.cetak-eksternal', $data)->setPaper('a4', 'landscape');
            }
            return $pdf->stream('Rapat Internal - '.DateToStringWDay($pelaksanaan->rapat->tanggal).'.pdf',array("Attachment" => false));
        }
        return abort(404);
    }

}
