<?php

namespace App\Http\Controllers\KegiatanLainnya;

use App\Http\Controllers\Controller;
use App\Http\Requests\KegiatanLainnya\KegiatanLainnyaRequest;
use App\Libraries\CoreSn;
use App\Models\Auths\User;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\KegiatanLainnya\KegiatanLainnya;
use App\Models\Rapat\Risalah;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use PDF;
use Yajra\DataTables\DataTables;
use ZipArchive;

class LainnyaLaporanController extends Controller
{
    protected $routes = 'kegiatan-lainnya.laporan';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setTitle('Kegiatan Lainnya Laporan');
        $this->setBreadcrumb(['Kegiatan Lainnya' => route($this->routes.'.index'), 'Laporan' => route($this->routes.'.index')]);
        // Header Grid Datatable
        $this->listStructs = [
            'listStruct' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => 'text-center',
                    'width' => '20px',
                ],
                /* --------------------------- */
                [
                    'data' => 'tahun',
                    'name' => 'tahun',
                    'label' => 'Tahun',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'rencana',
                    'name' => 'rencana',
                    'label' => 'Rencana Pelaksanaan',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'tipe',
                    'name' => 'tipe',
                    'label' => 'Tipe',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'jenis',
                    'name' => 'jenis',
                    'label' => 'Jenis',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'kategori',
                    'name' => 'kategori',
                    'label' => 'Kategori',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '200px'
                ],
                [
                    'data' => 'notulen',
                    'name' => 'notulen',
                    'label' => 'Notulen',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '150px'
                ],
                [
                    'data' => 'risalah',
                    'name' => 'risalah',
                    'label' => 'Risalah',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '100px',
                    'className' => 'text-center'
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '100px',
                    'className' => 'text-center'
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '80px',
                    'className' => 'text-center'
                ],
            ],
            'listStruct2' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => 'text-center',
                    'width' => '20px',
                ],
                /* --------------------------- */
                [
                    'data' => 'tahun',
                    'name' => 'tahun',
                    'label' => 'Tahun',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'rencana',
                    'name' => 'rencana',
                    'label' => 'Rencana Pelaksanaan',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'tipe',
                    'name' => 'tipe',
                    'label' => 'Tipe',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'jenis',
                    'name' => 'jenis',
                    'label' => 'Jenis',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'kategori',
                    'name' => 'kategori',
                    'label' => 'Kategori',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '200px'
                ],
                [
                    'data' => 'notulen',
                    'name' => 'notulen',
                    'label' => 'Notulen',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '150px'
                ],
                [
                    'data' => 'risalah',
                    'name' => 'risalah',
                    'label' => 'Risalah',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '100px',
                    'className' => 'text-center'
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '100px',
                    'className' => 'text-center'
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '80px',
                    'className' => 'text-center'
                ],
            ],
            'listStruct3' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => '#',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => 'text-center',
                    'width' => '20px',
                ],
                /* --------------------------- */
                [
                    'data' => 'tahun',
                    'name' => 'tahun',
                    'label' => 'Tahun',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'rencana',
                    'name' => 'rencana',
                    'label' => 'Rencana Pelaksanaan',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'tipe',
                    'name' => 'tipe',
                    'label' => 'Tipe',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'jenis',
                    'name' => 'jenis',
                    'label' => 'Jenis',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '100px',
                ],
                [
                    'data' => 'kategori',
                    'name' => 'kategori',
                    'label' => 'Kategori',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '200px'
                ],
                [
                    'data' => 'notulen',
                    'name' => 'notulen',
                    'label' => 'Notulen',
                    'className' => 'text-center',
                    'sortable' => true,
                    'width' => '150px'
                ],
                [
                    'data' => 'risalah',
                    'name' => 'risalah',
                    'label' => 'Risalah',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '100px',
                    'className' => 'text-center'
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '100px',
                    'className' => 'text-center'
                ],
                [
                    'data' => 'action',
                    'name' => 'action',
                    'label' => 'Aksi',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '80px',
                    'className' => 'text-center'
                ],
            ],
            'listStruct4' => [
                [
                    'data' => 'num',
                    'name' => 'num',
                    'label' => 'No',
                    'orderable' => false,
                    'searchable' => false,
                    'className' => 'text-center',
                    'width' => '20px',
                ],
                /* --------------------------- */
                [
                    'data' => 'risalah',
                    'name' => 'risalah',
                    'label' => 'Risalah',
                    'className' => 'text-left',
                    'sortable' => true,
                ],
                [
                    'data' => 'pic_1',
                    'name' => 'pic_1',
                    'label' => 'Penanggung Jawab',
                    'className' => 'text-left',
                    'sortable' => true,
                ],
                [
                    'data' => 'realisasi',
                    'name' => 'realisasi',
                    'label' => 'Realisasi',
                    'className' => 'text-left',
                    'sortable' => true,
                ],
                [
                    'data' => 'pic_2',
                    'name' => 'pic_2',
                    'label' => 'Penanggung Jawab',
                    'className' => 'text-left',
                    'sortable' => true,
                ],
                [
                    'data' => 'rencana',
                    'name' => 'rencana',
                    'label' => 'Rencana yang akan datang',
                    'className' => 'text-left',
                    'sortable' => true,
                ],
                [
                    'data' => 'pic_3',
                    'name' => 'pic_3',
                    'label' => 'Penanggung Jawab',
                    'className' => 'text-left',
                    'sortable' => true,
                ],
                [
                    'data' => 'status',
                    'name' => 'status',
                    'label' => 'Status',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '100px',
                    'className' => 'text-center'
                ],
                [
                    'data' => 'file',
                    'name' => 'file',
                    'label' => 'File',
                    'searchable' => false,
                    'sortable' => false,
                    'width' => '80px',
                    'className' => 'text-center'
                ],
            ],
        ];
    }

    public function grid()
    {
        $user = auth()->user();
        if($user->hasRole(['auditor','SVP - Internal Audit','President Director','Secretary']) || $user->isUserIa()){
            $records = KegiatanLainnya::with(['rkia', 'notulen'])
                                        ->where('status', 4)
                                        ->select('*')
                                        ->whereHas('rapat', function($rapat){
                                            $rapat->where('status_rapat', 1);
                                        });
            if(!isset(request()->order[0]['column'])) {
                  $records->orderBy('created_at','desc');
            }
            if ($tahun = request()->tahun) {
                $records->whereHas('rkia', function($rkia) use ($tahun) {
                    $rkia->whereHas('rencanaaudit', function($u) use ($tahun) {
                        $u->where('tahun', 'like', '%' . $tahun . '%');
                    });
                });
            }
        }else{
            $records = KegiatanLainnya::where('id', 0);
        }

        return DataTables::of($records->get())
            ->editColumn('num', function ($record) {
                return request()->start;
            })
            ->editColumn('tahun', function ($record) { 
                return $record->rkia->rencanaaudit->tahun;
            })
            ->editColumn('rencana', function ($record) { 
                return $record->rkia->rencana;
            })
            ->editColumn('tipe', function ($record) { 
                return '<span class="label label-success">Kegiatan Lainnya</span>';
            })
            ->editColumn('jenis', function ($record) { 
                if($record->jenis_rapat == 1){
                    return '<span class="label label-primary">Internal</span>'; 
                }else{
                    return '<span class="label label-warning">Eksternal</span>';
                }
            })
            ->editColumn('kategori', function ($record) { 
                return $record->rkia->kategori;
            })
            ->editColumn('notulen', function ($record) { 
                return $record->notulen->name;
            })
            ->editColumn('risalah', function ($record) use ($user) {
                $buttons = '';
                if ($record->notulen_rapat == auth()->id()) {
                    $buttons .= $this->makeButton([
                        'type'      => 'edit',
                        'label'     => '<i class="fa fa-book text-primary"></i>',
                        'tooltip'   => 'Risalah Rapat',
                        'class'     => 'risalah button',
                        'id'        => $record->id,
                    ]);
                }
                return $buttons;
            })
            ->editColumn('status', function ($record) {
                return '<span class="label label-info">Open</span>';            
            })
            ->editColumn('action', function ($record) use ($user) {
                $buttons = '';
                $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-eye text-primary"></i>',
                    'tooltip'   => 'Detil',
                    'class'     => 'detil button',
                    'id'        => $record->id,
                ]);
                return $buttons;
            })
            ->rawColumns(['tipe', 'jenis', 'risalah', 'status', 'action'])
            ->make(true);
    }

    public function onProgress()
    {
        $user = auth()->user();
        if($user->hasRole(['auditor','SVP - Internal Audit','President Director','Secretary']) || $user->isUserIa()){
            $records = KegiatanLainnya::with(['rkia', 'notulen'])
                                        ->where('status', 5)
                                        ->select('*')
                                        ->whereHas('rapat', function($rapat){
                                            $rapat->where('status_rapat', 1);
                                        });
            if(!isset(request()->order[0]['column'])) {
                  $records->orderBy('created_at','desc');
            }
            if ($tahun = request()->tahun) {
                $records->whereHas('rkia', function($rkia) use ($tahun) {
                    $rkia->whereHas('rencanaaudit', function($u) use ($tahun) {
                        $u->where('tahun', 'like', '%' . $tahun . '%');
                    });
                });
            }
        }else{
            $records = KegiatanLainnya::where('id', 0);
        }

        return DataTables::of($records->get())
            ->editColumn('num', function ($record) {
                return request()->start;
            })
            ->editColumn('tahun', function ($record) { 
                return $record->rkia->rencanaaudit->tahun;
            })
            ->editColumn('rencana', function ($record) { 
                return $record->rkia->rencana;
            })
            ->editColumn('tipe', function ($record) { 
                return '<span class="label label-success">Kegiatan Lainnya</span>';
            })
            ->editColumn('jenis', function ($record) { 
                if($record->jenis_rapat == 1){
                    return '<span class="label label-primary">Internal</span>'; 
                }else{
                    return '<span class="label label-warning">Eksternal</span>';
                }
            })
            ->editColumn('kategori', function ($record) { 
                return $record->rkia->kategori;
            })
            ->editColumn('notulen', function ($record) { 
                return $record->notulen->name;
            })
            ->editColumn('risalah', function ($record) use ($user) {
                $buttons = '';
                if ($record->notulen_rapat == auth()->id()) {
                    $buttons .= $this->makeButton([
                        'type'      => 'edit',
                        'label'     => '<i class="fa fa-book text-primary"></i>',
                        'tooltip'   => 'Risalah Rapat',
                        'class'     => 'risalah button',
                        'id'        => $record->id,
                    ]);
                }
                return $buttons;
            })
            ->editColumn('status', function ($record) {
                return '<span class="label label-success">On Progress</span>';            
            })
            ->editColumn('action', function ($record) use ($user) {
                $buttons = '';
                $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-eye text-primary"></i>',
                    'tooltip'   => 'Detil',
                    'class'     => 'detil button',
                    'id'        => $record->id,
                ]);
                return $buttons;
            })
            ->rawColumns(['tipe', 'jenis', 'risalah', 'status', 'action'])
            ->make(true);
    }

    public function historis()
    {
        $user = auth()->user();
        if($user->hasRole(['auditor','SVP - Internal Audit','President Director','Secretary']) || $user->isUserIa()){
            $records = KegiatanLainnya::with(['rkia', 'notulen'])
                                        ->where('status', 6)
                                        ->select('*')
                                        ->whereHas('rapat', function($rapat){
                                            $rapat->where('status_rapat', 1);
                                        });
            if(!isset(request()->order[0]['column'])) {
                  $records->orderBy('created_at','desc');
            }
            if ($tahun = request()->tahun) {
                $records->whereHas('rkia', function($rkia) use ($tahun) {
                    $rkia->whereHas('rencanaaudit', function($u) use ($tahun) {
                        $u->where('tahun', 'like', '%' . $tahun . '%');
                    });
                });
            }
        }else{
            $records = KegiatanLainnya::where('id', 0);
        }

        return DataTables::of($records->get())
            ->editColumn('num', function ($record) {
                return request()->start;
            })
            ->editColumn('tahun', function ($record) { 
                return $record->rkia->rencanaaudit->tahun;
            })
            ->editColumn('rencana', function ($record) { 
                return $record->rkia->rencana;
            })
            ->editColumn('tipe', function ($record) { 
                return '<span class="label label-success">Kegiatan Lainnya</span>';
            })
            ->editColumn('jenis', function ($record) { 
                if($record->jenis_rapat == 1){
                    return '<span class="label label-primary">Internal</span>'; 
                }else{
                    return '<span class="label label-warning">Eksternal</span>';
                }
            })
            ->editColumn('kategori', function ($record) { 
                return $record->rkia->kategori;
            })
            ->editColumn('notulen', function ($record) { 
                return $record->notulen->name;
            })
            ->editColumn('risalah', function ($record) use ($user) {
                $buttons = '';
                if ($record->notulen_rapat == auth()->id()) {
                    $buttons .= $this->makeButton([
                        'type'      => 'edit',
                        'label'     => '<i class="fa fa-book text-primary"></i>',
                        'tooltip'   => 'Risalah Rapat',
                        'class'     => 'risalah button',
                        'id'        => $record->id,
                    ]);
                }
                return $buttons;
            })
            ->editColumn('status', function ($record) {
                return '<span class="label label-danger">Closed</span>';            
            })
            ->editColumn('action', function ($record) use ($user) {
                $buttons = '';
                $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-eye text-primary"></i>',
                    'tooltip'   => 'Detil',
                    'class'     => 'detil button',
                    'id'        => $record->id,
                ]);
                $buttons .= $this->makeButton([
                    'type'      => 'edit',
                    'label'     => '<i class="fa fa-print text-default"></i>',
                    'tooltip'   => 'Cetak',
                    'class'     => 'm-l cetak button',
                    'id'        => $record->id,
                ]);
                return $buttons;
            })
            ->rawColumns(['tipe', 'jenis', 'risalah', 'status', 'action'])
            ->make(true);
    }

    public function gridRisalah($id)
    {
        $user = auth()->user();
        $records = Risalah::whereHas('rapat', function($rapat) use ($id) {
                                $rapat->whereHas('lainnya', function($lainnya) use ($id) {
                                    $lainnya->where('id', $id);
                                });
                            });

        return DataTables::of($records->get())
            ->editColumn('num', function ($record) {
                return request()->start;
            })
            ->editColumn('risalah', function ($record) { 
                return $record->risalah;
            })
            ->editColumn('pic_1', function ($record) { 
                return $record->pic_1->name ?? $record->pen_1;
            })
            ->editColumn('realisasi', function ($record) { 
                return $record->realisasi;
            })
            ->editColumn('pic_2', function ($record) { 
                return $record->pic_2->name ?? $record->pen_2;
            })
            ->editColumn('rencana', function ($record) { 
                return $record->rencana;
            })
            ->editColumn('pic_3', function ($record) { 
                return $record->pic_3->name ?? $record->pen_3;
            })
            ->editColumn('status', function ($record) {
                $buttons = '<div class="btn-group-vertical" style="width: 110px;">';
                if ($record->status == 0) {
                    $buttons .= '<button type="button" class="btn btn-info update-status button 0" data-id="'.$record->rapat->lainnya->id.'" data-risalah="'.$record->id.'" data-status="0">Open <i class="fa fa-check ml-1"></i></button>';
                    $buttons .= '<button type="button" class="btn btn-success update-status button 1" data-id="'.$record->rapat->lainnya->id.'" data-risalah="'.$record->id.'" data-status="1">On Progress <i class="fa fa-check ml-1 hide"></i></button>';
                    $buttons .= '<button type="button" class="btn btn-danger update-status button 2" data-id="'.$record->rapat->lainnya->id.'" data-risalah="'.$record->id.'" data-status="2">Closed <i class="fa fa-check ml-1 hide"></i></button>';
                }elseif ($record->status == 1) {
                    $buttons .= '<button type="button" class="btn btn-success update-status button 1" data-id="'.$record->rapat->lainnya->id.'" data-risalah="'.$record->id.'" data-status="1">On Progress <i class="fa fa-check ml-1"></i></button>';
                    $buttons .= '<button type="button" class="btn btn-danger update-status button 2" data-id="'.$record->rapat->lainnya->id.'" data-risalah="'.$record->id.'" data-status="2">Closed <i class="fa fa-check ml-1 hide"></i></button>';
                }elseif ($record->status == 2) {
                    $buttons .= '<button type="button" class="btn btn-danger update-status button 2 disabled" data-id="'.$record->rapat->lainnya->id.'" data-risalah="'.$record->id.'" data-status="2">Closed</button>';
                }
                $buttons .= '</div>';
                return $buttons;
            })
            ->editColumn('file', function ($record) use ($user) {
                $buttons = '';
                if ($record->status != 2) {
                    $buttons .= $this->makeButton([
                        'type'    => 'edit',
                        'label'   => '<i class="fa fa-upload text-primary"></i>',
                        'tooltip' => 'Upload File',
                        'class'   => 'upload-file button',
                        'id'      => $record->rapat->lainnya->id,
                        'datas'    => ['risalah_id' => $record->id]
                    ]);
                }
                if (count($record->files) > 0) {
                    $ml = '';
                    if($record->status != 2){
                        $ml = 'm-l';
                    }
                    $buttons .= $this->makeButton([
                        'type'    => 'edit',
                        'label'   => '<i class="fa fa-download text-primary"></i>',
                        'tooltip' => 'Download File',
                        'class'   => 'download-file button '.$ml,
                        'id'      => $record->rapat->lainnya->id,
                        'datas'    => ['risalah_id' => $record->id]
                    ]);
                }
                return $buttons;
            })
            ->rawColumns(['status', 'file'])
            ->make(true);
    }

    public function index()
    {
        $badgeTab = KegiatanLainnya::badgeTab('laporan');
        return $this->render('modules.lainnya.laporan.index', [
            'structs'    => $this->listStructs,
            'baru'       => $badgeTab->baru,
            'onProgress' => $badgeTab->onProgress
        ]);
    }

    public function show(KegiatanLainnya $laporan)
    {
        $this->pushBreadcrumb(['Detil' => route($this->routes.'.edit', $laporan->id)]);
        if ($laporan->jenis_rapat == 1) {
            return $this->render('modules.lainnya.laporan.detail-internal', ['record' => $laporan]);
        }else{
            return $this->render('modules.lainnya.laporan.detail-eksternal', ['record' => $laporan]);
        }
    }

    public function cetak(KegiatanLainnya $laporan)
    {
        $data = [
            'records' => $laporan->rapat,
            'today' => CoreSn::DateToString(Carbon::now()),
        ];
        $pdf = PDF::loadView('modules.rapat.internal.cetak', $data)->setPaper('a4', 'landscape');
        return $pdf->stream('Risalah Rapat - '.DateToStringWDay($laporan->rapat->tanggal).'.pdf',array("Attachment" => false));
        // $user = auth()->user();
        // if($user->hasRole(['auditor','SVP - Internal Audit','President Director','Secretary']) || $user->isUserIa()){
        //     $data = [
        //         'records' => $laporan,
        //         'today' => CoreSn::DateToString(Carbon::now()),
        //     ];
        //     $pdf = PDF::loadView('modules.lainnya.laporan.cetak', $data)->setPaper('a4', 'landscape');
        //     return $pdf->stream('Risalah Rapat - '.DateToStringWDay($laporan->rapat->tanggal).'.pdf',array("Attachment" => false));
        // }
        // return abort(404);
    }

    public function risalah(KegiatanLainnya $laporan)
    {
        $this->pushBreadcrumb(['Risalah Rapat' => route($this->routes.'.risalah', $laporan->id)]);
        return $this->render('modules.lainnya.laporan.risalah', [
            'structs'    => $this->listStructs,
            'record' => $laporan
        ]);
    }

    public function updateRisalah(Request $request, KegiatanLainnya $laporan)
    {
        return $laporan->saveFromRequest($request, 'laporan-update-risalah');
    }

    public function formUploadFile(KegiatanLainnya $laporan, Risalah $risalah)
    {
        return $this->render('modules.lainnya.laporan.upload', [
            'record' => $laporan,
            'risalah' => $risalah,
        ]);
    }

    public function uploadFile(KegiatanLainnyaRequest $request, KegiatanLainnya $laporan)
    {
        return $laporan->saveFromRequest($request, 'laporan-upload-file-risalah');
    }

    public function downloadFile(KegiatanLainnya $laporan, Risalah $risalah)
    {
        return $risalah->zipRisalahFile();
    }

}
