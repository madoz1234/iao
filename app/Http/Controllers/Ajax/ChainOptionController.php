<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Master\FokusAudit;
use App\Models\Master\BU;
use App\Models\Master\CO;
use App\Models\Master\Project;
use App\Models\Master\AnakPerusahaan;
use App\Models\Master\SpinPof;
use App\Models\Master\SpinPemenuhan;
use App\Models\Master\SpinPembobotanDetail;
use App\Models\Master\SpinPemenuhanDetail;
use App\Models\Master\KriteriaTemuan;
use App\Models\Master\KriteriaTemuanDetail;
use App\Models\Auths\User;
use App\Models\Master\BUDetailPic;
use App\Models\Master\CODetailPic;
use App\Models\Master\ProjectDetailPic;
use App\Models\Master\StandarisasiTemuan;
use App\Models\Master\StandarisasiTemuanDetail;
use App\Models\KegiatanAudit\Rencana\RencanaAudit;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\Survey\SurveyJawab;
use App\Models\Master\Temuan;
use App\Models\Master\TemuanDetail;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaDetail;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaDetailRekomendasi;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit;
use App\Models\KegiatanAudit\TindakLanjut\RegisterTLDetail;
use App\Models\Auths\Role;
use App\Models\Attachments;
use App\Models\Files;


use Datatables;

class ChainOptionController extends Controller
{
    
    function __construct()
    {
        
    }

    public function getFokusAudit(Request $request)
    {
        $bidang = $request->bidang;
        $fokus = $request->fokus;
        $selected = $request->selected;
        if($bidang){
        	if($fokus){
        		return FokusAudit::options('audit', 'id', [
		            'filters' => [
		                function ($q) use ($bidang,$fokus) {
		                    $q->where('bidang', $bidang);
		                }
		            ],
		            'selected' => $selected
		        ], '(Pilih Salah Satu)');
        	}else{
		    	return FokusAudit::options('audit', 'id', [
		            'filters' => [
		                function ($q) use ($bidang,$fokus) {
		                    $q->where('bidang', $bidang)
		                      ->doesntHave('langkahkerja');
		                }
		            ],
		            'selected' => $selected
		        ], '(Pilih Salah Satu)');
        	}
        }else{
	        return FokusAudit::options('audit', 'id', [
	            'filters' => [
	                function ($q) use ($bidang,$fokus) {
	                    $q->where('bidang', $bidang)
	                      ->doesntHave('langkahkerja');
	                }
	            ],
	            'selected' => $selected
	        ], '(Pilih Salah Satu)');
        }
    }

    public function getLangkah(Request $request)
    {
    	if($request->tipe == 1){
	        $id 	= $request->id;
	        $value 	= $request->value;
			$data = FokusAudit::find($value);
			$col =5;
			$ciks ='';
			foreach ($data->langkahkerja as $key => $val) {
				foreach ($val->detail as $kiy => $vil) {
					$cik[$kiy]= '<tr class="langkah-kerja-operasional-'.$id.' data_detail_operasional-'.$id.'-'.$kiy.'"><td>'.column_letter($kiy+1).'</td><td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="5">'.$vil->deskripsi.'</td></tr>';
				}
			}
			$pas[0]=$id;
			if($data->langkahkerja->count() > 0){
				$pas[1]=$cik;
			}else{
				$pas[1]=$ciks;
			}
			$pas[2]=1;
			return $pas;
    	}elseif($request->tipe == 2){
    		$id 	= $request->id;
	        $value 	= $request->value;
			$data = FokusAudit::find($value);
			$col =5;
			$ciks ='';
			foreach ($data->langkahkerja as $key => $val) {
				foreach ($val->detail as $kiy => $vil) {
					$cik[$kiy]= '<tr class="langkah-kerja-keuangan-'.$id.' data_detail_keuangan-'.$id.'-'.$kiy.'"><td>'.column_letter($kiy+1).'</td><td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="5">'.$vil->deskripsi.'</td></tr>';
				}
			}
			$pas[0]=$id;
			if($data->langkahkerja->count() > 0){
				$pas[1]=$cik;
			}else{
				$pas[1]=$ciks;
			}
			$pas[2]=2;
			return $pas;
    	}else{
    		$id 	= $request->id;
	        $value 	= $request->value;
			$data = FokusAudit::find($value);
			$col =5;
			$ciks ='';
			foreach ($data->langkahkerja as $key => $val) {
				foreach ($val->detail as $kiy => $vil) {
					$cik[$kiy]= '<tr class="langkah-kerja-sistem-'.$id.' data_detail_sistem-'.$id.'-'.$kiy.'"><td>'.column_letter($kiy+1).'</td><td style="text-align: left;border-right: 1px solid rgba(34,36,38,.1);" colspan="5">'.$vil->deskripsi.'</td></tr>';
				}
			}
			$pas[0]=$id;
			if($data->langkahkerja->count() > 0){
				$pas[1]=$cik;
			}else{
				$pas[1]=$ciks;
			}
			$pas[2]=3;
			return $pas;
    	}
    }

    public function getKategori(Request $request)
    {
        $kategori = $request->kategori;
        $selected = $request->selected;
        if($kategori == 0){
        	return BU::options('nama', 'id', [
	            'filters' => [],
	            'selected' => $selected
	        ], '(Pilih Salah Satu)');
        }elseif($kategori == 1){
        	return CO::options('nama', 'id', [
	            'filters' => [],
	            'selected' => $selected
	        ], '(Pilih Salah Satu)');
        }elseif($kategori == 2){
        	return Project::options('nama', 'id', [
	            'filters' => [],
	            'selected' => $selected
	        ], '(Pilih Salah Satu)');
        }elseif($kategori == 3){
        	return AnakPerusahaan::options('nama', 'id', [
	            'filters' => [],
	            'selected' => $selected
	        ], '(Pilih Salah Satu)');
        }else{

        }
    }

    public function getKategoris(Request $request)
    {
        $kategori = $request->kategori;
        $selected = $request->selected;
        if($kategori == 1){
        	return BU::optionsa('nama', 'id', [
	            'filters' => [],
	            'selected' => $selected
	        ], '(Pilih Salah Satu)');
        }elseif($kategori == 2){
        	return CO::optionsa('nama', 'id', [
	            'filters' => [],
	            'selected' => $selected
	        ], '(Pilih Salah Satu)');
        }elseif($kategori == 3){
        	return Project::optionsa('nama', 'id', [
	            'filters' => [],
	            'selected' => $selected
	        ], '(Pilih Salah Satu)');
        }elseif($kategori == 4){
        	return AnakPerusahaan::optionsa('nama', 'id', [
	            'filters' => [],
	            'selected' => $selected
	        ], '(Pilih Salah Satu)');
        }else{

        }
    }

    public function getKategoriz(Request $request)
    {
        $kategori = $request->kategori;
        $selected = $request->selected;
        if($kategori == 1){
        	return BU::optionsz('nama', 'id', [
	            'filters' => [],
	            'selected' => $selected
	        ], '(Pilih Salah Satu)');
        }elseif($kategori == 2){
        	return CO::optionsz('nama', 'id', [
	            'filters' => [],
	            'selected' => $selected
	        ], '(Pilih Salah Satu)');
        }elseif($kategori == 3){
        	return Project::optionsz('nama', 'id', [
	            'filters' => [],
	            'selected' => $selected
	        ], '(Pilih Salah Satu)');
        }elseif($kategori == 4){
        	return AnakPerusahaan::optionsz('nama', 'id', [
	            'filters' => [],
	            'selected' => $selected
	        ], '(Pilih Salah Satu)');
        }else{

        }
    }


    public function getPof(Request $request)
    {
        $id = $request->pof;
        $data = SpinPof::find($id);
        $cik='';
        foreach ($data->pemenuhan as $key => $val) {
        	$cik .='<ol class="list list-more1" style="text-align:justify;text-justify:inter-word;">';
        	foreach ($val->detail as $kiy => $valdetail) {
        		$cik.= '<li class="item"><p>'.readMoreText($valdetail->deskripsi, 100).'</p></li>';
        	}
        	$cik .='</ol>';
        }
		$pas[]=$cik;
		return $pas;
	}

	public function getBobot(Request $request)
    {
        $kuy = $request->id;
        $pof = $request->pof;
        $data = SpinPof::find($pof);
        foreach ($data->pemenuhan as $key => $val) {
        	foreach ($val->detail as $kiy => $valdetail) {
        		$cik[$kiy]= '<div class="input-group field">
                                <input type="hidden" class="form-control" name="detailNilai['.$kuy.'][data]['.$kiy.'][pemenuhan_detail_id]" placeholder="Bobot" value="'.$valdetail->id.'">
                                <input type="text" class="form-control inputmask-'.$kuy.' cek-data ambil-'.$kuy.' change-nilai" data-id="'.$kuy.'" data-detail="'.$kiy.'" name="detailNilai['.$kuy.'][data]['.$kiy.'][nilai]" placeholder="Bobot" maxlength="7">
                                <span class="input-group-addon" style="font-size: 10px;">% </span>
                            </div><br>';
        	}
        }

		$pas[]=$cik;
		$data = SpinPof::find($pof);
		return $pas;
	}

	public function getStandarisasis(Request $request)
    {
        $id = $request->id;
        $selected = $request->selected;
        $cek = StandarisasiTemuan::where('bidang', $id)->doesntHave('temuan')->get();
        $ar='';
        if($cek){
	        $ar .= '<option value="">(Pilih Salah Satu)</option>';
	        foreach ($cek as $key => $value) {
	        	$ar .= '<option value="'.$value->id.'">'.$value->kode.' - '.$value->deskripsi.'</option>';
	        }
        }
        return $ar;
	}

	public function getStandarisasiss(Request $request)
    {
        $id = $request->id;
        $val = $request->val;
        $selected = $request->selected;
        $cek = StandarisasiTemuan::where(function($u) use ($id, $val){
	        							$u->where(function($x) use ($id, $val){
	        								$x->doesntHave('temuan');
	        							})->orWhere(function($y) use ($id, $val){
	        								$y->where('id', $val);
	        							});
        						   })->get();
        $ar='';
        if($cek){
	        $ar .= '<option value="">(Pilih Salah Satu)</option>';
	        foreach ($cek as $key => $value) {
	        	$ar .= '<option value="'.$value->id.'">'.$value->kode.' - '.$value->deskripsi.'</option>';
	        }
        }
        return $ar;
	}

	public function getStandarisasi(Request $request)
    {
        $kuy = $request->id;
        $cek = StandarisasiTemuan::find($kuy);
        $cik='';
        foreach($cek->detail as $data){
        	$cik.= '<li class="item cek-cek" style="text-align:justify">'.readMoreText($data->deskripsi, 100).'</li>';
        }
        return $cik;
	}


    public function getLokasi(Request $request)
    {
        $lokasi = $request->lokasi;
        $project = Project::find($lokasi);
        return $project->alamat;
    }

    public function getNoAb(Request $request)
    {
        $lokasi = $request->lokasi;
        $project = Project::find($lokasi);
        return $project->project_ab;
    }

    public function getNoId(Request $request)
    {
        $lokasi = $request->lokasi;
        $project = Project::find($lokasi);
        return $project->project_id;
    }

// }

    public function getKriteriaTemuan(Request $request)
    {
        $kategori = $request->id;
        $selected = $request->selected;
        $temuan = KriteriaTemuan::where('kategori_id', $kategori)->first();
        if($temuan){
	    	return KriteriaTemuanDetail::options('deskripsi', 'id', [
	            'filters' => [
	                function ($q) use ($temuan) {
	                    $q->where('kriteria_id', $temuan->id);
	                }
	            ],
	            'selected' => $selected
	        ], '(Pilih Salah Satu)');
        }else{

        }
    }

    public function getKriteriaTemuans(Request $request)
    {
    	$cek[] ='';
        $standarisasi = $request->id;
        $selected = $request->selected;
        $standar = StandarisasiTemuan::find($request->id);
        $data = StandarisasiTemuanDetail::where('standarisasi_id', $standarisasi)->get();
        $cek2 = '';
        foreach ($data as $key => $value) {
        	$cek2.= '<li style="text-align:justify">'.readMoreText($value->deskripsi, 100).'</li>';
        }
        $cek[0]= $standar->kode;
        $cek[1]= $cek2;
        return $cek;
    }

    public function getKodeTemuan(Request $request)
    {
    	$temuan = TemuanDetail::find($request->id);
        return $temuan->kode;
    }

    public function getUser(Request $request)
    {
        $id = $request->user_id;
        $data = User::find($id);
        // dd($data->roles->first()->name);
        // ambil bu/co
        if($data->kategori == 1){
            $bu_nama = $data->bu ? $data->bu->nama : '-';
        }else if($data->kategori == 2){
            $bu_nama = $data->co ? $data->co->nama : '-';
        }else{
            $bu_nama = '-';
        }

        if ($data->hasRole(['svp'])) { //cek role SVP
        	$cektype = BUDetailPic::where('pic', $data->id)->first();
        	if($cektype){
        		$bu = $cektype->bu;
                if ($bu->detailproject->count() >= 2) {
                    $project[] = '<option value="0">Tidak ada pilihan</option>';
                    foreach ($bu->detailproject as $key => $val) {
                        $project[] = '<option value="'.$val->id.'">'.$val->nama.'</option>';
                    }
                } elseif($bu->detailproject->count() == 1) {
                    $project[] = '<option value="0">Tidak ada pilihan</option>';
                    foreach ($bu->detailproject as $key => $val) {
                        $project[] = '<option selected value="'.$val->id.'">'.$val->nama.'</option>';
                    }
                } else {
                    $project[] = '<option selected disable value="0">Tidak ada pilihan</option>';
                }
        		// $project[] = '-';
        		// foreach ($bu->detailproject as $key => $val) {
        		// 	$project[] = '<option value="'.$val->id.'">'.$val->nama.'</option>';
        		// }
        		return response()->json([
                    'email'      => $data->email,
        			'phone' 	 => ($data->phone == '') ? '-' : $data->phone,
                    'bu'         => $bu_nama,
                    'jabatan'    => $data->posisi ? $data->posisi : $data->roles->first()->name,
        			// 'bu'    	 => $bu->nama,
        			'project'    => $project,
        		]);
        	} else {
        		$carico = CODetailPic::where('pic', $data->id)->first();
                // dd($carico);
                if ($carico == '') {
                    $bu = '-';
                } else {
                    $bu = $carico->co->nama;
                }
        		return response()->json([
        			'email' 	 => $data->email,
                    'phone'      => ($data->phone == '') ? '-' : $data->phone,
        			'bu'    	 => $bu_nama,
                    'jabatan'    => $data->posisi ? $data->posisi : $data->roles->first()->name,
        			'project'    => '<option selected disable value="0">Tidak ada pilihan</option>',
        		]);
        	}
        }elseif ($data->hasRole(['kapro'])) { //cek role kapro
        	$record = ProjectDetailPic::where('pic', $data->id)->get();
            // dd($record->count());
            if ($record->count() >= 2) {
                $project[] = '<option value="0">Tidak ada pilihan</option>';
                foreach ($record as $val) {
                    $project[] = '<option value="'.$val->project->id.'">'.$val->project->nama.'</option>';
                }
            } elseif($record->count() == 1) {
                $project[] = '<option value="0">Tidak ada pilihan</option>';
                foreach ($record as $val) {
                    $project[] = '<option selected value="'.$val->project->id.'">'.$val->project->nama.'</option>';
                }
            } else {
                    $project[] = '<option selected disable value="0">Tidak ada pilihan</option>';
            }
    //     	$project[] = '-';
    //     	foreach ($record as $val) {
				// $project[] = '<option value="'.$val->project->id.'">'.$val->project->nama.'</option>';
    //     	}
        	return response()->json([
	            'email' 	 => $data->email,
                'phone'      => ($data->phone == '') ? '-' : $data->phone,
                'bu'         => $bu_nama,
                'jabatan'    => $data->posisi ? $data->posisi : $data->roles->first()->name,
                // 'bu'      => '-',
	            'project'    => $project,
        	]);
        }else{  //cek role bukan spv & kapro
        	return response()->json([
	            'email' 	 => $data->email,
                'phone'      => ($data->phone == '') ? '-' : $data->phone,
                'bu'         => $bu_nama,
                'jabatan'    => $data->posisi ? $data->posisi : $data->roles->first()->name, 
	            // 'bu'    	 => '-',
	            'project'    => '<option selected disable value="0">Tidak ada pilihan</option>',
        	]);
        }
	}

    public function getProject(Request $request)
    {
        $id = $request->project_id;
        $data = Project::find($id);
        // dd($data);
        // dd($data->bu->kode);
        if ($data == '') {
            $bu = '-';
        } else {
            if ($data->bu == '') {
                $bu = '-';
            } else {
                $bu = $data->bu->kode;
            }
        }
        return response()->json([
                'bu'         => $bu,
	            // 'bu'    	 => $data->bu->kode,
        	]);
	}

    public function getRoles(Request $request)
    {
        $postData = $request->tipe;
        // dd($postData);
        // if ($postData == 1) {
        //     dd('pegawai');
        // } else {
        //     dd('non-pegawai');
        // }
        echo Role::options('name', 'id', [
                'filters' => ['id' => $postData],
            ], '(Kabupaten/Kota)');
    }

    public function getFile(Request $request)
    {	
    	$tampil = array();
        $cari =Files::where('target_id', $request->id)->where('target_type', 'dokumen-detail')->first();
    	if($cari){
            $url = $cari->url;
            $show = asset('storage/'.$url);

            $type = $cari->type;
            $filename = $cari->filename;
			$ext = pathinfo($url, PATHINFO_EXTENSION);
            if($ext == 'png' OR $ext == 'jpg' OR $ext == 'jpeg' OR $ext == 'gif' OR $ext == 'ico' OR $ext == 'apng' OR $ext == 'xlsx'){
            	if($ext == 'xlsx'){
            		$ext ='office';
            	}else{
            		$ext=$ext;
            	}
                $arr[]= [
                    'caption' => $filename,
                    'type' => $ext,
                    'downloadUrl' => $show,
                    'size' => 930321,
                    'width' => "120px",
                    'key' => 1
                ];
            }else{
                $arr[]= [
                	'caption' => $filename,
                    'type' => 'pdf',
                    'caption' => $filename,
                    'downloadUrl' => $show,
                    'size' => 930321,
                    'width' => "120px",
                    'key' => 1
                ];
            }

            $tampil[0] = json_encode($show);
            $tampil[1] = json_encode($arr);
    	}
    	return $tampil;
    }


    public function getFileKka(Request $request)
    {	
    	$tampil = array();
        $cari =Files::where('target_id', $request->id)->where('target_type', 'kka')->first();
    	if($cari){
            $url = $cari->url;
            $show = asset('storage/'.$url);

            $type = $cari->type;
            $filename = $cari->filename;
			$ext = pathinfo($url, PATHINFO_EXTENSION);
            if($ext == 'png' OR $ext == 'jpg' OR $ext == 'jpeg' OR $ext == 'gif' OR $ext == 'ico' OR $ext == 'xlsx'){
            	if($ext == 'xlsx'){
            		$ext ='office';
            	}elseif($ext == 'png' OR $ext == 'jpg' OR $ext == 'jpeg' OR $ext == 'gif' OR $ext == 'ico'){
            		$ext ='image';
            	}else{
            		$ext=$ext;
            	}
                $arr[]= [
                    'caption' => $filename,
                    'type' => $ext,
                    'downloadUrl' => $show,
                    'size' => 930321,
                    'width' => "120px",
                    'key' => 1
                ];
            }else{
                $arr[]= [
                	'caption' => $filename,
                	'type' => 'pdf',
                    'caption' => $filename,
                    'downloadUrl' => $show,
                    'size' => 930321,
                    'width' => "120px",
                    'key' => 1
                ];
            }

            $tampil[0] = json_encode($show);
            $tampil[1] = json_encode($arr);
    	}
    	return $tampil;
    }

    public function getFileBA(Request $request)
    {	
    	$tampil = array();
        $cari = DraftKkaDetail::find($request->id);
    	if($cari){
            $ba = $cari->ba;
            $show = asset('storage/'.$ba);

            $type = $cari->type;
            $filename = $cari->filename;
			$ext = pathinfo($ba, PATHINFO_EXTENSION);
            if($ext == 'png' OR $ext == 'jpg' OR $ext == 'jpeg' OR $ext == 'gif' OR $ext == 'ico' OR $ext == 'xlsx'){
            	if($ext == 'xlsx'){
            		$ext ='office';
            	}else{
            		$ext=$ext;
            	}
                $arr[]= [
                    'caption' => $filename,
                    'downloadUrl' => $show,
                    'size' => 930321,
                    'width' => "120px",
                    'key' => 1
                ];
            }else{
                $arr[]= [
                	'caption' => $filename,
                    'caption' => $filename,
                    'downloadUrl' => $show,
                    'size' => 930321,
                    'width' => "120px",
                    'key' => 1
                ];
            }

            $tampil[0] = json_encode($show);
            $tampil[1] = json_encode($arr);
    	}
    	return $tampil;
    }


    public function getFileTL(Request $request)
    {	
    	$tampil = array();
        $cari = RegisterTLDetail::where('rekomendasi_id', $request->id)->first();
    	if($cari->detaillampiran){
        	foreach ($cari->detaillampiran as $key => $value) {
	            $url = $value->url;
	            $filename = $value->filename;
				$ext = pathinfo($url, PATHINFO_EXTENSION);
	            if($ext == 'png' OR $ext == 'jpg' OR $ext == 'jpeg' OR $ext == 'gif' OR $ext == 'ico' OR $ext == 'xlsx'){
	            	if($ext == 'xlsx'){
	            		$ext ='office';
	            	}else{
	            		$ext=$ext;
	            	}
	                $arr[]= [
	                    'caption' => $filename,
	                    'type' => $ext,
	                    'downloadUrl' => asset('storage/'.$url),
	                    'size' => 930321,
	                    'width' => "120px",
	                    'key' => 1
	                ];
	            }else{
	                $arr[]= [
	                	'caption' => $filename,
	                	'type' => 'pdf',
	                    'downloadUrl' => asset('storage/'.$url),
	                    'size' => 930321,
	                    'width' => "120px",
	                    'key' => 1
	                ];
	            }
	            $show[]= asset('storage/'.$url);
	            $tampil[0] = json_encode($show);
	            $tampil[1] = json_encode($arr);
        	}
    	}
    	return $tampil;
    }

    public function getData(Request $request)
    {	
    	$data 		= RencanaAuditDetail::whereIn('id', $request->id)->get();
    	$datas 		= RencanaAudit::whereIn('id', $data->pluck('rencana_id')->toArray())->get();
    	$cek_tahun 	= count($datas->pluck('tahun')->toArray());
    	$cek_tipe 	= array_unique($data->pluck('tipe_object')->toArray());
    	$cek_object = array_unique($data->pluck('object_id')->toArray());
    	$obs = array();
    	$ob = array();
    	if($cek_tahun > 1){
    		return 1;
    	}else{
    		return 4;
    		// if(count($cek_tipe) > 1){
    		// 	if(count($cek_tipe) <=2){
    		// 		foreach ($data->where('tipe_object', 2) as $key => $value) {
    		// 			$ob[]= findbu($value->object_id);
    		// 		}

    		// 		foreach ($data->where('tipe_object', 0) as $key => $value) {
    		// 			$obs[]=$value->object_id;
    		// 		}
    		// 		$cek = array_unique($ob);
    		// 		$ceks = array_unique($obs);
    		// 		if($cek > 1){
    		// 			return 2;
    		// 		}else{
    		// 			if(count($ceks) > 1){
    		// 				return 2;
    		// 			}else{
    		// 				if($ceks){
	    	// 					if($ceks[0] == $cek[0]){
	    	// 						return 4;
	    	// 					}else{
	    	// 						return 2;
	    	// 					}
    		// 				}else{
    		// 					return 2;
    		// 				}
    		// 			}
    		// 		}
    		// 	}else{
	    	// 		return 2;
    		// 	}
    		// }else{
    		// 	if($cek_tipe[0] == 2){
    		// 		$cek_project = Project::whereIn('id', $cek_object)->pluck('bu_id')->toArray();
    		// 		if(count(array_unique($cek_project)) > 1){
    		// 			return 3;
    		// 		}else{
    		// 			return 4;
    		// 		}
    		// 	}else{
	    	// 		if(count($cek_object) > 1){
		    // 			return 3;
		    // 		}else{
		    // 			return 4;
		    // 		}
    		// 	}
    		// }
    	}
    }

    public function getDatas(Request $request)
    {	
    	$penugasanaudit = PenugasanAudit::whereIn('id', $request->id)->get();
    	$data 		= RencanaAuditDetail::whereIn('id', $penugasanaudit->pluck('rencana_id')->toArray())->get();
    	$datas 		= RencanaAudit::whereIn('id', $data->pluck('rencana_id')->toArray())->get();
    	$cek_tahun 	= count($datas->pluck('tahun')->toArray());
    	$cek_tipe 	= array_unique($data->pluck('tipe_object')->toArray());
    	$cek_object = array_unique($data->pluck('object_id')->toArray());
    	$obs = array();
    	$ob = array();
    	if($cek_tahun > 1){
    		return 1;
    	}else{
    		return 4;
    		// if(count($cek_tipe) > 1){
    		// 	if(count($cek_tipe) <=2){
    		// 		foreach ($data->where('tipe_object', 2) as $key => $value) {
    		// 			$ob[]= findbu($value->object_id);
    		// 		}

    		// 		foreach ($data->where('tipe_object', 0) as $key => $value) {
    		// 			$obs[]=$value->object_id;
    		// 		}
    		// 		$cek = array_unique($ob);
    		// 		$ceks = array_unique($obs);
    		// 		if($cek > 1){
    		// 			return 2;
    		// 		}else{
    		// 			if(count($ceks) > 1){
    		// 				return 2;
    		// 			}else{
    		// 				if($ceks){
	    	// 					if($ceks[0] == $cek[0]){
	    	// 						return 4;
	    	// 					}else{
	    	// 						return 2;
	    	// 					}
    		// 				}else{
    		// 					return 2;
    		// 				}
    		// 			}
    		// 		}
    		// 	}else{
	    	// 		return 2;
    		// 	}
    		// }else{
    		// 	if($cek_tipe[0] == 2){
    		// 		$cek_project = Project::whereIn('id', $cek_object)->pluck('bu_id')->toArray();
    		// 		if(count(array_unique($cek_project)) > 1){
    		// 			return 3;
    		// 		}else{
    		// 			return 4;
    		// 		}
    		// 	}else{
	    	// 		if(count($cek_object) > 1){
		    // 			return 3;
		    // 		}else{
		    // 			return 4;
		    // 		}
    		// 	}
    		// }
    	}
    }

    public function getAllDataDashboard(Request $request)
    {	
    	$tahun = $request->val;
    	$data = array();
    	
    	/*KEGIATAN AUDIT*/
    	$kegiatan_audit_all 	 = RencanaAuditDetail::whereHas('rencanaaudit', function($u) use ($tahun){
    													$u->where('tahun', $tahun);
    												 })
    												 ->where('flag', 1)->where('tipe', 0)->where('status_lha', 0)->get()->count();
    	$kegiatan_audit_close 	 = RencanaAuditDetail::whereHas('rencanaaudit', function($u) use ($tahun){
    													$u->where('tahun', $tahun);
    												 })
    												 ->where('flag', 1)->where('tipe', 0)->where('status_lha', 1)->get()->count();
        
        /* KEGIATAN KONSULTASI*/
    	$kegiatan_konsultasi_all = RencanaAuditDetail::whereHas('rencanaaudit', function($u) use ($tahun){
    													$u->where('tahun', $tahun);
    												 })
    												 ->where('flag', 1)->where('tipe', 1)->get()->count();
    	$kegiatan_konsultasi_close = 0;

    	/*KEGIATAN LAIN*/

        $kegiatan_lain_all 		= RencanaAuditDetail::whereHas('rencanaaudit', function($u) use ($tahun){
    													$u->where('tahun', $tahun);
    												 })
    												 ->where('flag', 1)->where('tipe', 2)->get()->count();
    	$kegiatan_lain_close = 0;


    	/*SURVEY*/
    	$survey_all 		= SurveyJawab::whereHas('lha', function($lha) use ($tahun){	
							                    $lha->whereHas('draftkka', function($a) use ($tahun){
					                                $a->whereHas('program', function($b) use ($tahun){
				                                        $b->whereHas('penugasanaudit', function($d) use ($tahun){
				                                            $d->whereHas('rencanadetail', function($e) use ($tahun){
				                                                $e->whereHas('rencanaaudit', function($f) use ($tahun){
				                                                    $f->where('tahun', 'like', '%' . $tahun . '%');
				                                                });
				                                            });
				                                        });
					                                });
							                    });
                							})->get()->count();
    	$survey_close 	= SurveyJawab::whereHas('lha', function($lha) use ($tahun){	
							                    $lha->whereHas('draftkka', function($a) use ($tahun){
					                                $a->whereHas('program', function($b) use ($tahun){
				                                        $b->whereHas('penugasanaudit', function($d) use ($tahun){
				                                            $d->whereHas('rencanadetail', function($e) use ($tahun){
				                                                $e->whereHas('rencanaaudit', function($f) use ($tahun){
				                                                    $f->where('tahun', 'like', '%' . $tahun . '%');
				                                                });
				                                            });
				                                        });
					                                });
							                    });
                							})->where('status', 2)->count();

    	$data['audit'][0] 		= $kegiatan_audit_all;
    	$data['audit'][1] 		= $kegiatan_audit_close;

    	$data['survey'][0] 		= $survey_all;
    	$data['survey'][1] 		= $survey_close;

    	$data['konsultasi'][0] 	= $kegiatan_konsultasi_all;
    	$data['konsultasi'][1] 	= $kegiatan_konsultasi_close;

    	$data['lain'][0] 		= $kegiatan_lain_all;
    	$data['lain'][1] 		= $kegiatan_lain_close;
    	return $data;
    }
}

