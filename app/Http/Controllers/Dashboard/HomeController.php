<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\KegiatanAudit\Rencana\RencanaAudit;
use App\Models\KegiatanAudit\Rencana\RencanaAuditDetail;
use App\Models\KegiatanAudit\Persiapan\SuratPenugasanAudit\PenugasanAudit;
use App\Models\KegiatanAudit\Persiapan\ProgramAudit\ProgramAudit;
use App\Models\KegiatanAudit\Persiapan\TinjauanDokumen\TinjauanDokumen;
use App\Models\KegiatanAudit\Pelaksanaan\OpeningMeeting\OpeningMeeting;
use App\Models\KegiatanAudit\Pelaksanaan\ClosingMeeting\ClosingMeeting;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKka;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaDetail;
use App\Models\KegiatanAudit\Pelaksanaan\DraftKka\DraftKkaDetailRekomendasi;
use App\Models\KegiatanAudit\TindakLanjut\RegisterTL;
use App\Models\KegiatanAudit\Pelaporan\LHA;
use App\Models\Rapat\RapatInternal;
use App\Libraries\CoreSn;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;
use App\Models\Survey\SurveyJawab;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $routes = 'dashboard.home';
    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->middleware('auth');

        $this->listStructs = [
        	'progress' => [
		        	[
		                'data' => 'num',
		                'name' => 'num',
		                'label' => '#',
		                'orderable' => false,
		                'searchable' => false,
		                'className' => 'text-center',
		                'width' => '20px',
		            ],
		            [
		                'data' => 'tahun',
		                'name' => 'tahun',
		                'label' => 'Tahun',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'rencana',
		                'name' => 'rencana',
		                'label' => 'Rencana Pelaksanaan',
		                'className' => 'text-center',
		                'width' => '300px',
		            ],
		            [
		                'data' => 'tipe',
		                'name' => 'tipe',
		                'label' => 'Tipe',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'kategori',
		                'name' => 'kategori',
		                'label' => 'Kategori',
		                'className' => 'text-center',
		                'width' => '200px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'object_audit',
		                'name' => 'object_audit',
		                'label' => 'Objek Audit',
		                'className' => 'text-center',
		                'width' => '250px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'no_ab',
		                'name' => 'no_ab',
		                'label' => 'No AB',
		                'className' => 'text-left',
		                'width' => '150px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'stage_awal',
		                'name' => 'stage_awal',
		                'label' => 'Stage Saat Ini',
		                'className' => 'text-center',
		                'width' => '250px',
		                'sortable' => true,
		            ],
		            [
		                'data' => 'stage_akhir',
		                'name' => 'stage_akhir',
		                'label' => 'Stage Selanjutnya',
		                'className' => 'text-center',
		                'width' => '250px',
		                'sortable' => true,
		            ],
        	],
        ];
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      // Nitip cek notif
      SurveyJawab::checkSurveyForNotif();
    	$rapat = RapatInternal::get();
    	$arraypt = array();
    	$datas ='';
    	if($rapat){
	    	foreach ($rapat as $key => $value) {
	    		$datas = implode("<br>", $value->agendaInternal->pluck('agenda')->toArray());
		    	$arraypt[] = [
				                'startDate' => \Carbon\Carbon::parse($value->tanggal)->format('m/d/Y')." ".$value->jam_mulai,
				                'endDate' => \Carbon\Carbon::parse($value->tanggal)->format('m/d/Y')." ".$value->jam_selesai,
				                'summary' => $datas,
				            ];
	    	}
    	}
        return $this->render('modules.dashboard.home', [
        	'mockup' => true,
        	'structs' => $this->listStructs,
        	'rapat' => $arraypt,
        ]);
    }


    public function gridProgress(Request $request)
    {
    	$tahun = $request->tahun_progress;
        $records = RencanaAuditDetail::whereHas('rencanaaudit', function($a) use ($tahun){
        								$a->where('tahun', $tahun);
        							  })
        							  ->where('flag', 1)->where('tipe', 0)->select('*');
        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if($tahun = request()->tahun_progress) {
    		$records->whereHas('rencanaaudit', function($z) use ($tahun){
    			$z->where('tahun', $tahun);
    		});
        }
        return DataTables::of($records->get())
           ->addColumn('num', function ($record) {
               return request()->start;
           })
           ->addColumn('tahun', function ($record) {
               return $record->rencanaaudit->tahun;
           })
           ->addColumn('tipe', function ($record) {
                $tipe = '';
                if($record->tipe == 0){
               	  $tipe = '<span class="label label-primary">Audit</span>';
                }elseif($record->tipe == 1){
               	  $tipe = '<span class="label label-success">Kegiatan Konsultasi</span>';
                }else{
                  $tipe = '<span class="label label-info">Kegiatan Lain - Lain</span>';
                }
               	
                return $tipe;
           })
           ->addColumn('object_audit', function ($record) {
               return object_audit($record->tipe_object, $record->object_id);
           })
           ->addColumn('no_ab', function ($record) {
               return getAb($record->tipe_object, $record->object_id);
           })
           ->addColumn('kategori', function ($record) {
                $kategori = '';
                if($record->tipe == 0){
	                if($record->tipe_object == 0){
	               	  $kategori = '<span class="label label-success">Business Unit (BU)</span>';
	                }elseif($record->tipe_object == 1){
	               	  $kategori = '<span class="label label-info">Corporate Office (CO)</span>';
	                }elseif($record->tipe_object == 2){
	               	  $kategori = '<span class="label label-warning">Project</span>';
	                }else{
	               	  $kategori = '<span class="label label-primary">Anak Perusahaan</span>';
	                }
                }elseif($record->tipe == 1){
               	  	$kategori = $record->konsultasi->nama;
                }else{
                    $kategori = $record->lain->nama;
                }
                return $kategori;
           })
           ->addColumn('rencana', function ($record) {
               return $record->rencana;
           })
           ->addColumn('stage_awal', function ($record) {
           		if($record->status_penugasan == 1){
           			if($record->penugasan){
           				if($record->penugasan->status_audit == 0 && $record->penugasan->status_tinjauan == 0){
           					return 'Tinjauan Dokumen dan Program Audit';
           				}else{
           					if($record->penugasan->status_audit == 1){
           						if($record->penugasan->programaudit){
           							if($record->penugasan->programaudit->status < 3){
           								return 'Program Audit';
           							}else{
           								if($record->penugasan->programaudit->kka){
           									if($record->penugasan->programaudit->kka->status < 4){
           										return 'KKA';
           									}else{
           										if($record->penugasan->programaudit->kka->lha){
           											if($record->penugasan->programaudit->kka->lha->status < 6){
           												return 'LHA';
           											}else{
           												return 'Tindak Lanjut';
           											}
           										}else{
           											return 'KKA';
           										}
           									}
           								}else{
	           								return 'KKA dan Opening Meeting';
           								}
           							}
           						}else{
	           						return 'Program Audit';
           						}
           					}else{
           						return 'Tinjauan Dokumen dan Program Audit';
           					}
           				}
           			}else{
	           		   return 'Surat Penugasan';
           			}
           		}else{
	               return 'RKIA';
           		}
           })
           ->addColumn('stage_akhir', function ($record) {
               if($record->status_penugasan == 1){
           			if($record->penugasan){
           				if($record->penugasan->status_audit == 0 && $record->penugasan->status_tinjauan == 0){
           					return 'KKA';
           				}else{
           					if($record->penugasan->status_audit == 1){
           						if($record->penugasan->programaudit){
           							if($record->penugasan->programaudit->status < 3){
           								return 'KKA / Opening Meeting';
           							}else{
           								if($record->penugasan->programaudit->kka){
           									if($record->penugasan->programaudit->kka->status < 4){
           										return 'LHA';
           									}else{
           										if($record->penugasan->programaudit->kka->lha){
           											if($record->penugasan->programaudit->kka->lha->status < 6){
           												return 'Tindak Lanjut';
           											}else{
           												return 'Close';
           											}
           										}else{
           											return 'LHA';
           										}
           									}
           								}else{
	           								return 'LHA / Closing Meeting';
           								}
           							}
           						}else{
	           						return 'KKA / Opening Meeting';
           						}
           					}else{
           						return 'KKA';
           					}
           				}
           			}else{
	           		   return 'Tinjauan Dokumen / Program Audit';
           			}
           		}else{
	               return 'Surat Penugasan';
           		}
           })
           ->rawColumns(['tipe','object_audit','kategori','rencana'])
           ->make(true);
    }

    public function gridTahap(Request $request)
    {
        $tahun = $request->val;
    	$rkia_all 	 				= RencanaAuditDetail::whereHas('rencanaaudit', function($u) use ($tahun){
    													$u->where('tahun', $tahun)
    													  ->whereIn('status', [0,1,2,3]);
    												 })
    												 ->where('flag', 1)->where('tipe', 0)->get()->count();
    	$rkia_close 	 			= RencanaAuditDetail::whereHas('rencanaaudit', function($u) use ($tahun){
    													$u->where('tahun', $tahun)
    													  ->where('status', 4);
    												 })
    												 ->where('flag', 1)->where('tipe', 0)->get()->count();
    	$penugasan_1 	 			= RencanaAuditDetail::whereHas('rencanaaudit', function($u) use ($tahun){
			        							   		$u->whereNotNull('bukti')->where('tahun', $tahun);
			        							     })
			        							     ->where('status_penugasan', 0)
			        							     ->where('flag', 1)
			        							     ->where('tipe', 0)
			        							     ->get()->count();
		$penugasan_2	 			= PenugasanAudit::whereHas('rencanadetail', function($u) use ($tahun){
														$u->whereHas('rencanaaudit', function($z) use ($tahun){
															$z->where('tahun', $tahun);
														});
													  })
													  ->where('status', '!=', 0)
				    								  ->where('status', '!=', 4)
				    								  ->get()->count();
		$penugasan_all = $penugasan_1 + $penugasan_2;
    	$penugasan_close 	 		= PenugasanAudit::whereHas('rencanadetail', function($u) use ($tahun){
														$u->whereHas('rencanaaudit', function($z) use ($tahun){
															$z->where('tahun', $tahun);
														});
													  })
    												  ->whereIn('status', [4,5,6])->get()->count();

        $tinjauandokumen_1 = PenugasanAudit::whereHas('rencanadetail', function($u) use ($tahun){
														$u->whereHas('rencanaaudit', function($z) use ($tahun){
															$z->where('tahun', $tahun);
														});
													  })->where('status_tinjauan', 0)->whereIn('status', [1,2,3,4])->get()->count();
       	$tinjauandokumen_2 = TinjauanDokumen::whereHas('penugasanaudit', function($a) use ($tahun){
	       										$a->whereHas('rencanadetail', function($u) use ($tahun){
													$u->whereHas('rencanaaudit', function($z) use ($tahun){
														$z->where('tahun', $tahun);
													});
												 });
       										  })
       										   ->whereIn('status', [1,2,3,4])->get()->count();
       	$tinjauandokumen_all = $tinjauandokumen_1 + $tinjauandokumen_2;

    	$tinjauandokumen_close 	 	= TinjauanDokumen::whereHas('penugasanaudit', function($a) use ($tahun){
	       										$a->whereHas('rencanadetail', function($u) use ($tahun){
													$u->whereHas('rencanaaudit', function($z) use ($tahun){
														$z->where('tahun', $tahun);
													});
												 });
       										  })
       										   ->where('status', 5)->get()->count();

        $programaudit_1 = PenugasanAudit::whereHas('rencanadetail', function($u) use ($tahun){
														$u->whereHas('rencanaaudit', function($z) use ($tahun){
															$z->where('tahun', $tahun);
														});
													  })
    												  ->where('status_audit', 0)->whereIn('status', [1,2,3,4])->get()->count();
    	$programaudit_2 = ProgramAudit::whereHas('penugasanaudit', function($a) use ($tahun){
    									$a->whereHas('rencanadetail', function($u) use ($tahun){
											$u->whereHas('rencanaaudit', function($z) use ($tahun){
												$z->where('tahun', $tahun);
											});
										 });
    								  })
									  ->whereIn('status', [1,2])->get()->count();

    	$programaudit_all 		= $programaudit_1 + $programaudit_2;
    	$programaudit_close 	= ProgramAudit::whereHas('penugasanaudit', function($a) use ($tahun){
				    									$a->whereHas('rencanadetail', function($u) use ($tahun){
															$u->whereHas('rencanaaudit', function($z) use ($tahun){
																$z->where('tahun', $tahun);
															});
														 });
			    								   })
												   ->whereIn('status', [3,4])->get()->count();

	    $opening_1 = ProgramAudit::whereHas('penugasanaudit', function($a) use ($tahun){
				    									$a->whereHas('rencanadetail', function($u) use ($tahun){
															$u->whereHas('rencanaaudit', function($z) use ($tahun){
																$z->where('tahun', $tahun);
															});
														 });
			    								   })
												   ->doesntHave('opening')->where('status', 3)->get()->count();
		$opening_2 = OpeningMeeting::whereHas('program', function($a) use ($tahun){
										$a->whereHas('penugasanaudit', function($b) use ($tahun){
		    								$b->whereHas('rencanadetail', function($u) use ($tahun){
												$u->whereHas('rencanaaudit', function($z) use ($tahun){
													$z->where('tahun', $tahun);
												});
											});
	    								});
									})->where('status', 1)->get()->count();

    	$opening_all = $opening_1 + $opening_2;
    	$opening_close 	 	    	= OpeningMeeting::whereHas('program', function($a) use ($tahun){
										$a->whereHas('penugasanaudit', function($b) use ($tahun){
		    								$b->whereHas('rencanadetail', function($u) use ($tahun){
												$u->whereHas('rencanaaudit', function($z) use ($tahun){
													$z->where('tahun', $tahun);
												});
											});
	    								});
									  })->where('status', 2)->get()->count();
    	$kka_1 = ProgramAudit::whereHas('penugasanaudit', function($a) use ($tahun){
									$a->whereHas('rencanadetail', function($u) use ($tahun){
										$u->whereHas('rencanaaudit', function($z) use ($tahun){
											$z->where('tahun', $tahun);
										});
									 });
							   })
							   ->where('status', 3)->get()->count();
		$kka_2 = DraftKka::whereHas('program', function($a) use ($tahun){
									$a->whereHas('penugasanaudit', function($b) use ($tahun){
										$b->whereHas('rencanadetail', function($u) use ($tahun){
											$u->whereHas('rencanaaudit', function($z) use ($tahun){
												$z->where('tahun', $tahun);
											});
										 });
								    });
								})
							    ->whereIn('status', [1,2,3])->get()->count();

    	$kka_all = $kka_1 + $kka_2;
    	$kka_close 	 	    		= DraftKka::whereHas('program', function($a) use ($tahun){
													$a->whereHas('penugasanaudit', function($b) use ($tahun){
														$b->whereHas('rencanadetail', function($u) use ($tahun){
															$u->whereHas('rencanaaudit', function($z) use ($tahun){
																$z->where('tahun', $tahun);
															});
														 });
												    });
												})
											    ->whereIn('status', [4,5])->get()->count();

		$closing_1 = OpeningMeeting::whereHas('program', function($a) use ($tahun){
										$a->whereHas('penugasanaudit', function($b) use ($tahun){
		    								$b->whereHas('rencanadetail', function($u) use ($tahun){
												$u->whereHas('rencanaaudit', function($z) use ($tahun){
													$z->where('tahun', $tahun);
												});
											});
	    								});
									  })->doesntHave('closing')->where('status', 2)->get()->count();
		$closing_2 = ClosingMeeting::whereHas('opening', function($a) use ($tahun){
										$a->whereHas('program', function($b) use ($tahun){
											$b->whereHas('penugasanaudit', function($c) use ($tahun){
			    								$c->whereHas('rencanadetail', function($u) use ($tahun){
													$u->whereHas('rencanaaudit', function($z) use ($tahun){
														$z->where('tahun', $tahun);
													});
												});
		    								});
										});
									})->where('status', 1)->get()->count();
    	$closing_all = $closing_1 + $closing_2;
    	$closing_close 	 	    	= ClosingMeeting::whereHas('opening', function($a) use ($tahun){
														$a->whereHas('program', function($b) use ($tahun){
															$b->whereHas('penugasanaudit', function($c) use ($tahun){
							    								$c->whereHas('rencanadetail', function($u) use ($tahun){
																	$u->whereHas('rencanaaudit', function($z) use ($tahun){
																		$z->where('tahun', $tahun);
																	});
																});
						    								});
														});
													 })->whereIn('status', [2,3])->get()->count();
    	$lha_1 = DraftKka::whereHas('program', function($a) use ($tahun){
									$a->whereHas('penugasanaudit', function($b) use ($tahun){
										$b->whereHas('rencanadetail', function($u) use ($tahun){
											$u->whereHas('rencanaaudit', function($z) use ($tahun){
												$z->where('tahun', $tahun);
											});
										 });
								    });
								})
							    ->where('status', 4)->get()->count();
		$lha_2 = LHA::whereHas('draftkka', function($a) use ($tahun){
						$a->whereHas('program', function($b) use ($tahun){
							$b->whereHas('penugasanaudit', function($c) use ($tahun){
								$c->whereHas('rencanadetail', function($u) use ($tahun){
									$u->whereHas('rencanaaudit', function($z) use ($tahun){
										$z->where('tahun', $tahun);
									});
								 });
						    });
						});
					})->whereIn('status', [1,2,3,4,5])->get()->count();
    	$lha_all = $lha_1 + $lha_2;
    	$lha_close = LHA::whereHas('draftkka', function($a) use ($tahun){
						$a->whereHas('program', function($b) use ($tahun){
							$b->whereHas('penugasanaudit', function($c) use ($tahun){
								$c->whereHas('rencanadetail', function($u) use ($tahun){
									$u->whereHas('rencanaaudit', function($z) use ($tahun){
										$z->where('tahun', $tahun);
									});
								 });
						    });
						});
					 })->where('status', 6)->get()->count();
    	$data[0] = [$rkia_all,$penugasan_all,$tinjauandokumen_all,$programaudit_all,$opening_all,$kka_all,$closing_all,$lha_all];
    	$data[1] = [$rkia_close,$penugasan_close,$tinjauandokumen_close,$programaudit_close,$opening_close,$kka_close,$closing_close,$lha_close];

    	return $this->render('modules.dashboard.tahap-audit', [
            'onprogress' => implode(",", $data[0]),
            'close' => implode(",", $data[1])
        ]);
    }

    public function gridKegiatan(Request $request)
    {
    	$min = $request->min;
    	$max = $request->max;
    	$rkia_all = array();
        if($request->min < $request->max){
	    	$years = range($min, $max);
	    	foreach ($years as $key => $value) {
	        	$rkia_all[]= RencanaAuditDetail::whereHas('rencanaaudit', function($u) use ($value){
												$u->where('tahun', $value);
											 })
											 ->where('flag', 1)->where('status_lha', 0)->get()->count();
				$rkia_close[]= RencanaAuditDetail::whereHas('rencanaaudit', function($u) use ($value){
												$u->where('tahun', $value);
											 })
											 ->where('flag', 1)->where('status_lha', 1)->get()->count();
	    	}
        }
    	return $this->render('modules.dashboard.kegiatan-internal', [
            'onprogress' => implode(",", $rkia_all),
            'close' => implode(",", $rkia_close),
            'tahun' => implode(",", $years),
        ]);
    }

    public function gridTemuan(Request $request)
    {
    	$tahun = $request->tgl_temuan;
    	$operasional = DraftKkaDetail::whereHas('draft', function($a) use($tahun){
    							$a->whereHas('program', function($b) use($tahun){
    								$b->whereHas('penugasanaudit', function($c) use($tahun){
    									$c->whereHas('rencanadetail', function($d) use($tahun){
    										$d->whereHas('rencanaaudit', function($e) use($tahun){
    											$e->where('tahun', $tahun);
    										});
    									});
    								});
    							});
    						 })->where('tipe', 0)->get()->count();
    	$keuangan = DraftKkaDetail::whereHas('draft', function($a) use($tahun){
    							$a->whereHas('program', function($b) use($tahun){
    								$b->whereHas('penugasanaudit', function($c) use($tahun){
    									$c->whereHas('rencanadetail', function($d) use($tahun){
    										$d->whereHas('rencanaaudit', function($e) use($tahun){
    											$e->where('tahun', $tahun);
    										});
    									});
    								});
    							});
    						 })->where('tipe', 1)->get()->count();
    	$sistem = DraftKkaDetail::whereHas('draft', function($a) use($tahun){
    							$a->whereHas('program', function($b) use($tahun){
    								$b->whereHas('penugasanaudit', function($c) use($tahun){
    									$c->whereHas('rencanadetail', function($d) use($tahun){
    										$d->whereHas('rencanaaudit', function($e) use($tahun){
    											$e->where('tahun', $tahun);
    										});
    									});
    								});
    							});
    						 })->where('tipe', 2)->get()->count();
    	$data=[$operasional, $keuangan, $sistem];
    	return $this->render('modules.dashboard.temuan', [
            'data' => implode(",", $data),
        ]);
    }

    public function gridTemuans(Request $request)
    {
    	$tahun = $request->tgl_temuans;
    	$belum1 = DraftKkaDetail::whereHas('draft', function($a) use($tahun){
    							$a->whereHas('program', function($b) use($tahun){
    								$b->whereHas('penugasanaudit', function($c) use($tahun){
    									$c->whereHas('rencanadetail', function($d) use($tahun){
    										$d->whereHas('rencanaaudit', function($e) use($tahun){
    											$e->where('tahun', $tahun);
    										});
    									});
    								});
    							});
    						 })->where('tipe', 0)->where('status_tl', 1)->get()->count();
    	$belum2 = DraftKkaDetail::whereHas('draft', function($a) use($tahun){
    							$a->whereHas('program', function($b) use($tahun){
    								$b->whereHas('penugasanaudit', function($c) use($tahun){
    									$c->whereHas('rencanadetail', function($d) use($tahun){
    										$d->whereHas('rencanaaudit', function($e) use($tahun){
    											$e->where('tahun', $tahun);
    										});
    									});
    								});
    							});
    						 })->where('tipe', 1)->where('status_tl', 1)->get()->count();
    	$belum3 = DraftKkaDetail::whereHas('draft', function($a) use($tahun){
    							$a->whereHas('program', function($b) use($tahun){
    								$b->whereHas('penugasanaudit', function($c) use($tahun){
    									$c->whereHas('rencanadetail', function($d) use($tahun){
    										$d->whereHas('rencanaaudit', function($e) use($tahun){
    											$e->where('tahun', $tahun);
    										});
    									});
    								});
    							});
    						 })->where('tipe', 2)->where('status_tl', 1)->get()->count();
    	$belums = array($belum1,$belum2,$belum3);

    	$sudah1 = DraftKkaDetail::whereHas('draft', function($a) use($tahun){
    							$a->whereHas('program', function($b) use($tahun){
    								$b->whereHas('penugasanaudit', function($c) use($tahun){
    									$c->whereHas('rencanadetail', function($d) use($tahun){
    										$d->whereHas('rencanaaudit', function($e) use($tahun){
    											$e->where('tahun', $tahun);
    										});
    									});
    								});
    							});
    						 })->where('tipe', 0)->where('status_tl', 2)->get()->count();
    	$sudah2 = DraftKkaDetail::whereHas('draft', function($a) use($tahun){
    							$a->whereHas('program', function($b) use($tahun){
    								$b->whereHas('penugasanaudit', function($c) use($tahun){
    									$c->whereHas('rencanadetail', function($d) use($tahun){
    										$d->whereHas('rencanaaudit', function($e) use($tahun){
    											$e->where('tahun', $tahun);
    										});
    									});
    								});
    							});
    						 })->where('tipe', 1)->where('status_tl', 2)->get()->count();
    	$sudah3 = DraftKkaDetail::whereHas('draft', function($a) use($tahun){
    							$a->whereHas('program', function($b) use($tahun){
    								$b->whereHas('penugasanaudit', function($c) use($tahun){
    									$c->whereHas('rencanadetail', function($d) use($tahun){
    										$d->whereHas('rencanaaudit', function($e) use($tahun){
    											$e->where('tahun', $tahun);
    										});
    									});
    								});
    							});
    						 })->where('tipe', 2)->where('status_tl', 2)->get()->count();

    	$sudahs = array($sudah1,$sudah2,$sudah3);
    	return $this->render('modules.dashboard.temuans', [
            'belum' => implode(",", $belums),
            'sudah' => implode(",", $sudahs),
        ]);
    }

    public function gridTl(Request $request)
    {
    	$tahun = $request->tgl_tl;
    	$operasional = DraftKkaDetailRekomendasi::whereHas('draftdetail', function($aa) use($tahun){
		    							$aa->whereHas('draft', function($a) use($tahun){
			    							$a->whereHas('program', function($b) use($tahun){
			    								$b->whereHas('penugasanaudit', function($c) use($tahun){
			    									$c->whereHas('rencanadetail', function($d) use($tahun){
			    										$d->whereHas('rencanaaudit', function($e) use($tahun){
			    											$e->where('tahun', $tahun);
			    										});
			    									});
			    								});
			    							});
			    						 })->where('tipe', 0);
		    						 })->get()->count();
		$keuangan = DraftKkaDetailRekomendasi::whereHas('draftdetail', function($aa) use($tahun){
		    							$aa->whereHas('draft', function($a) use($tahun){
			    							$a->whereHas('program', function($b) use($tahun){
			    								$b->whereHas('penugasanaudit', function($c) use($tahun){
			    									$c->whereHas('rencanadetail', function($d) use($tahun){
			    										$d->whereHas('rencanaaudit', function($e) use($tahun){
			    											$e->where('tahun', $tahun);
			    										});
			    									});
			    								});
			    							});
			    						 })->where('tipe', 1);
		    						 })->get()->count();

		$sistem = DraftKkaDetailRekomendasi::whereHas('draftdetail', function($aa) use($tahun){
		    							$aa->whereHas('draft', function($a) use($tahun){
			    							$a->whereHas('program', function($b) use($tahun){
			    								$b->whereHas('penugasanaudit', function($c) use($tahun){
			    									$c->whereHas('rencanadetail', function($d) use($tahun){
			    										$d->whereHas('rencanaaudit', function($e) use($tahun){
			    											$e->where('tahun', $tahun);
			    										});
			    									});
			    								});
			    							});
			    						 })->where('tipe', 2);
		    						 })->get()->count();
		$data=[$operasional, $keuangan, $sistem];
    	return $this->render('modules.dashboard.tl', [
            'data' => implode(",", $data),
        ]);
    }

    public function gridTls(Request $request)
    {
    	$tahun = $request->tgl_tls;
    	$belum1 = DraftKkaDetailRekomendasi::whereHas('draftdetail', function($aa) use($tahun){
		    							$aa->whereHas('draft', function($a) use($tahun){
			    							$a->whereHas('program', function($b) use($tahun){
			    								$b->whereHas('penugasanaudit', function($c) use($tahun){
			    									$c->whereHas('rencanadetail', function($d) use($tahun){
			    										$d->whereHas('rencanaaudit', function($e) use($tahun){
			    											$e->where('tahun', $tahun);
			    										});
			    									});
			    								});
			    							});
			    						 })->where('tipe', 0);
		    						 })->whereHas('register', function($cc){
		    						 	$cc->whereIn('stage', [0,2]);
		    						 })->get()->count();
		$belum2 = DraftKkaDetailRekomendasi::whereHas('draftdetail', function($aa) use($tahun){
		    							$aa->whereHas('draft', function($a) use($tahun){
			    							$a->whereHas('program', function($b) use($tahun){
			    								$b->whereHas('penugasanaudit', function($c) use($tahun){
			    									$c->whereHas('rencanadetail', function($d) use($tahun){
			    										$d->whereHas('rencanaaudit', function($e) use($tahun){
			    											$e->where('tahun', $tahun);
			    										});
			    									});
			    								});
			    							});
			    						 })->where('tipe', 1);
		    						 })->whereHas('register', function($cc){
		    						 	$cc->whereIn('stage', [0,2]);
		    						 })->get()->count();

		$belum3 = DraftKkaDetailRekomendasi::whereHas('draftdetail', function($aa) use($tahun){
		    							$aa->whereHas('draft', function($a) use($tahun){
			    							$a->whereHas('program', function($b) use($tahun){
			    								$b->whereHas('penugasanaudit', function($c) use($tahun){
			    									$c->whereHas('rencanadetail', function($d) use($tahun){
			    										$d->whereHas('rencanaaudit', function($e) use($tahun){
			    											$e->where('tahun', $tahun);
			    										});
			    									});
			    								});
			    							});
			    						 })->where('tipe', 2);
		    						 })->whereHas('register', function($cc){
		    						 	$cc->whereIn('stage', [0,2]);
		    						 })->get()->count();

		$belums= array($belum1,$belum2,$belum3);    	

    	$sudah1 = DraftKkaDetailRekomendasi::whereHas('draftdetail', function($aa) use($tahun){
		    							$aa->whereHas('draft', function($a) use($tahun){
			    							$a->whereHas('program', function($b) use($tahun){
			    								$b->whereHas('penugasanaudit', function($c) use($tahun){
			    									$c->whereHas('rencanadetail', function($d) use($tahun){
			    										$d->whereHas('rencanaaudit', function($e) use($tahun){
			    											$e->where('tahun', $tahun);
			    										});
			    									});
			    								});
			    							});
			    						 })->where('tipe', 0);
		    						 })->whereHas('register', function($cc){
		    						 	$cc->where('stage', 4);
		    						 })->get()->count();
		$sudah2 = DraftKkaDetailRekomendasi::whereHas('draftdetail', function($aa) use($tahun){
		    							$aa->whereHas('draft', function($a) use($tahun){
			    							$a->whereHas('program', function($b) use($tahun){
			    								$b->whereHas('penugasanaudit', function($c) use($tahun){
			    									$c->whereHas('rencanadetail', function($d) use($tahun){
			    										$d->whereHas('rencanaaudit', function($e) use($tahun){
			    											$e->where('tahun', $tahun);
			    										});
			    									});
			    								});
			    							});
			    						 })->where('tipe', 1);
		    						 })->whereHas('register', function($cc){
		    						 	$cc->where('stage', 4);
		    						 })->get()->count();

		$sudah3 = DraftKkaDetailRekomendasi::whereHas('draftdetail', function($aa) use($tahun){
		    							$aa->whereHas('draft', function($a) use($tahun){
			    							$a->whereHas('program', function($b) use($tahun){
			    								$b->whereHas('penugasanaudit', function($c) use($tahun){
			    									$c->whereHas('rencanadetail', function($d) use($tahun){
			    										$d->whereHas('rencanaaudit', function($e) use($tahun){
			    											$e->where('tahun', $tahun);
			    										});
			    									});
			    								});
			    							});
			    						 })->where('tipe', 2);
		    						 })->whereHas('register', function($cc){
		    						 	$cc->where('stage', 4);
		    						 })->get()->count();

		$sudahs = array($sudah1,$sudah2,$sudah3); 

    	$onprogress1 = DraftKkaDetailRekomendasi::whereHas('draftdetail', function($aa) use($tahun){
		    							$aa->whereHas('draft', function($a) use($tahun){
			    							$a->whereHas('program', function($b) use($tahun){
			    								$b->whereHas('penugasanaudit', function($c) use($tahun){
			    									$c->whereHas('rencanadetail', function($d) use($tahun){
			    										$d->whereHas('rencanaaudit', function($e) use($tahun){
			    											$e->where('tahun', $tahun);
			    										});
			    									});
			    								});
			    							});
			    						 })->where('tipe', 0);
		    						 })->whereHas('register', function($cc){
		    						 	$cc->where('stage', 3);
		    						 })->get()->count();
		$onprogress2 = DraftKkaDetailRekomendasi::whereHas('draftdetail', function($aa) use($tahun){
		    							$aa->whereHas('draft', function($a) use($tahun){
			    							$a->whereHas('program', function($b) use($tahun){
			    								$b->whereHas('penugasanaudit', function($c) use($tahun){
			    									$c->whereHas('rencanadetail', function($d) use($tahun){
			    										$d->whereHas('rencanaaudit', function($e) use($tahun){
			    											$e->where('tahun', $tahun);
			    										});
			    									});
			    								});
			    							});
			    						 })->where('tipe', 1);
		    						 })->whereHas('register', function($cc){
		    						 	$cc->where('stage', 3);
		    						 })->get()->count();
		$onprogress3 = DraftKkaDetailRekomendasi::whereHas('draftdetail', function($aa) use($tahun){
		    							$aa->whereHas('draft', function($a) use($tahun){
			    							$a->whereHas('program', function($b) use($tahun){
			    								$b->whereHas('penugasanaudit', function($c) use($tahun){
			    									$c->whereHas('rencanadetail', function($d) use($tahun){
			    										$d->whereHas('rencanaaudit', function($e) use($tahun){
			    											$e->where('tahun', $tahun);
			    										});
			    									});
			    								});
			    							});
			    						 })->where('tipe', 2);
		    						 })->whereHas('register', function($cc){
		    						 	$cc->where('stage', 3);
		    						 })->get()->count();
		$sedang = array($onprogress1,$onprogress2,$onprogress3);     						 
    	return $this->render('modules.dashboard.tls', [
            'belum' => implode(",", $belums),
            'sedang' => implode(",", $sedang),
            'sudah' => implode(",", $sudahs),
        ]);
    }
}
